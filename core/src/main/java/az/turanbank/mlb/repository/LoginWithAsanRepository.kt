package az.turanbank.mlb.repository

import az.turanbank.mlb.network.model.request.GetAuthInfoRequestModel
import az.turanbank.mlb.network.model.request.GetPinFromAsanRequestModel
import az.turanbank.mlb.network.model.request.GetTokenRequestModel
import az.turanbank.mlb.network.model.request.LoginWithASANRequestModel
import az.turanbank.mlb.network.service.LoginRegisterService

class LoginWithAsanRepository(private val loginService: LoginRegisterService) {

    fun getPin(model: GetPinFromAsanRequestModel) =
        loginService.getPinFromAsan(model)

    fun checkPin(pin: String) =
        loginService.checkPinForAsanLogin(pin)

    fun getAuthInfo(model: GetAuthInfoRequestModel) =
        loginService.getAuthInfo(model)

    fun loginWithAsan(model: LoginWithASANRequestModel) =
        loginService.loginWithAsan(model)

    fun getToken(model: GetTokenRequestModel) =
        loginService.getToken(model)
}