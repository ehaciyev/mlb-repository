package az.turanbank.mlb.network

data class ServerStatusModel(
    private var statusCode: Int,
    private var statusMessage: String
) {

    override fun toString(): String {
        return "ServerStatusModel(statusCode=$statusCode, " +
                "statusMessage='$statusMessage')"
    }

    fun getMessage() =
        statusMessage

    fun getCode() =
        statusCode
}