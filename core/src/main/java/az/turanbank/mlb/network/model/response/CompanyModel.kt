package az.turanbank.mlb.network.model.response

data class CompanyModel(
    private var id: Long,
    private var name: String,
    private var taxNo: String,
    private var signCount: Int,
    private var signLevel: Int,
    private var certCode: String
) {
    override fun toString(): String {
        return "CompanyModel(" +
                "id=$id, " +
                "name='$name', " +
                "taxNo='$taxNo', " +
                "signCount=$signCount, " +
                "signLevel=$signLevel, " +
                "certCode='$certCode')"
    }
}