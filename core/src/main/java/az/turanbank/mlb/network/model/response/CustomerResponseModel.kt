package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class CustomerResponseModel(
    private var custId: Long,
    private var custCode: Long,
    private var name: String,
    private var surname: String,
    private var patronymic: String,
    private var pin: String,
    private var mobile: String,
    private var user_status: Int,
    private var birthDate: String,
    private var birthPlace: String,
    private var country: String,
    private var city: String,
    private var address: String,
    private var email: String,
    private var gender: String,
    private var work: String,
    private var workPlace: String,
    private var position: String,
    private var workPhone: String,
    private var extNo: String,
    private var block_reason: String,
    private var status: ServerStatusModel,
    private var token: String
) {

    fun getStatus(): ServerStatusModel {
        return status
    }
    fun getCustomerId(): Long {
        return custId
    }

    fun getCustomerPhoneNumber(): String {
        return mobile
    }

    override fun toString(): String {
        return "CustomerResponseModel(" +
                "customer_id=$custId, " +
                "customer_code=$custCode, " +
                "name='$name', " +
                "surname='$surname', " +
                "patronymic='$patronymic', " +
                "pin='$pin', " +
                "mobile_number='$mobile', " +
                "user_status=$user_status, " +
                "birthday='$birthDate', " +
                "birth_place='$birthPlace', " +
                "country='$country', " +
                "city='$city', " +
                "address='$address', " +
                "email='$email', " +
                "gender='$gender', " +
                "work='$work', " +
                "workplace='$workPlace', " +
                "position='$position', " +
                "workPhone='$workPhone', " +
                "extra_number='$extNo', " +
                "block_reason='$block_reason', " +
                "status=$status, " +
                "token='$token'" +
                ")"
    }
}