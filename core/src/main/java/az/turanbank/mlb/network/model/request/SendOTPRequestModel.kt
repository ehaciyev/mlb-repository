package az.turanbank.mlb.network.model.request

data class SendOTPRequestModel(
    private var custId: Long,
    private var mobile: String = ""
)