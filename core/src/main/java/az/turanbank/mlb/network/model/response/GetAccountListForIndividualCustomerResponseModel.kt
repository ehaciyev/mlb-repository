package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class GetAccountListForIndividualCustomerResponseModel(
    private var accountList: List<AccountModel>,
    private var serverStatusModel: ServerStatusModel
)