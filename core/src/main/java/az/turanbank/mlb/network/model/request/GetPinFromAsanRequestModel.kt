package az.turanbank.mlb.network.model.request

data class GetPinFromAsanRequestModel(
    private var phoneNumber: String = "",
    private var userId: String
) {
    override fun toString(): String {
        return "GetPinFromAsanRequestModel" +
                "(phoneNumber='$phoneNumber', " +
                "userId='$userId')"
    }
}