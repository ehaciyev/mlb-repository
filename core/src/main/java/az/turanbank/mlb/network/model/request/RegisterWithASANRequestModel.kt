package az.turanbank.mlb.network.model.request

data class RegisterWithASANRequestModel(
    private var transactionId: Long,
    private var certificate: String,
    private var challenge: String,
    private var pin: String,
    private var custCode: Long
)