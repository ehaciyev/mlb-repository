package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class VerifyOTPResponseModel(
    private var status: ServerStatusModel
) {
    override fun toString(): String {
        return "VerifyOTPResponseModel(status=$status)"
    }
}