package az.turanbank.mlb.network.model.request

data class RegisterIndividualASANRequestModel(
    private var username: String,
    private var password: String,
    private var repeatPassword: String,
    private var custId: Long
)