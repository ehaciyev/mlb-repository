package az.turanbank.mlb.network.model.request

data class GetAuthInfoRequestModel(
    private var phoneNumber: String,
    private var userId: String,
    private var lang: String
)