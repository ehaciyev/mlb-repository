package az.turanbank.mlb.network.model.request

data class VerifyOTPRequestModel(
    private var custId: Long,
    private var confirmCode: Int
)