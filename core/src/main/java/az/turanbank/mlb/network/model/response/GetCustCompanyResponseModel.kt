package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class GetCustCompanyResponseModel(
    private var customerResponseModel: CustomerResponseModel,
    private var companyModel: CompanyModel,
    private var serverStatusModel: ServerStatusModel
)