package az.turanbank.mlb.network.model.request

data class CheckForgotPassJuridicalRequestModel(
    private var pin: String,
    private var taxno: String,
    private var mobile: String
)