package az.turanbank.mlb.network.model.request

data class getAccountListForIndividualCustomer(
    private var custId: String,
    private var token: String
)