package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class GetPinFromAsanResponseModel(
    private var pin: String = "",
    private var status: ServerStatusModel
) {

    fun getPin(): String {
        return pin
    }
    fun getStatus(): ServerStatusModel {
        return status
    }
}