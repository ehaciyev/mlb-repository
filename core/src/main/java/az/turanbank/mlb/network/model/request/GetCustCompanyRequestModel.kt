package az.turanbank.mlb.network.model.request

data class GetCustCompanyRequestModel(
    private var pin: String,
    private var taxNo: String,
    private var mobile: String,
    private var userId: String,
    private var custCompStatus: Int
) {
    override fun toString(): String {
        return "GetCustCompanyRequestModel(" +
                "pin='$pin', " +
                "taxNo='$taxNo', " +
                "mobile='$mobile', " +
                "userId='$userId', " +
                "custCompStatus=$custCompStatus)"
    }
}