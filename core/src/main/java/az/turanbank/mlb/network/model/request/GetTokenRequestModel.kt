package az.turanbank.mlb.network.model.request

data class GetTokenRequestModel(
    private var custId: Long,
    private var device: String,
    private var location: String
)