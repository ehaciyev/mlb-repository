package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class RegisterWithASANResponseModel(
    private var custId: Long,
    private var serverStatusModel: ServerStatusModel
) {
    override fun toString(): String {
        return "RegisterWithASANResponseModel(custId=$custId, serverStatusModel=$serverStatusModel)"
    }
}