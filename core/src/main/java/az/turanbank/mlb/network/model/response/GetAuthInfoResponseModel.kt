package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class GetAuthInfoResponseModel(
    private var phoneNumber: String,
    private var userId: String,
    private var lang: String,
    private var transactionId: Long,
    private var verificationCode: String,
    private var certificate: String,
    private var personalCode: String,
    private var challenge: String,
    private var fullName: String,
    private var status: ServerStatusModel
) {
    var verificationCod = verificationCode
    fun getCert(): String {
        return certificate
    }
    fun getChallenge(): String {
        return challenge
    }
    fun getTranID(): Long {
        return transactionId
    }
    override fun toString(): String {
        return "GetAuthInfoResponseModel(phoneNumber='$phoneNumber', userId='$userId', lang='$lang', transactionId='$transactionId', verificationCode='$verificationCode', certificate='$certificate', personalCode='$personalCode', challenge='$challenge', fullName='$fullName', serverStatusModel=$status)"
    }
}