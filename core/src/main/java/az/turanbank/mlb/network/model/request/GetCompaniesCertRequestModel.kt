package az.turanbank.mlb.network.model.request

data class GetCompaniesCertRequestModel(
    private var pin: String,
    private var mobile: String,
    private var userId: String,
    private var custCompStatus: Int
) {
    override fun toString(): String {
        return "GetCompaniesCertRequestModel" +
                "(pin='$pin', " +
                "mobile='$mobile', " +
                "userId='$userId', " +
                "custCompStatus=$custCompStatus)"
    }
}