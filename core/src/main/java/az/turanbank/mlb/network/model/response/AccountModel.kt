package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class AccountModel(
    private var accountId: Long,
    private var accountName: String,
    private var iban: String,
    private var openDate: String,
    private var currName: String,
    private var branchName: String,
    private var currentBalance: Double,
    private var statusServerStatusModel: ServerStatusModel
)