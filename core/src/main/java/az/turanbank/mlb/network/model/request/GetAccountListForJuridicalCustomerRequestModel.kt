package az.turanbank.mlb.network.model.request

data class GetAccountListForJuridicalCustomerRequestModel(
    private var custId: Long,
    private var companyId: Long,
    private var token: String
)