package az.turanbank.mlb.network.service

import az.turanbank.mlb.config.Configuration
import az.turanbank.mlb.network.ServerResponseModel
import az.turanbank.mlb.network.model.request.CheckIndividualCustomerRequestModel
import az.turanbank.mlb.network.model.request.SendOTPRequestModel
import az.turanbank.mlb.network.model.request.VerifyOTPRequestModel
import az.turanbank.mlb.network.model.request.GetPinFromAsanRequestModel
import az.turanbank.mlb.network.model.request.GetAuthInfoRequestModel
import az.turanbank.mlb.network.model.request.GetTokenRequestModel
import az.turanbank.mlb.network.model.request.LoginWithASANRequestModel
import az.turanbank.mlb.network.model.request.RegisterWithASANRequestModel
import az.turanbank.mlb.network.model.request.RegisterIndividualASANRequestModel
import az.turanbank.mlb.network.model.request.LoginWithUsernameAndPasswordIndividualRequestModel
import az.turanbank.mlb.network.model.request.CheckForgotPassJuridicalRequestModel
import az.turanbank.mlb.network.model.request.RegisterJuridicalCustomerRequestModel
import az.turanbank.mlb.network.model.request.GetCompaniesCertRequestModel
import az.turanbank.mlb.network.model.request.GetCustCompanyRequestModel
import az.turanbank.mlb.network.model.request.RegisterJuridicalASANRequestModel
import az.turanbank.mlb.network.model.response.CustomerResponseModel
import az.turanbank.mlb.network.model.response.SendOTPResponseModel
import az.turanbank.mlb.network.model.response.GetPinFromAsanResponseModel
import az.turanbank.mlb.network.model.response.GetAuthInfoResponseModel
import az.turanbank.mlb.network.model.response.RegisterWithASANResponseModel
import az.turanbank.mlb.network.model.response.GetCompaniesCertResponseModel
import az.turanbank.mlb.network.model.response.GetCustCompanyResponseModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface LoginRegisterService {

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("checkIndividualCustomer")
    fun checkIndividualCustomer(@Body body: CheckIndividualCustomerRequestModel):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("sendOtpCode")
    fun sendOtpCode(@Body sendOTPRequestModel: SendOTPRequestModel):
            Observable<SendOTPResponseModel>
    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("verifyOtpCode")
    fun verifyOtpCode(@Body verifyOTPRequestModel: VerifyOTPRequestModel):
            Observable<ServerResponseModel>
    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("getPinFromAsan")
    fun getPinFromAsan(@Body getPinFromAsanRequestModel: GetPinFromAsanRequestModel):
            Observable<GetPinFromAsanResponseModel>
    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )

    @GET("checkPinForAsanLogin")
    fun checkPinForAsanLogin(@Query("pin") pin: String):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @GET("checkPinForAsanRegister")
    fun checkPinForAsanRegister(@Query("pin") pin: String):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("getAuthInfo")
    fun getAuthInfo(@Body getAuthInfoRequestModel: GetAuthInfoRequestModel):
            Observable<GetAuthInfoResponseModel>
    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("getToken")
    fun getToken(@Body getTokenRequestModel: GetTokenRequestModel):
            Observable<CustomerResponseModel>
    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("loginWithAsan")
    fun loginWithAsan(@Body loginWithASANRequestModel: LoginWithASANRequestModel):
            Observable<ServerResponseModel>
    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("registerWithAsan")
    fun registerWithAsan(@Body registerWithASANRequestModel: RegisterWithASANRequestModel):
            Observable<RegisterWithASANResponseModel>
    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("registerIndividualAsan")
    fun registerIndividualAsan(@Body registerIndividualASANRequestModel: RegisterIndividualASANRequestModel):
            Observable<ServerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("registerIndividualMobile")
    fun registerIndividualMobile(@Body registerIndividualASANRequestModel: RegisterIndividualASANRequestModel):
            Observable<ServerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("loginWithUsrAndPwdIndividual")
    fun loginWithUsrAndPwdIndividual(@Body loginWithUsernameAndPasswordIndividualRequestModel: LoginWithUsernameAndPasswordIndividualRequestModel):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("changePassword")
    fun changePassword(@Body registerIndividualASANRequestModel: RegisterIndividualASANRequestModel):
            Observable<ServerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("checkForgotPassJuridical")
    fun checkForgotPassJuridical(@Body checkForgotPassJuridicalRequestModel: CheckForgotPassJuridicalRequestModel):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("checkForgotPassIndividual")
    fun checkForgotPassIndividual(@Body checkIndividualCustomerRequestModel: CheckIndividualCustomerRequestModel):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("checkUnblockJuridical")
    fun checkUnblockJuridical(@Body checkForgotPassJuridicalRequestModel: CheckForgotPassJuridicalRequestModel):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("checkUnblockIndividual")
    fun checkUnblockIndividual(@Body checkIndividualCustomerRequestModel: CheckIndividualCustomerRequestModel):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @GET("unblockCustomer/")
    fun unblockCustomer(@Query("custId") custId: String):
            Observable<ServerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("checkJuridicalCustomer")
    fun checkJuridicalCustomer(@Body checkForgotPassJuridicalRequestModel: CheckForgotPassJuridicalRequestModel):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("registerJuridicalCustomer")
    fun registerJuridicalCustomer(@Body registerJuridicalCustomerRequestModel: RegisterJuridicalCustomerRequestModel):
            Observable<ServerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @GET("checkPinForAsanJuridicalLogin")
    fun checkPinForAsanJuridicalLogin(@Query("pin") pin: String):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @GET("checkPinForAsanJuridicalRegister")
    fun checkPinForAsanJuridicalRegister(@Query("pin") pin: String):
            Observable<CustomerResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("getCompaniesCert")
    fun getCompaniesCert(@Body getCompaniesCertRequestModel: GetCompaniesCertRequestModel):
            Observable<GetCompaniesCertResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("getCustCompany")
    fun getCustCompany(@Body getCustCompanyRequestModel: GetCustCompanyRequestModel):
            Observable<GetCustCompanyResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("loginWithUsrAndPwdJuridical")
    fun loginWithUsrAndPwdJuridical(@Body loginWithUsernameAndPasswordIndividualRequestModel: LoginWithUsernameAndPasswordIndividualRequestModel):
            Observable<GetCustCompanyResponseModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("registerJuridicalAsan")
    fun registerJuridicalAsan(@Body registerJuridicalASANRequestModel: RegisterJuridicalASANRequestModel):
            Observable<ServerResponseModel>

    companion object Factory {
        fun create(): LoginRegisterService {
            val interceptor = HttpLoggingInterceptor()

            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val gson: Gson =
                GsonBuilder().setLenient().create()
            val okHttpClient: OkHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES).addInterceptor(interceptor)
                .build()

            val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                // .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl(Configuration().loginRegistrationBaseUrl)
                .client(okHttpClient)
                .build()
            return retrofit.create(LoginRegisterService::class.java)
        }
    }
}