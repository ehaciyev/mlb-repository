package az.turanbank.mlb.network.model.request

data class CheckIndividualCustomerRequestModel(
    private var pin: String = "",
    private var mobile: String = ""
)