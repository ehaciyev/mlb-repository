package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class GetAccountRequisitiesResponseModel(
    private var branchCode: Long,
    private var branchName: String,
    private var branchInn: String,
    private var correspondentAccount: String,
    private var swift: String,
    private var address: String,
    private var phone: String,
    private var fax: String,
    private var accountName: String,
    private var currName: String,
    private var iban: String,
    private var accountInn: String,
    private var serverStatusModel: ServerStatusModel
) {
    override fun toString(): String {
        return "GetAccountRequisitiesResponseModel(" +
                "branchCode=$branchCode," +
                " branchName='$branchName', " +
                "branchInn='$branchInn', " +
                "correspondentAccount='$correspondentAccount', " +
                "swift='$swift', " +
                "address='$address', " +
                "phone='$phone', " +
                "fax='$fax', " +
                "accountName='$accountName', " +
                "currName='$currName', " +
                "iban='$iban', " +
                "accountInn='$accountInn', " +
                "serverStatusModel=$serverStatusModel)"
    }
}