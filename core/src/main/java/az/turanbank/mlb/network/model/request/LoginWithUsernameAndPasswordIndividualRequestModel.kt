package az.turanbank.mlb.network.model.request

data class LoginWithUsernameAndPasswordIndividualRequestModel(
    private var username: String,
    private var password: String
) {
    override fun toString(): String {
        return "LoginWithUsernameAndPasswordIndividualRequestModel" +
                "(username='$username', " +
                "password='$password')"
    }
}