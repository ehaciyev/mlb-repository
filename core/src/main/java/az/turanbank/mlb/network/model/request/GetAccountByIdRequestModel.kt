package az.turanbank.mlb.network.model.request

data class GetAccountByIdRequestModel(
    private var custId: Long,
    private var accountId: Long,
    private var token: String
)