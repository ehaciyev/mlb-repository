package az.turanbank.mlb.network.model.request

data class RegisterJuridicalCustomerRequestModel(
    private var pin: String,
    private var taxNo: String,
    private var username: String,
    private var password: String,
    private var repeatPassword: String
) {
    override fun toString(): String {
        return "RegisterJuridicalCustomerRequestModel" +
                "(pin='$pin', " +
                "taxNo='$taxNo'," +
                " username='$username', " +
                "password='$password', " +
                "repeatPassword='$repeatPassword')"
    }
}