package az.turanbank.mlb.network.model.request

data class GetAccountListForIndividualCustomerRequestModel(
    private var custId: Long,
    private var token: String
) {
    override fun toString(): String {
        return "GetAccountListForIndividualCustomerRequestModel" +
                "(custId=$custId, " +
                "token='$token')"
    }
}