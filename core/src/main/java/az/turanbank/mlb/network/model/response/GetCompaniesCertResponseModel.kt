package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

data class GetCompaniesCertResponseModel(
    private var company: List<CompanyModel>,
    private var serverStatusModel: ServerStatusModel
)