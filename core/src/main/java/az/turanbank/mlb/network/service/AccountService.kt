package az.turanbank.mlb.network.service

import az.turanbank.mlb.config.Configuration
import az.turanbank.mlb.network.ServerStatusModel
import az.turanbank.mlb.network.model.request.GetAccountByIdRequestModel
import az.turanbank.mlb.network.model.request.GetAccountListForJuridicalCustomerRequestModel
import az.turanbank.mlb.network.model.request.RegisterJuridicalASANRequestModel
import az.turanbank.mlb.network.model.request.getAccountListForIndividualCustomer
import az.turanbank.mlb.network.model.response.AccountModel
import az.turanbank.mlb.network.model.response.GetAccountRequisitiesResponseModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Body
import java.util.concurrent.TimeUnit

interface AccountService {
    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("getAccountListForIndividualCustomer")
    fun getAccountListForIndividualCustomer(@Body registerJuridicalASANRequestModel: RegisterJuridicalASANRequestModel):
            Observable<ServerStatusModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("getAccountListForJuridicalCustomer")
    fun getAccountListForJuridicalCustomer(@Body getAccountListForJuridicalCustomerRequestModel: GetAccountListForJuridicalCustomerRequestModel):
            Observable<getAccountListForIndividualCustomer>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("getAccountById")
    fun getAccountById(@Body getAccountByIdRequestModel: GetAccountByIdRequestModel):
            Observable<AccountModel>

    @Headers(
        "Charset: UTF-8",
        "Content-Type: application/json",
        "Accept: application/json",
        "authorization: Basic dHVyYW5iYW5rOjEyMzQ1"
    )
    @POST("getAccountRequisites")
    fun getAccountRequisites(@Body getAccountByIdRequestModel: GetAccountByIdRequestModel):
            Observable<GetAccountRequisitiesResponseModel>

    companion object Factory {
        fun create(): AccountService {
            val interceptor = HttpLoggingInterceptor()

            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val gson: Gson =
                GsonBuilder().setLenient().create()
            val okHttpClient: OkHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES).addInterceptor(interceptor)
                .build()

            val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                // .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl(Configuration().accountBaseUrl)
                .client(okHttpClient)
                .build()
            return retrofit.create(AccountService::class.java)
        }
    }
}