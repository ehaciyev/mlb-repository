package az.turanbank.mlb.network.model.request

data class LoginWithASANRequestModel(
    private var transactionId: Long,
    private var certificate: String,
    private var challenge: String
) {
    override fun toString(): String {
        return "LoginWithASANRequestModel(transactionId=$transactionId, certificate='$certificate', challenge='$challenge')"
    }
}