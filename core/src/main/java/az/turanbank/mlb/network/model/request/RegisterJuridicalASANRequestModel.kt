package az.turanbank.mlb.network.model.request

data class RegisterJuridicalASANRequestModel(
    private var pin: String,
    private var taxNo: String,
    private var custId: Long,
    private var compId: Long,
    private var username: String,
    private var password: String,
    private var repeatePassword: String
) {
    override fun toString(): String {
        return "RegisterJuridicalASANRequestModel(" +
                "pin='$pin', " +
                "taxNo='$taxNo', " +
                "custId=$custId, " +
                "compId=$compId, " +
                "username='$username', " +
                "password='$password', " +
                "repeatePassword='$repeatePassword')"
    }
}