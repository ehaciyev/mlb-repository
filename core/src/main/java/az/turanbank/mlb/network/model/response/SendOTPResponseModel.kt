package az.turanbank.mlb.network.model.response

import az.turanbank.mlb.network.ServerStatusModel

class SendOTPResponseModel(
    private var confirmCode: Int,
    private var status: ServerStatusModel
) {

    override fun toString(): String {
        return "SendOTPResponseModel(" +
                "confirmCode=$confirmCode, " +
                "status=$status)"
    }
}