package az.turanbank.ui.home

import android.annotation.SuppressLint
import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.data.remote.model.response.CampaignModel
import az.turanbank.mlb.data.remote.model.response.CardStatementModel
import az.turanbank.mlb.data.remote.model.response.GetProfileImageResponseModel
import az.turanbank.mlb.domain.user.usecase.GetProfileImageUseCase
import az.turanbank.mlb.domain.user.usecase.login.GetCampaignListUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetCardListForJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetIndividualCardListUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.statement.GetCardStatementForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.statement.GetCardStatementForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

interface HomeFragmentViewModelInputs : BaseViewModelInputs {
    fun getCustomerCardList()
    fun getCardStatement()
    fun getCampaigns()
    fun getProfileImage()
}

interface HomeFragmentViewModelOutputs : BaseViewModelOutputs {
    fun cardListSuccess(): PublishSubject<ArrayList<CardListModel>>
    fun cardStatementSuccess(): PublishSubject<ArrayList<CardStatementModel>>
    fun campaignsSuccess(): PublishSubject<ArrayList<CampaignModel>>
    fun cardNotFound(): CompletableSubject
    fun cardStatementNotFound(): CompletableSubject
    fun onStatementError(): PublishSubject<Int>
    fun getProfileImageSuccess(): PublishSubject<GetProfileImageResponseModel>
}

class HomeViewModel @Inject constructor(
    private val getIndividualCardListUseCase: GetIndividualCardListUseCase,
    private val getCardListForJuridicalUseCase: GetCardListForJuridicalUseCase,
    private val getCardStatementForIndividualCustomerUseCase: GetCardStatementForIndividualCustomerUseCase,
    private val getCardStatementForJuridicalCustomerUseCase: GetCardStatementForJuridicalCustomerUseCase,
    private val getCampaignListUseCase: GetCampaignListUseCase,
    private val getProfileImageUseCase: GetProfileImageUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    HomeFragmentViewModelInputs,
    HomeFragmentViewModelOutputs {
    override fun cardStatementNotFound() = cardStatementNotFound

    override fun cardNotFound() = cardNotFound
    val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun cardStatementSuccess() = statement

    private val statementError = PublishSubject.create<Int>()
    private val campaigns = PublishSubject.create<ArrayList<CampaignModel>>()
    private var cards = ArrayList<CardListModel>()
    private val getProfileImageSuccess = PublishSubject.create<GetProfileImageResponseModel>()


    override fun getCampaigns() {
        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }

        getCampaignListUseCase.execute(enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    campaigns.onNext(it.respCampaignList)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun campaignsSuccess() = campaigns

    fun cardsNull () = cards.isEmpty()

    fun getCardId(position: Int) = cards[position].cardId
    fun getCardNumber(position: Int) = cards[position].cardNumber
    fun getCardStatus(position: Int) = cards[position].cardStatus
    @SuppressLint("SimpleDateFormat")
    override fun getCardStatement() {
        val cDate = Date()

        val fDate = SimpleDateFormat("dd-MM-yyyy").format(cDate).replace("-", ".")

        if (isCustomerJuridical) {
            getCardStatementForJuridicalCustomerUseCase.execute(
                custId,
                compId,
                cardNumber = null,
                token = token,
                startDate = fDate,
                endDate = fDate
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        statement.onNext(it.cardStamentList)
                    } else {
                        statementError.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)

        } else {
            getCardStatementForIndividualCustomerUseCase.execute(
                custId = custId,
                cardNumber = null,
                token = token,
                startDate = fDate,
                endDate = fDate
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        statement.onNext(it.cardStamentList)
                    } else {
                        statementError.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        }
    }

    override val inputs: HomeFragmentViewModelInputs = this
    override val outputs: HomeFragmentViewModelOutputs = this

    val cardList = PublishSubject.create<ArrayList<CardListModel>>()
    val statement = PublishSubject.create<ArrayList<CardStatementModel>>()
    private val cardNotFound = CompletableSubject.create()
    private val cardStatementNotFound = CompletableSubject.create()

    override fun cardListSuccess() = cardList

    override fun getCustomerCardList() {
        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }
        if (isCustomerJuridical) {
            getCardListForJuridicalUseCase.execute(custId, compId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    when (it.status.statusCode) {
                        1 -> {
                            cardList.onNext(it.cardList)
                            cards = it.cardList
                        }
                        137 -> cardNotFound.onComplete()
                        else -> error.onNext(1878)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        } else {
            getIndividualCardListUseCase.execute(custId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    when (it.status.statusCode) {
                        1 -> {
                            cardList.onNext(it.cardList)
                            cards = it.cardList
                        }
                        137 -> cardNotFound.onComplete()
                        else -> cardNotFound.onComplete()
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        }
    }


    override fun onStatementError() = statementError

    override fun getProfileImage() {
        var custCompId: Long? = null
        if (isCustomerJuridical) custCompId = compId

        // showProgress.onNext(true)

        getProfileImageUseCase.execute(custId, custCompId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                //   showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    getProfileImageSuccess.onNext(it)
                }
            }, {
                it.printStackTrace()
            })
            .addTo(subscriptions)
    }

    override fun getProfileImageSuccess() =  getProfileImageSuccess
}