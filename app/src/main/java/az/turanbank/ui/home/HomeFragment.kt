package az.turanbank.ui.home

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.response.CampaignModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.ZoomCenterCardLayoutManager
import az.turanbank.mlb.presentation.adapter.MlbHomeCardStatementRecyclerViewAdapter
import az.turanbank.mlb.presentation.adapter.PlasticCardRecyclerViewAdapter
import az.turanbank.mlb.presentation.adapter.StoriesRecyclerViewAdapter
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.cardoperations.CardOperationsActivity
import az.turanbank.mlb.presentation.home.CampaignActivity
import az.turanbank.mlb.presentation.home.MlbHomeActivity
import az.turanbank.mlb.presentation.resources.card.activity.CardActivity
import az.turanbank.mlb.presentation.resources.card.requisit.CardRequisitesActivity
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementInternalActivity
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementTabbedActivity
import com.bumptech.glide.Glide
import com.chahinem.pageindicator.PageIndicator
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.navigation.NavigationView
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_exchange_central_bank.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import java.lang.ClassCastException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class HomeFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory
    private lateinit var homeViewModel: HomeViewModel

    private var myClipboard: ClipboardManager? = null
    lateinit var myClip: ClipData

    private lateinit var statementsProgress: ImageView
    private lateinit var statementsFail: TextView
    private lateinit var plasticCardProgress: ImageView
    private lateinit var plasticCardActionsContainer: LinearLayout

    private lateinit var navigationView: NavigationView

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    private lateinit var bottomSheet: LinearLayout

    lateinit var cardInfoContainer: LinearLayout

    lateinit var cardCopyContainer: LinearLayout

    lateinit var viewBottom: View

    lateinit var shimmerViewContainer: ShimmerFrameLayout

    //lateinit var statementShimmerContainer: ShimmerFrameLayout

    lateinit var cardStatementShimmerContainer: ShimmerFrameLayout

    lateinit var storyShimmerView: ShimmerFrameLayout


    private val storyList = arrayListOf<CampaignModel>()

    private var cardPosition = 0

    private lateinit var pageIndicator: PageIndicator
    private val cardBlocked = PublishSubject.create<Boolean>()
    private lateinit var informationNotFound: TextView
    private lateinit var date: TextView

    private var needReload = false

    private val snapHelper: LinearSnapHelper = object : LinearSnapHelper() {
        override fun findTargetSnapPosition(
            layoutManager: RecyclerView.LayoutManager?,
            velocityX: Int,
            velocityY: Int
        ): Int {

            val centerView = findSnapView(layoutManager) ?: return RecyclerView.NO_POSITION

            val position = layoutManager!!.getPosition(centerView)
            var targetPosition = -1

            if (layoutManager.canScrollHorizontally()) {
                targetPosition = if (velocityX < 0) {
                    position - 1
                } else {
                    position + 1
                }
            }

            if (layoutManager.canScrollVertically()) {
                targetPosition = if (velocityY < 0) {
                    position - 1
                } else {
                    position + 1
                }
            }
            val firstItem = 0
            val lastItem = layoutManager.itemCount - 1
            targetPosition = lastItem.coerceAtMost(targetPosition.coerceAtLeast(firstItem))
            cardPosition = targetPosition
            if (!homeViewModel.cardsNull()) {
                cardBlocking(cardPosition)
            }
            return targetPosition
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        needReload = true
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        // Get the clipboard system service
        myClipboard = activity?.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager

        val storyRecyclerView = root.findViewById<RecyclerView>(R.id.storyRecyclerView)
        plasticCardProgress = root.findViewById(R.id.plasticCardProgress)
        statementsFail = root.findViewById(R.id.cardStatementsRecyclerViewFail)
        statementsProgress = root.findViewById(R.id.statementsProgress)
        date = root.findViewById(R.id.date)

        shimmerViewContainer = root.findViewById(R.id.shimmer_view_container)
        //statementShimmerContainer = root.findViewById(R.id.statementShimmerContainer)
        cardStatementShimmerContainer = root.findViewById(R.id.cardStatementShimmerContainer)
        storyShimmerView = root.findViewById(R.id.storyShimmerView)

        viewBottom = root.findViewById(R.id.view_bottom_sheet_bg)
        bottomSheet = root.findViewById(R.id.bottom_sheet)
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)

        cardInfoContainer = root.findViewById(R.id.cardInfoContainer)
        cardCopyContainer = root.findViewById(R.id.cardCopyContainer)

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> { }
                    BottomSheetBehavior.STATE_EXPANDED -> { }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        viewBottom.visibility = View.GONE
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> { }
                    BottomSheetBehavior.STATE_SETTLING -> { }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                viewBottom.visibility = View.VISIBLE
                viewBottom.alpha = slideOffset
            }
        })

        viewBottom.setOnClickListener {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
        }


        setClickListeners(root)
        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val currentDate = sdf.format(Date()).replace("-", ".")
        date.text = currentDate

        plasticCardActionsContainer = root.findViewById(R.id.plasticCardActionsContainer)
        pageIndicator = root.findViewById(R.id.pageIndicator)
        informationNotFound = root.findViewById(R.id.informationNotFound)

        //animateProgressImage()

        val storyAdapter =
            StoriesRecyclerViewAdapter(storyList) {
                val intent = Intent(requireActivity(), CampaignActivity::class.java)
                intent.putExtra("campaignId", storyList[it].campaignId)
                startActivity(intent)
            }

        snapHelper.attachToRecyclerView(storyRecyclerView)

        val storyRecyclerViewLayoutManager =
            ZoomCenterCardLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        storyRecyclerView.layoutManager = storyRecyclerViewLayoutManager
        storyRecyclerView.adapter = storyAdapter


        setOutputListeners()
        return root
    }

    private fun cardBlocking(position: Int) {
        if (homeViewModel.getCardStatus(position) == "0") {
            cardBlocked.onNext(true)
        } else {
            cardBlocked.onNext(false)
        }
    }

    override fun onResume() {
        super.onResume()
        shimmerViewContainer.startShimmerAnimation()
        //statementShimmerContainer.startShimmerAnimation()
        cardStatementShimmerContainer.startShimmerAnimation()
        storyShimmerView.startShimmerAnimation()
        date.visibility = View.GONE
        if (needReload) {
            homeViewModel.inputs.getCustomerCardList()
            homeViewModel.inputs.getCardStatement()
            needReload = false
        }
    }

    private fun setClickListeners(root: View) {

        val pay = root.findViewById<LinearLayout>(R.id.pay)

        val payImage = root.findViewById<ImageView>(R.id.payImage)

        val requisite = root.findViewById<LinearLayout>(R.id.requisite)

        val statement = root.findViewById<LinearLayout>(R.id.statement)

        val more = root.findViewById<LinearLayout>(R.id.more)
        more.setOnClickListener {
            val intent = Intent(requireActivity(), CardActivity::class.java)
            intent.putExtra("cardId", homeViewModel.getCardId(cardPosition))
            intent.putExtra("cardId", homeViewModel.getCardId(cardPosition))
            intent.putExtra(
                "shortCardNumber",
                homeViewModel.getCardNumber(cardPosition).replaceRange(4, 12, " **** **** ")
            )
            startActivity(intent)
        }
        cardBlocked.subscribe { blocked ->
            if (blocked) {
                payImage.setColorFilter(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.disabled_color
                    )
                )
                requisiteImage.setColorFilter(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.disabled_color
                    )
                )
                statementImage.setColorFilter(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.disabled_color
                    )
                )
            } else {
                if (homeViewModel.isCustomerJuridical) {
                    payImage.setColorFilter(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.disabled_color
                        )
                    )
                } else {
                    payImage.setColorFilter(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.colorPrimary
                        )
                    )
                }
                requisiteImage.setColorFilter(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.colorPrimary
                    )
                )
                statementImage.setColorFilter(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.colorPrimary
                    )
                )
            }
            pay.setOnClickListener {
                if (!blocked && !homeViewModel.isCustomerJuridical) {
                    val intent = Intent(requireActivity(), CardOperationsActivity::class.java)
                    intent.putExtra("cardId", homeViewModel.getCardId(cardPosition))
                    startActivity(intent)
                }
            }

            requisite.setOnClickListener {
                if (!blocked) {
                    val intent = Intent(requireActivity(), CardRequisitesActivity::class.java)
                    intent.putExtra("cardId", homeViewModel.getCardId(cardPosition))
                    intent.putExtra("cardNumber", homeViewModel.getCardNumber(cardPosition))
                    startActivity(intent)
                }
            }

            statement.setOnClickListener {
                if (!blocked) {
                    val intent = Intent(requireActivity(), CardStatementTabbedActivity::class.java)
                    intent.putExtra("cardId", homeViewModel.getCardId(cardPosition))
                    intent.putExtra("cardNumber", homeViewModel.getCardNumber(cardPosition))
                    startActivity(intent)
                }
            }
        }.addTo(subscriptions)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            homeViewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
            setInputListeners()
            val activity = context as MlbHomeActivity
            navigationView = activity.findViewById(R.id.nav_view)
        } catch (ce: ClassCastException){
            throw ClassCastException(context.toString())
        }

    }

    private fun setInputListeners() {
        homeViewModel.inputs.getProfileImage()
        homeViewModel.inputs.getCampaigns()
    }


    private fun setOutputListeners() {
        homeViewModel.outputs.getProfileImageSuccess().subscribe {
            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)
            val navImage = requireActivity().findViewById<ImageView>(R.id.imageView)
            Glide
                .with(this)
                .load(byteArray)
                .centerCrop()
                .into(navImage)
        }.addTo(subscriptions)
        homeViewModel.outputs.cardNotFound().subscribe {
            plasticCardProgress.clearAnimation()
            plasticCardProgress.visibility = View.GONE
            navigationView.menu.findItem(R.id.card_operations).isVisible = false
            //   informationNotFound.visibility = View.VISIBLE

            cardStatementShimmerContainer.stopShimmerAnimation()
            cardStatementShimmerContainer.visibility = View.GONE
        }.addTo(subscriptions)

        homeViewModel.outputs.onStatementError().subscribe {
            statementsProgress.clearAnimation()
            statementsProgress.visibility = View.GONE

            cardStatementsRecyclerViewFail.text = getString(ErrorMessageMapper().getErrorMessage(it))
            cardStatementsRecyclerViewFail.visibility = View.VISIBLE

            // stop animating Shimmer and hide the layout
            shimmerViewContainer.stopShimmerAnimation()
            shimmerViewContainer.visibility = View.GONE

            //statementShimmerContainer.stopShimmerAnimation()
            //statementShimmerContainer.visibility = View.GONE

            cardStatementShimmerContainer.stopShimmerAnimation()
            cardStatementShimmerContainer.visibility = View.GONE

        }.addTo(subscriptions)

        homeViewModel.outputs.cardStatementSuccess().subscribe { list ->

            val adapter =
                MlbHomeCardStatementRecyclerViewAdapter(list) {
                    val intent =
                        Intent(requireActivity(), CardStatementInternalActivity::class.java)
                    intent.putExtra("date", list[it].trDate)
                    intent.putExtra("cardNumber", list[it].cardNumber)
                    intent.putExtra("debetAmount", list[it].dtAmount)
                    intent.putExtra("creditAmount", list[it].crAmount)
                    intent.putExtra("purpose", list[it].purpose)
                    intent.putExtra("currency", list[it].currency)
                    intent.putExtra("fee", list[it].fee)
                    startActivity(intent)
                }
            val llm = LinearLayoutManager(requireContext())
            llm.orientation = LinearLayoutManager.VERTICAL
            cardStatementsRecyclerView.layoutManager = llm
            cardStatementsRecyclerView.adapter = adapter
            statementsProgress.clearAnimation()
            statementsProgress.visibility = View.GONE
            cardStatementShimmerContainer.stopShimmerAnimation()
            cardStatementShimmerContainer.visibility = View.GONE
            date.visibility = View.VISIBLE
        }.addTo(subscriptions)

        homeViewModel.outputs.onError().subscribe{
            AlertDialogMapper(requireActivity(), it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
        homeViewModel.outputs.cardListSuccess().subscribe { cardList ->

            navigationView.menu.findItem(R.id.card_operations).isVisible = true
            val plasticCard = PlasticCardRecyclerViewAdapter(cardList, {iteration->
                    if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                    } else {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
                    }

                    cardInfoContainer.setOnClickListener{view->
                        val intent = Intent(requireActivity(), CardActivity::class.java)
                        intent.putExtra("cardId", homeViewModel.getCardId(cardPosition))
                        intent.putExtra(
                            "shortCardNumber",
                            cardList[iteration].cardNumber.replaceRange(4, 12, " **** **** ")
                        )
                        startActivity(intent)
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
                    }

                    cardCopyContainer.setOnClickListener{
                        copyText("Card Number:" + cardList[iteration].cardNumber)
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
                    }


                },{
                    //copyText(cardList[it].cardNumber)
                })
            val plasticCardRecyclerViewLayoutManager =
                ZoomCenterCardLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            plasticCardRecyclerView.layoutManager = plasticCardRecyclerViewLayoutManager
            snapHelper.attachToRecyclerView(plasticCardRecyclerView)
            plasticCardRecyclerView.adapter = plasticCard
            if (cardList[0].cardStatus == "0") {
                cardBlocked.onNext(true)
            } else {
                cardBlocked.onNext(false)
            }
            pageIndicator.central_recycler_view
            pageIndicator.attachTo(plasticCardRecyclerView)
            //plasticCardProgress.clearAnimation()
            plasticCardProgress.visibility = View.GONE
            plasticCardActionsContainer.visibility = View.VISIBLE
            shimmerViewContainer.stopShimmerAnimation()
            shimmerViewContainer.visibility = View.GONE
            date.visibility = View.VISIBLE

        }.addTo(subscriptions)

        homeViewModel.outputs.campaignsSuccess().subscribe {
            storyList.clear()
            storyList.addAll(it)
            storyRecyclerView.adapter?.notifyDataSetChanged()
            storyShimmerView.stopShimmerAnimation()
            storyShimmerView.visibility = View.GONE

        }.addTo(subscriptions)
    }

    private fun copyText(text: String) {
        myClip = ClipData.newPlainText("text", text)
        myClipboard?.setPrimaryClip(myClip)
        Toast.makeText(requireActivity(), getString(R.string.copyCardNumber), Toast.LENGTH_SHORT).show();
    }

    override fun onPause() {
        super.onPause()
        shimmerViewContainer.stopShimmerAnimation()
        //statementShimmerContainer.stopShimmerAnimation()
        cardStatementShimmerContainer.stopShimmerAnimation()
        storyShimmerView.stopShimmerAnimation()
    }


}