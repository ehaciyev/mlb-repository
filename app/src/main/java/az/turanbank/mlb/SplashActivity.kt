package az.turanbank.mlb

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.data.local.CategoryImageMapper
import az.turanbank.mlb.data.local.CurrencySignMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.WarningPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.login.activities.LoginActivity
import io.reactivex.rxkotlin.addTo
import java.util.*
import javax.inject.Inject


class SplashActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: SplashViewModel

    var isCheckVersion = false

    @Inject
    lateinit var sharedPrefs: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_application)

        ErrorMessageMapper().saveDataToMap()
        CurrencySignMapper().saveSignMap()
        CategoryImageMapper().saveCategoryMapping()

        viewModel = ViewModelProvider(this, factory)[SplashViewModel::class.java]

        val appLocale = sharedPrefs.getString("lang", Resources.getSystem().configuration.locale.language)

        sharedPrefs.edit().putString("lang", appLocale).apply()


        Log.i("PACKAGE", applicationContext.packageName)
        val myLocale = Locale(appLocale)
        val res = resources
        val displayMetrics = res.displayMetrics
        val configuration = res.configuration
        configuration.setLocale(myLocale)
        res.updateConfiguration(configuration, displayMetrics)

        setOutputListeners()
        setInputListeners()

    }

    private fun setOutputListeners() {
        viewModel.outputs.onCheckAppVersionSuccess().subscribe{
            if (viewModel.pinIsSet) {
                viewModel.inputs.checkToken()
            } else {
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            if(it ==295){
                val intent = Intent(this, WarningPageViewActivity::class.java)
                val description = getString(R.string.checkVersionText)
                intent.putExtra("description", description)
                intent.putExtra("isCheckVersion", !viewModel.isCheckVersion)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this, LoginActivity::class.java)
                if(sharedPrefs.getBoolean("pinIsSet",false)){
                    intent.putExtra("description", "$it")
                    startActivity(intent)
                    finish()
                } else {
                    intent.putExtra("description", "$it")
                    startActivity(intent)
                    finish()
                }

            }
        }.addTo(subscriptions)

        viewModel.outputs.tokenUpdated().subscribe {
            startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
            finish()
        }.addTo(subscriptions)

    }

    private fun setInputListeners() {
        viewModel.inputs.checkAppVersion(getString(R.string.versionName))

        /*if (viewModel.pinIsSet) {
            viewModel.inputs.checkToken()
        } else {
            if(!viewModel.isCheckVersion){
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){

            }
        }
    }
}
