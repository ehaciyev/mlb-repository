package az.turanbank.mlb.di.module.viewmodel

import androidx.lifecycle.ViewModel
import az.turanbank.mlb.di.scope.IPScope
import az.turanbank.mlb.di.scope.ViewModelKey
import az.turanbank.mlb.presentation.ips.SendOtpCodeForAliasViewModel
import az.turanbank.mlb.presentation.ips.accounts.delete.DeleteIpsAccountViewModel
import az.turanbank.mlb.presentation.ips.accounts.register.RegisterAccountIpsSystemViewModel
import az.turanbank.mlb.presentation.ips.accounts.setdefault.ChooseAliasForDefaultViewModel
import az.turanbank.mlb.presentation.ips.accounts.setdefault.account.ChooseAccountForDefaultViewModel
import az.turanbank.mlb.presentation.ips.aliases.create.CreateNewAliasViewModel
import az.turanbank.mlb.presentation.ips.aliases.delete.DeleteIPSAliasViewModel
import az.turanbank.mlb.presentation.ips.aliases.link.account.SelectAccountLinkingToAliasViewModel
import az.turanbank.mlb.presentation.ips.aliases.link.alias.LinkAliasToAccountViewModel
import az.turanbank.mlb.presentation.ips.accounts.authorization.IPSAuthorizationViewModel
import az.turanbank.mlb.presentation.ips.accounts.myaccounts.MyLinkedAccountsViewModel
import az.turanbank.mlb.presentation.ips.aliases.link.choosedefault.ChooseDefaultAliasViewModel
import az.turanbank.mlb.presentation.ips.aliases.myaliases.MyAliasesViewModel
import az.turanbank.mlb.presentation.ips.outgoing.MyIncomePaymentsViewModel
import az.turanbank.mlb.presentation.ips.paidToMe.PaidToMeViewModel
import az.turanbank.mlb.presentation.ips.pay.preconfirm.confirm.ConfirmWithIPSViewModel
import az.turanbank.mlb.presentation.ips.pay.preconfirm.TransferWithIPSViewModel
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.account.IPSCreditorAccountChoiceViewModel
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.alias.IPSCreditorAliasChoiceViewModel
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.moreinfo.AccountAndCustomerInfoViewModel
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.account.IPSAccountChoiceViewModel
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.alias.IPSAliasChoiceViewModel
import az.turanbank.mlb.presentation.ips.payrequest.requestor.account.ChooseRequestedAccountViewModel
import az.turanbank.mlb.presentation.ips.payrequest.requestor.alias.ChooseRequestedAliasViewModel
import az.turanbank.mlb.presentation.ips.payrequest.sender.account.ChooseIPSSenderAccountViewModel
import az.turanbank.mlb.presentation.ips.payrequest.sender.alias.ChooseIPSSenderAliasViewModel
import az.turanbank.mlb.presentation.ips.register.InstantPaymentSplashViewModel
import az.turanbank.mlb.presentation.ips.nonregistered.NonRegisteredPaymentViewModel
import az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.account.NonRegisteredIPSCreditorAccountChoiceViewModel
import az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.alias.NonRegisteredIPSCreditorAliasChoiceViewModel
import az.turanbank.mlb.presentation.ips.nonregistered.pay.debitor.NonRegisteredIPSAccountChoiceViewModel
import az.turanbank.mlb.presentation.ips.pay.IPSPayViewModel
import az.turanbank.mlb.presentation.ips.register.account.InstantPaymentAccountMultiSelectViewModel
import az.turanbank.mlb.presentation.ips.accounts.authchoice.InstantPaymentAccountAuthSelectViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class IpsViewModelModule {

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(InstantPaymentSplashViewModel::class)
    abstract fun bindInstantPaymentSplashViewModel(viewModel: InstantPaymentSplashViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(InstantPaymentAccountMultiSelectViewModel::class)
    abstract fun bindInstantPaymentAccountMultiSelectViewModel(viewModel: InstantPaymentAccountMultiSelectViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(ChooseDefaultAliasViewModel::class)
    abstract fun bindChooseDefaultAliasViewModel(viewModel: ChooseDefaultAliasViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(MyLinkedAccountsViewModel::class)
    abstract fun bindMyLinkedAccountsViewModel(viewModel: MyLinkedAccountsViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(InstantPaymentAccountAuthSelectViewModel::class)
    abstract fun bindInstantPaymentAccountAuthSelectViewModel(viewModel: InstantPaymentAccountAuthSelectViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(IPSPayViewModel::class)
    abstract fun bindIPSPayViewModel(viewModel: IPSPayViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(MyAliasesViewModel::class)
    abstract fun bindMyAliasesViewModel(viewModel: MyAliasesViewModel): ViewModel


    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(NonRegisteredIPSCreditorAccountChoiceViewModel::class)
    abstract fun bindNonRegisteredIPSCreditorAccountChoiceViewModel(viewModel: NonRegisteredIPSCreditorAccountChoiceViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(NonRegisteredIPSCreditorAliasChoiceViewModel::class)
    abstract fun bindNonRegisteredIPSCreditorAliasChoiceViewModel(viewModel: NonRegisteredIPSCreditorAliasChoiceViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(NonRegisteredIPSAccountChoiceViewModel::class)
    abstract fun bindNonRegisteredIPSAccountChoiceViewModel(viewModel: NonRegisteredIPSAccountChoiceViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(NonRegisteredPaymentViewModel::class)
    abstract fun bindNonRegisteredPaymentViewModel(viewModel: NonRegisteredPaymentViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(AccountAndCustomerInfoViewModel::class)
    abstract fun bindAccountAndCustomerInfoViewModel(viewModel: AccountAndCustomerInfoViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(IPSAuthorizationViewModel::class)
    abstract fun bindIPSAuthorizationViewModel(viewModel: IPSAuthorizationViewModel): ViewModel

    /*@Binds
    @IPScope
    @IntoMap
    @ViewModelKey(SinglePaymentViewModel::class)
    abstract fun bindSinglePaymentViewModel(viewModel: SinglePaymentViewModel): ViewModel*/


    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(PaidToMeViewModel::class)
    abstract fun bindPaidToMeViewModel(viewModel: PaidToMeViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(IPSAccountChoiceViewModel::class)
    abstract fun bindIPSAccountChoiceViewModel(viewModel: IPSAccountChoiceViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(IPSAliasChoiceViewModel::class)
    abstract fun bindIPSAliasChoiceViewModel(viewModel: IPSAliasChoiceViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(IPSCreditorAccountChoiceViewModel::class)
    abstract fun bindIPSCreditorAccountChoiceViewModel(viewModel: IPSCreditorAccountChoiceViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(IPSCreditorAliasChoiceViewModel::class)
    abstract fun bindIPSCreditorAliasChoiceViewModel(viewModel: IPSCreditorAliasChoiceViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(ChooseRequestedAliasViewModel::class)
    abstract fun bindChooseRequestedAliasViewModel(viewModel: ChooseRequestedAliasViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(ChooseRequestedAccountViewModel::class)
    abstract fun bindChooseRequestedAccountViewModel(viewModel: ChooseRequestedAccountViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(ChooseIPSSenderAccountViewModel::class)
    abstract fun bindChooseIPSSenderAccountViewModel(viewModel: ChooseIPSSenderAccountViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(ChooseIPSSenderAliasViewModel::class)
    abstract fun bindChooseIPSSenderAliasViewModel(viewModel: ChooseIPSSenderAliasViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(DeleteIPSAliasViewModel::class)
    abstract fun bindDeleteIPSAliasViewModel(viewModel: DeleteIPSAliasViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(LinkAliasToAccountViewModel::class)
    abstract fun bindLinkAliasToAccountViewModel(viewModel: LinkAliasToAccountViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(SelectAccountLinkingToAliasViewModel::class)
    abstract fun bindSelectAccountLinkingToAliasViewModel(viewModel: SelectAccountLinkingToAliasViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(SendOtpCodeForAliasViewModel::class)
    abstract fun bindSendOtpCodeForAliasViewModel(viewModel: SendOtpCodeForAliasViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(CreateNewAliasViewModel::class)
    abstract fun bindCreateNewAliasViewModel(viewModel: CreateNewAliasViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(RegisterAccountIpsSystemViewModel::class)
    abstract fun bindRegisterAccountIpsSystemViewModel(viewModel: RegisterAccountIpsSystemViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(TransferWithIPSViewModel::class)
    abstract fun bindTransferWithIPSViewMode(viewModel: TransferWithIPSViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(ConfirmWithIPSViewModel::class)
    abstract fun bindConfirmWithIPSViewModel(viewModel: ConfirmWithIPSViewModel): ViewModel


    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(DeleteIpsAccountViewModel::class)
    abstract fun bindDeleteIpsAccountViewModel(viewModel: DeleteIpsAccountViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(ChooseAliasForDefaultViewModel::class)
    abstract fun bindChooseAliasForDefaultViewModel(viewModel: ChooseAliasForDefaultViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(ChooseAccountForDefaultViewModel::class)
    abstract fun bindChooseAccountForDefaultViewModel(viewModel: ChooseAccountForDefaultViewModel): ViewModel

    @Binds
    @IPScope
    @IntoMap
    @ViewModelKey(MyIncomePaymentsViewModel::class)
    abstract fun bindMyIncomePaymentsViewModel(viewModel: MyIncomePaymentsViewModel): ViewModel
}