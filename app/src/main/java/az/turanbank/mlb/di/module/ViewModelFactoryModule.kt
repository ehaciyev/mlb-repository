package az.turanbank.mlb.di.module

import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.di.ViewModelProviderFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}