package az.turanbank.mlb.di.module

import android.content.Context
import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.Constants
import az.turanbank.mlb.data.remote.MlbInterceptor
import az.turanbank.mlb.presentation.base.BaseActivity
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedInputStream
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import javax.net.ssl.*

const val CONNECT_TIME_OUT = 300L
const val READ_TIMEOUT = 300L

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun providesRetrofitInstance(
        callAdapterFactory: RxJava2CallAdapterFactory,
        converterFactory: GsonConverterFactory
    ): Retrofit = Retrofit.Builder()
        .baseUrl("http://10.0.1.189:8080/mlb/")
        .addCallAdapterFactory(callAdapterFactory)
        .addConverterFactory(converterFactory)
        .build()

    @Provides
    @Singleton
    fun provideGson() = Gson()

    @Provides
    fun provideGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides
    @Singleton
    fun provideRxJavaCallAdapter(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Provides
    @Singleton
    fun provideMlbInterceptor(
        context: Context,
        sharedPrefs: SharedPreferences,
        baseActivity: BaseActivity
    ) = MlbInterceptor(context, baseActivity, sharedPrefs)

    @Provides
    @Singleton
    fun provideHostnameVerifier(

    ) = HostnameVerifier{_, _ ->
        return@HostnameVerifier true
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        mlbInterceptor: MlbInterceptor,
        context: Context,
        hostnameVerifier: HostnameVerifier
    ): OkHttpClient {
        val sslContext = SSLContext.getInstance("TLS")

        var trustManagers: Array<TrustManager> = emptyArray()

        try {
            val keyStore: KeyStore = KeyStore.getInstance(KeyStore.getDefaultType())
            keyStore.load(null, null)

            val certInputStream = context.assets.open(Constants.PEM_FILE)
            val bis = BufferedInputStream(certInputStream)
            val certificateFactory = CertificateFactory.getInstance("X.509")

            while (bis.available() > 0) {
                val cert = certificateFactory.generateCertificate(bis)
                keyStore.setCertificateEntry("www.appservtest.tb/mlb", cert)
            }

            val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(keyStore)
            trustManagers = trustManagerFactory.trustManagers

            sslContext.init(null, trustManagers, null)

        } catch (e: Exception) {
            e.printStackTrace()
        }


        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(mlbInterceptor)
            .hostnameVerifier(hostnameVerifier)
            .sslSocketFactory(sslContext.socketFactory,  trustManagers[0] as X509TrustManager)
            .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .build()

    }
}