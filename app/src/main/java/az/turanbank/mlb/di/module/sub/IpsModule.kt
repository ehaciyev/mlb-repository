package az.turanbank.mlb.di.module.sub

import az.turanbank.mlb.data.remote.Constants
import az.turanbank.mlb.data.remote.model.service.IPSApiService
import az.turanbank.mlb.data.remote.model.service.IPSApiServiceProvider
import az.turanbank.mlb.di.scope.IPScope
import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.domain.user.data.IPSRepositoryType
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

const val MLB_IPS = "mlb_ips"

@Module
class IpsModule {

    @Provides
    @IPScope
    fun providesExchangeApiServiceProvider(@Named(MLB_IPS) retrofit: Retrofit): IPSApiServiceProvider =
        object : IPSApiServiceProvider {
            val authApiService = retrofit.create(IPSApiService::class.java)
            override fun getInstance() = authApiService
        }

    @Named(MLB_IPS)
    @Provides
    @IPScope
    fun providesExchangeRetrofitInstance(
        callAdapterFactory: RxJava2CallAdapterFactory,
        converterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl("${Constants.BASE_URL}ips/")
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(converterFactory)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @IPScope
    fun providesExchangeRepository(serviceProvider: IPSApiServiceProvider): IPSRepositoryType {
        return IPSRepository(serviceProvider = serviceProvider)
    }
}