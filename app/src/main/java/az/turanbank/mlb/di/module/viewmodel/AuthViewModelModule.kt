package az.turanbank.mlb.di.module.viewmodel

import androidx.lifecycle.ViewModel
import az.turanbank.mlb.di.scope.AuthScope
import az.turanbank.mlb.di.scope.ViewModelKey
import az.turanbank.mlb.presentation.login.activities.LoginViewModel
import az.turanbank.mlb.presentation.forgot.change.ChangePasswordViewModel
import az.turanbank.mlb.presentation.forgot.individual.IndividualForgotPasswordViewModel
import az.turanbank.mlb.presentation.forgot.juridical.JuridicalForgotPasswordViewModel
import az.turanbank.mlb.presentation.login.asan.individual.LoginAsanIndividualViewModel
import az.turanbank.mlb.presentation.login.asan.individual.LoginIndividualAsanPinViewModel
import az.turanbank.mlb.presentation.login.asan.juridical.JuridialLoginCompaniesListViewModel
import az.turanbank.mlb.presentation.login.asan.juridical.LoginAsanJuridicalPinViewModel
import az.turanbank.mlb.presentation.login.asan.juridical.LoginAsanJuridicalViewModel
import az.turanbank.mlb.presentation.login.unblock.UnblockUserViewModel
import az.turanbank.mlb.presentation.login.username.LoginUsernamePasswordViewModel
import az.turanbank.mlb.presentation.register.asan.individual.IndividualRegisterAsanPinViewModel
import az.turanbank.mlb.presentation.register.asan.individual.IndividualRegisterAsanViewModel
import az.turanbank.mlb.presentation.register.asan.juridical.JuridialRegisterCompaniesListViewModel
import az.turanbank.mlb.presentation.register.asan.juridical.JuridicalRegisterAsanPinViewModel
import az.turanbank.mlb.presentation.register.asan.juridical.JuridicalRegisterAsanViewModel
import az.turanbank.mlb.presentation.register.mobile.individual.IndividualRegisterMobileViewModel
import az.turanbank.mlb.presentation.register.mobile.individual.RegisterIndividualViewModel
import az.turanbank.mlb.presentation.register.mobile.individual.VerifyIndividualRegisterMobileViewModel
import az.turanbank.mlb.presentation.register.mobile.juridical.JuridicalRegisterMobileViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AuthViewModelModule {

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(IndividualRegisterMobileViewModel::class)
    abstract fun bindIndividualRegisterMobileViewModel(viewModel: IndividualRegisterMobileViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(IndividualRegisterAsanViewModel::class)
    abstract fun bindIndividualRegisterAsanViewModel(viewModel: IndividualRegisterAsanViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(IndividualRegisterAsanPinViewModel::class)
    abstract fun bindIndividualRegisterAsanPinViewModel(viewModel: IndividualRegisterAsanPinViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(VerifyIndividualRegisterMobileViewModel::class)
    abstract fun bindVerifyIndividualRegisterMobileViewModel(viewModel: VerifyIndividualRegisterMobileViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(JuridicalRegisterMobileViewModel::class)
    abstract fun bindJuridicalRegisterMobileViewModel(viewModel: JuridicalRegisterMobileViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(RegisterIndividualViewModel::class)
    abstract fun bindIndividualRegisterMobileSubmitViewModel(viewModel: RegisterIndividualViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(LoginUsernamePasswordViewModel::class)
    abstract fun bindLoginUsernamePasswordViewModel(viewModel: LoginUsernamePasswordViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(JuridicalRegisterAsanViewModel::class)
    abstract fun bindJuridicalRegisterAsanViewModel(viewModel: JuridicalRegisterAsanViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(JuridicalRegisterAsanPinViewModel::class)
    abstract fun bindJuridicalRegisterAsanPinViewModel(viewModel: JuridicalRegisterAsanPinViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(JuridialRegisterCompaniesListViewModel::class)
    abstract fun bindJuridialRegisterCompaniesListViewModel(viewModel: JuridialRegisterCompaniesListViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(LoginAsanIndividualViewModel::class)
    abstract fun bindLoginAsanIndividualViewModel(viewModel: LoginAsanIndividualViewModel): ViewModel


    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(LoginIndividualAsanPinViewModel::class)
    abstract fun bindLoginIndividualAsanPinViewModel(viewModel: LoginIndividualAsanPinViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(LoginAsanJuridicalViewModel::class)
    abstract fun bindLoginJuridicalAsanViewModel(viewModel: LoginAsanJuridicalViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(LoginAsanJuridicalPinViewModel::class)
    abstract fun bindLoginAsanJuridicalPinViewModel(viewModel: LoginAsanJuridicalPinViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(JuridialLoginCompaniesListViewModel::class)
    abstract fun bindLoginAsanJuridicalCompaniesListViewModel(viewModel: JuridialLoginCompaniesListViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(IndividualForgotPasswordViewModel::class)
    abstract fun bindIndividualForgotPasswordViewModel(viewModel: IndividualForgotPasswordViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(JuridicalForgotPasswordViewModel::class)
    abstract fun bindJuridicalForgotPasswordViewModel(viewModel: JuridicalForgotPasswordViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(ChangePasswordViewModel::class)
    abstract fun bindChangePasswordViewModel(viewModel: ChangePasswordViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(UnblockUserViewModel::class)
    abstract fun bindUnblockUserViewModel(viewModel: UnblockUserViewModel): ViewModel

    @Binds
    @AuthScope
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel
}