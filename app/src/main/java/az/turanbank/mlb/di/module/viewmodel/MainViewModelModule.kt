package az.turanbank.mlb.di.module.viewmodel

import androidx.lifecycle.ViewModel
import az.turanbank.mlb.SplashViewModel
import az.turanbank.mlb.di.scope.IPScope
import az.turanbank.mlb.di.scope.MainScope
import az.turanbank.mlb.di.scope.ViewModelKey
import az.turanbank.mlb.presentation.activity.ContactsWithBankViewModel
import az.turanbank.mlb.presentation.activity.HomePageSettingsViewModel
import az.turanbank.mlb.presentation.activity.RegisterWithMobileViewModel
import az.turanbank.mlb.presentation.activity.UserAgreementActivityViewModel
import az.turanbank.mlb.presentation.activity.settings.AboutViewModel
import az.turanbank.mlb.presentation.branch.BranchNetworkTabbedViewModel
import az.turanbank.mlb.presentation.branch.fragment.BranchViewModel
import az.turanbank.mlb.presentation.cardoperations.CardOperationsViewModel
import az.turanbank.mlb.presentation.cardoperations.cardhistory.CardHistoryViewModel
import az.turanbank.mlb.presentation.cardoperations.cardtocard.CardToCardViewModel
import az.turanbank.mlb.presentation.cardoperations.cardtocard.SuccessCardToCardViewModel
import az.turanbank.mlb.presentation.cardoperations.cardtocard.VerifyCardToCardViewModel
import az.turanbank.mlb.presentation.cardoperations.cashbycode.CashByCodeViewModel
import az.turanbank.mlb.presentation.cardoperations.cashbycode.SuccessCashByCodeViewModel
import az.turanbank.mlb.presentation.cardoperations.cashbycode.VerifyCashByCodeViewModel
import az.turanbank.mlb.presentation.cardoperations.othercard.CardToOtherCardViewModel
import az.turanbank.mlb.presentation.cardoperations.templates.CardTemplateActivityViewModel
import az.turanbank.mlb.presentation.cardoperations.templates.CardTemplateListViewModel
import az.turanbank.mlb.presentation.home.CampaignViewModel
import az.turanbank.mlb.presentation.home.MlbHomeActivityViewModel
import az.turanbank.mlb.presentation.home.ProfileFragmentViewModel
import az.turanbank.mlb.presentation.ips.outgoing.paymentchoice.SinglePaymentViewModel
import az.turanbank.mlb.presentation.ips.paidToMe.paidtomechoice.SinglePaidToMeViewModel
import az.turanbank.mlb.presentation.login.activities.pin.LoginPinViewModel
import az.turanbank.mlb.presentation.news.NewsViewModel
import az.turanbank.mlb.presentation.orders.CreditOrderViewModel
import az.turanbank.mlb.presentation.payments.PaymentsViewModel
import az.turanbank.mlb.presentation.payments.history.PaymentHistoryViewModel
import az.turanbank.mlb.presentation.payments.pay.categories.PaymentCategoriesViewModel
import az.turanbank.mlb.presentation.payments.pay.check.CheckPaymentViewModel
import az.turanbank.mlb.presentation.payments.pay.check.custom.ChooseInvoiceWithSubChildActivityViewModel
import az.turanbank.mlb.presentation.payments.pay.check.custom.ChooseInvoiceWithChildActivityViewModel
import az.turanbank.mlb.presentation.payments.pay.check.regular.ChooseInvoiceViewModel
import az.turanbank.mlb.presentation.payments.pay.params.PaymentParametersViewModel
import az.turanbank.mlb.presentation.payments.pay.pay.VerifyPaymentViewModel
import az.turanbank.mlb.presentation.payments.pay.result.PaymentResultViewModel
import az.turanbank.mlb.presentation.payments.pay.subcategories.PaymentMerchantsViewModel
import az.turanbank.mlb.presentation.payments.template.PaymentTemplatesViewModel
import az.turanbank.mlb.presentation.profile.LanguageChangeViewModel
import az.turanbank.mlb.presentation.profile.PasswordChangeViewModel
import az.turanbank.mlb.presentation.resources.account.*
import az.turanbank.mlb.presentation.resources.account.abroad.AccountOperationAbroadVerifyViewModel
import az.turanbank.mlb.presentation.resources.account.abroad.AccountOperationOutLandViewModel
import az.turanbank.mlb.presentation.resources.account.abroad.edit.EditAccountOperationAbroadViewModel
import az.turanbank.mlb.presentation.resources.account.changename.AccountChangeNameActivityViewModel
import az.turanbank.mlb.presentation.resources.account.exchange.BuyForeignCurrencyViewModel
import az.turanbank.mlb.presentation.resources.account.exchange.edit.EditConvertViewModel
import az.turanbank.mlb.presentation.resources.account.exchange.success.BuyCurrencySuccessViewModel
import az.turanbank.mlb.presentation.resources.account.exchange.verify.BuyCurrencyVerifyViewModel
import az.turanbank.mlb.presentation.resources.activity.ResourcesViewModel
import az.turanbank.mlb.presentation.resources.card.activity.CardActivityViewModel
import az.turanbank.mlb.presentation.resources.card.list.CardViewModel
import az.turanbank.mlb.presentation.resources.card.requisit.CardRequisitesViewModel
import az.turanbank.mlb.presentation.resources.card.sms.AddSmsNumberViewModel
import az.turanbank.mlb.presentation.resources.card.sms.DeleteOrUpdateSmsNotificationViewModel
import az.turanbank.mlb.presentation.resources.card.sms.SmsViewModel
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementFragmentViewModel
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementInternalViewModel
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementTabbedViewModel
import az.turanbank.mlb.presentation.resources.deposit.activity.DepositViewModel
import az.turanbank.mlb.presentation.resources.deposit.earlypayedamounts.EarlyPayedAmountsViewModel
import az.turanbank.mlb.presentation.resources.deposit.fragment.DepositListViewModel
import az.turanbank.mlb.presentation.resources.deposit.payedamounts.PayedAmountsViewModel
import az.turanbank.mlb.presentation.resources.loan.activity.LoanViewModel
import az.turanbank.mlb.presentation.resources.loan.fragment.LoanListViewModel
import az.turanbank.mlb.presentation.resources.loan.pay.PayLoanViewModel
import az.turanbank.mlb.presentation.resources.loan.pay.choose.ChoosePayTypeViewModel
import az.turanbank.mlb.presentation.resources.loan.pay.choose.account.NoAccountViewModel
import az.turanbank.mlb.presentation.resources.loan.pay.success.SuccessLoanPaymentViewModel
import az.turanbank.mlb.presentation.resources.loan.pay.verify.VerifyLoanPaymentViewModel
import az.turanbank.mlb.presentation.resources.loan.payment.LoanPayedAmountsViewModel
import az.turanbank.mlb.presentation.resources.loan.paymentplan.LoanPaymentPlanViewModel
import az.turanbank.ui.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelModule {

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(MlbHomeActivityViewModel::class)
    abstract fun bindMlbHomeActivityViewModel(viewModel: MlbHomeActivityViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(ChoosePayTypeViewModel::class)
    abstract fun bindChoosePayTypeViewModel(viewModel: ChoosePayTypeViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(SinglePaymentViewModel::class)
    abstract fun bindSinglePaymentViewModel(viewModel: SinglePaymentViewModel): ViewModel


    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(SinglePaidToMeViewModel::class)
    abstract fun bindSinglePaidToMeViewModel(viewModel: SinglePaidToMeViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(ChooseInvoiceWithChildActivityViewModel::class)
    abstract fun bindChooseInvoiceWithChildActivityViewModel(viewModel: ChooseInvoiceWithChildActivityViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountChangeNameActivityViewModel::class)
    abstract fun bindAccountChangeNameActivityViewModel(viewModel: AccountChangeNameActivityViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(ChooseInvoiceWithSubChildActivityViewModel::class)
    abstract fun bindChooseInvoiceChildCustomActivityViewModel(viewModelWithSub: ChooseInvoiceWithSubChildActivityViewModel): ViewModel


    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(UserAgreementActivityViewModel::class)
    abstract fun bindUserAgreementActivityViewModel(viewModel: UserAgreementActivityViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(RegisterWithMobileViewModel::class)
    abstract fun bindRegisterWithMobileViewModel(viewModel: RegisterWithMobileViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountStatementsTabbedViewModel::class)
    abstract fun bindAccountStatementsTabbedViewModel(viewModel: AccountStatementsTabbedViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(ChooseInvoiceViewModel::class)
    abstract fun bindChooseInvoiceViewModel(viewModel: ChooseInvoiceViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardStatementInternalViewModel::class)
    abstract fun bindCardStatementInternalViewModel(viewModel: CardStatementInternalViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardStatementTabbedViewModel::class)
    abstract fun bindCardStatementTabbedViewModel(viewModel: CardStatementTabbedViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(SuccessLoanPaymentViewModel::class)
    abstract fun bindSuccessLoanPaymentViewModel(viewModel: SuccessLoanPaymentViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(HomePageSettingsViewModel::class)
    abstract fun bindHomePageSettingsViewModel(viewModel: HomePageSettingsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AboutViewModel::class)
    abstract fun bindAboutViewModel(viewModel: AboutViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(ResourcesViewModel::class)
    abstract fun bindResourcesViewModel(viewModel: ResourcesViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardOperationsViewModel::class)
    abstract fun bindCardOperationsViewModel(viewModel: CardOperationsViewModel): ViewModel


    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountTransfersActivityViewModel::class)
    abstract fun bindAccountTransfersActivityViewModel(viewModel: AccountTransfersActivityViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(BranchNetworkTabbedViewModel::class)
    abstract fun bindBranchNetworkTabbedViewModel(viewModel: BranchNetworkTabbedViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(ContactsWithBankViewModel::class)
    abstract fun bindContactsWithBankViewModel(viewModel: ContactsWithBankViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeFragmentViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardViewModel::class)
    abstract fun bindCardViewModel(viewModel: CardViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountViewModel::class)
    abstract fun bindAccountViewModel(viewModel: AccountViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(LoanListViewModel::class)
    abstract fun bindLoanListViewModel(listViewModel: LoanListViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(DepositListViewModel::class)
    abstract fun bindDepositListViewModel(viewModel: DepositListViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardActivityViewModel::class)
    abstract fun bindCardActivityViewModel(viewModel: CardActivityViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(SmsViewModel::class)
    abstract fun bindSmsViewModel(viewModel: SmsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AddSmsNumberViewModel::class)
    abstract fun bindAddSmsNumberViewModel(viewModel: AddSmsNumberViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardRequisitesViewModel::class)
    abstract fun bindCardRequisitesViewModel(viewModel: CardRequisitesViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountActivityViewModel::class)
    abstract fun bindAccountActivityViewModel(viewModel: AccountActivityViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountStatementsViewModel::class)
    abstract fun bindAccountStatementsActivityViewModel(viewModel: AccountStatementsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountRequisitesActivityViewModel::class)
    abstract fun bindAccountRequisitesActivityViewModel(viewModel: AccountRequisitesActivityViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardStatementFragmentViewModel::class)
    abstract fun bindAccountCardStatementActivityViewModel(viewModel: CardStatementFragmentViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardToCardViewModel::class)
    abstract fun bindMyCardTransferActivityViewModel(viewModel: CardToCardViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(VerifyCardToCardViewModel::class)
    abstract fun bindVerifyCardToCardViewModel(viewModel: VerifyCardToCardViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountOperationInternalViewModel::class)
    abstract fun bindAccountOperationInternalViewModel(viewModel: AccountOperationInternalViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountOperationInternalVerifyViewModel::class)
    abstract fun bindAccountOperationInternalVerifyViewModel(viewModel: AccountOperationInternalVerifyViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(DeleteOrUpdateSmsNotificationViewModel::class)
    abstract fun bindDeleteOrUpdateSmsNotificationViewModel(viewModel: DeleteOrUpdateSmsNotificationViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountOperationInternalResultViewModel::class)
    abstract fun bindAccountOperationInternalResultViewModel(viewModel: AccountOperationInternalResultViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(TransferHistoryViewModel::class)
    abstract fun bindTransferHistoryViewModel(viewModel: TransferHistoryViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CashByCodeViewModel::class)
    abstract fun bindCashByCodeViewModel(viewModel: CashByCodeViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(VerifyCashByCodeViewModel::class)
    abstract fun bindVerifyCashByCodeViewModel(viewModel: VerifyCashByCodeViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountOperationInLandViewModel::class)
    abstract fun bindAccountOperationInLandViewModel(viewModel: AccountOperationInLandViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardToOtherCardViewModel::class)
    abstract fun bindCardToOtherCardViewModel(viewModel: CardToOtherCardViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardTemplateListViewModel::class)
    abstract fun bindCardTemplateListViewModel(viewModel: CardTemplateListViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountOperationInLandVerifyViewModel::class)
    abstract fun bindAccountOperationInLandVerifyViewModel(viewModel: AccountOperationInLandVerifyViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(SuccessCashByCodeViewModel::class)
    abstract fun bindSuccessCashByCodeViewModel(viewModel: SuccessCashByCodeViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(SuccessCardToCardViewModel::class)
    abstract fun bindSuccessCardToCardViewModel(viewModel: SuccessCardToCardViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountTransfersViewModel::class)
    abstract fun bindAccountTransfersViewModel(viewModel: AccountTransfersViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardHistoryViewModel::class)
    abstract fun bindCardHistoryViewModel(viewModel: CardHistoryViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CardTemplateActivityViewModel::class)
    abstract fun bindCardTemplateActivityViewModel(viewModel: CardTemplateActivityViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AuthMultiDocsViewModel::class)
    abstract fun bindAuthMultiDocsViewModel(viewModel: AuthMultiDocsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(LoanViewModel::class)
    abstract fun bindLoanViewModel(viewModel: LoanViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(LoanPaymentPlanViewModel::class)
    abstract fun bindLoanPaymentPlanViewModel(viewModel: LoanPaymentPlanViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(LoanPayedAmountsViewModel::class)
    abstract fun bindLoanPaymentsViewModel(viewModel: LoanPayedAmountsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(DepositViewModel::class)
    abstract fun bindDepositViewModel(viewModel: DepositViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(SignFileViewModel::class)
    abstract fun bindSignFileViewModel(viewModel: SignFileViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PayedAmountsViewModel::class)
    abstract fun bindPayedAmountsViewModel(viewModel: PayedAmountsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(EarlyPayedAmountsViewModel::class)
    abstract fun bindEarlyPayedAmountsViewModel(viewModel: EarlyPayedAmountsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(FirstAuthOperationViewModel::class)
    abstract fun bindFirstAuthOperationViewModel(viewModel: FirstAuthOperationViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PaymentsViewModel::class)
    abstract fun bindPaymentsViewModel(viewModel: PaymentsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(ProfileFragmentViewModel::class)
    abstract fun bindProfileFragmentViewModel(viewModel: ProfileFragmentViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(SignFileFirstAuthViewModel::class)
    abstract fun bindSignFileFirstAuthViewModel(viewModel: SignFileFirstAuthViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(OperationDetailsViewModel::class)
    abstract fun bindoperationDetailsViewModel(viewModel: OperationDetailsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(OperationTemplateViewModel::class)
    abstract fun bindOperationTemplateViewModel(viewModel: OperationTemplateViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(EditOperationInLandViewModel::class)
    abstract fun bindEditOperationInLandViewModel(viewModel: EditOperationInLandViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(EditOperationInternalViewModel::class)
    abstract fun bindEditOperationInternalViewModel(viewModel: EditOperationInternalViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CampaignViewModel::class)
    abstract fun bindCampaignViewModel(viewModel: CampaignViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountOperationOutLandViewModel::class)
    abstract fun bindAccountOperationOutLandViewModel(viewModel: AccountOperationOutLandViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(AccountOperationAbroadVerifyViewModel::class)
    abstract fun bindAccountOperationAbroadVerifyViewModel(viewModel: AccountOperationAbroadVerifyViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CreditOrderViewModel::class)
    abstract fun bindCreditOrderViewModel(viewModel: CreditOrderViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(BuyForeignCurrencyViewModel::class)
    abstract fun bindBuyForeignCurrencyViewModel(viewModel: BuyForeignCurrencyViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(BuyCurrencyVerifyViewModel::class)
    abstract fun bindBuyCurrencyVerifyViewModel(viewModel: BuyCurrencyVerifyViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(BuyCurrencySuccessViewModel::class)
    abstract fun bindBuyCurrencySuccessViewModel(viewModel: BuyCurrencySuccessViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(BranchViewModel::class)
    abstract fun bindBranchViewModel(viewModel: BranchViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(NewsViewModel::class)
    abstract fun bindNewsViewModel(viewModel: NewsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(LoginPinViewModel::class)
    abstract fun bindLoginPinViewModel(viewModel: LoginPinViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PasswordChangeViewModel::class)
    abstract fun bindPasswordChangeViewModel(viewModel: PasswordChangeViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PayLoanViewModel::class)
    abstract fun bindPayLoanViewModel(viewModel: PayLoanViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(VerifyLoanPaymentViewModel::class)
    abstract fun bindVerifyLoanPaymentViewModel(viewModel: VerifyLoanPaymentViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(EditAccountOperationAbroadViewModel::class)
    abstract fun bindEditAccountOperationAbroadViewModel(viewModel: EditAccountOperationAbroadViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(EditConvertViewModel::class)
    abstract fun bindEditConvertViewModel(viewModel: EditConvertViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(NoAccountViewModel::class)
    abstract fun bindNoAccountViewModel(viewModel: NoAccountViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(LanguageChangeViewModel::class)
    abstract fun bindLanguageChangeViewModel(viewModel: LanguageChangeViewModel): ViewModel


    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PaymentCategoriesViewModel::class)
    abstract fun bindPaymentCategoriesViewModel(viewModel: PaymentCategoriesViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PaymentMerchantsViewModel::class)
    abstract fun bindPaymentMerchantsViewModel(viewModel: PaymentMerchantsViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PaymentParametersViewModel::class)
    abstract fun bindPaymentParametersViewModel(viewModel: PaymentParametersViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(CheckPaymentViewModel::class)
    abstract fun bindCheckPaymentViewModel(viewModel: CheckPaymentViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(VerifyPaymentViewModel::class)
    abstract fun bindVerifyPaymentViewModel(viewModel: VerifyPaymentViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PaymentHistoryViewModel ::class)
    abstract fun bindPaymentHistoryViewModel(viewModel: PaymentHistoryViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PaymentResultViewModel ::class)
    abstract fun bindPaymentResultViewModel(viewModel: PaymentResultViewModel): ViewModel

    @Binds
    @MainScope
    @IntoMap
    @ViewModelKey(PaymentTemplatesViewModel ::class)
    abstract fun bindPaymentTemplatesViewModel(viewModel: PaymentTemplatesViewModel): ViewModel
}