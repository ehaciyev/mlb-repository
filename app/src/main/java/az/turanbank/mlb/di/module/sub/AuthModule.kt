package az.turanbank.mlb.di.module.sub

import az.turanbank.mlb.data.remote.Constants.Companion.BASE_URL
import az.turanbank.mlb.data.remote.model.service.AuthApiService
import az.turanbank.mlb.data.remote.model.service.AuthApiServiceProvider
import az.turanbank.mlb.di.scope.AuthScope
import az.turanbank.mlb.domain.user.data.AuthRepository
import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

const val MLB_REGISTER = "mlb_register"

@Module
class AuthModule {

    @Provides
    @AuthScope
    fun providesAuthApiServiceProvider(@Named(MLB_REGISTER) retrofit: Retrofit): AuthApiServiceProvider =
        object : AuthApiServiceProvider {
            val authApiService = retrofit.create(AuthApiService::class.java)
            override fun getInstance() = authApiService
        }

    @Named(MLB_REGISTER)
    @Provides
    @AuthScope
    fun providesRetrofitInstance(
        callAdapterFactory: RxJava2CallAdapterFactory,
        converterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl("${BASE_URL}register/")
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(converterFactory)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @AuthScope
    fun providesAuthRepository(serviceProvider: AuthApiServiceProvider): AuthRepositoryType {
        return AuthRepository(serviceProvider = serviceProvider)
    }
}