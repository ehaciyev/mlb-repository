package az.turanbank.mlb.di.scope

import javax.inject.Scope

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AuthScope
