package az.turanbank.mlb.di.module.viewmodel

import androidx.lifecycle.ViewModel
import az.turanbank.mlb.di.scope.ViewModelKey
import az.turanbank.mlb.presentation.exchange.converter.fragment.cashConverter.CashConvertViewModel
import az.turanbank.mlb.presentation.exchange.converter.fragment.cashlessConverter.CashlessConvertViewModel
import az.turanbank.mlb.presentation.exchange.fragment.ExchangeViewModel
import az.turanbank.mlb.presentation.exchange.fragment.cash.ExchangeCashViewModel
import az.turanbank.mlb.presentation.exchange.fragment.cashless.ExchangeCashlessViewModel
import az.turanbank.mlb.presentation.exchange.fragment.centralbank.ExchangeCentralBankViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ExchangeViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ExchangeCashViewModel::class)
    abstract fun bindExchangeCashViewModel(viewModel: ExchangeCashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExchangeCashlessViewModel::class)
    abstract fun bindExchangeCashlessViewModel(viewModel: ExchangeCashlessViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExchangeCentralBankViewModel::class)
    abstract fun bindExchangeCentralBankViewModel(viewModel: ExchangeCentralBankViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExchangeViewModel::class)
    abstract fun bindExchangeViewModel(viewModel: ExchangeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CashConvertViewModel::class)
    abstract fun bindCashConvertViewModel(viewModel: CashConvertViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CashlessConvertViewModel::class)
    abstract fun bindCashlessConvertViewModel(viewModel: CashlessConvertViewModel): ViewModel
}