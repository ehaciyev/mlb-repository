package az.turanbank.mlb.di.module.sub

import az.turanbank.mlb.data.remote.Constants.Companion.BASE_URL
import az.turanbank.mlb.data.remote.model.service.ExchangeApiService
import az.turanbank.mlb.data.remote.model.service.ExchangeApiServiceProvider
import az.turanbank.mlb.domain.user.data.ExchangeRepository
import az.turanbank.mlb.domain.user.data.ExchangeRepositoryType
import az.turanbank.mlb.presentation.exchange.converter.ConverterUtil
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

const val MLB_EXCHANGE = "mlb_exchange"

@Module
class ExchangeModule {

    @Provides
    @Singleton
    fun providesExchangeApiServiceProvider(@Named(MLB_EXCHANGE) retrofit: Retrofit): ExchangeApiServiceProvider =
        object : ExchangeApiServiceProvider {
            override fun getInstance() = retrofit.create(ExchangeApiService::class.java)
        }

    @Named(MLB_EXCHANGE)
    @Provides
    @Singleton
    fun providesExchangeRetrofitInstance(
        callAdapterFactory: RxJava2CallAdapterFactory,
        converterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl("${BASE_URL}exchange/")
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(converterFactory)
            .client(okHttpClient)
            .build()
    }

    @Named(MLB_EXCHANGE)
    @Provides
    @Singleton
    fun providesExchangeUtil(): ConverterUtil {
        return ConverterUtil()
    }

    @Provides
    @Singleton
    fun providesExchangeRepository(serviceProvider: ExchangeApiServiceProvider): ExchangeRepositoryType {
        return ExchangeRepository(serviceProvider = serviceProvider)
    }
}