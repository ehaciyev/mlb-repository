package az.turanbank.mlb.di

import android.app.Application
import az.turanbank.mlb.presentation.base.BaseApplication
import az.turanbank.mlb.di.module.*
import az.turanbank.mlb.di.module.sub.ExchangeModule
import az.turanbank.mlb.domain.MlbSessionManager
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuildersModule::class,
        AppModule::class,
        ViewModelFactoryModule::class,
        NetworkModule::class,
        ExchangeModule::class
    ]
)
interface AppComponent : AndroidInjector<BaseApplication> {

    fun sessionManager() : MlbSessionManager

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}