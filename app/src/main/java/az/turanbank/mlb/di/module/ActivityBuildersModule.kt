package az.turanbank.mlb.di.module

import az.turanbank.mlb.SplashActivity
import az.turanbank.mlb.di.module.sub.AuthModule
import az.turanbank.mlb.di.module.sub.IpsModule
import az.turanbank.mlb.di.module.sub.MainModule
import az.turanbank.mlb.di.module.viewmodel.AuthViewModelModule
import az.turanbank.mlb.di.module.viewmodel.ExchangeViewModelModule
import az.turanbank.mlb.di.module.viewmodel.IpsViewModelModule
import az.turanbank.mlb.di.module.viewmodel.MainViewModelModule
import az.turanbank.mlb.di.scope.AuthScope
import az.turanbank.mlb.di.scope.IPScope
import az.turanbank.mlb.di.scope.MainScope
import az.turanbank.mlb.presentation.activity.*
import az.turanbank.mlb.presentation.activity.settings.AboutAppActivity
import az.turanbank.mlb.presentation.branch.BranchNetworkTabbedActivity
import az.turanbank.mlb.presentation.branch.fragment.list.BranchListFragment
import az.turanbank.mlb.presentation.branch.fragment.map.BranchMapFragment
import az.turanbank.mlb.presentation.cardoperations.CardOperationsActivity
import az.turanbank.mlb.presentation.cardoperations.NewOperationsFragment
import az.turanbank.mlb.presentation.cardoperations.cardhistory.CardHistoryFragment
import az.turanbank.mlb.presentation.cardoperations.cardtocard.CardToCardActivity
import az.turanbank.mlb.presentation.cardoperations.cardtocard.SuccessCardToCardActivity
import az.turanbank.mlb.presentation.cardoperations.cardtocard.VerifyCardToCardActivity
import az.turanbank.mlb.presentation.cardoperations.cashbycode.CashByCodeActivity
import az.turanbank.mlb.presentation.cardoperations.cashbycode.SuccessCashByCodeActivity
import az.turanbank.mlb.presentation.cardoperations.cashbycode.VerifyCashByCodeActivity
import az.turanbank.mlb.presentation.cardoperations.othercard.CardToOtherCardActivity
import az.turanbank.mlb.presentation.cardoperations.templates.CardTemplateActivity
import az.turanbank.mlb.presentation.cardoperations.templates.CardTemplateListFragment
import az.turanbank.mlb.presentation.exchange.ExchangeActivity
import az.turanbank.mlb.presentation.exchange.converter.ExchangeConverterActivity
import az.turanbank.mlb.presentation.exchange.converter.fragment.cashConverter.CashConvertFragment
import az.turanbank.mlb.presentation.exchange.converter.fragment.cashlessConverter.CashlessConvertFragment
import az.turanbank.mlb.presentation.exchange.fragment.cash.ExchangeCashFragment
import az.turanbank.mlb.presentation.exchange.fragment.cashless.ExchangeCashlessFragment
import az.turanbank.mlb.presentation.exchange.fragment.centralbank.ExchangeCentralBankFragment
import az.turanbank.mlb.presentation.forgot.change.ChangePasswordActivity
import az.turanbank.mlb.presentation.forgot.individual.IndividualForgotPasswordActivity
import az.turanbank.mlb.presentation.forgot.juridical.JuridicalForgotPasswordActivity
import az.turanbank.mlb.presentation.forgot.verify.ForgotPasswordVerifyOtpCodeActivity
import az.turanbank.mlb.presentation.home.CampaignActivity
import az.turanbank.mlb.presentation.home.MlbHomeActivity
import az.turanbank.mlb.presentation.home.ProfileFragment
import az.turanbank.mlb.presentation.ips.InstantPaymentActivity
import az.turanbank.mlb.presentation.ips.VerifyOtpCodeForAliasActivity
import az.turanbank.mlb.presentation.ips.accounts.IPSAccountsActivity
import az.turanbank.mlb.presentation.ips.accounts.authorization.IPSAuthorizationActivity
import az.turanbank.mlb.presentation.ips.accounts.delete.DeleteIpsAccountActivity
import az.turanbank.mlb.presentation.ips.accounts.myaccounts.MyLinkedAccountsActivity
import az.turanbank.mlb.presentation.ips.accounts.register.RegisterAccountIpsSystemActivity
import az.turanbank.mlb.presentation.ips.accounts.setdefault.alias.ChooseAliasForDefaultActivity
import az.turanbank.mlb.presentation.ips.accounts.setdefault.account.ChooseAccountForDefaultActivity
import az.turanbank.mlb.presentation.ips.aliases.IPSAliasesActivity
import az.turanbank.mlb.presentation.ips.aliases.create.CreateNewAliasActivity
import az.turanbank.mlb.presentation.ips.aliases.delete.DeleteIPSAliasActivity
import az.turanbank.mlb.presentation.ips.aliases.link.account.SelectAccountLinkingToAliasActivity
import az.turanbank.mlb.presentation.ips.aliases.link.alias.SelectAliasLinkingToAccountActivity
import az.turanbank.mlb.presentation.ips.aliases.link.choosedefault.ChooseDefaultAlias
import az.turanbank.mlb.presentation.ips.aliases.myaliases.MyAliasesActivity
import az.turanbank.mlb.presentation.ips.outgoing.MyIncomePaymentsActivity
import az.turanbank.mlb.presentation.ips.outgoing.paymentchoice.SinglePaymentActivity
import az.turanbank.mlb.presentation.ips.paidToMe.PaidToMeActivity
import az.turanbank.mlb.presentation.ips.paidToMe.paidtomechoice.SinglePaidToMeActivity
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.IPSCreditorAccountSelectActivity
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.account.IPSCreditorAccountChoiceFragment
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.alias.IPSCreditorAliasChoiceFragment
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.moreinfo.AccountAndCustomerInfoActivity
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.account.IPSAccountChoiceFragment
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.alias.IPSAliasChoiceFragment
import az.turanbank.mlb.presentation.ips.pay.preconfirm.TransferWithIPSActivity
import az.turanbank.mlb.presentation.ips.pay.preconfirm.confirm.ConfirmWithIPSActivity
import az.turanbank.mlb.presentation.ips.payrequest.requestor.IPSPayRequestTabbedActivity
import az.turanbank.mlb.presentation.ips.payrequest.requestor.account.ChooseIPSRequestedAccountFragment
import az.turanbank.mlb.presentation.ips.payrequest.requestor.alias.ChooseIPSRequestedAliasFragment
import az.turanbank.mlb.presentation.ips.payrequest.sender.IPSPaySenderActivity
import az.turanbank.mlb.presentation.ips.payrequest.sender.account.ChooseIPSSenderAccountFragment
import az.turanbank.mlb.presentation.ips.payrequest.sender.alias.ChooseIPSSenderAliasFragment
import az.turanbank.mlb.presentation.ips.register.InstantPaymentRegisterRulesActivity
import az.turanbank.mlb.presentation.ips.register.InstantPaymentSplashActivity
import az.turanbank.mlb.presentation.ips.nonregistered.NonRegisteredPayment
import az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.NonRegisteredIPSTransferActivity
import az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.account.NonRegisteredIPSAccountChoiceFragment
import az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.alias.NonRegisteredIPSAliasChoiceFragment
import az.turanbank.mlb.presentation.ips.nonregistered.pay.debitor.NonRegisteredIPSAccountChoiceActivity
import az.turanbank.mlb.presentation.ips.pay.IPSPayActivity
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.IPSTransferActivity
import az.turanbank.mlb.presentation.ips.register.account.InstantPaymentAccountMultiSelectActivity
import az.turanbank.mlb.presentation.ips.accounts.authchoice.InstantPaymentAccountAuthSelectActivity
import az.turanbank.mlb.presentation.login.activities.LoginActivity
import az.turanbank.mlb.presentation.login.activities.pin.LoginPinActivity
import az.turanbank.mlb.presentation.login.asan.individual.LoginAsanIndividualActivity
import az.turanbank.mlb.presentation.login.asan.individual.LoginIndividualAsanPinActivity
import az.turanbank.mlb.presentation.login.asan.juridical.LoginAsanJuridicalActivity
import az.turanbank.mlb.presentation.login.asan.juridical.LoginAsanJuridicalCompaniesListActivity
import az.turanbank.mlb.presentation.login.asan.juridical.LoginAsanJuridicalPinActivity
import az.turanbank.mlb.presentation.login.unblock.UnblockUserActivity
import az.turanbank.mlb.presentation.login.username.LoginUsernamePasswordActivity
import az.turanbank.mlb.presentation.news.NewsActivity
import az.turanbank.mlb.presentation.news.internal.InternalNewsActivity
import az.turanbank.mlb.presentation.orders.CreditOrderFragment
import az.turanbank.mlb.presentation.orders.OrdersActivity
import az.turanbank.mlb.presentation.payments.PaymentsFragment
import az.turanbank.mlb.presentation.payments.history.PaymentHistoryFragment
import az.turanbank.mlb.presentation.payments.pay.categories.PaymentCategoriesFragment
import az.turanbank.mlb.presentation.payments.pay.check.CheckPaymentActivity
import az.turanbank.mlb.presentation.payments.pay.check.custom.ChooseInvoiceWithSubChildActivity
import az.turanbank.mlb.presentation.payments.pay.check.regular.ChooseInvoiceActivity
import az.turanbank.mlb.presentation.payments.pay.check.custom.ChooseInvoiceWithChildActivity
import az.turanbank.mlb.presentation.payments.pay.params.PaymentParametersActivity
import az.turanbank.mlb.presentation.payments.pay.pay.VerifyPaymentActivity
import az.turanbank.mlb.presentation.payments.pay.result.PaymentResultActivity
import az.turanbank.mlb.presentation.payments.pay.subcategories.PaymentMerchantsActivity
import az.turanbank.mlb.presentation.payments.template.PaymentTemplatesFragment
import az.turanbank.mlb.presentation.profile.LanguageChangeActivity
import az.turanbank.mlb.presentation.profile.PasswordChangeActivity
import az.turanbank.mlb.presentation.register.RegisterActivity
import az.turanbank.mlb.presentation.register.asan.individual.IndividualRegisterAsanActivity
import az.turanbank.mlb.presentation.register.asan.individual.IndividualRegisterAsanPinActivity
import az.turanbank.mlb.presentation.register.asan.juridical.JuridicalRegisterAsanActivity
import az.turanbank.mlb.presentation.register.asan.juridical.JuridicalRegisterAsanPinActivity
import az.turanbank.mlb.presentation.register.asan.juridical.JuridicalRegisterCompaniesListActivity
import az.turanbank.mlb.presentation.register.mobile.individual.IndividualRegisterMobileActivity
import az.turanbank.mlb.presentation.register.mobile.individual.IndividualRegisterMobileSubmitActivity
import az.turanbank.mlb.presentation.register.mobile.individual.VerifyIndividualRegisterMobileActivity
import az.turanbank.mlb.presentation.register.mobile.juridical.JuridicalRegisterMobileActivity
import az.turanbank.mlb.presentation.resources.account.*
import az.turanbank.mlb.presentation.resources.account.abroad.AccountOperationAbroadActivity
import az.turanbank.mlb.presentation.resources.account.abroad.AccountOperationAbroadVerifyActivity
import az.turanbank.mlb.presentation.resources.account.abroad.edit.EditOperationAbroadActivity
import az.turanbank.mlb.presentation.resources.account.changename.AccountChangeNameActivity
import az.turanbank.mlb.presentation.resources.account.exchange.ConvertMoneyActivity
import az.turanbank.mlb.presentation.resources.account.exchange.buy.BuyForeignCurrencyFragment
import az.turanbank.mlb.presentation.resources.account.exchange.edit.EditConvertMoneyActivity
import az.turanbank.mlb.presentation.resources.account.exchange.sell.SellForeignCurrencyFragment
import az.turanbank.mlb.presentation.resources.account.exchange.success.BuyCurrencySuccessActivity
import az.turanbank.mlb.presentation.resources.account.exchange.verify.BuyCurrencyVerifyActivity
import az.turanbank.mlb.presentation.resources.activity.ResourcesActivity
import az.turanbank.mlb.presentation.resources.card.activity.CardActivity
import az.turanbank.mlb.presentation.resources.card.list.CardsFragment
import az.turanbank.mlb.presentation.resources.card.requisit.CardRequisitesActivity
import az.turanbank.mlb.presentation.resources.card.sms.AddSmsNumberActivity
import az.turanbank.mlb.presentation.resources.card.sms.DeleteOrUpdateSmsNotificationActivity
import az.turanbank.mlb.presentation.resources.card.sms.SmsActivity
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementFragment
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementInternalActivity
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementTabbedActivity
import az.turanbank.mlb.presentation.resources.deposit.activity.DepositActivity
import az.turanbank.mlb.presentation.resources.deposit.earlypayedamounts.EarlyPayedAmountsActivity
import az.turanbank.mlb.presentation.resources.deposit.fragment.DepositFragment
import az.turanbank.mlb.presentation.resources.deposit.payedamounts.PayedAmountsActivity
import az.turanbank.mlb.presentation.resources.loan.activity.LoanActivity
import az.turanbank.mlb.presentation.resources.loan.fragment.LoansFragment
import az.turanbank.mlb.presentation.resources.loan.pay.PayLoanActivity
import az.turanbank.mlb.presentation.resources.loan.pay.choose.ChoosePayTypeActivity
import az.turanbank.mlb.presentation.resources.loan.pay.choose.account.NoAccountsFragment
import az.turanbank.mlb.presentation.resources.loan.pay.choose.card.UnblockCardsFragment
import az.turanbank.mlb.presentation.resources.loan.pay.success.SuccessLoanPaymentActivity
import az.turanbank.mlb.presentation.resources.loan.pay.verify.VerifyLoanPaymentActivity
import az.turanbank.mlb.presentation.resources.loan.payment.LoanPayedAmountsActivity
import az.turanbank.mlb.presentation.resources.loan.paymentplan.LoanPaymentPlanActivity
import az.turanbank.ui.home.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun registerWithAsanActivity(): IndividualRegisterAsanActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun individualRegisterMobileActivity(): IndividualRegisterMobileActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun individualRegisterAsanPinActivity(): IndividualRegisterAsanPinActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun verifyIndividualRegisterMobileActivity(): VerifyIndividualRegisterMobileActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun individualRegisterMobileActivitySubmit(): IndividualRegisterMobileSubmitActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun juridicalRegisterMobileActivitySubmit(): JuridicalRegisterMobileActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun loginUsernamePassword(): LoginUsernamePasswordActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun juridicalRegisterAsanActivity(): JuridicalRegisterAsanActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun juridicalRegisterAsanPinActivity(): JuridicalRegisterAsanPinActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun juridicalRegisterCompaniesListActivity(): JuridicalRegisterCompaniesListActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun loginAsanIndividualActivity(): LoginAsanIndividualActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun loginIndividualAsanPinActivity(): LoginIndividualAsanPinActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun loginAsanJuridicalActivity(): LoginAsanJuridicalActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun loginJuridicalPinActivity(): LoginAsanJuridicalPinActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun loginAsanJuridicalCompaniesListActivity(): LoginAsanJuridicalCompaniesListActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun individualForgotPasswordActivity(): IndividualForgotPasswordActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun juridicalForgotPasswordActivity(): JuridicalForgotPasswordActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun forgotPasswordVerifyOtpCodeActivity(): ForgotPasswordVerifyOtpCodeActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun changePasswordActivity(): ChangePasswordActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun unblockUserActivity(): UnblockUserActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountActivity(): AccountActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountStatementsTabbedActivity(): AccountStatementsTabbedActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountRequisitesActivity(): AccountRequisitesActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun userAgreementActivity(): UserAgreementActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun choosePayTypeActivity(): ChoosePayTypeActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun registerWithMobileActivity(): RegisterWithMobileActivity

    /*@MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun runtimeLocaleChanger(): RuntimeLocaleChanger*/

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun convertMoneyActivity(): ConvertMoneyActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun instantPaymentComingSoon(): InstantPaymentComingSoon

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountChangeNameActivity(): AccountChangeNameActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun singlePaymentActivity(): SinglePaymentActivity

    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun myAliasesActivity(): MyAliasesActivity

    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun myLinkedAccountsActivity(): MyLinkedAccountsActivity

    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun chooseDefaultAlias(): ChooseDefaultAlias

    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun nonRegisteredPayment(): NonRegisteredPayment

    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun nonRegisteredIPSAccountChoiceActivity(): NonRegisteredIPSAccountChoiceActivity

    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun nonRegisteredIPSTransferActivity(): NonRegisteredIPSTransferActivity

    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsPayActivity(): IPSPayActivity


    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun nonRegisteredIPSAliasChoiceFragment(): NonRegisteredIPSAliasChoiceFragment

    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun instantPaymentAccountAuthSelectActivity(): InstantPaymentAccountAuthSelectActivity


    @IPScope
    @ContributesAndroidInjector(modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun nonRegisteredIPSAccountChoiceFragment(): NonRegisteredIPSAccountChoiceFragment


    @ContributesAndroidInjector(modules = [ExchangeViewModelModule::class])
    abstract fun exchangeActivity(): ExchangeActivity

    @ContributesAndroidInjector(modules = [ExchangeViewModelModule::class])
    abstract fun exchangeCashFragment(): ExchangeCashFragment

    @ContributesAndroidInjector(modules = [ExchangeViewModelModule::class])
    abstract fun exchangeCashlessFragment(): ExchangeCashlessFragment

    @ContributesAndroidInjector(modules = [ExchangeViewModelModule::class])
    abstract fun exchangeCentralBankFragment(): ExchangeCentralBankFragment

    @ContributesAndroidInjector(modules = [ExchangeViewModelModule::class])
    abstract fun cashConverFragment(): CashConvertFragment

    @ContributesAndroidInjector(modules = [ExchangeViewModelModule::class])
    abstract fun cashlessConvertFragment(): CashlessConvertFragment

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun splashApplication(): SplashActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountStatementsFragment(): AccountStatementsFragment

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun transferHistoryFragment(): TransferHistoryFragment

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun transferOperationsFragment(): TransferOperationsFragment

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun transferTemplateFragment(): AccountTemplateFragment

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountTransfersFragment(): AccountTransfersFragment

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun homeFragment(): HomeFragment

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun campaignActivity(): CampaignActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun errorPageViewActivity(): ErrorPageViewActivity

    @AuthScope
    @ContributesAndroidInjector(modules = [AuthModule::class, AuthViewModelModule::class])
    abstract fun loginActivity(): LoginActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class, MainViewModelModule::class])
    abstract fun loginPinActivity(): LoginPinActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun mlbHomeActivity(): MlbHomeActivity


    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardsFragment(): CardsFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountssFragment(): AccountsFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun loansFragment(): LoansFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun depositsFragment(): DepositFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardActivity(): CardActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun chooseInvoiceActivity(): ChooseInvoiceActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun chooseInvoiceChildCustomActivity(): ChooseInvoiceWithSubChildActivity


    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun chooseInvoiceCustomActivity(): ChooseInvoiceWithChildActivity


    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardStatementInternalActivity(): CardStatementInternalActivity


    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun smsActivity(): SmsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun addNumberSmsActivity(): AddSmsNumberActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardRequisitesActivity(): CardRequisitesActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardStatementActivity(): CardStatementFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountTransfersActivity(): AccountTransfersActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun myCardTransferActivity(): CardToCardActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun verifyCardToCardActivity(): VerifyCardToCardActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountOperationInternalActivity(): AccountOperationInternalActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountOperationInternalVerifyActivity(): AccountOperationInternalVerifyActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun deleteOrUpdateSmsNotificationActivity(): DeleteOrUpdateSmsNotificationActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountOperationInternalResultActivity(): AccountOperationInternalResultActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cashByCodeActivity(): CashByCodeActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun verifyCashByCodeActivity(): VerifyCashByCodeActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountOperationInLandActivity(): AccountOperationInLandActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardToOtherCardActivity(): CardToOtherCardActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardTemplateListFragment(): CardTemplateListFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun authorizationsFragment(): AuthorizationsFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountOperationInLandVerifyActivity(): AccountOperationInLandVerifyActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun successLoanPaymentActivity(): SuccessLoanPaymentActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun successCashByCodeActivity(): SuccessCashByCodeActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun successCardToCardActivity(): SuccessCardToCardActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardHistoryFragment(): CardHistoryFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardTemplateActivity(): CardTemplateActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun newOperationsFragment(): NewOperationsFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun authorizationMultiDocsActivity(): AuthorizationMultiDocsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun loanActivity(): LoanActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun loanPaymentPlanActivity(): LoanPaymentPlanActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun loanPaymentsActivity(): LoanPayedAmountsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun depositActivity(): DepositActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun signFileActivity(): SignFileActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun payedAmountsActivity(): PayedAmountsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun earlyPayedAmountsActivity(): EarlyPayedAmountsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun firstAuthOperationListActivity(): FirstAuthOperationListActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun signFileFirstAuthActivity(): SignFileFirstAuthActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun operationDetailsActivity(): OperationDetailsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun singleOperationDetailsActivity(): SingleOperationDetailsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun paymentsFragment(): PaymentsFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun profileFragment(): ProfileFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun editOperationInLandActivity(): EditOperationInLandActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountOperationOutLandActivity(): AccountOperationAbroadActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun accountOperationAbroadVerifyActivity(): AccountOperationAbroadVerifyActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun editOperationInternalActivity(): EditOperationInternalActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun creditOrderFragment(): CreditOrderFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun ordersActivity(): OrdersActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun buyForeignCurrencyFragment(): BuyForeignCurrencyFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun sellForeignCurrencyFragment(): SellForeignCurrencyFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun buyCurrencyVerifyActivity(): BuyCurrencyVerifyActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun buyCurrencySuccessActivity(): BuyCurrencySuccessActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun branchListFragment(): BranchListFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun branchMapFragment(): BranchMapFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun newsActivity(): NewsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun internalNewsActivity(): InternalNewsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun passwordChangeActivity(): PasswordChangeActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun payLoanActivity(): PayLoanActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun verifyLoanPaymentActivity(): VerifyLoanPaymentActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun editOperationAbroadActivity(): EditOperationAbroadActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun editConvertMoneyActivity(): EditConvertMoneyActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun noAccountsFragment(): NoAccountsFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun unblockCardsFragment(): UnblockCardsFragment



    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun languageChangeActivity(): LanguageChangeActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun homePageSettingsActivity(): HomePageSettingsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun aboutAppActivity(): AboutAppActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun resourcesActivity(): ResourcesActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardOperationsActivity(): CardOperationsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun registerActivity(): RegisterActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun exchangeConverterActivity(): ExchangeConverterActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun branchNetworkTabbedActivity(): BranchNetworkTabbedActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun contactsWithBankActivity(): ContactsWithBankActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun cardStatementTabbedActivity(): CardStatementTabbedActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun instantPaymentSplashActivity(): InstantPaymentSplashActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun instantPaymentAccountMultiSelectActivity(): InstantPaymentAccountMultiSelectActivity


    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun myIncomePaymentsActivity(): MyIncomePaymentsActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun confirmWithIPSActivityinstantPaymentSplashActivity(): ConfirmWithIPSActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun paidToMeActivity(): PaidToMeActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun instantPaymentActivity(): InstantPaymentActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun iPSTransferActivity(): IPSTransferActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsAuthorizationActivity(): IPSAuthorizationActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsCreditorAccountSelectActivity(): IPSCreditorAccountSelectActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun accountAndCustomerInfoActivity(): AccountAndCustomerInfoActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsPayRequestTabbedActivity(): IPSPayRequestTabbedActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun singlePaidToMeActivity(): SinglePaidToMeActivity


    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsPaySenderActivity(): IPSPaySenderActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun successfulPageViewActivity(): SuccessfulPageViewActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun warningPageViewActivity(): WarningPageViewActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun instantPaymentRegisterRulesActivity(): InstantPaymentRegisterRulesActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsAliasesActivity(): IPSAliasesActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsAccountsActivity(): IPSAccountsActivity


    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsAccountChoice(): IPSAccountChoiceFragment

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsAliasChoice(): IPSAliasChoiceFragment

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsCreditorAccountChoice(): IPSCreditorAccountChoiceFragment

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun ipsCreditorAliasChoice(): IPSCreditorAliasChoiceFragment

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun chooseRequestedAccountFragment(): ChooseIPSRequestedAccountFragment

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun chooseRequestedAliasFragment(): ChooseIPSRequestedAliasFragment

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun chooseIPSSenderAccountFragment(): ChooseIPSSenderAccountFragment

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun transferWithIPSActivity(): TransferWithIPSActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun chooseIPSSenderAliasFragment(): ChooseIPSSenderAliasFragment

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun deleteIPSAliasActivity(): DeleteIPSAliasActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun linkAliasToAccountActivity(): SelectAliasLinkingToAccountActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun selectAccountLinkingToAliasActivity(): SelectAccountLinkingToAliasActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun sendOtpCodeForAliasActivity(): VerifyOtpCodeForAliasActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun createNewAliasActivity(): CreateNewAliasActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun registerAccountIpsSystemActivity(): RegisterAccountIpsSystemActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun paymentCategoriesFragment(): PaymentCategoriesFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun paymentMerchantsFragment(): PaymentMerchantsActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun paymentHistoryFragment(): PaymentHistoryFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun paymentTemplatesFragment(): PaymentTemplatesFragment

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun paymentParametersActivity(): PaymentParametersActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun checkPaymentActivity(): CheckPaymentActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun verifyPaymentActivity(): VerifyPaymentActivity

    @MainScope
    @ContributesAndroidInjector (modules = [MainModule::class, MainViewModelModule::class])
    abstract fun paymentResultActivity(): PaymentResultActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun deleteIpsAccountActivity(): DeleteIpsAccountActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun chooseAliasForDefaultActivity(): ChooseAliasForDefaultActivity

    @IPScope
    @ContributesAndroidInjector (modules = [IpsModule::class, IpsViewModelModule::class])
    abstract fun chooseAccountForDefaultActivity(): ChooseAccountForDefaultActivity

}