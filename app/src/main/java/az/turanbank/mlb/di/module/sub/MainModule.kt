package az.turanbank.mlb.di.module.sub

import az.turanbank.mlb.data.remote.Constants.Companion.BASE_URL
import az.turanbank.mlb.data.remote.model.service.MainApiService
import az.turanbank.mlb.data.remote.model.service.MainApiServiceProvider
import az.turanbank.mlb.di.scope.MainScope
import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.domain.user.data.MainRepositoryType
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

const val MLB_MAIN = "mlb_main"

@Module
class MainModule {
    @Provides
    @MainScope
    fun providesMainRepository(serviceProvider: MainApiServiceProvider): MainRepositoryType {
        return MainRepository(serviceProvider = serviceProvider)
    }

    @Provides
    @MainScope
    fun providesMainApiServiceProvider(@Named(MLB_MAIN) retrofit: Retrofit): MainApiServiceProvider =
        object : MainApiServiceProvider {
            val mainApiService = retrofit.create(MainApiService::class.java)
            override fun getInstance() = mainApiService
        }

    @Named(MLB_MAIN)
    @Provides
    @MainScope
    fun providesRetrofitInstance(
        callAdapterFactory: RxJava2CallAdapterFactory,
        converterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(converterFactory)
            .client(okHttpClient)
            .build()
    }
}