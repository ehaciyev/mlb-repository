package az.turanbank.mlb.domain.user.usecase.resources.loan.pay

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class LoanPaymentJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, compId: Long, token: String?, payType: Int, dtAccountId: Long, loanId: Long, amount: Double) =
        mainRepository.loanPayJuridical(custId, compId, token, payType, dtAccountId, loanId, amount)
}