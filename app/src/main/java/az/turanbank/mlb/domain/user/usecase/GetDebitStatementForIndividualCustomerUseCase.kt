package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetDebitStatementForIndividualCustomerUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, token: String?, accountId: Long, beginDate: String, endDate: String) =
        repository.getDebitStatementForIndividualCustomer(custId, token, accountId, beginDate, endDate)
}