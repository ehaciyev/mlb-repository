package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetBankListForJuridicalUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(ccy: String, custId: Long, compId: Long, token: String?) =
        repository.getBankInforListForJuridical(ccy, custId, compId, token)
}