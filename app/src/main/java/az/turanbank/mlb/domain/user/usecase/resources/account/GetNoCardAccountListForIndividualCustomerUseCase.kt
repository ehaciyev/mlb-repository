package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetNoCardAccountListForIndividualCustomerUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?,lang: EnumLangType) = mainRepository.getNoCardAccountListForIndividualCustomer(custId, token,lang)
}