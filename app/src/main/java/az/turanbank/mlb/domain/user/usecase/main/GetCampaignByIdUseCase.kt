package az.turanbank.mlb.domain.user.usecase.main

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetCampaignByIdUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(lang: EnumLangType,id: Long) = mainRepository.getCampaignById(lang, id)
}