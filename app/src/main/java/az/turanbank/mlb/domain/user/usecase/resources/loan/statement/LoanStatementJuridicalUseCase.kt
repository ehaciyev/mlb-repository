package az.turanbank.mlb.domain.user.usecase.resources.loan.statement

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class LoanStatementJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, loanId: Long,  compId: Long, token: String?) =
        mainRepository.loanStatementJuridical(custId, loanId, compId, token)
}