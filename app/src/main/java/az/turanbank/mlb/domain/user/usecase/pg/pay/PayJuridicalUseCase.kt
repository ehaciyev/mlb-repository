package az.turanbank.mlb.domain.user.usecase.pg.pay

import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.request.ReqPaymentDataList
import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class PayJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        transactionId: String?,
        categoryId: Long,
        merchantId: Long,
        providerId: Long,
        cardId: Long,
        reqPaymentDataList: ArrayList<ReqPaymentDataList>
    ) = mainRepository.payJuridical(
        custId,
        compId,
        token,
        transactionId,
        categoryId,
        merchantId,
        providerId,
        cardId,
        reqPaymentDataList
    )
}