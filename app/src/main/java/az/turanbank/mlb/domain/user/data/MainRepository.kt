package az.turanbank.mlb.domain.user.data

import az.turanbank.mlb.data.remote.model.branch.GetBranchMapListResponseModel
import az.turanbank.mlb.data.remote.model.cardoperations.*
import az.turanbank.mlb.data.remote.model.pg.request.*
import az.turanbank.mlb.data.remote.model.pg.response.payments.category.GetCategoryListResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.CheckMerchantResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.list.GetPaymentListResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.merchant.GetMerchantListResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.param.GetMerchantParamListResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.pay.PayResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.template.PaymentTemplateListResponseModel
import az.turanbank.mlb.data.remote.model.request.*
import az.turanbank.mlb.data.remote.model.resources.account.*
import az.turanbank.mlb.data.remote.model.resources.account.exchange.CreateExchangeOperationRequestModel
import az.turanbank.mlb.data.remote.model.resources.account.exchange.CreateExchangeOperationResponseModel
import az.turanbank.mlb.data.remote.model.resources.account.exchange.UpdateExchangeByIdRequestModel
import az.turanbank.mlb.data.remote.model.resources.card.*
import az.turanbank.mlb.data.remote.model.resources.card.sms.*
import az.turanbank.mlb.data.remote.model.resources.deposit.*
import az.turanbank.mlb.data.remote.model.resources.loan.*
import az.turanbank.mlb.data.remote.model.response.*
import az.turanbank.mlb.data.remote.model.service.GetCampaignByIdRequestModel
import az.turanbank.mlb.data.remote.model.service.GetCampaignListRequestModel
import az.turanbank.mlb.data.remote.model.service.MainApiServiceProvider
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

interface MainRepositoryType {
    fun changeAccountNameIndividual(custId: Long, token: String?,accountId: Long, accountName: String): Observable<ServerResponseModel>
    fun changeAccountNameJuridical(custId: Long, compId: Long?,token: String?,accountId: Long, accountName: String): Observable<ServerResponseModel>
    fun checkAppVersion(versionName: String): Single<CheckAppVersionResponseModel>
    fun checkInternetConnection(): Single<CheckInternetServerStatusModel>
    fun checkToken(token: String?, custId: Long, compId: Long?): Single<CheckTokenResponseModel>
    fun getIndividualCardList(custId: Long, token: String?,lang: EnumLangType): Observable<GetCardListResponseModel>
    fun getCategoryListIndividual(custId: Long, token: String?, lang: EnumLangType): Observable<GetCategoryListResponseModel>
    fun getCategoryListJuridical(custId: Long, compId: Long,token: String?, lang: EnumLangType): Observable<GetCategoryListResponseModel>
    fun getPaymentListJuridical(custId: Long, token: String?, compId: Long?,lang: EnumLangType): Observable<GetPaymentListResponseModel>
    fun getPaymentListIndividual(custId: Long, token: String?,lang: EnumLangType): Observable<GetPaymentListResponseModel>
    fun paymentTempListIndividual(custId: Long, token: String?,lang: EnumLangType): Observable<PaymentTemplateListResponseModel>
    fun paymentTempListJuridical(custId: Long, compId: Long?, token: String?,lang: EnumLangType): Observable<PaymentTemplateListResponseModel>
    fun removePaymentTempIndividual(custId: Long, token: String?, tempId: Long): Observable<ServerResponseModel>
    fun removePaymentTempJuridical(custId: Long, compId: Long?, token: String?, tempId: Long): Observable<ServerResponseModel>
    fun createPaymentTemplateIndividual(custId: Long, token: String?, tempName: String, merchantId: Long, identificationType: String, identificationCode: ArrayList<IdentificationCodeRequestModel>, amount: Double, currency: String, providerId: Long, categoryId: Long, lang: EnumLangType): Observable<ServerResponseModel>
    fun createPaymentTemplateJuridical(custId: Long, compId: Long?, token: String?, tempName: String, merchantId: Long, identificationType: String, identificationCode: ArrayList<IdentificationCodeRequestModel>, amount: Double, currency: String, providerId: Long, categoryId: Long,lang: EnumLangType): Observable<ServerResponseModel>
    fun payIndividual(custId: Long, token: String?, transactionId: String?, categoryId: Long, merchantId: Long, providerId: Long, cardId: Long, reqPaymentDataList: ArrayList<ReqPaymentDataList>): Observable<PayResponseModel>
    fun payJuridical(custId: Long, compId: Long?, token: String?, transactionId: String?, categoryId: Long, merchantId: Long, providerId: Long, cardId: Long, reqPaymentDataList: ArrayList<ReqPaymentDataList>): Observable<PayResponseModel>
    fun getMerchantListIndividual(custId: Long, token: String?, categoryId: Long, lang: EnumLangType): Observable<GetMerchantListResponseModel>
    fun getMerchantListJuridical(custId: Long, compId: Long, token: String?, categoryId: Long, lang: EnumLangType): Observable<GetMerchantListResponseModel>
    fun getMerchantParamListIndividual(custId: Long, token: String?, merchantId: Long, lang: EnumLangType): Observable<GetMerchantParamListResponseModel>
    fun getMerchantParamListJuridical(custId: Long, compId: Long, token: String?, merchantId: Long, lang: EnumLangType): Observable<GetMerchantParamListResponseModel>
    fun checkMerchantIndividual(custId: Long, token: String?, identificationType: String, identificationCode: ArrayList<IdentificationCodeRequestModel>, merchantId: Long, categoryId: Long, lang: EnumLangType, providerId: Long): Observable<CheckMerchantResponseModel>
    fun checkMerchantJuridical(custId: Long, compId: Long, token: String?, identificationType: String, identificationCode: ArrayList<IdentificationCodeRequestModel>, merchantId: Long, categoryId: Long, lang: EnumLangType, providerId: Long): Observable<CheckMerchantResponseModel>
    fun getJuridicalCardList(custId: Long, compId: Long, token: String?,lang: EnumLangType): Observable<GetCardListResponseModel>
    fun getIndividualCardById(custId: Long, cardId: Long, token: String?,lang: EnumLangType): Observable<CardListResponseModel>
    fun getJuridicalCardById(custId: Long, compId: Long, cardId: Long, token: String?,lang: EnumLangType): Observable<CardListResponseModel>
    fun getAccountListForIndividualCustomer(custId: Long, token: String?, lang: EnumLangType): Observable<GetAccountListResponseModel>
    fun getAccountListForJuridicalCustomer(custId: Long, compId: Long, token: String?,lang: EnumLangType): Observable<GetAccountListResponseModel>
    fun getNoCardAccountListForIndividualCustomer(custId: Long, token: String?, lang: EnumLangType): Observable<GetAccountListResponseModel>
    fun getNoCardAccountListForJuridicalCustomer(custId: Long, compId: Long, token: String?,lang: EnumLangType): Observable<GetAccountListResponseModel>
    fun loansForIndividualCustomer(custId: Long, token: String?,lang: EnumLangType): Observable<GetLoansListResponseModel>
    fun loansForJuridicalCustomer(custId: Long, compId: Long, token: String?,lang: EnumLangType): Observable<GetLoansListResponseModel>
    fun individualDepositList(custId: Long, token: String?, lang: EnumLangType): Observable<DepositListResponseModel>
    fun juridicalDepositList(custId: Long, compId: Long, token: String?, lang: EnumLangType): Observable<DepositListResponseModel>
    fun getCardStatementForIndividualCustomer(custId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String): Observable<GetCardStatementResponseModel>
    fun getCardStatementForJuridicalCustomer(custId: Long, compId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String): Observable<GetCardStatementResponseModel>
    fun getCardDebitStatementForIndividualCustomer(custId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String): Observable<GetCardStatementResponseModel>
    fun getCardDebitStatementForJuridicalCustomer(custId: Long, compId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String): Observable<GetCardStatementResponseModel>
    fun getCardCreditStatementForIndividualCustomer(custId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String): Observable<GetCardStatementResponseModel>
    fun getCardCreditStatementForJuridicalCustomer(custId: Long, compId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String): Observable<GetCardStatementResponseModel>
    fun getBlockReasons(custId: Long, token: String?, lang: EnumLangType): Single<BlockReasonResponseModel>
    fun cardToCard(custId: Long, requestorCardId: Long, destinationCardId: Long, currency: String, amount: Double?, token: String?,lang: EnumLangType): Single<CardToCardResponseModel>
    fun cardToOtherCard(custId: Long, requestorCardId: Long, destinationCardNumber: String, currency: String, amount: Double?, token: String?,lang: EnumLangType): Single<CardToCardResponseModel>
    fun blockCardForIndividualCustomer(custId: Long, token: String?, cardId: Long, blockReason: String): Single<ServerResponseModel>
    fun blockCardForJuridicalCustomer(custId: Long, compId: Long, token: String?, cardId: Long, blockReason: String): Single<ServerResponseModel>
    fun unblockCardForIndividualCustomer(custId: Long, token: String?, cardId: Long): Single<ServerResponseModel>
    fun unblockCardForJuridicalCustomer(custId: Long, compId: Long, token: String?, cardId: Long): Single<ServerResponseModel>
    fun addOrUpdateSmsNotificationForIndividualCustomer(mobile: String, repeatMobile: String, custId: Long, token: String?, cardId: Long): Single<ServerResponseModel>
    fun addOrUpdateSmsNotificationForJuridicalCustomer(mobile: String, repeatMobile: String, custId: Long, compId: Long, token: String?, cardId: Long): Single<ServerResponseModel>
    fun disableSmsNotificationForIndividualCustomer(mobile: String, repeatMobile: String, custId: Long, token: String?, cardId: Long): Single<ServerResponseModel>
    fun disableSmsNotificationForJuridicalCustomer(mobile: String, repeatMobile: String, custId: Long, compId: Long, token: String?, cardId: Long): Single<ServerResponseModel>
    fun getSmsNotificationNumberForIndividualCustomer(custId: Long, token: String?, cardId: Long,lang: EnumLangType): Single<GetSmsNotificationNumberResponseModel>
    fun getSmsNotificationNumberForJuridicalCustomer(custId: Long, compId: Long, token: String?, cardId: Long,lang: EnumLangType): Single<GetSmsNotificationNumberResponseModel>
    fun getCardRequisiteForIndividualCustomer(custId: Long, cardId: Long, token: String?, lang: EnumLangType): Single<GetCardRequisitesResponseModel>
    fun getCardRequisiteForJuridicalCustomer(custId: Long, compId: Long, cardId: Long, token: String?,lang: EnumLangType): Single<GetCardRequisitesResponseModel>
    fun getAccountRequisitesForIndividualCustomer(custId: Long, accountId: Long, token: String?): Single<GetAccountRequisitesResponseModel>
    fun cashByCode(custId: Long, requestorCardId: Long, destinationPhoneNumber: String, currency: String, amount: Double?, token: String?, lang: EnumLangType): Single<CashByCodeResponseModel>
    fun getAccountRequisitesForJuridicalCustomer(custId: Long, compId: Long, accountId: Long, token: String?): Single<GetAccountRequisitesResponseModel>
    fun getAccountStatementForJuridicalCustomer(custId: Long, compId: Long, token: String?, accountId: Long, startDate: String, endDate: String): Single<GetAccountStatementResponseModel>
    fun getAccountStatementForIndividualCustomer(custId: Long, token: String?, accountId: Long, startDate: String, endDate: String): Single<GetAccountStatementResponseModel>
    fun getCreditStatementForIndividualCustomer(custId: Long, token: String?, accountId: Long, startDate: String, endDate: String): Single<GetAccountStatementResponseModel>
    fun getCreditStatementForJuridicalCustomer(custId: Long, compId: Long, token: String?, accountId: Long, startDate: String, endDate: String): Single<GetAccountStatementResponseModel>
    fun getDebitStatementForIndividualCustomer(custId: Long, token: String?, accountId: Long, startDate: String, endDate: String): Single<GetAccountStatementResponseModel>
    fun getDebitStatementForJuridicalCustomer(custId: Long, compId: Long, token: String?, accountId: Long, startDate: String, endDate: String): Single<GetAccountStatementResponseModel>
    fun createInternalOperationIndividual(custId: Long, token: String?, dtAccountId: Long, crIban: String, amount: Double, purpose: String, taxNo: String?,lang: EnumLangType): Single<DomesticOperationResponseModel>
    fun createInternalOperationJuridical(custId: Long, compId: Long, token: String?, dtAccountId: Long, crIban: String, amount: Double, purpose: String, taxNo: String?,lang: EnumLangType): Single<DomesticOperationResponseModel>
    fun getCustInfoByIban(custId: Long, token: String?, iban: String): Single<GetCustInfoByIbanResponseModel>
    fun getAllOperationListIndividual(custId: Long, token: String?,lang:EnumLangType): Single<GetAllOperationListResponseModel>
    fun getBankInforListIndividual(ccy: String,custId: Long, token: String?): Single<GetBankInfoResponseModel>
    fun getBankInforListForJuridical(ccy: String, custId: Long, compId: Long, token: String?): Single<GetBankInfoResponseModel>
    fun getAllOperationListJuridical(custId: Long, compId: Long, token: String?,lang: EnumLangType): Single<GetAllOperationListResponseModel>
    fun getCardOperationTempList(custId: Long, token: String?,lang: EnumLangType): Single<GetCardOperationTempListResponseModel>
    fun getCardOperationTempById(tempId: Long, custId: Long, token: String?,lang: EnumLangType): Single<CardTemplateResponseModel>
    fun deleteCardOperationTemp(tempId: Long, custId: Long, token: String?,lang: EnumLangType): Single<ServerResponseModel>
    fun getBudgetLevelCode(custId: Long, token: String?): Single<GetBudgetCodeResponseModel>
    fun getBudgetClassificationCode(custId: Long, token: String?): Single<GetBudgetCodeResponseModel>
    fun getBudgetAccountList(custId: Long, token: String?): Single<GetBudgetAccountListResponseModel>
    fun saveCashByCodeTemp(custId: Long, tempName: String, requestorCardNumber: String, destinationPhoneNumber: String, amount: Double, currency: String, token: String?): Single<ServerResponseModel>
    fun saveCardOperationTemp(custId: Long, tempName: String, requestorCardNumber: String, destinationCardNumber: String, amount: Double, currency: String, cardOperationType: Int, token: String?): Single<ServerResponseModel>
    fun getBudgetAccountByIban(custId: Long, token: String?, iban: String): Single<GetBudgetAccountByIbanResponseModel>
    fun createInLandOperationJuridical(custId: Long, compId: Long, token: String?, dtAccountId: Long, crIban: String, crCustTaxid: String, crCustName: String, crBankCode: String, crBankName: String, crBankTaxid: String, crBankCorrAcc: String, budgetCode: String, budgetLvl: String, amount: Double, purpose: String, note: String, operationType: Int,lang: EnumLangType): Single<DomesticOperationResponseModel>
    fun createInLandOperationIndividual(custId: Long, token: String?, dtAccountId: Long, crIban: String, crCustTaxid: String, crCustName: String, crBankCode: String, crBankName: String, crBankTaxid: String, crBankCorrAcc: String, budgetCode: String, budgetLvl: String, amount: Double, purpose: String, note: String, operationType: Int,lang: EnumLangType): Single<DomesticOperationResponseModel>
    fun getCampaignList(lang: EnumLangType): Single<CampaignResponseModel>
    fun getSignInfo(operationIds: ArrayList<Long?>, custId: Long, token: String?, phoneNumber: String?, userId: String?, lang: EnumLangType, certCode: String?): Single<GetSignInfoResponseModel>
    fun getSecondSignInfo(operationIds: ArrayList<Long?>, custId: Long, compId: Long, token: String?, phoneNumber: String?, userId: String?, lang: EnumLangType, certCode: String?): Single<GetSignInfoResponseModel>
    fun getAuthOperationListIndividual(custId: Long, token: String?, lang: EnumLangType): Single<GetAuthOperationListResponseModel>
    fun loanStatementIndividual(custId: Long, loanId: Long, token: String?): Single<LoanStatementResponseModel>
    fun loanStatementJuridical(custId: Long, loanId: Long, compId: Long, token: String?): Single<LoanStatementResponseModel>
    fun loanPaymentPlanIndividual(custId: Long, loanId: Long, token: String?): Single<LoanPaymentPlanResponseModel>
    fun loanPaymentPlanJuridical(custId: Long, loanId: Long, compId: Long, token: String?): Single<LoanPaymentPlanResponseModel>
    fun loanPayedAmountsIndividual(custId: Long, loanId: Long, token: String?): Single<LoanPayedAmountsResponseModel>
    fun loanPayedAmountsJuridical(custId: Long, loanId: Long, compId: Long, token: String?): Single<LoanPayedAmountsResponseModel>
    fun getDepositByIdForIndividual(custId: Long, depositId: Long, token: String?,lang: EnumLangType): Single<DepositByIdResponseModel>
    fun getDepositByIdForJuridical(custId: Long, compId: Long, depositId: Long, token: String?,lang: EnumLangType): Single<DepositByIdResponseModel>
    fun getCardNumbersByDepositIdForIndividual(custId: Long, depositId: Long, token: String?,lang: EnumLangType): Single<GetDepositCardNumbersResponseModel>
    fun getCardNumbersByDepositIdForJuridical(custId: Long, compId: Long, depositId: Long, token: String?,lang: EnumLangType): Single<GetDepositCardNumbersResponseModel>
    fun getPayedAmountsByDepositIdForIndividual(custId: Long, depositId: Long, token: String?,lang: EnumLangType): Single<GetPayedAmountsByDepositIdResponseModel>
    fun getPayedAmountsByDepositIdForJuridical(custId: Long, compId: Long, depositId: Long, token: String?,lang: EnumLangType): Single<GetPayedAmountsByDepositIdResponseModel>
    fun getEarlyPaysByDepositIdForIndividual(custId: Long, depositId: Long, token: String?,lang: EnumLangType): Single<GetEarlyPaysByDepositResponseModel>
    fun getEarlyPaysByDepositIdForJuridical(custId: Long, compId: Long, depositId: Long, token: String?,lang: EnumLangType): Single<GetEarlyPaysByDepositResponseModel>
    fun signFile(custId: Long, token: String?, operationIds: ArrayList<Long>, transactionId: Long, phoneNumber: String?, userId: String?, lang: EnumLangType, certCode: String?,successOperId: ArrayList<Long>,failOperId: ArrayList<Long>): Single<SignFileResponseModel>
    fun signFileSecondAuth(custId: Long, compId: Long, token: String?, operationIds: ArrayList<Long>,
                           transactionId: Long, phoneNumber: String?, userId: String?, lang: EnumLangType, certCode: String?,
                           successOperId: ArrayList<Long>, failOperId: ArrayList<Long>): Single<SignFileResponseModel>
    fun getCampaignById(lang: EnumLangType,id: Long): Single<CampaignModel>
    fun logout(custId: Long, compId: Long?, token: String?): Single<LogoutResponseModel>
    fun getSecondAuthOperationList(custId: Long, compId: Long, token: String?,lang: EnumLangType): Single<GetAuthOperationListResponseModel>
    fun getFirstAuthOperationList(custId: Long, compId: Long, token: String?,lang: EnumLangType): Single<GetFirstAuthOperationListResponseModel>
    fun getFirstSignInfo(custId: Long, compId: Long, token: String?, userId: String?, phoneNumber: String?, certCode: String?, lang: EnumLangType, batchId: Long): Single<GetSignInfoResponseModel>
    fun signFileFirstAuth(custId: Long, token: String?, compId: Long, transactionId: Long, userId: String?, phoneNumber: String?,
                          certCode: String?, batchId: Long,
                          successOperId: ArrayList<Long>, failOperId: ArrayList<Long>): Single<SignFileResponseModel>
    fun getOperationTempListIndividual(custId: Long, token: String?, lang: EnumLangType): Single<GetAccountTemplateListResponseModel>
    fun getOperationTempListJuridical(custId: Long, compId: Long, token: String?, lang: EnumLangType): Single<GetAccountTemplateListResponseModel>
    fun deleteOperation(custId: Long, token: String?, operId: Long): Single<ServerResponseModel>
    fun addOperationTempIndividual(custId: Long, tempName: String, token: String?, operId: Long): Single<ServerResponseModel>
    fun addOperationTempJuridical(custId: Long, compId: Long, tempName: String, token: String?, operId: Long): Single<ServerResponseModel>
    fun getPaymentDocByOperId(custId: Long, token: String?, operId: Long): Single<GetPaymentDocByOperIdResponseModel>
    fun getOperationById(custId: Long, token: String?, operId: Long): Single<GetOperationByIdResponseModel>
    fun deleteOperationTemp(custId: Long, token: String?, tempId: Long): Single<ServerResponseModel>
    fun createAbroadOperationIndividual(custId: Long, token: String?, dtAccountId: Long, amount: Double, crBankName: String, crBankSwift: String, crCustName: String, crCustAddress: String, crIban: String, purpose: String, crBankBranch: String, crBankAddress: String, crBankCountry: String, crBankCity: String, crCustPhone: String, note: String, crCorrBankName: String, crCorrBankCountry: String, crCorrBankCity: String, crCorrBankSwift: String, crCorrBankAccount: String, crCorrBankBranch: String,lang: EnumLangType): Single<DomesticOperationResponseModel>
    fun createAbroadOperationJuridical(custId: Long, compId: Long, token: String?, dtAccountId: Long, amount: Double, crBankName: String, crBankSwift: String, crCustName: String, crCustAddress: String, crIban: String, purpose: String, crBankBranch: String, crBankAddress: String, crBankCountry: String, crBankCity: String, crCustPhone: String, note: String, crCorrBankName: String, crCorrBankCountry: String, crCorrBankCity: String, crCorrBankSwift: String, crCorrBankAccount: String, crCorrBankBranch: String,lang: EnumLangType): Single<DomesticOperationResponseModel>
    fun getAsanDocByOperId(custId: Long, token: String?, operId: Long): Single<GetPaymentDocByOperIdResponseModel>
    fun updateOperationById(custId: Long, compId: Long?, token: String?, dtAccountId: Long, crIban: String?, crCustTaxid: String?, crCustName: String?, crBankCode: String?, crBankName: String?, crBankTaxid: String?, crBankCorrAcc: String?, budgetCode: String?, budgetLvl: String?, amount: Double, purpose: String?, note: String?, operationType: Int?, operId: Long, operNameId: Long): Single<ServerResponseModel>
    fun updateAbroadById(custId: Long, compId: Long?, token: String?, dtAccountId: Long, amount: Double, crBankName: String, crBankSwift: String, crCustName: String, crCustAddress: String, crIban: String, purpose: String, crBankBranch: String, crBankAddress: String, crBankCountry: String, crBankCity: String, crCustPhone: String, note: String, crCorrBankName: String, crCorrBankCountry: String, crCorrBankCity: String, crCorrBankSwift: String, crCorrBankAccount: String, crCorrBankBranch: String, operId: Long, operNameId: Long): Single<ServerResponseModel>
    fun updateExchangeOperation(custId: Long, compId: Long?, token: String?, dtAccountId: Long, dtAmount: Double, crAmount: Double, crIban: String, dtBranchId: Long, crBranchId: Long, exchangeRate: Double, exchangeOperationType: Int, operId: Long, operNameId: Long): Single<ServerResponseModel>
    fun getBranchList(custId: Long, token: String?, lang: EnumLangType): Single<GetBranchListResponseModel>
    fun createExchangeOperationIndividual(custId: Long, token: String?, dtAccountId: Long, dtAmount: Double, crAmount: Double, crIban: String, dtBranchId: Long, crBranchId: Long, exchangeRate: Double, exchangeOperationType: Int, lang: EnumLangType): Single<CreateExchangeOperationResponseModel>
    fun createExchangeOperationJuridical(custId: Long, compId: Long, token: String?, dtAccountId: Long, dtAmount: Double, crAmount: Double, crIban: String, dtBranchId: Long, crBranchId: Long, exchangeRate: Double,exchangeOperationType: Int,lang:EnumLangType): Single<CreateExchangeOperationResponseModel>
    fun getOrderKindList(custId: Long, token: String?, orderType: Int?, kindType: Long?,lang: EnumLangType): Single<GetOrderKindListResponseModel>
    fun createOrderJuridical(custId: Long, compId: Long?, token: String?, branchId: Long, orderType: Int?, kindId: Long?, period: Int, amount: Double, ccy: String,lang: EnumLangType): Single<ServerResponseModel>
    fun createOrderIndividual(custId: Long, token: String?, branchId: Long, orderType: Int?, kindId: Long?, period: Int, amount: Double, ccy: String,lang: EnumLangType): Single<ServerResponseModel>
    fun getOrderKindTypeList(custId: Long, token: String?, orderType: Int?, lang: EnumLangType): Single<GetOrderKindTypeResponseModel>
    fun getOrderPeriodList(custId: Long, token: String?, orderType: Int?, kindType: Long?,lang: EnumLangType): Single<GetOrderPeriodListResponseModel>
    fun operationAdvancedSearch(custId: Long, compId: Long?, token: String?, dtIban: String?, minAmt: Double?, maxAmt: Double?, startDate: String?, endDate: String?, operStateId: Int?,lang: EnumLangType): Single<GetAllOperationListResponseModel>
    fun getBranchMapList(lang: EnumLangType): Single<GetBranchMapListResponseModel>
    fun getOperationStateList(custId: Long, token: String?,lang: EnumLangType): Single<GetOperationStateListResponseModel>
    fun updateProfileImage(custId: Long, token: String?, compId: Long?, bytesStr: String): Single<ServerResponseModel>
    fun getProfileImage(custId: Long, compId: Long?, token: String?): Single<GetProfileImageResponseModel>
    fun changePasswordIndividual(custId: Long, token: String?, oldPassword: String?, password: String?, repeatPassword: String?): Single<ServerResponseModel>
    fun changePasswordJuridical(custId: Long, compId: Long?, token: String?, oldPassword: String?, password: String?, repeatPassword: String?): Single<ServerResponseModel>
    fun loanPayIndividual(custId: Long, token: String?, payType: Int, dtAccountId: Long, loanId: Long, amount: Double): Single<LoanPaymentResponseModel>
    fun loanPayJuridical(custId: Long, compId: Long, token: String?, payType: Int, dtAccountId: Long, loanId: Long, amount: Double): Single<LoanPaymentResponseModel>
}

class MainRepository @Inject constructor(
    private val serviceProvider: MainApiServiceProvider
) : MainRepositoryType {

    override fun getOrderPeriodList(
        custId: Long,
        token: String?,
        orderType: Int?,
        kindType: Long?,
        lang: EnumLangType
    ): Single<GetOrderPeriodListResponseModel> {
        val request = GetOrderPeriodRequestModel(custId, token, orderType, kindType,lang)
        return serviceProvider.getInstance().getOrderPeriodList(request)
    }

    override fun operationAdvancedSearch(
        custId: Long,
        compId: Long?,
        token: String?,
        dtIban: String?,
        minAmt: Double?,
        maxAmt: Double?,
        startDate: String?,
        endDate: String?,
        operStateId: Int?,
        lang: EnumLangType
    ): Single<GetAllOperationListResponseModel> {
        val request = OperationAdvancedSearchRequestModel(custId, compId, token, dtIban, minAmt, maxAmt, startDate, endDate, operStateId, null,lang)
        return serviceProvider.getInstance().operationAdvancedSearch(request)
    }

    override fun getBranchMapList(lang: EnumLangType) = serviceProvider.getInstance().getBranchMapList(lang)

    override fun getOperationStateList(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetOperationStateListResponseModel> {
        val request = GetOperationStateListRequestModel(custId, token,lang)
        return serviceProvider.getInstance().getOperationStateList(request)
    }

    override fun updateProfileImage(
        custId: Long,
        token: String?,
        compId: Long?,
        bytesStr: String
    ): Single<ServerResponseModel> {
        val request = UpdateProfileImageRequestModel(custId, token, compId, bytesStr)
        return serviceProvider.getInstance().updateProfileImage(request)
    }

    override fun getProfileImage(
        custId: Long,
        compId: Long?,
        token: String?
    ): Single<GetProfileImageResponseModel> {
        val request = GetProfileImageRequestModel(custId, compId, token)
        return serviceProvider.getInstance().getProfileImage(request)
    }

    override fun changePasswordIndividual(
        custId: Long,
        token: String?,
        oldPassword: String?,
        password: String?,
        repeatPassword: String?
    ): Single<ServerResponseModel> {
        val request = PasswordChangeIndividualRequestModel(custId, token, oldPassword, password, repeatPassword)
        return serviceProvider.getInstance().changePasswordIndividual(request)
    }

    override fun changePasswordJuridical(
        custId: Long,
        compId: Long?,
        token: String?,
        oldPassword: String?,
        password: String?,
        repeatPassword: String?
    ): Single<ServerResponseModel> {
        val request = PasswordChangeJuridicalRequestModel(custId, compId, token, oldPassword, password, repeatPassword)
        return serviceProvider.getInstance().changePasswordJuridical(request)
    }

    override fun loanPayIndividual(
        custId: Long,
        token: String?,
        payType: Int,
        dtAccountId: Long,
        loanId: Long,
        amount: Double
    ): Single<LoanPaymentResponseModel> {
        val request = LoanPaymentRequestModel(custId, null, token, payType, dtAccountId, loanId, amount)

        return serviceProvider.getInstance().loanPayIndividual(request)
    }

    override fun loanPayJuridical(
        custId: Long,
        compId: Long,
        token: String?,
        payType: Int,
        dtAccountId: Long,
        loanId: Long,
        amount: Double
    ): Single<LoanPaymentResponseModel> {
        val request = LoanPaymentRequestModel(custId, compId, token, payType, dtAccountId, loanId, amount)

        return serviceProvider.getInstance().loanPayJuridical(request)
    }

    override fun getOrderKindTypeList(
        custId: Long,
        token: String?,
        orderType: Int?,
        lang: EnumLangType
    ): Single<GetOrderKindTypeResponseModel> {
        val request = GetOrderKindTypeListRequestModel(custId, token, orderType, lang)
        return serviceProvider.getInstance().getOrderKindTypeList(request)
    }

    override fun createOrderJuridical(
        custId: Long,
        compId: Long?,
        token: String?,
        branchId: Long,
        orderType: Int?,
        kindId: Long?,
        period: Int,
        amount: Double,
        ccy: String,
        lang: EnumLangType
    ): Single<ServerResponseModel> {
        val request = CreateOrderRequestModel(custId, compId, token, branchId, orderType, kindId, period, amount, ccy,lang)
        return serviceProvider.getInstance().createOrderJuridical(request)
    }

    override fun createOrderIndividual(
        custId: Long,
        token: String?,
        branchId: Long,
        orderType: Int?,
        kindId: Long?,
        period: Int,
        amount: Double,
        ccy: String,
        lang: EnumLangType
    ): Single<ServerResponseModel> {
        val request = CreateOrderRequestModel(custId, null, token, branchId, orderType, kindId, period, amount, ccy,lang)
        return serviceProvider.getInstance().createOrderIndividual(request)
    }

    override fun getOrderKindList(
        custId: Long,
        token: String?,
        orderType: Int?,
        kindType: Long?,
        lang: EnumLangType
    ): Single<GetOrderKindListResponseModel> {
        val request = GetOrderKindListRequestModel(custId, token, orderType, kindType,lang)

        return serviceProvider.getInstance().getOrderKindList(request)
    }

    override fun createExchangeOperationIndividual(
        custId: Long,
        token: String?,
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int,
        lang: EnumLangType
    ): Single<CreateExchangeOperationResponseModel> {
        val request = CreateExchangeOperationRequestModel(custId, null, token, dtAccountId, dtAmount, crAmount, crIban, dtBranchId, crBranchId, exchangeRate, exchangeOperationType,lang)

        return serviceProvider.getInstance().createExchangeOperationIndividual(request)
    }

    override fun createExchangeOperationJuridical(
        custId: Long,
        compId: Long,
        token: String?,
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int,
        lang: EnumLangType
    ): Single<CreateExchangeOperationResponseModel> {
        val request = CreateExchangeOperationRequestModel(custId, compId, token, dtAccountId, dtAmount, crAmount, crIban, dtBranchId, crBranchId, exchangeRate, exchangeOperationType,lang)

        return serviceProvider.getInstance().createExchangeOperationJuridical(request)
    }

    override fun getBranchList(custId: Long, token: String?, lang: EnumLangType): Single<GetBranchListResponseModel> {
        val request = GetBranchListRequestModel(custId, token, lang)
        return serviceProvider.getInstance().getBranchList(request)
    }

    override fun getAsanDocByOperId(
        custId: Long,
        token: String?,
        operId: Long
    ): Single<GetPaymentDocByOperIdResponseModel> {
        val request = GetPaymentDocByOperIdRequestModel(custId, token, operId)
        return serviceProvider.getInstance().getAsanDocByOperId(request)
    }

    override fun updateOperationById(
        custId: Long,
        compId: Long?,
        token: String?,
        dtAccountId: Long,
        crIban: String?,
        crCustTaxid: String?,
        crCustName: String?,
        crBankCode: String?,
        crBankName: String?,
        crBankTaxid: String?,
        crBankCorrAcc: String?,
        budgetCode: String?,
        budgetLvl: String?,
        amount: Double,
        purpose: String?,
        note: String?,
        operationType: Int?,
        operId: Long,
        operNameId: Long
    ): Single<ServerResponseModel> {
        val request = UpdateOperationByIdRequestModel(
            custId = custId,
            compId = compId,
            token = token,
            dtAccountId = dtAccountId,
            crIban = crIban,
            crCustTaxid = crCustTaxid,
            crCustName = crCustName,
            crBankCode = crBankCode,
            crBankName = crBankName,
            crBankTaxid = crBankTaxid,
            crBankCorrAcc = crBankCorrAcc,
            budgetCode = budgetCode,
            budgetLvl = budgetLvl,
            amount = amount,
            purpose = purpose,
            note = note,
            operationType = operationType,
            operId = operId,
            operNameId = operNameId
        )
        return serviceProvider.getInstance().updateOperationById(request)
    }

    override fun updateAbroadById(
        custId: Long,
        compId: Long?,
        token: String?,
        dtAccountId: Long,
        amount: Double,
        crBankName: String,
        crBankSwift: String,
        crCustName: String,
        crCustAddress: String,
        crIban: String,
        purpose: String,
        crBankBranch: String,
        crBankAddress: String,
        crBankCountry: String,
        crBankCity: String,
        crCustPhone: String,
        note: String,
        crCorrBankName: String,
        crCorrBankCountry: String,
        crCorrBankCity: String,
        crCorrBankSwift: String,
        crCorrBankAccount: String,
        crCorrBankBranch: String,
        operId: Long,
        operNameId: Long
    ): Single<ServerResponseModel> {
        val request = UpdateAbroadOperationRequestModel(
            custId,
            compId,
            token,
            dtAccountId,
            amount,
            crBankName,
            crBankSwift,
            crCustName,
            crCustAddress,
            crIban,
            purpose,
            crBankBranch,
            crBankAddress,
            crBankCountry,
            crBankCity,
            crCustPhone,
            note,
            crCorrBankName,
            crCorrBankCountry,
            crCorrBankCity,
            crCorrBankSwift,
            crCorrBankAccount,
            crCorrBankBranch,
            operId,
            operNameId
        )

        return serviceProvider.getInstance().updateAbroadById(request)

    }

    override fun updateExchangeOperation(
        custId: Long,
        compId: Long?,
        token: String?,
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int,
        operId: Long,
        operNameId: Long
    ): Single<ServerResponseModel> {
        val request = UpdateExchangeByIdRequestModel(custId, compId, token, dtAccountId, dtAmount, crAmount, crIban, dtBranchId, crBranchId, exchangeRate, exchangeOperationType, operId, operNameId)

        return serviceProvider.getInstance().updateExchangeById(request)
    }

    override fun getOperationById(
        custId: Long,
        token: String?,
        operId: Long
    ): Single<GetOperationByIdResponseModel> {
        val request = GetOperationByIdRequestModel(operId, custId, token)
        return serviceProvider.getInstance().getOperationById(request)
    }

    override fun getPaymentDocByOperId(
        custId: Long,
        token: String?,
        operId: Long
    ): Single<GetPaymentDocByOperIdResponseModel> {
        val request = GetPaymentDocByOperIdRequestModel(custId, token, operId)
        return serviceProvider.getInstance().getPaymentDocByOperId(request)
    }

    override fun deleteOperationTemp(
        custId: Long,
        token: String?,
        tempId: Long
    ): Single<ServerResponseModel> {
        val request = DeleteOperationTemplateRequestModel(custId, token, tempId)

        return serviceProvider.getInstance().deleteOperationTemp(request)
    }

    override fun createAbroadOperationIndividual(
        custId: Long,
        token: String?,
        dtAccountId: Long,
        amount: Double,
        crBankName: String,
        crBankSwift: String,
        crCustName: String,
        crCustAddress: String,
        crIban: String,
        purpose: String,
        crBankBranch: String,
        crBankAddress: String,
        crBankCountry: String,
        crBankCity: String,
        crCustPhone: String,
        note: String,
        crCorrBankName: String,
        crCorrBankCountry: String,
        crCorrBankCity: String,
        crCorrBankSwift: String,
        crCorrBankAccount: String,
        crCorrBankBranch: String,
        lang: EnumLangType
    ): Single<DomesticOperationResponseModel> {
        val request = CreateAbroadOperationRequestModel(custId, null, token, dtAccountId, amount, crBankName, crBankSwift, crCustName, crCustAddress, crIban, purpose, crBankBranch, crBankAddress, crBankCountry, crBankCity, crCustPhone, note, crCorrBankName, crCorrBankCountry, crCorrBankCity, crCorrBankSwift, crCorrBankAccount, crCorrBankBranch,lang)

        return serviceProvider.getInstance().createAbroadOperationIndividual(request)
    }

    override fun createAbroadOperationJuridical(
        custId: Long,
        compId: Long,
        token: String?,
        dtAccountId: Long,
        amount: Double,
        crBankName: String,
        crBankSwift: String,
        crCustName: String,
        crCustAddress: String,
        crIban: String,
        purpose: String,
        crBankBranch: String,
        crBankAddress: String,
        crBankCountry: String,
        crBankCity: String,
        crCustPhone: String,
        note: String,
        crCorrBankName: String,
        crCorrBankCountry: String,
        crCorrBankCity: String,
        crCorrBankSwift: String,
        crCorrBankAccount: String,
        crCorrBankBranch: String,
        lang: EnumLangType
    ): Single<DomesticOperationResponseModel> {
        val request = CreateAbroadOperationRequestModel(custId, compId, token, dtAccountId, amount, crBankName, crBankSwift, crCustName, crCustAddress, crIban, purpose, crBankBranch, crBankAddress, crBankCountry, crBankCity, crCustPhone, note, crCorrBankName, crCorrBankCountry, crCorrBankCity, crCorrBankSwift, crCorrBankAccount, crCorrBankBranch,lang)

        return serviceProvider.getInstance().createAbroadOperationJuridical(request)
    }

    override fun getOperationTempListIndividual(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetAccountTemplateListResponseModel> {
        val request = GetCardListIndividualRequestModel(custId, token, lang)

        return serviceProvider.getInstance().getOperationTempListIndividual(request)
    }

    override fun getOperationTempListJuridical(
        custId: Long,
        compId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetAccountTemplateListResponseModel> {
        val request = GetJuridicalCardListRequestModel(custId, compId, token,lang)

        return serviceProvider.getInstance().getOperationTempListJuridical(request)
    }

    override fun deleteOperation(
        custId: Long,
        token: String?,
        operId: Long
    ): Single<ServerResponseModel> {
        val request = DeleteOperationRequestModel(custId, token, operId)
        return serviceProvider.getInstance().deleteOperation(request)
    }

    override fun addOperationTempIndividual(custId: Long, tempName: String, token: String?, operId: Long): Single<ServerResponseModel> {
        val request = AddOperationTemplateRequestModel(custId, null, tempName, token, operId)

        return serviceProvider.getInstance().addOperationTempIndividual(request)
    }

    override fun addOperationTempJuridical(custId: Long, compId: Long, tempName: String, token: String?, operId: Long): Single<ServerResponseModel> {
        val request = AddOperationTemplateRequestModel(custId, compId, tempName, token, operId)

        return serviceProvider.getInstance().addOperationTempJuridical(request)
    }

    override fun signFileFirstAuth(
        custId: Long,
        token: String?,
        compId: Long,
        transactionId: Long,
        userId: String?,
        phoneNumber: String?,
        certCode: String?,
        batchId: Long,
        successOperId: ArrayList<Long>,
        failOperId: ArrayList<Long>
    ): Single<SignFileResponseModel> {
        val request = SignFileFirstAuthRequestModel(custId, token, compId, transactionId, userId, phoneNumber, certCode, batchId,successOperId,failOperId)
        return serviceProvider.getInstance().signFileFirstAuth(request)
    }

    override fun getFirstSignInfo(
        custId: Long,
        compId: Long,
        token: String?,
        userId: String?,
        phoneNumber: String?,
        certCode: String?,
        lang: EnumLangType,
        batchId: Long
    ): Single<GetSignInfoResponseModel> {
        val request = GetFirstSignInfoRequestModel(custId, compId, token, userId, phoneNumber, certCode, lang, batchId)
        return serviceProvider.getInstance().getFirstSignInfo(request)
    }

    override fun getFirstAuthOperationList(
        custId: Long,
        compId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetFirstAuthOperationListResponseModel> {
        val request = GetFirstAuthOperationListRequestModel(custId, compId, token,lang)
        return serviceProvider.getInstance().getFirstAuthOperationList(request)
    }

    override fun signFileSecondAuth(
        custId: Long,
        compId: Long,
        token: String?,
        operationIds: ArrayList<Long>,
        transactionId: Long,
        phoneNumber: String?,
        userId: String?,
        lang: EnumLangType,
        certCode: String?,
        successOperId: ArrayList<Long>,
        failOperId: ArrayList<Long>
    ): Single<SignFileResponseModel> {

        val request = SignFileSecondAuthRequestModel(
            custId = custId,
            compId = compId,
            phoneNumber = phoneNumber,
            token = token,
            transactionId = transactionId,
            operationIds = operationIds,
            userId = userId,
            certCode = certCode,
            lang = lang,
            successOperId = successOperId,
            failOperId = failOperId
        )

        return serviceProvider.getInstance().signFileSecondAuth(request)
    }

    override fun getSecondSignInfo(
        operationIds: ArrayList<Long?>,
        custId: Long,
        compId: Long,
        token: String?,
        phoneNumber: String?,
        userId: String?,
        lang: EnumLangType,
        certCode: String?
    ): Single<GetSignInfoResponseModel> {
        val request = GetSecondSignInfoRequestModel(
            operationIds = operationIds,
            custId = custId,
            compId = compId,
            token = token,
            phoneNumber = phoneNumber,
            userId = userId,
            lang = lang,
            certCode = certCode
        )
        return  serviceProvider.getInstance().getSecondSignInfo(request)
    }

    override fun getSecondAuthOperationList(
        custId: Long,
        compId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetAuthOperationListResponseModel> {
        val request = GetSecondAuthOperationListRequestModel(custId, compId, token,lang)

        return serviceProvider.getInstance().getSecondAuthOperationList(request)
    }

    override fun logout(custId: Long, compId: Long?, token: String?): Single<LogoutResponseModel> {
        val request = LogoutRequestModel(custId, compId, token)
        return serviceProvider.getInstance().logout(request)
    }

    override fun getDepositByIdForIndividual(
        custId: Long,
        depositId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<DepositByIdResponseModel> {
        val request = GetDepositIndividualRequestModel(custId, depositId, token,lang)

        return serviceProvider.getInstance().individualDepositById(request)
    }

    override fun getDepositByIdForJuridical(
        custId: Long,
        compId: Long,
        depositId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<DepositByIdResponseModel> {
        val request = GetDepositJuridicalRequestModel(custId, compId, depositId, token,lang)

        return serviceProvider.getInstance().juridicalDepositById(request)
    }

    override fun getCardNumbersByDepositIdForIndividual(
        custId: Long,
        depositId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetDepositCardNumbersResponseModel> {
        val request = GetDepositIndividualRequestModel(custId, depositId, token,lang)

        return serviceProvider.getInstance().individualDepositCardsById(request)
    }

    override fun getCardNumbersByDepositIdForJuridical(
        custId: Long,
        compId: Long,
        depositId: Long,
        token: String?,
        lang:EnumLangType
    ): Single<GetDepositCardNumbersResponseModel> {
        val request = GetDepositJuridicalRequestModel(custId, compId, depositId, token,lang)

        return serviceProvider.getInstance().juridicalDepositCardsById(request)
    }

    override fun getPayedAmountsByDepositIdForIndividual(
        custId: Long,
        depositId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetPayedAmountsByDepositIdResponseModel> {
        val request = GetDepositIndividualRequestModel(custId, depositId, token,lang)

        return serviceProvider.getInstance().individualPayedAmountsById(request)
    }

    override fun getPayedAmountsByDepositIdForJuridical(
        custId: Long,
        compId: Long,
        depositId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetPayedAmountsByDepositIdResponseModel> {
        val request = GetDepositJuridicalRequestModel(custId, compId, depositId, token,lang)

        return serviceProvider.getInstance().juridicalPayedAmountsById(request)
    }

    override fun getEarlyPaysByDepositIdForIndividual(
        custId: Long,
        depositId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetEarlyPaysByDepositResponseModel> {
        val request = GetDepositIndividualRequestModel(custId, depositId, token,lang)

        return serviceProvider.getInstance().individualEarlyPayPercentsById(request)
    }

    override fun getEarlyPaysByDepositIdForJuridical(
        custId: Long,
        compId: Long,
        depositId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<GetEarlyPaysByDepositResponseModel> {
        val request = GetDepositJuridicalRequestModel(custId, compId, depositId, token,lang)

        return serviceProvider.getInstance().juridicalEarlyPayPercentsById(request)
    }

    override fun loanPayedAmountsIndividual(
        custId: Long,
        loanId: Long,
        token: String?
    ): Single<LoanPayedAmountsResponseModel> {
        val request = LoanPaymentPlanIndividualRequestModel(custId, loanId, token)

        return serviceProvider.getInstance().loanPaymentIndividual(request)
    }

    override fun loanPayedAmountsJuridical(
        custId: Long,
        loanId: Long,
        compId: Long,
        token: String?
    ): Single<LoanPayedAmountsResponseModel> {
        val request = LoanPaymentPlanJuridicalRequestModel(custId, compId, loanId, token)

        return serviceProvider.getInstance().loanPaymentJuridical(request)
    }

    override fun loanPaymentPlanIndividual(
        custId: Long,
        loanId: Long,
        token: String?
    ): Single<LoanPaymentPlanResponseModel> {
        val request = LoanPaymentPlanIndividualRequestModel(custId, loanId, token)

        return serviceProvider.getInstance().loanPaymentPlanIndividual(request)
    }

    override fun loanPaymentPlanJuridical(
        custId: Long,
        loanId: Long,
        compId: Long,
        token: String?
    ): Single<LoanPaymentPlanResponseModel> {
        val request = LoanPaymentPlanJuridicalRequestModel(custId, compId, loanId, token)

        return serviceProvider.getInstance().loanPaymentPlanJuridical(request)
    }

    override fun loanStatementIndividual(
        custId: Long,
        loanId: Long,
        token: String?
    ): Single<LoanStatementResponseModel> {
        val request = LoanPaymentPlanIndividualRequestModel(custId, loanId, token)

        return serviceProvider.getInstance().loanStatementIndividual(request)
    }

    override fun loanStatementJuridical(
        custId: Long,
        loanId: Long,
        compId: Long,
        token: String?
    ): Single<LoanStatementResponseModel> {
        val request = LoanPaymentPlanJuridicalRequestModel(custId, compId, loanId, token)

        return serviceProvider.getInstance().loanStatementJuridical(request)
    }

    override fun signFile(
        custId: Long,
        token: String?,
        operationIds: ArrayList<Long>,
        transactionId: Long,
        phoneNumber: String?,
        userId: String?,
        lang: EnumLangType,
        certCode: String?,
        successOperId: ArrayList<Long>,
        failOperId: ArrayList<Long>
    ): Single<SignFileResponseModel> {

        val request = SignFileRequestModel(
            custId = custId,
            phoneNumber = phoneNumber,
            token = token,
            transactionId = transactionId,
            operationIds = operationIds,
            userId = userId,
            certCode = certCode,
            lang = lang,
            successOperId = successOperId,
            failOperId = failOperId
        )

        return serviceProvider.getInstance().signFile(request)
    }

    override fun getSignInfo(
        operationIds: ArrayList<Long?>,
        custId: Long,
        token: String?,
        phoneNumber: String?,
        userId: String?,
        lang: EnumLangType,
        certCode: String?
    ): Single<GetSignInfoResponseModel> {
        val request = GetSignInfoRequestModel(
            operationIds = operationIds,
            custId = custId,
            token = token,
            phoneNumber = phoneNumber,
            userId = userId,
            lang = lang,
            certCode = certCode
        )

        return serviceProvider.getInstance().getSignInfo(request)
    }

    override fun getAuthOperationListIndividual(custId: Long, token: String?,lang: EnumLangType):
            Single<GetAuthOperationListResponseModel> {
        val request = GetAuthOperationListIndividualRequestModel(custId, token,lang)
        return serviceProvider.getInstance().getAuthOperationListIndividual(request)
    }

    override fun getCampaignList(lang: EnumLangType) : Single<CampaignResponseModel>{
        val request = GetCampaignListRequestModel(lang)
        return serviceProvider.getInstance().getCampaignList(request)
    }


    override fun getCampaignById(lang: EnumLangType, id: Long): Single<CampaignModel>{
        val request = GetCampaignByIdRequestModel(lang, id)
        return serviceProvider.getInstance().getCampaignById(request)
    }



    override fun getCardDebitStatementForIndividualCustomer(
        custId: Long,
        token: String?,
        cardNumber: String?,
        startDate: String,
        endDate: String
    ): Observable<GetCardStatementResponseModel> {
        val request = GetCardStatementForIndividualCustomerRequestModel(custId, token, cardNumber, startDate, endDate)

        return serviceProvider.getInstance().getCardDebitStatementForIndividualCustomer(request)
    }

    override fun getCardDebitStatementForJuridicalCustomer(
        custId: Long,
        compId: Long,
        token: String?,
        cardNumber: String?,
        startDate: String,
        endDate: String
    ): Observable<GetCardStatementResponseModel> {
        val request = GetCardStatementForJuridicalCustomerRequestModel(custId, compId, token, cardNumber, startDate, endDate)

        return serviceProvider.getInstance().getCardDebitStatementForJuridicalCustomer(request)
    }

    override fun getCardCreditStatementForIndividualCustomer(
        custId: Long,
        token: String?,
        cardNumber: String?,
        startDate: String,
        endDate: String
    ): Observable<GetCardStatementResponseModel> {
        val request = GetCardStatementForIndividualCustomerRequestModel(custId, token, cardNumber, startDate, endDate)

        return serviceProvider.getInstance().getCardCreditStatementForIndividualCustomer(request)
    }

    override fun getCardCreditStatementForJuridicalCustomer(
        custId: Long,
        compId: Long,
        token: String?,
        cardNumber: String?,
        startDate: String,
        endDate: String
    ): Observable<GetCardStatementResponseModel> {
        val request = GetCardStatementForJuridicalCustomerRequestModel(custId, compId, token, cardNumber, startDate, endDate)

        return serviceProvider.getInstance().getCardCreditStatementForJuridicalCustomer(request)
    }

    override fun deleteCardOperationTemp(
        tempId: Long,
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Single<ServerResponseModel> {
        val request = GetCardOperationTempListRequestModel(tempId, custId, token,lang)

        return serviceProvider.getInstance().deleteCardOperationTemp(request)
    }

    override fun saveCashByCodeTemp(
        custId: Long,
        tempName: String,
        requestorCardNumber: String,
        destinationPhoneNumber: String,
        amount: Double,
        currency: String,
        token: String?
    ): Single<ServerResponseModel> {
        val request = SaveCashByCodeTempRequestModel(custId, tempName, requestorCardNumber, destinationPhoneNumber, currency, amount, token)

            return serviceProvider.getInstance().saveCashByCodeTemp(request)
        }

        override fun saveCardOperationTemp(
            custId: Long,
            tempName: String,
            requestorCardNumber: String,
            destinationCardNumber: String,
            amount: Double,
            currency: String,
            cardOperationType: Int,
            token: String?
        ): Single<ServerResponseModel> {
            val request = SaveCardOperationTempRequestModel(
                custId,
                tempName,
                requestorCardNumber,
                destinationCardNumber,
                currency,
                cardOperationType,
                amount,
                token
            )

            return serviceProvider.getInstance().saveCardOperationTemp(request)
        }

        override fun createInLandOperationJuridical(
            custId: Long,
            compId: Long,
            token: String?,
            dtAccountId: Long,
            crIban: String,
            crCustTaxid: String,
            crCustName: String,
            crBankCode: String,
            crBankName: String,
            crBankTaxid: String,
            crBankCorrAcc: String,
            budgetCode: String,
            budgetLvl: String,
            amount: Double,
            purpose: String,
            note: String,
            operationType: Int,
            lang: EnumLangType
        ): Single<DomesticOperationResponseModel> {
            val request = CreateInLandOperationJuridicalRequestModel(
                custId,
                compId,
                token,
                dtAccountId,
                crIban,
                crCustTaxid,
                crCustName,
                crBankCode,
                crBankName,
                crBankTaxid,
                crBankCorrAcc,
                budgetCode,
                budgetLvl,
                amount,
                purpose,
                note,
                operationType,
                lang
            )
            return serviceProvider.getInstance().createInLandOperationJuridical(request)
        }

        override fun createInLandOperationIndividual(
            custId: Long,
            token: String?,
            dtAccountId: Long,
            crIban: String,
            crCustTaxid: String,
            crCustName: String,
            crBankCode: String,
            crBankName: String,
            crBankTaxid: String,
            crBankCorrAcc: String,
            budgetCode: String,
            budgetLvl: String,
            amount: Double,
            purpose: String,
            note: String,
            operationType: Int,
            lang: EnumLangType
        ): Single<DomesticOperationResponseModel> {
            val request = CreateInLandOperationIndividualRequestModel(
                custId,
                token,
                dtAccountId,
                crIban,
                crCustTaxid,
                crCustName,
                crBankCode,
                crBankName,
                crBankTaxid,
                crBankCorrAcc,
                budgetCode,
                budgetLvl,
                amount,
                purpose,
                note,
                operationType,
                lang
            )

            return serviceProvider.getInstance().createInLandOperationIndividual(request)
        }

        override fun getBudgetAccountByIban(
            custId: Long,
            token: String?,
            iban: String
        ): Single<GetBudgetAccountByIbanResponseModel> {
            val request = GetBudgetAccountByIbanRequestModel(custId, token, iban)

            return serviceProvider.getInstance().getBudgetAccountByIban(request)
        }

        override fun getCardOperationTempList(custId: Long, token: String?,lang: EnumLangType)
                : Single<GetCardOperationTempListResponseModel> {
            val request = GetCardOperationTempListRequestModel(null, custId, token,lang)

            return serviceProvider.getInstance().getCardOperationTempList(request)
        }

        override fun getCardOperationTempById(tempId: Long, custId: Long, token: String?,lang: EnumLangType)
                : Single<CardTemplateResponseModel> {
            val request = GetCardOperationTempListRequestModel(tempId, custId, token,lang)

            return serviceProvider.getInstance().getCardOperationTempById(request)
        }

        override fun cardToOtherCard(
            custId: Long,
            requestorCardId: Long,
            destinationCardNumber: String,
            currency: String,
            amount: Double?,
            token: String?,
            lang: EnumLangType
        ): Single<CardToCardResponseModel> {
            val request = CardToOtherCardRequestModel(
                custId,
                requestorCardId,
                destinationCardNumber,
                currency,
                amount,
                token,
                lang
            )
            return serviceProvider.getInstance().cardToOtherCard(request)
        }


        override fun getBudgetAccountList(
            custId: Long,
            token: String?
        ): Single<GetBudgetAccountListResponseModel> {
            val request = GetBudgetAccountListRequestModel(custId, token)
            return serviceProvider.getInstance().getBudgetAccountList(request)
        }

        override fun getBudgetLevelCode(
            custId: Long,
            token: String?
        ): Single<GetBudgetCodeResponseModel> {
            val request = GetBudgetCodeRequestModel(custId, token)
            return serviceProvider.getInstance().getBudgetLevelCode(request)
        }

        override fun getBudgetClassificationCode(
            custId: Long,
            token: String?
        ): Single<GetBudgetCodeResponseModel> {
            val request = GetBudgetCodeRequestModel(custId, token)
            return serviceProvider.getInstance().getBudgetClassificationCode(request)
        }

        override fun cashByCode(
            custId: Long,
            requestorCardId: Long,
            destinationPhoneNumber: String,
            currency: String,
            amount: Double?,
            token: String?,
            lang: EnumLangType
        ): Single<CashByCodeResponseModel> {
            val request = CashByCodeRequestModel(
                custId,
                requestorCardId,
                destinationPhoneNumber,
                currency,
                amount,
                token,
                lang
            )
            return serviceProvider.getInstance().cashByCode(request)
        }

        override fun getBankInforListIndividual(
            ccy: String,
            custId: Long,
            token: String?
        ): Single<GetBankInfoResponseModel> {
            val request = GetBankListForIndividualRequestModel(ccy, custId, token)
            return serviceProvider.getInstance().getBankListForIndividual(request)
        }

        override fun getBankInforListForJuridical(
            ccy: String,
            custId: Long,
            compId: Long,
            token: String?
        ): Single<GetBankInfoResponseModel> {
            val request = GetBankListForJuridicalRequestModel(ccy, custId, compId, token)
            return serviceProvider.getInstance().getBankListForJuridical(request)
        }

        override fun getAllOperationListIndividual(
            custId: Long,
            token: String?,
            lang: EnumLangType
        ): Single<GetAllOperationListResponseModel> {
            val reqest = GetAllOperationListIndividualRequestModel(
                custId, token, lang
            )
            return serviceProvider.getInstance().getAllOperationListIndividual(reqest)
        }

        override fun getAllOperationListJuridical(
            custId: Long,
            compId: Long,
            token: String?,
            lang: EnumLangType
        ): Single<GetAllOperationListResponseModel> {
            val reqest = GetAllOperationListJuridicalRequestModel(
                custId, compId, token, lang
            )
            return serviceProvider.getInstance().getAllOperationListJuridical(reqest)
        }

        override fun createInternalOperationJuridical(
            custId: Long,
            compId: Long,
            token: String?,
            dtAccountId: Long,
            crIban: String,
            amount: Double,
            purpose: String,
            taxNo: String?,
            lang: EnumLangType
        ): Single<DomesticOperationResponseModel> {
            val request = CreateInternalOperationJuridicalRequestModel(
                custId = custId,
                token = token,
                dtAccountId = dtAccountId,
                crIban = crIban,
                amount = amount,
                purpose = purpose,
                compId = compId,
                taxNo = taxNo,
                lang = lang
            )

            return serviceProvider.getInstance().createInternalOperationJuridical(request)
        }

        override fun getCustInfoByIban(
            custId: Long,
            token: String?,
            iban: String
        ): Single<GetCustInfoByIbanResponseModel> {
            val request = GetCustInfoByIbanRequestModel(
                custId = custId,
                token = token,
                iban = iban
            )
            return serviceProvider.getInstance().getCustInfoByIban(request)
        }

        override fun getSmsNotificationNumberForIndividualCustomer(
            custId: Long,
            token: String?,
            cardId: Long,
            lang: EnumLangType
        ): Single<GetSmsNotificationNumberResponseModel> {
            val request = GetCardByIdIndividualRequestModel(custId, cardId, token,lang)

            return serviceProvider.getInstance()
                .getSmsNotificationNumberForIndividualCustomer(request)
        }

        override fun getSmsNotificationNumberForJuridicalCustomer(
            custId: Long,
            compId: Long,
            token: String?,
            cardId: Long,
            lang: EnumLangType
        ): Single<GetSmsNotificationNumberResponseModel> {
            val request = GetCardByIdJuridicalRequestModel(custId, compId, cardId, token,lang)

            return serviceProvider.getInstance()
                .getSmsNotificationNumberForJuridicalCustomer(request)
        }

        override fun createInternalOperationIndividual(
            custId: Long,
            token: String?,
            dtAccountId: Long,
            crIban: String,
            amount: Double,
            purpose: String,
            taxNo: String?,
            lang: EnumLangType
        ): Single<DomesticOperationResponseModel> {
            val request = CreateInternalOperationIndividualRequestModel(
                custId = custId,
                token = token,
                dtAccountId = dtAccountId,
                crIban = crIban,
                amount = amount,
                purpose = purpose,
                taxNo = taxNo,
                lang = lang
            )

            return serviceProvider.getInstance().createInternalOperationIndividual(request)
        }

        override fun getCreditStatementForJuridicalCustomer(
            custId: Long,
            compId: Long,
            token: String?,
            accountId: Long,
            startDate: String,
            endDate: String
        ): Single<GetAccountStatementResponseModel> {
            val request = GetAccountStatementForJuridicalCustomerRequestModel(
                custId = custId,
                compId = compId,
                token = token,
                accountId = accountId,
                startDate = startDate,
                endDate = endDate
            )

            return serviceProvider.getInstance().getCreditStatementForJuridicalCustomer(request)
        }

        override fun getDebitStatementForJuridicalCustomer(
            custId: Long,
            compId: Long,
            token: String?,
            accountId: Long,
            startDate: String,
            endDate: String
        ): Single<GetAccountStatementResponseModel> {
            val request = GetAccountStatementForJuridicalCustomerRequestModel(
                custId = custId,
                compId = compId,
                token = token,
                accountId = accountId,
                startDate = startDate,
                endDate = endDate
            )
            return serviceProvider.getInstance().getDebitStatementForJuridicalCustomer(request)
        }

        override fun getCreditStatementForIndividualCustomer(
            custId: Long,
            token: String?,
            accountId: Long,
            startDate: String,
            endDate: String
        ): Single<GetAccountStatementResponseModel> {
            val request = GetAccountStatementForIndividualCustomerRequestModel(
                custId = custId,
                token = token,
                accountId = accountId,
                startDate = startDate,
                endDate = endDate
            )
            return serviceProvider.getInstance().getCreditStatementForIndividualCustomer(request)
        }

        override fun getDebitStatementForIndividualCustomer(
            custId: Long,
            token: String?,
            accountId: Long,
            startDate: String,
            endDate: String
        ): Single<GetAccountStatementResponseModel> {
            val request = GetAccountStatementForIndividualCustomerRequestModel(
                custId = custId,
                token = token,
                accountId = accountId,
                startDate = startDate,
                endDate = endDate
            )
            return serviceProvider.getInstance().getDebitStatementForIndividualCustomer(request)
        }

        override fun cardToCard(
            custId: Long,
            requestorCardId: Long,
            destinationCardId: Long,
            currency: String,
            amount: Double?,
            token: String?,
            lang:EnumLangType
        ): Single<CardToCardResponseModel> {
            val request = CardToCardRequestModel(
                custId,
                requestorCardId,
                destinationCardId,
                currency,
                amount,
                token,
                lang
            )

            return serviceProvider.getInstance().cardToCard(request)
        }

        override fun getIndividualCardById(
            custId: Long,
            cardId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<CardListResponseModel> {
            val request = GetCardByIdIndividualRequestModel(custId, cardId, token,lang)
            return serviceProvider.getInstance().getCardByIdForIndividual(request)
        }

        override fun getJuridicalCardById(
            custId: Long,
            compId: Long,
            cardId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<CardListResponseModel> {
            val request = GetCardByIdJuridicalRequestModel(custId, compId, cardId, token, lang)
            return serviceProvider.getInstance().getCardByIdForJuridical(request)
        }

        override fun loansForJuridicalCustomer(
            custId: Long,
            compId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<GetLoansListResponseModel> {
            val request = GetJuridicalCardListRequestModel(custId, compId, token,lang)
            return serviceProvider.getInstance().loansForJuridicalCustomer(request)
        }

        override fun juridicalDepositList(
            custId: Long,
            compId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<DepositListResponseModel> {
            val request = GetJuridicalCardListRequestModel(custId, compId, token, lang)
            return serviceProvider.getInstance().juridicalDepositList(request)
        }

        override fun getAccountListForJuridicalCustomer(
            custId: Long,
            compId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<GetAccountListResponseModel> {
            val request = GetJuridicalCardListRequestModel(custId, compId, token, lang)
            return serviceProvider.getInstance().getAccountListForJuridicalCustomer(request)
        }

    override fun getNoCardAccountListForIndividualCustomer(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<GetAccountListResponseModel> {
        val request = GetCardListIndividualRequestModel(custId, token, lang)

        return serviceProvider.getInstance().getNoCardAccountListForIndividualCustomer(request)
    }

    override fun getNoCardAccountListForJuridicalCustomer(
        custId: Long,
        compId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<GetAccountListResponseModel> {
        val request = GetJuridicalCardListRequestModel(custId, compId, token, lang)
        return serviceProvider.getInstance().getNoCardAccountListForJuridicalCustomer(request)
    }


    override fun getAccountRequisitesForIndividualCustomer(
            custId: Long,
            accountId: Long,
            token: String?
        ): Single<GetAccountRequisitesResponseModel> {
            val request =
                GetAccountRequisitesForIndividualCustomerRequestModel(
                    custId = custId,
                    accountId = accountId,
                    token = token
                )
            return serviceProvider.getInstance().getAccountRequisitesForIndividualCustomer(request)
        }

        override fun getAccountRequisitesForJuridicalCustomer(
            custId: Long,
            compId: Long,
            accountId: Long,
            token: String?
        ): Single<GetAccountRequisitesResponseModel> {
            val request =
                GetAccountRequisitesForJuridicalCustomerRequestModel(
                    custId = custId,
                    compId = compId,
                    accountId = accountId,
                    token = token
                )
            return serviceProvider.getInstance().getAccountRequisitesForJuridicalCustomer(request)
        }

        override fun getCardRequisiteForIndividualCustomer(
            custId: Long,
            cardId: Long,
            token: String?,
            lang: EnumLangType
        ): Single<GetCardRequisitesResponseModel> {
            val request = GetCardRequisitesForIndividualCustomerRequestModel(custId, cardId, token, lang)

            return serviceProvider.getInstance().getCardRequisiteForIndividualCustomer(request)
        }

        override fun getCardRequisiteForJuridicalCustomer(
            custId: Long,
            compId: Long,
            cardId: Long,
            token: String?,
            lang: EnumLangType
        ): Single<GetCardRequisitesResponseModel> {
            val request =
                GetCardRequisitesForJuridicalCustomerRequestModel(custId, compId, cardId, token, lang)

            return serviceProvider.getInstance().getCardRequisiteForJuridicalCustomer(request)
        }

        override fun addOrUpdateSmsNotificationForIndividualCustomer(
            mobile: String,
            repeatMobile: String,
            custId: Long,
            token: String?,
            cardId: Long
        ): Single<ServerResponseModel> {
            val request = EnableSmsNotificationForIndividualCustomerRequestModel(
                mobile,
                repeatMobile,
                custId,
                token,
                cardId
            )

            return serviceProvider.getInstance().enableSmsNotificationForIndividualCustomer(request)
        }

        override fun addOrUpdateSmsNotificationForJuridicalCustomer(
            mobile: String,
            repeatMobile: String,
            custId: Long,
            compId: Long,
            token: String?,
            cardId: Long
        ): Single<ServerResponseModel> {
            val request = EnableSmsNotificationForJuridicalCustomerRequestModel(
                mobile,
                repeatMobile,
                custId,
                compId,
                token,
                cardId
            )

            return serviceProvider.getInstance().enableSmsNotificationForJuridicalCustomer(request)
        }

        override fun disableSmsNotificationForIndividualCustomer(
            mobile: String,
            repeatMobile: String,
            custId: Long,
            token: String?,
            cardId: Long
        ): Single<ServerResponseModel> {
            val request = EnableSmsNotificationForIndividualCustomerRequestModel(
                mobile,
                repeatMobile,
                custId,
                token,
                cardId
            )

            return serviceProvider.getInstance()
                .disableSmsNotificationForIndividualCustomer(request)
        }

        override fun disableSmsNotificationForJuridicalCustomer(
            mobile: String,
            repeatMobile: String,
            custId: Long,
            compId: Long,
            token: String?,
            cardId: Long
        ): Single<ServerResponseModel> {
            val request = EnableSmsNotificationForJuridicalCustomerRequestModel(
                mobile,
                repeatMobile,
                custId,
                compId,
                token,
                cardId
            )

            return serviceProvider.getInstance().disableSmsNotificationForJuridicalCustomer(request)
        }

        override fun unblockCardForIndividualCustomer(
            custId: Long,
            token: String?,
            cardId: Long
        ): Single<ServerResponseModel> {
            val request = UnBlockCardForIndividualCustomerRequestModel(custId, token, cardId)

            return serviceProvider.getInstance().unBlockCardForIndividualCustomer(request)
        }

        override fun unblockCardForJuridicalCustomer(
            custId: Long,
            compId: Long,
            token: String?,
            cardId: Long
        ): Single<ServerResponseModel> {
            val request = UnBlockCardForJuridicalCustomerRequestModel(custId, compId, token, cardId)

            return serviceProvider.getInstance().unBlockCardForJuridicalCustomer(request)
        }

        override fun blockCardForIndividualCustomer(
            custId: Long,
            token: String?,
            cardId: Long,
            blockReason: String
        ): Single<ServerResponseModel> {
            val request =
                BlockCardForIndividualCustomerRequestModel(custId, token, cardId, blockReason)

            return serviceProvider.getInstance().blockCardForIndividualCustomer(request)
        }

        override fun blockCardForJuridicalCustomer(
            custId: Long,
            compId: Long,
            token: String?,
            cardId: Long,
            blockReason: String
        ): Single<ServerResponseModel> {
            val request =
                BlockCardForJuridicalCustomerRequestModel(
                    custId,
                    compId,
                    token,
                    cardId,
                    blockReason
                )

            return serviceProvider.getInstance().blockCardForJuridicalCustomer(request)
        }

        override fun getAccountStatementForJuridicalCustomer(
            custId: Long,
            compId: Long,
            token: String?,
            accountId: Long,
            startDate: String,
            endDate: String
        ): Single<GetAccountStatementResponseModel> {

            val request = GetAccountStatementForJuridicalCustomerRequestModel(
                custId = custId,
                compId = compId,
                token = token,
                accountId = accountId,
                startDate = startDate,
                endDate = endDate
            )
            return serviceProvider.getInstance().getAccountStatementForJuridicalCustomer(request)
        }

        override fun getAccountStatementForIndividualCustomer(
            custId: Long,
            token: String?,
            accountId: Long,
            startDate: String,
            endDate: String
        ): Single<GetAccountStatementResponseModel> {

            val request = GetAccountStatementForIndividualCustomerRequestModel(
                custId = custId,
                token = token,
                accountId = accountId,
                startDate = startDate,
                endDate = endDate
            )
            return serviceProvider.getInstance().getAccountStatementForIndividualCustomer(request)
        }

        override fun getBlockReasons(
            custId: Long,
            token: String?,
            lang: EnumLangType
        ): Single<BlockReasonResponseModel> {
            val request = GetCardListIndividualRequestModel(custId, token, lang)

            return serviceProvider.getInstance().getCardBlockReasons(request)
        }

        override fun individualDepositList(
            custId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<DepositListResponseModel> {
            val request = GetCardListIndividualRequestModel(custId, token, lang)

            return serviceProvider.getInstance().individualDepositList(request)
        }

        override fun loansForIndividualCustomer(
            custId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<GetLoansListResponseModel> {
            val request = GetCardListIndividualRequestModel(custId, token, lang)

            return serviceProvider.getInstance().loansForIndividualCustomer(request)
        }

        override fun getCardStatementForIndividualCustomer(
            custId: Long,
            token: String?,
            cardNumber: String?,
            startDate: String,
            endDate: String
        ): Observable<GetCardStatementResponseModel> {
            val request = GetCardStatementForIndividualCustomerRequestModel(
                custId = custId,
                token = token,
                cardNumber = cardNumber,
                endDate = endDate,
                startDate = startDate
            )
            return serviceProvider.getInstance().getCardStatementForIndividualCustomer(request)
        }

        override fun getCardStatementForJuridicalCustomer(
            custId: Long,
            compId: Long,
            token: String?,
            cardNumber: String?,
            startDate: String,
            endDate: String
        ): Observable<GetCardStatementResponseModel> {
            val request = GetCardStatementForJuridicalCustomerRequestModel(
                custId = custId,
                compId = compId,
                token = token,
                cardNumber = cardNumber,
                endDate = endDate,
                startDate = startDate
            )

            return serviceProvider.getInstance().getCardStatementForJuridicalCustomer(request)
        }

        override fun getJuridicalCardList(
            custId: Long,
            compId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<GetCardListResponseModel> {
            val request = GetJuridicalCardListRequestModel(custId, compId, token,lang)
            return serviceProvider.getInstance().getCardListForJuridicalCustomer(request)
        }

    override fun changeAccountNameIndividual(
        custId: Long,
        token: String?,
        accountId: Long,
        accountName: String
    ) : Observable<ServerResponseModel> {
        val request = ChangeAccountNameIndividualRequestModel(custId, token, accountId, accountName)
        return serviceProvider.getInstance().changeAccountNameIndividual(request)
    }

    override fun changeAccountNameJuridical(
        custId: Long,
        compId: Long?,
        token: String?,
        accountId: Long,
        accountName: String
    ) : Observable<ServerResponseModel> {
        val request = ChangeAccountNameJuridicalRequestModel(custId, compId, token, accountId, accountName)
        return serviceProvider.getInstance().changeAccountNameJuridical(request)
    }


    override fun checkAppVersion(versionName: String): Single<CheckAppVersionResponseModel> {
        return serviceProvider.getInstance().checkAppVersion(versionName)
    }

    override fun checkInternetConnection(): Single<CheckInternetServerStatusModel> {
        return serviceProvider.getInstance().checkInternet()
    }

    override fun checkToken(
            token: String?,
            custId: Long,
            compId: Long?
        ): Single<CheckTokenResponseModel> {
            val request = CheckTokenRequestModel(token, custId, compId)

            return serviceProvider.getInstance().checkToken(request)
        }

        override fun getIndividualCardList(
            custId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<GetCardListResponseModel> {
            val request = GetCardListIndividualRequestModel(custId, token, lang)

            return serviceProvider.getInstance().getCardListForIndividualCustomer(request)
        }

    override fun getCategoryListIndividual(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<GetCategoryListResponseModel> {
        val model = GetCategoryListRequestModel(token, null, custId, lang)

        return serviceProvider.getInstance().getCategoryListIndividual(model)
    }

    override fun getMerchantListIndividual(
        custId: Long,
        token: String?,
        categoryId: Long,
        lang: EnumLangType
    ): Observable<GetMerchantListResponseModel> {
        val model = GetMerchantListRequestModel(custId, null, token, categoryId, lang)

        return serviceProvider.getInstance().getMerchantListIndividual(model)
    }

    override fun getMerchantParamListIndividual(
        custId: Long,
        token: String?,
        merchantId: Long,
        lang: EnumLangType
    ): Observable<GetMerchantParamListResponseModel> {
        val model = GetMerchantParamListRequestModel(custId, null, token, merchantId, lang)

        return serviceProvider.getInstance().getMerchantParamListIndividual(model)
    }


    override fun getCategoryListJuridical(
        custId: Long,
        compId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<GetCategoryListResponseModel> {
        val model = GetCategoryListRequestModel(token, compId, custId, lang)

        return serviceProvider.getInstance().getCategoryListJuridical(model)
    }

    override fun getPaymentListJuridical(
        custId: Long,
        token: String?,
        compId: Long?,
        lang: EnumLangType
    ): Observable<GetPaymentListResponseModel> {
        val getPaymentListRequestModel = GetPaymentListRequestModel(custId, compId, token,lang)

        return serviceProvider.getInstance().paymentListJuridical(getPaymentListRequestModel)
    }

    override fun getPaymentListIndividual(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<GetPaymentListResponseModel> {
        val getPaymentListRequestModel = GetPaymentListRequestModel(custId, null, token,lang)

        return serviceProvider.getInstance().paymentListIndividual(getPaymentListRequestModel)
    }

    override fun paymentTempListIndividual(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<PaymentTemplateListResponseModel> {
        val requestModel = GetPaymentListRequestModel(custId, null, token,lang)

        return serviceProvider.getInstance().paymentTempListIndividual(requestModel)
    }

    override fun paymentTempListJuridical(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType
    ): Observable<PaymentTemplateListResponseModel> {
        val requestModel = GetPaymentListRequestModel(custId, compId, token,lang)

        return serviceProvider.getInstance().paymentTempListJuridical(requestModel)
    }

    override fun removePaymentTempIndividual(
        custId: Long,
        token: String?,
        tempId: Long
    ): Observable<ServerResponseModel> {
        val requestModel = RemovePaymentTemplateRequestModel(custId, null, token, tempId)

        return serviceProvider.getInstance().removePaymentTempIndividual(requestModel)
    }

    override fun removePaymentTempJuridical(
        custId: Long,
        compId: Long?,
        token: String?,
        tempId: Long
    ): Observable<ServerResponseModel> {
        val requestModel = RemovePaymentTemplateRequestModel(custId, compId, token, tempId)

        return serviceProvider.getInstance().removePaymentTempIndividual(requestModel)
    }

    override fun createPaymentTemplateIndividual(
        custId: Long,
        token: String?,
        tempName: String,
        merchantId: Long,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        amount: Double,
        currency: String,
        providerId: Long,
        categoryId: Long,
        lang: EnumLangType
    ): Observable<ServerResponseModel> {
        val requestModel = CreatePaymentTemplateRequestModel(custId, null, token, tempName, merchantId, identificationType, identificationCode, amount, currency, providerId, categoryId,lang)

        return serviceProvider.getInstance().createPaymentTempIndividual(requestModel)
    }

    override fun createPaymentTemplateJuridical(
        custId: Long,
        compId: Long?,
        token: String?,
        tempName: String,
        merchantId: Long,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        amount: Double,
        currency: String,
        providerId: Long,
        categoryId: Long,
        lang: EnumLangType
    ): Observable<ServerResponseModel> {
        val requestModel = CreatePaymentTemplateRequestModel(custId, compId, token, tempName, merchantId, identificationType, identificationCode, amount,currency, providerId, categoryId,lang)

        return serviceProvider.getInstance().createPaymentTempJuridical(requestModel)
    }

    override fun payIndividual(
        custId: Long,
        token: String?,
        transactionId: String?,
        categoryId: Long,
        merchantId: Long,
        providerId: Long,
        cardId: Long,
        reqPaymentDataList: ArrayList<ReqPaymentDataList>
    ): Observable<PayResponseModel> {
        val payRequestModel =
            PayRequestModel(
                custId,
                null,
                token,
                2,
                transactionId,
                categoryId,
                merchantId,
                providerId,
                cardId,
                reqPaymentDataList
            )
        return serviceProvider.getInstance().payIndividual(payRequestModel)
    }

    override fun payJuridical(
        custId: Long,
        compId: Long?,
        token: String?,
        transactionId: String?,
        categoryId: Long,
        merchantId: Long,
        providerId: Long,
        cardId: Long,
        reqPaymentDataList: ArrayList<ReqPaymentDataList>
    ): Observable<PayResponseModel> {
        val payRequestModel =
            PayRequestModel(
                custId,
                compId,
                token,
                2,
                transactionId,
                categoryId,
                merchantId,
                providerId,
                cardId,
                reqPaymentDataList
            )
        return serviceProvider.getInstance().payJuridical(payRequestModel)
    }

    override fun getMerchantListJuridical(
        custId: Long,
        compId: Long,
        token: String?,
        categoryId: Long,
        lang: EnumLangType
    ): Observable<GetMerchantListResponseModel> {
        val model = GetMerchantListRequestModel(custId, compId, token, categoryId, lang)

        return serviceProvider.getInstance().getMerchantListJuridical(model)
    }

    override fun getMerchantParamListJuridical(
        custId: Long,
        compId: Long,
        token: String?,
        merchantId: Long,
        lang: EnumLangType
    ): Observable<GetMerchantParamListResponseModel> {
        val model = GetMerchantParamListRequestModel(custId, compId, token, merchantId, lang)

        return serviceProvider.getInstance().getMerchantParamListJuridical(model)
    }

    override fun checkMerchantIndividual(
        custId: Long,
        token: String?,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        merchantId: Long,
        categoryId: Long,
        lang: EnumLangType,
        providerId: Long
    ): Observable<CheckMerchantResponseModel> {
        val model = CheckMerchantRequestModel(custId, null, token, identificationType, identificationCode, merchantId, categoryId, lang, providerId)

        return serviceProvider.getInstance().checkMerchantIndividual(model)
    }

    override fun checkMerchantJuridical(
        custId: Long,
        compId: Long,
        token: String?,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        merchantId: Long,
        categoryId: Long,
        lang: EnumLangType,
        providerId: Long
    ): Observable<CheckMerchantResponseModel> {

        val model = CheckMerchantRequestModel(custId, compId, token, identificationType, identificationCode, merchantId, categoryId, lang, providerId)

        return serviceProvider.getInstance().checkMerchantJuridical(model)
    }

    override fun getAccountListForIndividualCustomer(
            custId: Long,
            token: String?,
            lang: EnumLangType
        ): Observable<GetAccountListResponseModel> {
            val request = GetCardListIndividualRequestModel(custId, token,lang)

            return serviceProvider.getInstance().getAccountListForIndividualCustomer(request)
        }

}
