package az.turanbank.mlb.domain.user.usecase.block

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CheckUnblockIndividualUseCase @Inject constructor(
    private val authRepositoryType: AuthRepositoryType
){
    fun execute(pin: String, mobile: String,lang: EnumLangType) = authRepositoryType.checkUnblockIndividual(pin, mobile,lang)
}