package az.turanbank.mlb.domain.user.usecase.cardoperations

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CardToOtherCardUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, requestorCardId: Long, destinationCardNumber: String, currency: String, amount: Double?, token: String?,lang: EnumLangType) =
        mainRepositoryType.cardToOtherCard(custId, requestorCardId, destinationCardNumber, currency, amount, token,lang)
}