package az.turanbank.mlb.domain.user.usecase.cardoperations

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class DeleteCardOperationTempUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(tempId: Long, custId: Long, token: String?,lang: EnumLangType) =
        mainRepository.deleteCardOperationTemp(tempId, custId, token,lang)
}