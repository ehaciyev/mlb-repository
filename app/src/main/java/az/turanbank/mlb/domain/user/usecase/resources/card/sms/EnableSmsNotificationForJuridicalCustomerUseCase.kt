package az.turanbank.mlb.domain.user.usecase.resources.card.sms

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class EnableSmsNotificationForJuridicalCustomerUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
) {
    fun execute(mobile: String, repeatMobile: String, custId: Long, compId: Long, token: String?, cardId: Long) =
        mainRepositoryType.addOrUpdateSmsNotificationForJuridicalCustomer(mobile, repeatMobile, custId, compId, token, cardId)
}