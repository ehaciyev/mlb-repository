package az.turanbank.mlb.domain.user.usecase.ips.register

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class SendMobileOtpUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, compId: Long?, mobile: String, token: String?,lang: EnumLangType) =
        ipsRepository.sendOtpCode(custId, compId, mobile, token,lang)
}