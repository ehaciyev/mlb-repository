package az.turanbank.mlb.domain.user.usecase.resources.loan.pay

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class LoanPaymentIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?, payType: Int, dtAccountId: Long, loanId: Long, amount: Double) =
        mainRepository.loanPayIndividual(custId, token, payType, dtAccountId, loanId, amount)
}