package az.turanbank.mlb.domain.user.usecase.resources.account.abroad

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class UpdateAbroadByIdUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        dtAccountId: Long,
        amount: Double,
        crBankName: String,
        crBankSwift: String,
        crCustName: String,
        crCustAddress: String,
        crIban: String,
        purpose: String,
        crBankBranch: String,
        crBankAddress: String,
        crBankCountry: String,
        crBankCity: String,
        crCustPhone: String,
        note: String,
        crCorrBankName: String,
        crCorrBankCountry: String,
        crCorrBankCity: String,
        crCorrBankSwift: String,
        crCorrBankAccount: String,
        crCorrBankBranch: String,
        operId: Long,
        operNameId: Long
    ) =
        mainRepository.updateAbroadById(
            custId,
            compId,
            token,
            dtAccountId,
            amount,
            crBankName,
            crBankSwift,
            crCustName,
            crCustAddress,
            crIban,
            purpose,
            crBankBranch,
            crBankAddress,
            crBankCountry,
            crBankCity,
            crCustPhone,
            note,
            crCorrBankName,
            crCorrBankCountry,
            crCorrBankCity,
            crCorrBankSwift,
            crCorrBankAccount,
            crCorrBankBranch,
            operId,
            operNameId
        )
}
