package az.turanbank.mlb.domain.user.data

import az.turanbank.mlb.data.remote.model.ips.request.*
import az.turanbank.mlb.data.remote.model.ips.request.VerifyOTPRequestModel
import az.turanbank.mlb.data.remote.model.ips.response.*
import az.turanbank.mlb.data.remote.model.request.*
import az.turanbank.mlb.data.remote.model.resources.card.GetCardListIndividualRequestModel
import az.turanbank.mlb.data.remote.model.response.*
import az.turanbank.mlb.data.remote.model.service.IPSApiServiceProvider
import az.turanbank.mlb.data.remote.model.request.GetAccountByIdRequestModel
import az.turanbank.mlb.data.remote.model.service.GetBankInfos
import az.turanbank.mlb.data.remote.model.service.GetNotLinkedAccountListByAliasIdRequestModel
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

interface IPSRepositoryType{
    fun getAccountByIdForIndividualCustomer(compId: Long?, custId: Long, token: String?,accountId: Long, lang: EnumLangType): Observable<GetAccountIdResponseModel>
    fun registerUserIpsSystem(custId: Long, token: String?,lang: EnumLangType): Observable<ServerResponseModel>
    fun getUnregAccounts(custId: Long, token: String?,lang: EnumLangType): Observable<RegisterAccountsIpsSystemResponseModel>
    fun sendOtpCodeForEmail(custId: Long, compId: Long?, email: String, token: String?,lang: EnumLangType): Observable<SendOTPResponseModel>
    fun getIpsAccountByAlias(custId: Long, token: String?, type: String, value: String): Observable<GetAccountByAliasResponseModel>
    fun sendOtpCode(custId: Long, compId: Long?, mobile: String, token: String?,lang: EnumLangType): Observable<SendOTPResponseModel>
    fun verifyOtpCode(custId: Long, compId: Long?, token: String?,confirmCode: String): Observable<SendOTPResponseModel>
    fun verifyOtpCodeForEmail(custId: Long, compId: Long?, token: String?, confirmCode: String): Observable<SendOTPResponseModel>
    fun getDefaultCompounds(custId: Long, token: String?, lang: EnumLangType): Observable<GetDefaultCompoundsResponseModel>
    fun linkAliasToAccount(custId: Long, token: String?, aliasIdAndAccountIdList: ArrayList<AliasAndAccountRequestModel>): Observable<LinkAliasToAccountResponseModel>
    fun deleteIPSAlias(compId: Long?, custId: Long, token: String?, aliasId: ArrayList<Long>, lang: EnumLangType): Observable<DeleteAliaseServerResponseModel>
    fun getAllBanks(custId: Long, compId: Long?, token: String?,lang: EnumLangType): Observable<GetAllBanksResponseModel>
    fun checkCustomerRegistration(custId: Long, compId: Long?, token: String?, lang: EnumLangType): Observable<ServerResponseModel>
    fun checkAccountRegistration(custId: Long, compId: Long?, token: String?,lang: EnumLangType): Observable<ServerResponseModel>
    fun getMLBAccounts(custId: Long, compId: Long?, token: String?, lang: EnumLangType): Observable<GetMLBAccountsResponseModel>
    fun getMLBAliases(custId: Long, compId: Long?, token: String?,lang: EnumLangType): Observable<GetMLBAliasesResponseModel>
    fun getIPSAliases(custId: Long, compId: Long?, token: String?,lang: EnumLangType): Observable<GetIPSAliasesResponseModel>
    fun getIPSAccounts(custId: Long, compId: Long?, token: String?,lang: EnumLangType): Observable<GetIPSAccountsResponseModel>
    fun createAlias(custId: Long, token: String?, aliases: ArrayList<AliasRequestModel>): Observable<GetMLBAliasesResponseModel>
    fun registerIpsAccounts(accountId: ArrayList<Long>, custId: Long, token: String?): Observable<RegisterAccountsIpsSystemResponseModel>
    fun deleteIpsAccount(compId: Long?, custId: Long, token: String?, accountId: ArrayList<Long>, lang: EnumLangType): Observable<DeleteIPSAccountResponseModel>
    fun setDefault(compoundId: Long, custId: Long, token: String?): Observable<ServerResponseModel>
    fun getBudgetLevel(custId: Long, compId: Long?, token: String?): Observable<GetBankInfos>
    fun getBudgetCode(custId: Long, compId: Long?, token: String?): Observable<GetBankInfos>
    fun getCompounds(custId: Long, token: String?,lang: EnumLangType): Observable<GetDefaultCompoundsResponseModel>
    fun getNotLinkedAccountListByAliasId(custId: Long,compId: Long?,aliasId: Long,token: String?,lang:EnumLangType): Observable<GetMLBAccountsResponseModel>
    fun getAccountsByAliasId(custId: Long,compId: Long?,aliasId: Long,token: String?,lang: EnumLangType): Observable<GetMLBLinkAccountsResponseModel>
    fun getIpsAuthMethods(custId: Long, compId: Long?, token: String?,lang: EnumLangType): Observable<GetIpsAuthMethodResponseModel>
    fun createAccountToAccountIpsOperation(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType,
        receiverBankId: Long,
        ipsPaymentTypeId:Int,
        branchId: Int,
        branchName: String,
        paymentAmount: Double,
        senderCustomerName: String,
        receiverCustomerName: String,
        senderCustomerIban: String,
        receiverCustomerIban: String,
        additionalInfoAboutPayment: String,
        additionalInfFromCustomer: String,
        remittanceInfo: String,
        budgetLvl: String,
        budgetCode: String
    ): Observable<CreateIpsOperationResponseModel>

    fun createAccountToAliasIpsOperation(
        custId: Long,
        compId: Long?,
        token: String?,
        receiverBankId: Long,
        senderAccountId: Long,
        receiverAliasTypeId: Long,
        receiverAlias: String?,
        paymentAmount: Long,
        receiverCustomerName: String,
        additionalInfoAboutPayment: String,
        additionalInfoFromCustomer: String,
        remittanceInfo: String
    ): Observable<CreateIpsOperationResponseModel>

    fun createAliasToAliasIpsOperation(
        custId: Long,
        compId: Long?,
        token: String?,
        receiverBankId: Long,
        senderAliasId: Long,
        receiverAliasTypeId: Long,
        receiverAlias: String?,
        paymentAmount: Long,
        receiverCustomerName: String,
        additionalInfoAboutPayment: String,
        additionalInfoFromCustomer: String,
        remittanceInfo: String
    ): Observable<CreateIpsOperationResponseModel>

    fun createAliasToAccountIpsOperation(
        custId: Long,
        compId: Long?,
        token: String?,
        receiverBankId: Long,
        senderAliasId: Long,
        businessProcessId: Long,
        paymentAmount: Long,
        receiverCustomerName: String,
        receiverCustomerAccount: String,
        additionalInfoAboutPayment: String,
        additionalInfoFromCustomer: String,
        remittanceInfo: String,
        receiverTaxid: String,
        budgetLvl: String,
        budgetCode: String
    ): Observable<CreateIpsOperationResponseModel>

    fun getBudgetAccountByIbanIPS(custId: Long, token: String?, iban: String): Single<GetBudgetAccountByIbanResponseModel>


    fun getAccountAndCustomerInfo(
        iban: String,
        servicerBIC: String?,
        servicerMemberId: String?,
        swiftCode: String?,
        testSwiftCode: String?,
        custId: Long,
        token: String?
    ): Observable<GetAccountAndCustomerInfoResponseModel>

    fun getAccountAndCustomerInfoByAlias(
        custId: Long,
        token: String?,
        aliasTypeId: EnumAliasType,
        value: String
    ): Observable<GetAccountAndCustomerInfoByAliasResponseModel>

    fun getAuthAccount(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<RegisterAccountsIpsSystemResponseModel>

    fun operationAdvancedSearch(
        custId: Long,
        compId: Long?,
        token: String?,
        operNameId: Int?,
        lang: EnumLangType
    ): Observable<GetAllOperationListResponseModel>

    fun receivePaymentListIndividual(
        custId: Long,
        compId: Long?,
        token: String?
    ): Observable<ReceivePaymentListIndividualResponseModel>

}
class IPSRepository @Inject constructor(
    private val serviceProvider: IPSApiServiceProvider
): IPSRepositoryType{
    override fun createAliasToAccountIpsOperation(
        custId: Long,
        compId: Long?,
        token: String?,
        receiverBankId: Long,
        senderAliasId: Long,
        businessProcessId: Long,
        paymentAmount: Long,
        receiverCustomerName: String,
        receiverCustomerAccount: String,
        additionalInfoAboutPayment: String,
        additionalInfoFromCustomer: String,
        remittanceInfo: String,
        receiverTaxid: String,
        budgetLvl:String,
        budgetCode:String
    ): Observable<CreateIpsOperationResponseModel> {
        val ipsModel = IpsPaymentModelAliasToAccount(
            paymentAmount,
            receiverCustomerName,
            receiverCustomerAccount,
            additionalInfoAboutPayment,
            additionalInfoFromCustomer,
            remittanceInfo,
            receiverTaxid,
            budgetLvl,
            budgetCode
        )

        val requestModel = CreateAliasToAccountIpsOperationRequestModel(
            custId,compId,token, receiverBankId, senderAliasId, businessProcessId, ipsPayment = ipsModel
        )
        return serviceProvider.getInstance().createIpsOperation(requestModel)
    }

    override fun getBudgetAccountByIbanIPS(
        custId: Long,
        token: String?,
        iban: String
    ): Single<GetBudgetAccountByIbanResponseModel> {
        val request = GetBudgetAccountByIbanRequestModel(custId, token, iban)

        return serviceProvider.getInstance().getBudgetAccountByIbanIPS(request)
    }

    override fun getAccountAndCustomerInfo(
        iban: String,
        servicerBIC: String?,
        servicerMemberId: String?,
        swiftCode: String?,
        testSwiftCode: String?,
        custId: Long,
        token: String?
    ): Observable<GetAccountAndCustomerInfoResponseModel> {
        val model = GetAccountAndCustomerInfoRequestModel(iban,servicerBIC,servicerMemberId,swiftCode, testSwiftCode,custId,token)
        return serviceProvider.getInstance().getAccountAndCustomerInfo(model)
    }

    override fun getAccountAndCustomerInfoByAlias(
        custId: Long,
        token: String?,
        aliasTypeId: EnumAliasType,
        value: String
    ): Observable<GetAccountAndCustomerInfoByAliasResponseModel> {
        val model = GetAccountAndCustomerInfoByAliasRequestModel(custId, token, aliasTypeId, value)
        return serviceProvider.getInstance().getAccountAndCustomerInfoByAlias(model)
    }


    override fun getAuthAccount(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<RegisterAccountsIpsSystemResponseModel> {
        val model = GetCardListIndividualRequestModel(custId, token,lang)
        return serviceProvider.getInstance().getAuthAccounts(model)
    }

    override fun operationAdvancedSearch(
        custId: Long,
        compId: Long?,
        token: String?,
        operNameId: Int?,
        lang: EnumLangType
    ): Observable<GetAllOperationListResponseModel> {
        val requestModel = OperationAdvancedSearchRequestModel(custId, compId, token, null, null, null, null, null, null, operNameId,lang)

        return serviceProvider.getInstance().operationAdvancedSearch(requestModel)
    }

    override fun receivePaymentListIndividual(
        custId: Long,
        compId: Long?,
        token: String?
    ): Observable<ReceivePaymentListIndividualResponseModel> {
        val requestModel = ReceivePaymentsRequestModel(custId, compId, token)

        return serviceProvider.getInstance().receivePaymentListIndividual(requestModel)
    }

    override fun createAliasToAliasIpsOperation(
        custId: Long,
        compId: Long?,
        token: String?,
        receiverBankId: Long,
        senderAliasId: Long,
        receiverAliasTypeId: Long,
        receiverAlias: String?,
        paymentAmount: Long,
        receiverCustomerName: String,
        additionalInfoAboutPayment: String,
        additionalInfoFromCustomer: String,
        remittanceInfo: String
    ): Observable<CreateIpsOperationResponseModel> {
        val ipsModel = IpsPaymentModelWithAlias(
            paymentAmount,
            receiverCustomerName,
            additionalInfoAboutPayment,
            additionalInfoFromCustomer,
            remittanceInfo
        )

        var requestModel = CreateAliasToAliasIpsOperationRequestModel(
            custId,compId,token,receiverBankId, senderAliasId, receiverAliasTypeId,receiverAlias, ipsPayment = ipsModel
        )

        return serviceProvider.getInstance().createIpsOperation(requestModel)}

    override fun createAccountToAliasIpsOperation(
        custId: Long,
        compId: Long?,
        token: String?,
        receiverBankId: Long,
        senderAccountId: Long,
        receiverAliasTypeId: Long,
        receiverAlias: String?,
        paymentAmount: Long,
        receiverCustomerName: String,
        additionalInfoAboutPayment: String,
        additionalInfoFromCustomer: String,
        remittanceInfo: String
    ): Observable<CreateIpsOperationResponseModel> {
        val ipsModel = IpsPaymentModelWithAlias(
            paymentAmount,
            receiverCustomerName,
            additionalInfoAboutPayment,
            additionalInfoFromCustomer,
            remittanceInfo
        )

        var requestModel = CreateAccountToAliasIpsOperationRequestModel(
            custId,compId,token,receiverBankId, senderAccountId, receiverAliasTypeId,receiverAlias, ipsPayment = ipsModel
        )

        return serviceProvider.getInstance().createIpsOperation(requestModel)
    }

    override fun registerUserIpsSystem(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<ServerResponseModel> {
        val model = GetCardListIndividualRequestModel(custId, token, lang)

        return serviceProvider.getInstance().registerIndividualCustomer(model)
    }

    override fun getUnregAccounts(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<RegisterAccountsIpsSystemResponseModel> {
        val model = GetCardListIndividualRequestModel(custId, token, lang)

        return serviceProvider.getInstance().getUnregAccounts(model)
    }

    override fun sendOtpCodeForEmail(
        custId: Long,
        compId: Long?,
        email: String,
        token: String?,
        lang: EnumLangType
    ): Observable<SendOTPResponseModel> {
        val model = SendOTPForEmailRequestModel(custId, compId, email, token,lang)

        return serviceProvider.getInstance().sendOtpCodeForEmail(model)
    }

    override fun getIpsAccountByAlias(
        custId: Long,
        token: String?,
        type: String,
        value: String
    ): Observable<GetAccountByAliasResponseModel> {
        val model = GetAccountByAliasRequestModel(custId, token, type, value)

        return serviceProvider.getInstance().getIpsAccountByAlias(model)
    }

    override fun sendOtpCode(
        custId: Long,
        compId: Long?,
        mobile: String,
        token: String?,
        lang: EnumLangType
    ): Observable<SendOTPResponseModel> {
        val model = SendOTPRequestModel(custId, compId, mobile, token,lang)

        return serviceProvider.getInstance().sendOtpCode(model)
    }

    override fun verifyOtpCode(
        custId: Long,
        compId: Long?,
        token: String?,
        confirmCode: String
    ): Observable<SendOTPResponseModel> {
        val model = VerifyOTPRequestModel(custId, compId, token,confirmCode)

        return serviceProvider.getInstance().verifyOtpCode(model)
    }

    override fun verifyOtpCodeForEmail(
        custId: Long,
        compId: Long?,
        token: String?,
        confirmCode: String
    ): Observable<SendOTPResponseModel> {
        val model = VerifyOTPRequestModel(custId, compId, token,confirmCode)

        return serviceProvider.getInstance().verifyOtpCodeForEmail(model)
    }

    override fun getDefaultCompounds(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<GetDefaultCompoundsResponseModel> {
        val model = GetCardListIndividualRequestModel(custId, token, lang)

        return serviceProvider.getInstance().getDefaultCompounds(model)
    }

    override fun linkAliasToAccount(
        custId: Long,
        token: String?,
        aliasIdAndAccountIdList: ArrayList<AliasAndAccountRequestModel>
    ): Observable<LinkAliasToAccountResponseModel> {
        val model = LinkAliasToAccountRequestModel(custId, token, aliasIdAndAccountIdList)

        return serviceProvider.getInstance().linkAliasToAccount(model)
    }

    override fun deleteIPSAlias(
        compId: Long?,
        custId: Long,
        token: String?,
        aliasId: ArrayList<Long>,
        lang: EnumLangType
    ): Observable<DeleteAliaseServerResponseModel> {
        val model =
            DeleteIPSAliasRequestModel(
                compId,
                custId,
                token,
                aliasId,
                lang
            )

        return serviceProvider.getInstance().deleteIpsAlias(model)
    }

    override fun getAllBanks(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType
    ): Observable<GetAllBanksResponseModel> {
        val model = GetJuridicalCardListRequestModel(custId, compId, token,lang)

        return serviceProvider.getInstance().getAllBanks(model)
    }

    override fun checkCustomerRegistration(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType
    ): Observable<ServerResponseModel> {
        val model = GetJuridicalCardListRequestModel(custId, compId, token,lang)

        return serviceProvider.getInstance().checkCustomerRegistration(model)
    }

    override fun checkAccountRegistration(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType
    ): Observable<ServerResponseModel> {
        val model = GetJuridicalCardListRequestModel(custId, compId, token, lang)

        return serviceProvider.getInstance().checkAccountRegistration(model)
    }

    override fun getMLBAccounts(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType
    ): Observable<GetMLBAccountsResponseModel> {
        val model = GetJuridicalCardListRequestModel(custId, compId, token,lang)
        return serviceProvider.getInstance().getMLBAccounts(model)
    }

    override fun getMLBAliases(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType
    ): Observable<GetMLBAliasesResponseModel> {
        val model = GetJuridicalCardListRequestModel(custId, compId, token,lang)

        return serviceProvider.getInstance().getMLBAliases(model)
    }

    override fun getIPSAliases(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType
    ): Observable<GetIPSAliasesResponseModel> {
        val requestModel = GetJuridicalCardListRequestModel(custId, compId, token,lang)

        return serviceProvider.getInstance().getIpsAliases(requestModel)
    }

    override fun getIPSAccounts(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType
    ): Observable<GetIPSAccountsResponseModel> {
        val requestModel = GetJuridicalCardListRequestModel(custId, compId, token, lang)

        return serviceProvider.getInstance().getIpsAccounts(requestModel)
    }

    override fun createAlias(
        custId: Long,
        token: String?,
        aliases: ArrayList<AliasRequestModel>
    ): Observable<GetMLBAliasesResponseModel> {
        val model = CreateAliasRequestModel(custId, token, aliases)

        return serviceProvider.getInstance().createAlias(model)
    }

    override fun registerIpsAccounts(
        accountId: ArrayList<Long>,
        custId: Long,
        token: String?
    ): Observable<RegisterAccountsIpsSystemResponseModel> {
        val registerAccountIpsSystemRequestModel = RegisterAccountIpsSystemRequestModel(accountId, custId, token)

        return serviceProvider.getInstance().registerIpsAccounts(registerAccountIpsSystemRequestModel)
    }

    override fun deleteIpsAccount(
        compId: Long?,
        custId: Long,
        token: String?,
        accountId: ArrayList<Long>,
        lang: EnumLangType
    ): Observable<DeleteIPSAccountResponseModel> {
        val requestModel = GetAccountByIdRequestModel(compId, custId, token, accountId, lang)

        return serviceProvider.getInstance().deleteIpsAccount(requestModel)
    }

    override fun setDefault(
        compoundId: Long,
        custId: Long,
        token: String?
    ): Observable<ServerResponseModel> {
        val setDefaultAccountRequestModel = SetDefaultAccountRequestModel(custId, token, compoundId)

        return serviceProvider.getInstance().setDefault(setDefaultAccountRequestModel)
    }

    override fun getBudgetLevel(
        custId: Long,
        compId: Long?,
        token: String?
    ): Observable<GetBankInfos> {
        val req = GetBudgetCode(custId, compId, token)
        return serviceProvider.getInstance().getBudgetLvlList(req)
    }

    override fun getBudgetCode(
        custId: Long,
        compId: Long?,
        token: String?
    ): Observable<GetBankInfos> {
        val req = GetBudgetCode(custId, compId, token)
        return serviceProvider.getInstance().getBudgetCodeList(req)
    }

    override fun getCompounds(
        custId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<GetDefaultCompoundsResponseModel> {
        val requestModel = GetCardListIndividualRequestModel(custId, token, lang)

        return serviceProvider.getInstance().getCompounds(requestModel)
    }

    override fun getNotLinkedAccountListByAliasId(
        custId: Long,
        compId: Long?,
        aliasId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<GetMLBAccountsResponseModel> {
        val requestModel = GetNotLinkedAccountListByAliasIdRequestModel(custId,compId,aliasId,token,lang)
        return serviceProvider.getInstance().getNotLinkedAccountListByAliasId(requestModel)
    }

    override fun getAccountsByAliasId(
        custId: Long,
        compId: Long?,
        aliasId: Long,
        token: String?,
        lang: EnumLangType
    ): Observable<GetMLBLinkAccountsResponseModel> {
        val requestModel = GetNotLinkedAccountListByAliasIdRequestModel(custId,compId,aliasId,token,lang)
        return serviceProvider.getInstance().getAccountsByAliasId(requestModel)
    }

    override fun getIpsAuthMethods(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType
    ): Observable<GetIpsAuthMethodResponseModel> {
        val requestModel = GetIpsAuthMethodRequestModel(custId,compId,token,lang)
        return serviceProvider.getInstance().getIpsAuthMethods(requestModel)
    }

    override fun createAccountToAccountIpsOperation(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType,
        receiverBankId: Long,
        ipsPaymentTypeId:Int,
        branchId: Int,
        branchName: String,
        paymentAmount: Double,
        senderCustomerName: String,
        receiverCustomerName: String,
        senderCustomerIban: String,
        receiverCustomerIban: String,
        additionalInfoAboutPayment: String,
        additionalInfFromCustomer: String,
        remittanceInfo: String,
        budgetLvl: String,
        budgetCode: String
    ): Observable<CreateIpsOperationResponseModel> {
        val ipsModel = IpsPaymentModelAccountToAccount(
            paymentAmount,
            senderCustomerName,
            receiverCustomerName,
            senderCustomerIban,
            receiverCustomerIban,
            additionalInfoAboutPayment,
            additionalInfFromCustomer,
            remittanceInfo,
            budgetLvl,
            budgetCode
        )
        val requestModel = CreateAccountToAccountIpsOperationRequestModel(
            custId,compId,token, lang, receiverBankId, ipsPaymentTypeId, branchId, branchName, ipsPayment = ipsModel
        )
        return serviceProvider.getInstance().createIpsOperation(requestModel)
    }

    override fun getAccountByIdForIndividualCustomer(
        compId: Long?,
        custId: Long,
        token: String?,
        accountId: Long,
        lang: EnumLangType
    ): Observable<GetAccountIdResponseModel> {
        val model = GetAccountByIdRequestModel2(compId, custId, token, accountId, lang)

        return serviceProvider.getInstance().getAccountByIdForIndividualCustomer(model)
    }
}