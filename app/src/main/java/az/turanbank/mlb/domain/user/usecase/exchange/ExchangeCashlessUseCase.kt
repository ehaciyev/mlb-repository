package az.turanbank.mlb.domain.user.usecase.exchange

import az.turanbank.mlb.domain.user.data.ExchangeRepositoryType
import javax.inject.Inject

class ExchangeCashlessUseCase @Inject constructor(
    private val exchangeRepository: ExchangeRepositoryType
){
    fun execute(exchangeDate: String?) = exchangeRepository.cashlessExchange(exchangeDate)
}