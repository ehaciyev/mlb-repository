package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetBankListForIndividualUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(ccy: String, custId: Long, token: String?) =
        repository.getBankInforListIndividual(ccy, custId, token)
}