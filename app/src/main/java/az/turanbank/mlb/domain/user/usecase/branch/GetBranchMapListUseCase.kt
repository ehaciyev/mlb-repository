package az.turanbank.mlb.domain.user.usecase.branch

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetBranchMapListUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(lang: EnumLangType) = mainRepository.getBranchMapList(lang)
}