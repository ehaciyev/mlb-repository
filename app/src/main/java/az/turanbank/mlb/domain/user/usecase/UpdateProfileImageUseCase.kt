package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class UpdateProfileImageUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long,
                token: String?,
                compId: Long?,
                bytesStr: String) =
        repository.updateProfileImage(custId, token, compId, bytesStr)
}