package az.turanbank.mlb.domain.user.usecase.orders

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetBranchListUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, token: String?,lang: EnumLangType) =
        repository.getBranchList(custId, token,lang)
}