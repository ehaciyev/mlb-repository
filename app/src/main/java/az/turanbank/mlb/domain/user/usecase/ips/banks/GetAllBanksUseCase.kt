package az.turanbank.mlb.domain.user.usecase.ips.banks

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetAllBanksUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, compId: Long?, token: String?,lang: EnumLangType) =
        ipsRepository.getAllBanks(custId, compId, token,lang)
}
