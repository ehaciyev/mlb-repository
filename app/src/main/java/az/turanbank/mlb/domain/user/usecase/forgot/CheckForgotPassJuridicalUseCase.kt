package az.turanbank.mlb.domain.user.usecase.forgot

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CheckForgotPassJuridicalUseCase @Inject constructor(
    private val authRepositoryType: AuthRepositoryType
){
    fun execute(pin: String, mobile: String, taxNo: String,lang: EnumLangType) = authRepositoryType.checkForgotPassJuridical(pin, mobile, taxNo,lang)
}