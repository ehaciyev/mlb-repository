package az.turanbank.mlb.domain.user.usecase.pg.pay

import az.turanbank.mlb.data.remote.model.pg.request.ReqPaymentDataList
import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class PayIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        token: String?,
        transactionId: String?,
        categoryId: Long,
        merchantId: Long,
        providerId: Long,
        cardId: Long,
        reqPaymentDataList: ArrayList<ReqPaymentDataList>
    ) = mainRepository.payIndividual(
        custId,
        token,
        transactionId,
        categoryId,
        merchantId,
        providerId,
        cardId,
        reqPaymentDataList
    )
}