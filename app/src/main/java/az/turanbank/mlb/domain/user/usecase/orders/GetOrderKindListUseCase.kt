package az.turanbank.mlb.domain.user.usecase.orders

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetOrderKindListUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, token: String?, orderType: Int?, kindType: Long?,lang: EnumLangType) =
        repository.getOrderKindList(custId, token, orderType, kindType,lang)
}