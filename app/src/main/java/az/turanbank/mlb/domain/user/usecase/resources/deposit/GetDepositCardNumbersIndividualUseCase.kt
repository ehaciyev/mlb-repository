package az.turanbank.mlb.domain.user.usecase.resources.deposit

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetDepositCardNumbersIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, depositId: Long, token: String?,lang:EnumLangType) =
        mainRepository.getCardNumbersByDepositIdForIndividual(custId, depositId, token,lang)
}