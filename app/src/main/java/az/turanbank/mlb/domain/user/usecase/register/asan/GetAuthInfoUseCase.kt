package az.turanbank.mlb.domain.user.register.usecase

import az.turanbank.mlb.domain.user.data.AuthRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetAuthInfoUseCase @Inject constructor(
    private val repository: AuthRepository
) {
    fun execute(phoneNumber: String, userId: String, lang: EnumLangType) =
        repository.getAuthInfo(phoneNumber, userId,lang)
}