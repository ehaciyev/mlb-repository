package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetSecondAuthOperationListUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, compId: Long, token: String?,lang: EnumLangType) =
        repository.getSecondAuthOperationList(custId, compId, token,lang)
}