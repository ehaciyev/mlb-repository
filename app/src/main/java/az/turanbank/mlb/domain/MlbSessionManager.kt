package az.turanbank.mlb.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import az.turanbank.mlb.data.remote.model.response.CustomerResponseModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MlbSessionManager @Inject constructor() {

    private val cachedUser: MediatorLiveData<CustomerResponseModel> = MediatorLiveData()

    fun authenticateUser(customerResponseModel: LiveData<CustomerResponseModel>) {
        cachedUser.value = null
        cachedUser.addSource(customerResponseModel) {
            cachedUser.value = it
            cachedUser.removeSource(customerResponseModel)
        }
    }

    fun getAuthUser() = cachedUser
}