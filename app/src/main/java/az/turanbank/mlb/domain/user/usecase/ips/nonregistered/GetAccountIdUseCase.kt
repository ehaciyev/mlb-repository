package az.turanbank.mlb.domain.user.usecase.ips.nonregistered

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetAccountIdUseCase @Inject constructor(
    val ipsRepository: IPSRepository
) {
    fun execute(compId: Long?, custId: Long, token: String?,accountId: Long,lang: EnumLangType) =
        ipsRepository.getAccountByIdForIndividualCustomer(compId, custId, token, accountId,lang)
}
