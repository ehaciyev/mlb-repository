package az.turanbank.mlb.domain.user.usecase.login

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class JuridicalLoginUsernamePasswordUseCase @Inject constructor(
    private val repositoryType: AuthRepositoryType
){
    fun execute(username: String, password: String) = repositoryType.loginUsernamePasswordJuridical(username, password)
}