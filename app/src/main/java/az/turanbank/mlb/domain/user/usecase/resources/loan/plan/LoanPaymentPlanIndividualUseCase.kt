package az.turanbank.mlb.domain.user.usecase.resources.loan.plan

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class LoanPaymentPlanIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, loanId: Long, token: String?) =
        mainRepository.loanPaymentPlanIndividual(custId, loanId, token)
}