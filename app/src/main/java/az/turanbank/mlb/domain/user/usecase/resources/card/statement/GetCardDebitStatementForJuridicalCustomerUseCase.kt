package az.turanbank.mlb.domain.user.usecase.resources.card.statement

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetCardDebitStatementForJuridicalCustomerUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, compId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String) =
        mainRepository.getCardDebitStatementForJuridicalCustomer(custId, compId, token, cardNumber, startDate, endDate)
}