package az.turanbank.mlb.domain.user.usecase.register.mobile

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CheckJuridicalCustomerUseCase @Inject constructor(
    private val repositoryType: AuthRepositoryType
){
    fun execute(pin: String, mobile: String, taxNo: String,lang: EnumLangType) = repositoryType.checkJuridicalCustomer(pin, mobile, taxNo,lang)
}