package az.turanbank.mlb.domain.user.usecase.resources.loan.plan

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class LoanPaymentPlanJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, compId: Long, loanId: Long, token: String?) =
        mainRepository.loanPaymentPlanJuridical(custId, loanId, compId, token)
}