package az.turanbank.mlb.domain.user.usecase.resources.card.blocking

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class UnblockCardForIndividualCustomerUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, token: String?, cardId: Long) =
        mainRepositoryType.unblockCardForIndividualCustomer(custId, token, cardId)
}