package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetOperationByIdUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(operId: Long, custId: Long, token: String?) = repository.getOperationById(custId, token, operId)
}