package az.turanbank.mlb.domain.user.usecase.resources.card.sms

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetSmsNotificationNumberForJuridicalCustomerUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, compId: Long, cardId: Long, token: String?,lang: EnumLangType) =
        mainRepositoryType.getSmsNotificationNumberForJuridicalCustomer(custId, compId, token, cardId,lang)
}