package az.turanbank.mlb.domain.user.usecase.ips.pay

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CreateAccountToAccountIpsOperationUseCase @Inject constructor(private val ipsRepository: IPSRepository) {

    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        lang: EnumLangType,
        receiverBankId: Long,
        ipsPaymentTypeId:Int,
        branchId: Int,
        branchName: String,
        paymentAmount: Double,
        senderCustomerName: String,
        receiverCustomerName: String,
        senderCustomerIban: String,
        receiverCustomerIban: String,
        additionalInfoAboutPayment: String,
        additionalInfFromCustomer: String,
        remittanceInfo: String,
        budgetLvl: String,
        budgetCode: String
    ) = ipsRepository.createAccountToAccountIpsOperation(
        custId,
        compId,
        token,
        lang,
        receiverBankId,
        ipsPaymentTypeId,
        branchId,
        branchName,
        paymentAmount,
        senderCustomerName,
        receiverCustomerName,
        senderCustomerIban,
        receiverCustomerIban,
        additionalInfoAboutPayment,
        additionalInfFromCustomer,
        remittanceInfo,
        budgetLvl,
        budgetCode
    )
}