package az.turanbank.mlb.domain.user.usecase.block

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class BlockJuridicalCustomerUseCase  @Inject constructor(
    private val authRepositoryType: AuthRepositoryType
){
    fun execute(custId: Long, compId: Long) = authRepositoryType.blockJuridicalCustomer(custId, compId)
}