package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class RegisterIndividualAsanUseCase @Inject constructor(
    private val repository: AuthRepository
) {
    fun execute(username: String, password: String, repeatPassword: String, custId: Long) =
        repository.registerIndividualAsan(username, password, repeatPassword, custId)
}