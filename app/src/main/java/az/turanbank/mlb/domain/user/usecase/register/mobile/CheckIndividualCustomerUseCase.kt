package az.turanbank.mlb.domain.user.usecase.register.mobile

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CheckIndividualCustomerUseCase @Inject constructor(
    private val repository: AuthRepositoryType
) {
    fun execute(pin: String, mobile: String, lang: EnumLangType) = repository.checkIndividualCustomer(pin, mobile,lang)
}