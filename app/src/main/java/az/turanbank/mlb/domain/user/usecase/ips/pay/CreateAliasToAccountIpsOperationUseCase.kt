package az.turanbank.mlb.domain.user.usecase.ips.pay

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class CreateAliasToAccountIpsOperationUseCase @Inject constructor(private val ipsRepository: IPSRepository){
    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        receiverBankId: Long,
        senderAliasId: Long,
        businessProcessId: Long,
        paymentAmount: Long,
        receiverCustomerName: String,
        receiverCustomerAccount: String,
        additionalInfoAboutPayment: String,
        additionalInfoFromCustomer: String,
        remittanceInfo: String,
        receiverTaxid: String,
        budgetLvl: String,
        budgetCode: String
    ) = ipsRepository.createAliasToAccountIpsOperation(
        custId,
        compId,
        token,
        receiverBankId,
        senderAliasId,
        businessProcessId,
        paymentAmount,
        receiverCustomerName,
        receiverCustomerAccount,
        additionalInfoAboutPayment,
        additionalInfoFromCustomer,
        remittanceInfo,
        receiverTaxid,
        budgetLvl,
        budgetCode
    )
}