package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetBudgetAccountByIbanIPSUseCase @Inject constructor(
    private val repository: IPSRepository
) {
    fun execute(custId: Long, token: String?, iban: String) =
        repository.getBudgetAccountByIbanIPS(custId, token, iban)
}