package az.turanbank.mlb.domain.user.usecase.cardoperations

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CashByCodeUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, requestorCardId: Long, destinationPhoneNumber: String, currency: String, amount: Double?, token: String?,lang: EnumLangType) =
        mainRepositoryType.cashByCode(custId, requestorCardId, destinationPhoneNumber, currency, amount, token,lang)
}