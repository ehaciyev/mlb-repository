package az.turanbank.mlb.domain.user.usecase.pg.category

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetCategoryListJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(custId: Long, compId: Long, token: String?, lang: EnumLangType) =
        mainRepository.getCategoryListJuridical(custId, compId, token, lang)
}