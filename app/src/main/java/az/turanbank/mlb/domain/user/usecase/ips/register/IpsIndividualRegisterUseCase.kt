package az.turanbank.mlb.domain.user.usecase.ips.register

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class IpsIndividualRegisterUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, token: String?,lang: EnumLangType) =
        ipsRepository.registerUserIpsSystem(custId, token,lang)
}