package az.turanbank.mlb.domain.user.usecase.resources.deposit

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetJuridicalDepositListUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, compId: Long, token: String?,lang: EnumLangType) = mainRepositoryType.juridicalDepositList(custId, compId, token,lang)
}