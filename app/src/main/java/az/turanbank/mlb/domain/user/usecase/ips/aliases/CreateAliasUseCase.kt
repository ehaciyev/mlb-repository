package az.turanbank.mlb.domain.user.usecase.ips.aliases

import az.turanbank.mlb.data.remote.model.ips.request.AliasRequestModel
import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class CreateAliasUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
){
    fun execute(custId: Long, token: String?, aliases: ArrayList<AliasRequestModel>) =
        ipsRepository.createAlias(custId, token, aliases)
}