package az.turanbank.mlb.domain.user.usecase.resources

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetAccountRequisitesForJuridicalCustomerUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, compId: Long, accountId: Long, token: String?) =
        repository.getAccountRequisitesForJuridicalCustomer(
            custId = custId,
            compId = compId,
            accountId = accountId,
            token = token
        )
}