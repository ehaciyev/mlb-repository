package az.turanbank.mlb.domain.user.usecase.pg.check

import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CheckMerchantJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long,
        token: String?,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        merchantId: Long,
        categoryId: Long,
        lang: EnumLangType,
        providerId: Long
    ) = mainRepository.checkMerchantJuridical(
        custId,
        compId,
        token,
        identificationType,
        identificationCode,
        merchantId,
        categoryId,
        lang,
        providerId
    )
}