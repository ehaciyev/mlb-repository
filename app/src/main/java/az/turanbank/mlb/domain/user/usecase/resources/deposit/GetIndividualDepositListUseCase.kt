package az.turanbank.mlb.domain.user.usecase.resources.deposit

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetIndividualDepositListUseCase @Inject constructor(
        private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, token: String?,lang: EnumLangType) = mainRepositoryType.individualDepositList(custId, token,lang)
}