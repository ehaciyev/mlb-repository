package az.turanbank.mlb.domain.user.usecase.resources.card.requisite

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetCardRequisiteForIndividualCustomerUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, cardId: Long, token: String?,lang: EnumLangType) =
        mainRepositoryType.getCardRequisiteForIndividualCustomer(custId, cardId, token,lang)
}