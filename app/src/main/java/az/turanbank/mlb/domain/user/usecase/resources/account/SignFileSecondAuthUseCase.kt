package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class SignFileSecondAuthUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, compId: Long, token: String?, operationIds: ArrayList<Long>, transactionId: Long, phoneNumber: String?,
                userId: String?, lang: EnumLangType, certCode: String?,successOperId: ArrayList<Long>,
                failOperId: ArrayList<Long>) =
        repository.signFileSecondAuth(custId, compId, token, operationIds, transactionId, phoneNumber, userId, lang, certCode,successOperId,failOperId)
}