package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class CheckPinForAsanRegisterUseCase @Inject constructor(private val repository: AuthRepository) {
    fun execute(pin: String) = repository.checkPinForAsanRegister(pin)
}