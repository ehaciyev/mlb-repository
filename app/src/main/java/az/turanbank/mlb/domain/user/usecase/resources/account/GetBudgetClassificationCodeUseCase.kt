package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetBudgetClassificationCodeUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, token: String?) =
        repository.getBudgetClassificationCode(custId, token)
}