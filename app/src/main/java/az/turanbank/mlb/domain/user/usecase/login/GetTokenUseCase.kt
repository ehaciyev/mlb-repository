package az.turanbank.mlb.domain.user.usecase.login

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class GetTokenUseCase @Inject constructor(
    private val authRepositoryType: AuthRepositoryType
){
    fun execute(custId: Long, device: String, location: String) = authRepositoryType.getToken(custId, device, location)
}