package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class GetPinFromAsanUseCase @Inject constructor(
    private val repository: AuthRepositoryType
) {
    fun execute(phoneNumber: String, userId: String) = repository.getPinFromAsan(phoneNumber, userId)
}