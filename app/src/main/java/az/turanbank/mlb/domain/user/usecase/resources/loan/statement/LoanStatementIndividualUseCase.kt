package az.turanbank.mlb.domain.user.usecase.resources.loan.statement

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class LoanStatementIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, loanId: Long, token: String?) =
        mainRepository.loanStatementIndividual(custId, loanId, token)
}