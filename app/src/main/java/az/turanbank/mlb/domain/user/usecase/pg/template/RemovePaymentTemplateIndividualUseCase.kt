package az.turanbank.mlb.domain.user.usecase.pg.template

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class RemovePaymentTemplateIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?, tempId: Long) = mainRepository.removePaymentTempIndividual(custId, token, tempId)
}