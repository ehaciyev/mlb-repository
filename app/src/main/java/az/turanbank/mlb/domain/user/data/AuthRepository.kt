package az.turanbank.mlb.domain.user.data

import az.turanbank.mlb.data.remote.model.service.AuthApiServiceProvider
import az.turanbank.mlb.data.remote.model.request.*
import az.turanbank.mlb.data.remote.model.response.*
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

interface AuthRepositoryType {
    fun getPinFromAsan(phoneNumber: String, userId: String): Single <GetPinFromAsanResponseModel>
    fun checkIndividualCustomer(pin: String, mobile: String,lang: EnumLangType): Observable <CustomerResponseModel>
    fun sendOTPCode(custId: Long, compId: Long?, mobile: String,lang: EnumLangType): Observable<SendOTPResponseModel>
    fun verifyOTPCode(custId: Long, compId: Long?, confirmCode: String, broadcastType: Int): Observable<VerifyOTPResponseModel>
    fun individualRegisterMobileSubmit(username: String, password: String, repeatPassword: String, custId: Long): Observable<ServerResponseModel>
    fun loginUsernamePasswordIndividual(username: String, password: String): Single<CustomerResponseModel>
    fun checkPinForAsanRegister(pin: String): Single <CustomerResponseModel>
    fun checkPinForAsanJuridicalRegister(pin: String): Single <CustomerResponseModel>
    fun getAuthInfo(phoneNumber: String, userId: String, lang: EnumLangType): Single <GetAuthInfoResponseModel>
    fun registerWithAsan(transactionId: Long, certificate: String, challenge: String, custCode: Long, pin: String): Single <RegisterWithASANResponseModel>
    fun registerIndividualAsan(username: String, password: String, repeatPassword: String, custId: Long): Single<ServerResponseModel>
    fun loginWithAsan(transactionId: Long, certificate: String, challenge: String): Observable<ServerResponseModel>
    fun getCompaniesCert(pin: String, mobile: String, userId: String, custCompStatus: Int): Single<GetCompaniesCertResponseModel>
    fun selectCustCompany(pin: String, taxNo: String, mobile: String, userId: String, device: String, location: String): Single<SelectCustCompanyResponseModel>
    fun getCustCompany(pin: String, taxNo: String, mobile: String, userId: String, custCompStatus: Int): Single<GetCustCompanyResponseModel>
    fun registerJuridicalAsan(pin: String, taxNo: String, custId: Long, compId: Long, username: String, password: String, repeatPassword: String): Single<ServerResponseModel>
    fun checkJuridicalCustomer(pin: String, mobile: String, taxNo: String,lang: EnumLangType): Single <CustomerResponseModel>
    fun juridicalRegisterSubmit(pin: String, taxNo: String, username: String, password: String, repeatPassword: String): Single <ServerResponseModel>
    fun loginUsernamePasswordJuridical(username: String, password: String): Single <MlbResponseModel>
    fun checkPinForAsanIndividualLogin(pin: String): Single <CustomerResponseModel>
    fun checkPinForAsanJuridicalLogin(pin: String): Single <CustomerResponseModel>
    fun getToken(custId: Long, device: String, location: String): Single <CustomerResponseModel>
    fun checkForgotPassJuridical(pin: String, mobile: String, taxNo: String,lang: EnumLangType): Single <CustomerResponseModel>
    fun checkForgotPassIndividual(pin: String, mobile: String,lang: EnumLangType): Single <CustomerResponseModel>
    fun changePasswordIndividual(custId: Long, password: String, repeatPassword: String): Single <ServerResponseModel>
    fun changePasswordJuridical(custId: Long, compId: Long, password: String, repeatPassword: String): Single <ServerResponseModel>
    fun blockIndividualCustomer(custId: Long): Single <ServerResponseModel>
    fun blockJuridicalCustomer(custId: Long, compId: Long): Single <ServerResponseModel>
    fun checkUnblockJuridical(pin: String, taxNo: String, mobile: String,lang: EnumLangType): Single <CustomerResponseModel>
    fun checkUnblockIndividual(pin: String, mobile: String,lang: EnumLangType): Single <CustomerResponseModel>
    fun loginIndividualForAsan(userId: String?, phoneNumber: String?, transactionId: Long, certificate: String, challenge: String, custId: Long, device: String, location: String): Single <CustomerResponseModel>
    fun unblockIndividualCustomer(custId: Long): Single <ServerResponseModel>
    fun unblockJuridicalCustomer(custId: Long, compId: Long): Single <ServerResponseModel>
}

class AuthRepository @Inject constructor(
    private val serviceProvider: AuthApiServiceProvider
) : AuthRepositoryType {

    override fun loginIndividualForAsan(
        userId: String?,
        phoneNumber: String?,
        transactionId: Long,
        certificate: String,
        challenge: String,
        custId: Long,
        device: String,
        location: String
    ): Single<CustomerResponseModel> {
        val request = LoginIndividualForAsanRequestModel(
            userId,
            phoneNumber,
            transactionId,
            certificate,
            challenge,
            custId,
            device,
            location
        )
        return serviceProvider.getInstance().loginIndividualForAsan(request)
    }

    override fun checkUnblockJuridical(
        pin: String,
        taxNo: String,
        mobile: String,
        lang: EnumLangType
    ): Single<CustomerResponseModel> {
        val request = CheckForgotPassJuridicalRequestModel(pin, taxNo, mobile,lang)

        return serviceProvider.getInstance().checkUnblockJuridical(request)
    }

    override fun checkUnblockIndividual(
        pin: String,
        mobile: String,
        lang: EnumLangType
    ): Single<CustomerResponseModel> {
        val request = CheckIndividualCustomerRequestModel(pin, mobile,lang)

        return serviceProvider.getInstance().checkUnblockIndividual(request)
    }

    override fun unblockIndividualCustomer(custId: Long): Single<ServerResponseModel> {
        val request = UnblockIndividualCustomerRequestModel(custId)
        return serviceProvider.getInstance().unblockIndividualCustomer(request)
    }

    override fun unblockJuridicalCustomer(custId: Long, compId: Long): Single<ServerResponseModel> {
        val request = UnblockJuridicalCustomerRequestModel(custId, compId)
        return  serviceProvider.getInstance().unblockJuridicalCustomer(request)
    }

    override fun getCustCompany(
        pin: String,
        taxNo: String,
        mobile: String,
        userId: String,
        custCompStatus: Int
    ): Single<GetCustCompanyResponseModel> {
        val request = GetCustCompanyRequestModel(
            pin,
            taxNo,
            mobile,
            userId,
            custCompStatus
        )
        return serviceProvider.getInstance().getCustCompany(request)
    }

    override fun blockIndividualCustomer(
        custId: Long
    ): Single<ServerResponseModel> {

        val request = UnblockIndividualCustomerRequestModel(custId)
        return serviceProvider.getInstance().blockIndividualCustomer(request)
    }

    override fun blockJuridicalCustomer(
        custId: Long,
        compId: Long
    ): Single<ServerResponseModel> {

        val request = UnblockJuridicalCustomerRequestModel(custId, compId)
        return serviceProvider.getInstance().blockJuridicalCustomer(request)
    }


    override fun checkForgotPassJuridical(
        pin: String,
        mobile: String,
        taxNo: String,
        lang: EnumLangType
    ): Single<CustomerResponseModel> {
        val request = CheckForgotPassJuridicalRequestModel(pin, taxNo, mobile,lang)

        return serviceProvider.getInstance().checkForgotPassJuridical(request)
    }

    override fun checkForgotPassIndividual(
        pin: String,
        mobile: String,
        lang: EnumLangType
    ): Single<CustomerResponseModel> {

        val request = CheckIndividualCustomerRequestModel(pin, mobile,lang)

        return serviceProvider.getInstance().checkForgotPassIndividual(request)
    }

    override fun changePasswordIndividual(
        custId: Long,
        password: String,
        repeatPassword: String
    ): Single<ServerResponseModel> {
        val request = ChangePasswordRequestModel(custId, null, password, repeatPassword)

        return serviceProvider.getInstance().changePasswordIndividual(request)
    }

    override fun changePasswordJuridical(
        custId: Long,
        compId: Long,
        password: String,
        repeatPassword: String
    ): Single<ServerResponseModel> {
        val request = ChangePasswordRequestModel(custId, compId, password, repeatPassword)

        return serviceProvider.getInstance().changePasswordJuridical(request)
    }

    override fun getToken(
        custId: Long,
        device: String,
        location: String
    ): Single<CustomerResponseModel> {
        val request = GetTokenRequestModel(custId, device, location)
        return serviceProvider.getInstance().getToken(request)
    }

    override fun checkPinForAsanJuridicalLogin(pin: String): Single<CustomerResponseModel> {
        return serviceProvider.getInstance().checkPinForAsanJuridicalLogin(pin)
    }

    override fun checkPinForAsanIndividualLogin(pin: String): Single<CustomerResponseModel> {
        return serviceProvider.getInstance().checkPinForAsanLogin(pin)
    }

    override fun loginUsernamePasswordJuridical(
        username: String,
        password: String
    ): Single<MlbResponseModel> {
        val request = LoginWithUsernameAndPasswordIndividualRequestModel(username, password)
        return serviceProvider.getInstance().loginUsernamePasswordJuridical(request)
    }

    override fun juridicalRegisterSubmit(
        pin: String,
        taxNo: String,
        username: String,
        password: String,
        repeatPassword: String
    ): Single<ServerResponseModel> {
        val request = RegisterJuridicalCustomerRequestModel(pin, taxNo, username, password, repeatPassword)

        return serviceProvider.getInstance().registerJuridicalCustomer(request)
    }

    override fun checkJuridicalCustomer(
        pin: String,
        mobile: String,
        taxNo: String,
        lang: EnumLangType
    ): Single<CustomerResponseModel> {
        val request = CheckForgotPassJuridicalRequestModel(pin, taxNo, mobile,lang)

        return serviceProvider.getInstance().checkJuridicalCustomer(request)
    }


    override fun getPinFromAsan(phoneNumber: String, userId: String): Single <GetPinFromAsanResponseModel> {
        val request = GetPinFromAsanRequestModel(phoneNumber, userId)
        return serviceProvider.getInstance().getPinFromAsan(request)
    }

    override fun checkIndividualCustomer(pin: String, mobile: String, lang: EnumLangType): Observable <CustomerResponseModel> {
        val request = CheckIndividualCustomerRequestModel(pin, mobile, lang)
        return serviceProvider.getInstance().checkIndividualCustomer(request)
    }

    override fun sendOTPCode(custId: Long, compId: Long?, mobile: String,lang: EnumLangType): Observable<SendOTPResponseModel> {
        val request = SendOTPRegisterRequestModel(custId, compId, mobile, lang)
        return serviceProvider.getInstance().sendOTPCode(request)
    }

    override fun verifyOTPCode(custId: Long, compId: Long?, confirmCode: String, broadcastType: Int): Observable<VerifyOTPResponseModel> {
        val request = VerifyOTPRequestModel(custId, compId, confirmCode, broadcastType)
        return serviceProvider.getInstance().verifyOTPCode(request)
    }

    override fun individualRegisterMobileSubmit(
        username: String,
        password: String,
        repeatPassword: String,
        custId: Long
    ): Observable<ServerResponseModel> {
        val request = RegisterIndividualAsanRequestModel(username, password, repeatPassword, custId)
        return serviceProvider.getInstance().registerWithMobile(request)
    }

    override fun loginUsernamePasswordIndividual(
        username: String,
        password: String
    ): Single<CustomerResponseModel> {
        val request = LoginWithUsernameAndPasswordIndividualRequestModel(username, password)
        return serviceProvider.getInstance().loginUsernamePasswordIndividual(request)
    }

    override fun checkPinForAsanRegister(pin: String): Single<CustomerResponseModel> =
        serviceProvider.getInstance().checkPinForAsanRegister(pin)

    override fun getAuthInfo(phoneNumber: String, userId: String, lang: EnumLangType): Single<GetAuthInfoResponseModel> {
        val request = GetAuthInfoRequestModel(phoneNumber, userId,lang)
        return serviceProvider.getInstance().getAuthInfo(request)
    }

    override fun registerWithAsan(
        transactionId: Long,
        certificate: String,
        challenge: String,
        custCode: Long,
        pin: String
    ) : Single<RegisterWithASANResponseModel> {

        val request = RegisterWithAsanRequestModel (
            transactionId = transactionId,
            certificate = certificate,
            challenge = challenge,
            custCode =  custCode,
            pin =  pin)
        return serviceProvider.getInstance().registerWithAsan(request)
    }

    override fun registerIndividualAsan(
        username: String,
        password: String,
        repeatPassword: String,
        custId: Long
    ) : Single<ServerResponseModel> {
        val request = RegisterIndividualAsanRequestModel(
            username,
            password,
            repeatPassword,
            custId
        )

      return serviceProvider.getInstance().registerIndividualAsan(request)
    }

    override fun checkPinForAsanJuridicalRegister(pin: String): Single<CustomerResponseModel> =
        serviceProvider.getInstance().checkPinForAsanJuridicalRegister(pin)

    override fun loginWithAsan(
        transactionId: Long,
        certificate: String,
        challenge: String
    ): Observable<ServerResponseModel> {
        val request = LoginWithASANRequestModel(
            transactionId,
            certificate,
            challenge
        )
       return serviceProvider.getInstance().loginWithAsan(request)
    }

    override fun getCompaniesCert(pin: String, mobile: String, userId: String, custCompStatus: Int): Single<GetCompaniesCertResponseModel> {
        val request = GetCompaniesCertRequestModel(
            pin,
            mobile,
            userId,
            custCompStatus
        )
            return serviceProvider.getInstance().getCompaniesCert(request)
    }

    override fun selectCustCompany(
        pin: String,
        taxNo: String,
        mobile: String,
        userId: String,
        device: String,
        location: String
    ): Single<SelectCustCompanyResponseModel> {
        val request = SelectCustCompanyRequestModel(
            pin,
            taxNo,
            mobile,
            userId,
            device,
            location
        )
        return serviceProvider.getInstance().selectCustCompany(request)
    }

    override fun registerJuridicalAsan(
        pin: String,
        taxNo: String,
        custId: Long,
        compId: Long,
        username: String,
        password: String,
        repeatPassword: String
    ): Single<ServerResponseModel> {
        val request = RegisterJuridicalAsanRequestModel(
            pin,
            taxNo,
            custId,
            compId,
            username,
            password,
            repeatPassword
        )
        return serviceProvider.getInstance().registerJuridicalAsan(request)
    }
}