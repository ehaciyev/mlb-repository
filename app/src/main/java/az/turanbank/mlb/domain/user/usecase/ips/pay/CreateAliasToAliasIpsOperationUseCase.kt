package az.turanbank.mlb.domain.user.usecase.ips.pay

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class CreateAliasToAliasIpsOperationUseCase @Inject constructor(private val ipsRepository: IPSRepository) {

    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        receiverBankId: Long,
        senderAliasId: Long,
        receiverAliasTypeId: Long,
        receiverAlias: String?,
        paymentAmount: Long,
        receiverCustomerName: String,
        additionalInfoAboutPayment: String,
        additionalInfoFromCustomer: String,
        remittanceInfo: String
    ) = ipsRepository.createAliasToAliasIpsOperation(
        custId,
        compId,
        token,
        receiverBankId,
        senderAliasId,
        receiverAliasTypeId,
        receiverAlias,
        paymentAmount,
        receiverCustomerName,
        additionalInfoAboutPayment,
        additionalInfoFromCustomer,
        remittanceInfo
    )
}