package az.turanbank.mlb.domain.user.usecase.ips.register

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class VerifyEmailOtpUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, compId: Long?, token: String?, confirmCode: String) =
        ipsRepository.verifyOtpCodeForEmail(custId, compId, token, confirmCode)
}