package az.turanbank.mlb.domain.user.usecase.ips.credentials

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetDefaultCompoundsUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, token: String?,lang: EnumLangType) = ipsRepository.getDefaultCompounds(custId, token,lang)
}