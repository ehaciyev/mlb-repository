package az.turanbank.mlb.domain.user.usecase.ips.register

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CheckUserRegisteredUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun exectue(custId: Long, compId: Long?, token: String?,lang:EnumLangType) =
        ipsRepository.checkCustomerRegistration(custId, compId, token,lang)
}