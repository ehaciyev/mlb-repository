package az.turanbank.mlb.domain.user.usecase.resources.card

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetCardListForJuridicalUseCase  @Inject constructor(
        val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, compId: Long, token: String?, lang: EnumLangType) = mainRepositoryType.getJuridicalCardList(custId, compId, token,lang)
}