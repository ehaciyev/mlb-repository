package az.turanbank.mlb.domain.user.usecase.pg.template

import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CreatePaymentTemplateJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        tempName: String,
        merchantId: Long,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        amount: Double,
        currency: String,
        providerId: Long,
        categoryId: Long,
        lang: EnumLangType
    ) = mainRepository.createPaymentTemplateJuridical(
        custId,
        compId,
        token,
        tempName,
        merchantId,
        identificationType,
        identificationCode,
        amount,
        currency,
        providerId,
        categoryId,
        lang
    )
}