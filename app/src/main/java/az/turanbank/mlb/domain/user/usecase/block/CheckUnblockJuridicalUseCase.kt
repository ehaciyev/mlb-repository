package az.turanbank.mlb.domain.user.usecase.block

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CheckUnblockJuridicalUseCase @Inject constructor(
    private val authRepositoryType: AuthRepositoryType
){
    fun execute(pin: String, taxNo: String, mobile: String,lang: EnumLangType) = authRepositoryType.checkUnblockJuridical(pin, taxNo, mobile,lang)
}