package az.turanbank.mlb.domain.user.usecase.exchange

import az.turanbank.mlb.domain.user.data.ExchangeRepositoryType
import javax.inject.Inject

class ConvertCashUseCase @Inject constructor(
    private val exchangeRepositoryType: ExchangeRepositoryType
){
    fun execute(fromCurrency: String, toCurrency: String, currencyAmount: Double) =
        exchangeRepositoryType.convertCash(fromCurrency, toCurrency, currencyAmount)
}