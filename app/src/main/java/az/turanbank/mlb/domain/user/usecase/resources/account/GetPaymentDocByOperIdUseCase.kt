package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetPaymentDocByOperIdUseCase @Inject constructor(
    private val repository: MainRepository
){
    fun execute(custId: Long, token: String?, operId: Long) = repository.getPaymentDocByOperId(custId, token, operId)
}