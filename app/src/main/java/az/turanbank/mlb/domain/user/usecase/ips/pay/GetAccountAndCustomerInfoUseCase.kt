package az.turanbank.mlb.domain.user.usecase.ips.pay

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class GetAccountAndCustomerInfoUseCase @Inject constructor(private val ipsRepository: IPSRepository) {
    fun execute(
        iban:String,
        servicerBIC: String?,
        servicerMemberId: String?,
        swiftCode: String?,
        testSwiftCode: String?,
        custId:Long,
        token: String?
    ) = ipsRepository.getAccountAndCustomerInfo(
        iban,
        servicerBIC,
        servicerMemberId,
        swiftCode,
        testSwiftCode,
        custId,
        token
    )
}