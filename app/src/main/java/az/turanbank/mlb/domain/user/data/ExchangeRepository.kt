package az.turanbank.mlb.domain.user.data

import az.turanbank.mlb.data.remote.model.service.ExchangeApiServiceProvider
import az.turanbank.mlb.data.remote.model.exchange.*
import io.reactivex.Observable
import javax.inject.Inject

interface ExchangeRepositoryType {
    fun cashExchange(exchangeDate: String?): Observable<ExchangeCashResponseModel>
    fun cashlessExchange(exchangeDate: String?): Observable<ExchangeCashlessResponseModel>
    fun centralBank(exchangeDate: String?): Observable<ExchangeCentralBankResponseModel>
    fun currenciesCash(): Observable<ConverterCurrencyListModel>
    fun currenciesCashless(): Observable<ConverterCurrencyListModel>
    fun convertCash(fromCurrency: String, toCurrency: String, currencyAmount: Double): Observable<ConvertCashResponseModel>
}

class ExchangeRepository @Inject constructor(
    private val serviceProvider: ExchangeApiServiceProvider
) : ExchangeRepositoryType {
    override fun convertCash(
        fromCurrency: String,
        toCurrency: String,
        currencyAmount: Double
    ): Observable<ConvertCashResponseModel> {
        val request = ConvertCashRequestModel(fromCurrency, toCurrency, currencyAmount)
        return serviceProvider.getInstance().converterCash(request)
    }

    override fun currenciesCash() =
        serviceProvider.getInstance().currenciesCash()

    override fun currenciesCashless() =
        serviceProvider.getInstance().currenciesCashless()

    override fun cashlessExchange(exchangeDate: String?) =
        serviceProvider.getInstance().exchangeCashless(exchangeDate)

    override fun centralBank(exchangeDate: String?) =
        serviceProvider.getInstance().centralBank(exchangeDate)

    override fun cashExchange(exchangeDate: String?) =
        serviceProvider.getInstance().exchangeCash(exchangeDate)
}