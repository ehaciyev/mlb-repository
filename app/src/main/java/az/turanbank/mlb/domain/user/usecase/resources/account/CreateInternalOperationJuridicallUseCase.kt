package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CreateInternalOperationJuridicallUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, compId: Long, token: String?, dtAccountId: Long, crIban: String, amount: Double, purpose: String, taxNo: String?,lang: EnumLangType) =
        repository.createInternalOperationJuridical(
            custId = custId,
            token = token,
            dtAccountId = dtAccountId,
            crIban = crIban,
            amount = amount,
            purpose = purpose,
            compId = compId,
            taxNo = taxNo,
            lang = lang
        )
}