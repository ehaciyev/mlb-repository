package az.turanbank.mlb.domain.user.usecase.resources

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetAccountRequisitesForIndividualCustomerUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, accountId: Long, token: String?) =
        repository.getAccountRequisitesForIndividualCustomer(
            custId = custId,
            accountId = accountId,
            token = token
        )
}