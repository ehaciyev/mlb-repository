package az.turanbank.mlb.domain.user.usecase.resources.card.blocking

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class BlockCardForIndividualCustomerUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
) {
    fun execute(custId: Long, token: String?, cardId: Long, blockReason: String) =
        mainRepositoryType.blockCardForIndividualCustomer(custId, token, cardId, blockReason)
}
