package az.turanbank.mlb.domain.user.usecase.login

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class CheckPinForAsanJuridicalLoginUseCase @Inject constructor(
    private val authRepositoryType: AuthRepositoryType
){
    fun execute(pin: String) = authRepositoryType.checkPinForAsanJuridicalLogin(pin)
}