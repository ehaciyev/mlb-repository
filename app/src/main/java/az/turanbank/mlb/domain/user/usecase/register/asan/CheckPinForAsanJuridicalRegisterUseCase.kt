package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class CheckPinForAsanJuridicalRegisterUseCase @Inject constructor(
    private val repository: AuthRepository
) {
    fun execute(pin: String) = repository.checkPinForAsanJuridicalRegister(pin)
}