package az.turanbank.mlb.domain.user.usecase.ips.credentials

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class DeleteIPSAliasUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(compId: Long?, custId: Long, token: String?, aliasId: ArrayList<Long>, lang: EnumLangType) =
        ipsRepository.deleteIPSAlias(compId, custId, token, aliasId,lang)
}