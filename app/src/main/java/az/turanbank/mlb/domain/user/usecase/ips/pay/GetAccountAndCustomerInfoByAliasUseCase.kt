package az.turanbank.mlb.domain.user.usecase.ips.pay

import az.turanbank.mlb.data.remote.model.ips.request.EnumAliasType
import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class GetAccountAndCustomerInfoByAliasUseCase @Inject constructor(private val ipsRepository: IPSRepository) {

    fun execute(
        custId: Long,
        token: String?,
        aliasTypeId: EnumAliasType,
        value: String
    ) = ipsRepository.getAccountAndCustomerInfoByAlias(
        custId,
        token,
        aliasTypeId,
        value
    )
}