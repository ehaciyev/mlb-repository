package az.turanbank.mlb.domain.user.usecase.resources.card.statement

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetCardCreditStatementForIndividualCustomerUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String) =
        mainRepository.getCardCreditStatementForIndividualCustomer(custId, token, cardNumber, startDate, endDate)
}