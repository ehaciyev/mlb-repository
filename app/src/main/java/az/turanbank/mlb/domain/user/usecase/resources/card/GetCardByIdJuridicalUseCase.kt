package az.turanbank.mlb.domain.user.usecase.resources.card

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetCardByIdJuridicalUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, compId: Long, cardId: Long, token: String?,lang: EnumLangType) =
        mainRepositoryType.getJuridicalCardById(custId, compId, cardId, token,lang)
}