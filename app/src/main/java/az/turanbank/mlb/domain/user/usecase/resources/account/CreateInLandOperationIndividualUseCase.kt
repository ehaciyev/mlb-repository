package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CreateInLandOperationIndividualUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(
        custId: Long,
        token: String?,
        dtAccountId: Long,
        crIban: String,
        crCustTaxid: String,
        crCustName: String,
        crBankCode: String,
        crBankName: String,
        crBankTaxid: String,
        crBankCorrAcc: String,
        budgetCode: String,
        budgetLvl: String,
        amount: Double,
        purpose: String,
        note: String,
        operationType: Int,
        lang: EnumLangType
    ) =
        repository.createInLandOperationIndividual(
            custId, token, dtAccountId, crIban, crCustTaxid, crCustName, crBankCode, crBankName, crBankTaxid, crBankCorrAcc, budgetCode, budgetLvl, amount, purpose, note, operationType,lang
        )
}