package az.turanbank.mlb.domain.user.usecase.resources.loan.list

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetJuridicalLoansListUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, compId: Long, token: String?,lang: EnumLangType) = mainRepositoryType.loansForJuridicalCustomer(custId, compId, token,lang)
}