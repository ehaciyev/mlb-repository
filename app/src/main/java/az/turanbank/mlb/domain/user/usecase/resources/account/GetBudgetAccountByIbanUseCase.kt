package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetBudgetAccountByIbanUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, token: String?, iban: String) =
        repository.getBudgetAccountByIban(custId, token, iban)
}