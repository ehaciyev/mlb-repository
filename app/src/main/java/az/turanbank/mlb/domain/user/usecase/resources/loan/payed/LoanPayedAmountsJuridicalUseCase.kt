package az.turanbank.mlb.domain.user.usecase.resources.loan.payed

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class LoanPayedAmountsJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, compId: Long, loanId: Long, token: String?) =
        mainRepository.loanPayedAmountsJuridical(custId, loanId, compId, token)
}