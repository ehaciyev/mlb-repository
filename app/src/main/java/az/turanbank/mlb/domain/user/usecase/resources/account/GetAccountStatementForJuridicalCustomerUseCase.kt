package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetAccountStatementForJuridicalCustomerUseCase @Inject constructor(
        private val repository: MainRepository
){
    fun execute(custId: Long, compId: Long, token: String?, accountId: Long, startDate: String, endDate: String) =
            repository.getAccountStatementForJuridicalCustomer(
                    custId, compId, token, accountId, startDate, endDate
            )
}