package az.turanbank.mlb.domain.user.usecase.register.mobile

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class SendOTPCodeUseCase @Inject constructor(
    private val repository: AuthRepositoryType
) {
    fun execute(custId: Long, compId: Long?, mobile: String, lang: EnumLangType) =
        repository.sendOTPCode(custId, compId, mobile,lang)
}