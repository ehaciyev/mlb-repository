package az.turanbank.mlb.domain.user.usecase.login

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class IndividualLoginUsernamePasswordUseCase @Inject constructor(private val repository: AuthRepositoryType) {
    fun execute(username: String, password: String) = repository.loginUsernamePasswordIndividual(username, password)
}