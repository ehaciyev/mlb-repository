package az.turanbank.mlb.domain.user.usecase.resources.card.statement

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetCardStatementForJuridicalCustomerUseCase  @Inject constructor(
        private val repository: MainRepository
){
    fun execute(custId: Long, compId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String) =
            repository.getCardStatementForJuridicalCustomer(custId, compId, token, cardNumber, startDate, endDate)
}