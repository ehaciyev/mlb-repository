package az.turanbank.mlb.domain.user.usecase.pg.merchant

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetMerchantListJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(custId: Long, compId: Long, token: String?, categoryId: Long, lang: EnumLangType) =
        mainRepository.getMerchantListJuridical(custId, compId, token, categoryId, lang)
}