package az.turanbank.mlb.domain.user.usecase.resources.card.sms

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class DisableSmsNotificationForIndividualCustomerUseCase @Inject constructor(
    private val mainApi: MainRepositoryType
){
    fun execute(mobile: String, repeatMobile: String, custId: Long, token: String?, cardId: Long) =
        mainApi.disableSmsNotificationForIndividualCustomer(mobile, repeatMobile, custId, token, cardId)
}