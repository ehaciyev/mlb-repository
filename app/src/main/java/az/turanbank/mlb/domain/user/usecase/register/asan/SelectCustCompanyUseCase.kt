package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class SelectCustCompanyUseCase @Inject constructor(private val repository: AuthRepository) {
    fun execute(pin: String, taxNo: String, mobile: String, userId: String,device: String,
                location: String) =
        repository.selectCustCompany(pin, taxNo, mobile, userId, device, location)
}