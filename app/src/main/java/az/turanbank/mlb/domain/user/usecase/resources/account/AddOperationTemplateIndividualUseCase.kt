package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class AddOperationTemplateIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(custId: Long, tempName: String, token: String?, operId: Long) =
        mainRepository.addOperationTempIndividual(custId, tempName, token, operId)
}