package az.turanbank.mlb.domain.user.usecase.login

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class CheckPinForIndividualAsanLoginUseCase @Inject constructor(
    private val repositoryType: AuthRepositoryType
) {
    fun execute(pin: String) = repositoryType.checkPinForAsanIndividualLogin(pin)
}