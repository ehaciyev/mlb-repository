package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class PasswordChangeJuridicalUseCase @Inject constructor(
    private val repository: MainRepository
){
    fun execute(custId: Long, compId: Long?, token: String?, currentPassword: String?, password: String?, repeatPassword: String?) =
        repository.changePasswordJuridical(custId, compId, token, currentPassword, password, repeatPassword)
}