package az.turanbank.mlb.domain.user.usecase.pg.history

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetPaymentListJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(custId: Long, compId: Long?, token: String?,lang: EnumLangType) =
        mainRepository.getPaymentListJuridical(custId, token, compId,lang)
}