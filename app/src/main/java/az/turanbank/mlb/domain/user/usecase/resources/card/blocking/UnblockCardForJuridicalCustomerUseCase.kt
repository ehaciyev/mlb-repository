package az.turanbank.mlb.domain.user.usecase.resources.card.blocking

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class UnblockCardForJuridicalCustomerUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, compId: Long, token: String?, cardId: Long) =
        mainRepositoryType.unblockCardForJuridicalCustomer(custId, compId, token, cardId)
}