package az.turanbank.mlb.domain.user.usecase.main

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class LogoutUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, compId: Long?, token: String?) =
        repository.logout(custId, compId, token)
}