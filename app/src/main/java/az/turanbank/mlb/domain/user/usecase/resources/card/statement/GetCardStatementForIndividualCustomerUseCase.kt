package az.turanbank.mlb.domain.user.usecase.resources.card.statement

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetCardStatementForIndividualCustomerUseCase @Inject constructor(
        private val repository: MainRepository
){
    fun execute(custId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String) =
            repository.getCardStatementForIndividualCustomer(custId, token, cardNumber, startDate, endDate)
}