package az.turanbank.mlb.domain.user.usecase.cardoperations

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class SaveCardOperationTempUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, tempName: String, requestorCardNumber: String, destinationCardNumber: String, amount: Double, currency: String, cardOperationType: Int, token: String?) =
        mainRepositoryType.saveCardOperationTemp(custId, tempName, requestorCardNumber, destinationCardNumber, amount, currency, cardOperationType, token)
}