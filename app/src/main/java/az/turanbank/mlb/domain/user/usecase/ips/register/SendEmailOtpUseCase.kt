package az.turanbank.mlb.domain.user.usecase.ips.register

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class SendEmailOtpUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, compId: Long?, email: String, token: String?,lang:EnumLangType) =
        ipsRepository.sendOtpCodeForEmail(custId, compId, email, token,lang)
}