package az.turanbank.mlb.domain.user.usecase.pg.history

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetPaymentListIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?,lang: EnumLangType) = mainRepository.getPaymentListIndividual(custId, token,lang)
}