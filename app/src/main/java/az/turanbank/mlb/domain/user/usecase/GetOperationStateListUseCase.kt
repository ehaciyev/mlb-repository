package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetOperationStateListUseCase @Inject constructor(
    private val repository: MainRepository
) {

    fun execute(custId: Long, token: String?, lang: EnumLangType) =
        repository.getOperationStateList(custId, token,lang)
}