package az.turanbank.mlb.domain.user.usecase.pg.category

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetCategoryListIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?, lang: EnumLangType) = mainRepository.getCategoryListIndividual(custId, token, lang)
}