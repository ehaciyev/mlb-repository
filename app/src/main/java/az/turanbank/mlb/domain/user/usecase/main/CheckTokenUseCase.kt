package az.turanbank.mlb.domain.user.usecase.main

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class CheckTokenUseCase @Inject constructor (
    val repository: MainRepository) {
    fun execute(token: String?, custId: Long, compId: Long?) = repository.checkToken(token, custId, compId)
}