package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetSignInfoUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(
        operationIds: ArrayList<Long?>,
        custId: Long,
        token: String?,
        phoneNumber: String?,
        userId: String?,
        lang: EnumLangType,
        certCode: String?
    ) = repository.getSignInfo(
            operationIds, custId, token, phoneNumber, userId, lang, certCode
        )
}