package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetSecondSignInfoUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(
        operationIds: ArrayList<Long?>,
        custId: Long,
        compId: Long,
        token: String?,
        phoneNumber: String?,
        userId: String?,
        lang: EnumLangType,
        certCode: String?
    ) = repository.getSecondSignInfo(
        operationIds, custId, compId, token, phoneNumber, userId, lang, certCode
    )
}