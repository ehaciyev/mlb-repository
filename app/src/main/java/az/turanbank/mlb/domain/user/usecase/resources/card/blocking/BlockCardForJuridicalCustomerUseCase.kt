package az.turanbank.mlb.domain.user.usecase.resources.card.blocking

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class BlockCardForJuridicalCustomerUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
) {
    fun execute(custId: Long, compId: Long, token: String?, cardId: Long, blockReason: String) =
        mainRepositoryType.blockCardForJuridicalCustomer(custId, compId, token, cardId, blockReason)
}