package az.turanbank.mlb.domain.user.usecase.main

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class CheckAppVersionUseCase @Inject constructor (
    val repository: MainRepository) {
    fun execute(versionName: String) = repository.checkAppVersion(versionName)
}