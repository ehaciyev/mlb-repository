package az.turanbank.mlb.domain.user.usecase.resources.account.exchange

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class UpdateExchangeOperationUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int,
        operId: Long,
        operNameId: Long
    ) =
        mainRepository.updateExchangeOperation(
            custId,
            compId,
            token,
            dtAccountId,
            dtAmount,
            crAmount,
            crIban,
            dtBranchId,
            crBranchId,
            exchangeRate,
            exchangeOperationType,
            operId,
            operNameId
        )
}