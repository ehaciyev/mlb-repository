package az.turanbank.mlb.domain.user.usecase.cardoperations

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CardToCardUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, requestorCardId: Long, destinationCardId: Long, currency: String, amount: Double?, token: String?,lang: EnumLangType) =
        mainRepositoryType.cardToCard(custId, requestorCardId, destinationCardId, currency, amount, token,lang)
}