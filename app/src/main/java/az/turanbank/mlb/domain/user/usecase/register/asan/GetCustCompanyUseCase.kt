package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class GetCustCompanyUseCase @Inject constructor(private val repository: AuthRepository) {
    fun execute(pin: String, taxNo: String, mobile: String, userId: String, custCompStatus: Int) =
        repository.getCustCompany(pin, taxNo, mobile, userId, custCompStatus)
}