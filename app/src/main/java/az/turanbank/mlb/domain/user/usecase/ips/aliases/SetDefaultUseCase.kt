package az.turanbank.mlb.domain.user.usecase.ips.aliases

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class SetDefaultUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, compoundId: Long, token: String?) =
        ipsRepository.setDefault(compoundId, custId, token)
}