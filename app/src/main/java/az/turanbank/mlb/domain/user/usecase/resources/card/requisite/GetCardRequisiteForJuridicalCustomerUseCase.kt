package az.turanbank.mlb.domain.user.usecase.resources.card.requisite

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetCardRequisiteForJuridicalCustomerUseCase  @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, cardId: Long, compId: Long, token: String?,lang:EnumLangType) =
        mainRepositoryType.getCardRequisiteForJuridicalCustomer(custId, compId, cardId, token, lang)
}