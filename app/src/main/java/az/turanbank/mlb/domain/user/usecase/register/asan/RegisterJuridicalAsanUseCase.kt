package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class RegisterJuridicalAsanUseCase @Inject constructor(
    private val repository: AuthRepository
) {
    fun execute(
        pin: String,
        taxNo: String,
        custId: Long,
        compId: Long,
        username: String,
        password: String,
        repeatPassword: String
    ) = repository.registerJuridicalAsan(pin, taxNo, custId, compId, username, password, repeatPassword)
}