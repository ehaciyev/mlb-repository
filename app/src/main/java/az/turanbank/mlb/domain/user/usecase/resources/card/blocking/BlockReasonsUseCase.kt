package az.turanbank.mlb.domain.user.usecase.resources.card.blocking

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class BlockReasonsUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, token: String?,lang: EnumLangType) = mainRepositoryType.getBlockReasons(custId, token,lang)
}