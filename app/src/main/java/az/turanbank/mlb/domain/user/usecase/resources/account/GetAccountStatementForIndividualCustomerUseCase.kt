package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetAccountStatementForIndividualCustomerUseCase @Inject constructor(
        private val repository: MainRepository
) {
    fun execute(custId: Long, token: String?, accountId: Long, beginDate: String, endDate: String) =
            repository.getAccountStatementForIndividualCustomer(custId, token, accountId, beginDate, endDate)
}