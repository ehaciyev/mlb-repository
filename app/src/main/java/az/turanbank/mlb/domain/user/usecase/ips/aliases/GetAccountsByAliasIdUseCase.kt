package az.turanbank.mlb.domain.user.usecase.ips.aliases

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetAccountsByAliasIdUseCase @Inject constructor(private val ipsRepository: IPSRepository){
    fun execute(custId: Long, compId: Long?, aliasId: Long, token: String?,
                 lang: EnumLangType
    ) = ipsRepository.getAccountsByAliasId(custId,compId, aliasId, token, lang)
}