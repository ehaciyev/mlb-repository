package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class DeleteOperationTemplateUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?, tempId: Long) =
        mainRepository.deleteOperationTemp(custId, token, tempId)
}