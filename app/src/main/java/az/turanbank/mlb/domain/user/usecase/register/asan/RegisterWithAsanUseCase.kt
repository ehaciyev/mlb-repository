package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class RegisterWithAsanUseCase @Inject constructor(
    private val repository: AuthRepository
) {
    fun execute(
        transactionId: Long,
        certificate: String,
        challenge: String,
        custCode: Long,
        pin: String
    ) =
        repository.registerWithAsan(transactionId, certificate, challenge, custCode, pin)
}