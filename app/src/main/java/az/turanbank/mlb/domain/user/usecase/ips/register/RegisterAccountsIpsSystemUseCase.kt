package az.turanbank.mlb.domain.user.usecase.ips.register

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class RegisterAccountsIpsSystemUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(accountId: ArrayList<Long>, custId: Long, token: String?) =
        ipsRepository.registerIpsAccounts(accountId, custId, token)
}