package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.data.remote.model.response.ServerResponseModel
import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Single
import javax.inject.Inject

class CreateOrderUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        branchId: Long,
        orderType: Int?,
        kindId: Long?,
        period: Int,
        amount: Double,
        ccy: String,
        lang: EnumLangType
    ): Single<ServerResponseModel> {
        return if(compId != null) {
            repository.createOrderJuridical(custId, compId, token, branchId, orderType, kindId, period, amount, ccy,lang)
        } else {
            repository.createOrderIndividual(custId, token, branchId, orderType, kindId, period, amount, ccy,lang)
        }
    }
}