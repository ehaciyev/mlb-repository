package az.turanbank.mlb.domain.user.usecase.ips.credentials

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetMLBAliasesUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, compId: Long?, token: String?,lang: EnumLangType) =
        ipsRepository.getMLBAliases(custId, compId, token,lang)
}