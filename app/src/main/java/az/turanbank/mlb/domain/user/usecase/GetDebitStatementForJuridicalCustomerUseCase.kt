package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetDebitStatementForJuridicalCustomerUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, compId: Long, token: String?, accountId: Long, beginDate: String, endDate: String) =
        repository.getDebitStatementForJuridicalCustomer(custId, compId, token, accountId, beginDate, endDate)
}