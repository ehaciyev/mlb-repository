package az.turanbank.mlb.domain.user.usecase.ips.pay

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class GetAllBudgetLevelUseCase @Inject constructor(private val ipsRepository: IPSRepository) {

    fun execute(
        custId: Long,
        compId: Long?,
        token: String?
    ) = ipsRepository.getBudgetLevel(
        custId,
        compId,
        token
    )
}