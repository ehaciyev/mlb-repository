package az.turanbank.mlb.domain.user.usecase.pg.param

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetMerchantParamListIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?, merchantId: Long, lang: EnumLangType) = mainRepository.getMerchantParamListIndividual(custId, token, merchantId, lang)
}