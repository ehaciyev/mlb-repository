package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class PasswordChangeIndividualUseCase @Inject constructor(
    private val repository: MainRepository
){
    fun execute(custId: Long, token: String?, oldPassword: String?, password: String?, repeatPassword: String?) =
        repository.changePasswordIndividual(custId, token, oldPassword, password, repeatPassword)
}