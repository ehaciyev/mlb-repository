package az.turanbank.mlb.domain.user.usecase.register.mobile

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class RegisterJuridicalMobileUseCase @Inject constructor(
    private val repositoryType: AuthRepositoryType
){
    fun execute(pin: String, taxNo: String, username: String, password: String, repeatPassword: String) =
        repositoryType.juridicalRegisterSubmit(pin, taxNo, username, password, repeatPassword)
}