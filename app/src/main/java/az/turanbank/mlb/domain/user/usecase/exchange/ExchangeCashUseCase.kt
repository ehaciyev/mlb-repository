package az.turanbank.mlb.domain.user.usecase.exchange

import az.turanbank.mlb.domain.user.data.ExchangeRepositoryType
import javax.inject.Inject

class ExchangeCashUseCase @Inject constructor(
    private val exchangeRepositoryType: ExchangeRepositoryType
){
    fun execute(exchangeDate: String?) = exchangeRepositoryType.cashExchange(exchangeDate)
}