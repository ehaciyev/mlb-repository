package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class JuridicalRegisterGetCompaniesCertUseCase @Inject constructor(
    private val repository: AuthRepository
) {
    fun execute(pin: String, mobile: String, userId: String, custCompStatus: Int) =
        repository.getCompaniesCert(pin, mobile, userId, custCompStatus)
}