package az.turanbank.mlb.domain.user.usecase.forgot

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class ChangePasswordIndividualUseCase @Inject constructor(
    private val authRepositoryType: AuthRepositoryType
){
    fun execute(custId: Long, password: String, repeatPassword: String) =
        authRepositoryType.changePasswordIndividual(custId, password, repeatPassword)
}