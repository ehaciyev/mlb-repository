package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class AddOperationTemplateJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(custId: Long, compId: Long, tempName: String, token: String?, operId: Long) =
        mainRepository.addOperationTempJuridical(custId, compId, tempName, token, operId)
}