package az.turanbank.mlb.domain.user.usecase.ips.aliases

import az.turanbank.mlb.data.remote.model.ips.request.AliasAndAccountRequestModel
import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class LinkAliasToAccountUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(
        custId: Long,
        token: String?,
        aliasIdAndAccountIdList: ArrayList<AliasAndAccountRequestModel>
    ) = ipsRepository.linkAliasToAccount(custId, token, aliasIdAndAccountIdList)
}