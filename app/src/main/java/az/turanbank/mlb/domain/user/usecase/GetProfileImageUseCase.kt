package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetProfileImageUseCase @Inject constructor(
    private val repository: MainRepository
){
    fun execute(custId: Long, compId: Long?, token: String?) =
        repository.getProfileImage(custId, compId, token)
}