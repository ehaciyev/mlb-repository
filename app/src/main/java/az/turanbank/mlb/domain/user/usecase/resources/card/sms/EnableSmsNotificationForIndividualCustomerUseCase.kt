package az.turanbank.mlb.domain.user.usecase.resources.card.sms

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class EnableSmsNotificationForIndividualCustomerUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
) {
    fun execute(mobile: String, repeatMobile: String, custId: Long, token: String?, cardId: Long) =
        mainRepositoryType.addOrUpdateSmsNotificationForIndividualCustomer(mobile, repeatMobile, custId, token, cardId)
}
