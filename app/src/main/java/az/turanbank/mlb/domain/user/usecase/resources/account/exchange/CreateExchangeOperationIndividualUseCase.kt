package az.turanbank.mlb.domain.user.usecase.resources.account.exchange

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CreateExchangeOperationIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        token: String?,
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int,
        lang: EnumLangType
    ) =
        mainRepository.createExchangeOperationIndividual(
            custId,
            token,
            dtAccountId,
            dtAmount,
            crAmount,
            crIban,
            dtBranchId,
            crBranchId,
            exchangeRate,
            exchangeOperationType,
            lang
        )
}