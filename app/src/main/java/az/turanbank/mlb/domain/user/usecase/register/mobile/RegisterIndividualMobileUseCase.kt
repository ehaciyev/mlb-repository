package az.turanbank.mlb.domain.user.usecase.register.mobile

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class RegisterIndividualMobileUseCase @Inject constructor(
    private val repository: AuthRepositoryType
) {
    fun execute(username: String, password: String, repeatPassword: String, custId: Long) =
        repository.individualRegisterMobileSubmit(
            username,
            password,
            repeatPassword,
            custId
        )
}