package az.turanbank.mlb.domain.user.usecase.pg.merchant

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetMerchantListIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?, categoryId: Long, lang: EnumLangType) = mainRepository.getMerchantListIndividual(custId, token, categoryId, lang)
}