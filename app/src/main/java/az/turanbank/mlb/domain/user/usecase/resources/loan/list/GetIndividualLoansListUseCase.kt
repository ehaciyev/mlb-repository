package az.turanbank.mlb.domain.user.usecase.resources.loan.list

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetIndividualLoansListUseCase @Inject constructor(
        private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, token: String?,lang: EnumLangType) = mainRepositoryType.loansForIndividualCustomer(custId, token, lang)
}