package az.turanbank.mlb.domain.user.usecase.forgot

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class ChangePasswordJuridicalUseCase @Inject constructor(
    private val authRepositoryType: AuthRepositoryType
){
    fun execute(custId: Long, compId: Long, password: String, repeatPassword: String) =
        authRepositoryType.changePasswordJuridical(custId, compId, password, repeatPassword)
}