package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class UpdateOperationByIdUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long? = null,
        token: String?,
        dtAccountId: Long,
        crIban: String?,
        crCustTaxid: String? = null,
        crCustName: String? = null,
        crBankCode: String? = null,
        crBankName: String? = null,
        crBankTaxid: String? = null,
        crBankCorrAcc: String? = null,
        budgetCode: String? = null,
        budgetLvl: String? = null,
        amount: Double,
        purpose: String? = null,
        note: String? = null,
        operationType: Int? = null,
        operId: Long,
        operNameId: Long
    ) = repository.updateOperationById(
        custId,
        compId,
        token,
        dtAccountId,
        crIban,
        crCustTaxid,
        crCustName,
        crBankCode,
        crBankName,
        crBankTaxid,
        crBankCorrAcc,
        budgetCode,
        budgetLvl,
        amount,
        purpose,
        note,
        operationType,
        operId,
        operNameId
    )
}