package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetFirstSignInfoUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long,
                compId: Long,
                token: String?,
                userId: String?,
                phoneNumber: String?,
                certCode: String?,
                lang: EnumLangType,
                batchId: Long) = repository.getFirstSignInfo(custId, compId, token, userId, phoneNumber, certCode, lang, batchId)
}