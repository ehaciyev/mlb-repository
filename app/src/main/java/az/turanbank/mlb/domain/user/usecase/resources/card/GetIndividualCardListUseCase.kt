package az.turanbank.mlb.domain.user.usecase.resources.card

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetIndividualCardListUseCase @Inject constructor(
    val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, token: String?,lang: EnumLangType) = mainRepositoryType.getIndividualCardList(custId, token,lang)
}
