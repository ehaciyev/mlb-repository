package az.turanbank.mlb.domain.user.usecase.ips.paidToMe

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class PaidToMeUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, compId: Long?, token: String?) =
        ipsRepository.receivePaymentListIndividual(custId, compId, token)
}