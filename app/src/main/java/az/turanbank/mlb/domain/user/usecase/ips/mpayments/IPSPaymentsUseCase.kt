package az.turanbank.mlb.domain.user.usecase.ips.mpayments

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class IPSPaymentsUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(custId: Long, compId: Long?, token: String?, operNameId: Int?,lang: EnumLangType) =
        ipsRepository.operationAdvancedSearch(custId, compId, token, operNameId,lang)
}