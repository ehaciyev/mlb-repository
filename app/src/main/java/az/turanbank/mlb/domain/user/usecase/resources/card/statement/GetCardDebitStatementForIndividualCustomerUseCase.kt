package az.turanbank.mlb.domain.user.usecase.resources.card.statement

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetCardDebitStatementForIndividualCustomerUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, token: String?, cardNumber: String?, startDate: String, endDate: String) =
        mainRepository.getCardDebitStatementForIndividualCustomer(custId, token, cardNumber, startDate, endDate)
}