package az.turanbank.mlb.domain.user.usecase.resources.card.sms

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class DisableSmsNotificationForJuridicalCustomerUseCase @Inject constructor(
    private val mainApi: MainRepositoryType
){
    fun execute(mobile: String, repeatMobile: String, custId: Long, compId: Long, token: String?, cardId: Long) =
        mainApi.disableSmsNotificationForJuridicalCustomer(mobile, repeatMobile, custId, compId, token, cardId)
}