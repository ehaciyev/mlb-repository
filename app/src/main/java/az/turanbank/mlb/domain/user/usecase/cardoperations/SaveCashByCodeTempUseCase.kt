package az.turanbank.mlb.domain.user.usecase.cardoperations

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import javax.inject.Inject

class SaveCashByCodeTempUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, tempName: String, requestorCardNumber: String, destinationPhoneNumber: String, amount: Double, currency: String, token: String?) =
        mainRepositoryType.saveCashByCodeTemp(custId, tempName, requestorCardNumber, destinationPhoneNumber, amount, currency, token)
}