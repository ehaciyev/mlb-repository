package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class SignFileFirstAuthUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(
        custId: Long,
        token: String?,
        compId: Long,
        transactionId: Long,
        userId: String?,
        phoneNumber: String?,
        certCode: String?,
        batchId: Long,
        successOperId: ArrayList<Long>,
        failOperId: ArrayList<Long>
    )  = repository.signFileFirstAuth(custId, token, compId, transactionId, userId, phoneNumber, certCode, batchId,successOperId,failOperId)
}