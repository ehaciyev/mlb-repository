package az.turanbank.mlb.domain.user.usecase.cardoperations

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetCardOperationsTempByIdUseCase @Inject constructor(
    private val mainRepositoryType: MainRepositoryType
){
    fun execute(tempId: Long, custId: Long, token: String?,lang: EnumLangType) =
        mainRepositoryType.getCardOperationTempById(tempId, custId, token,lang)
}