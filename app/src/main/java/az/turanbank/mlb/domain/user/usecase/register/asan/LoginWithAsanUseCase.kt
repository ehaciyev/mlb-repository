package az.turanbank.mlb.domain.user.usecase.register.asan

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class LoginWithAsanUseCase @Inject constructor(
    private val repository: AuthRepository
) {
    fun execute(transactionId: Long, certificate: String, challenge: String) =
        repository.loginWithAsan(transactionId, certificate, challenge)
}