package az.turanbank.mlb.domain.user.usecase.resources.account.exchange

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class CreateExchangeOperationJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long,
        token: String?,
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int,
        lang: EnumLangType
    ) =
        mainRepository.createExchangeOperationJuridical(
            custId,
            compId,
            token,
            dtAccountId,
            dtAmount,
            crAmount,
            crIban,
            dtBranchId,
            crBranchId,
            exchangeRate,
            exchangeOperationType,
            lang
        )
}