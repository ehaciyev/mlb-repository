package az.turanbank.mlb.domain.user.usecase

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class GetCreditStatementForIndividualCustomerUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(custId: Long, token: String?, accountId: Long, beginDate: String, endDate: String) =
        repository.getCreditStatementForIndividualCustomer(custId, token, accountId, beginDate, endDate)
}