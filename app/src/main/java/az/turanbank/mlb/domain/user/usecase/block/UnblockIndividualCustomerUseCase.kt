package az.turanbank.mlb.domain.user.usecase.block

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class UnblockIndividualCustomerUseCase @Inject constructor(
    private val authRepository: AuthRepositoryType
){
    fun execute(custId: Long) = authRepository.unblockIndividualCustomer(custId)
}