package az.turanbank.mlb.domain.user.usecase.resources.deposit

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetEarlyPaysByDepositIdJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, compId: Long, depositId: Long, token: String?,lang:EnumLangType) =
        mainRepository.getEarlyPaysByDepositIdForJuridical(custId, compId, depositId, token,lang)
}