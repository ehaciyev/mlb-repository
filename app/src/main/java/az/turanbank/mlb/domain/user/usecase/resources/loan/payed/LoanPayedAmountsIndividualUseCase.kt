package az.turanbank.mlb.domain.user.usecase.resources.loan.payed

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class LoanPayedAmountsIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, loanId: Long, token: String?) =
        mainRepository.loanPayedAmountsIndividual(custId, loanId, token)
}