package az.turanbank.mlb.domain.user.usecase.register.mobile

import az.turanbank.mlb.domain.user.data.AuthRepositoryType
import javax.inject.Inject

class VerifyOTPCodeUseCase @Inject constructor(
    private val repository: AuthRepositoryType
) {
    fun execute(custId: Long, compId: Long?, confirmCode: String, broadcastType: Int) = repository.verifyOTPCode(custId, compId, confirmCode, broadcastType)
}