package az.turanbank.mlb.domain.user.usecase.ips.pay

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class GetAllBudgetCodesUseCase @Inject constructor(private val ipsRepository: IPSRepository) {

    fun execute(
        custId: Long,
        compId: Long?,
        token: String?
    ) = ipsRepository.getBudgetCode(
        custId,
        compId,
        token
    )
}