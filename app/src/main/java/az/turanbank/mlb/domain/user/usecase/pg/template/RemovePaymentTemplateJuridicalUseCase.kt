package az.turanbank.mlb.domain.user.usecase.pg.template

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class RemovePaymentTemplateJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
){
    fun execute(custId: Long, compId: Long, token: String?, tempId: Long) = mainRepository.removePaymentTempJuridical(custId, compId, token, tempId)
}