package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepositoryType
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class GetIndividualAccountListUseCase @Inject constructor(
        private val mainRepositoryType: MainRepositoryType
){
    fun execute(custId: Long, token: String?,lang: EnumLangType) = mainRepositoryType.getAccountListForIndividualCustomer(custId, token,lang)
}