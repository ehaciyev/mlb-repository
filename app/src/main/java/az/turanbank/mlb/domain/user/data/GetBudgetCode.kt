package az.turanbank.mlb.domain.user.data

class GetBudgetCode(
    val custId: Long,
    val compId: Long?,
    val token: String?
)
