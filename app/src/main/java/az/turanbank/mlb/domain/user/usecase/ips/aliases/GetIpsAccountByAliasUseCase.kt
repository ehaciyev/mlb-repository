package az.turanbank.mlb.domain.user.usecase.ips.aliases

import az.turanbank.mlb.domain.user.data.IPSRepository
import javax.inject.Inject

class GetIpsAccountByAliasUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
){
    fun execute(custId: Long, token: String?, type: String, value: String) = ipsRepository.getIpsAccountByAlias(custId, token, type, value)
}