package az.turanbank.mlb.domain.user.usecase.resources.account

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class OperationAdvancedSearchUseCase @Inject constructor(
    private val repository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        dtIban: String?,
        minAmt: Double?,
        maxAmt: Double?,
        startDate: String?,
        endDate: String?,
        operStateId: Int?,
        lang: EnumLangType
    ) =
        repository.operationAdvancedSearch(
            custId,
            compId,
            token,
            dtIban,
            minAmt,
            maxAmt,
            startDate,
            endDate,
            operStateId,
            lang
        )
}