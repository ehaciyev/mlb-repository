package az.turanbank.mlb.domain.user.usecase.ips.credentials

import az.turanbank.mlb.domain.user.data.IPSRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class DeleteIpsAccountUseCase @Inject constructor(
    private val ipsRepository: IPSRepository
) {
    fun execute(compId: Long?, custId: Long,token: String?, accountId: ArrayList<Long>, lang: EnumLangType) =
        ipsRepository.deleteIpsAccount(compId, custId, token, accountId, lang)
}