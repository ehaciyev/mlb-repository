package az.turanbank.mlb.domain.user.usecase.login

import az.turanbank.mlb.domain.user.data.AuthRepository
import javax.inject.Inject

class LoginIndividualForAsanUseCase @Inject constructor(
    private val repository: AuthRepository
) {
    fun execute(
        userId: String?,
        phoneNumber: String?,
        transactionId: Long,
        certificate: String,
        challenge: String,
        custId: Long,
        device: String,
        location: String
    ) = repository.loginIndividualForAsan(
        userId,
        phoneNumber, transactionId, certificate, challenge, custId, device, location
    )
}