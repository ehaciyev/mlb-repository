package az.turanbank.mlb.domain.user.usecase.pg.template

import az.turanbank.mlb.domain.user.data.MainRepository
import az.turanbank.mlb.util.EnumLangType
import javax.inject.Inject

class PaymentTemplateListIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(custId: Long, token: String?,lang: EnumLangType) =
        mainRepository.paymentTempListIndividual(custId, token,lang)
}