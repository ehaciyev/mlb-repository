package az.turanbank.mlb

import android.app.AlertDialog
import android.content.*
import java.lang.ref.WeakReference
import java.util.Date
import java.util.concurrent.TimeUnit
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import androidx.annotation.StringRes


object RateThisApp {
    private val TAG: String? = RateThisApp::class.java.simpleName
    private val PREF_NAME: String = "turanbank.mlb"
    private val KEY_INSTALL_DATE: String = "rta_install_date"
    private val KEY_LAUNCH_TIMES: String = "rta_launch_times"
    private val KEY_OPT_OUT: String = "rta_opt_out"
    private val KEY_ASK_LATER_DATE: String = "rta_ask_later_date"
    private var mInstallDate: Date = Date()
    private var mLaunchTimes = 0
    private var mOptOut = false
    private var mAskLaterDate: Date = Date()
    private var sConfig: Config = Config()
    private var sCallback: Callback? = null

    // Weak ref to avoid leaking the context
    private var sDialogRef: WeakReference<AlertDialog?>? = null

    /**
     * If true, print LogCat
     */
    const val DEBUG = false

    /**
     * Initialize RateThisApp configuration.
     * @param config Configuration object.
     */
    fun init(config: Config) {
        sConfig = config
    }

    /**
     * Set callback instance.
     * The callback will receive yes/no/later events.
     * @param callback
     */
    fun setCallback(callback: Callback?) {
        sCallback = callback
    }

    /**
     * Call this API when the launcher activity is launched.<br></br>
     * It is better to call this API in onCreate() of the launcher activity.
     * @param context Context
     */
    fun onCreate(context: Context) {
        var pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = pref.edit()
        // If it is the first launch, save the date in shared preference.
        if (pref.getLong(KEY_INSTALL_DATE, 0) == 0L) {
            storeInstallDate(context, editor)
        }
        // Increment launch times
        var launchTimes = pref.getInt(KEY_LAUNCH_TIMES, 0)
        launchTimes++
        editor.putInt(KEY_LAUNCH_TIMES, launchTimes)
        log("Launch times; $launchTimes")
        editor.apply()
        mInstallDate = Date(pref.getLong(KEY_INSTALL_DATE, 0))
        mLaunchTimes = pref.getInt(KEY_LAUNCH_TIMES, 0)
        mOptOut = pref.getBoolean(KEY_OPT_OUT, false)
        mAskLaterDate = Date(pref.getLong(KEY_ASK_LATER_DATE, 0))
        printStatus(context)
    }

    /**
     * This API is deprecated.
     * You should call onCreate instead of this API in Activity's onCreate().
     * @param context
     */
    @Deprecated("", ReplaceWith("onCreate(context)", "az.turanbank.mlb.RateThisApp.onCreate"))
    fun onStart(context: Context) {
        onCreate(context)
    }

    /**
     * Show the rate dialog if the criteria is satisfied.
     * @param context Context
     * @return true if shown, false otherwise.
     */
    fun showRateDialogIfNeeded(context: Context): Boolean {
        return if (shouldShowRateDialog()) {
            showRateDialog(context)
            true
        } else {
            false
        }
    }

    /**
     * Show the rate dialog if the criteria is satisfied.
     * @param context Context
     * @param themeId Theme ID
     * @return true if shown, false otherwise.
     */
    fun showRateDialogIfNeeded(context: Context, themeId: Int): Boolean {
        return if (shouldShowRateDialog()) {
            showRateDialog(context, themeId)
            true
        } else {
            false
        }
    }

    /**
     * Check whether the rate dialog should be shown or not.
     * Developers may call this method directly if they want to show their own view instead of
     * dialog provided by this library.
     * @return
     */
    private fun shouldShowRateDialog(): Boolean {
        return if (mOptOut) {
            false
        } else {
            if (mLaunchTimes >= sConfig!!.mCriteriaLaunchTimes) {
                return true
            }
            val threshold: Long = TimeUnit.DAYS.toMillis(sConfig!!.mCriteriaInstallDays.toLong()) // msec
            Date().time - mInstallDate!!.time >= threshold && Date().time - mAskLaterDate!!.time >= threshold
        }
    }

    /**
     * Show the rate dialog
     * @param context
     */
    private fun showRateDialog(context: Context) {
        val builder = AlertDialog.Builder(context)
        showRateDialog(context, builder)
    }

    /**
     * Show the rate dialog
     * @param context
     * @param themeId
     */
    private fun showRateDialog(context: Context, themeId: Int) {
        val builder = AlertDialog.Builder(context, themeId)
        showRateDialog(context, builder)
    }

    /**
     * Stop showing the rate dialog
     * @param context
     */
    fun stopRateDialog(context: Context) {
        setOptOut(context, true)
    }

    /**
     * Get count number of the rate dialog launches
     * @return
     */
    fun getLaunchCount(context: Context): Int {
        val pref: SharedPreferences =
            context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        return pref.getInt(KEY_LAUNCH_TIMES, 0)
    }

    private fun showRateDialog(context: Context, builder: AlertDialog.Builder) {
        if (sDialogRef != null && sDialogRef!!.get() != null) {
            // Dialog is already present
            return
        }
        val titleId =
            if (sConfig.mTitleId != 0) sConfig.mTitleId else R.string.rta_dialog_title
        val messageId =
            if (sConfig.mMessageId != 0) sConfig.mMessageId else R.string.rta_dialog_message
        val cancelButtonID =
            if (sConfig.mCancelButton != 0) sConfig.mCancelButton else R.string.rta_dialog_cancel
        val thanksButtonID =
            if (sConfig.mNoButtonId != 0) sConfig.mNoButtonId else R.string.rta_dialog_no
        val rateButtonID =
            if (sConfig.mYesButtonId != 0) sConfig.mYesButtonId else R.string.rta_dialog_ok
        builder.setTitle(titleId)
        builder.setMessage(messageId)
        when (sConfig.mCancelMode) {
            Config.CANCEL_MODE_BACK_KEY_OR_TOUCH_OUTSIDE -> builder.setCancelable(
                true
            ) // It's the default anyway
            Config.CANCEL_MODE_BACK_KEY -> {
                builder.setCancelable(false)
                builder.setOnKeyListener(DialogInterface.OnKeyListener { dialog, keyCode, event ->
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        dialog!!.cancel()
                        true
                    } else {
                        false
                    }
                })
            }
            Config.CANCEL_MODE_NONE -> builder.setCancelable(false)
        }
        builder.setPositiveButton(rateButtonID) { _, _ ->
            if (sCallback != null) {
                sCallback!!.onYesClicked()
            }
            val appPackage: String = context.packageName
            var url: String? = "market://details?id=$appPackage"
            if (!TextUtils.isEmpty(sConfig.mUrl)) {
                url = sConfig!!.mUrl
            }
            try {
                context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
            } catch (anfe: ActivityNotFoundException) {
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + context.packageName)
                    )
                )
            }
            setOptOut(context, true)
        }
        builder.setNeutralButton(cancelButtonID) { _, _ ->
            if (sCallback != null) {
                sCallback!!.onCancelClicked()
            }
            clearSharedPreferences(context)
            storeAskLaterDate(context)
        }
        builder.setNegativeButton(thanksButtonID) { _, _ ->
            if (sCallback != null) {
                sCallback!!.onNoClicked()
            }
            setOptOut(context, true)
        }
        builder.setOnCancelListener {
            if (sCallback != null) {
                sCallback!!.onCancelClicked()
            }
            clearSharedPreferences(context)
            storeAskLaterDate(context)
        }
        builder.setOnDismissListener(DialogInterface.OnDismissListener { sDialogRef!!.clear() })
        sDialogRef = WeakReference(builder.show())
    }

    /**
     * Clear data in shared preferences.<br></br>
     * This API is called when the "Later" is pressed or canceled.
     * @param context
     */
    private fun clearSharedPreferences(context: Context) {
        val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = pref.edit()
        editor.remove(KEY_INSTALL_DATE)
        editor.remove(KEY_LAUNCH_TIMES)
        editor.apply()
    }

    /**
     * Set opt out flag.
     * If it is true, the rate dialog will never shown unless app data is cleared.
     * This method is called when Yes or No is pressed.
     * @param context
     * @param optOut
     */
    private fun setOptOut(context: Context, optOut: Boolean) {
        val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = pref.edit()
        editor.putBoolean(KEY_OPT_OUT, optOut)
        editor.apply()
        mOptOut = optOut
    }

    /**
     * Store install date.
     * Install date is retrieved from package manager if possible.
     * @param context
     * @param editor
     */
    private fun storeInstallDate(
        context: Context,
        editor: SharedPreferences.Editor
    ) {
        var installDate: Date? = Date()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            val packMan: PackageManager = context.packageManager
            try {
                val pkgInfo: PackageInfo = packMan.getPackageInfo(context.packageName, 0)
                installDate = Date(pkgInfo.firstInstallTime)
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
        }
        editor.putLong(KEY_INSTALL_DATE, installDate!!.time)
        log("First install: $installDate")
    }

    /**
     * Store the date the user asked for being asked again later.
     * @param context
     */
    private fun storeAskLaterDate(context: Context) {
        val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor? = pref.edit()
        editor!!.putLong(KEY_ASK_LATER_DATE, System.currentTimeMillis())
        editor.apply()
    }

    /**
     * Print values in SharedPreferences (used for debug)
     * @param context
     */
    private fun printStatus(context: Context) {
        val pref: SharedPreferences =
            context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        log("*** RateThisApp Status ***")
        log("Install Date: " + Date(pref.getLong(KEY_INSTALL_DATE, 0)))
        log("Launch Times: " + pref.getInt(KEY_LAUNCH_TIMES, 0))
        log("Opt out: " + pref.getBoolean(KEY_OPT_OUT, false))
    }

    /**
     * Print log if enabled
     * @param message
     */
    private fun log(message: String?) {
        if (DEBUG) {
            Log.v(TAG, message)
        }
    }

    /**
     * RateThisApp configuration.
     */
    class Config
    /**
     * Constructor with default criteria.
     */ @JvmOverloads constructor(
        val mCriteriaInstallDays: Int = 3,
        val mCriteriaLaunchTimes: Int = 5
    ) {
        var mUrl: String? = null
        var mTitleId = 0
        var mMessageId = 0
        var mYesButtonId = 0
        var mNoButtonId = 0
        var mCancelButton = 0
        var mCancelMode =
            CANCEL_MODE_BACK_KEY_OR_TOUCH_OUTSIDE

        /**
         * Set title string ID.
         * @param stringId
         */
        fun setTitle(@StringRes stringId: Int) {
            mTitleId = stringId
        }

        /**
         * Set message string ID.
         * @param stringId
         */
        fun setMessage(@StringRes stringId: Int) {
            mMessageId = stringId
        }

        /**
         * Set rate now string ID.
         * @param stringId
         */
        fun setYesButtonText(@StringRes stringId: Int) {
            mYesButtonId = stringId
        }

        /**
         * Set no thanks string ID.
         * @param stringId
         */
        fun setNoButtonText(@StringRes stringId: Int) {
            mNoButtonId = stringId
        }

        /**
         * Set cancel string ID.
         * @param stringId
         */
        fun setCancelButtonText(@StringRes stringId: Int) {
            mCancelButton = stringId
        }

        /**
         * Set navigation url when user clicks rate button.
         * Typically, url will be https://play.google.com/store/apps/details?id=PACKAGE_NAME for Google Play.
         * @param url
         */
        fun setUrl(url: String?) {
            mUrl = url
        }

        /**
         * Set the cancel mode; namely, which ways the user can cancel the dialog.
         * @param cancelMode
         */
        fun setCancelMode(cancelMode: Int) {
            mCancelMode = cancelMode
        }

        companion object {
            const val CANCEL_MODE_BACK_KEY_OR_TOUCH_OUTSIDE = 0
            const val CANCEL_MODE_BACK_KEY = 1
            const val CANCEL_MODE_NONE = 2
        }
        /**
         * Constructor.
         * @param mCriteriaInstallDays
         * @param mCriteriaLaunchTimes
         */
    }

    /**
     * Callback of dialog click event
     */
    interface Callback {
        /**
         * "Rate now" event
         */
        fun onYesClicked()

        /**
         * "No, thanks" event
         */
        fun onNoClicked()

        /**
         * "Later" event
         */
        fun onCancelClicked()
    }
}