package az.turanbank.mlb

import android.content.SharedPreferences
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.domain.user.usecase.main.CheckAppVersionUseCase
import az.turanbank.mlb.domain.user.usecase.main.CheckTokenUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import javax.inject.Inject

interface SplashViewModelInputs : BaseViewModelInputs {
    fun checkToken()
    fun checkAppVersion(versionName: String)
}

interface SplashViewModelOutputs : BaseViewModelOutputs {
    fun tokenUpdated(): CompletableSubject
    fun onCheckAppVersionSuccess(): CompletableSubject
}

class SplashViewModel @Inject constructor(
    private val checkTokenUseCase: CheckTokenUseCase,
    private val checkAppVersionUseCase: CheckAppVersionUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
    SplashViewModelInputs,
    SplashViewModelOutputs {

    override fun tokenUpdated() = tokenUpdated

    override fun onCheckAppVersionSuccess() = onCheckAppVersionSuccess

    override val inputs: SplashViewModelInputs = this
    override val outputs: SplashViewModelOutputs = this

    var isCheckVersion = false

    private val tokenUpdated = CompletableSubject.create()
    private var onCheckAppVersionSuccess = CompletableSubject.create()

    val pinIsSet =  sharedPrefs.getBoolean("pinIsSet", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)


    override fun checkToken() {
        if (!isCustomerJuridical)
            checkTokenUseCase.execute(token, custId, null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        updateToken(it.customer.token)
                    } else {
                        sharedPrefs.edit().putBoolean("pinIsSet", false).apply()
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    sharedPrefs.edit().putBoolean("pinIsSet", false).apply()
                    error.onNext(1878)
                    it.printStackTrace()
                }).addTo(subscriptions)
        else
            checkTokenUseCase.execute(token, custId, compId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        updateToken(it.customer.token)
                    } else {
                        sharedPrefs.edit().putBoolean("pinIsSet", false).apply()
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    sharedPrefs.edit().putBoolean("pinIsSet", false).apply()
                    error.onNext(1878)
                    it.printStackTrace()
                }).addTo(subscriptions)
    }

    override fun checkAppVersion(versionName: String) {
        checkAppVersionUseCase.execute(versionName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    isCheckVersion = true
                    onCheckAppVersionSuccess.onComplete()
                } else {
                    isCheckVersion = false
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    private fun updateToken(token: String) {
        sharedPrefs.edit().putString("token", token).apply()
        tokenUpdated.onComplete()
    }
}