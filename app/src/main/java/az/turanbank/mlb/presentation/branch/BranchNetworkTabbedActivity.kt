package az.turanbank.mlb.presentation.branch

import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.branch.fragment.list.BranchListFragment
import az.turanbank.mlb.presentation.branch.fragment.map.BranchMapFragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_branch_network_tabbed.*

import javax.inject.Inject


class BranchNetworkTabbedActivity : BaseActivity(), BranchListFragment.SendCoordinates {

    private var searchItem: MenuItem? = null

    lateinit var homeScreenToolbar: Toolbar

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: BranchNetworkTabbedViewModel

    private var searchShow = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_branch_network_tabbed)

        homeScreenToolbar = findViewById(R.id.home_screen_toolbar)
        viewModel = ViewModelProvider(this,factory)[BranchNetworkTabbedViewModel::class.java]

        view_pager.adapter =
            BranchNetworkPagerAdapter(
                this,
                supportFragmentManager
            )
        tabs.setupWithViewPager(view_pager)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            homeScreenToolbar.setTitleTextColor(getColor(R.color.white))
            homeScreenToolbar.setSubtitleTextColor(getColor(R.color.white))
        }
        setSupportActionBar(homeScreenToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.branch_network)


        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                onTabSelected(tab?.position!!)
            }
        })

    }

    private fun onTabSelected(position: Int) {
        view_pager.currentItem = position
        if (position == 0) {
            searchShow = true
        } else if (position == 1) {
            searchShow = false
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.getItem(0)?.isVisible = searchShow
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.branch_search_menu, menu)
        searchItem = menu?.findItem(R.id.action_search)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun sendLatLong(latitude: Double, longitude: Double) {
        val tag = "android:switcher:" + R.id.view_pager.toString() + ":" + 1
        val fragment = supportFragmentManager.findFragmentByTag(tag) as BranchMapFragment
        fragment.showBranchLocation(latitude, longitude)
    }
}