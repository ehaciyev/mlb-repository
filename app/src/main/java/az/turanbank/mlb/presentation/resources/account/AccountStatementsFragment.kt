package az.turanbank.mlb.presentation.resources.account

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import io.reactivex.rxkotlin.addTo
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class AccountStatementsFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private var startDateCalendar: Calendar = Calendar.getInstance()
    private var endDateCalendar: Calendar = Calendar.getInstance()
    lateinit var accountStatementProgressImage: ImageView
    lateinit var accountStatementRecyclerView: RecyclerView
    lateinit var failMessage: TextView

    private lateinit var viewModel: AccountStatementsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.account_statements_fragment, container, false)

        val accountStatementsStartDateCalendar: LinearLayout = view.findViewById(R.id.accountStatementsStartDateCalendar)
        val accountStatementsEndDateCalendar: LinearLayout = view.findViewById(R.id.accountStatementsEndDateCalendar)
        val accountStatementStartDateEditText: EditText = view.findViewById(R.id.accountStatementStartDateEditText)
        val accountStatementEndDateEditText: EditText = view.findViewById(R.id.accountStatementEndDateEditText)

        accountStatementProgressImage = view.findViewById(R.id.accountStatementProgressImage)
        failMessage = view.findViewById(R.id.failMessage)
        accountStatementRecyclerView = view.findViewById(R.id.accountStatementRecyclerView)

        accountStatementsStartDateCalendar.setOnClickListener {
            val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                startDateCalendar.set(Calendar.YEAR, year)
                startDateCalendar.set(Calendar.MONTH, monthOfYear)
                startDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                accountStatementStartDateEditText.setText(
                    simpleDateFormat().format(
                        startDateCalendar.time
                    ).replace("-", ".")
                )
                if(accountStatementEndDateEditText.text.toString().isNotEmpty()) {
                    getAccountStatements()
                    animateProgressImage()
                }
            }

            DatePickerDialog(
                requireContext(), R.style.DateDialogTheme, date, startDateCalendar
                    .get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH),
                startDateCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        accountStatementsEndDateCalendar.setOnClickListener {
            val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                endDateCalendar.set(Calendar.YEAR, year)
                endDateCalendar.set(Calendar.MONTH, monthOfYear)
                endDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                accountStatementEndDateEditText.setText(simpleDateFormat().format(endDateCalendar.time).replace("-", "."))
                if(accountStatementStartDateEditText.toString().isNotEmpty()) {
                    getAccountStatements()
                    animateProgressImage()
                }
            }

            DatePickerDialog(
                requireContext(), R.style.DateDialogTheme, date, endDateCalendar
                    .get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH),
                endDateCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        return view
    }

    private fun animateProgressImage() {
        accountStatementProgressImage.visibility = View.VISIBLE
        failMessage.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        accountStatementProgressImage.startAnimation(rotate)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory) [AccountStatementsViewModel::class.java]
    }

    private fun getAccountStatements() {

        val statementType = arguments?.getInt("type")
        statementType?.let {
            viewModel.inputs.getAccountStatement(
                requireActivity().intent.getLongExtra("accountId", 0L),
                simpleDateFormat().format(startDateCalendar.time).replace("-", "."),
                simpleDateFormat().format(endDateCalendar.time).replace("-", "."),
                statementType
            )
            setOutputListeners()
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.showProgress().subscribe {
            if(it) {
                animateProgressImage()
            } else {
                accountStatementProgressImage.clearAnimation()
                accountStatementProgressImage.visibility = View.GONE
            }
        }.addTo(subscriptions)

        viewModel.outputs.accountStatement().subscribe {
            failMessage.visibility = View.GONE
            val llm = LinearLayoutManager(requireContext())
            llm.orientation = LinearLayoutManager.VERTICAL
            accountStatementRecyclerView.layoutManager = llm
            val adapter = AccountStatementRecyclerViewAdapter(it)
            accountStatementRecyclerView.adapter = adapter
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe{
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
            accountStatementProgressImage.clearAnimation()
            accountStatementProgressImage.visibility = View.GONE
            failMessage.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    private fun simpleDateFormat(): SimpleDateFormat {
        val myFormat = "dd-MM-yyyy"
        return SimpleDateFormat(myFormat, Locale.US)
    }

    companion object {
        fun newInstance() =
            AccountStatementsFragment()
    }
}
