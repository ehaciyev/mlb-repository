package az.turanbank.mlb.presentation.resources.activity

import android.content.Context
import android.content.SharedPreferences
import az.turanbank.mlb.presentation.base.BaseViewModel
import javax.inject.Inject


class ResourcesViewModel  @Inject constructor(private val context: Context,
                                              private val sharedPrefs: SharedPreferences): BaseViewModel() {
}