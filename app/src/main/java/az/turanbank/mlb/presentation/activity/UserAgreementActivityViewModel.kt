package az.turanbank.mlb.presentation.activity

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.base.BaseViewModel
import kotlinx.android.synthetic.main.activity_user_agreement.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class UserAgreementActivityViewModel @Inject constructor(sharedPreferences: SharedPreferences): BaseViewModel() {

}
