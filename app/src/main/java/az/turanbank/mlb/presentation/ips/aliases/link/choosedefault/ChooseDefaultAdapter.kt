package az.turanbank.mlb.presentation.ips.aliases.link.choosedefault

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.AliasAccountCompoundModel
import kotlinx.android.synthetic.main.ips_single_select_list_default_view.view.*

class ChooseDefaultAdapter(
    private val alias: ArrayList<AliasAccountCompoundModel>,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<ChooseDefaultAdapter.AliasSingleSelectViewHolder>() {

    private var selected: RadioButton? = null
    private var selectedItem : AliasAccountCompoundModel? = null
    private var selectedPosition: Int? = null

    class AliasSingleSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(model: AliasAccountCompoundModel, onClickListener: (position: Int) -> Unit) {
            itemView.aliasName.text = model.alias
            itemView.aliasTypeAndAccount.text = model.aliasType
            itemView.setOnClickListener {
                onClickListener.invoke(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        AliasSingleSelectViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.ips_single_select_list_default_view, parent, false
            )
        )

    override fun getItemCount() = alias.size

    override fun onBindViewHolder(holder: AliasSingleSelectViewHolder, position: Int) {
        var model = alias[position]
        holder.itemView.aliasName.text = model.alias
        holder.itemView.aliasTypeAndAccount.text = model.aliasType

        holder.itemView.setOnClickListener { onClickListener.invoke(position) }

        holder.itemView.selectedAlias.setOnClickListener {
            selectedPosition = position
            if (selected != null) {
                selectedItem!!.isSelected = false
                selected!!.isChecked = false
            }
            holder.itemView.selectedAlias.isChecked = true
            model.isSelected = true
            selectedItem = model
            selected = holder.itemView.selectedAlias

        }
    }

    fun getSelectedItem() = selectedItem

    fun getSelectedPosition() = selectedPosition
}