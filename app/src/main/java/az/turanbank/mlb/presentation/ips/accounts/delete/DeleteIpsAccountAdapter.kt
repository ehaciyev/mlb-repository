package az.turanbank.mlb.presentation.ips.accounts.delete

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.response.IPSMLBAccountResponseModel
import kotlinx.android.synthetic.main.ips_single_delete_item.view.*

class DeleteIpsAccountAdapter(
    private val context:Context,
    private val accounts: ArrayList<IPSMLBAccountResponseModel>,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<DeleteIpsAccountAdapter.AliasSingleSelectViewHolder>() {

    private var selected: RadioButton? = null
    private var selectedItem : IPSMLBAccountResponseModel? = null
    private var selectedPosition: Int? = null

    class AliasSingleSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        AliasSingleSelectViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.ips_single_delete_item, parent, false
            )
        )

    override fun getItemCount() = accounts.size

    override fun onBindViewHolder(holder: AliasSingleSelectViewHolder, position: Int) {

        var account = accounts[position]
        holder.itemView.aliasName.text = account.iban
        holder.itemView.aliasTypeAndAccount.text = account.currency

        if(account.isSelected){
            holder.itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.checked_account))
            holder.itemView.selectedAlias.isChecked = true
        } else {
            holder.itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.unchecked_account))
            holder.itemView.selectedAlias.isChecked = false
        }
        holder.itemView.setOnClickListener { onClickListener.invoke(position) }
        holder.itemView.selectedAlias.setOnClickListener { onClickListener.invoke(position) }
    }

}