package az.turanbank.mlb.presentation.exchange

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.exchange.fragment.cash.ExchangeCashFragment
import az.turanbank.mlb.presentation.exchange.fragment.cashless.ExchangeCashlessFragment
import az.turanbank.mlb.presentation.exchange.fragment.centralbank.ExchangeCentralBankFragment


class ExchangePagerAdapter(private val date: String, private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm,  BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    val bundle = Bundle()
    override fun getItem(position: Int): Fragment {
        bundle.apply { putString("date", date) }
        return when (position){
            0 -> ExchangeCashFragment().apply {
                arguments = Bundle().apply {
                    putString("date", date)
            }}
            1 -> ExchangeCashlessFragment().apply {
                arguments = Bundle().apply {
                    putString("date", date)
                }}
            2 -> ExchangeCentralBankFragment().apply {
                arguments = Bundle().apply {
                    putString("date", date)
                }}
            else -> ExchangeCashFragment().apply {
                arguments = Bundle().apply {
                    putString("date", date)
                }}
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position){
            0 -> context.resources.getString(R.string.cash)
            1 -> context.resources.getString(R.string.cashless)
            2 -> context.resources.getString(R.string.central_bank)
            else -> context.resources.getString(R.string.cash)
        }
    }

    override fun getCount(): Int {
        return 3
    }
}