package az.turanbank.mlb.presentation.ips.payrequest.requestor.alias


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.ips.request.EnumAliasType
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.payrequest.sender.IPSPaySenderActivity
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class ChooseIPSRequestedAliasFragment : BaseFragment() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ChooseRequestedAliasViewModel

    lateinit var aliasContainer: LinearLayout
    private lateinit var progressImage: ImageView
    private lateinit var submitText: TextView

    lateinit var submitButton: LinearLayout

    lateinit var alias: AppCompatTextView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_choose_requested_alias, container, false)

        viewModel =
            ViewModelProvider(this, factory)[ChooseRequestedAliasViewModel::class.java]

        aliasContainer = view.findViewById(R.id.aliasContainer)
        submitButton = view.findViewById(R.id.submitButton)
        progressImage = view.findViewById(R.id.progressImage)
        submitText = view.findViewById(R.id.submitText)
        alias = view.findViewById(R.id.alias)

        setInputListeners()
        setOutputListeners()

        return view
    }


    private fun setOutputListeners() {
        viewModel.outputs.aliasesSuccess().subscribe { aliasesList ->
            viewModel.setSelectedAliasType(0)
            aliasContainer.setOnClickListener {
                val builder: android.app.AlertDialog.Builder =
                    android.app.AlertDialog.Builder(requireContext())
                builder.setTitle(getString(R.string.select_alias))
                val list = arrayOfNulls<String>(aliasesList.size)

                for (i in 0 until aliasesList.size) {
                    list[i] = aliasesList[i]
                }
                builder.setItems(list) { _, which ->
                    viewModel.setSelectedAliasType(which)
                    alias.text = aliasesList[which]
                }
                val dialog = builder.create()
                dialog.show()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onAccountAndCustomerInfoByAlias().subscribe{
            val intent = Intent(requireActivity(), IPSPaySenderActivity::class.java)
            intent.putExtra("creditorAccountOrAlias", alias.text.toString())
            startActivity(intent)
            requireActivity().finish()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getAliases()
        submitButton.setOnClickListener {
            showProgressBar(true)
            if(alias.text.toString() == getString(R.string.select_account)){
                AlertDialogMapper(requireActivity(), 124008).showAlertDialog()
                showProgressBar(false)
            } else {
                viewModel.inputs.getAccountAndCustomerInfoByAlias(viewModel.getSelectedAliasType() as EnumAliasType, alias.text.toString())
            }
        }
    }
    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        aliasContainer.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

}
