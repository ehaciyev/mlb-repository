package az.turanbank.mlb.presentation.resources.account

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.CurrencySignMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.secondPartOfMoney
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_operation_in_land_verify.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AccountOperationInLandVerifyActivity : BaseActivity() {

    lateinit var viewmodel: AccountOperationInLandVerifyViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_operation_in_land_verify)

        toolbar_title.text = getString(R.string.domestic_transfer)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        viewmodel =
            ViewModelProvider(this, factory)[AccountOperationInLandVerifyViewModel::class.java]

        val dtAccountId = intent.getStringExtra("dtAccountId")
        val crIban = intent.getStringExtra("opInCrAccId")
        val crCustTaxid = intent.getStringExtra("opInLandCrAccTaxNo")
        val crCustName = intent.getStringExtra("opInCrAccName")
        val crBankCode = intent.getStringExtra("bankCode")
        val crBankName = intent.getStringExtra("crBankId")
        val crBankTaxid = intent.getStringExtra("opInLandBankIban")
        val crBankCorrAcc = intent.getStringExtra("opInLandCorrespondentAcc")
        val budgetCode = intent.getStringExtra("budgetCode")
        val budgetLvl = intent.getStringExtra("budgetLvl")
        val amount = intent.getStringExtra("opInLandAmount")
        val purpose = intent.getStringExtra("opInLandPurpose")
        val note = intent.getStringExtra("opInLandExtraInfo")
        val currency = intent.getStringExtra("currency")
        val operationType = intent.getIntExtra("operationType", 2)

        val curr = CurrencySignMapper().getCurrencySign(currency)
        val bigAmount = "$curr $amount"

        opInLandAccountId.text = intent.getStringExtra("opInLandAccountId")
        crBankId.text = intent.getStringExtra("crBankId")
        opInLandBankIban.text = intent.getStringExtra("opInLandBankIban")
        opInLandCorrespondentAcc.text = intent.getStringExtra("opInLandCorrespondentAcc")
        opInLandSwift.text = intent.getStringExtra("opInLandSwift")
        opInCrAccId.text = intent.getStringExtra("opInCrAccId")
        opInCrAccName.text = intent.getStringExtra("opInCrAccName")
        opInLandCrAccTaxNo.text = intent.getStringExtra("opInLandCrAccTaxNo")
        opInLandAmount.text = intent.getStringExtra("opInLandAmount")
        opInLandPurpose.text = intent.getStringExtra("opInLandPurpose")
        opInLandExtraInfo.text = intent.getStringExtra("opInLandExtraInfo")

        if (!intent.getStringExtra("opInLandBudgetClassCode").isNullOrEmpty() &&
            !intent.getStringExtra("opInLandBudgetLevelCode").isNullOrEmpty()
        ) {
            opInLandBudgetClassCodeContainerCard.visibility = View.VISIBLE
            opInLandBudgetLevelCodeContainerCard.visibility = View.VISIBLE
            opInLandBudgetClassCode.text = intent.getStringExtra("opInLandBudgetClassCode")
            opInLandBudgetLevelCode.text = intent.getStringExtra("opInLandBudgetLevelCode")
        } else {
            opInLandBudgetClassCodeContainerCard.visibility = View.GONE
            opInLandBudgetLevelCodeContainerCard.visibility = View.GONE
        }

        bigAmountInteger.text = CurrencySignMapper().getCurrencySign(curr) + firstPartOfMoney(amount.toDouble())
        bigAmountReminder.text = secondPartOfMoney(amount.toDouble())

        //bigViewAmount.text = bigAmount
        setOutputListeners()
        submitButton.setOnClickListener {
            showProgressBar(true)
            viewmodel.inputs.createInLandOperation(
                dtAccountId = dtAccountId!!.toLong(),
                crIban = crIban,
                crCustTaxid = crCustTaxid,
                crCustName = crCustName,
                crBankCode = crBankCode,
                crBankName = crBankName,
                crBankTaxid = crBankTaxid,
                crBankCorrAcc = crBankCorrAcc,
                budgetCode = budgetCode,
                budgetLvl = budgetLvl,
                amount = amount.toDouble(),
                purpose = purpose,
                note = note,
                operationType = operationType
            )
        }
    }

    private fun setOutputListeners() {
        viewmodel.outputs.onError().subscribe {
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)

        viewmodel.outputs.createInLandOperationSuccess().subscribe {
            val intent = Intent()
            intent.putExtra("accountResponse", it)
            intent.putExtra("operationNo", it.operationNo)
            intent.putExtra("operationStatus", it.operationStatus)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }.addTo(subscriptions)
    }

    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }
}
