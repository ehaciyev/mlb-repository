package az.turanbank.mlb.presentation.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.fragment.LoginIndividualFragment
import az.turanbank.mlb.presentation.fragment.LoginJuridicalFragment

class LoginPageAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> {
                LoginIndividualFragment()
            }
            1 -> {
                LoginJuridicalFragment()
            }
            else -> LoginIndividualFragment()
        }
    }


    override fun getPageTitle(position: Int): CharSequence? {

        return when (position) {
            0 -> {
                context.resources.getString(R.string.individual)
            }
            1 -> {
                context.resources.getString(R.string.juridical)
            }
            else -> null
        }
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return 2
    }
}