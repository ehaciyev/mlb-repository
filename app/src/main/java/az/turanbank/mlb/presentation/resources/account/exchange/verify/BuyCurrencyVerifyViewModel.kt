package az.turanbank.mlb.presentation.resources.account.exchange.verify

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.exchange.CreateExchangeOperationResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.exchange.CreateExchangeOperationIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.exchange.CreateExchangeOperationJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface BuyCurrencyVerifyViewModelInputs : BaseViewModelInputs {
    fun makeExchange(
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int
    )
}

interface BuyCurrencyVerifyViewModelOutputs : BaseViewModelOutputs {
    fun onExchangeSuccess(): PublishSubject<CreateExchangeOperationResponseModel>
}

class BuyCurrencyVerifyViewModel @Inject constructor(
    private val createExchangeOperationIndividualUseCase: CreateExchangeOperationIndividualUseCase,
    private val createExchangeOperationJuridicalUseCase: CreateExchangeOperationJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    BuyCurrencyVerifyViewModelInputs,
    BuyCurrencyVerifyViewModelOutputs {
    override val inputs: BuyCurrencyVerifyViewModelInputs = this

    override val outputs: BuyCurrencyVerifyViewModelOutputs = this


    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val exchange = PublishSubject.create<CreateExchangeOperationResponseModel>()

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    override fun makeExchange(
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int
    ) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if (isCustomerJuridical) {
            createExchangeOperationJuridicalUseCase
                .execute(
                    custId,
                    compId,
                    token,
                    dtAccountId,
                    dtAmount,
                    crAmount,
                    crIban,
                    dtBranchId,
                    crBranchId,
                    exchangeRate,
                    exchangeOperationType,
                    enumLangType
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        exchange.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            createExchangeOperationIndividualUseCase
                .execute(
                    custId,
                    token,
                    dtAccountId,
                    dtAmount,
                    crAmount,
                    crIban,
                    dtBranchId,
                    crBranchId,
                    exchangeRate,
                    exchangeOperationType,
                    enumLangType
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        exchange.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onExchangeSuccess() = exchange

}