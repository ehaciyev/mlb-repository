package az.turanbank.mlb.presentation.ips.payrequest.requestor.alias

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.request.EnumAliasType
import az.turanbank.mlb.data.remote.model.ips.response.GetAccountAndCustomerInfoByAliasResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAliasesUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetAccountAndCustomerInfoByAliasUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ChooseRequestedAliasViewModelInputs: BaseViewModelInputs{
    fun getAliases()
    fun getAccountAndCustomerInfoByAlias(aliasTypeId:EnumAliasType, value:String)
}
interface ChooseRequestedAliasViewModelOutputs: BaseViewModelOutputs{
    fun aliasesSuccess(): PublishSubject<ArrayList<String>>
    fun onAccountAndCustomerInfoByAlias(): PublishSubject<GetAccountAndCustomerInfoByAliasResponseModel>
}
class ChooseRequestedAliasViewModel @Inject constructor(
    sharedPrefs: SharedPreferences,
    private val getMLBAliasesUseCase: GetMLBAliasesUseCase,
    private val getAccountAndCustomerInfoByAliasUseCase: GetAccountAndCustomerInfoByAliasUseCase
) : BaseViewModel(),
    ChooseRequestedAliasViewModelInputs,
    ChooseRequestedAliasViewModelOutputs {
    override val inputs: ChooseRequestedAliasViewModelInputs = this

    override val outputs: ChooseRequestedAliasViewModelOutputs = this
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private var selectedAliasType: Int = 0

    private val accountAndCustomerInfoByAlias = PublishSubject.create<GetAccountAndCustomerInfoByAliasResponseModel>()


    private val aliasList = PublishSubject.create<ArrayList<String>>()

    override fun getAliases() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getMLBAliasesUseCase.execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val aliasList = arrayListOf<String>()
                if (it.status.statusCode == 1) {
                    it.aliases.forEach { alias ->
                        aliasList.add(alias.ipsValue)
                    }
                    this.aliasList.onNext(aliasList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun getAccountAndCustomerInfoByAlias(aliasTypeId: EnumAliasType, value: String) {
        getAccountAndCustomerInfoByAliasUseCase
            .execute(custId,token,aliasTypeId,value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it.status.statusCode) {
                    1 -> {
                        accountAndCustomerInfoByAlias.onNext(it)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            },{
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun aliasesSuccess() = aliasList
    override fun onAccountAndCustomerInfoByAlias() = accountAndCustomerInfoByAlias

    fun getSelectedAliasType() = selectedAliasType

    fun setSelectedAliasType(list: Int){
        when(list){
            0 -> {selectedAliasType = 1}
            1 -> {selectedAliasType = 2}
            2 -> {selectedAliasType = 3}
            3 -> {selectedAliasType = 4}
        }
    }

}