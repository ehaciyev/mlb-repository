package az.turanbank.mlb.presentation.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.activity.AGREED
import az.turanbank.mlb.presentation.activity.UserAgreementActivity
import az.turanbank.mlb.presentation.register.asan.individual.IndividualRegisterAsanActivity
import az.turanbank.mlb.presentation.register.mobile.individual.IndividualRegisterMobileActivity

class IndividualRegisterFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_individual_register, container, false)

        val asan = view.findViewById<CardView>(R.id.withAsan)
        val mobile = view.findViewById<CardView>(R.id.withMobile)

        mobile.setOnClickListener {
            startActivityForResult(Intent(activity, UserAgreementActivity::class.java), 2)
        }
        asan.setOnClickListener {
            startActivityForResult(Intent(activity, UserAgreementActivity::class.java), 1)
        }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1) {
            if(resultCode == AGREED) {
                startActivity(Intent(requireActivity(), IndividualRegisterAsanActivity::class.java))            }
        } else if(requestCode == 2) {
            if(resultCode == AGREED) {
                val mobileIndividualIntent = Intent(requireActivity(), IndividualRegisterMobileActivity::class.java)
                mobileIndividualIntent.putExtra("customerType", "individual")
                startActivity(mobileIndividualIntent)
            }
        }
    }
}
