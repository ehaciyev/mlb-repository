package az.turanbank.mlb.presentation.resources.loan.pay.success

import android.content.Context
import android.content.SharedPreferences
import az.turanbank.mlb.presentation.base.BaseViewModel
import javax.inject.Inject

class SuccessLoanPaymentViewModel @Inject constructor(private val context: Context, private val sharedPrefs: SharedPreferences):
    BaseViewModel() {

    fun getLang():String?{
        return sharedPrefs.getString("lang","az")
    }
}
