package az.turanbank.mlb.presentation.base

import android.content.Context
import az.turanbank.mlb.RuntimeLocaleChanger
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : DaggerFragment(){
    protected val subscriptions = CompositeDisposable()

    override fun onAttach(context: Context) {
        super.onAttach(RuntimeLocaleChanger.wrapContext(context)!!)
    }
}