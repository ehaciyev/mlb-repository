package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.SignFileResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.SignFileFirstAuthUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface SignFileFirstAuthViewModelInputs : BaseViewModelInputs {
    fun signFile(transactionId: Long, batchId: Long,successOperId: ArrayList<Long>,
                 failOperId: ArrayList<Long>)

}

interface SignFileFirstAuthViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun signFileSuccess(): PublishSubject<SignFileResponseModel>
}

class SignFileFirstAuthViewModel @Inject constructor(
    private val signFileFirstAuthUseCase: SignFileFirstAuthUseCase,
    private val sharedPrefs: SharedPreferences
):
    BaseViewModel(),
    SignFileFirstAuthViewModelInputs,
    SignFileFirstAuthViewModelOutputs {

    override fun signFile(transactionId: Long, batchId: Long,successOperId: ArrayList<Long>,
                          failOperId: ArrayList<Long>) {
        signFileFirstAuthUseCase.execute(
            custId,
            token,
            compId,
            transactionId,
            userId,
            phoneNumber,
            certCode,
            batchId,
            successOperId,
            failOperId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    signFileSuccess.onNext(it)
                } else {
                    signFileSuccess.onNext(it)
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun showProgress() = showProgress

    override fun signFileSuccess() = signFileSuccess

    override val inputs: SignFileFirstAuthViewModelInputs = this
    override val outputs: SignFileFirstAuthViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val phoneNumber = sharedPrefs.getString("phoneNumber", "")
    val userId = sharedPrefs.getString("userId", "")
    val certCode = sharedPrefs.getString("certCode", "")
    private val showProgress = PublishSubject.create<Boolean>()
    private val signFileSuccess = PublishSubject.create<SignFileResponseModel>()
}