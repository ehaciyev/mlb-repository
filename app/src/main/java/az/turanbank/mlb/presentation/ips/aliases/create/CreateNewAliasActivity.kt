package az.turanbank.mlb.presentation.ips.aliases.create

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.InstantPaymentActivity
import az.turanbank.mlb.presentation.ips.VerifyOtpCodeForAliasActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.util.addPrefix
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_create_new_alias.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class CreateNewAliasActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CreateNewAliasViewModel

    private var broadcastType = 0

    lateinit var dialog: MlbProgressDialog
    private val fromRegister: Boolean by lazy { intent.getBooleanExtra("fromRegister", false) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_new_alias)

        viewModel = ViewModelProvider(this, factory)[CreateNewAliasViewModel::class.java]
        initToolbarAndListeners()

        dialog = MlbProgressDialog(this)
        setOutputListeners()
        setInputListeners()

    }

    private fun setInputListeners() {
        if (fromRegister) {
            skip.visibility = View.VISIBLE
        } else {
            //skip.visibility = View.GONE
        }
        submitButton.setOnClickListener {
            dialog.showDialog()
            if (mobileOrEmail.text.toString().trim().isNotEmpty()) {
                if (broadcastType > 0) {
                    if (broadcastType == 1) {
                        viewModel.inputs.sendOtpCode(mobileOrEmail.text.toString().addPrefix())
                    } else {
                        viewModel.inputs.sendEmailOtpCode(mobileOrEmail.text.toString())
                    }
                } else {
                    dialog.hideDialog()
                    AlertDialogMapper(this, 124014).showAlertDialog()
                }
            } else {
                dialog.hideDialog()
                AlertDialogMapper(this, 124013).showAlertDialog()
            }
        }
        skip.setOnClickListener {
            startActivity(Intent(this, InstantPaymentActivity::class.java))
            finish()
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.otpSent().subscribe {
            dialog.hideDialog()
            val intent = Intent(this, VerifyOtpCodeForAliasActivity::class.java)
            val mobileOrEmail = if (this.broadcastType == 1) {
                "+"+mobileOrEmail.text.toString().addPrefix()
            } else {
                mobileOrEmail.text.toString()
            }
            if (broadcastType == 1){
                intent.putExtra("message", getString(R.string.enter_otp_code))
                intent.putExtra("numberOrEmail", getString(R.string.code_sent_to, mobileOrEmail))
            } else {
                intent.putExtra("message", getString(R.string.enter_otp_code_for_mail))
                intent.putExtra("numberOrEmail", getString(R.string.code_sent_to_mail, mobileOrEmail))
            }
            val broadcastType = if (broadcastType == 1) {
                "MOBILE"
            } else {
                "EMAIL"
            }

            intent.putExtra("confirmCode", it)
            intent.putExtra("broadcastType", broadcastType)
            intent.putExtra("mobileOrEmail", mobileOrEmail)
            intent.putExtra("fromRegister", fromRegister)

            startActivity(intent)
            finish()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            dialog.hideDialog()
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun initToolbarAndListeners() {
        toolbar_title.text = getString(R.string.create_new_alias)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        aliasTypeContainer.setOnClickListener {
            val builder: AlertDialog.Builder =
                AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.select_account))

            val aliasTypeList = arrayOf(getString(R.string.mobile), getString(R.string.e_mail))
            builder.setItems(aliasTypeList) { _, which ->
                aliasType.text = aliasTypeList[which]
                if (which == 0) {
                    prefix.visibility = View.VISIBLE
                    mobileOrEmail.hint = "XX XXX XX XX"
                    mobileOrEmail.inputType = InputType.TYPE_CLASS_NUMBER
                } else {
                    prefix.visibility = View.GONE
                    mobileOrEmail.hint = getString(R.string.email_sample)
                    mobileOrEmail.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                }
                broadcastType = which + 1
            }
            val dialog = builder.create()
            dialog.show()
        }
    }
}