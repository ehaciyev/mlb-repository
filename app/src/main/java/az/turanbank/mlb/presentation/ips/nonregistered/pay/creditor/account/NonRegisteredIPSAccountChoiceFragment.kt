package az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.account

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.BankListAdapter
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.moreinfo.AccountAndCustomerInfoActivity
import az.turanbank.mlb.util.animateProgressImage
import az.turanbank.mlb.util.revertProgressImageAnimation
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_ipscreditor_account_choice.*
import javax.inject.Inject


class NonRegisteredIPSAccountChoiceFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: NonRegisteredIPSCreditorAccountChoiceViewModel

    private lateinit var bankContainer: LinearLayout
    private lateinit var progressImage: ImageView
    private lateinit var submitText: TextView

    lateinit var submitButton: LinearLayout

    lateinit var bank: AutoCompleteTextView

    private var receiverName: String? = null
    private var receiverSurname: String? = null
    private var taxNumber: String? = null

    lateinit var account: AppCompatEditText


    private val senderFullName: String by lazy { requireActivity().intent.getStringExtra("senderFullName") }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_ipscreditor_account_choice, container, false)

        viewModel =
            ViewModelProvider(this, factory)[NonRegisteredIPSCreditorAccountChoiceViewModel::class.java]

        initViews(view)

        setOutputListeners()
        setInputListeners()

        return view
    }

    private fun setOutputListeners() {
        viewModel.outputs.onBankListSuccess().subscribe { banks ->
            val list = arrayListOf<String>()
            val bankHash = HashMap<String, String>()
            val bankCodeHash = HashMap<String, String>()

            for (i in 0 until banks.bankInfos.size) {
                list.add(banks.bankInfos[i].name)
                bankHash[banks.bankInfos[i].name] = banks.bankInfos[i].id.toString()
                bankCodeHash[banks.bankInfos[i].name] = banks.bankInfos[i].code
            }
            val adapter = BankListAdapter(requireContext(), list)
            bank.threshold = 0
            adapter.notifyDataSetChanged()

            bank.onItemClickListener = AdapterView.OnItemClickListener {_, _, position, _ ->
                viewModel.setSelectedBankId(bankHash[adapter.getItem(position)]!!.toLong())
                viewModel.setSelectedBankCode(bankCodeHash[adapter.getItem(position)])
            }

            bank.setAdapter(adapter)
        }.addTo(subscriptions)


        viewModel.outputs.onReceiverCustomerSuccess().subscribe{
            receiverName = it.name
            receiverSurname = it.surname
            taxNumber = it.taxNumber
            sendIntent(AccountAndCustomerInfoActivity(), true)
        }.addTo(subscriptions)


        viewModel.outputs.onError().subscribe{
            //account not found error code
            if(it == 33311){
                AlertDialog.Builder(requireActivity())
                    .setTitle(R.string.error_occured)
                    .setMessage(requireActivity().getString(ErrorMessageMapper().getErrorMessage(it)))
                    .setPositiveButton(R.string.ok) { _, _ ->
                        sendIntent(AccountAndCustomerInfoActivity(), false)
                    }
                    .setCancelable(true)
                    .show()
            } else {
                AlertDialogMapper(requireActivity(), it).showAlertDialog()
            }
        }.addTo(subscriptions)

    }


    private fun setInputListeners() {
        viewModel.inputs.getBankList()
        submitButton.setOnClickListener {
            showProgressBar(true)
            when {
                bank.text.toString().trim().isEmpty() -> {
                    AlertDialogMapper(requireActivity(), 124020).showAlertDialog()
                    showProgressBar(false)
                }
                account.text.toString().trim().isEmpty() -> {
                    AlertDialogMapper(requireActivity(), 124021).showAlertDialog()
                    showProgressBar(false)
                }
                else -> {
                    viewModel.inputs.getReceiverCustomerInfo(account.text.toString())
                    showProgressBar(false)
                }
            }
        }
    }

    private fun initViews(view: View) {
        bankContainer = view.findViewById(R.id.bankContainer)

        progressImage = view.findViewById(R.id.progressImage)

        submitText = view.findViewById(R.id.submitText)

        submitButton = view.findViewById(R.id.submitButton)

        bank = view.findViewById(R.id.bank)

        account = view.findViewById(R.id.account)
        account.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(28))
    }

    private fun sendIntent(activity: Activity, isRegistered: Boolean){
        val intent = Intent(requireActivity(), activity::class.java)
        intent.putExtra("isRegistered", isRegistered)
        intent.putExtra("senderAccountOrAlias", requireActivity().intent.getStringExtra("senderAccountOrAlias"))
        intent.putExtra("senderId", requireActivity().intent.getLongExtra("senderId",0L))
        intent.putExtra("isGovernmentPayment", viewModel.isGovernmentPayment())
        intent.putExtra("receiverId", viewModel.getSelectedBankId())
        intent.putExtra("receiverAccountOrAlias", account.text.toString())
        intent.putExtra("fromAccount",requireActivity().intent.getBooleanExtra("fromAccount",true))
        intent.putExtra("toAccount",true)
        intent.putExtra("receiverCustomerName", receiverName)
        intent.putExtra("receiverCustomerSurname", receiverSurname)
        intent.putExtra("senderFullName", senderFullName)
        intent.putExtra("voenCode",taxNumber)
        startActivity(intent)
    }

    private fun showProgressBar(show: Boolean) {
        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        bankContainer.isClickable = !show
        accountContainer.isClickable = !show
        bank.isClickable = !show
        account.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }
}
