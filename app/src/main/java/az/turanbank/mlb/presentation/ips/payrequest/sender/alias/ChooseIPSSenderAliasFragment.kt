package az.turanbank.mlb.presentation.ips.payrequest.sender.alias


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.ips.request.EnumAliasType
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.BankListAdapter
import az.turanbank.mlb.presentation.ips.pay.preconfirm.TransferWithIPSActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_ipscreditor_alias_choice.*
import javax.inject.Inject

class ChooseIPSSenderAliasFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ChooseIPSSenderAliasViewModel
    private lateinit var bankContainer: LinearLayout
    private lateinit var aliasTypeContainer: LinearLayout
    private lateinit var progressImage: ImageView
    private lateinit var submitText: TextView

    lateinit var submitButton: LinearLayout

    lateinit var bank: AutoCompleteTextView

    private var iban = ""
    private var receiverName = ""

    lateinit var alias: AppCompatEditText
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_ipscreditor_alias_choice, container, false)

        viewModel = ViewModelProvider(this, factory)[ChooseIPSSenderAliasViewModel::class.java]
        initViews(view)

        setOutputListeners()
        setInputListeners()

        return view
    }

    private fun setOutputListeners() {
        viewModel.outputs.onBankListSuccess().subscribe { banks ->
            val list = arrayOfNulls<String>(banks.size)

            for (i in 0 until banks.size) {
                list[i] = banks[i]
            }
            val adapter =
                BankListAdapter(requireContext(), banks)
            bank.threshold = 0
            adapter.notifyDataSetChanged()
            bank.setAdapter(adapter)
        }.addTo(subscriptions)

        viewModel.outputs.onAccountAndCustomerInfoByAlias().subscribe{
            showProgressBar(false)
            var intent = Intent(requireActivity(), TransferWithIPSActivity::class.java)
            intent.putExtra("bank", bank.text.toString())
            intent.putExtra("accountoralias_num", alias.text.toString() )
            intent.putExtra("receiverCustomerName",it.name)
            intent.putExtra("isPayRequest",true)
            intent.putExtra("receiverCustomerName",it.surname)
            intent.putExtra("receiverIban",it.id.iban)
            startActivity(intent)
        }.addTo(subscriptions)

        viewModel.onError().subscribe{
            showProgressBar(false)
            AlertDialogMapper(requireActivity(),it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getBankList()

        val typeList = arrayOf(
            getString(R.string.pin),
            getString(R.string.tin),
            getString(R.string.e_mail),
            getString(R.string.mobile))


        aliasTypeContainer.setOnClickListener{
            val builder: android.app.AlertDialog.Builder =
                android.app.AlertDialog.Builder(requireContext())
            builder.setTitle(getString(R.string.select_alias_type))

            builder.setItems(typeList){_, which ->
                when(which){
                    0 -> {
                        alias.inputType = InputType.TYPE_CLASS_TEXT
                        alias.hint = getString(R.string.pin)
                    }
                    1 -> {
                        alias.inputType = InputType.TYPE_CLASS_TEXT
                        alias.hint = getString(R.string.tin)
                    }
                    2 -> {
                        alias.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                        alias.hint = getString(R.string.e_mail)
                    }
                    3 -> {
                        alias.inputType = InputType.TYPE_CLASS_NUMBER
                        alias.hint = getString(R.string.mobile)
                    }
                }
                viewModel.setSelectedAliasType(which)
                aliasType.text = typeList[which]
            }

            val dialog = builder.create()
            dialog.show()
        }


        submitButton.setOnClickListener {
            showProgressBar(true)
            if (alias.text.toString() == getString(R.string.select_alias)) {
                AlertDialogMapper(requireActivity(), 124008).showAlertDialog()
                showProgressBar(false)
            } else {
                viewModel.inputs.getAccountAndCustomerInfoByAlias(viewModel.getSelectedAliasType() as EnumAliasType, alias.text.toString())
            }
        }
    }

    private fun initViews(view: View) {
        bankContainer = view.findViewById(R.id.bankContainer)

        aliasTypeContainer = view.findViewById(R.id.aliasTypeContainer)

        progressImage = view.findViewById(R.id.progressImage)

        submitText = view.findViewById(R.id.submitText)

        submitButton = view.findViewById(R.id.submitButton)

        bank = view.findViewById(R.id.bank)

        alias = view.findViewById(R.id.alias)

        aliasTypeContainer.visibility = View.VISIBLE
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        bankContainer.isClickable = !show
        alias.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.light_gray
                )
            )
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.colorPrimary
                )
            )
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

}
