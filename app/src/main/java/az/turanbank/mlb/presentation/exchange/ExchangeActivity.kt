package az.turanbank.mlb.presentation.exchange

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.exchange.converter.ExchangeConverterActivity
import az.turanbank.mlb.presentation.exchange.fragment.ExchangeViewModel
import com.google.android.material.tabs.TabLayout
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_exchange.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.toolbar_title
import kotlinx.android.synthetic.main.mlb_toolbar_with_calendar.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class ExchangeActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ExchangeViewModel

    private var calendar: Calendar = Calendar.getInstance()

    lateinit var sectionsPagerAdapter: ExchangePagerAdapter

    private var theDate = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchange)

        viewModel = ViewModelProvider(this, factory)[ExchangeViewModel::class.java]

        sectionsPagerAdapter = ExchangePagerAdapter(
            theDate,
            this,
            supportFragmentManager
        )
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)

        toolbar_title.text = getString(R.string.exchanges)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        date.setOnClickListener {
            val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabel()
                sectionsPagerAdapter.notifyDataSetChanged()
            }

            DatePickerDialog(
                this, R.style.DateDialogTheme, date, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
        tabs.setupWithViewPager(viewPager)
        exchange_converter.setOnClickListener {
            startActivity(Intent(this, ExchangeConverterActivity::class.java))
        }
        viewModel.outputs.onDateChanged().subscribe {
            viewPager.adapter = null
            sectionsPagerAdapter = ExchangePagerAdapter(
                it,
                this,
                supportFragmentManager
            )
            viewPager.adapter = sectionsPagerAdapter
        }.addTo(subscriptions)
        viewPager.offscreenPageLimit = 3
    }

    private fun updateLabel() {
        val myFormat = "dd-MM-yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        viewModel.inputs.changeDate(sdf.format(calendar.time).replace("-", "."))
    }
}