package az.turanbank.mlb.presentation.ips.aliases.create

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.ips.register.SendEmailOtpUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.SendMobileOtpUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface CreateNewAliasViewModelInputs : BaseViewModelInputs {
    fun sendOtpCode(mobile: String)
    fun sendEmailOtpCode(email: String)
}

interface CreateNewAliasViewModelOutputs : BaseViewModelOutputs {
    fun otpSent(): PublishSubject<Int>
}

class CreateNewAliasViewModel @Inject constructor(
    private val sendMobileOtpUseCase: SendMobileOtpUseCase,
    private val sendEmailOtpUseCase: SendEmailOtpUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    CreateNewAliasViewModelInputs,
    CreateNewAliasViewModelOutputs {
    override val inputs: CreateNewAliasViewModelInputs = this

    override val outputs: CreateNewAliasViewModelOutputs = this
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val token = sharedPrefs.getString("token", "")

    private val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val otpSuccess = PublishSubject.create<Int>()

    override fun sendOtpCode(

        mobile: String
    ) {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        sendMobileOtpUseCase
            .execute(custId, compId, mobile, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    otpSuccess.onNext(it.status.statusCode)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun sendEmailOtpCode(email: String) {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }

        sendEmailOtpUseCase
            .execute(custId, compId, email, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    otpSuccess.onNext(it.status.statusCode)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun otpSent() = otpSuccess
}