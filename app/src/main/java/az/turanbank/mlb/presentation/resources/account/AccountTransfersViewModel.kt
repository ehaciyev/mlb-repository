package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetProfileImageResponseModel
import az.turanbank.mlb.domain.user.usecase.GetProfileImageUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountTransfersInputs: BaseViewModelInputs{
    fun getProfileImage()
}

interface AccountTransfersOutputs: BaseViewModelOutputs{
    fun getProfileImageSuccess(): PublishSubject<GetProfileImageResponseModel>
}

class AccountTransfersViewModel @Inject constructor(
    private val sharedPrefs: SharedPreferences, private val getProfileImageUseCase: GetProfileImageUseCase
): BaseViewModel(), AccountTransfersInputs, AccountTransfersOutputs{

    override val inputs :AccountTransfersInputs = this

    override val outputs :AccountTransfersOutputs = this

    private val getProfileImageSuccess = PublishSubject.create<GetProfileImageResponseModel>()

    private val pagesList = arrayListOf(TransferOperationsFragment(), AccountTemplateFragment(), TransferHistoryFragment())

    fun getSignLevel() = sharedPrefs.getInt("signLevel", 0)

    fun getSignCount() = sharedPrefs.getInt("signCount", 0)

    fun isCustomerJuridical() = sharedPrefs.getBoolean("isCustomerJuridical", false)

    fun loggedInWithAsan() = sharedPrefs.getBoolean("loggedInWithAsan", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    fun getPages() = pagesList
    override fun getProfileImage() {
        var custCompId: Long? = null
        if (isCustomerJuridical) custCompId = compId

        // showProgress.onNext(true)

        getProfileImageUseCase.execute(custId, custCompId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                //   showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    getProfileImageSuccess.onNext(it)
                }
            }, {
                it.printStackTrace()
            })
            .addTo(subscriptions)
    }

    override fun getProfileImageSuccess() =  getProfileImageSuccess
}