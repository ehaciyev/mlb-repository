package az.turanbank.mlb.presentation.ips.accounts.myaccounts

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.IPSMLBAccountResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAccountsUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface MyLinkedAccountsInputViewModel : BaseViewModelInputs {
    fun getAccounts()
}

interface MyLinkedAccountsOutputViewModel : BaseViewModelOutputs {
    fun onAccountsSuccess(): PublishSubject<ArrayList<IPSMLBAccountResponseModel>>
}


class MyLinkedAccountsViewModel @Inject constructor(
    sharedPrefs: SharedPreferences,
    private val getMLBAccountsUseCase: GetMLBAccountsUseCase) :
    BaseViewModel(), MyLinkedAccountsInputViewModel, MyLinkedAccountsOutputViewModel {

    override val inputs: MyLinkedAccountsInputViewModel = this
    override val outputs: MyLinkedAccountsOutputViewModel = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    val accountList = PublishSubject.create<ArrayList<IPSMLBAccountResponseModel>>()


    override fun getAccounts() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        getMLBAccountsUseCase
            .execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    accountList.onNext(it.accounts)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun onAccountsSuccess() = accountList
}
