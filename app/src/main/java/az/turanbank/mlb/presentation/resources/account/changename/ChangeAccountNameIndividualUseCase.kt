package az.turanbank.mlb.presentation.resources.account.changename

import az.turanbank.mlb.data.remote.model.pg.request.ReqPaymentDataList
import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class ChangeAccountNameIndividualUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        token: String?,
        accountId: Long,
        accountName: String
    ) = mainRepository.changeAccountNameIndividual(
        custId,
        token,
        accountId,
        accountName
    )
}
