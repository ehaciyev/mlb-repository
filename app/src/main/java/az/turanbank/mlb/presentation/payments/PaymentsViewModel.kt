package az.turanbank.mlb.presentation.payments

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetProfileImageResponseModel
import az.turanbank.mlb.domain.user.usecase.GetProfileImageUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface PaymentsViewModelInputs: BaseViewModelInputs{
        fun getProfileImage()
}
interface PaymentsViewModelOutputs: BaseViewModelOutputs{
        fun getProfileImageSuccess(): PublishSubject<GetProfileImageResponseModel>
}
class PaymentsViewModel @Inject constructor(
        private val sharedPrefs: SharedPreferences,private val getProfileImageUseCase: GetProfileImageUseCase
) : BaseViewModel(),
        PaymentsViewModelInputs,
        PaymentsViewModelOutputs {

        override val inputs: PaymentsViewModelInputs = this
        override val outputs: PaymentsViewModelOutputs = this

        val token = sharedPrefs.getString("token", "")
        val custId = sharedPrefs.getLong("custId", 0L)
        val compId = sharedPrefs.getLong("compId", 0L)
        private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
        private val getProfileImageSuccess = PublishSubject.create<GetProfileImageResponseModel>()

        override fun getProfileImage() {
                var custCompId: Long? = null
                if (isCustomerJuridical) custCompId = compId

                // showProgress.onNext(true)

                getProfileImageUseCase.execute(custId, custCompId, token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                                //   showProgress.onNext(false)
                                if(it.status.statusCode == 1) {
                                        getProfileImageSuccess.onNext(it)
                                }
                        }, {
                                it.printStackTrace()
                        })
                        .addTo(subscriptions)
        }

        override fun getProfileImageSuccess() =  getProfileImageSuccess

}