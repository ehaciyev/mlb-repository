package az.turanbank.mlb.presentation.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView

import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.login.asan.juridical.LoginAsanJuridicalActivity
import az.turanbank.mlb.presentation.login.username.LoginUsernamePasswordActivity

class LoginJuridicalFragment : Fragment() {
    private lateinit var loginUsername: CardView
    private val intentUsername by lazy {
        Intent(requireActivity(), LoginUsernamePasswordActivity::class.java)
    }
    private lateinit var loginAsan: CardView
    private val intentAsan by lazy {
        Intent(requireActivity(), LoginAsanJuridicalActivity::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login_juridical, container, false)

        loginUsername = view.findViewById(R.id.withUsername)
        loginAsan = view.findViewById(R.id.withAsan)

        intentUsername.putExtra("customerType", "juridical")
        handleListeners()
        return view
    }

    private fun handleListeners() {
        loginUsername.setOnClickListener { startActivity(intentUsername) }
        loginAsan.setOnClickListener { startActivity(intentAsan) }

    }
}
