package az.turanbank.mlb.presentation.cardoperations.cashbycode

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.cardoperations.SaveCashByCodeTempUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import javax.inject.Inject

interface SuccessCashByCodeViewModelInputs: BaseViewModelInputs {
    fun saveTemp(tempName: String, requestorCardNumber: String, destinationPhoneNumber: String, amount: Double, currency: String)
}
interface SuccessCashByCodeViewModelOutputs: BaseViewModelOutputs {
    fun saveTempSuccess(): CompletableSubject
}
class SuccessCashByCodeViewModel @Inject constructor(
    private val saveCashByCodeTempUseCase: SaveCashByCodeTempUseCase,
    private val sharedPrefs: SharedPreferences
): BaseViewModel(),
    SuccessCashByCodeViewModelInputs,
    SuccessCashByCodeViewModelOutputs{
    override val inputs: SuccessCashByCodeViewModelInputs = this

    override val outputs: SuccessCashByCodeViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)

    private val savingCompleted = CompletableSubject.create()

    override fun saveTemp(
        tempName: String,
        requestorCardNumber: String,
        destinationPhoneNumber: String,
        amount: Double,
        currency: String
    ) {
        saveCashByCodeTempUseCase
            .execute(custId, tempName, requestorCardNumber, destinationPhoneNumber, amount, currency, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    savingCompleted.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            },{

            }).addTo(subscriptions)
    }

    override fun saveTempSuccess() = savingCompleted

    fun getLang():String?{
        return sharedPrefs.getString("lang","az")
    }

}