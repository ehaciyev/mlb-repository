package az.turanbank.mlb.presentation.resources.account

import android.content.Context
import android.os.Bundle
import android.util.Base64
import android.view.*
import android.widget.ImageView
import androidx.appcompat.widget.ActionMenuView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class AccountTransfersFragment : BaseFragment() {

    lateinit var viewmodel: AccountTransfersViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var tabs : TabLayout

    lateinit var viewPager: ViewPager

    lateinit var accountTransfersAdapter: AccountTransfersPagerAdapter

    private var tabPosition = 0

    private var fromHome = false
    private var fromTemp = false

    private lateinit var amvMenu: ActionMenuView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_account_transfers, container, false)

        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbar2)
        amvMenu = toolbar.findViewById(R.id.amvMenu)

        viewmodel = ViewModelProvider(this, factory) [AccountTransfersViewModel::class.java]

        viewPager = view.findViewById(R.id.accountTransfersViewPager)

        tabs = view.findViewById(R.id.accountTransfersTabs)

        accountTransfersAdapter =
            AccountTransfersPagerAdapter(
                viewmodel.getPages(),
                childFragmentManager,
                requireContext()
            )

        viewPager.adapter = accountTransfersAdapter
        tabs.setupWithViewPager(viewPager)

        tabOperations()

        setOutputListener()
        setInputListener()

        return view
    }

    private fun setOutputListener() {
        viewmodel.outputs.getProfileImageSuccess().subscribe {
            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)
            val navImage = requireActivity().findViewById<ImageView>(R.id.imageView)
            Glide
                .with(this)
                .load(byteArray)
                .centerCrop()
                .into(navImage)
        }.addTo(subscriptions)
    }

    private fun setInputListener() {
        viewmodel.inputs.getProfileImage()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getBoolean("fromHome")?.let {
            fromHome = it
        }

        arguments?.getBoolean("fromTemp")?.let {
            fromTemp = it
        }
    }

    private fun tabOperations() {
        if( fromHome) {
            tabs.getTabAt(2)?.select()
        }
        if( fromTemp) {
            tabs.getTabAt(1)?.select()
        }
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tabPosition = tab!!.position
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (amvMenu.menu.size() == 0)
            inflater.inflate(R.menu.transfer_history_menu, amvMenu.menu)
        amvMenu.menu.findItem(R.id.transfer_history_filter).isVisible = tabPosition == 2
        super.onCreateOptionsMenu(menu, inflater)
    }
}
