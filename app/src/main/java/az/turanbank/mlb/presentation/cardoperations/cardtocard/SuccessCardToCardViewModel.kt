package az.turanbank.mlb.presentation.cardoperations.cardtocard

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.cardoperations.SaveCardOperationTempUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import javax.inject.Inject

interface SuccessCardToCardViewModelInputs: BaseViewModelInputs{
    fun saveTemp(tempName: String, requestorCardNumber: String, destinationCardNumber: String, amount: Double, currency: String, cardOperationType: Int)
}
interface SuccessCardToCardViewModelOutputs: BaseViewModelOutputs{
    fun saveTempSuccess(): CompletableSubject
}
class SuccessCardToCardViewModel @Inject constructor(
    private val saveCardOperationTempUseCase: SaveCardOperationTempUseCase,
    private val sharedPrefs: SharedPreferences
): BaseViewModel(),
        SuccessCardToCardViewModelInputs,
        SuccessCardToCardViewModelOutputs{
    override val inputs: SuccessCardToCardViewModelInputs = this

    override val outputs: SuccessCardToCardViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)

    private val savingCompleted = CompletableSubject.create()

    override fun saveTemp(
        tempName: String,
        requestorCardNumber: String,
        destinationCardNumber: String,
        amount: Double,
        currency: String,
        cardOperationType: Int
    ) {
        saveCardOperationTempUseCase
            .execute(custId, tempName, requestorCardNumber, destinationCardNumber, amount, currency, cardOperationType, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    savingCompleted.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            },{

            }).addTo(subscriptions)
    }

    override fun saveTempSuccess() = savingCompleted

    fun getLang(): String?{
        return sharedPrefs.getString("lang","az")
    }
}