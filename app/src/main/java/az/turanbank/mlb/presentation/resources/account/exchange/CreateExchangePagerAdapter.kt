package az.turanbank.mlb.presentation.resources.account.exchange

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.resources.account.exchange.buy.BuyForeignCurrencyFragment

private val TAB_TITLES = arrayOf(
    R.string.foreign_currency_buy,
    R.string.foreign_currency_sell
)
class CreateExchangePagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getItem(position: Int): Fragment {
            val bundle = Bundle()

            when (position) {
                0 -> {
                    bundle.putString("from", "create")
                    bundle.putInt("type", 0)
                }
                1-> {
                    bundle.putString("from", "create")
                    bundle.putInt("type", 1)
                }
                else -> {
                    bundle.putString("from", "create")
                    bundle.putInt("type", 0)
                }
            }

            val fragment = BuyForeignCurrencyFragment()
            fragment.arguments = bundle

            return fragment
        }

    override fun getPageTitle(position: Int) =
        context.resources.getString(TAB_TITLES[position])


    override fun getCount() = 2
}