package az.turanbank.mlb.presentation.profile

import android.content.Context
import android.content.SharedPreferences
import az.turanbank.mlb.presentation.base.BaseViewModel
import javax.inject.Inject

class LanguageChangeViewModel @Inject constructor(private val context: Context, private val sharedPrefs: SharedPreferences): BaseViewModel() {
    fun getLang() = sharedPrefs.getString("lang", "az")

    fun setLang(lang: String) {
       sharedPrefs.edit().putString("lang", lang).apply()
    }
}