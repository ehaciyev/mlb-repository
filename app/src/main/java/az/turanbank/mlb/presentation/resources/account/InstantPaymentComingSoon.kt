package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.EnumLangType
import kotlinx.android.synthetic.main.activity_payment_result.*
import javax.inject.Inject

class InstantPaymentComingSoon : BaseActivity() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instant_payment_coming_soon)

        val lang = sharedPreferences.getString("lang","az")
        var enumLangType = EnumLangType.AZ

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }


    }
}
