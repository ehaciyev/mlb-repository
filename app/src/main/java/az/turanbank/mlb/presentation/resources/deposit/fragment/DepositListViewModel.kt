package az.turanbank.mlb.presentation.resources.deposit.fragment

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.deposit.DepositListModel
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetIndividualDepositListUseCase
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetJuridicalDepositListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface DepositListViewModelInputs : BaseViewModelInputs {
    fun getDeposits()
}

interface DepositListViewModelOutputs : BaseViewModelOutputs {
    fun onDepositListGot(): PublishSubject<ArrayList<DepositListModel>>
}

class DepositListViewModel @Inject constructor(
    private val getIndividualDepositListUseCase: GetIndividualDepositListUseCase,
    private val getJuridicalDepositListUseCase: GetJuridicalDepositListUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
    DepositListViewModelInputs,
    DepositListViewModelOutputs {
    override val inputs: DepositListViewModelInputs = this

    override val outputs: DepositListViewModelOutputs = this

    private val depositList = PublishSubject.create<ArrayList<DepositListModel>>()

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    override fun getDeposits() {
        if (isCustomerJuridical) {
            getJuridicalDepositListUseCase.execute(custId, compId, token, EnumLangType.EN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        depositList.onNext(it.deposits)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },
                    {
                        it.printStackTrace()
                        error.onNext(1878)
                    }).addTo(subscriptions)
        } else {
            getIndividualDepositListUseCase.execute(custId, token,EnumLangType.EN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        depositList.onNext(it.deposits)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },
                    {
                        it.printStackTrace()
                        error.onNext(1878)
                    }).addTo(subscriptions)
        }
    }

    override fun onDepositListGot() = depositList

}