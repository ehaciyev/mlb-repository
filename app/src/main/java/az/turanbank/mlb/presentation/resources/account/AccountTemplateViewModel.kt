package az.turanbank.mlb.presentation.resources.account

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountTemplateResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.GetOperationTempListIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetOperationTempListJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.resources.account.abroad.AccountOperationAbroadActivity
import az.turanbank.mlb.presentation.resources.account.exchange.ConvertMoneyActivity
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface OperationTemplateViewModelInputs : BaseViewModelInputs {
    fun getTemplateList()
}

interface OperationTemplateViewModelOutputs : BaseViewModelOutputs {
    fun onTemplateListSuccess(): PublishSubject<ArrayList<AccountTemplateResponseModel>>
}

class OperationTemplateViewModel @Inject constructor(
    private val getOperationTempListIndividualUseCase: GetOperationTempListIndividualUseCase,
    private val getOperationTempListJuridicalUseCase: GetOperationTempListJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    OperationTemplateViewModelInputs,
    OperationTemplateViewModelOutputs {

    override val inputs: OperationTemplateViewModelInputs = this

    override val outputs: OperationTemplateViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang", "az")
    var enumLangType = EnumLangType.AZ

    private val templateList = PublishSubject.create<ArrayList<AccountTemplateResponseModel>>()

    private var templates = arrayListOf<AccountTemplateResponseModel>()

    override fun getTemplateList() {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        if (isCustomerJuridical) {
            getOperationTempListJuridicalUseCase
                .execute(custId, compId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        templateList.onNext(it.respOperationTempList)
                        templates = it.respOperationTempList
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getOperationTempListIndividualUseCase
                .execute(custId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        templateList.onNext(it.respOperationTempList)
                        templates = it.respOperationTempList
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }
    fun getIntentWithData(activity: Activity, template: AccountTemplateResponseModel): Intent {
        lateinit var intent: Intent

        when (template.operNameId) {
            1 -> {
                intent = Intent(activity, AccountOperationInternalActivity::class.java)
            }
            2 -> {
                intent = Intent(activity, AccountOperationInLandActivity::class.java)
            }
            3 ->{
                intent = Intent(activity, AccountOperationAbroadActivity::class.java)
            }
            4 ->{
                intent = Intent(activity, ConvertMoneyActivity::class.java)
            }
        }
        intent.putExtra("template", template)
        return intent
    }

    override fun onTemplateListSuccess() = templateList
}