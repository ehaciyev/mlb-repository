package az.turanbank.mlb.presentation.resources.account

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.resources.account.AccountTemplateResponseModel
import az.turanbank.mlb.data.remote.model.response.BankInfoModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.adapter.GetBankListAdapter
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_operation_in_land.*
import javax.inject.Inject

class AccountOperationInLandActivity : BaseActivity() {

    lateinit var viewModel: AccountOperationInLandViewModel

    lateinit var dialog: MlbProgressDialog

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private var selectedAccountId: String = ""
    private lateinit var textWatcher: TextWatcher

    private val accountList = arrayListOf<AccountListModel>()
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_operation_in_land)

        toolbar_title.text = getString(R.string.domestic_transfer)

        toolbar_back_button.setOnClickListener { onBackPressed() }

        opInCrAccId.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(28))

        dialog = MlbProgressDialog(this)

        viewModel =
            ViewModelProvider(this, factory)[AccountOperationInLandViewModel::class.java]

        buttonEnabled(false)
        initTextWatcher()

        setOutputListeners()
        setInputListeners()
    }

    private fun initTextWatcher() {
        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                if (opInLandCrAccTaxNo.text.toString().length in 1..9) {
                    opInLandCrAccTaxNoContainer.background = ContextCompat.getDrawable(
                        this@AccountOperationInLandActivity,
                        R.drawable.incorrect_input_item_background
                    )
                    buttonEnabled(false)
                } else if (opInCrAccId.text.toString().length in 1..27) {
                    opInLandCrAccIdContainer.background = ContextCompat.getDrawable(
                        this@AccountOperationInLandActivity,
                        R.drawable.incorrect_input_item_background
                    )
                    buttonEnabled(false)
                } else {
                    opInLandCrAccTaxNoContainer.background = ContextCompat.getDrawable(
                        this@AccountOperationInLandActivity,
                        R.drawable.input_item_background
                    )
                    opInLandCrAccIdContainer.background = ContextCompat.getDrawable(
                        this@AccountOperationInLandActivity,
                        R.drawable.input_item_background
                    )

                    if (opInLandBudgetClassCodeContainer.visibility == View.VISIBLE) {
                        if (!opInLandAccountId.text.isNullOrEmpty() &&
                            !crBankId.text.isNullOrEmpty() &&
                            !opInLandBankIban.text.isNullOrEmpty() &&
                            !opInLandCorrespondentAcc.text.isNullOrEmpty() &&
                            !opInLandSwift.text.isNullOrEmpty() &&
                            !opInCrAccId.text.isNullOrEmpty() &&
                            !opInCrAccName.text.isNullOrEmpty() &&
                            (opInLandAmount.text.toString().toDouble() > 0.00) &&
                            !opInLandAmount.text.isNullOrEmpty() &&
                            !opInLandPurpose.text.isNullOrEmpty() &&
                            !opInLandBudgetClassCode.text.isNullOrEmpty() &&
                            !opInLandBudgetLevelCode.text.isNullOrEmpty()
                        ) {

                            buttonEnabled(true)
                            submitButton.setOnClickListener {
                                val intent = Intent(
                                    this@AccountOperationInLandActivity,
                                    AccountOperationInLandVerifyActivity::class.java
                                )

                                intent.putExtra("dtAccountId", selectedAccountId)
                                intent.putExtra(
                                    "opInLandAccountId",
                                    opInLandAccountId.text.toString().split(" ")[0]
                                )
                                intent.putExtra("crBankId", crBankId.text.toString())
                                intent.putExtra(
                                    "opInLandBankIban",
                                    opInLandBankIban.text.toString()
                                )
                                intent.putExtra(
                                    "opInLandCorrespondentAcc",
                                    opInLandCorrespondentAcc.text.toString()
                                )
                                intent.putExtra("opInLandSwift", opInLandSwift.text.toString())
                                intent.putExtra("opInCrAccId", opInCrAccId.text.toString())
                                intent.putExtra("opInCrAccName", opInCrAccName.text.toString())
                                intent.putExtra(
                                    "opInLandCrAccTaxNo",
                                    opInLandCrAccTaxNo.text.toString()
                                )
                                intent.putExtra("opInLandAmount", opInLandAmount.text.toString())
                                intent.putExtra("opInLandPurpose", opInLandPurpose.text.toString())
                                intent.putExtra(
                                    "opInLandExtraInfo",
                                    opInLandExtraInfo.text.toString()
                                )
                                intent.putExtra(
                                    "opInLandAzipsSwitch",
                                    opInLandAzipsSwitch.isChecked
                                )
                                intent.putExtra("operationType", getOperationType())
                                intent.putExtra("currency", opInLandAccountId.text.toString().split(" ")[2])
                                intent.putExtra("bankCode", viewModel.getSelectedBankCode())
                                intent.putExtra(
                                    "budgetCode",
                                    viewModel.getSelectedBudgetClassCode()
                                )
                                intent.putExtra("budgetLvl", viewModel.getSelectedBudgetLevelCode())

                                if (opInLandBudgetClassCodeContainer.visibility == View.VISIBLE) {
                                    intent.putExtra(
                                        "opInLandBudgetClassCode",
                                        opInLandBudgetClassCode.text.toString()
                                    )
                                    intent.putExtra(
                                        "opInLandBudgetLevelCode",
                                        opInLandBudgetLevelCode.text.toString()
                                    )
                                }

                                startActivityForResult(intent, 7)
                            }
                        } else {
                            buttonEnabled(false)
                        }
                    } else {
                        if (!opInLandAccountId.text.isNullOrEmpty() &&
                            !crBankId.text.isNullOrEmpty() &&
                            !opInLandBankIban.text.isNullOrEmpty() &&
                            !opInLandCorrespondentAcc.text.isNullOrEmpty() &&
                            !opInLandSwift.text.isNullOrEmpty() &&
                            !opInCrAccId.text.isNullOrEmpty() &&
                            !opInCrAccName.text.isNullOrEmpty() &&
                            !opInLandAmount.text.isNullOrEmpty() &&
                            !opInLandPurpose.text.isNullOrEmpty()
                        ) {
                            buttonEnabled(true)
                            submitButton.setOnClickListener {
                                val intent = Intent(
                                    this@AccountOperationInLandActivity,
                                    AccountOperationInLandVerifyActivity::class.java
                                )

                                intent.putExtra("dtAccountId", selectedAccountId)
                                intent.putExtra(
                                    "opInLandAccountId",
                                    opInLandAccountId.text.toString().split(" ")[0]
                                )
                                intent.putExtra("crBankId", crBankId.text.toString())
                                intent.putExtra(
                                    "opInLandBankIban",
                                    opInLandBankIban.text.toString()
                                )
                                intent.putExtra(
                                    "opInLandCorrespondentAcc",
                                    opInLandCorrespondentAcc.text.toString()
                                )
                                intent.putExtra("opInLandSwift", opInLandSwift.text.toString())
                                intent.putExtra("opInCrAccId", opInCrAccId.text.toString())
                                intent.putExtra("opInCrAccName", opInCrAccName.text.toString())
                                intent.putExtra(
                                    "opInLandCrAccTaxNo",
                                    opInLandCrAccTaxNo.text.toString()
                                )
                                intent.putExtra("currency", opInLandAccountId.text.toString().split(" ")[2])
                                intent.putExtra("opInLandAmount", opInLandAmount.text.toString())
                                intent.putExtra("opInLandPurpose", opInLandPurpose.text.toString())
                                intent.putExtra(
                                    "opInLandExtraInfo",
                                    opInLandExtraInfo.text.toString()
                                )
                                intent.putExtra(
                                    "opInLandAzipsSwitch",
                                    opInLandAzipsSwitch.isChecked
                                )
                                intent.putExtra("operationType", getOperationType())
                                intent.putExtra("bankCode", viewModel.getSelectedBankCode())
                                intent.putExtra(
                                    "budgetCode",
                                    viewModel.getSelectedBudgetClassCode()
                                )
                                intent.putExtra("budgetLvl", viewModel.getSelectedBudgetLevelCode())

                                if (opInLandBudgetClassCodeContainer.visibility == View.VISIBLE) {
                                    intent.putExtra(
                                        "opInLandBudgetClassCode",
                                        opInLandBudgetClassCode.text.toString()
                                    )
                                    intent.putExtra(
                                        "opInLandBudgetLevelCode",
                                        opInLandBudgetLevelCode.text.toString()
                                    )
                                }

                                startActivityForResult(intent, 7)
                            }
                        } else {
                            buttonEnabled(false)
                        }
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        opInLandAccountId.addTextChangedListener(textWatcher)
        crBankId.addTextChangedListener(textWatcher)
        opInLandBankIban.addTextChangedListener(textWatcher)
        opInLandCorrespondentAcc.addTextChangedListener(textWatcher)
        opInLandSwift.addTextChangedListener(textWatcher)
        opInCrAccId.addTextChangedListener(textWatcher)
        opInCrAccName.addTextChangedListener(textWatcher)
        opInLandAmount.addTextChangedListener(textWatcher)
        opInLandPurpose.addTextChangedListener(textWatcher)
        opInLandBudgetLevelCode.addTextChangedListener(textWatcher)
        opInLandBudgetClassCode.addTextChangedListener(textWatcher)
    }

    private fun getOperationType(): Int {
        return if (opInLandAzipsSwitch.isChecked) 3
        else 2
    }

    private fun checkFromTemplateOrNot() {
        if (intent.getSerializableExtra("template") != null) {
            deleteTemp.visibility = View.VISIBLE
            fillFormAlongWithTemplate(intent.getSerializableExtra("template") as AccountTemplateResponseModel)
        } else {
            deleteTemp.visibility = View.GONE
        }
    }

    private fun showDeleteTemp(tempId: Long) {

        val alert = androidx.appcompat.app.AlertDialog.Builder(this)
        alert.setMessage(getString(R.string.template_delete_confirmation_message))

        alert.setCancelable(true)
        alert.setPositiveButton(
            getString(R.string.yes)
        ) { _, _ ->
            viewModel.inputs.deleteTemp(tempId)
        }

        alert.setNegativeButton(
            getString(R.string.no)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }

    @SuppressLint("SetTextI18n")
    private fun fillFormAlongWithTemplate(template: AccountTemplateResponseModel) {

        deleteTemp.setOnClickListener {
            showDeleteTemp(template.id)
        }
        opInLandAzipsSwitch.isChecked = template.operationTypeId == 3
        opInLandAzipsSwitch.isClickable = false
        for (i in 0 until accountList.size) {
            if (accountList[i].iban == template.dtIban) {
                opInLandAccountId.setText(template.dtIban + " / " + template.dtCcy)
                break
            }
        }

        val templateBank =
            crBankId.adapter.getItem(viewModel.getBankPosition(template.crBankCode)) as BankInfoModel
        crBankId.setText(templateBank.bankNameFull)

        opInLandBankIban.setText(templateBank.bankTaxid)
        opInLandCorrespondentAcc.setText(templateBank.bankCorrAcc)
        opInLandSwift.setText(templateBank.bankSwift)
        opInCrAccId.setText(template.crIban)
        opInLandAmount.setText(template.amt.toString())
        viewModel.setSelectedBankCode(templateBank.bankCode)
        opInCrAccName.setText(template.crCustName)
        opInLandCrAccTaxNo.setText(template.crCustTaxid)
        opInLandPurpose.setText(template.operPurpose)
        opInLandExtraInfo.setText(template.note)
        when (templateBank.bankCode) {
            "210005" -> {
                opInLandBudgetClassCodeContainer.visibility = View.VISIBLE
                opInLandBudgetLevelCodeContainer.visibility = View.VISIBLE
                opInLandAzipsSwitchContainer.visibility = View.GONE
                opInCrAccName.isEnabled = false
                opInLandCrAccTaxNo.isEnabled = false

                opInLandBudgetLevelCode.setText(template.budgetLvl)
                opInLandBudgetClassCode.setText(template.budgetCode)
                opInCrAccId.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        if (s.toString().length == 28) {
                            viewModel.inputs.getBudgetAccountByIban(s.toString().trim())
                        }
                    }

                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {

                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {

                    }
                })
                viewModel.inputs.getBudgetClassificationCode()
                viewModel.inputs.getBudgetLevelCode()
            }
            "210027" -> {
                opInLandAzipsSwitchContainer.visibility = View.GONE
                opInCrAccName.isEnabled = true
                opInLandCrAccTaxNo.isEnabled = true
            }
            else -> {
                opInCrAccName.isEnabled = true
                opInLandCrAccTaxNo.isEnabled = true
                opInLandBudgetClassCodeContainer.visibility = View.GONE
                opInLandBudgetLevelCodeContainer.visibility = View.GONE
                opInLandAzipsSwitchContainer.visibility = View.VISIBLE
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent(this, AccountOperationInternalResultActivity::class.java)

                intent.putExtra("dtAccountId", viewModel.getSelectedBankCode().toLong())
                intent.putExtra("crIban", opInCrAccId.text.toString())
                intent.putExtra("dtIban", opInLandAccountId.text.toString())
                intent.putExtra("amount", opInLandAmount.text.toString())
                intent.putExtra("accountName", opInCrAccName.text.toString())
                intent.putExtra("operationNo", data?.getStringExtra("operationNo"))
                intent.putExtra("operationStatus", data?.getStringExtra("operationStatus"))
                intent.putExtra("accountResponse", data?.getSerializableExtra("accountResponse"))

                startActivity(intent)
                finish()
            }
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onTemplateDeleted().subscribe {
            Toast.makeText(this, getString(R.string.template_successfully_deleted), Toast.LENGTH_LONG).show()
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.showGetBudgetClassificationCodeProgress().subscribe {
            showProgressImages(opInLandBudgetClassCodeProgress, it)
        }.addTo(subscriptions)

        viewModel.outputs.showGetBudgetLevelCodeProgress().subscribe {
            showProgressImages(opInLandBudgetLevelCodeProgress, it)
        }.addTo(subscriptions)

        viewModel.outputs.crAccountTaxNoProgress().subscribe {
            showProgressImages(opInLandCrAccTaxNoProgress, it)
        }.addTo(subscriptions)

        viewModel.outputs.crAccountNameProgress().subscribe {
            showProgressImages(opInCrAccNameProgress, it)
        }.addTo(subscriptions)

        viewModel.outputs.onAccountSuccess().subscribe { accountListModel ->

            accountList.clear()
            accountList.addAll(accountListModel)
            var selectedAccountCurr: String
            var selectedAccountIban: String

//            val accountIdFromIntent = intent.getLongExtra("accountId", 0L)
            val accountIbanFromIntent = intent.getStringExtra("accountIban")
            val accountCurrNameFromIntent = intent.getStringExtra("currName")

            val accountNameArray = arrayOfNulls<String>(accountListModel.size)
            selectedAccountId = accountListModel[0].accountId.toString()
            selectedAccountIban =
                accountListModel[0].iban + " / " + accountListModel[0].currName
            selectedAccountCurr = accountListModel[0].currName

            for (i in 0 until accountListModel.size) {
                if (!(accountCurrNameFromIntent.isNullOrEmpty()) && (accountListModel[i].iban == accountIbanFromIntent)) {
                    selectedAccountId = intent.getLongExtra("accountId", 0L).toString()
                    // opInLandAccountId.setText(accountIbanFromIntent + " / " + accountCurrNameFromIntent)
                    // opInLandAccountId.setText(accountIbanFromIntent + " / " + accountCurrNameFromIntent)
                    selectedAccountIban = "$accountIbanFromIntent / $accountCurrNameFromIntent"
                    opInLandAccountId.setText(selectedAccountIban)
                    selectedAccountCurr = accountCurrNameFromIntent
                    break
                }
            }

            for (i in 0 until accountListModel.size) {
                accountNameArray[i] =
                    (accountListModel[i].iban + " / " + accountListModel[i].currName)
            }

            viewModel.inputs.getBankList(selectedAccountCurr)
            /*
            if (accountIdFromIntent != 0L && !accountIbanFromIntent.isNullOrEmpty()) {
                selectedAccountId = intent.getLongExtra("accountId", 0L).toString()
                // opInLandAccountId.setText(accountIbanFromIntent + " / " + accountCurrNameFromIntent)
                selectedAccountIban = "$accountIbanFromIntent / $accountCurrNameFromIntent"
                opInLandAccountId.setText(selectedAccountIban)
                selectedAccountCurr = accountCurrNameFromIntent!!
            } else {
                selectedAccountId = accountListModel[0].accountId.toString()
                selectedAccountIban =
                    accountListModel[0].iban + " / " + accountListModel[0].currName
                opInLandAccountId.setText(selectedAccountIban)
                selectedAccountCurr = accountListModel[0].currName
            }*/

            // opInLandAccountId.setText(selectedAccountIban)

            if (opInLandAccountId.text.toString().split(" ")[0].endsWith("944")) {
                opInLandAzipsSwitchContainer.visibility = View.VISIBLE
            } else {
                opInLandAzipsSwitchContainer.visibility = View.GONE
            }

            if (intent.getSerializableExtra("template") != null) {
                selectedAccountCurr =
                    (intent.getSerializableExtra("template") as AccountTemplateResponseModel).amtCcy
            }
            viewModel.inputs.getBankList(selectedAccountCurr)

            opInLandDtAccountIdContainer.setOnClickListener {

                val builder =
                    AlertDialog.Builder(this@AccountOperationInLandActivity)
                builder.setTitle("Hesab seçin")

                /* val accountNameArray = arrayOfNulls<String>(accountListModel.size)
                 val b: AlertDialog.Builder =
                     AlertDialog.Builder(this@AccountOperationInLandActivity)
                 b.setTitle("Hesab seçin")

                 for (i in 0 until accountListModel.size) {
                     accountNameArray[i] =
                         (accountListModel[i].iban + " / " + accountListModel[i].currName)
                 }*/
                builder.setItems(accountNameArray) { _, which ->
                    opInLandAccountId.setText(accountNameArray[which])

                    if (accountNameArray[which]!!.split(" ")[0].endsWith("944")) {
                        opInLandAzipsSwitchContainer.visibility = View.VISIBLE
                    } else {
                        opInLandAzipsSwitchContainer.visibility = View.GONE
                    }
                    selectedAccountId = accountListModel[which].accountId.toString()

                    selectedAccountIban = accountListModel[which].accountId.toString()
                    selectedAccountCurr = accountListModel[which].currName
                    viewModel.inputs.getBankList(selectedAccountCurr)
                }
                val dialog = builder.create()
                dialog.show()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onGetBankListSuccess().subscribe {
            val adapter = GetBankListAdapter(this, it)

            crBankId.threshold = 1
            crBankId.setAdapter(adapter)
            crBankId.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                    opInLandBankIban.setText(it[position].bankTaxid)
                    opInLandCorrespondentAcc.setText(it[position].bankCorrAcc)
                    opInLandSwift.setText(it[position].bankSwift)

                    viewModel.setSelectedBankCode(it[position].bankCode)

                    when (it[position].bankCode) {
                        "210005" -> {
                            opInLandBudgetClassCodeContainer.visibility = View.VISIBLE
                            opInLandBudgetLevelCodeContainer.visibility = View.VISIBLE
                            opInLandAzipsSwitchContainer.visibility = View.GONE

                            opInCrAccName.isEnabled = false
                            opInLandCrAccTaxNo.isEnabled = false

                            opInCrAccId.addTextChangedListener(object : TextWatcher {
                                override fun afterTextChanged(s: Editable?) {
                                    if (s.toString().length == 28) {
                                        viewModel.inputs.getBudgetAccountByIban(s.toString().trim())
                                    }
                                }

                                override fun beforeTextChanged(
                                    s: CharSequence?,
                                    start: Int,
                                    count: Int,
                                    after: Int
                                ) {

                                }

                                override fun onTextChanged(
                                    s: CharSequence?,
                                    start: Int,
                                    before: Int,
                                    count: Int
                                ) {

                                }
                            })
                            viewModel.inputs.getBudgetClassificationCode()
                            viewModel.inputs.getBudgetLevelCode()
                        }
                        "210027" -> {
                            opInLandAzipsSwitchContainer.visibility = View.GONE
                            opInCrAccName.isEnabled = true
                            opInLandCrAccTaxNo.isEnabled = true
                        }
                        else -> {
                            opInCrAccName.isEnabled = true
                            opInLandCrAccTaxNo.isEnabled = true
                            opInLandBudgetClassCodeContainer.visibility = View.GONE
                            opInLandBudgetLevelCodeContainer.visibility = View.GONE
                            opInLandAzipsSwitchContainer.visibility = View.VISIBLE
                        }
                    }
                }
            checkFromTemplateOrNot()
        }.addTo(subscriptions)

        viewModel.outputs.getBudgetAccountByIbanSuccess().subscribe {
            opInCrAccName.setText(it.name)
            opInLandCrAccTaxNo.setText(it.taxNumber)
        }.addTo(subscriptions)

        viewModel.outputs.getBudgetClassificationCodeSuccess().subscribe { budgetCodeList ->

            opInLandBudgetClassCode.isFocusable = true
            val accountNameArray = arrayOfNulls<String>(budgetCodeList.size)

            for (i in 0 until budgetCodeList.size) {
                accountNameArray[i] = (budgetCodeList[i].budgetValue)
            }

            val adapter =
                ArrayAdapter<String>(this, android.R.layout.select_dialog_item, accountNameArray)
            opInLandBudgetClassCode.setAdapter(adapter)

            opInLandBudgetClassCode.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                    viewModel.setSelectedBudgetClassCode(budgetCodeList[position].budgetId)
                }

        }.addTo(subscriptions)

        viewModel.outputs.getBudgetLevelCodeSuccess().subscribe { budgetCodeList ->
            opInLandBudgetLevelCode.isFocusable = true

            val accountNameArray = arrayOfNulls<String>(budgetCodeList.size)

            for (i in 0 until budgetCodeList.size) {
                accountNameArray[i] = (budgetCodeList[i].budgetValue)
            }

            val adapter =
                ArrayAdapter<String>(this, android.R.layout.select_dialog_item, accountNameArray)
            opInLandBudgetLevelCode.setAdapter(adapter)

            opInLandBudgetLevelCode.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                    viewModel.setSelectedBudgetLevelCode(budgetCodeList[position].budgetId)
                }
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun showProgressImages(progressImage: ImageView, show: Boolean) {
        hideProgressImages(progressImage, show)

        if (show) {
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }

    private fun buttonEnabled(show: Boolean) {
        if (!show) {
            submitButton.isFocusable = false
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
        } else {
            submitButton.isFocusable = true
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
    }

    private fun hideProgressImages(progressImage: ImageView, hide: Boolean) {
        if (hide) {
            progressImage.visibility = View.GONE
        } else {
            progressImage.visibility = View.VISIBLE
        }
    }

    private fun revertProgressImageAnimation(progressImage: ImageView) {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage(progressImage: ImageView) {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun setInputListeners() {
        viewModel.inputs.getAccountList()
    }
}
