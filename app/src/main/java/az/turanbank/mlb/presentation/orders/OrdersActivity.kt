package az.turanbank.mlb.presentation.orders

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.orders.ui.main.SectionsPagerAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class OrdersActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orders)

        toolbar_back_button.setOnClickListener { onBackPressed() }

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs2)
        tabs.setupWithViewPager(viewPager)
    }
}