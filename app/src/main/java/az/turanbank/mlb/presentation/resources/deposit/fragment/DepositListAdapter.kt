package az.turanbank.mlb.presentation.resources.deposit.fragment

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.deposit.DepositListModel
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.formatDecimal
import az.turanbank.mlb.util.secondPartOfMoney
import kotlinx.android.synthetic.main.card_list_view.view.*

class DepositListAdapter (private val depositList: ArrayList<DepositListModel>, private val clickListener: (position: Int) -> Unit) : RecyclerView.Adapter<DepositListAdapter.DepositViewHolder>() {
    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): DepositViewHolder =
        DepositViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_list_view,
                parent,
                false
            )
        )

    override fun getItemCount() = depositList.size

    override fun onBindViewHolder(holder: DepositViewHolder, position: Int) {
        holder.bind(depositList[position], clickListener)
    }
    class DepositViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(deposit: DepositListModel, clickListener: (position: Int) -> Unit) {

            with(deposit) {
                itemView.cardName.text = this.name
                itemView.cardNumber.text = this.branchName
                itemView.cardAmount.text = firstPartOfMoney(this.amount)
                itemView.cardAmountReminder.text = secondPartOfMoney(this.amount)
                itemView.currency.text = this.currency
                itemView.setOnClickListener {
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}