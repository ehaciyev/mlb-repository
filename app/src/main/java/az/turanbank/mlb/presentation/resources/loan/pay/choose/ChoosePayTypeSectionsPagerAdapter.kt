package az.turanbank.mlb.presentation.resources.loan.pay.choose

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.resources.loan.pay.choose.account.NoAccountsFragment
import az.turanbank.mlb.presentation.resources.loan.pay.choose.card.UnblockCardsFragment

private val TAB_TITLES = arrayOf(
    R.string.from_card,
    R.string.from_account
    // R.string.operations
)
private val TAB_FRAGMENTS = arrayOf(
    UnblockCardsFragment(),
    NoAccountsFragment()
)

class ChoosePayTypeSectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return TAB_FRAGMENTS[position].apply {
            arguments = Bundle().apply {
                putBoolean("fromChoose", true)
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return 2
    }
}