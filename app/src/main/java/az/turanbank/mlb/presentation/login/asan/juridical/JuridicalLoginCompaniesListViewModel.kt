package az.turanbank.mlb.presentation.login.asan.juridical

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.CompanyModel
import az.turanbank.mlb.data.remote.model.response.CustomerResponseModel
import az.turanbank.mlb.domain.user.usecase.register.asan.SelectCustCompanyUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.JuridicalRegisterGetCompaniesCertUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface JuridialLoginCompaniesListViewModelInputs : BaseViewModelInputs {
    fun getCompaniesCert(
        pin: String,
        mobile: String,
        userId: String
    )

    fun selectCustCompany(
        pin: String,
        taxNo: String,
        mobile: String,
        userId: String,
        device: String,
        location: String
    )

    fun setSignCount(signCount: Int)

    fun setSignLevel(signCount: Int)
}

interface JuridialLoginCompaniesListViewModelOutputs : BaseViewModelOutputs {
    fun companies(): Observable<List<CompanyModel>>
    fun custCompany(): Observable<CustomerResponseModel>
}

class JuridialLoginCompaniesListViewModel @Inject constructor(
    private val juridicalRegisterGetCompaniesCertUseCase: JuridicalRegisterGetCompaniesCertUseCase,
    private val selectCustCompanyUseCase: SelectCustCompanyUseCase,
    private val sharedPreferences: SharedPreferences
) :
    BaseViewModel(),
    JuridialLoginCompaniesListViewModelInputs,
    JuridialLoginCompaniesListViewModelOutputs
{

    override fun setSignCount(signCount: Int) {
        sharedPreferences.edit().putInt("signCount", signCount).apply()
    }

    override fun setSignLevel(signCount: Int) {
        sharedPreferences.edit().putInt("signLevel", signCount).apply()
    }

    override fun selectCustCompany(
        pin: String,
        taxNo: String,
        mobile: String,
        userId: String,
        device: String,
        location: String
    ) {
        selectCustCompanyUseCase.execute(pin, taxNo, mobile, userId, device, location)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    sharedPreferences.edit().putLong("compId", it.company.id).apply()
                    sharedPreferences.edit().putString("companyName", it.company.name).apply()
                    sharedPreferences.edit().putString("token", it.customer.token).apply()
                    sharedPreferences.edit().putBoolean("isCustomerJuridical", true).apply()
                    sharedPreferences.edit().putBoolean("loggedInWithAsan", true).apply()
                    sharedPreferences.edit().putString("certCode", it.company.certCode).apply()
                    sharedPreferences.edit().putString("phoneNumber", mobile).apply()
                    sharedPreferences.edit().putString("userId", userId).apply()
                    custCompany.onNext(it.customer)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            })
            .addTo(subscriptions)
    }

    override fun custCompany() = custCompany

    override val inputs: JuridialLoginCompaniesListViewModelInputs = this
    override val outputs: JuridialLoginCompaniesListViewModelOutputs = this

    private val companies = PublishSubject.create<List<CompanyModel>>()
    private val custCompany = PublishSubject.create<CustomerResponseModel>()

    override fun getCompaniesCert(pin: String, mobile: String, userId: String) {
        juridicalRegisterGetCompaniesCertUseCase.execute(pin, mobile, userId, 1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    companies.onNext(it.company)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun companies() = companies
}