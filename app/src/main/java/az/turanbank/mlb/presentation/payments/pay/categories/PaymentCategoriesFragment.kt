package az.turanbank.mlb.presentation.payments.pay.categories


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.pg.response.payments.category.CategoryResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.payments.pay.subcategories.PaymentMerchantsActivity
import com.facebook.shimmer.ShimmerFrameLayout
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject


class PaymentCategoriesFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: PaymentCategoriesViewModel

    lateinit var adapter: PaymentCategoriesAdapter

    lateinit var viewsContainer: GridView

    /*lateinit var progress: ImageView*/

    lateinit var failMessage: TextView

    lateinit var mShimmerViewContainer: ShimmerFrameLayout

    private val categories = arrayListOf<CategoryResponseModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_payment_categories, container, false)

        viewModel =
            ViewModelProvider(this, factory)[PaymentCategoriesViewModel::class.java]

        viewsContainer = view.findViewById(R.id.container)

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container)

        /*progress = view.findViewById(R.id.progress)*/

        failMessage = view.findViewById(R.id.failMessage)

        //animateProgressImage()
        adapter = PaymentCategoriesAdapter(requireContext(), categories){
            val intent = Intent(requireActivity(), PaymentMerchantsActivity::class.java)

            intent.putExtra("categoryId", categories[it].categoryId)
            intent.putExtra("categoryName", categories[it].categoryName)
            intent.putExtra("displayName", categories[it].displayName)
            startActivityForResult(intent,7)
        }

        viewsContainer.adapter = adapter

        setOutputListeners()
        setInputListeners()

        return view
    }

    private fun setInputListeners() {
        viewModel.inputs.getCategories()
    }

    private fun setOutputListeners() {
        viewModel.outputs.categoryNames().subscribe {categories ->
            this.categories.clear()
            this.categories.addAll(categories)
            adapter.notifyDataSetChanged()

            // stop animating Shimmer and hide the layout
            mShimmerViewContainer.stopShimmerAnimation()
            mShimmerViewContainer.visibility = View.GONE

            /*progress.clearAnimation()
            progress.visibility = View.GONE*/
            failMessage.visibility = View.GONE
            viewsContainer.visibility = View.VISIBLE
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            viewsContainer.visibility = View.GONE
            mShimmerViewContainer.visibility = View.GONE
            failMessage.visibility = View.VISIBLE
            /*progress.clearAnimation()
            progress.visibility = View.GONE*/
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
        }.addTo(subscriptions)
    }
    private fun animateProgressImage() {
        viewsContainer.visibility = View.GONE
        failMessage.visibility = View.GONE
        /*progress.visibility = View.VISIBLE*/
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

       /* progress.startAnimation(rotate)*/
    }

    override fun onResume() {
        super.onResume()
        mShimmerViewContainer.startShimmerAnimation();
    }

    override fun onPause() {
        super.onPause()
        mShimmerViewContainer.stopShimmerAnimation();
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                requireActivity().setResult(Activity.RESULT_OK)
                requireActivity().finish()
            }
        }
    }
}
