package az.turanbank.mlb.presentation.resources.account

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.register.InstantPaymentSplashActivity
import az.turanbank.mlb.presentation.resources.account.abroad.AccountOperationAbroadActivity
import az.turanbank.mlb.presentation.resources.account.exchange.ConvertMoneyActivity

class TransferOperationsFragment : BaseFragment() {

    //lateinit var viewModel: TransferOperationsViewModel

    override fun onResume() {
        super.onResume()
        setHasOptionsMenu(isVisible)
        setMenuVisibility(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_transfer_operations, container, false)

        val operationsInternalCardView = view.findViewById<CardView>(R.id.operationsInternal)
        val operationsDomesticCardView = view.findViewById<CardView>(R.id.operationsDomestic)
        val operationsOutlandCardView = view.findViewById<CardView>(R.id.operationsOutland)
        val convert = view.findViewById<CardView>(R.id.convert)
        val instantPaymentsContainer = view.findViewById<CardView>(R.id.instantPaymentsContainer)

        operationsInternalCardView.setOnClickListener {
            startActivityForResult(Intent(requireContext(), AccountOperationInternalActivity::class.java).apply {
                putExtra("accountId", requireActivity().intent.getLongExtra("accountId", 0L))
                putExtra("accountIban", requireActivity().intent.getStringExtra("accountIban"))
                putExtra("currName", requireActivity().intent.getStringExtra("currName"))
            },7)
        }

        operationsDomesticCardView.setOnClickListener {
            startActivityForResult(Intent(requireContext(), AccountOperationInLandActivity::class.java).apply {
                putExtra("accountId", requireActivity().intent.getLongExtra("accountId", 0L))
                putExtra("accountIban", requireActivity().intent.getStringExtra("accountIban"))
                putExtra("currName", requireActivity().intent.getStringExtra("currName"))
            },7)
        }
        operationsOutlandCardView.setOnClickListener {
            startActivityForResult(Intent(requireContext(), AccountOperationAbroadActivity::class.java).apply {
                putExtra("accountId", requireActivity().intent.getLongExtra("accountId", 0L))
                putExtra("accountIban", requireActivity().intent.getStringExtra("accountIban"))
                putExtra("currName", requireActivity().intent.getStringExtra("currName"))
            },7)
        }
        convert.setOnClickListener {
            startActivityForResult(Intent(requireContext(), ConvertMoneyActivity::class.java).apply {
                putExtra("accountId", requireActivity().intent.getLongExtra("accountId", 0L))
                putExtra("accountIban", requireActivity().intent.getStringExtra("accountIban"))
                putExtra("currName", requireActivity().intent.getStringExtra("currName"))
            },7)
        }

        instantPaymentsContainer.setOnClickListener{
            //startActivity(Intent(requireActivity(), InstantPaymentComingSoon::class.java))
            startActivityForResult(Intent(requireActivity(), InstantPaymentSplashActivity::class.java)
                , 7)
        }
        return view
    }

    /*companion object {
        @JvmStatic
        fun newInstance() = TransferOperationsFragment()
    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                requireActivity().setResult(Activity.RESULT_OK, Intent())
                requireActivity().finish()
            }
        }
    }
}

