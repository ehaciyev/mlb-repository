package az.turanbank.mlb.presentation.register.mobile.individual

import az.turanbank.mlb.domain.user.usecase.register.asan.RegisterIndividualAsanUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.RegisterJuridicalAsanUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.RegisterIndividualMobileUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.RegisterJuridicalMobileUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface IndividualRegisterMobileSubmitViewModelInputs : BaseViewModelInputs {
    fun submitForms(
        username: String,
        password: String,
        repeatPassword: String,
        custId: Long,
        registrationType: String
    )

    fun registerJuridicalCustomer(
        pin: String,
        taxNo: String,
        username: String,
        password: String,
        repeatPassword: String
    )

    fun registerJuridicalAsan(
        pin: String,
        taxNo: String,
        custId: Long,
        compId: Long,
        username: String,
        password: String,
        repeatPassword: String
    )
}

interface IndividualRegisterMobileSubmitViewModelOutputs : BaseViewModelOutputs {
    fun registrationOnSuccess(): Observable<Boolean>
    fun showProgress(): PublishSubject<Boolean>
}

class RegisterIndividualViewModel @Inject constructor(
    private val registerMobileJuridical: RegisterJuridicalMobileUseCase,
    private val registerIndividualMobileUseCase: RegisterIndividualMobileUseCase,
    private val registerIndividualAsanUseCase: RegisterIndividualAsanUseCase,
    private val registerJuridicalAsanUseCase: RegisterJuridicalAsanUseCase
) : BaseViewModel(),
    IndividualRegisterMobileSubmitViewModelInputs,
    IndividualRegisterMobileSubmitViewModelOutputs {

    override val inputs: IndividualRegisterMobileSubmitViewModelInputs = this
    override val outputs: IndividualRegisterMobileSubmitViewModelOutputs = this
    val showProgress = PublishSubject.create<Boolean>()

    override fun submitForms(
        username: String,
        password: String,
        repeatPassword: String,
        custId: Long,
        registrationType: String
    ) {

        showProgress.onNext(true)
        when (registrationType) {
            "asan" ->
                registerIndividualAsanUseCase.execute(username, password, repeatPassword, custId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showProgress.onNext(false)
                        if (it.status.statusCode == 1) {
                            success.onNext(true)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                    }).addTo(subscriptions)

            "mobile" ->
                registerIndividualMobileUseCase.execute(
                    username,
                    password,
                    repeatPassword,
                    custId
                ).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it.status.statusCode == 1) {
                            showProgress.onNext(false)
                            success.onNext(true)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    }).addTo(subscriptions)
        }
    }

    override fun registerJuridicalCustomer(
        pin: String,
        taxNo: String,
        username: String,
        password: String,
        repeatPassword: String
    ) {
        showProgress.onNext(true)

        registerMobileJuridical.execute(
            pin, taxNo, username, password, repeatPassword
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)

                success.onNext(true)
                if (it.status.statusCode == 1) {
                    success.onNext(true)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }


    override fun registrationOnSuccess() = success

    override fun showProgress() = showProgress

    override fun registerJuridicalAsan(
        pin: String,
        taxNo: String,
        custId: Long,
        compId: Long,
        username: String,
        password: String,
        repeatPassword: String
    ) {
        showProgress.onNext(true)

        registerJuridicalAsanUseCase.execute(
            pin,
            taxNo,
            custId,
            compId,
            username,
            password,
            repeatPassword
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    success.onNext(true)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }
}