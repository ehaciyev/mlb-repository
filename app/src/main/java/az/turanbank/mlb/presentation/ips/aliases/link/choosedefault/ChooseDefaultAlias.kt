package az.turanbank.mlb.presentation.ips.aliases.link.choosedefault

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.ips.AliasAccountCompoundModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.WarningPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_choose_default_alias.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class ChooseDefaultAlias : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ChooseDefaultAliasViewModel

    lateinit var adapter: ChooseDefaultAdapter

    private val aliases = arrayListOf<AliasAccountCompoundModel>()

    val linkedAccountList = arrayListOf<AliasAccountCompoundModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_default_alias)

        viewModel = ViewModelProvider(this, factory)[ChooseDefaultAliasViewModel::class.java]

        toolbar_title.text = getString(R.string.link_alias)
        toolbar_back_button.setOnClickListener { onBackPressed() }



        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm

        adapter = ChooseDefaultAdapter(aliases) { position ->
            /*aliases.forEach {
                if (aliases[position].alias == it.alias){
                    linkedAccountList.add(it)
                }
            }*/
        }

        recyclerView.adapter = adapter

        //animateProgress()
        recyclerViewShimmer.visibility = View.VISIBLE
        recyclerViewShimmer.startShimmerAnimation()

        setOutputListener()
        setInputListener()

        submitButton.setOnClickListener {
            if(adapter.getSelectedPosition() != null){
                val intent = Intent(this, WarningPageViewActivity::class.java)
                val description =  getString(R.string.alias_linked_default, adapter.getSelectedItem()!!.alias)
                intent.putExtra("fromDeleteAlias", true)
                intent.putExtra("description", description)
                startActivityForResult(intent, 7)
            } else {
                AlertDialogMapper(this, 8000).showAlertDialog()
            }
        }
    }

    private fun setInputListener() {
        viewModel.inputs.getCompounds()
    }

    private fun setOutputListener() {
        viewModel.outputs.onCompounds().subscribe {
            recyclerViewShimmer.stopShimmerAnimation()
            recyclerViewShimmer.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            message.text = getString(R.string.alias_linked_to_account_default, it[0].alias)
            aliases.clear()
            aliases.addAll(it)
            recyclerView.adapter?.notifyDataSetChanged()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            recyclerViewShimmer.stopShimmerAnimation()
            recyclerViewShimmer.visibility = View.GONE
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }

    override fun onPause() {
        super.onPause()
        recyclerViewShimmer.stopShimmerAnimation()
        recyclerViewShimmer.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK, Intent())
                finish()
            }
        }
    }
}