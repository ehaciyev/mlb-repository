package az.turanbank.mlb.presentation.resources.account

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class AccountStatementsTabbedActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: AccountStatementsTabbedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_statements_tabbed)

        viewModel = ViewModelProvider(this, factory)[AccountStatementsTabbedViewModel::class.java]

        val TAB_TITLES = arrayOf(
            getString(R.string.full),
            getString(R.string.import_text),
            getString(R.string.export_text)
        )

        val sectionsPagerAdapter =
            AccountStatementsPagerAdapter(applicationContext,TAB_TITLES,
                supportFragmentManager
            )
        val viewPager: ViewPager = findViewById(R.id.accountStatementsViewPager)


        toolbar_back_button.setOnClickListener { onBackPressed() }
        toolbar_title.text = getString(R.string.account_statement)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.accountStatementsTabs)
        tabs.setupWithViewPager(viewPager)
    }
}
