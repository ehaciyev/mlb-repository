package az.turanbank.mlb.presentation.resources.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseFragment

class AuthorizationsFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_authorizations, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AuthorizationsFragment().apply {

            }
    }
}
