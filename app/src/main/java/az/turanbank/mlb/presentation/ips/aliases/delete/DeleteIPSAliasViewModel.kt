package az.turanbank.mlb.presentation.ips.aliases.delete

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.aliases.GetIpsAccountByAliasUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.DeleteIPSAliasUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAliasesUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.lang.Exception
import javax.inject.Inject

interface DeleteIPSAliasViewModelInputs : BaseViewModelInputs {
    fun getAliases()
    fun deleteAlias(aliasId: ArrayList<Long>)
    //fun getDefaultAccount(type: String, value: String)
}

interface DeleteIPSAliasViewModelOutputs : BaseViewModelOutputs {
    fun onAliases(): PublishSubject<ArrayList<MLBAliasResponseModel>>
    fun aliasDeleted(): PublishSubject<Boolean>
    fun noAliasFound(): PublishSubject<Boolean>
    //fun defaultAccountFound(): PublishSubject<String>
    fun unSuccessfullAliases(): PublishSubject<ArrayList<MLBAliasResponseModel>>
}

class DeleteIPSAliasViewModel @Inject constructor(
    private val getMLBAliasesUseCase: GetMLBAliasesUseCase,
    private val deleteIPSAliasUseCase: DeleteIPSAliasUseCase,
    private val getIpsAccountByAliasUseCase: GetIpsAccountByAliasUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    DeleteIPSAliasViewModelInputs,
    DeleteIPSAliasViewModelOutputs {
    override val inputs: DeleteIPSAliasViewModelInputs = this

    override val outputs: DeleteIPSAliasViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val aliases = PublishSubject.create<ArrayList<MLBAliasResponseModel>>()
    private val unSuccessAliase = PublishSubject.create<ArrayList<MLBAliasResponseModel>>()

    private val aliasDeleted = PublishSubject.create<Boolean>()
    private val aliasDefaultAccount = PublishSubject.create<String>()

    private val noAliasFound = PublishSubject.create<Boolean>()

    override fun getAliases() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        getMLBAliasesUseCase.execute(custId, compId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ aliases ->
                if (aliases.status.statusCode == 1) {
                    this.aliases.onNext(aliases.aliases)
                } else {
                    error.onNext(aliases.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun deleteAlias(aliasId: ArrayList<Long>) {

        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        deleteIPSAliasUseCase
            .execute(compId, custId, token, aliasId, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it.status.statusCode) {
                    1 -> {
                        aliasDeleted.onNext(true)
                    }
                    309-> {
                        unSuccessAliase.onNext(it.aliases)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    /*override fun getDefaultAccount(type: String, value: String) {
        getIpsAccountByAliasUseCase
            .execute(custId, token, type, value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    aliasDefaultAccount.onNext(it.respIpsAccountList[0].id.iban)
                } else if (it.status.statusCode == 126||it.status.statusCode == 33311) {
                    //aliasDefaultAccount.onNext("no")
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)

    }*/

    override fun onAliases() = aliases
    override fun aliasDeleted() = aliasDeleted
    override fun noAliasFound(): PublishSubject<Boolean> {
        return noAliasFound
    }

    //override fun defaultAccountFound() = aliasDefaultAccount
    override fun unSuccessfullAliases() = unSuccessAliase
}