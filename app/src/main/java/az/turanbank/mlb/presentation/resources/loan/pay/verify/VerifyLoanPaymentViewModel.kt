package az.turanbank.mlb.presentation.resources.loan.pay.verify

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.loan.LoanPaymentResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.loan.pay.LoanPaymentIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.loan.pay.LoanPaymentJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface VerifyPaymentLoanViewModelInputs : BaseViewModelInputs {
    fun payLoan(payType: Int, dtAccountId: Long, loanId: Long, amount: Double)
}

interface VerifyPaymentLoanViewModelOutputs : BaseViewModelOutputs {
    fun onPayLoanSuccess(): PublishSubject<LoanPaymentResponseModel>
}

class VerifyLoanPaymentViewModel @Inject constructor(
    private val loanPaymentJuridicalUseCase: LoanPaymentJuridicalUseCase,
    private val loanPaymentIndividualUseCase: LoanPaymentIndividualUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    VerifyPaymentLoanViewModelInputs,
    VerifyPaymentLoanViewModelOutputs {
    override val inputs: VerifyPaymentLoanViewModelInputs = this

    override val outputs: VerifyPaymentLoanViewModelOutputs = this

    private val paymentResponse = PublishSubject.create<LoanPaymentResponseModel>()

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    override fun payLoan(payType: Int, dtAccountId: Long, loanId: Long, amount: Double) {
        if (isCustomerJuridical) {
            loanPaymentJuridicalUseCase
                .execute(custId, compId, token, payType, dtAccountId, loanId, amount)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        paymentResponse.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(100)
                }).addTo(subscriptions)
        } else {
            loanPaymentIndividualUseCase
                .execute(custId, token, payType, dtAccountId, loanId, amount)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        paymentResponse.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(100)
                }).addTo(subscriptions)
        }
    }

    override fun onPayLoanSuccess() = paymentResponse
}