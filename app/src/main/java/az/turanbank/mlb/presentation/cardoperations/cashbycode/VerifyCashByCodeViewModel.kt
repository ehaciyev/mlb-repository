package az.turanbank.mlb.presentation.cardoperations.cashbycode

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.cardoperations.CashByCodeResponseModel
import az.turanbank.mlb.domain.user.usecase.cardoperations.CashByCodeUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface VerifyCashByCodeViewModelInputs : BaseViewModelInputs {
    fun submitCashByCode(
        requestorCardId: Long,
        destinationPhoneNumber: String,
        currency: String,
        amount: Double
    )
}

interface VerifyCashByCodeViewModelOutputs : BaseViewModelOutputs {
    fun onSuccessCashByCode(): PublishSubject<CashByCodeResponseModel>
}

class VerifyCashByCodeViewModel @Inject constructor(
    private val cashByCodeUseCase: CashByCodeUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    VerifyCashByCodeViewModelInputs, VerifyCashByCodeViewModelOutputs {

    override val inputs: VerifyCashByCodeViewModelInputs = this
    override val outputs: VerifyCashByCodeViewModelOutputs = this

    val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val cashByCode = PublishSubject.create<CashByCodeResponseModel>()

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun submitCashByCode(
        requestorCardId: Long,
        destinationPhoneNumber: String,
        currency: String,
        amount: Double
    ) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        cashByCodeUseCase
            .execute(custId, requestorCardId, destinationPhoneNumber, currency, amount, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    cashByCode.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {

            }).addTo(subscriptions)
    }

    override fun onSuccessCashByCode() = cashByCode
}