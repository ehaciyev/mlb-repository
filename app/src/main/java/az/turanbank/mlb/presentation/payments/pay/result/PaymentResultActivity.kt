package az.turanbank.mlb.presentation.payments.pay.result

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.param.MerchantParamListResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.pay.PayResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.home.MlbHomeActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_payment_result.*
import kotlinx.android.synthetic.main.add_to_templates_dialog_view.view.*
import java.util.*
import javax.inject.Inject

class PaymentResultActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: PaymentResultViewModel

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    private val payment: PayResponseModel by lazy { intent.getSerializableExtra("payment") as PayResponseModel }

    private val identificationCode: ArrayList<IdentificationCodeRequestModel> by lazy { intent.getSerializableExtra("identificationCode") as ArrayList<IdentificationCodeRequestModel> }
    private val merchantId: Long by lazy { intent.getLongExtra("merchantId", 0L) }
    private val identificationType: String by lazy { intent.getStringExtra("identificationType") }
    private val currency: String by lazy { intent.getStringExtra("currency") }
    private val providerId: Long by lazy { intent.getLongExtra("providerId", 0L) }
    private val amountText: Double by lazy { intent.getDoubleExtra("amount", 0.0) }
    private val categoryId: Long by lazy { intent.getLongExtra("categoryId", 0L) }
    private val feeCalculationMethod: String by lazy { intent.getStringExtra("feeCalculationMethod") }
    private val isFromHistory: Boolean by lazy { intent.getBooleanExtra("isFromHistory",false) }


    private val arrayGlobal: ArrayList<MerchantParamListResponseModel> = arrayListOf<MerchantParamListResponseModel>()

    private val parametersList = arrayListOf<IdentificationCodeRequestModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_result)

        viewModel = ViewModelProvider(this, factory)[PaymentResultViewModel::class.java]
        close.setOnClickListener {
            MlbHomeActivity().finish()
            startActivityForResult(
                Intent(
                    this,
                    MlbHomeActivity::class.java
                ).putExtra("fromTemplate", false), 7
            )
            finish()
        }

        if(payment.specialCode.isNullOrEmpty()){
            specialCodeContainer.visibility = View.GONE
        }

        if(isFromHistory){
            linearLayout13.visibility = View.GONE
        } else {
            linearLayout13.visibility = View.VISIBLE
        }

        specialCode.text = payment.specialCode
        receiptNo.text = payment.receiptNumber
        date.text = payment.billingDate
        amount.text = payment.amount.toString()
        if(payment.description.isNullOrEmpty()){

        } else {
            if(payment.description!!.split(";").size >1){
                //identificationCode
                if(payment.description!!.split(";")[1].split(":").size>1){
                    note.text = payment.description!!.split(";")[1].split(":")[1]
                } else {
                    note.text = payment.description!!.split(";")[1]
                }
            } else {
                note.text = payment.description!!.split(";")[0]// identificationType
            }

        }
        serviceName.text = payment.merchantName
        card.text = payment.cardNumber

        when (sharedPreferences.getString("lang", "az")) {
            "az" -> {
                paid_stamp.setImageResource(R.drawable.pechat_az)
            }
            "en" -> {
                paid_stamp.setImageResource(R.drawable.pechat_eng)
            }
            "ru" -> {
                paid_stamp.setImageResource(R.drawable.pechat_rus)
            }
        }


        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        addToTemplate.setOnClickListener { showSaveTempEditText() }

        share.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, generateShareString())

            startActivity(Intent.createChooser(shareIntent, getString(R.string.send)))
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.saveTemplateSuccess().subscribe {
            //MlbHomeActivity().finish()
            startActivityForResult(Intent(this, MlbHomeActivity::class.java).putExtra("fromTemplate", true),7)
            finish()
        }.addTo(subscriptions)

        viewModel.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
            progress.clearAnimation()
            progress.visibility = View.GONE
            cardView.visibility = View.VISIBLE
        }.addTo(subscriptions)

    }

    private fun showSaveTempEditText() {

        val alert = AlertDialog.Builder(this)
        alert.setTitle(getString(R.string.template_name))

        val view =
            LayoutInflater.from(this).inflate(R.layout.add_to_templates_dialog_view, null, false)
        alert.setView(view)

        alert.setCancelable(false)
        alert.setPositiveButton(
            getString(R.string.save_all_caps)
        ) { _, _ ->
            if (!TextUtils.isEmpty(view.tempNameEditText.text.toString())) {
                animateProgressImage()
                viewModel.inputs.saveTemplate(
                    view.tempNameEditText.text.toString(),
                    merchantId,
                    identificationType,
                    identificationCode,
                    amountText,
                    currency,
                    providerId,
                    categoryId
                )
            }
        }

        alert.setNegativeButton(
            getString(R.string.cancel_all_caps)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }

    private fun generateShareString(): String {
        return getString(R.string.payment) + "\n" +
                getString(R.string.card_to_card_share_oper_number) + payment.receiptNumber + "\n" +
                getString(R.string.share_oper_date) + payment.billingDate + "\n" +
                getString(R.string.card_to_card_share_debitor_card) + payment.cardNumber + "\n" +
                getString(R.string.card_to_card_share_amount) + payment.amount + "\n" +
                getString(R.string.service) + ": ${payment.merchantName}" + "\n"
    }
    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        cardView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}