package az.turanbank.mlb.presentation.resources.loan.activity

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.loan.LoanStatementResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.loan.statement.LoanStatementIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.loan.statement.LoanStatementJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface LoanViewModelInputs: BaseViewModelInputs{
    fun getTheLoan(loanId: Long)
}
interface LoanViewModelOutputs: BaseViewModelOutputs{
    fun onStatementSuccess(): PublishSubject<LoanStatementResponseModel>
}
class LoanViewModel @Inject constructor(
    private val loanStatementIndividualUseCase: LoanStatementIndividualUseCase,
    private val loanStatementJuridicalUseCase: LoanStatementJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    LoanViewModelInputs,
    LoanViewModelOutputs {

    override val inputs: LoanViewModelInputs = this

    override val outputs: LoanViewModelOutputs = this

    private val statement = PublishSubject.create<LoanStatementResponseModel>()

    var loanTypeId = 0

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    override fun getTheLoan(loanId: Long) {
        if (isCustomerJuridical){
            loanStatementJuridicalUseCase
                .execute(custId, loanId, compId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        statement.onNext(it)
                        loanTypeId = it.loanTypeId
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                }).addTo(subscriptions)
        } else {
            loanStatementIndividualUseCase
                .execute(custId, loanId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        statement.onNext(it)
                        loanTypeId = it.loanTypeId
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                }).addTo(subscriptions)
        }
    }
    override fun onStatementSuccess() = statement
}