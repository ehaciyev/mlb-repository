package az.turanbank.mlb.presentation.payments.template

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.response.payments.template.PaymentTemplateResponseModel
import kotlinx.android.synthetic.main.card_template_list_view.view.*
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class PaymentTemplateAdapter constructor(
    private val templateList: ArrayList<PaymentTemplateResponseModel>,
    private val onClickListeners: (position: Int) -> Unit
) : RecyclerView.Adapter<PaymentTemplateAdapter.PaymentTemplateViewHolder>() {
    class PaymentTemplateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(template: PaymentTemplateResponseModel, onClickListeners: (position: Int) -> Unit){

            val decimalFormat = DecimalFormat("0.00")
            val symbols = DecimalFormatSymbols()
            symbols.decimalSeparator = '.'
            decimalFormat.decimalFormatSymbols = symbols


            itemView.name.text = template.tempName
            itemView.cardNumber.text = template.createdDate

            itemView.amountInteger.text = decimalFormat.format(template.amount).split(".")[0]+","
            itemView.amountReminder.text = decimalFormat.format(template.amount).split(".")[1] + template.currency
            itemView.operationType.visibility = View.GONE

            itemView.setOnClickListener { onClickListeners.invoke(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PaymentTemplateViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.card_template_list_view,
            parent,
            false
        )
    )

    override fun getItemCount() = templateList.size

    override fun onBindViewHolder(holder: PaymentTemplateViewHolder, position: Int) {
        holder.bind(templateList[position], onClickListeners)
    }
}