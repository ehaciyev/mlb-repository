package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.response.*
import az.turanbank.mlb.domain.user.usecase.resources.account.*
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface EditOperationInLandViewModelInputs : BaseViewModelInputs {
    fun getOperationById(operId: Long)
    fun getAccountList()
    fun getBankList(ccy: String)
    fun getBudgetClassificationCode()
    fun getBudgetLevelCode()
    fun getBudgetAccountByIban(iban: String)
    fun updateOperationById(dtAccountId: Long,
                            crIban: String,
                            crCustTaxid: String,
                            crCustName: String,
                            crBankCode: String,
                            crBankName: String,
                            crBankTaxid: String,
                            crBankCorrAcc: String,
                            budgetCode: String?,
                            budgetLvl: String?,
                            amount: Double,
                            purpose: String,
                            note: String,
                            operationType: Int,
                            operId: Long,
                            operNameId: Long)
}

interface EditOperationInLandViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun onAccountSuccess(): BehaviorSubject<ArrayList<AccountListModel>>
    fun operationDetails(): PublishSubject<GetOperationByIdResponseModel>
    fun onGetBankListSuccess(): BehaviorSubject<ArrayList<BankInfoModel>>
    fun getBudgetClassificationCodeSuccess(): PublishSubject<ArrayList<BudgetCode>>
    fun getBudgetLevelCodeSuccess(): PublishSubject<ArrayList<BudgetCode>>
    fun getBudgetAccountByIbanSuccess(): PublishSubject<BudgetAccountModel>
    fun showGetBudgetClassificationCodeProgress(): PublishSubject<Boolean>
    fun showGetBudgetLevelCodeProgress(): PublishSubject<Boolean>
    fun operationUpdated(): CompletableSubject
}

class EditOperationInLandViewModel @Inject constructor(
    private val getIndividualAccountListUseCase: GetNoCardAccountListForIndividualCustomerUseCase,
    private val getJuridicalAccountListUseCase: GetNoCardAccountListForJuridicalCustomerUseCase,
    private val getBankListForIndividualUseCase: GetBankListForIndividualUseCase,
    private val getBankListForJuridicalUseCase: GetBankListForJuridicalUseCase,
    private val getBudgetLevelCodeUseCase: GetBudgetLevelCodeUseCase,
    private val getBudgetClassificationCodeUseCase: GetBudgetClassificationCodeUseCase,
    private val getBudgetAccountByIbanUseCase: GetBudgetAccountByIbanUseCase,
    private val getOperationByIdUseCase: GetOperationByIdUseCase,
    private val updateOperationByIdUseCase: UpdateOperationByIdUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(), EditOperationInLandViewModelInputs, EditOperationInLandViewModelOutputs {

    override fun updateOperationById(
        dtAccountId: Long,
        crIban: String,
        crCustTaxid: String,
        crCustName: String,
        crBankCode: String,
        crBankName: String,
        crBankTaxid: String,
        crBankCorrAcc: String,
        budgetCode: String?,
        budgetLvl: String?,
        amount: Double,
        purpose: String,
        note: String,
        operationType: Int,
        operId: Long,
        operNameId: Long
    ) {
        showProgress.onNext(true)

        var custCompId: Long? = null

        if(isCustomerJuridical) {
            custCompId = compId
        }
        updateOperationByIdUseCase.execute(
            custId, custCompId, token, dtAccountId, crIban, crCustTaxid, crCustName, crBankCode, crBankName, crBankTaxid, crBankCorrAcc, budgetCode, budgetLvl, amount, purpose, note, operationType, operId, operNameId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    operationUpdated.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    override val inputs: EditOperationInLandViewModelInputs = this
    override val outputs: EditOperationInLandViewModelOutputs = this

    private var selectedBankCode: String = ""
    private var budgetClassCode: String? = null

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val operationDetails = PublishSubject.create<GetOperationByIdResponseModel>()
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val accountList = BehaviorSubject.create<ArrayList<AccountListModel>>()
    private val bankList = BehaviorSubject.create<ArrayList<BankInfoModel>>()
    private val showGetBudgetClassificationCodeProgress = PublishSubject.create<Boolean>()
    private val bankClassificationCode = PublishSubject.create<ArrayList<BudgetCode>>()
    private val showGetBudgetLevelCodeProgress = PublishSubject.create<Boolean>()
    private val bankLevelCode = PublishSubject.create<ArrayList<BudgetCode>>()
    private val showProgress = PublishSubject.create<Boolean>()
    private val getBudgetAccountByIbanProgress = PublishSubject.create<Boolean>()
    private val getBudgetAccountByIban = PublishSubject.create<BudgetAccountModel>()
    private val crAccountTaxNoProgress = PublishSubject.create<Boolean>()
    private val crAccountNameProgress = PublishSubject.create<Boolean>()
    private val operationUpdated = CompletableSubject.create()
    private var budgetLevelCode: String? = null


    fun getAccountId(iban: String): Long? {
        accountList.value?.let {
            for(account in accountList.value!!) {
                if(iban == account.iban)
                    return account.accountId
            }
        }
        return null
    }

    override fun showGetBudgetClassificationCodeProgress() = showGetBudgetClassificationCodeProgress

    fun setSelectedBankCode(value: String) {
        this.selectedBankCode = value
    }

    fun setSelectedBudgetClassCode(value: String?) {
        this.budgetClassCode = value
    }

    override fun getBudgetAccountByIbanSuccess() = getBudgetAccountByIban

    override fun getBudgetAccountByIban(iban: String) {
        crAccountTaxNoProgress.onNext(true)
        crAccountNameProgress.onNext(true)
        getBudgetAccountByIbanUseCase.execute(custId, token, iban)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                crAccountTaxNoProgress.onNext(false)
                crAccountNameProgress.onNext(false)
                getBudgetAccountByIbanProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    getBudgetAccountByIban.onNext(it.budgetAccount)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getBudgetLevelCodeSuccess() = bankLevelCode

    fun getSelectedBankCode() = selectedBankCode

    override fun showProgress() = showProgress

    override fun operationDetails() = operationDetails

    override fun operationUpdated() = operationUpdated

    override fun getOperationById(operId: Long) {
        showProgress.onNext(true)
        getOperationByIdUseCase.execute(operId, custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    operationDetails.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    override fun getAccountList() {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getJuridicalAccountListUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                lang = EnumLangType.AZ
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getIndividualAccountListUseCase.execute(custId, token,EnumLangType.AZ)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onAccountSuccess() = accountList

    override fun getBankList(ccy: String) {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getBankListForJuridicalUseCase.execute(ccy, custId, compId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        bankList.onNext(it.bankInfoList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getBankListForIndividualUseCase.execute(ccy, custId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        bankList.onNext(it.bankInfoList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onGetBankListSuccess() = bankList

    fun getSelectedBudgetClassCode() = budgetClassCode

    override fun getBudgetClassificationCodeSuccess() = bankClassificationCode

    fun setSelectedBudgetLevelCode (value: String?) {
        this.budgetLevelCode = value
    }

    fun getSelectedBudgetLevelCode() = budgetLevelCode

    override fun showGetBudgetLevelCodeProgress() = showGetBudgetLevelCodeProgress

    override fun getBudgetClassificationCode() {
        showGetBudgetClassificationCodeProgress.onNext(true)
        getBudgetClassificationCodeUseCase.execute(custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    showGetBudgetClassificationCodeProgress.onNext(false)
                    bankClassificationCode.onNext(it.budgetCodeList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getBudgetLevelCode() {
        showGetBudgetLevelCodeProgress.onNext(true)
        getBudgetLevelCodeUseCase.execute(custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    showGetBudgetLevelCodeProgress.onNext(false)
                    bankLevelCode.onNext(it.budgetCodeList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }
}