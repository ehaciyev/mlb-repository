package az.turanbank.mlb.presentation

import java.io.IOException

class NoConnectionException: IOException(){
    override val message: String? get() = "No network available, check your wifi or data connection"
}

class NoInternetException: IOException(){
    override val message: String?
        get() = "No internet available, check your connected wifi or data connection"
}