package az.turanbank.mlb.presentation.resources.activity

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.resources.account.AccountsFragment
import az.turanbank.mlb.presentation.resources.card.list.CardsFragment
import az.turanbank.mlb.presentation.resources.loan.fragment.LoansFragment
import az.turanbank.mlb.presentation.resources.deposit.fragment.DepositFragment

private val TAB_TITLES = arrayOf(
    R.string.cards,
    R.string.accounts,
    R.string.credits,
    R.string.deposits
)
private val TAB_FRAGMENTS = arrayOf(
    CardsFragment(),
    AccountsFragment(),
    LoansFragment(),
    DepositFragment()
)

//check Fragments is empty or not

class ResourcesPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return TAB_FRAGMENTS[position].apply {
            arguments = Bundle().apply {
                putBoolean("fromChoose", false)
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 4 total pages.
        return 4
    }
}