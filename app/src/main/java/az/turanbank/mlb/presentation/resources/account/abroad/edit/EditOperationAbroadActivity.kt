package az.turanbank.mlb.presentation.resources.account.abroad.edit

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_operation_outland.*
import javax.inject.Inject

class EditOperationAbroadActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: EditAccountOperationAbroadViewModel

    private var accountList = arrayListOf<AccountListModel>()

    lateinit var dialog: MlbProgressDialog

    private val operId: Long by lazy { intent.getLongExtra("operId", 0L) }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_operation_outland)

        viewModel =
            ViewModelProvider(this, factory)[EditAccountOperationAbroadViewModel::class.java]

        dialog = MlbProgressDialog(this)
        dialog.showDialog()

        toolbar_title.text = getString(R.string.international_transfer)

        toolbar_back_button.setOnClickListener {
            onBackPressed()
        }
        setInputListeners()

        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.operationUpdated().subscribe {
            Toast.makeText(this, getString(R.string.transfer_successfully_updated), Toast.LENGTH_LONG).show()
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onAccountSuccess().subscribe { accountListModel ->

            for (i in 0 until accountListModel.size) {
                if (accountListModel[i].currName != "AZN") {
                    accountList.add(accountListModel[i])
                }
            }

            if (accountList.isNotEmpty()) {

                val accountNameArray = arrayOfNulls<String>(accountList.size)
                for (i in 0 until accountList.size) {
                    accountNameArray[i] = (accountList[i].iban)
                }
                debitorAccountIdContainer.setOnClickListener {
                    val builder: AlertDialog.Builder =
                        AlertDialog.Builder(this@EditOperationAbroadActivity)
                    builder.setTitle(getString(R.string.select_account))

                    builder.setItems(accountNameArray) { _, which ->
                        debitorAccountId.setText(accountNameArray[which])
                    }
                    val dialog = builder.create()
                    dialog.show()
                }
            }

        }.addTo(subscriptions)

        viewModel.outputs.operationDetails().subscribe {
            debitorAccountId.setText(it.dtIban)
            amount.setText(it.amt.toString())
            creditorBankName.setText(it.crBankName)
            swiftEditText.setText(it.crBankSwift)
            creditorCustomerName.setText(it.crCustName)
            toAddress.setText(it.crCustAddress)
            crIban.setText(it.crIban)
            purpose.setText(it.operPurpose)
            toBranch.setText(it.crCorrBankBranch)
            toCountry.setText(it.crBankCountry)
            toCity.setText(it.crBankCity)
            creditorCustomerPhone.setText(it.crCustPhone)
            note.setText(it.note)
            mediatorBankName.setText(it.crCorrBankName)
            mediatorCountry.setText(it.crCorrBankCountry)
            mediatorCity.setText(it.crBankCorrCity)
            mediatorSwiftEditText.setText(it.crCorrBankSwift)
            reporterAccount.setText(it.crCorrBankAccount)
            mediatorBranch.setText(it.crCorrBankBranch)
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getOperationById(operId)
        viewModel.inputs.getAccountList()
        submitButton.setOnClickListener {

            if (
                debitorAccountId.text.toString().trim().isNotEmpty()
                && amount.text.toString().trim().isNotEmpty()
                && amount.text.toString().toDouble() > 0.00
                && creditorBankName.text.toString().trim().isNotEmpty()
                && swiftEditText.text.toString().trim().isNotEmpty()
                && creditorCustomerName.text.toString().trim().isNotEmpty()
                && crIban.text.toString().trim().isNotEmpty()
                && purpose.text.toString().trim().isNotEmpty()
            ) {


                viewModel.inputs.updateOperationById(
                    viewModel.getDtAccountId(debitorAccountId.text.toString()),
                    amount.text.toString().toDouble(),
                    creditorBankName.text.toString(),
                    swiftEditText.text.toString(),
                    creditorCustomerName.text.toString(),
                    toAddress.text.toString(),
                    crIban.text.toString(),
                    purpose.text.toString(),
                    toBranch.text.toString(),
                    toAddress.text.toString(),
                    toCountry.text.toString(),
                    toCity.text.toString(),
                    creditorCustomerPhone.text.toString(),
                    note.text.toString(),
                    mediatorBankName.text.toString(),
                    mediatorCountry.text.toString(),
                    mediatorCity.text.toString(),
                    mediatorSwiftEditText.text.toString(),
                    reporterAccount.text.toString(),
                    mediatorBranch.text.toString(),
                    operId,
                    3L
                )
            } else {
                checkInputs()
            }
        }
    }

    private fun checkInputs() {
        if (amount.text.toString().trim().isEmpty()) {
            amount.error = getString(R.string.amount_is_wrong)
        } else {
            amount.error = null
        }
        if (creditorBankName.text.toString().trim().isEmpty()) {
            creditorBankName.error = getString(R.string.enter_creditor_bank)
        } else {
            creditorBankName.error = null
        }
        if (swiftEditText.text.toString().trim().isEmpty()) {
            swiftEditText.error = getString(R.string.enter_creditor_bank_swift)
        } else {
            swiftEditText.error = null
        }
        if (creditorCustomerName.text.toString().trim().isEmpty()) {
            creditorCustomerName.error = getString(R.string.enter_creditor_name)
        } else {
            creditorCustomerName.error = null
        }
        if (crIban.text.toString().trim().isEmpty()) {
            crIban.error = getString(R.string.enter_creditor_iban)
        } else {
            crIban.error = null
        }
        if (purpose.text.toString().trim().isEmpty()) {
            purpose.error = getString(R.string.enter_creditor_purpose)
        } else {
            purpose.error = null
        }
    }
}
