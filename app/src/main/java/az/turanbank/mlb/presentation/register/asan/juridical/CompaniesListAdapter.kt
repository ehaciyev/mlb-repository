package az.turanbank.mlb.presentation.register.asan.juridical

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.CompanyModel
import kotlinx.android.synthetic.main.juridical_company_list_item.view.*

class CompaniesListAdapter(private val companyList: ArrayList<CompanyModel>, private val clickListener: (position: Int) -> Unit): RecyclerView.Adapter<CompaniesListAdapter.CompanyViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyViewHolder =
        CompanyViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.juridical_company_list_item,
                parent,
                false))

    override fun getItemCount(): Int = companyList.size

    override fun onBindViewHolder(holder: CompanyViewHolder, position: Int) =
        holder.bind(companyList[position], clickListener)

    class CompanyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(company: CompanyModel, clickListener: (position: Int) -> Unit) {
            with(company) {
                itemView.companyName.text = this.name
                itemView.taxNo.text = this.taxNo
                itemView.setOnClickListener {
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}