package az.turanbank.mlb.presentation.resources.account.abroad.edit

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.response.GetOperationByIdResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.*
import az.turanbank.mlb.domain.user.usecase.resources.account.abroad.UpdateAbroadByIdUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface EditAccountOperationAbroadViewModelInputs : BaseViewModelInputs {
    fun getOperationById(operId: Long)
    fun getAccountList()
    fun updateOperationById(
        dtAccountId: Long,
        amount: Double,
        crBankName: String,
        crBankSwift: String,
        crCustName: String,
        crCustAddress: String,
        crIban: String,
        purpose: String,
        crBankBranch: String,
        crBankAddress: String,
        crBankCountry: String,
        crBankCity: String,
        crCustPhone: String,
        note: String,
        crCorrBankName: String,
        crCorrBankCountry: String,
        crCorrBankCity: String,
        crCorrBankSwift: String,
        crCorrBankAccount: String,
        crCorrBankBranch: String,
        operId: Long,
        operNameId: Long
    )
}

interface EditAccountOperationAbroadViewModelOutputs : BaseViewModelOutputs {
    fun onAccountSuccess(): BehaviorSubject<ArrayList<AccountListModel>>
    fun showProgress(): PublishSubject<Boolean>
    fun operationUpdated(): CompletableSubject
    fun operationDetails(): PublishSubject<GetOperationByIdResponseModel>
}

class EditAccountOperationAbroadViewModel @Inject constructor(
    private val getIndividualAccountListUseCase: GetNoCardAccountListForIndividualCustomerUseCase,
    private val getJuridicalAccountListUseCase: GetNoCardAccountListForJuridicalCustomerUseCase,
    private val updateAbroadByIdUseCase: UpdateAbroadByIdUseCase,
    private val getOperationByIdUseCase: GetOperationByIdUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    EditAccountOperationAbroadViewModelInputs,
    EditAccountOperationAbroadViewModelOutputs {
    override val inputs: EditAccountOperationAbroadViewModelInputs = this

    override val outputs: EditAccountOperationAbroadViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val showProgress = PublishSubject.create<Boolean>()

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val accountList = BehaviorSubject.create<ArrayList<AccountListModel>>()

    private val operationUpdated = CompletableSubject.create()

    private val operationDetails = PublishSubject.create<GetOperationByIdResponseModel>()

    override fun updateOperationById(
        dtAccountId: Long,
        amount: Double,
        crBankName: String,
        crBankSwift: String,
        crCustName: String,
        crCustAddress: String,
        crIban: String,
        purpose: String,
        crBankBranch: String,
        crBankAddress: String,
        crBankCountry: String,
        crBankCity: String,
        crCustPhone: String,
        note: String,
        crCorrBankName: String,
        crCorrBankCountry: String,
        crCorrBankCity: String,
        crCorrBankSwift: String,
        crCorrBankAccount: String,
        crCorrBankBranch: String,
        operId: Long,
        operNameId: Long
    ) {
        showProgress.onNext(true)

        var custCompId: Long? = null

        if (isCustomerJuridical) {
            custCompId = compId
        }
        updateAbroadByIdUseCase.execute(
            custId,
            custCompId,
            token,
            dtAccountId,
            amount,
            crBankName,
            crBankSwift,
            crCustName,
            crCustAddress,
            crIban,
            purpose,
            crBankBranch,
            crBankAddress,
            crBankCountry,
            crBankCity,
            crCustPhone,
            note,
            crCorrBankName,
            crCorrBankCountry,
            crCorrBankCity,
            crCorrBankSwift,
            crCorrBankAccount,
            crCorrBankBranch,
            operId,
            operNameId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    operationUpdated.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    override fun operationDetails() = operationDetails

    override fun getOperationById(operId: Long) {
        showProgress.onNext(true)
        getOperationByIdUseCase.execute(operId, custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    operationDetails.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    override fun getAccountList() {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getJuridicalAccountListUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                lang = EnumLangType.AZ
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getIndividualAccountListUseCase.execute(custId, token,EnumLangType.AZ)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    fun getDtAccountId(iban: String): Long {
        accountList.value?.let {
            for (i in 0 until it.size) {
                if (it[i].iban == iban)
                    return it[i].accountId
            }
        }
        return 0L
    }

    override fun showProgress() = showProgress

    override fun onAccountSuccess() = accountList

    override fun operationUpdated() = operationUpdated
}