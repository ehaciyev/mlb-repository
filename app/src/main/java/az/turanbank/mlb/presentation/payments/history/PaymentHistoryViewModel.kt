package az.turanbank.mlb.presentation.payments.history

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.pg.response.payments.list.PaymentResponseModel
import az.turanbank.mlb.domain.user.usecase.pg.history.GetPaymentListIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.history.GetPaymentListJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface PaymentHistoryViewModelInputs : BaseViewModelInputs {
    fun getPayments()
}

interface PaymentHistoryViewModelOutputs : BaseViewModelOutputs {
    fun onPaymentListSuccess(): PublishSubject<ArrayList<PaymentResponseModel>>
}

class PaymentHistoryViewModel @Inject constructor(
    private val getPaymentListIndividualUseCase: GetPaymentListIndividualUseCase,
    private val getPaymentListJuridicalUseCase: GetPaymentListJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(), PaymentHistoryViewModelInputs, PaymentHistoryViewModelOutputs {

    override val inputs: PaymentHistoryViewModelInputs = this
    override val outputs: PaymentHistoryViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val paymentList = PublishSubject.create<ArrayList<PaymentResponseModel>>()
    override fun getPayments() {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        val respond = if (isCustomerJuridical){
            getPaymentListJuridicalUseCase.execute(custId, compId, token,enumLangType)
        } else {
            getPaymentListIndividualUseCase.execute(custId, token,enumLangType)
        }

        respond.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    paymentList.onNext(it.paymentList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {

            }).addTo(subscriptions)
    }

    override fun onPaymentListSuccess(): PublishSubject<ArrayList<PaymentResponseModel>> = paymentList
}