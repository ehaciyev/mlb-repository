package az.turanbank.mlb.presentation.resources.loan.fragment

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.loan.LoanListModel
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.formatDecimal
import az.turanbank.mlb.util.secondPartOfMoney
import kotlinx.android.synthetic.main.card_list_view.view.*

class LoanListAdapter(
    private val currencyList: ArrayList<LoanListModel>,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<LoanListAdapter.LoanViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): LoanViewHolder =
        LoanViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_list_view,
                parent,
                false
            )
        )

    override fun getItemCount() = currencyList.size

    override fun onBindViewHolder(holder: LoanViewHolder, position: Int) {
        holder.bind(currencyList[position], clickListener)
    }

    class LoanViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(loans: LoanListModel, clickListener: (position: Int) -> Unit) {

            with(loans) {
                itemView.cardName.text = this.loanType
                itemView.cardNumber.text = this.branch
                itemView.cardAmount.text = firstPartOfMoney(this.loanAmount)
                itemView.cardAmountReminder.text = secondPartOfMoney(this.loanAmount)
                itemView.currency.text =this.currency
                itemView.setOnClickListener {
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}