package az.turanbank.mlb.presentation.ips.pay.creditorchoice.moreinfo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.pay.preconfirm.TransferWithIPSActivity
import az.turanbank.mlb.util.animateProgressImage
import az.turanbank.mlb.util.revertProgressImageAnimation
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject


class AccountAndCustomerInfoActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: AccountAndCustomerInfoViewModel

    private val senderAccountOrAlias: String by lazy { intent.getStringExtra("senderAccountOrAlias") }
    private val receiverAccountOrAlias: String by lazy { intent.getStringExtra("receiverAccountOrAlias") }
    private val isGovernmentPayment: Boolean by lazy { intent.getBooleanExtra("isGovernmentPayment", false) }
    private val receiverId: Long by lazy { intent.getLongExtra("receiverId",0L ) }
    private val senderIBAN: String by lazy { intent.getStringExtra("senderIBAN") }
    private val fromAccount: Boolean by lazy { intent.getBooleanExtra("fromAccount",true) }
    private val toAccount: Boolean by lazy { intent.getBooleanExtra("toAccount",true) }
    private val receiverCustomerName: String by lazy { intent.getStringExtra("receiverCustomerName") }
    private val receiverCustomerSurname: String by lazy { intent.getStringExtra("receiverCustomerSurname") }
    private val isRegistered: Boolean by lazy { intent.getBooleanExtra("isRegistered", false) }
    private val taxCode: String by lazy { intent.getStringExtra("voenCode") }
    private val senderFullName: String by lazy { intent.getStringExtra("senderFullName") }
    private val isEnabled: Boolean by lazy { intent.getBooleanExtra("isEnabled", true) }
    private val receiverIban: String by lazy { intent.getStringExtra("receiverIban") }

    lateinit var receiverFullName: AppCompatEditText
    lateinit var voenCode: AppCompatEditText
    lateinit var submitButton: LinearLayout
    lateinit var submitText: TextView
    lateinit var progressImage: ImageView

    lateinit var receiverContainer: LinearLayout
    lateinit var voenContainer: LinearLayout

    private var businessProcessId: Long = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_and_customer_info)

        toolbar_title.text = getString(R.string.pay)
        toolbar_back_button.setOnClickListener{onBackPressed()}

        viewModel = ViewModelProvider(this,factory)[AccountAndCustomerInfoViewModel::class.java]

        initView()

        if(isRegistered){
            setDisable(true)
            receiverFullName.setText("$receiverCustomerName $receiverCustomerSurname")
            if(taxCode.isNullOrEmpty()){
                voenCode.setText(getString(R.string.no_voen_available))
            } else {
                voenCode.setText(taxCode)
            }
        } else{
            setDisable(false)
        }

        if(!isEnabled){
            setDisable(true)
            receiverFullName.setText("$receiverCustomerName $receiverCustomerSurname")
            if(taxCode.isNullOrEmpty()){
                voenCode.setText(getString(R.string.no_voen_available))
            } else {
                voenCode.setText(taxCode)
            }
        } else{
            setDisable(false)
        }

        voenCode.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(10))

        submitButton.setOnClickListener{
            showProgressBar(true)
            when {
                receiverFullName.text.toString().trim().isEmpty() -> {
                    receiverFullName.error = getString(R.string.enter_name)
                    showProgressBar(false)
                }

                voenCode.text.toString().trim().isEmpty() -> {
                    voenCode.error = getString(R.string.enter_voen)
                    showProgressBar(false)
                }
                voenCode.text.toString().trim().length < 10 -> {
                    voenCode.error = getString(R.string.voen_validation)
                    showProgressBar(false)
                }
                else -> {
                    receiverFullName.error = null
                    voenCode.error = null
                    sendIntent()
                    showProgressBar(false)
                }
            }
        }
    }

    private fun setDisable(boolean: Boolean) {
        receiverContainer.isFocusable = !boolean
        receiverFullName.isFocusable = !boolean

        voenContainer.isFocusable = !boolean
        voenCode.isFocusable = !boolean

    }

    private fun initView() {
        receiverFullName = findViewById(R.id.receiverFullName)
        submitButton = findViewById(R.id.submitButton)
        submitText = findViewById(R.id.submitText)
        voenCode = findViewById(R.id.voenCode)
        progressImage = findViewById(R.id.progressImage)
        receiverContainer = findViewById(R.id.receiverContainer)
        voenContainer = findViewById(R.id.voenContainer)
    }

    private fun sendIntent(){
        val intent = Intent(this, TransferWithIPSActivity::class.java)
        intent.putExtra("senderAccountOrAlias", senderAccountOrAlias)
        intent.putExtra("senderIBAN", senderIBAN)
        intent.putExtra("isGovernmentPayment", isGovernmentPayment)
        intent.putExtra("receiverId", receiverId)
        intent.putExtra("receiverAccountOrAlias", receiverAccountOrAlias)
        intent.putExtra("fromAccount", fromAccount)
        intent.putExtra("toAccount",toAccount)
        intent.putExtra("receiverCustomerName", receiverFullName.text.toString())
        intent.putExtra("voenCode", voenCode.text.toString())
        intent.putExtra("senderFullName",senderFullName)
        intent.putExtra("receiverIban",receiverIban)
        startActivityForResult(intent,7)
    }

    private fun showProgressBar(show: Boolean) {
        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        receiverContainer.isClickable = !show
        voenContainer.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}
