package az.turanbank.mlb.presentation.register.asan.juridical

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.addPrefix
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.content_individual_register_asan.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class JuridicalRegisterAsanActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewmodel: JuridicalRegisterAsanViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_juridical_register_asan)

        toolbar_back_button.setOnClickListener { onBackPressed() }

        viewmodel = ViewModelProvider(
            this,
            viewModelFactory
        )[JuridicalRegisterAsanViewModel::class.java]

        toolbar_title.text = getString(R.string.register_with_asan)
        setupInputListeners()
        setupOutputListeners()
    }

    private fun setupOutputListeners() {
        viewmodel.outputs.showProgress().subscribe {
            showProgressBar(it)
        }.addTo(subscriptions)

        viewmodel.outputs.verificationCode()
            .subscribe { getAuthInfoResponseModel ->

                val intent = Intent(
                    this@JuridicalRegisterAsanActivity,
                    JuridicalRegisterAsanPinActivity::class.java
                )

                intent.putExtra("verificationCode", getAuthInfoResponseModel.verificationCode)
                intent.putExtra("certificate", getAuthInfoResponseModel.certificate)
                intent.putExtra("challenge", getAuthInfoResponseModel.challenge)
                intent.putExtra("transactionId", getAuthInfoResponseModel.transactionId)
                intent.putExtra("pin", viewmodel.custPin())
                intent.putExtra("phoneNumber", asanPhoneNumber.text.toString().addPrefix())
                intent.putExtra("asanId", asanId.text.toString())

                startActivity(intent)
                finish()
            }
            .addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setupInputListeners() {
        submitButton.setOnClickListener {
            if (asanPhoneNumber.text.toString().trim().isNotEmpty()) {
                if (asanId.text.toString().trim().isNotEmpty()) {
                    showProgressBar(true)
                    viewmodel.inputs.onSubmitClicked(
                        asanPhoneNumber.text.toString().addPrefix(),
                        asanId.text.toString()
                    )
                } else {
                    AlertDialogMapper(this, 124001).showAlertDialog()
                }
            } else {
                AlertDialogMapper(this, 124002).showAlertDialog()
            }
        }
    }

    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        asanPhoneNumber.isClickable = !show
        asanId.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
