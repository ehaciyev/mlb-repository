package az.turanbank.mlb.presentation.register.asan.juridical

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_juridical_register_asan_pin.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class JuridicalRegisterAsanPinActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewmodel: JuridicalRegisterAsanPinViewModel

    private val transactionId by lazy { intent.getLongExtra("transactionId", 0L) }
    private val challenge: String by lazy { intent.getStringExtra("challenge") }
    private val certificate: String by lazy { intent.getStringExtra("certificate") }
    private val pin: String by lazy { intent.getStringExtra("pin") }
    private val phoneNumber: String by lazy { intent.getStringExtra("phoneNumber") }
    private val userId: String by lazy { intent.getStringExtra("asanId") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_juridical_register_asan_pin)

        toolbar_back_button.setOnClickListener { onBackPressed() }

        verificationCode.setText(intent.getStringExtra("verificationCode"))

        viewmodel = ViewModelProvider(
            this,
            viewModelFactory
        )[JuridicalRegisterAsanPinViewModel::class.java]

        toolbar_title.text = getString(R.string.register_with_asan)
        setupInputListeners()
        setupOutputListeners()
    }

    private fun setupOutputListeners() {
        viewmodel.outputs.loginWithAsanSuccess()
            .subscribe {
                if(it) {
                    val intent = Intent(this@JuridicalRegisterAsanPinActivity, JuridicalRegisterCompaniesListActivity::class.java)
                    intent.putExtra("phoneNumber", phoneNumber)
                    intent.putExtra("pin", pin)
                    intent.putExtra("asanId", userId)

                    startActivity(intent)
                    finish()
                }
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
            finish()
        }.addTo(subscriptions)
    }

    private fun setupInputListeners() {
        showProgressBar(true)
        viewmodel.loginWithAsan(transactionId, certificate, challenge)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if(show)
        {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

}
