package az.turanbank.mlb.presentation.resources.loan.payment

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.loan.LoanPayedAmountModel
import kotlinx.android.synthetic.main.payment_plan_list.view.*

class LoanPayedAmountsAdapter (private val payedAmountPlanList: ArrayList<LoanPayedAmountModel>, private val clickListener: (position: Int) -> Unit) : RecyclerView.Adapter<LoanPayedAmountsAdapter.LoanPaymentListViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): LoanPaymentListViewHolder =
        LoanPaymentListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.payment_plan_list,
                parent,
                false
            )
        )

    override fun getItemCount() = payedAmountPlanList.size

    override fun onBindViewHolder(holder: LoanPaymentListViewHolder, position: Int) {
        holder.bind(payedAmountPlanList[position], clickListener)
    }
    class LoanPaymentListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(loansPayments: LoanPayedAmountModel, clickListener: (position: Int) -> Unit) {
            with(loansPayments) {
                itemView.date.text = this.payDate
                itemView.amount.text = this.payAmount.toString()
                itemView.mainLoan.text = this.payRest.toString()
                itemView.setOnClickListener {
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}