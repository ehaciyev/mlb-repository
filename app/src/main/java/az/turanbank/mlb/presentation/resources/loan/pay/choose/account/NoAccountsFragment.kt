package az.turanbank.mlb.presentation.resources.loan.pay.choose.account

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.resources.account.AccountListAdapter
import com.facebook.shimmer.ShimmerFrameLayout
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class NoAccountsFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: NoAccountViewModel

    private lateinit var recyclerView: RecyclerView
    private lateinit var failMessage: TextView

    private lateinit var accountAdapter: AccountListAdapter

    private lateinit var progress: ImageView

    lateinit var shimmerCardContainer: ShimmerFrameLayout

    private var accountList: ArrayList<AccountListModel> = arrayListOf()

    private val fromChoose: Boolean by lazy { arguments?.getBoolean("fromChoose")!! }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_cards, container, false)
        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView = view.findViewById(R.id.cardsRecyclerView)
        progress = view.findViewById(R.id.progress)
        failMessage = view.findViewById(R.id.failMessage)
        shimmerCardContainer = view.findViewById(R.id.shimmerCardContainer)

        recyclerView.layoutManager = llm

        accountAdapter = AccountListAdapter(
            accountList
        ) {
            val intent = Intent()
            intent.putExtra("dtAccountId", accountList[it].accountId)
            intent.putExtra("dtNumber", accountList[it].iban)
            intent.putExtra("currency", accountList[it].currName)
            intent.putExtra("payType", 2)
            requireActivity().setResult(Activity.RESULT_OK, intent)
            requireActivity().finish()
        }

        recyclerView.adapter = accountAdapter
        setOutputListeners()
        setInputListeners()
        return view
    }

    private fun setOutputListeners() {
        viewModel.outputs.onAccountSuccess().subscribe {
            setRecyclerView(it)
            failMessage.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            progress.clearAnimation()
            progress.visibility = View.GONE
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            failMessage.visibility = View.VISIBLE
            shimmerCardContainer.stopShimmerAnimation()
            shimmerCardContainer.visibility = View.GONE
            recyclerView.visibility = View.GONE
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
        }.addTo(subscriptions)
    }

    private fun setRecyclerView(accounts: ArrayList<AccountListModel>) {
        accountList.clear()
        accountList.addAll(accounts)
        recyclerView.adapter?.notifyDataSetChanged()
        shimmerCardContainer.stopShimmerAnimation()
        shimmerCardContainer.visibility = View.GONE
    }

    private fun setInputListeners() {
        //animateProgressImage()
        accountList.clear()
        recyclerView.adapter?.notifyDataSetChanged()
        viewModel.inputs.getAccounts()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = ViewModelProvider(this, factory)[NoAccountViewModel::class.java]
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }

    override fun onResume() {
        super.onResume()
        shimmerCardContainer.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmerCardContainer.stopShimmerAnimation()
    }
}