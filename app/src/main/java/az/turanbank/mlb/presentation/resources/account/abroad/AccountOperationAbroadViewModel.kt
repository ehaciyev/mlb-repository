package az.turanbank.mlb.presentation.resources.account.abroad

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.domain.user.usecase.resources.account.*
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountOperationOutLandViewModelInputs : BaseViewModelInputs {
    fun getAccountList()
    fun deleteTemp(tempId: Long)
}

interface AccountOperationOutLandViewModelOutputs : BaseViewModelOutputs {
    fun onAccountSuccess(): BehaviorSubject<ArrayList<AccountListModel>>
    fun onTemplateDeleted(): CompletableSubject
    fun showProgress(): PublishSubject<Boolean>
}

class AccountOperationOutLandViewModel @Inject constructor(
    private val getIndividualAccountListUseCase: GetNoCardAccountListForIndividualCustomerUseCase,
    private val getJuridicalAccountListUseCase: GetNoCardAccountListForJuridicalCustomerUseCase,
    private val deleteOperationTemplateUseCase: DeleteOperationTemplateUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    AccountOperationOutLandViewModelInputs,
    AccountOperationOutLandViewModelOutputs {
    override val inputs: AccountOperationOutLandViewModelInputs = this

    override val outputs: AccountOperationOutLandViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val showProgress = PublishSubject.create<Boolean>()

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val accountList = BehaviorSubject.create<ArrayList<AccountListModel>>()
    private val delete = CompletableSubject.create()

    override fun getAccountList() {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getJuridicalAccountListUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                lang = EnumLangType.AZ
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getIndividualAccountListUseCase.execute(custId, token,EnumLangType.AZ)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    fun getDtAccountName(iban: String): String {
        accountList.value?.let {
            for (i in 0 until it.size) {
                if (it[i].iban == iban)
                    return it[i].accountName
            }
        }
        return ""
    }
    fun getDtAccountId(iban: String): Long {
        accountList.value?.let {
            for (i in 0 until it.size) {
                if (it[i].iban == iban)
                    return it[i].accountId
            }
        }
        return 0L
    }
    fun getCrAccountCurrency(iban: String): String {
        accountList.value?.let {
            for (i in 0 until it.size) {
                if (it[i].iban == iban)
                    return it[i].currName
            }
        }
        return ""
    }

    override fun deleteTemp(tempId: Long) {
        showProgress.onNext(true)
        deleteOperationTemplateUseCase
            .execute(custId, token, tempId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1){
                    delete.onComplete()
                }
            }, {
                showProgress.onNext(false)
                it.printStackTrace()
                error.onNext(1872)
            }).addTo(subscriptions)
    }

    override fun showProgress() = showProgress

    override fun onTemplateDeleted() = delete

    override fun onAccountSuccess() = accountList
}