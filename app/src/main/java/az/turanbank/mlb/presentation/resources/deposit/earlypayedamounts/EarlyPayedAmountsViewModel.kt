package az.turanbank.mlb.presentation.resources.deposit.earlypayedamounts

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.deposit.EarlyPaysResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetEarlyPaysByDepositIdIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetEarlyPaysByDepositIdJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface EarlyPayedAmountsViewModelInputs: BaseViewModelInputs{
    fun getEarlyPayedAmounts(depositId: Long)
}
interface EarlyPayedAmountsViewModelOutputs: BaseViewModelOutputs{
    fun onEarlyPayedAmountsSuccess(): PublishSubject<ArrayList<EarlyPaysResponseModel>>
}
class EarlyPayedAmountsViewModel @Inject constructor(
    private val earlyPaysByDepositIdIndividualUseCase: GetEarlyPaysByDepositIdIndividualUseCase,
    private val earlyPaysByDepositIdJuridicalUseCase: GetEarlyPaysByDepositIdJuridicalUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
    EarlyPayedAmountsViewModelInputs,
    EarlyPayedAmountsViewModelOutputs {
    override val inputs: EarlyPayedAmountsViewModelInputs = this

    override val outputs: EarlyPayedAmountsViewModelOutputs = this
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val earlyPayList = PublishSubject.create<ArrayList<EarlyPaysResponseModel>>()
    override fun getEarlyPayedAmounts(depositId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if (isCustomerJuridical) {
            earlyPaysByDepositIdJuridicalUseCase
                .execute(custId, compId, depositId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        earlyPayList.onNext(it.respDepositEarlyPays)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            earlyPaysByDepositIdIndividualUseCase
                .execute(custId, depositId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        earlyPayList.onNext(it.respDepositEarlyPays)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onEarlyPayedAmountsSuccess() = earlyPayList
}