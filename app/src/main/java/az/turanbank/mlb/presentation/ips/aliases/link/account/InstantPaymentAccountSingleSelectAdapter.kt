package az.turanbank.mlb.presentation.ips.aliases.link.account

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.response.GetIPSAccounts
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.presentation.data.SelectIPSMLBAccountModel
import kotlinx.android.synthetic.main.account_multi_select_list_view.view.*
import kotlinx.android.synthetic.main.account_multi_select_list_view.view.container

class InstantPaymentAccountSingleSelectAdapter(private val accountList: ArrayList<GetIPSAccounts>,
                                               private val context: Context,
                                               private val onClickDefaultListener: (position: Int) -> Unit,
                                               private val onClickSelectedAccountListener: (position: Int) -> Unit) :
    RecyclerView.Adapter<InstantPaymentAccountSingleSelectAdapter.AccountViewHolder>() {

   /* private var selected: RadioButton? = null
    private var selectedItem : GetIPSAccounts? = null
    private var selectedPosition: Int? = null*/


    private var selectedRadioIndex = -1
    private var selectedRadioItem: GetIPSAccounts? = null
    private var selectedRadioView: RadioButton? = null
    private var selectedItemsFromServer = arrayListOf<GetIPSAccounts>()
    private var selectedItems = arrayListOf<GetIPSAccounts>()


    class AccountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AccountViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.account_multi_select_list_view,
            parent,
            false
        )
    )

    override fun getItemCount() = accountList.size

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        var account = accountList[position]

        /*if(account.isLinked.isNullOrEmpty()){
            if(account.checked){
                holder.itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.checked_account))
                holder.itemView.checkBox.isChecked = true
            } else {
                holder.itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.unchecked_account))
                holder.itemView.checkBox.isChecked = false
            }
        } else {
            //holder.itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.unchecked_account))
            holder.itemView.checkBox.isChecked = true
            holder.itemView.isClickable = false
        }

        if(account.isDefault == 1){
            holder.itemView.defaultAccount.isChecked = true
            selectedPosition = position
            if (selected != null) {
                selectedItem!!.isDefault = 0
                selected!!.isChecked = false
            }
            holder.itemView.defaultAccount.isChecked = true
            account.isDefault = 1
            selectedItem = account
            selected = holder.itemView.defaultAccount
        }

        holder.itemView.iban.text = account.iban
        holder.itemView.currency.text = account.currency
        holder.itemView.setOnClickListener { onClickListener.invoke(position) }
        holder.itemView.checkBox.setOnClickListener { onClickListener.invoke(position) }

        holder.itemView.defaultAccount.setOnClickListener {
            selectedPosition = position
            if (selected != null) {
                selectedItem!!.isDefault = 0
                selected!!.isChecked = false
            } else {
                holder.itemView.defaultAccount.isChecked = true
                account.isDefault = 1
                selectedItem = account
                selected = holder.itemView.defaultAccount
            }

        }*/




        holder.itemView.iban.text = account.iban
        holder.itemView.currency.text = account.currency

        if(account.isDefault == 1){
            holder.itemView.defaultAccount.isChecked = true
            selectedRadioView = holder.itemView.defaultAccount
            selectedRadioIndex = position
            selectedRadioItem = account
        }

        if(!account.isLinked.isNullOrEmpty()){
            holder.itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.unselectedButtonColor))
            holder.itemView.selectedAccount.isChecked = true
            holder.itemView.selectedAccount.isClickable = false
            account.checked = true
            selectedItemsFromServer.add(account)
            //selectedItems.add(account)
        } else {
            holder.itemView.selectedAccount.isClickable = true
        }

        if(holder.itemView.selectedAccount.isClickable){
            holder.itemView.selectedAccount.setOnCheckedChangeListener { _, isChecked ->
                if(isChecked){
                    account.checked = true
                    selectedItems.add(account)
                } else {
                    account.checked = true
                    selectedItems.remove(account)
                }
                //onclickListenerCheckBox.invoke(position)
            }
        }


        holder.itemView.defaultAccount.setOnClickListener {
            if (selectedRadioView != null) {
                selectedRadioView!!.isChecked = false
                selectedRadioItem!!.isDefault = 0
            }
            selectedRadioIndex = position
            holder.itemView.defaultAccount.isChecked = true
            account.isDefault = 1
            selectedRadioItem = account
            selectedRadioView = holder.itemView.defaultAccount
            onClickDefaultListener.invoke(position)

        }
    }


    fun getSelectedDefaultAccount() = selectedRadioItem

    fun getSelectedItems() = selectedItems

    fun getSelecteditemsFromServer() = selectedItemsFromServer

    /*fun getSelectedItem(): GetIPSAccounts?{
        return selectedItem
    }

    fun getDefaultItem(): GetIPSAccounts?{
        return selectedItem
    }

    fun getLinkedItem(): GetIPSAccounts?{
        return selectedItem
    }

    fun getSelectedPosition() = selectedPosition*/
}