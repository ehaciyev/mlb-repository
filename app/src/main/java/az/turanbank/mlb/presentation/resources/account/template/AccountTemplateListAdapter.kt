package az.turanbank.mlb.presentation.resources.account.template

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.account.AccountTemplateResponseModel
import kotlinx.android.synthetic.main.card_template_list_view.view.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class AccountTemplateListAdapter(
    private val templateList: ArrayList<AccountTemplateResponseModel>,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<AccountTemplateListAdapter.AccountTemplateViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AccountTemplateViewHolder = AccountTemplateViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.card_template_list_view,
            parent,
            false
        )
    )

    override fun getItemCount() = templateList.size

    override fun onBindViewHolder(holder: AccountTemplateViewHolder, position: Int) {
        holder.bind(templateList[position], clickListener)
    }

    class AccountTemplateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(template: AccountTemplateResponseModel, clickListener: (position: Int) -> Unit) {
            val decimalFormat = DecimalFormat("0.00")
            decimalFormat.roundingMode = RoundingMode.HALF_EVEN
            val symbols = DecimalFormatSymbols()
            symbols.decimalSeparator = '.'
            decimalFormat.decimalFormatSymbols = symbols

            val currency: String = if(template.operNameId == 4){
                template.crCcy
            } else {
                template.amtCcy
            }
            val amount: String = if(template.operNameId == 4){
                decimalFormat.format(template.crAmt)
            } else {
                decimalFormat.format(template.amt)
            }

            itemView.name.text = template.tempName
            itemView.amountInteger.text = amount.split(".")[0]+","
            itemView.amountReminder.text = amount.split(".")[1]+" "+currency

            itemView.operationType.text = template.operTypeName
            itemView.cardNumber.text = template.crIban
            itemView.setOnClickListener { clickListener.invoke(adapterPosition) }
        }
    }
}