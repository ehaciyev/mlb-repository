package az.turanbank.mlb.presentation.ips.paidToMe

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.ReceivePaymentsModel
import az.turanbank.mlb.domain.user.usecase.ips.paidToMe.PaidToMeUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface PaidToMeInputViewModel: BaseViewModelInputs{
    fun getPayments()
}

interface PaidToMeOutputViewModel: BaseViewModelOutputs{
    fun onPaymentsArrived(): PublishSubject<ArrayList<ReceivePaymentsModel>>
    fun showProgress(): PublishSubject<Boolean>
}
class PaidToMeViewModel @Inject constructor(private val paidToMeUseCase: PaidToMeUseCase,
                                            sharedPrefs: SharedPreferences):
    BaseViewModel(), PaidToMeInputViewModel, PaidToMeOutputViewModel {

    override val inputs: PaidToMeInputViewModel = this

    override val outputs: PaidToMeOutputViewModel = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)

    private val compId = sharedPrefs.getLong("compId", 0L)

    private val payments = PublishSubject.create<ArrayList<ReceivePaymentsModel>>()

    private val showProgress = PublishSubject.create<Boolean>()

    override fun getPayments() {
        val compId = if (isCustomerJuridical){
            compId
        } else {
            null
        }
        showProgress.onNext(true)
        paidToMeUseCase
            .execute(custId, compId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1){
                    payments.onNext(it.receivePayments)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onPaymentsArrived() = payments
    override fun showProgress() = showProgress


}