package az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor

import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_ipstransfer.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class NonRegisteredIPSTransferActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nonregistered_ipstransfer)

        toolbar_title.text = getString(R.string.pay)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        view_pager.adapter =
            NonRegisteredIPSTransferPagerAdapter(
                this,
                supportFragmentManager
            )
        tabs.setupWithViewPager(view_pager)
    }
}