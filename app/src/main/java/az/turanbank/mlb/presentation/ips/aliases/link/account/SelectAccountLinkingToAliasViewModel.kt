package az.turanbank.mlb.presentation.ips.aliases.link.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.request.AliasAndAccountRequestModel
import az.turanbank.mlb.data.remote.model.ips.response.GetIPSAccounts
import az.turanbank.mlb.domain.user.usecase.ips.aliases.GetAccountsByAliasIdUseCase
import az.turanbank.mlb.domain.user.usecase.ips.aliases.LinkAliasToAccountUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAccountsUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetNotLinkedAccountListByAliasId
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.data.SelectIPSMLBAccountModel
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface SelectAccountLinkingToAliasViewModelInputs : BaseViewModelInputs {
    fun getAccountsByAliasId(aliasId: Long)
    fun linkAccountToAlias(aliasIdAndAccountIdList: ArrayList<AliasAndAccountRequestModel>)
}

interface SelectAccountLinkingToAliasViewModelOutputs : BaseViewModelOutputs {
    fun onAccountListSuccess(): PublishSubject<ArrayList<GetIPSAccounts>>
    fun linkCompleted(): CompletableSubject
}

class SelectAccountLinkingToAliasViewModel @Inject constructor(
    private val getAccountsByAliasIdUseCase: GetAccountsByAliasIdUseCase,
    private val linkAliasToAccountUseCase: LinkAliasToAccountUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    SelectAccountLinkingToAliasViewModelInputs,
    SelectAccountLinkingToAliasViewModelOutputs {
    override val inputs: SelectAccountLinkingToAliasViewModelInputs = this

    override val outputs: SelectAccountLinkingToAliasViewModelOutputs = this


    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val lang = sharedPrefs.getString("lang", "az")
    var enumLangType = EnumLangType.AZ

    private val accountList = PublishSubject.create<ArrayList<GetIPSAccounts>>()

    private val linking = CompletableSubject.create()

    private val accounts = arrayListOf<GetIPSAccounts>()
    override fun getAccountsByAliasId(aliasId: Long) {

        when (lang) {
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getAccountsByAliasIdUseCase
            .execute(custId, compId, aliasId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ accountsResponse ->
                if (accountsResponse.status.statusCode == 1) {
                    accountsResponse.accountList.forEach {
                        accounts.add(it) }
                    accountList.onNext(accounts)
                } else {
                    error.onNext(accountsResponse.status.statusCode)
                }
            }
                , {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
    }

    override fun linkAccountToAlias(aliasIdAndAccountIdList: ArrayList<AliasAndAccountRequestModel>) {
        linkAliasToAccountUseCase.execute(custId, token, aliasIdAndAccountIdList)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    linking.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {

            }).addTo(subscriptions)
    }

    override fun onAccountListSuccess() = accountList
    override fun linkCompleted() = linking
}