package az.turanbank.mlb.presentation.resources.account

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.resources.account.AccountTemplateResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_operation_internal.*
import javax.inject.Inject

class AccountOperationInternalActivity : BaseActivity() {

    lateinit var viewModel: AccountOperationInternalViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var dialog: MlbProgressDialog

    private var accountList = arrayListOf<AccountListModel>()
    private lateinit var textWatcher: TextWatcher

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_account_operation_internal)
        crIban.filters = arrayOf(InputFilter.AllCaps())
        crIban.filters = arrayOf(InputFilter.LengthFilter(28))


        toolbar_title.text = getString(R.string.internal_transfer)

        toolbar_back_button.setOnClickListener { onBackPressed() }

        dialog = MlbProgressDialog(this)
        initTextWatcher()

        viewModel =
            ViewModelProvider(this, factory)[AccountOperationInternalViewModel::class.java]

        toolbar_back_button.setOnClickListener {
            finish()
        }

        buttonEnabled(false)

        crIban.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 28) {
                    showProgressImages(true)
                    viewModel.inputs.getCustInfoByIban(s.toString())
                    observeCustomerInfo()
                } else {
                    crAccTaxContainer.visibility = View.GONE
                    opCrAccName.setText("")
                    opCrAccTaxNo.setText("")
                    hideProgressImages(true)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        setOutputListeners()
        setInputListeners()

        /*submitButton.setOnClickListener {
            val intent = Intent(this, AccountOperationInternalVerifyActivity::class.java)
            intent.putExtra("dtAccountId", viewModel.getDtAccountId(dtAccountId.text.toString()))
            intent.putExtra("crIban", crIban.text.toString())
            intent.putExtra("amount", opAmount.text.toString())
            intent.putExtra("purpose", opPurpose.text.toString())
            intent.putExtra("accountName", opCrAccName.text.toString())
            if (!opCrAccTaxNo.text.isNullOrEmpty())
                intent.putExtra("taxNo", opCrAccTaxNo.text.toString())

            startActivityForResult(intent, 7)
        }*/
    }

    private fun initTextWatcher() {
        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!crIban.text.isNullOrEmpty() &&
                    !opAmount.text.isNullOrEmpty() &&
                    !dtAccountId.text.isNullOrEmpty() &&
                    (opAmount.text.toString().toDouble() > 0.00) &&
                    !opPurpose.text.isNullOrEmpty() &&
                    !opCrAccName.text.isNullOrEmpty()
                ) {
                    buttonEnabled(true)

                    submitButton.setOnClickListener {
                        val intent = Intent(
                            this@AccountOperationInternalActivity,
                            AccountOperationInternalVerifyActivity::class.java
                        )
                        intent.putExtra(
                            "dtAccountId",
                            viewModel.getDtAccountId(dtAccountId.text.toString().split(" ")[0])
                        )
                        intent.putExtra("dtIban", dtAccountId.text.toString().split(" ")[0])
                        intent.putExtra("crIban", crIban.text.toString())
                        intent.putExtra("amount", opAmount.text.toString())
                        intent.putExtra(
                            "currency",
                            viewModel.getAccountCurrency(dtAccountId.text.toString().split(" ")[0])
                        )
                        intent.putExtra("purpose", opPurpose.text.toString())
                        intent.putExtra("accountName", opCrAccName.text.toString())
                        if (!opCrAccTaxNo.text.isNullOrEmpty())
                            intent.putExtra("taxNo", opCrAccTaxNo.text.toString())

                        startActivityForResult(intent, 7)
                    }
                } else {
                    buttonEnabled(false)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        crIban.addTextChangedListener(textWatcher)
        opAmount.addTextChangedListener(textWatcher)
        dtAccountId.addTextChangedListener(textWatcher)
        opPurpose.addTextChangedListener(textWatcher)
        opCrAccName.addTextChangedListener(textWatcher)
    }

    private fun checkFromTemplateOrNot() {
        if (intent.getSerializableExtra("template") != null) {
            deleteTemp.visibility = View.VISIBLE
            fillFormAlongWithTemplate(intent.getSerializableExtra("template") as AccountTemplateResponseModel)
        } else {
            deleteTemp.visibility = View.GONE
        }
    }

    private fun showDeleteTemp(tempId: Long) {

        val alert = androidx.appcompat.app.AlertDialog.Builder(this)
        alert.setMessage(getString(R.string.template_delete_confirmation_message))

        alert.setCancelable(true)
        alert.setPositiveButton(
            getString(R.string.yes)
        ) { _, _ ->
            viewModel.inputs.deleteTemp(tempId)
        }

        alert.setNegativeButton(
            getString(R.string.no)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }

    @SuppressLint("SetTextI18n")
    private fun fillFormAlongWithTemplate(template: AccountTemplateResponseModel) {
        deleteTemp.setOnClickListener {
            showDeleteTemp(template.id)
        }
        crIban.setText(template.crIban)
        opAmount.setText(template.amt.toString())
        opPurpose.setText(template.operPurpose)
        for (i in 0 until accountList.size) {
            if (accountList[i].iban == template.dtIban) {
                dtAccountId.setText(template.dtIban + " / " + template.dtCcy)
                break
            }
        }
    }

    private fun hideProgressImages(hide: Boolean) {
        if (hide) {
            opCrAccountNameProgres.visibility = View.GONE
            opTaxProgress.visibility = View.GONE
        } else {
            opCrAccountNameProgres.visibility = View.VISIBLE
            opTaxProgress.visibility = View.VISIBLE
        }
    }

    private fun showProgressImages(show: Boolean) {
        hideProgressImages(show)

        if (show) {
            opCrAccountNameProgres.visibility = View.VISIBLE
            opTaxProgress.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            opCrAccountNameProgres.visibility = View.GONE
            opTaxProgress.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        opCrAccountNameProgres.clearAnimation()
        opTaxProgress.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        opCrAccountNameProgres.startAnimation(rotate)
        opTaxProgress.startAnimation(rotate)
    }

    private fun observeCustomerInfo() {
        viewModel.outputs.custInfo().subscribe {
            if (it.status.statusCode == 1) {

                it.taxNumber?.let { taxNumber ->
                    crAccTaxContainer.visibility = View.VISIBLE
                    opCrAccTaxNo.setText(taxNumber)
                }
                showProgressImages(false)
                opCrAccName.setText(it.custName)
            } else {
                showProgressImages(false)
            }
        }.addTo(subscriptions)
    }

    private fun buttonEnabled(show: Boolean) {

        if (!show) {
            submitButton.isClickable = false
            submitButton.isEnabled = false
            submitButton.isFocusable = false
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
        } else {
            submitButton.isClickable = true
            submitButton.isEnabled = true
            submitButton.isFocusable = true
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent(this, AccountOperationInternalResultActivity::class.java)

                intent.putExtra(
                    "dtAccountId",
                    viewModel.getDtAccountId(dtAccountId.text.toString().split(" ")[0])
                )
                intent.putExtra("crIban", crIban.text.toString())
                intent.putExtra("dtIban", dtAccountId.text.toString())
                intent.putExtra("amount", opAmount.text.toString())
                intent.putExtra("accountName", opCrAccName.text.toString())
                intent.putExtra("operationNo", data?.getStringExtra("operationNo"))
                intent.putExtra("accountResponse", data?.getSerializableExtra("accountResponse"))
                intent.putExtra("operationStatus", data?.getStringExtra("operationStatus"))
                if (!intent.getStringExtra("taxNo").isNullOrEmpty()) {
                    intent.putExtra("taxNo", opCrAccTaxNo.text.toString())
                }
                startActivity(intent)
                finish()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setOutputListeners() {

        viewModel.outputs.onTemplateDeleted().subscribe {
            Toast.makeText(this, getString(R.string.template_successfully_deleted), Toast.LENGTH_LONG).show()
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onAccountSuccess().subscribe { accountListModel ->
            accountList = accountListModel
            checkFromTemplateOrNot()

            val accountNameArray = arrayOfNulls<String>(accountListModel.size)

            val accountIbanFromIntent = intent.getStringExtra("accountIban")
            val accountCurrNameFromIntent = intent.getStringExtra("currName")
            /*if (intent.getSerializableExtra("template") == null) {
                dtAccountId.setText(accountListModel[0].iban + " / " + accountListModel[0].currName)
            }*/

            for (i in 0 until accountListModel.size) {
                if (accountListModel[i].iban == accountIbanFromIntent) {
                    dtAccountId.setText("$accountIbanFromIntent / $accountCurrNameFromIntent")
                    break
                }
            }

            for (i in 0 until accountListModel.size) {
                accountNameArray[i] =
                    (accountListModel[i].iban + " / " + accountListModel[i].currName)
            }
            dtAccountIdContainer.setOnClickListener {
                val builder: AlertDialog.Builder =
                    AlertDialog.Builder(this@AccountOperationInternalActivity)
                builder.setTitle(getString(R.string.select_account))

                /* for (i in 0 until accountListModel.size) {
                     accountNameArray[i] = (accountListModel[i].iban + " / " + accountListModel[i].currName)
                 }*/
                builder.setItems(accountNameArray) { _, which ->
                    dtAccountId.setText(accountNameArray[which])
                }
                val dialog = builder.create()
                dialog.show()
            }

            viewModel.outputs.onError().subscribe {
                AlertDialogMapper(this, it).showAlertDialog()
            }.addTo(subscriptions)
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getAccountList()
    }
}
