package az.turanbank.mlb.presentation.ips.aliases.myaliases

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.response.GetMLBAliasesResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.RespIpsModelAccount
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_my_aliases.*
import kotlinx.android.synthetic.main.activity_my_aliases.container
import kotlinx.android.synthetic.main.activity_my_aliases.noDataAvailable
import kotlinx.android.synthetic.main.activity_my_income_payments.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class MyAliasesActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: MyAliasesViewModel

    lateinit var adapter: MyAliasesAdapter

    private var aliases: ArrayList<MLBAliasResponseModel> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_aliases)

        toolbar_back_button.setOnClickListener { onBackPressed() }

        viewModel = ViewModelProvider(this, factory)[MyAliasesViewModel::class.java]

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        container.layoutManager = llm

        adapter = MyAliasesAdapter(applicationContext,aliases){

        }

        container.adapter = adapter

        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        viewModel.inputs.getAliasses()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onGetAliasesSuccess().subscribe{
            aliases.clear()
            aliases.addAll(it)
            adapter.notifyDataSetChanged()
            noDataAvailable.visibility = View.GONE
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe{
            noDataAvailable.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }
}