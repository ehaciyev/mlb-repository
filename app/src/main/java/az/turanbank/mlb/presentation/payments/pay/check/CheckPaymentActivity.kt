package az.turanbank.mlb.presentation.payments.pay.check

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.widget.ScrollView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.AvansResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.ChildInvoiceResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.InvoiceResponseModel
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.payments.pay.pay.VerifyPaymentActivity
import az.turanbank.mlb.presentation.payments.pay.result.PaymentResultActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog

import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_check_payment.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class CheckPaymentActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CheckPaymentViewModel
    lateinit var mDialog: MlbProgressDialog

    lateinit var appCompatTextView: AppCompatTextView
    lateinit var scrollContainer: ScrollView

    private val avans: AvansResponseModel by lazy { intent.getSerializableExtra("avans") as AvansResponseModel }
    private val invoice: InvoiceResponseModel by lazy { intent.getSerializableExtra("invoice") as InvoiceResponseModel }

    private val identificationCode: ArrayList<IdentificationCodeRequestModel> by lazy { intent.getSerializableExtra("identificationCode") as ArrayList<IdentificationCodeRequestModel> }
    private val identificationType: String by lazy { intent.getStringExtra("identificationType") }
    private val merchantId: Long by lazy { intent.getLongExtra("merchantId", 0L) }
    private val merchantName: String by lazy { intent.getStringExtra("merchantName") }
    private val merchantDisplayName: String by lazy { intent.getStringExtra("merchantDisplayName") }
    private val categoryName: String by lazy { intent.getStringExtra("categoryName") }
    private val categoryId: Long by lazy { intent.getLongExtra("categoryId", 0L) }
    private val providerId: Long by lazy { intent.getLongExtra("providerId", 0L) }
    private val transactionId: String by lazy { intent.getStringExtra("transactionId") }
    private val transactionNumberFromIntent: String by lazy { intent.getStringExtra("transactionNumber") }
    private val serviceCodeUser: String by lazy { intent.getStringExtra("serviceCodeUser") }
    private val isRespChildInvoice: Boolean by lazy { intent.getBooleanExtra("isRespChildInvoice", false) }
    lateinit var respChildInvoice: ArrayList<ChildInvoiceResponseModel>
    private val amountTemplate: String by lazy { intent.getStringExtra("amountTemplate") }
    private val isInvoice: Boolean by lazy { intent.getBooleanExtra("isInvoice", false) }


    private var isPenalty: Boolean = false

    private var currencyText = ""
    private var plasticCardNumber = ""
    private var cardId = 0L
    //private var service = ""
    //private var codePayer: Long? = null
    private var code = ""
    private var transactionNumber = ""
    private var serviceName_ = ""

    private var paymentReceiver: Long? = null
    private var paymentReceiverDescription: String? = null

    var minValue = 0.01
    var maxValue = 100000.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_payment)

        viewModel =
            ViewModelProvider(this, factory)[CheckPaymentViewModel::class.java]

        toolbar_back_button.setOnClickListener { onBackPressed() }

        mDialog = MlbProgressDialog(this)

        respChildInvoice = if(isRespChildInvoice){
            intent.getSerializableExtra("respChildInvoice") as ArrayList<ChildInvoiceResponseModel>
        } else {
            arrayListOf()
        }

        if (!isInvoice) {
            // it is for avans
            if(!amountTemplate.isNullOrEmpty()){
                amount.setText(amountTemplate)
            } else {
                if(avans.amount < 0.0){
                    amount.setText("0.0")
                } else {
                    amount.setText(avans.amount.toString())
                }

                if(avans.partialPayment == 1){
                    amount.isFocusable = true
                    if(!(avans.minAllowed == 0.0 || avans.minAllowed == null) && (avans.maxAllowed == 0.0|| avans.maxAllowed == null)){
                        minValue = avans.minAllowed
                        maxValue = avans.maxAllowed
                    }
                } else {
                    amount.isFocusable = false
                }
            }

            toolbar_title.text = avans.serviceName
            transactionNumber = avans.transactionNumber

            serviceName_ = avans.serviceName
            code = avans.serviceCode.toString()

            val names = arrayOfNulls<String>(avans.paymentReceiverCodeName.size)
            val codes =  arrayOfNulls<Long>(avans.paymentReceiverCodeName.size)

            if(!avans.paymentReceiverCodeName.isNullOrEmpty()){
                for (i in avans.paymentReceiverCodeName.indices) {
                    names[i] = avans.paymentReceiverCodeName[i].name
                    codes[i] = avans.paymentReceiverCodeName[i].code
                }
            }

            if(avans.fullName.isNullOrEmpty()){
                disableContainerNumber(1)
            } else {
                fullNameText.text = getString(
                    R.string.fullname_formula,
                    avans.fullName
                )
            }

            if(avans.serviceName.isNullOrEmpty()){
                disableContainerNumber(2)
            } else {
                purpose.text = avans.serviceName
            }

            if(code.isNullOrEmpty()){
                disableContainerNumber(4)
            } else {
                payerCode.text = code
            }
            invoiceCode.text = serviceCodeUser
            // disable document date
            disableContainerNumber(5)

            if (names.size > 1) {
                paymentReceiverDescription = names[0]!!
                paymentReceiver = codes[0]!!
                wanker.text = paymentReceiverDescription
                wankerDownArrow.visibility = View.VISIBLE
                paymentReceiver = avans.paymentReceiverCodeName[0].code
                wanker.setOnClickListener {
                    val builder: android.app.AlertDialog.Builder =
                        android.app.AlertDialog.Builder(this)
                    builder.setTitle(getString(R.string.choose))

                    builder.setItems(names) { _, which ->
                        paymentReceiverDescription = names[which]!!
                        paymentReceiver = codes[which]!!
                        wanker.text = paymentReceiverDescription
                    }
                    val dialog = builder.create()
                    dialog.show()
                }
            } else if(names.size ==1) {
                paymentReceiverDescription = names[0]!!
                paymentReceiver = codes[0]!!
                wankerSingle.visibility = View.VISIBLE
                wankerSingle.text = paymentReceiverDescription
                wankerContainer.visibility = View.GONE
            } else {
                paymentReceiver = avans.paymentReceiver
                paymentReceiverDescription = avans.paymentReceiverDescription
                wankerSingle.visibility = View.VISIBLE
                wankerSingle.text = paymentReceiverDescription
                wankerContainer.visibility = View.GONE
            }

        } else {
            if(!amountTemplate.isNullOrEmpty()){
                amount.setText(amountTemplate)
            } else {
                amount.setText(invoice.amount.toString())
                if(invoice.partialPayment ==1){
                    amount.isFocusable = true
                    if(!(invoice.minAllowed == 0.0 || invoice.minAllowed == null || invoice.maxAllowed == 0.0|| invoice.maxAllowed == null)){
                        minValue = invoice.minAllowed
                        maxValue = invoice.maxAllowed
                    }
                } else {
                    amount.isFocusable = false
                }
            }

            if(invoice.paymentReceiverCodeName.isNullOrEmpty()){
                paymentReceiver = invoice.paymentReceiver
                paymentReceiverDescription = invoice.paymentReceiverDescription

                wankerSingle.visibility = View.VISIBLE
                wankerSingle.text = paymentReceiverDescription
                wankerContainer.visibility = View.GONE

            } else {
                val names = arrayOfNulls<String>(invoice.paymentReceiverCodeName.size)
                val codes = arrayOfNulls<Long>(invoice.paymentReceiverCodeName.size)

                for (i in invoice.paymentReceiverCodeName.indices) {
                    names[i] = invoice.paymentReceiverCodeName[i].name
                    codes[i] = invoice.paymentReceiverCodeName[i].code
                }
                paymentReceiverDescription = names[0]!!
                paymentReceiver = codes[0]!!

                if(names.size > 1){
                    wanker.text = paymentReceiverDescription
                    wanker.setOnClickListener {
                        val builder: android.app.AlertDialog.Builder =
                            android.app.AlertDialog.Builder(this)
                        builder.setTitle(getString(R.string.choose))

                        builder.setItems(names) { _, which ->
                            paymentReceiverDescription = names[which]!!
                            paymentReceiver = codes[which]!!
                            //paymentReceiver = invoice.paymentReceiverCodeName[which].code
                            wanker.text = paymentReceiverDescription
                        }
                        val dialog = builder.create()
                        dialog.show()
                    }
                } else if(names.size == 1){
                    paymentReceiverDescription = names[0]!!
                    paymentReceiver = codes[0]!!
                    wankerSingle.visibility = View.VISIBLE
                    wankerSingle.text = paymentReceiverDescription
                    wankerContainer.visibility = View.GONE
                } else {
                    paymentReceiver = invoice.paymentReceiver
                    paymentReceiverDescription = invoice.paymentReceiverDescription

                    wankerSingle.visibility = View.VISIBLE
                    wankerSingle.text = paymentReceiverDescription
                    wankerContainer.visibility = View.GONE

                }
            }

            toolbar_title.text = invoice.serviceName
            //paymentReceiver = null

            transactionNumber = if(isPenalty){
                transactionNumberFromIntent
            } else {
                invoice.transactionNumber
            }

            //service = invoice.paymentReceiver
            //code = invoice.payerCode

            if(invoice.fullName.isNullOrEmpty()){
                disableContainerNumber(1)
            } else {
                fullNameText.text = invoice.fullName
            }

            if(invoice.serviceName.isNullOrEmpty()){
                disableContainerNumber(3)
            } else {
                purpose.text = invoice.serviceName
            }

            if(invoice.invoiceCode.isNullOrEmpty()){
                payerCode.text = serviceCodeUser
            } else {
                payerCode.text = invoice.invoiceCode
            }

            if(invoice.payerCode.isNullOrEmpty()){
                disableContainerNumber(3)
            } else {
                invoiceCode.text = invoice.payerCode
            }

            if(invoice.invoiceDate.isNullOrEmpty()){
                disableContainerNumber(5)
            } else {
                documentDate.text = invoice.invoiceDate
            }

            serviceName_ = invoice.serviceName

            if (invoice.amount != 0.0)
            amount.setText(invoice.amount.toString())
        }

        setOutputListeners()
        setInputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.showProgress().subscribe{
            if(it){
                mDialog.showDialog()
            } else {
                mDialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.cardListSuccess().subscribe {
            val filteredList = arrayListOf<CardListModel>()
            it.forEach { card ->
                if (card.cardStatus != "0") {
                    filteredList.add(card)
                }
            }
            attachToTheView(filteredList)
        }.addTo(subscriptions)
        viewModel.outputs.onCheckCompleted().subscribe {
            transactionNumber = it.transactionNumber

            payerCode.text = invoice.invoiceNumber
            invoiceCode.text = invoice.invoiceCode
        }.addTo(subscriptions)

        viewModel.outputs.cardListNull().subscribe{
            if(it){
                mDialog.hideDialog()
                AlertDialogMapper(this, 124024).showAlertDialogWithCloseOption()
            }
        }.addTo(subscriptions)

    }

    private fun attachToTheView(filteredList: ArrayList<CardListModel>) {
        val items = arrayOfNulls<String>(filteredList.size)

        for (i in filteredList.indices) {
            /*items[i] = filteredList[i].cardNumber*/
            items[i] = filteredList[i].cardNumber.replaceRange(4, 12, " **** **** ") + " / " + filteredList[i].balance + filteredList[i].currency
        }

        currency.text = filteredList[0].currency

        cardContainer.setOnClickListener {
            val builder: android.app.AlertDialog.Builder =
                android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.choose))

            builder.setItems(items) { _, which ->
                currencyText = filteredList[which].currency
                plasticCardNumber = filteredList[which].cardNumber.replaceRange(4, 12, " **** **** ")
                cardNumber.text = plasticCardNumber
                currency.text = currencyText
                cardId = filteredList[which].cardId
            }
            val dialog = builder.create()
            dialog.show()
        }

        appCompatTextView.visibility = View.VISIBLE
        scrollContainer.visibility = View.VISIBLE
    }

    private fun setInputListeners() {
        viewModel.inputs.getCards()

        if (providerId == 1L && categoryName == "penalty") {
            isPenalty = true
            val identificationCode = arrayListOf<IdentificationCodeRequestModel>()
            identificationCode.add(IdentificationCodeRequestModel("invoiceNumber", invoice.invoiceNumber))
            identificationCode.add(IdentificationCodeRequestModel("invoiceCode", invoice.invoiceCode))
            viewModel.inputs.check(
                merchantId,
                "payerCode",
                identificationCode,
                categoryId,
                providerId
            )
        }

        submitButton.setOnClickListener {
            if (amount.text.toString().trim().isNotEmpty()) {
                if (cardId != 0L) {
                    if(amount.text.toString().trim().toDouble() < minValue){
                        amount.error = getString(R.string.enter_min_allowance)
                    } else if(amount.text.toString().trim().toDouble() > maxValue){
                        amount.error = getString(R.string.enter_max_allowance)
                    } else {
                        amount.error = null
                        val intent = Intent(this, VerifyPaymentActivity::class.java)
                        intent.putExtra("currency", currency.text.toString())
                        intent.putExtra("providerId", providerId)
                        intent.putExtra("respChildInvoice",respChildInvoice)
                        intent.putExtra("plasticCard", plasticCardNumber)
                        intent.putExtra("identificationType", identificationType)
                        intent.putExtra("identificationCode", identificationCode)
                        intent.putExtra("amount", amount.text.toString().toDouble())
                        intent.putExtra("transactionId",transactionId)
                        intent.putExtra("transactionNumber",transactionNumberFromIntent)
                       /* intent.putExtra("service", service)
                        intent.putExtra("codePayer",codePayer)*/
                        intent.putExtra("merchantId", merchantId)
                        intent.putExtra("code", code)
                        intent.putExtra("cardId", cardId)
                        intent.putExtra("paymentReceiver", paymentReceiver)
                        intent.putExtra("transactionNumber", transactionNumber)
                        intent.putExtra("merchantName", merchantName)
                        intent.putExtra("merchantDisplayName",merchantDisplayName)
                        intent.putExtra("isInvoice", isInvoice)
                        if(isInvoice){
                            intent.putExtra("invoice", invoice)
                        } else {
                            intent.putExtra("avans", avans)
                        }

                        intent.putExtra("paymentReceiver", paymentReceiver)
                        intent.putExtra("paymentReceiverDescription",paymentReceiverDescription)

                        intent.putExtra("serviceCodeUser",serviceCodeUser)
                        intent.putExtra("payerCode", payerCode.text)
                        intent.putExtra("invoiceCode", invoiceCode.text.toString())
                        intent.putExtra("categoryId",categoryId)
                        intent.putExtra("serviceName", serviceName_)
                        intent.putExtra("subscriberName", fullNameText.text.toString())
                        startActivityForResult(intent, 7)
                    }

                } else{
                    AlertDialogMapper(this, 124006).showAlertDialog()
                }
            } else {
                AlertDialogMapper(this, 124003).showAlertDialog()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK) {
                var intent = Intent(this, PaymentResultActivity::class.java)
                intent.putExtra("payment", data?.getSerializableExtra("payment"))
                intent.putExtra("currency", data?.getSerializableExtra("currency"))
                intent.putExtra("identificationType", identificationType)
                intent.putExtra("identificationCode", identificationCode)
                intent.putExtra("amount", amount.text.toString().toDouble())
                intent.putExtra("categoryId", categoryId)
                intent.putExtra("providerId", providerId)
                intent.putExtra("merchantId", merchantId)
                intent.putExtra("isInvoice", isInvoice)
                if (isInvoice) {
                    intent.putExtra("invoice", invoice)
                } else {
                    intent.putExtra("avans", avans)
                }
                startActivity(intent)
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }


    fun disableContainerNumber(value: Int) {
        when (value) {
            1 -> {
                //name surname container
                nameSurnameContainer.visibility = View.GONE
            }
            2 -> {
                //invoice container
                invoiceCodeContainer.visibility = View.GONE
            }

            3 -> {
                //pament purpose container
                paymentPurposeContainer.visibility = View.GONE
            }
            4 -> {

                //payer code container
                payerCodeContainer.visibility = View.GONE
            }
            5 -> {
                // document time container
                documentDateContainer.visibility = View.GONE
            }
            else -> {
                //do something
            }
        }
    }



}