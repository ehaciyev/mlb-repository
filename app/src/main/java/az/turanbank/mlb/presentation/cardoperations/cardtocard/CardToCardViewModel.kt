package az.turanbank.mlb.presentation.cardoperations.cardtocard

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.domain.user.usecase.cardoperations.DeleteCardOperationTempUseCase
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashlessUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetCardListForJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetIndividualCardListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import java.util.ArrayList
import javax.inject.Inject

interface CarcToCardViewModelInputs : BaseViewModelInputs {
    fun getCardList()
    fun deleteTemp(tempId: Long)
}

interface CardToCardViewModelOutputs : BaseViewModelOutputs {
    fun cardListSuccess(): PublishSubject<ArrayList<CardListModel>>
    fun currencyListSuccess(): PublishSubject<ArrayList<String>>
    fun currencyAndCardSuccess(): PublishSubject<Boolean>
    fun templateDeleted(): CompletableSubject
}

class CardToCardViewModel @Inject constructor(
    private val getIndividualCardListUseCase: GetIndividualCardListUseCase,
    private val getCardListForJuridicalUseCase: GetCardListForJuridicalUseCase,
    private val getCashlessUseCase: ExchangeCashlessUseCase,
    private val deleteCardOperationTempUseCase: DeleteCardOperationTempUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
    CarcToCardViewModelInputs,
    CardToCardViewModelOutputs {

    override val inputs: CarcToCardViewModelInputs = this

    override val outputs: CardToCardViewModelOutputs = this

    val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val cardList = PublishSubject.create<ArrayList<CardListModel>>()

    private val currencyList = PublishSubject.create<ArrayList<String>>()

    private val currencyAndCard = PublishSubject.create<Boolean>()

    private val delete = CompletableSubject.create()

    override fun getCardList() {
        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }
        if (isCustomerJuridical) {
            getCardListForJuridicalUseCase.execute(custId, compId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        cardList.onNext(it.cardList)
                        getCurrencies()
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        } else {
            getIndividualCardListUseCase.execute(custId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        cardList.onNext(it.cardList)
                        getCurrencies()
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        }
    }

    private fun getCurrencies() {
        val list = arrayListOf("AZN")
        getCashlessUseCase.execute("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    for (i in 0 until it.exchangeCashlessList.size - 1) {
                        list.add(it.exchangeCashlessList[i].currency)
                    }
                }
            }, {

            },{
                currencyList.onNext(list)
                currencyAndCard.onNext(true)
            }).addTo(subscriptions)
    }

    override fun deleteTemp(tempId: Long) {
        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }

        deleteCardOperationTempUseCase
            .execute(tempId, custId, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    delete.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {

            }).addTo(subscriptions)
    }

    override fun templateDeleted() = delete
    override fun cardListSuccess() = cardList
    override fun currencyAndCardSuccess() = currencyAndCard
    override fun currencyListSuccess() = currencyList

    fun getLang(): String? = sharedPrefs.getString("lang","az")

}
