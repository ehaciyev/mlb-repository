package az.turanbank.mlb.presentation.payments.pay.check.custom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.ChildInvoiceResponseModel
import kotlinx.android.synthetic.main.check_child_item_list_view_custom.view.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class ChooseInvoiceWithSubChildAdapter(
    private val context: Context,
    private val invoice: ArrayList<ChildInvoiceResponseModel>,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<ChooseInvoiceWithSubChildAdapter.ChoosePaymentViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) =
        ChoosePaymentViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.check_child_item_list_view_custom,
                parent,
                false
            )
        )

    override fun getItemCount() = invoice.size

    override fun onBindViewHolder(holder: ChoosePaymentViewHolder, position: Int) {
        holder.bindInvoice(invoice[position], context, onClickListener)
    }

    class ChoosePaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindInvoice(invoice: ChildInvoiceResponseModel, context: Context, onClickListener: (position: Int) -> Unit) {
            val decimalFormat = DecimalFormat("0.00")
            decimalFormat.roundingMode = RoundingMode.HALF_EVEN
            val symbols = DecimalFormatSymbols()
            symbols.decimalSeparator = '.'
            decimalFormat.decimalFormatSymbols = symbols

            if(invoice.serviceName.split("-").size > 1 ){
                itemView.numberText.text = invoice.serviceName.split("-")[0]
                itemView.description.text = invoice.serviceName.split("-")[1]
            } else {
                itemView.numberText.visibility = View.GONE
                itemView.description.text = invoice.serviceName
            }

            itemView.amountInteger.text = decimalFormat.format(invoice.amount).split(".")[0] + ","
            itemView.amountReminder.text = decimalFormat.format(invoice.amount).split(".")[1]

            itemView.setOnClickListener { onClickListener.invoke(adapterPosition) }

        }
    }
}
