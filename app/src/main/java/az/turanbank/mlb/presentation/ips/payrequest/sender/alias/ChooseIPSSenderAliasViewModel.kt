package az.turanbank.mlb.presentation.ips.payrequest.sender.alias

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.request.EnumAliasType
import az.turanbank.mlb.data.remote.model.ips.response.GetAccountAndCustomerInfoByAliasResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.banks.GetAllBanksUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetAccountAndCustomerInfoByAliasUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ChooseIPSSenderAliasViewModelInputs: BaseViewModelInputs{
    fun getBankList()
    fun getAccountAndCustomerInfoByAlias(aliasTypeId:EnumAliasType, value:String)
}
interface ChooseIPSSenderAliasViewModelOutputs: BaseViewModelOutputs{
    fun onBankListSuccess(): PublishSubject<ArrayList<String>>
    fun onAccountAndCustomerInfoByAlias(): PublishSubject<GetAccountAndCustomerInfoByAliasResponseModel>
}
class ChooseIPSSenderAliasViewModel @Inject constructor(
    private val getAllBanksUseCase: GetAllBanksUseCase,
    private val getAccountAndCustomerInfoByAliasUseCase: GetAccountAndCustomerInfoByAliasUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
    ChooseIPSSenderAliasViewModelInputs,
    ChooseIPSSenderAliasViewModelOutputs {
    override val inputs: ChooseIPSSenderAliasViewModelInputs = this

    override val outputs: ChooseIPSSenderAliasViewModelOutputs = this

    private var selectedAliasType: Int = 0
    private val accountAndCustomerInfoByAlias = PublishSubject.create<GetAccountAndCustomerInfoByAliasResponseModel>()


    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val bankList = PublishSubject.create<ArrayList<String>>()

    override fun getBankList() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getAllBanksUseCase
            .execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    val nameList = arrayListOf<String>()
                    it.bankInfos.forEach { bank ->
                        nameList.add(bank.name)
                    }
                    bankList.onNext(nameList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getAccountAndCustomerInfoByAlias(aliasTypeId: EnumAliasType, value: String) {
        getAccountAndCustomerInfoByAliasUseCase
            .execute(custId,token,aliasTypeId,value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it.status.statusCode) {
                    1 -> {
                        accountAndCustomerInfoByAlias.onNext(it)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            },{
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onBankListSuccess() = bankList
    override fun onAccountAndCustomerInfoByAlias() = accountAndCustomerInfoByAlias

    fun getSelectedAliasType() = selectedAliasType

    fun setSelectedAliasType(list: Int){
        when(list){
            0 -> {selectedAliasType = 1}
            1 -> {selectedAliasType = 2}
            2 -> {selectedAliasType = 3}
            3 -> {selectedAliasType = 4}
        }
    }
}