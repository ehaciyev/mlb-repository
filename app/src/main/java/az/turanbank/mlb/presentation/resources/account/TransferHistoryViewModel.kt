package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.response.GetAllOperationListResponseModel
import az.turanbank.mlb.data.remote.model.response.OperationStatusModel
import az.turanbank.mlb.domain.user.usecase.GetOperationStateListUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.*
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface TransferHistoryViewModelInputs : BaseViewModelInputs {
    fun getAllOperationList()
    fun operationAdvancedSearch(
        dtIban: String?,
        minAmt: Double?,
        maxAmt: Double?,
        startDate: String?,
        endDate: String?,
        operStateId: Int?
    )

    fun getAccountList()
    fun getOperationStateList()
}

interface TransferHistoryViewModelOutputs : BaseViewModelOutputs {
    fun getAllOperationListSuccess(): PublishSubject<GetAllOperationListResponseModel>
    fun onAccountSuccess(): BehaviorSubject<java.util.ArrayList<AccountListModel>>
    fun showProgress(): PublishSubject<Boolean>
    fun showAccountProgress(): PublishSubject<Boolean>
    fun getOperationStateProgress(): PublishSubject<Boolean>
    fun getOperationStateSuccess(): PublishSubject<ArrayList<OperationStatusModel>>
    fun operationNotFound(): PublishSubject<Boolean>
    fun operationAdvancedSearchSuccess(): PublishSubject<GetAllOperationListResponseModel>
}

class TransferHistoryViewModel @Inject constructor(
    private val getAllOperationListIndividualUseCase: GetAllOperationListIndividualUseCase,
    private val operationAdvancedSearchUseCase: OperationAdvancedSearchUseCase,
    private val getOperationStateListUseCase: GetOperationStateListUseCase,
    private val getAllOperationListJuridicalUseCase: GetAllOperationListJuridicalUseCase,
    private val getIndividualAccountListUseCase: GetNoCardAccountListForIndividualCustomerUseCase,
    private val getJuridicalAccountListUseCase: GetNoCardAccountListForJuridicalCustomerUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    TransferHistoryViewModelOutputs,
    TransferHistoryViewModelInputs {

    private val accountList = BehaviorSubject.create<ArrayList<AccountListModel>>()

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun getAccountList() {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        showAccountProgress.onNext(true) //TODO Progress
        if (isCustomerJuridical) {
            getJuridicalAccountListUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                lang = enumLangType
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showAccountProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getIndividualAccountListUseCase.execute(custId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showAccountProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun operationNotFound() = operationNotFound

    override fun operationAdvancedSearchSuccess(): PublishSubject<GetAllOperationListResponseModel> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override val inputs: TransferHistoryViewModelInputs = this
    override val outputs: TransferHistoryViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val showProgress = PublishSubject.create<Boolean>()
    private val showAccountProgress = PublishSubject.create<Boolean>()
    private val operationNotFound = PublishSubject.create<Boolean>()
    private val operationStateList = PublishSubject.create<java.util.ArrayList<OperationStatusModel>>()
    private val getOperationStateListProgress = PublishSubject.create<Boolean>()
    private val operationList = PublishSubject.create<GetAllOperationListResponseModel>()

    override fun showProgress(): PublishSubject<Boolean> = showProgress

    override fun showAccountProgress() = showAccountProgress
    override fun getOperationStateProgress() = getOperationStateListProgress
    override fun getOperationStateSuccess() = operationStateList

    override fun getAllOperationList() {
        showProgress.onNext(true)
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        if (isCustomerJuridical) {
            getAllOperationListJuridicalUseCase.execute(custId, compId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        operationList.onNext(it)
                    } else {
                        operationNotFound.onNext(true)
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                })
                .addTo(subscriptions)
        } else {
            getAllOperationListIndividualUseCase.execute(custId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        operationList.onNext(it)
                    } else {
                        operationNotFound.onNext(true)
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                })
                .addTo(subscriptions)
        }
    }

    override fun operationAdvancedSearch(
        dtIban: String?,
        minAmt: Double?,
        maxAmt: Double?,
        startDate: String?,
        endDate: String?,
        operStateId: Int?
    ) {
        var custCompId: Long? = null

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }

        if (isCustomerJuridical) custCompId = compId

        showProgress.onNext(true)
        operationAdvancedSearchUseCase.execute(
            custId,
            custCompId,
            token,
            dtIban,
            minAmt,
            maxAmt,
            startDate,
            endDate,
            operStateId,
            enumLangType
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    operationList.onNext(it)
                } else {
                    operationNotFound.onNext(true)
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)

    }

    override fun getOperationStateList() {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        getOperationStateListProgress.onNext(true)
        getOperationStateListUseCase.execute(custId, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                getOperationStateListProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    operationStateList.onNext(it.respStateList)
                }
            }, {
                getOperationStateListProgress.onNext(false)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun getAllOperationListSuccess() = operationList

    override fun onAccountSuccess() = accountList
}
