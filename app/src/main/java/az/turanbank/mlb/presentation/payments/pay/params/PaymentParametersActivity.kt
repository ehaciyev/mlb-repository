package az.turanbank.mlb.presentation.payments.pay.params

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.ChildInvoiceResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.param.MerchantParamListResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.payments.pay.check.CheckPaymentActivity
import az.turanbank.mlb.presentation.payments.pay.check.regular.ChooseInvoiceActivity
import az.turanbank.mlb.presentation.payments.pay.check.custom.ChooseInvoiceWithChildActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_payment_parameters.*
import kotlinx.android.synthetic.main.activity_payment_parameters.progress
import javax.inject.Inject


class PaymentParametersActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: PaymentParametersViewModel

    lateinit var spinnerParentView: View

    lateinit var editText: EditText

    private val merchantId: Long by lazy { intent.getLongExtra("merchantId", 0L) }
    private val categoryId: Long by lazy { intent.getLongExtra("categoryId", 0L) }
    private val categoryName: String by lazy { intent.getStringExtra("categoryName") }

    private val isFromTemplate: Boolean by lazy { intent.getBooleanExtra("fromTemplate",false) }


    private lateinit var identificationType: String
    private var providerId = 0L
    private lateinit var identificationParamName: String
    private var identificationParamValue: String = ""
    private val merchantName: String by lazy { intent.getStringExtra("merchantName") }
    var merchantDisplayName: String = ""
    private val providerMerchantId: Long by lazy { intent.getLongExtra("providerMerchantId", 0L) }
    private val displayName: String by lazy { intent.getStringExtra("displayName") }

    private val parameters = arrayListOf<ArrayList<MerchantParamListResponseModel>>()

    private val arrayGlobal: ArrayList<MerchantParamListResponseModel> = arrayListOf<MerchantParamListResponseModel>()

    private val arrayGlobalTemplate: ArrayList<MerchantParamListResponseModel> = arrayListOf<MerchantParamListResponseModel>()

    private val sortedArray: ArrayList<ArrayList<MerchantParamListResponseModel>> = arrayListOf<ArrayList<MerchantParamListResponseModel>>()
    private val headerArray: ArrayList<MerchantParamListResponseModel> = arrayListOf<MerchantParamListResponseModel>()

    var amountTemplate: String = ""
    var codePrefix: String = ""

    var serviceCode: String = ""

    private var feeCalculationMethod = ""
    private var feePercent = 0.0
    private var feeMinAmount = 0.0
    private var feeMaxAmount = 0.0

    private val parametersList = arrayListOf<IdentificationCodeRequestModel>()
    private val views = arrayListOf<View>()

    private val viewHeader = arrayListOf<View>()

    private val viewCode = arrayListOf<View>()

    private var fromTemplate: Boolean = false

    private var paramValueFromTemplate: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_parameters)

        toolbar_title.text = displayName
        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewModel =
            ViewModelProvider(this, factory)[PaymentParametersViewModel::class.java]

        animateProgressImage()
        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        viewModel.inputs.getMerchantParams(merchantId)
        submitButton.setOnClickListener {
            parametersList.clear()
            if(fromTemplate){
                views.forEach {
                    it.isFocusable = false
                }
                serviceCode = paramValueFromTemplate
                parametersList.add(IdentificationCodeRequestModel(identificationParamName, paramValueFromTemplate))
                viewModel.inputs.check(
                    merchantId,
                    identificationType,
                    parametersList,
                    categoryId,
                    providerId
                )
                showProgressBar(true)

            } else {
                views.forEach {
                    if((it as AppCompatEditText).text?.trim().toString().isEmpty()){
                        checkInputField(it)
                    } else {
                        parametersList.add(IdentificationCodeRequestModel(it.tag.toString(), codePrefix + it.text.toString()))
                        serviceCode = codePrefix + it.text.toString()

                        viewModel.inputs.check(
                            merchantId,
                            identificationType,
                            parametersList,
                            categoryId,
                            providerId
                        )
                        showProgressBar(true)
                    }
                }

            }

        }
    }

    private fun checkInputField(view: View) {
        if((view as EditText).text.trim().toString().isEmpty()){
            view.error = getString(R.string.enter_search_field)
        } else{
            view.error = null
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onCheckCompleted().subscribe {
            showProgressBar(false)
            if(it.invoice.size == 1 && it.avans.size == 0){
                if(it.invoice[0].respChildInvoiceList.isNullOrEmpty()){
                    var intent = Intent(this, CheckPaymentActivity::class.java)
                        .putExtra("isInvoice", true)
                        .putExtra("transactionId",it.transactionId)
                        .putExtra("merchantDisplayName",merchantDisplayName)
                        .putExtra("transactionNumber",it.transactionNumber)
                        .putExtra("invoice", it.invoice[0])
                        .putExtra("providerId", providerId)
                        .putExtra("serviceCodeUser",serviceCode)
                        .putExtra("categoryId", categoryId)
                        .putExtra("categoryName",categoryName)
                        .putExtra("merchantId", merchantId)
                        .putExtra("merchantName", merchantName)
                        .putExtra("identificationCode", parametersList)
                        .putExtra("identificationType", identificationType)
                        .putExtra("amountTemplate", amountTemplate)
                    startActivityForResult(intent, 7)
                } else {
                    var respArrayList: ArrayList<ArrayList<ChildInvoiceResponseModel>> = arrayListOf()
                    respArrayList.add(it.invoice[0].respChildInvoiceList)
                    var intent = Intent(this, ChooseInvoiceWithChildActivity::class.java)
                        .putExtra("isInvoice", true)
                        .putExtra("merchantDisplayName",merchantDisplayName)
                        .putExtra("respChildInvoice", respArrayList)
                        .putExtra("transactionId",it.transactionId)
                        .putExtra("invoice", it.invoice)
                        .putExtra("transactionNumber", it.transactionNumber)
                        .putExtra("serviceCodeUser", serviceCode)
                        .putExtra("providerId", providerId)
                        .putExtra("categoryId", categoryId)
                        .putExtra("categoryName",categoryName)
                        .putExtra("merchantId", merchantId)
                        .putExtra("merchantName", merchantName)
                        .putExtra("identificationCode", parametersList)
                        .putExtra("identificationType", identificationType)
                        .putExtra("amountTemplate", amountTemplate)
                    startActivityForResult(intent, 7)
                }
            } else if(it.invoice.size == 0 && it.avans.size == 1){
                var intent = Intent(this, CheckPaymentActivity::class.java)
                    .putExtra("isInvoice", false)
                    .putExtra("transactionId",it.transactionId)
                    .putExtra("transactionNumber",it.transactionNumber)
                    .putExtra("merchantDisplayName", merchantDisplayName)
                    .putExtra("providerId", providerId)
                    .putExtra("avans", it.avans[0])
                    .putExtra("amountTemplate", amountTemplate)
                    .putExtra("serviceCodeUser",serviceCode)
                    .putExtra("categoryId", categoryId)
                    .putExtra("categoryName",categoryName)
                    .putExtra("transactionNumber",it.transactionNumber)
                    .putExtra("merchantId", merchantId)
                    .putExtra("merchantName", merchantName)
                    .putExtra("identificationCode", parametersList)
                    .putExtra("identificationType", identificationType)
                startActivityForResult(intent, 7)
            } else if(it.invoice.size == 0 && it.avans.size == 0){
                AlertDialogMapper(this, 1992).showAlertDialogWithCloseOption()
            } else {
                val respCildList: ArrayList<ChildInvoiceResponseModel> = arrayListOf()
                var respArrayList: ArrayList<ArrayList<ChildInvoiceResponseModel>> = arrayListOf()
                for(i in 0 until it.invoice.size){
                    if (it.invoice[i].respChildInvoiceList != null){
                        it.invoice[i].respChildInvoiceList.forEach {
                            respCildList.add(it)
                            respArrayList[i]
                        }
                    }
                }

                if(respCildList.isEmpty()){
                    var isInvoice = it.invoice.size != 0 || it.avans.size <= 0

                    var intent = Intent(this, ChooseInvoiceActivity::class.java)
                        .putExtra("isInvoice", isInvoice)
                        .putExtra("transactionId",it.transactionId)
                        .putExtra("providerId", providerId)
                        .putExtra("merchantDisplayName",merchantDisplayName)
                        .putExtra("invoice", it.invoice)
                        .putExtra("avans", it.avans)
                        .putExtra("transactionNumber",it.transactionNumber)
                        .putExtra("merchantName", merchantName)
                        .putExtra("categoryId", categoryId)
                        .putExtra("categoryName",categoryName)
                        .putExtra("merchantId", merchantId)
                        .putExtra("serviceCodeUser",serviceCode)
                        .putExtra("identificationCode", parametersList)
                        .putExtra("identificationType", identificationType)
                        .putExtra("amountTemplate", amountTemplate)
                    startActivityForResult(intent, 7)
                } else {
                    var intent = Intent(this, ChooseInvoiceWithChildActivity::class.java)
                        .putExtra("isInvoice", true)
                        .putExtra("transactionId",it.transactionId)
                        .putExtra("respChildInvoice", respArrayList)
                        .putExtra("merchantDisplayName",merchantDisplayName)
                        .putExtra("providerId", providerId)
                        .putExtra("invoice", it.invoice)
                        .putExtra("transactionNumber",it.transactionNumber)
                        .putExtra("merchantName", merchantName)
                        .putExtra("merchantDisplayName",merchantDisplayName)
                        .putExtra("categoryId", categoryId)
                        .putExtra("categoryName",categoryName)
                        .putExtra("merchantId", merchantId)
                        .putExtra("serviceCodeUser",serviceCode)
                        .putExtra("identificationCode", parametersList)
                        .putExtra("identificationType", identificationType)
                        .putExtra("amountTemplate", amountTemplate)
                    startActivityForResult(intent, 7)
                }
            }
        }.addTo(subscriptions)
        viewModel.outputs.onInputsReceived().subscribe { fields ->
            if (isFromTemplate) {
                arrayGlobalTemplate.addAll(fields)
                templateMode()
            } else{
                arrayGlobal.addAll(fields)
                addView(fields[0],false)
            }

            progress.clearAnimation()
            progress.visibility = View.GONE
            container.visibility = View.VISIBLE

        }.addTo(subscriptions)
        viewModel.onError().subscribe {
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)

        viewModel.outputs.onTemplateRemoved().subscribe {
            finish()
        }.addTo(subscriptions)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }

    private fun templateMode() {
        fromTemplate = true

        deleteTemp.visibility = View.VISIBLE
        //spinnerParentView.visibility = View.GONE
        val templatePaymentParameters = intent.getStringExtra("description").split(";")

        val identificationType = templatePaymentParameters[0]

        val parametersListTemp = arrayListOf<IdentificationCodeRequestModel>()

        if(templatePaymentParameters.size > 2){
            parametersListTemp.add(IdentificationCodeRequestModel(templatePaymentParameters[1].split(":")[0],
                templatePaymentParameters[1].split(":")[1]))

            parametersListTemp.add(IdentificationCodeRequestModel(templatePaymentParameters[2].split(":")[0],
                templatePaymentParameters[2].split(":")[1]))
        } else {
            parametersListTemp.add(IdentificationCodeRequestModel(templatePaymentParameters[1].split(":")[0],
                templatePaymentParameters[1].split(":")[1]))
        }

        val inputs = getInputsForIdentificationType(identificationType)

        toolbar_title.text = inputs[0].merchantDisplayName

        refreshableContainer.removeAllViews()
        views.clear()

        paramValueFromTemplate = parametersListTemp[0].paramValue!!

        if(inputs[0].paramType =="select"){
            val number = paramValueFromTemplate.substring(2,paramValueFromTemplate.length )
            val numberCode = paramValueFromTemplate.substring(0,2)
            addView(inputs[0],true)
            (views[0] as AppCompatEditText).setText(number)
            (viewCode[0] as EditText).setText(numberCode)
            if(viewHeader.size==0){

            } else {
                (viewHeader[0] as EditText).setText(inputs[0].paramDisplayName)
            }


        } else if(inputs[0].paramType == "text"){
            addView(inputs[0],true)
            for (i in views.indices) {
                (views[i] as AppCompatEditText).setText(parametersListTemp[i].paramValue)
            }

        }

        deleteTemp.setOnClickListener {
            showDeleteTemp(
                intent.getLongExtra(
                    "templateId",
                    0L
                )
            )
        }
    }

    private fun showDeleteTemp(tempId: Long) {

        val alert = androidx.appcompat.app.AlertDialog.Builder(this)
        alert.setMessage(getString(R.string.template_delete_confirmation_message))

        alert.setCancelable(true)
        alert.setPositiveButton(
            getString(R.string.yes)
        ) { _, _ ->
            viewModel.inputs.removeTemplate(tempId)
        }

        alert.setNegativeButton(
            getString(R.string.no)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }


    private fun getInputsForIdentificationType(identificationType: String): ArrayList<MerchantParamListResponseModel> {
        var paramList = arrayListOf<MerchantParamListResponseModel>()
        arrayGlobalTemplate.forEach {
            if (it.merchantDescName == identificationType) {
                paramList.add(it)
            }
        }
        return paramList
    }

    private val arrayText: ArrayList<MerchantParamListResponseModel> = arrayListOf<MerchantParamListResponseModel>()
    private val arraySelect: ArrayList<MerchantParamListResponseModel> = arrayListOf<MerchantParamListResponseModel>()

    private fun addView(model: MerchantParamListResponseModel, fromTemplate: Boolean) {
        if(fromTemplate){
            if(arrayGlobalTemplate.size == 1){
                if(model.paramType == "select"){
                    if(model.paramDisplayName.isNullOrEmpty()){
                        AlertDialogMapper(this, 1993).showAlertDialogWithCloseOption()
                    } else {
                        makeNumberView(model)
                    }
                } else if(model.paramType == "text") {
                    if(model.paramDisplayName.isNullOrEmpty()){
                        AlertDialogMapper(this, 1993).showAlertDialogWithCloseOption()
                    } else {
                        makeEditText(model)
                    }
                }
            } else {
                sortArray(arrayGlobalTemplate, headerArray, sortedArray)
                if(model.paramDisplayName.isNullOrEmpty()){
                    AlertDialogMapper(this, 1993).showAlertDialogWithCloseOption()
                } else {
                    makeSpinner(model)
                }
            }
        } else {

            if(arrayGlobal.size ==1){
                if(model.paramType == "select"){
                    if(model.paramDisplayName.isNullOrEmpty()){
                        AlertDialogMapper(this, 1993).showAlertDialogWithCloseOption()
                    } else{
                        makeNumberView(model)
                    }
                } else if(model.paramType == "text") {
                    if(model.paramDisplayName.isNullOrEmpty()){
                        AlertDialogMapper(this, 1993).showAlertDialogWithCloseOption()
                    } else{
                        makeEditText(model)
                    }
                }
            } else {
                arrayGlobal.forEach{
                    when (it.paramType) {
                        "text" -> {
                            arrayText.add(it)
                        }
                        "select" -> {
                            arraySelect.add(it)
                        }
                        else -> {

                        }
                    }
                }
                sortArray(arrayGlobal, headerArray, sortedArray)
                if(model.paramDisplayName.isNullOrEmpty()){
                    AlertDialogMapper(this, 1993).showAlertDialogWithCloseOption()
                } else{
                    makeSpinner(model)
                }
            }
        }

    }

    private fun makeEditText(model: MerchantParamListResponseModel) {
        codePrefix = ""
        identificationType = model.merchantDescName
        providerId = model.providerId
        identificationParamName = model.paramName
        merchantDisplayName = model.merchantDisplayName

        val ll = LayoutInflater.from(this).inflate(
            R.layout.edit_text,
            container,
            false
        ) as LinearLayout
        editText = ll.findViewById<EditText>(R.id.inputHolder)
        editText.hint = model.paramDisplayName
        if (model.format == "string") {
            editText.inputType = InputType.TYPE_CLASS_TEXT
        } else {
            editText.inputType = InputType.TYPE_CLASS_NUMBER
        }
        editText.filters = arrayOf(InputFilter.LengthFilter(model.maxLen))

        editText.tag = model.paramName
        editText.isFocusable = !fromTemplate
        views.add(editText)
        refreshableContainer.addView(ll)
    }

    private fun makeStationaryView(model: MerchantParamListResponseModel) {
        merchantDisplayName = model.merchantDisplayName
        codePrefix = ""
        identificationType = model.merchantDescName
        providerId = model.providerId
        identificationParamName = model.paramName

        val ll = LayoutInflater.from(this).inflate(
            R.layout.spinner_stationary_number,
            container,
            false
        ) as LinearLayout

        var list: Array<String?> = model.paramDesc.substring(0,model.paramDesc.length - 1).split(",").toTypedArray()
        var listCode: Array<String?> = model.paramValue.substring(0,model.paramValue.length - 1).split(",").toTypedArray()

        val itemCodeHolder = ll.findViewById<EditText>(R.id.itemStationaryCodeHolder)
        val itemHolder = ll.findViewById<EditText>(R.id.itemStationaryHolder)

        //itemCodeHolder.setText(list[0])
        itemCodeHolder.hint = list[0]
        itemCodeHolder.tag = model.paramName

        codePrefix = listCode[0].toString()

        itemHolder.hint = model.paramDisplayName
        itemHolder.tag = model.merchantDescName

        if (model.format == "string") {
            itemHolder.inputType = InputType.TYPE_CLASS_TEXT
        } else {
            itemHolder.inputType = InputType.TYPE_CLASS_NUMBER
        }
        itemHolder.filters = arrayOf(InputFilter.LengthFilter(50))

        if(fromTemplate){
            itemHolder.isFocusable = false
            itemCodeHolder.isFocusable = false

        } else {
            itemHolder.isFocusable = true
            itemCodeHolder.isFocusable = true
            itemCodeHolder.setOnClickListener {
                val builder: android.app.AlertDialog.Builder =
                    android.app.AlertDialog.Builder(this)
                builder.setItems(list) { _, which ->
                    itemCodeHolder.setText(list[which])
                    codePrefix = listCode[which].toString()
                }
                val dialog = builder.create()
                dialog.show()
            }
        }

        //views.add(itemCodeHolder)
        viewCode.add(itemCodeHolder)
        views.add(itemHolder)
        refreshableContainer.addView(ll)

    }

    private fun makeSpinner(model: MerchantParamListResponseModel) {
        merchantDisplayName = model.merchantDisplayName
        codePrefix = ""
        identificationType = model.merchantDescName
        providerId = model.providerId
        val ll = LayoutInflater.from(this).inflate(
            R.layout.spinner,
            container,
            false
        ) as LinearLayout
        val itemHolder = ll.findViewById<EditText>(R.id.itemHolder)
        itemHolder.setText(model.merchantDescValue)
        itemHolder.hint = model.paramDisplayName
        itemHolder.tag = model.paramName

        var position: Int = 0
        var selectedType: String = model.paramType

        if(fromTemplate){
            itemHolder.isFocusable = false
        } else {
            itemHolder.setOnClickListener {
                val builder: android.app.AlertDialog.Builder =
                    android.app.AlertDialog.Builder(this)

                var arrayList : ArrayList<String> = arrayListOf()
                headerArray.forEach {
                    arrayList.add(it.merchantDescValue)
                }
                var list = arrayList.toTypedArray()

                builder.setItems(list) { _, which ->
                    itemHolder.setText(list[which])
                    position = which
                    selectedType = "text"
                    for(i in 0 until views.size){
                        views.removeAt(0)
                    }

                    refreshableContainer.removeAllViews()
                    if(headerArray[which].paramType =="text"){
                        sortedArray[position].forEach{
                            makeEditText(it)
                        }
                    } else {
                        if(providerMerchantId == 940000L){
                            makeStationaryView(headerArray[which])
                        } else {
                            makeNumberView(headerArray[which])
                        }
                    }

                }
                val dialog = builder.create()
                dialog.show()
            }
        }

        if(selectedType =="text"){
            viewHeader.add(itemHolder)
            staticContainer.addView(ll)
            refreshableContainer.removeAllViews()
            sortedArray[0].forEach{
                makeEditText(it)
            }
        } else if(selectedType =="select"){
            viewHeader.add(itemHolder)
            staticContainer.addView(ll)
            refreshableContainer.removeAllViews()
            if(providerMerchantId == 940000L){
                makeStationaryView(model)
            } else {
                makeNumberView(model)
            }
        }
    }


    var position: Int = 0

    private fun makeNumberView(model: MerchantParamListResponseModel) {
        merchantDisplayName = model.merchantDisplayName
        codePrefix = ""
        identificationType = model.merchantDescName
        providerId = model.providerId
        identificationParamName = model.paramName

        val ll = LayoutInflater.from(this).inflate(
            R.layout.spinner_number,
            container,
            false
        ) as LinearLayout

        var list: Array<String?> = model.paramDesc.substring(0,model.paramValue.length - 1).split(",").toTypedArray()
        var listCode: Array<String?> = model.paramValue.substring(0,model.paramValue.length - 1).split(",").toTypedArray()

        val itemCodeHolder = ll.findViewById<EditText>(R.id.itemCodeHolder)
        val itemHolder = ll.findViewById<EditText>(R.id.itemHolder)
        val numberCodeHolder = ll.findViewById<LinearLayout>(R.id.numberCodeHolder)

        //itemCodeHolder.setText(list[0])
        itemCodeHolder.hint = list[0]
        itemCodeHolder.tag = model.paramName

        codePrefix = listCode[0].toString()

        itemHolder.hint = model.paramDisplayName
        itemHolder.tag = model.merchantDescName

        if (model.format == "string") {
            itemHolder.inputType = InputType.TYPE_CLASS_TEXT
        } else {
            itemHolder.inputType = InputType.TYPE_CLASS_NUMBER
        }
        itemHolder.filters = arrayOf(InputFilter.LengthFilter(50))

        if(fromTemplate){
            itemHolder.isFocusable = false
            itemCodeHolder.isFocusable = false

        } else {
            itemHolder.isFocusable = true
            itemCodeHolder.isFocusable = true
            numberCodeHolder.setOnClickListener {
                val builder: android.app.AlertDialog.Builder =
                    android.app.AlertDialog.Builder(this)
                builder.setItems(list) { _, which ->
                    itemCodeHolder.setText(list[which])
                    codePrefix = listCode[which].toString()
                }
                val dialog = builder.create()
                dialog.show()
            }
        }

        //views.add(itemCodeHolder)
        viewCode.add(itemCodeHolder)
        views.add(itemHolder)
        refreshableContainer.addView(ll)

    }



    private fun animateProgressImage() {
        container.visibility = View.GONE
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }

    private fun sortArrayToArrays(
        unsortedArray: ArrayList<MerchantParamListResponseModel>,
        sortedArray: ArrayList<MerchantParamListResponseModel>,
        copyArray: ArrayList<MerchantParamListResponseModel>
    ) {
        val copy = arrayListOf(unsortedArray[0])
        sortedArray.add(copy[0])
        unsortedArray.removeAt(0)

        unsortedArray.forEach {
            if (it.merchantDescId == copy[0].merchantDescId) {
                copy.add(it)
            } else {
                sortedArray.add(it)
            }
        }
        copyArray.addAll(copy)
        //unsortedArray.removeAll(copy)

        /*if (unsortedArray.size != 0) {
            sortArrayToArrays(unsortedArray, sortedArray,copyArray)
        }*/
    }

    private fun sortArray(
        unsortedArray: ArrayList<MerchantParamListResponseModel>,
        headerArray: ArrayList<MerchantParamListResponseModel>,
        sortedArray: ArrayList<ArrayList<MerchantParamListResponseModel>>
    ) {
        val copy = arrayListOf(unsortedArray[0])
        unsortedArray.removeAt(0)
        unsortedArray.forEach {
            if (it.merchantDescId == copy[0].merchantDescId) {
                copy.add(it)
            } else {

            }
        }
        unsortedArray.removeAll(copy)
        headerArray.add(copy[0])
        sortedArray.add(copy)
        if (unsortedArray.size != 0) {
            sortArray(unsortedArray, headerArray, sortedArray)
        }
    }



    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateButtonProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateButtonProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}