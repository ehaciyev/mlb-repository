package az.turanbank.mlb.presentation.profile

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.PasswordChangeIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.PasswordChangeJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface PasswordChangeViewModelInputs : BaseViewModelInputs {
    fun changePassword(currentPassword: String?, password: String?, repeatPassword: String?)
}

interface PasswordChangeViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun changePasswordFinished(): PublishSubject<Boolean>
}


class PasswordChangeViewModel @Inject constructor(
    private val passwordChangeJuridicalUseCase: PasswordChangeJuridicalUseCase,
    private val passwordChangeIndividualUseCase: PasswordChangeIndividualUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    PasswordChangeViewModelInputs,
    PasswordChangeViewModelOutputs {

    override val inputs: PasswordChangeViewModelInputs = this
    override val outputs: PasswordChangeViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val showProgress = PublishSubject.create<Boolean>()
    private val changePasswordFinished = PublishSubject.create<Boolean>()

    override fun showProgress() = showProgress

    override fun changePasswordFinished() = changePasswordFinished


    override fun changePassword(
        currentPassword: String?,
        password: String?,
        repeatPassword: String?
    ) {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            passwordChangeJuridicalUseCase.execute(
                custId,
                compId,
                token,
                currentPassword,
                password,
                repeatPassword
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)

                    if (it.status.statusCode == 1) {
                        changePasswordFinished.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        } else {
            passwordChangeIndividualUseCase.execute(
                custId,
                token,
                currentPassword,
                password,
                repeatPassword
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)

                    if (it.status.statusCode == 1) {
                        changePasswordFinished.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        }
    }
}