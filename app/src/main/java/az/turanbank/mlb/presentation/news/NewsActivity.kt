package az.turanbank.mlb.presentation.news

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.CampaignModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.news.internal.InternalNewsActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_news.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class NewsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: NewsViewModel

    private val campaignList = arrayListOf<CampaignModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        toolbar_title.text = getString(R.string.compaigns)
        toolbar_back_button.setOnClickListener{onBackPressed()}

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        viewModel =
            ViewModelProvider(this, factory)[NewsViewModel::class.java]

        recyclerView.adapter = NewsAdapter(this, campaignList){
            val intent = Intent(this, InternalNewsActivity::class.java)
            intent.putExtra("campaignId", campaignList[it].campaignId)
            startActivity(intent)
        }

        setOutputListeners()
        setInputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.campaignsSuccess().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            failMessage.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            campaignList.clear()
            campaignList.addAll(it)
            recyclerView.adapter?.notifyDataSetChanged()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            failMessage.visibility = View.VISIBLE
            progress.clearAnimation()
            progress.visibility = View.GONE
            recyclerView.visibility = View.GONE
        }.addTo(subscriptions)
    }
    private fun setInputListeners(){
        animateProgressImage()
        viewModel.inputs.getCampaigns()
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
        failMessage.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
