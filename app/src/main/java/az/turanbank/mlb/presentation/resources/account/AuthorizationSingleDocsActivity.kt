package az.turanbank.mlb.presentation.resources.account

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import az.turanbank.mlb.R

class AuthorizationSingleDocsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorization_single_docs)
    }
}
