package az.turanbank.mlb.presentation.resources.account

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Base64
import android.widget.LinearLayout
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.resources.account.abroad.edit.EditOperationAbroadActivity
import az.turanbank.mlb.presentation.resources.account.exchange.edit.EditConvertMoneyActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_single_operation_details.*
import java.io.*
import javax.inject.Inject


class SingleOperationDetailsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: OperationDetailsViewModel
    lateinit var dialog: MlbProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_operation_details)

        dialog = MlbProgressDialog(this)

        viewmodel = ViewModelProvider(this, factory)[OperationDetailsViewModel::class.java]

        detailOpClose.setOnClickListener {
            finish()
        }

        when (intent.getIntExtra("operStateId", 1)) {
            1 -> {
                disable(operationActionDownloadAdoc)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_blue
                    )
                )
            }
            2 -> {
                disable(operationActionDelete, operationActionEdit)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_blue
                    )
                )
            }
            6 -> {
                disable(operationActionDelete, operationActionEdit)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_black
                    )
                )
            }
            7 -> {
                disable(operationActionDelete, operationActionEdit)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_black
                    )
                )
            }
            3, 4 -> {
                disable(operationActionDelete, operationActionEdit)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_yellow
                    )
                )
            }
            5 -> {
                disable(operationActionDelete, operationActionEdit)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_green
                    )
                )
            }
            8 -> {
                disable(operationActionDelete, operationActionEdit)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_red
                    )
                )
            }
        }

        setOutputListeners()
        setInputListeners()
    }

    private fun isStoragePermissionGranted(requestCode: Int): Boolean {
        return if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED
            )
                true
            else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    requestCode
                )
                false
            }
        } else {
            true
        }
    }

    private fun disable(vararg actions: LinearLayout) {
        for(action in actions) {
            action.getChildAt(0).isClickable = false
            action.background = ContextCompat.getDrawable(this, R.drawable.operation_action_disabled_state)
            action.isClickable = false
            action.isFocusable = false
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val requestedPermissionsCount = permissions.size
        var grantedPermissionsCount = 0

        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                grantedPermissionsCount++
            }
        }

        if (requestedPermissionsCount == grantedPermissionsCount) {
            if (requestCode == 1) {
                viewmodel.inputs.getPaymentDocByOperId(intent.getLongExtra("operId", 0L))
            } else if (requestCode == 2) {
                viewmodel.inputs.getAsanDocByOperId(intent.getLongExtra("operId", 0L))
            }
        }
    }

    private fun setOutputListeners() {
        viewmodel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
        viewmodel.outputs.paymentDoc().subscribe {
            val fileName = intent.getLongExtra("operId", 0L).toString() + ".pdf"
            val file = getFileDestination(fileName)
            //  val destPath = getExternalFilesDir(null)?.absolutePath
            //  val file = File(destPath, fileName)

            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                inputStream = ByteArrayInputStream(byteArray)

                file?.let {f ->
                    outputStream = FileOutputStream(f)
                }

                while (true) {
                    val read = inputStream.read(fileReader)

                    if (read == -1) {
                        break
                    }
                    outputStream?.write(fileReader, 0, read)
                }

                Toast.makeText(this, fileName + getString(R.string.successfully_downloaded), Toast.LENGTH_LONG)
                    .show()

                openDownloadedPdfFile(file)

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.paymentADoc().subscribe {
            val fileName = intent.getLongExtra("operId", 0L).toString() + ".adoc"

            //  val filePath = File(Environment.getExternalStorageDirectory(), null)
            val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName)
            //   val destPath = getExternalFilesDir(null)?.absolutePath
            //   val file = File(destPath, fileName)

            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)

            var iStream: InputStream? = null
            var oStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                iStream = ByteArrayInputStream(byteArray)
                oStream = FileOutputStream(file)

                while (true) {
                    val read = iStream.read(fileReader)

                    if (read == -1) {
                        break
                    }
                    oStream.write(fileReader, 0, read)
                }

                Toast.makeText(this, fileName + getString(R.string.successfully_downloaded), Toast.LENGTH_LONG)
                    .show()

                openDirectoryIntent(file)

                //openAdocFolder(Environment.getExternalStorageDirectory())

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                iStream?.close()
                oStream?.close()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.operationDeleted().subscribe {
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(subscriptions)

        viewmodel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.operationDetails().subscribe {
            val currency: String = if(it.operTypeId == 5 || it.operTypeId ==6){
                it.crCcy
            } else {
                it.amtCcy
            }
            val amount: String = if(it.operTypeId == 5 || it.operTypeId ==6){
                it.crAmt.toString()
            } else {
                it.amt.toString()
            }

            val fullAmount = "$amount $currency"
            opDetailType.text = intent.getStringExtra("operName")
            detailOpNo.text = it.operId.toString()
            detailOpDtAccId.text = it.dtIban
            detailOpCrAccountName.text = it.crCustName
            detailOpCrAccId.text = it.crIban
            detailOpAmount.text = fullAmount
            detailOpPurpose.text = it.operPurpose
            detailOpCreatedDate.text = intent.getStringExtra("createdDate")
            detailOpStatusTitle.text = intent.getStringExtra("operState")

            detailOpShare.setOnClickListener { _ ->
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, it.toString())
                startActivity(Intent.createChooser(shareIntent, "Göndər..."))
            }

            if(operationActionEdit.isClickable)
            operationActionEdit.setOnClickListener { _ ->
                when (it.operNameId) {
                    2L -> {
                        val editIntent = Intent(
                            this@SingleOperationDetailsActivity,
                            EditOperationInLandActivity::class.java
                        )
                        editIntent.putExtra("operId", intent.getLongExtra("operId", 0L))
                        startActivityForResult(editIntent, 88)
                    }

                    1L -> {
                        val editIntent = Intent(
                            this@SingleOperationDetailsActivity,
                            EditOperationInternalActivity::class.java
                        )
                        editIntent.putExtra("operId", intent.getLongExtra("operId", 0L))
                        startActivityForResult(editIntent, 88)
                    }
                    3L -> {
                        val editIntent = Intent(
                            this@SingleOperationDetailsActivity,
                            EditOperationAbroadActivity::class.java
                        )
                        editIntent.putExtra("operId", intent.getLongExtra("operId", 0L))
                        startActivityForResult(editIntent, 88)
                    }
                    4L -> {
                        val editIntent = Intent(
                            this@SingleOperationDetailsActivity,
                            EditConvertMoneyActivity::class.java
                        )
                        editIntent.putExtra("operId", intent.getLongExtra("operId", 0L))
                        editIntent.putExtra("operTypeId", intent.getIntExtra("operTypeId", 0))
                        startActivityForResult(editIntent, 88)
                    }
                }
            }

            if(operationActionDelete.isClickable)
            operationActionDelete.setOnClickListener {
                AlertDialog.Builder(this)
                    .setTitle(getString(R.string.attention))
                    .setMessage(getString(R.string.delete_confirmation_message))
                    .setPositiveButton(getString(R.string.yes)) { _, _ ->
                        viewmodel.inputs.deleteOperation(intent.getLongExtra("operId", 0L))
                    }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
            }

            if(operationActionDownloadPdf.isClickable)
            operationActionDownloadPdf.setOnClickListener {
                if (isStoragePermissionGranted(1)) {
                    viewmodel.inputs.getPaymentDocByOperId(intent.getLongExtra("operId", 0L))
                }
            }

            if(operationActionDownloadAdoc.isClickable)
            operationActionDownloadAdoc.setOnClickListener {
                if (isStoragePermissionGranted(2)) {
                    viewmodel.inputs.getAsanDocByOperId(intent.getLongExtra("operId", 0L))
                }
            }

        }.addTo(subscriptions)
    }


    private fun getFileDestination(fileName: String): File? {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            return File(Environment.getExternalStorageDirectory(), fileName)
        }
        return getExternalFilesDir(null)
    }

    private fun openDownloadedPdfFile(file: File?) {
        try {
            val apkURI = FileProvider.getUriForFile(
                this,
                applicationContext
                    .packageName + ".provider", file!!
            )

            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(apkURI, "application/pdf")
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun openAdocFolder(file: File) {
        try {

            val intent = Intent(Intent.ACTION_VIEW)
            val uri = Uri.parse(file.path)
            intent.setDataAndType(uri, "resource/folder")
            startActivity(Intent.createChooser(intent, "Open folder"))

        } catch (e: ActivityNotFoundException) {

        }
    }

    fun openDirectoryIntent(file: File) {
        val selectedUri = FileProvider.getUriForFile(this, "$packageName.fp", file)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
       // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(selectedUri, "*/*")
        startActivity(intent)
    }

    private fun setInputListeners() {
        viewmodel.inputs.getOperationById(intent.getLongExtra("operId", 0L))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 88) {
            if (resultCode == Activity.RESULT_OK) {
                viewmodel.inputs.getOperationById(intent.getLongExtra("operId", 0L))

                detailOpClose.setOnClickListener {
                    setResult(resultCode)
                    finish()
                }
            }
        }
    }
}
