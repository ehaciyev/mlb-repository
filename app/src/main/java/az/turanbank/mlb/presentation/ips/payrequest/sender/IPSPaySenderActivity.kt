package az.turanbank.mlb.presentation.ips.payrequest.sender

import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_ipstransfer.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class IPSPaySenderActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ipstransfer)

        toolbar_title.text = getString(R.string.send_pay_request)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        view_pager.adapter =
            IPSPaySenderPagerAdapter(
                this,
                supportFragmentManager
            )
        tabs.setupWithViewPager(view_pager)
    }
}
