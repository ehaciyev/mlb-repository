package az.turanbank.mlb.presentation.resources.account

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.AuthOperation
import kotlinx.android.synthetic.main.individual_auth_list_item.view.*

class AuthorizationMultiSelectRecyclerViewAdapter(
    private val context: Context,
    private val operationList: ArrayList<AuthOperation>,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<AuthorizationMultiSelectRecyclerViewAdapter.AuthMultiSelectViewHolder>() {

    private val selectedOperationList: ArrayList<AuthOperation> = arrayListOf()
    private var isMultiSelectModeActive = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AuthMultiSelectViewHolder =
        AuthMultiSelectViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.individual_auth_list_item,
                parent,
                false
            )
        )

    override fun getItemCount() = operationList.size

    private fun activateMultiSelection() {
        isMultiSelectModeActive = true
    }

    private fun deactivateMultiSelection() {
        isMultiSelectModeActive = true
    }

    fun setData(list: ArrayList<AuthOperation>) {
        operationList.clear()
        operationList.addAll(list)
        notifyDataSetChanged()
    }

    fun setAllSelected() {
        selectedOperationList.clear()
        for (op in operationList) {
            op.isSelected = true
            selectedOperationList.add(op)
        }
        activateMultiSelection()
        notifyDataSetChanged()
    }

    fun getAllSelectedItems() = selectedOperationList

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AuthMultiSelectViewHolder, position: Int) {
        val authOperation = operationList[position]
        val currency: String? = if(authOperation.operTypeId ==5 || authOperation.operTypeId ==6){
            authOperation.crCcy
        } else {
            authOperation.currency
        }
        val amount: String = if(authOperation.operTypeId ==5 || authOperation.operTypeId ==6){
            authOperation.crAmt.toString()
        } else {
            authOperation.amount.toString()
        }
        holder.itemView.authOpCrName.text = authOperation.crName
        holder.itemView.authOpCrIban.text = authOperation.crIban
        holder.itemView.authOpAmount.text = "$amount $currency"
        holder.itemView.authOpDate.text = authOperation.createdDate

        holder.itemView.authOpCheckBox.setOnCheckedChangeListener { _, isChecked ->
            if (isMultiSelectModeActive) {
                if (isChecked) {
                    if(!selectedOperationList.contains(authOperation))
                        selectedOperationList.add(authOperation)
                    authOperation.isSelected = true
                  //  holder.itemView.authOpCrName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    holder.itemView.authOperationContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.grayish))
                    clickListener.invoke(selectedOperationList.size)
                } else {
                    if(selectedOperationList.contains(authOperation))
                    authOperation.isSelected = false
                    selectedOperationList.remove(authOperation)
                    holder.itemView.authOperationContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                    clickListener.invoke(selectedOperationList.size)
                }
            }
        }

        if (isMultiSelectModeActive) {
            holder.itemView.authOpCheckBox.visibility = View.VISIBLE
        } else {
            holder.itemView.authOpCheckBox.visibility = View.GONE
        }

        if (authOperation.isSelected) {
            holder.itemView.authOpCheckBox.visibility = View.VISIBLE
           /* holder.itemView.authOpCrName.setTextColor(
                ContextCompat.getColor(context, R.color.colorPrimary)
            )*/
            holder.itemView.authOperationContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.grayish))
            holder.itemView.authOpCheckBox.isChecked = true
        } else {
          //  holder.itemView.authOpCrName.setTextColor(ContextCompat.getColor(context, R.color.dark_gray))
            holder.itemView.authOperationContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            holder.itemView.authOpCheckBox.isChecked = false
        }

        holder.itemView.setOnLongClickListener {
            if (!isMultiSelectModeActive) {
                activateMultiSelection()
                authOperation.isSelected = true
                notifyDataSetChanged()
            } else {
                deactivateMultiSelection()
                deselectAll()
            }
            true
        }

        holder.itemView.setOnClickListener {
            if (authOperation.isSelected) {
                authOperation.isSelected = false
                selectedOperationList.remove(authOperation)
           //     holder.itemView.authOpCrName.setTextColor(ContextCompat.getColor(context, R.color.dark_gray))
                holder.itemView.authOperationContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                notifyDataSetChanged()
            } else {
                if(selectedOperationList.size>0){
                    if(selectedOperationList[0].operStateId == authOperation.operStateId){
                        authOperation.isSelected = true
                        selectedOperationList.add(authOperation)
                        holder.itemView.authOperationContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.grayish))
                        notifyDataSetChanged()
                    } else {
                        authOperation.isSelected = false
                    }
                }

             //   holder.itemView.authOpCrName.setTextColor( ContextCompat.getColor( context, R.color.colorPrimary))

            }
        }
        clickListener.invoke(selectedOperationList.size)
    }

    fun deselectAll() {
        selectedOperationList.clear()
        for (op in operationList) {
            op.isSelected = false
        }
        clickListener.invoke(0)
        isMultiSelectModeActive = false
        notifyDataSetChanged()
    }

    class AuthMultiSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}