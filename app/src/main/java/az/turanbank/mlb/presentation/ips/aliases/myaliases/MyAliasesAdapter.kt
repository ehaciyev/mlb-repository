package az.turanbank.mlb.presentation.ips.aliases.myaliases

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.RespIpsModelAccount
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.alias_item.view.*
import javax.inject.Inject

@Suppress("DEPRECATION")
class MyAliasesAdapter(
    context: Context,
    private var aliases: ArrayList<MLBAliasResponseModel>,
    private var onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<MyAliasesAdapter.MyAliasesViewHolder>() {

    class MyAliasesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(aliases: ArrayList<MLBAliasResponseModel>, onClickListener: (position: Int) -> Unit) {
            itemView.setOnClickListener { onClickListener.invoke(adapterPosition) }
            itemView.aliasName.text = aliases[position].ipsValue
            itemView.aliasType.text = aliases[position].ipsType
            itemView.aliastoaccount.text = aliases[position].expirationDate
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyAliasesViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.alias_item, parent, false)
    )

    override fun getItemCount() = aliases.size

    override fun onBindViewHolder(holder: MyAliasesViewHolder, position: Int) {
        holder.bind(aliases, onClickListener)
    }
}