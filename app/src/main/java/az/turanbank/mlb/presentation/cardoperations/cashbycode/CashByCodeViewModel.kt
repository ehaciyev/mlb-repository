package az.turanbank.mlb.presentation.cardoperations.cashbycode

import android.content.SharedPreferences
import android.util.Log
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.domain.user.usecase.cardoperations.DeleteCardOperationTempUseCase
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashlessUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetCardListForJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetIndividualCardListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import java.util.ArrayList
import javax.inject.Inject

interface CashByCodeViewModelInputs: BaseViewModelInputs{
    fun getCardList()
    fun deleteTemp(tempId: Long)
}
interface CashByCodeViewModelOutputs: BaseViewModelOutputs{
    fun cardListSuccess(): PublishSubject<ArrayList<CardListModel>>
    fun currencyListSuccess(): PublishSubject<ArrayList<String>>
    fun currencyAndCardSuccess(): PublishSubject<Boolean>
    fun templateDeleted(): CompletableSubject
}
class CashByCodeViewModel @Inject constructor(

    private val getIndividualCardListUseCase: GetIndividualCardListUseCase,
    private val getCardListForJuridicalUseCase: GetCardListForJuridicalUseCase,
    private val getCashlessUseCase: ExchangeCashlessUseCase,
    private val deleteCardOperationTempUseCase: DeleteCardOperationTempUseCase,
    sharedPrefs: SharedPreferences
    ) : BaseViewModel(),
        CashByCodeViewModelInputs, CashByCodeViewModelOutputs{
    override val inputs: CashByCodeViewModelInputs = this
    override val outputs: CashByCodeViewModelOutputs = this

    private val cardList = PublishSubject.create<ArrayList<CardListModel>>()
    private val currencyList = PublishSubject.create<ArrayList<String>>()

    private val currencyAndCard = PublishSubject.create<Boolean>()

    private val delete = CompletableSubject.create()

    val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun getCardList() {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        if (isCustomerJuridical) {
            getCardListForJuridicalUseCase.execute(custId, compId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        cardList.onNext(it.cardList)
                    } else
                        error.onNext(it.status.statusCode)
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        } else {
            getIndividualCardListUseCase.execute(custId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        cardList.onNext(it.cardList)
                    } else
                        error.onNext(it.status.statusCode)
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                },{
                    getCurrencyList()
                })
                .addTo(subscriptions)
        }
    }
    private fun getCurrencyList() {
        val list = arrayListOf("AZN")
        Log.i("FIRST PART OF CURRENCY", "ENTERING HERE")
        getCashlessUseCase.execute("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    for (i in 0 until it.exchangeCashlessList.size - 1) {
                        list.add(it.exchangeCashlessList[i].currency)
                    }
                }
            }, {

            },{
                currencyList.onNext(list)
                currencyAndCard.onNext(true)
            }).addTo(subscriptions)
    }

    override fun deleteTemp(tempId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }

        deleteCardOperationTempUseCase
            .execute(tempId, custId, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    delete.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {

            }).addTo(subscriptions)
    }

    override fun templateDeleted() = delete

    override fun cardListSuccess() = cardList

    override fun currencyListSuccess() = currencyList

    override fun currencyAndCardSuccess() = currencyAndCard
}