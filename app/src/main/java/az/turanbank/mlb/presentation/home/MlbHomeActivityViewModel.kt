package az.turanbank.mlb.presentation.home

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetProfileImageResponseModel
import az.turanbank.mlb.domain.user.usecase.GetProfileImageUseCase
import az.turanbank.mlb.domain.user.usecase.main.CheckTokenUseCase
import az.turanbank.mlb.domain.user.usecase.main.LogoutUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface MlbHomeActivityViewModelInputs : BaseViewModelInputs {
    fun checkToken()
    fun logout()
    fun getProfileImage()
}

interface MlbHomeActivityViewModelOutputs : BaseViewModelOutputs {
    fun tokenUpdated(): CompletableSubject
    fun fullName(): String?
    fun companyName(): String?
    fun userLoggedOut(): PublishSubject<Boolean>
    fun getProfileImageSuccess(): PublishSubject<GetProfileImageResponseModel>
    fun showProgress(): PublishSubject<Boolean>
    fun themeName(): String?
}

class MlbHomeActivityViewModel @Inject constructor(
        private val checkTokenUseCase: CheckTokenUseCase,
        private val getProfileImageUseCase: GetProfileImageUseCase,
        private val logoutUseCase: LogoutUseCase,
        private val sharedPrefs: SharedPreferences
) : BaseViewModel(), MlbHomeActivityViewModelInputs, MlbHomeActivityViewModelOutputs {
    private val getProfileImageSuccess = PublishSubject.create<GetProfileImageResponseModel>()
    private val showProgress = PublishSubject.create<Boolean>()

    override fun userLoggedOut() = userLoggedOut
    override fun getProfileImageSuccess() = getProfileImageSuccess

    override fun showProgress() = showProgress
    override fun themeName() = sharedPrefs.getString("theme", "")

    override fun logout() {
        showProgress.onNext(true)
        if(isCustomerJuridical) {
            logoutUseCase.execute(
                custId = custId,
                compId = compId,
                token = token).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)

                    if(it.status.statusCode == 1) {
                        sharedPrefs.edit().putString("userId","").apply()
                        sharedPrefs.edit().putBoolean("userLoggedIn", false).apply()
                        userLoggedOut.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            logoutUseCase.execute(
                custId = custId,
                compId = null,
                token = token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if(it.status.statusCode == 1) {
                        sharedPrefs.edit().putBoolean("userLoggedIn", false).apply()
                        sharedPrefs.edit().putString("userId","").apply()
                        userLoggedOut.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun getProfileImage() {
        var custCompId: Long? = null
        if (isCustomerJuridical) custCompId = compId

        getProfileImageUseCase.execute(custId, custCompId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    getProfileImageSuccess.onNext(it)
                }
            }, {
                it.printStackTrace()
            })
            .addTo(subscriptions)
    }

    override fun fullName() = sharedPrefs.getString("fullname", "")

    override fun companyName(): String? {
        return if(isCustomerJuridical) sharedPrefs.getString("companyName", "")
        else ""
    }

    override fun tokenUpdated() = tokenUpdated

    fun getSignLevel() = sharedPrefs.getInt("signLevel", 0)

    fun getSignCount() = sharedPrefs.getInt("signCount", 0)

    val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    override val inputs: MlbHomeActivityViewModelInputs = this
    override val outputs: MlbHomeActivityViewModel = this
    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    private val tokenUpdated = CompletableSubject.create()
    private val userLoggedOut = PublishSubject.create<Boolean>()

    override fun checkToken() {
        val token = sharedPrefs.getString("token", "")
        val custId = sharedPrefs.getLong("custId", 0L)
        val compId = sharedPrefs.getLong("compId", 0L)
        val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

        if (!isCustomerJuridical)
            checkTokenUseCase.execute(token, custId, null)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it.status.statusCode == 1) {
                            updateToken(it.customer.token)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    }).addTo(subscriptions)
        else
            checkTokenUseCase.execute(token, custId, compId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it.status.statusCode == 1) {
                            updateToken(it.customer.token)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    }).addTo(subscriptions)
    }

    private fun updateToken(token: String) {
        sharedPrefs.edit().putString("token", token).apply()
        tokenUpdated.onComplete()
    }

    fun getSharedPreference() = sharedPrefs.getString("userId","")

}