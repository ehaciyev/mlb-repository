package az.turanbank.mlb.presentation.ips.payrequest.sender

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.ips.payrequest.sender.account.ChooseIPSSenderAccountFragment
import az.turanbank.mlb.presentation.ips.payrequest.sender.alias.ChooseIPSSenderAliasFragment

private val TAB_TITLES = arrayOf(
    R.string.account_title,
    R.string.alias
)
private val TAB_FRAGMENTS = arrayOf(
    ChooseIPSSenderAccountFragment(),
    ChooseIPSSenderAliasFragment()
)
class IPSPaySenderPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return TAB_FRAGMENTS[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount() = 2
}