package az.turanbank.mlb.presentation.ips.aliases.myaliases

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.GetMLBAliasesResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.RespIpsModelAccount
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetIPSAliasesUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAliasesUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.data.SelectIPSMLBAccountModel
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface MyAliasesViewModelInputs : BaseViewModelInputs {
    fun getAliasses()
}

interface MyAliasesViewModelOutputs : BaseViewModelOutputs {
    fun onGetAliasesSuccess(): PublishSubject<ArrayList<MLBAliasResponseModel>>
}

class MyAliasesViewModel @Inject constructor(
    sharedPrefs: SharedPreferences,
    private val getMLBAliasesUseCase: GetMLBAliasesUseCase
) :
    BaseViewModel(), MyAliasesViewModelInputs, MyAliasesViewModelOutputs {

    override val inputs: MyAliasesViewModelInputs = this
    override val outputs: MyAliasesViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private var getAliasessSuccess = PublishSubject.create<ArrayList<MLBAliasResponseModel>>()
    override fun getAliasses() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getMLBAliasesUseCase
            .execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    getAliasessSuccess.onNext(it.aliases)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }
                , {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
    }

    override fun onGetAliasesSuccess() = getAliasessSuccess

}