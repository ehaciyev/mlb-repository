package az.turanbank.mlb.presentation.resources.loan.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.resources.loan.LoanStatementResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.resources.loan.pay.PayLoanActivity
import az.turanbank.mlb.presentation.resources.loan.payment.LoanPayedAmountsActivity
import az.turanbank.mlb.presentation.resources.loan.paymentplan.LoanPaymentPlanActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_loan.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import javax.inject.Inject

class LoanActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: LoanViewModel

    private var payForMonth = 0.00

    private var rest = 0.00
    private var currencyText = ""

    private var loanType = ""
    private var loanIban = ""
    private val loanId: Long by lazy { intent.getLongExtra("loanId", 0L) }
    private val symbols = DecimalFormatSymbols()
    private val decimalFormat = DecimalFormat("0.00")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan)

        toolbar_title.text = ""
        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = '.'
        decimalFormat.decimalFormatSymbols = symbols
        viewModel =
            ViewModelProvider(this, factory)[LoanViewModel::class.java]

        setOutputListeners()
        setInputListeners()

        changeToolbar()
    }

    private fun changeToolbar() {
        toolbar_back_button.setOnClickListener { onBackPressed() }

    }

    private fun setOutputListeners() {
        viewModel.outputs.onStatementSuccess().subscribe {
            payForMonth = it.payForMonth
            rest = it.rest
            currencyText = it.currency
            loanType = it.loanType
            loanIban = it.iban
            formUpViewFromData(it)
            stopAnimation()
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }

    @SuppressLint("SetTextI18n")
    private fun formUpViewFromData(loan: LoanStatementResponseModel) {
        toolbar_title.text = loan.loanType
        plasticCardAmountInteger.text = firstPartOfMoney(loan.rest)
        plasticCardAmountReminder.text = secondPartOfMoney(loan.rest) + loan.currency
        plasticContractNumber.text = loan.iban

        creditInteger.text = firstPartOfMoney(loan.loanAmount)
        creditReminder.text = secondPartOfMoney(loan.loanAmount) + loan.currency

        clientName.text = loan.fullName
        creditTypeTextView.text = loan.loanType

        amountInteger.text = firstPartOfMoney(loan.loanAmount)
        amountReminder.text = secondPartOfMoney(loan.loanAmount)

        currency.text = loan.currency
        loanPercent.text = "%" + loan.annualInterestRate
        receiptNo.text = loan.contractNumber
        branchName.text = loan.branch

        mainLoanInteger.text = firstPartOfMoney(loan.rest)
        mainLoanReminder.text = secondPartOfMoney(loan.rest)

        currentLoanInteger.text = firstPartOfMoney(loan.interestRate)
        currentLoanReminder.text = secondPartOfMoney(loan.interestRate)

        openDate.text = loan.openDate
        closeDate.text = loan.closeDate

        if(loan.loanTypeId == 11){
            payImage.setColorFilter(ContextCompat.getColor(this, R.color.disabled_color))
            payText.setTextColor(ContextCompat.getColor(this, R.color.disabled_color))
        }
    }

    private fun firstPartOfMoney(amount: Double) =
        decimalFormat.format(amount).toString().split(".")[0] + ","

    private fun secondPartOfMoney(amount: Double) =
        decimalFormat.format(amount).toString().split(".")[1] + " "

    private fun stopAnimation() {
        progress.clearAnimation()
        progress.visibility = View.GONE
        constraintContainer.visibility = View.VISIBLE
        plasticCardInclude.visibility = View.VISIBLE
    }

    private fun setInputListeners() {
        animateProgressImage()
        constraintContainer.visibility = View.GONE
        plasticCardInclude.visibility = View.GONE
        viewModel.inputs.getTheLoan(loanId)
        paymentPlan.setOnClickListener {
            val intent = Intent(this, LoanPaymentPlanActivity::class.java)
            intent.putExtra("loanId", loanId)
            startActivity(intent)
        }
        payments.setOnClickListener {
            val intent = Intent(this, LoanPayedAmountsActivity::class.java)
            intent.putExtra("loanId", loanId)
            startActivity(intent)
        }
        pay.setOnClickListener {
            if (viewModel.loanTypeId != 11) {
                val intent = Intent(this, PayLoanActivity::class.java)
                intent.putExtra("loanId", loanId)
                intent.putExtra("payForMonth", payForMonth)
                intent.putExtra("rest", rest)
                intent.putExtra("currency", currencyText)
                intent.putExtra("loanType", loanType)
                intent.putExtra("loanIban", loanIban)
                startActivity(intent)
            }
        }
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
