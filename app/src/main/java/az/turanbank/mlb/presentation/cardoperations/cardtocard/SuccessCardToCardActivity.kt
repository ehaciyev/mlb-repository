package az.turanbank.mlb.presentation.cardoperations.cardtocard

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_success_card_to_card.*
import kotlinx.android.synthetic.main.add_to_templates_dialog_view.view.*
import javax.inject.Inject


class SuccessCardToCardActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: SuccessCardToCardViewModel

    private val receiptNumber: String by lazy { intent.getStringExtra("receiptNo") }

    private val requestorCardNumber: String by lazy { intent.getStringExtra("requestorCardNumber") }

    private val date: String by lazy { intent.getStringExtra("operationDate") }

    private val root: String by lazy { intent.getStringExtra("root") }

    private val destinationCard: String by lazy { intent.getStringExtra("dtCardNumber") }
    private val amountToShow: Double by lazy { intent.getDoubleExtra("amount", 0.00) }
    private val curr: String by lazy { intent.getStringExtra("currency") }

    private var fullAmount = ""
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_card_to_card)

        viewModel =
            ViewModelProvider(this, factory)[SuccessCardToCardViewModel::class.java]

        fullAmount = "$amountToShow $curr"
        bindDataToView()

        setInputListeners()
        setOutputListeners()

    }

    private fun setOutputListeners() {
        viewModel.outputs.saveTempSuccess().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            Toast.makeText(
                this,
                getString(R.string.template_successfully_created),
                Toast.LENGTH_LONG
            ).show()
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            cardView.visibility = View.VISIBLE
            Toast.makeText(
                this,
                getString(ErrorMessageMapper().getErrorMessage(it)),
                Toast.LENGTH_LONG
            ).show()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        close.setOnClickListener {
            setResult(Activity.RESULT_FIRST_USER)
            finish()
        }
        share.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, generateShareString())

            startActivity(Intent.createChooser(shareIntent, "Göndər..."))
        }

        addToTemplate.setOnClickListener {
            showSaveTempEditText()
        }
    }

    private fun bindDataToView() {

        when {
            viewModel.getLang() == "az" -> {
                paid_stamp.setImageResource(R.drawable.pechat_az)
            }
            viewModel.getLang() == "en" -> {
                paid_stamp.setImageResource(R.drawable.pechat_eng)
            }
            viewModel.getLang() == "ru" -> {
                paid_stamp.setImageResource(R.drawable.pechat_rus)
            }
        }

        receiptNo.text = receiptNumber
        operationDate.text = date
        dtCardNumber.text = requestorCardNumber
        amount.text = fullAmount
        note.text = intent.getStringExtra("note")
    }

    @SuppressLint("InflateParams")
    private fun showSaveTempEditText() {

        val alert = AlertDialog.Builder(this)
        alert.setTitle(getString(R.string.template_name))

        val view =
            LayoutInflater.from(this).inflate(R.layout.add_to_templates_dialog_view, null, false)
        alert.setView(view)

        alert.setCancelable(false)
        alert.setPositiveButton(
            getString(R.string.save_all_caps)
        ) { dialog, _ ->
            if (!TextUtils.isEmpty(view.tempNameEditText.text.toString())) {
                animateProgressImage()
                val cardOperationType: Int = if (root == "otherCard") {
                    2
                } else {
                    1
                }
                viewModel.inputs.saveTemp(
                    view.tempNameEditText.text.toString(),
                    requestorCardNumber = requestorCardNumber,
                    destinationCardNumber = destinationCard,
                    amount = amountToShow,
                    currency = curr,
                    cardOperationType = cardOperationType
                )
            }
        }

        alert.setNegativeButton(
            getString(R.string.cancel_all_caps)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }

    private fun generateShareString(): String {
        return getString(R.string.card_to_card_share_oper_type) + "\n" +
                getString(R.string.card_to_card_share_oper_number) + receiptNumber + "\n" +
                getString(R.string.card_to_card_share_creditors_card) + destinationCard + "\n" +
                getString(R.string.card_to_card_share_debitor_card) + requestorCardNumber + "\n" +
                getString(R.string.card_to_card_share_amount) + amount.text + "\n" +
                getString(R.string.card_to_card_share_curr) + curr + "\n"
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        cardView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
