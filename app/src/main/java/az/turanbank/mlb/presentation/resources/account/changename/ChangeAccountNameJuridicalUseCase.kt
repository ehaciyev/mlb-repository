package az.turanbank.mlb.presentation.resources.account.changename

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class ChangeAccountNameJuridicalUseCase @Inject constructor(
    private val mainRepository: MainRepository
) {
    fun execute(
        custId: Long,
        compId: Long?,
        token: String?,
        accountId: Long,
        accountName: String
    ) = mainRepository.changeAccountNameJuridical(
        custId,
        compId,
        token,
        accountId,
        accountName
    )
}
