package az.turanbank.mlb.presentation.resources.loan.pay.verify

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.CurrencySignMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.secondPartOfMoney
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_card_requisities.*
import kotlinx.android.synthetic.main.activity_verify_payment.*
import kotlinx.android.synthetic.main.activity_verify_payment_loan.*
import kotlinx.android.synthetic.main.activity_verify_payment_loan.bigAmountInteger
import kotlinx.android.synthetic.main.activity_verify_payment_loan.bigAmountReminder
import kotlinx.android.synthetic.main.activity_verify_payment_loan.progressImage
import kotlinx.android.synthetic.main.activity_verify_payment_loan.submitButton
import kotlinx.android.synthetic.main.activity_verify_payment_loan.submitText
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import javax.inject.Inject


class VerifyLoanPaymentActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: VerifyLoanPaymentViewModel
    private val decimalFormat = DecimalFormat("0.00")
    private val symbols = DecimalFormatSymbols()

    private val dtAccountId: Long by lazy { intent.getLongExtra("dtAccountId", 0L) }
    private val amount: Double by lazy { intent.getDoubleExtra("amount", 0.00) }
    private val payType: Int by lazy { intent.getIntExtra("payType", 0) }
    private val loanId: Long by lazy { intent.getLongExtra("loanId", 0L) }
    private val currency: String by lazy { intent.getStringExtra("currency") }
    private val loanType: String by lazy { intent.getStringExtra("loanType") }
    private val account: String by lazy { intent.getStringExtra("account") }
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_payment_loan)

        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = ','
        decimalFormat.decimalFormatSymbols = symbols

        viewModel =
            ViewModelProvider(this, factory)[VerifyLoanPaymentViewModel::class.java]

        toolbar_title.text = getString(R.string.credit_payment)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        setViewFromIntent()

        setOutputListeners()
        setInputListeners()
    }

    @SuppressLint("SetTextI18n")
    private fun setViewFromIntent() {

        val curr = CurrencySignMapper().getCurrencySign(currency)
        bigAmountInteger.text = firstPartOfMoney(amount)
        bigAmountReminder.text = secondPartOfMoney(amount)

        //bigViewAmount.text =curr+" "+ decimalFormat.format(amount)

        when(curr){
            "AZN"->{
                currencyImage.setBackgroundResource(R.drawable.ic_manat_white)
            }

            "USD"->{
                currencyImage.setBackgroundResource(R.drawable.ic_dollar)
            }

            "EUR"->{
                currencyImage.setBackgroundResource(R.drawable.ic_euro)
            }
        }
        amountInteger.text = decimalFormat.format(amount).toString().split(",")[0] + ","
        amountReminder.text = decimalFormat.format(amount).toString().split(",")[1] +" "+currency

        receiverName.text = loanType

        senderName.text = account
    }

    private fun setInputListeners() {
        submitButton.setOnClickListener {
            showProgressBar(true)
            viewModel.inputs.payLoan(payType, dtAccountId, loanId, amount)
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onPayLoanSuccess().subscribe {
            showProgressBar(false)
            val intent = Intent()
            intent.putExtra("loanResponse", it)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateSubmitImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateSubmitImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
