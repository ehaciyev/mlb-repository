package az.turanbank.mlb.presentation.resources.card.sms

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_delete_or_update_sms_notification.*
import javax.inject.Inject

class DeleteOrUpdateSmsNotificationActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: DeleteOrUpdateSmsNotificationViewModel

    private val number: String by lazy { intent.getStringExtra("number") }
    private val cardId: Long by lazy { intent.getLongExtra("cardId", 0L) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_or_update_sms_notification)
        viewModel =
            ViewModelProvider(this, factory)[DeleteOrUpdateSmsNotificationViewModel::class.java]
        numberView.text = number

        setInputListeners()
        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onNotificationDisabled().subscribe{
            progress.clearAnimation()
            progress.visibility = View.GONE
            infoHolder.visibility = View.VISIBLE
            if (it) {
                finish()
            }
        }.addTo(subscriptions)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        disableSms.setOnClickListener {
            showUserDialog()
        }
        editSms.setOnClickListener {
            val intent = Intent(this, AddSmsNumberActivity::class.java)
            intent.putExtra("edit", true)
            intent.putExtra("cardId", cardId)
            startActivityForResult(intent, 10)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 10){
            if(resultCode == Activity.RESULT_OK){
                numberView.text = data?.getStringExtra("number")
            }
        }
    }

    private fun showUserDialog() {
        android.app.AlertDialog.Builder(this)
            .setTitle(getString(R.string.attention))
            .setMessage(getString(R.string.sms_service_cancel_confirmation_message))
            .setPositiveButton(R.string.continue_login) { _, _ ->
                viewModel.inputs.disableSms(number, cardId)
                animateProgressImage()
            }
            .setNegativeButton(getString(R.string.cancel_all_caps)) { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(true)
            .show()
    }

    private fun animateProgressImage() {
        infoHolder.visibility = View.GONE
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
