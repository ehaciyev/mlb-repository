package az.turanbank.mlb.presentation.ips.pay.creditorchoice.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.GetAccountAndCustomerInfoResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.GetAllBanksResponseModel
import az.turanbank.mlb.data.remote.model.response.BudgetAccountModel
import az.turanbank.mlb.domain.user.usecase.ips.banks.GetAllBanksUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetAccountAndCustomerInfoUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetBudgetAccountByIbanIPSUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface IPSCreditorAccountChoiceViewModelInputs : BaseViewModelInputs {
    fun getBankList()
    fun getReceiverCustomerInfo(iban: String,swiftCode: String?, testswiftCode: String?)
    fun getBudgetAccountByIban(iban: String)
}

interface IPSCreditorAccountChoiceViewModelOutputs : BaseViewModelOutputs {
    fun onBankListSuccess(): BehaviorSubject<GetAllBanksResponseModel>
    fun onReceiverCustomerSuccess() : PublishSubject<GetAccountAndCustomerInfoResponseModel>
    fun getBudgetAccountByIbanSuccess(): PublishSubject<BudgetAccountModel>
}

class IPSCreditorAccountChoiceViewModel @Inject constructor(
    private val getAllBanksUseCase: GetAllBanksUseCase,
    private val getBudgetAccountByIbanUseCase: GetBudgetAccountByIbanIPSUseCase,
    private val getAccountAndCustomerInfoUseCase: GetAccountAndCustomerInfoUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    IPSCreditorAccountChoiceViewModelInputs,
    IPSCreditorAccountChoiceViewModelOutputs {
    override val inputs: IPSCreditorAccountChoiceViewModelInputs = this

    override val outputs: IPSCreditorAccountChoiceViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val bankList = BehaviorSubject.create<GetAllBanksResponseModel>()
    private var selectedBankId: Long? = null

    private var isGovernmentPayment: Boolean = false

    private var swiftCode: String? = null
    private var testswiftCode: String? = null

    private val getBudgetAccountByIban = PublishSubject.create<BudgetAccountModel>()

    private val getReceiverCustomerInfo = PublishSubject.create<GetAccountAndCustomerInfoResponseModel>()

    override fun getBudgetAccountByIbanSuccess() = getBudgetAccountByIban

    override fun getBudgetAccountByIban(iban: String) {
        getBudgetAccountByIbanUseCase.execute(custId, token, iban)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    getBudgetAccountByIban.onNext(it.budgetAccount)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getBankList() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getAllBanksUseCase
            .execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    bankList.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getReceiverCustomerInfo(iban: String,swiftCode: String?, testswiftCode: String?) {
        getAccountAndCustomerInfoUseCase
            .execute(iban,null,null,swiftCode,testswiftCode, custId,token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it.status.statusCode) {
                    1 -> {
                        getReceiverCustomerInfo.onNext(it)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            },{
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onBankListSuccess() = bankList
    override fun onReceiverCustomerSuccess() = getReceiverCustomerInfo

    fun setSelectedBankId(id: Long) {
        if(id == 210005L) {
            isGovernmentPayment = true
        }
        this.selectedBankId = id
    }

    fun setSelectedSwiftCode(swiftCode: String?){
        this.swiftCode = swiftCode
    }

    fun setTestSelectedSwiftCode(testswiftCode: String?){
        this.testswiftCode = testswiftCode
    }

    fun getSelectedSwiftCode() = swiftCode
    fun getSelectedTestSwiftCode() = testswiftCode


    fun setSelectedBankCode(bankCode: String?) {
        isGovernmentPayment = bankCode == "210005"
    }

    fun getSelectedBankId() = selectedBankId

    fun isGovernmentPayment() = isGovernmentPayment
}