package az.turanbank.mlb.presentation.ips.accounts.setdefault

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.AliasAccountCompoundModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetCompoundsUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetDefaultCompoundsUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAliasesUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ChooseAliasForDefaultViewModelInputs: BaseViewModelInputs{
    fun getCompounds()
}
interface ChooseAliasForDefaultViewModelOutputs: BaseViewModelOutputs{
    fun onCompounds(): PublishSubject<ArrayList<AliasAccountCompoundModel>>
}
class ChooseAliasForDefaultViewModel @Inject constructor(
    private val getMLBAliasesUseCase: GetMLBAliasesUseCase,
    private val getDefaultCompoundsUseCase: GetDefaultCompoundsUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
        ChooseAliasForDefaultViewModelInputs,
        ChooseAliasForDefaultViewModelOutputs
{

    override val inputs: ChooseAliasForDefaultViewModelInputs = this

    override val outputs: ChooseAliasForDefaultViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val aliases = PublishSubject.create<ArrayList<AliasAccountCompoundModel>>()

    override fun getCompounds() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getDefaultCompoundsUseCase.execute(custId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ aliases ->
                if (aliases.status.statusCode == 1) {
                    this.aliases.onNext(aliases.compounds)
                } else {
                    error.onNext(aliases.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun onCompounds() = aliases
}