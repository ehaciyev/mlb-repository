package az.turanbank.mlb.presentation.login.asan.juridical

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.register.asan.LoginWithAsanUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import javax.inject.Inject

interface JuridicalRegisterAsanPinViewModelInputs : BaseViewModelInputs {
    fun loginWithAsan(transactionId: Long,
                      certificate: String,
                      challenge: String)
    fun storeUserPhone(phoneNumber: String)
    fun storeUserId(userId: String)
}

interface JuridicalRegisterAsanPinViewModelOutputs : BaseViewModelOutputs {
    fun onLogiIndividualAsan(): CompletableSubject
}

class LoginAsanJuridicalPinViewModel @Inject constructor(
    private val loginWithAsanUseCase: LoginWithAsanUseCase,
    private val sharedPreferences: SharedPreferences
) :
    BaseViewModel(),
    JuridicalRegisterAsanPinViewModelInputs,
    JuridicalRegisterAsanPinViewModelOutputs {

    override val inputs: JuridicalRegisterAsanPinViewModelInputs = this
    override val outputs: JuridicalRegisterAsanPinViewModelOutputs = this

    private val loginIndividualAsan = CompletableSubject.create()

    override fun storeUserId(userId: String) {
        sharedPreferences.edit().putString("userId", userId).apply()
    }

    override fun storeUserPhone(phoneNumber: String) {
        sharedPreferences.edit().putString("phoneNumber", phoneNumber).apply()
    }

    override fun loginWithAsan(
        transactionId: Long,
        certificate: String,
        challenge: String
    ) {
        loginWithAsanUseCase.execute(transactionId, certificate, challenge)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    loginIndividualAsan.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun onLogiIndividualAsan() = loginIndividualAsan
}