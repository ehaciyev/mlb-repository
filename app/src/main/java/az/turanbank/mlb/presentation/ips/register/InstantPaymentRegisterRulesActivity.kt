package az.turanbank.mlb.presentation.ips.register

import android.content.SharedPreferences
import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.activity.AGREED
import az.turanbank.mlb.presentation.activity.DISAGREED
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_instant_payment_register_rules.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class InstantPaymentRegisterRulesActivity : BaseActivity() {

    @Inject
    lateinit var sharedPrefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instant_payment_register_rules)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        toolbar_title.text = resources.getString(R.string.register_with_mobile)

        agree.setOnClickListener {
            setResult(AGREED)
            sharedPrefs.edit().putBoolean("userAgreement", true).apply()
            finish()
        }

        disagree.setOnClickListener {
            setResult(DISAGREED)
            sharedPrefs.edit().putBoolean("userAgreement", false).apply()
            finish()
        }
    }
}
