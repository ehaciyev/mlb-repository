package az.turanbank.mlb.presentation.resources.account

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.AuthOperation
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_sign_file.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class SignFileActivity : BaseActivity() {

    lateinit var viewmodel: SignFileViewModel
    private val vCode: String by lazy { intent.getStringExtra("verificationCode") }
    private val tId: Long by lazy { intent.getLongExtra("transactionId", 0L) }
    private val successOperId: ArrayList<Long> by lazy { intent.getSerializableExtra("successOperId") as ArrayList<Long> }
    private val failOperId: ArrayList<Long> by lazy { intent.getSerializableExtra("failOperId") as ArrayList<Long> }


    @Inject
    lateinit var factory: ViewModelProviderFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_file)

        toolbar_title.text = getString(R.string.sign_with_asan)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        viewmodel = ViewModelProvider(this, factory)[SignFileViewModel::class.java]

        setOutputListeners()
        setInputListeners()

        verificationCode.setText(vCode)
        showProgressBar(true)
    }

    private fun setInputListeners() {
        val operationIds = intent.getSerializableExtra("operationIdsJson")
        viewmodel.inputs.signFile(tId, operationIds = operationIds as ArrayList<AuthOperation>,successOperId = successOperId,failOperId = failOperId)
    }

    private fun setOutputListeners() {
        viewmodel.outputs.showProgress().subscribe {
            showProgressBar(it)
        }.addTo(subscriptions)

        viewmodel.outputs.signFileSuccess().subscribe {

            val successfulOperCount = it.successStatus.size
            val failedOperCount = it.failStatus.size

            if (successfulOperCount == 0) {
                val intent = Intent()
                intent.putExtra("description", getString(R.string.docs_sign_fail))
                setResult(Activity.RESULT_CANCELED, intent)
                finish()
            } else if (successfulOperCount > 0 && failedOperCount == 0) {
                val intent = Intent()
                intent.putExtra("description", getString(R.string.docs_sign_success))
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else if(successfulOperCount > failedOperCount) {
                val intent = Intent()
                intent.putExtra("description",
                    failedOperCount.toString() + "/" + successfulOperCount.toString() + getString(R.string.docs_partially_signed))
                setResult(Activity.RESULT_FIRST_USER, intent)
                finish()
            }
        }.addTo(subscriptions)
    }

    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
