package az.turanbank.mlb.presentation.resources.card.requisit

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.card.GetCardRequisitesResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.card.requisite.GetCardRequisiteForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.requisite.GetCardRequisiteForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface CardRequisitesViewModelInputs : BaseViewModelInputs {
    fun getCardRequisites(cardId: Long)
}

interface CardRequisitesViewModelOutputs : BaseViewModelOutputs {
    fun onRequisiteSuccess(): PublishSubject<GetCardRequisitesResponseModel>
}

class CardRequisitesViewModel @Inject constructor(
    private val getCardRequisiteForIndividualCustomerUseCase: GetCardRequisiteForIndividualCustomerUseCase,
    private val getCardRequisiteForJuridicalCustomerUseCase: GetCardRequisiteForJuridicalCustomerUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
CardRequisitesViewModelInputs,
CardRequisitesViewModelOutputs{

    override val inputs: CardRequisitesViewModelInputs = this
    override val outputs: CardRequisitesViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val requisite = PublishSubject.create<GetCardRequisitesResponseModel>()
    override fun getCardRequisites(cardId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if(isCustomerJuridical){
            getCardRequisiteForJuridicalCustomerUseCase
                .execute(custId, cardId, compId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        requisite.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },{
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getCardRequisiteForIndividualCustomerUseCase
                .execute(custId, cardId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        requisite.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },{
                    error.onNext(1878)
                }).addTo(subscriptions)
       }
    }

    override fun onRequisiteSuccess() = requisite
}
