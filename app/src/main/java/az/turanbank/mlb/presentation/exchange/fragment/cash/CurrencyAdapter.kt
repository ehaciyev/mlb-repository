package az.turanbank.mlb.presentation.exchange.fragment.cash

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ExchangeFlagMapper
import az.turanbank.mlb.data.remote.model.exchange.CurrencyResponseModel
import kotlinx.android.synthetic.main.turan_currency_list_item.view.*

class CurrencyAdapter (private val currencyList: ArrayList<CurrencyResponseModel>) : RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CurrencyViewHolder = CurrencyViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.turan_currency_list_item,
            parent,
            false))

    override fun getItemCount() = currencyList.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencyList[position])
    }
    class CurrencyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(currency: CurrencyResponseModel) {
            with(currency) {
                itemView.currency_name.text = this.currency
                itemView.currency_buy.text = this.cashBuy.toString()
                itemView.currency_sell.text = this.cashSell.toString()
                itemView.currency_icon.setImageResource(ExchangeFlagMapper().resolveFlag(this.currency))
            }
        }
    }
}