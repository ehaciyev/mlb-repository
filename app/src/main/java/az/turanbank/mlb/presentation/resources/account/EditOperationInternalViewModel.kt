package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.response.DomesticOperationResponseModel
import az.turanbank.mlb.data.remote.model.response.GetCustInfoByIbanResponseModel
import az.turanbank.mlb.data.remote.model.response.GetOperationByIdResponseModel
import az.turanbank.mlb.domain.user.usecase.GetCustInfoByIbanUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.*
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface EditOperationInternalViewModelInputs : BaseViewModelInputs {
    fun getOperationById(operId: Long)
    fun getAccountList()
    fun getCustInfoByIban(iban: String)
    fun updateInternalOperation(
        operId: Long,
        operNameId: Long,
        dtAccountId: Long,
        crIban: String,
        amount: Double,
        purpose: String,
        taxNo: String?
    )
}

interface EditOperationInternalViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun operationDetails(): PublishSubject<GetOperationByIdResponseModel>
    fun onAccountSuccess(): BehaviorSubject<ArrayList<AccountListModel>>
    fun custInfo(): PublishSubject<GetCustInfoByIbanResponseModel>
    fun updateInternalOperationSuccess(): PublishSubject<DomesticOperationResponseModel>
    fun operationUpdated(): CompletableSubject
}

class EditOperationInternalViewModel @Inject constructor(
    private val getOperationByIdUseCase: GetOperationByIdUseCase,
    private val getIndividualAccountListUseCase: GetNoCardAccountListForIndividualCustomerUseCase,
    private val getJuridicalAccountListUseCase: GetNoCardAccountListForJuridicalCustomerUseCase,
    private val getCustInfoByIbanUseCase: GetCustInfoByIbanUseCase,
    private val updateOperationByIdUseCase: UpdateOperationByIdUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
    EditOperationInternalViewModelInputs,
    EditOperationInternalViewModelOutputs {

    override fun operationUpdated() = operationUpdated

    override val inputs: EditOperationInternalViewModelInputs = this
    override val outputs: EditOperationInternalViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val operationDetails = PublishSubject.create<GetOperationByIdResponseModel>()
    private val showProgress = PublishSubject.create<Boolean>()
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val accountList = BehaviorSubject.create<ArrayList<AccountListModel>>()
    private val custInfo = PublishSubject.create<GetCustInfoByIbanResponseModel>()
    private val createInternalOperationSuccess: PublishSubject<DomesticOperationResponseModel> =
        PublishSubject.create()
    private val operationUpdated = CompletableSubject.create()

    override fun updateInternalOperation(
        operId: Long,
        operNameId: Long,
        dtAccountId: Long,
        crIban: String,
        amount: Double,
        purpose: String,
        taxNo: String?
    ) {
        showProgress.onNext(true)

        var custCompId: Long? = null

        if(isCustomerJuridical) {
            custCompId = compId
        }

        updateOperationByIdUseCase.execute(
            custId = custId,
            compId = custCompId,
            token = token,
            operId = operId,
            operNameId = operNameId,
            dtAccountId = dtAccountId,
            crIban = crIban,
            amount = amount,
            purpose = purpose,
            crCustTaxid = taxNo
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    operationUpdated.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun updateInternalOperationSuccess() = createInternalOperationSuccess

    override fun operationDetails() = operationDetails

    override fun getOperationById(operId: Long) {
        showProgress.onNext(true)
        getOperationByIdUseCase.execute(operId, custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    operationDetails.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    override fun showProgress() = showProgress

    override fun getAccountList() {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getJuridicalAccountListUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                lang = EnumLangType.AZ
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getIndividualAccountListUseCase.execute(custId, token,EnumLangType.AZ)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onAccountSuccess() = accountList

    fun getDtAccountId(iban: String): Long {
        accountList.value?.let {
            for (i in 0 until it.size) {
                if (it[i].iban == iban)
                    return it[i].accountId
            }
        }
        return 0L
    }

    override fun getCustInfoByIban(iban: String) {
        getCustInfoByIbanUseCase.execute(custId, token, iban)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    custInfo.onNext(it)
                } else {
                    custInfo.onNext(it)
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun custInfo() = custInfo

}