package az.turanbank.mlb.presentation.cardoperations.othercard

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.domain.user.usecase.cardoperations.DeleteCardOperationTempUseCase
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashlessUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetCardListForJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetIndividualCardListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

interface CardToToherCardViewModelInputs : BaseViewModelInputs {
    fun getCardList()
    fun deleteTemp(tempId: Long)
}

interface CardToToherCardViewModelOutputs : BaseViewModelOutputs {
    fun cardListSuccess(): PublishSubject<ArrayList<CardListModel>>
    fun currencyListSuccess(): PublishSubject<ArrayList<String>>
    fun currencyAndCardSuccess(): PublishSubject<Boolean>
    fun templateDeleted(): CompletableSubject
}

class CardToOtherCardViewModel @Inject constructor(
    private val getIndividualCardListUseCase: GetIndividualCardListUseCase,
    private val getCardListForJuridicalUseCase: GetCardListForJuridicalUseCase,
    private val getCashlessUseCase: ExchangeCashlessUseCase,
    private val deleteCardOperationTempUseCase: DeleteCardOperationTempUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    CardToToherCardViewModelInputs, CardToToherCardViewModelOutputs {
    override val inputs: CardToToherCardViewModelInputs = this
    override val outputs: CardToToherCardViewModelOutputs = this


    private val cardList = PublishSubject.create<ArrayList<CardListModel>>()
    private val currencyList = PublishSubject.create<ArrayList<String>>()

    private val currencyAndCard = PublishSubject.create<Boolean>()

    val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val delete = CompletableSubject.create()

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ
    override fun getCardList() {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if (isCustomerJuridical) {
            getCardListForJuridicalUseCase.execute(custId, compId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        cardList.onNext(it.cardList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        } else {
            getIndividualCardListUseCase.execute(custId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        cardList.onNext(it.cardList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                }, {
                    getCurrencyList()
                })
                .addTo(subscriptions)
        }
    }

    private fun getCurrencyList() {
        val list = arrayListOf("AZN")
        getCashlessUseCase.execute("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    for (i in 0 until it.exchangeCashlessList.size - 1) {
                        list.add(it.exchangeCashlessList[i].currency)
                    }
                }
            }, {

            }, {
                currencyList.onNext(list)
                currencyAndCard.onNext(true)
            }).addTo(subscriptions)
    }

    override fun deleteTemp(tempId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        deleteCardOperationTempUseCase
            .execute(tempId, custId, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    delete.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {

            }).addTo(subscriptions)
    }

    override fun templateDeleted() = delete

    override fun cardListSuccess() = cardList

    override fun currencyListSuccess() = currencyList

    override fun currencyAndCardSuccess() = currencyAndCard
}