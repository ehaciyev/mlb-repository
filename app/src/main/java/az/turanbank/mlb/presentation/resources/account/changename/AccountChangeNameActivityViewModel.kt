package az.turanbank.mlb.presentation.resources.account.changename

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.ServerResponseModel
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountChangeNameActivityInputViewModel: BaseViewModelInputs{
    fun changeName(accountId: Long, accountName: String)
}

interface AccountChangeNameActivityOutputViewModel: BaseViewModelOutputs{

    fun onChangeNameSuccess(): PublishSubject<Boolean>
}

class AccountChangeNameActivityViewModel @Inject constructor(sharedPrefs: SharedPreferences,
                                                             private val changeAccountNameIndividualUseCase: ChangeAccountNameIndividualUseCase,
                                                             private val changeAccountNameJuridicalUseCase: ChangeAccountNameJuridicalUseCase):
    BaseViewModel(), AccountChangeNameActivityInputViewModel, AccountChangeNameActivityOutputViewModel {

    override val inputs: AccountChangeNameActivityInputViewModel = this
    override val outputs: AccountChangeNameActivityOutputViewModel = this
    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val changeNameSuccess = PublishSubject.create<Boolean>()


    override fun changeName(accountId: Long, accountName: String) {
        if(isCustomerJuridical) {
            changeAccountNameJuridicalUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                accountId = accountId,
                accountName = accountName
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        changeNameSuccess.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            changeAccountNameIndividualUseCase.execute(
                custId = custId,
                token = token,
                accountId = accountId,
                accountName = accountName
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        changeNameSuccess.onNext(true)
                    } else {
                        changeNameSuccess.onNext(false)
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onChangeNameSuccess() = changeNameSuccess

}
