package az.turanbank.mlb.presentation.cardoperations.templates

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_template.*
import java.math.RoundingMode
import java.text.DecimalFormat
import javax.inject.Inject

class CardTemplateActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardTemplateActivityViewModel

    private var fromCardList = arrayListOf<String>()

    private var fromCardModel = arrayListOf<CardListModel>()

    private var currencies = arrayListOf<String>()

    private val decimalFormat = DecimalFormat("#.##")
    private val tempId: Long by lazy { intent.getLongExtra("tempId", 0L) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_template)

        viewModel =
            ViewModelProvider(this, factory)[CardTemplateActivityViewModel::class.java]
        toolbar_title.text = ""
        toolbar_back_button.setOnClickListener { onBackPressed() }
        decimalFormat.roundingMode = RoundingMode.CEILING
        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        viewModel.inputs.getTheTemplate(tempId)

        submitButton.setOnClickListener {
            showProgressBar(true)
            startActivity(viewModel.getIntentWithData(this, amount.text.toString().toDouble()))
            finish()
        }
    }
    private fun showDeleteTemp(){

        val alert = AlertDialog.Builder(this)
        alert.setMessage("Şablon silinsin?")

        alert.setCancelable(true)
        alert.setPositiveButton("BƏLİ"
        ) { dialog, _ ->
            animateProgressImage()
            viewModel.inputs.deleteTemp(tempId)
        }

        alert.setNegativeButton("XEYR"
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }
    private fun setOutputListeners() {
        viewModel.outputs.onTemplateSuccess().subscribe {
            toolbar_title.text = it.tempName

            fromCardTextView.text = it.destinationCardNumber

            for (i in 0 until fromCardModel.size){
                if(fromCardModel[i].cardNumber == it.destinationCardNumber){
                    toCardOrMobileSpinner.setSelection(i)
                    break
                }
            }
            amount.setText(it.amount.toString())

            for (i in 0 until currencies.size){
                if(currencies[i] == it.currency){
                    currencySpinner.setSelection(i)
                    break
                }
            }
        }.addTo(subscriptions)

        viewModel.outputs.cardListSuccess().subscribe {
            addItemsToSpinner(it)
        }.addTo(subscriptions)

        viewModel.outputs.currencyListSuccess().subscribe {
            currencies.clear()
            currencies.addAll(it)
            val currencyAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, currencies)
            currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            currencySpinner.adapter = currencyAdapter
        }.addTo(subscriptions)

        viewModel.outputs.currencyAndCardSuccess().subscribe {
            if (it){
                stopAnimation()
                deleteTemp.setOnClickListener {
                    showDeleteTemp()
                }
            } else {
                animateProgressImage()
            }
        }.addTo(subscriptions)

        viewModel.outputs.templateDeleted().subscribe{
            Toast.makeText(this, "Şablon uğurla silindi!", Toast.LENGTH_LONG).show()
            finish()
        }.addTo(subscriptions)
    }

    private fun stopAnimation() {
        mainView.visibility = View.VISIBLE
        progress.clearAnimation()
        progress.visibility = View.GONE
    }

    private fun addItemsToSpinner(cardList: ArrayList<CardListModel>){
        val latestList = excludeDestinationCardNumberFromList(cardList)

        fromCardModel.clear()
        fromCardModel.addAll(latestList)

        fromCardList.clear()

        changeListToDropDownItem(latestList)

        attachListToDropDown()
    }

    private fun changeListToDropDownItem(latestList: java.util.ArrayList<CardListModel>) {
        for (i in 0 until latestList.size) {
            fromCardList.add(
                latestList[i].cardNumber.replaceRange(4, 12, "********") + "\n" + "" +
                        decimalFormat.format(latestList[i].balance).toString() + " " + latestList[i].currency
            )
        }
    }

    private fun attachListToDropDown() {
        val adapter = ArrayAdapter(this, R.layout.spinner_item, fromCardList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        toCardOrMobileSpinner.adapter = adapter
    }

    private fun excludeDestinationCardNumberFromList(cardList: ArrayList<CardListModel>): ArrayList<CardListModel>{
        for (i in 0 until cardList.size){
            if( cardList[i].cardNumber == viewModel.destinationCardNumber){
                cardList.removeAt(i)
                break
            }
        }
        return cardList
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        mainView.visibility = View.GONE

        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        toCardOrMobileSpinner.isEnabled = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.info_uploading)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateSubmitImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateSubmitImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
