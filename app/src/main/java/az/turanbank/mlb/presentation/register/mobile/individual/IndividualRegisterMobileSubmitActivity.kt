package az.turanbank.mlb.presentation.register.mobile.individual

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.activity.SuccessfulPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_individual_register_asan_pin.progressImage
import kotlinx.android.synthetic.main.activity_individual_register_asan_pin.submitButton
import kotlinx.android.synthetic.main.activity_individual_register_asan_pin.submitText
import kotlinx.android.synthetic.main.activity_individual_register_mobile_submit.password
import kotlinx.android.synthetic.main.activity_individual_register_mobile_submit.repeatPassword
import kotlinx.android.synthetic.main.activity_individual_register_mobile_submit.username
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class IndividualRegisterMobileSubmitActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory

    private lateinit var viewmodel: RegisterIndividualViewModel

    private val custId by lazy { intent.getLongExtra("custId", 0L) }
    private val registrationType by lazy { intent.getStringExtra("registrationType") }
    private val customerType by lazy { intent.getStringExtra("customerType") }
    private val pin by lazy { intent.getStringExtra("pin") }
    private val compId by lazy { intent.getLongExtra("compId", 0L) }
    private val taxNo by lazy { intent.getStringExtra("taxNo") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual_register_mobile_submit)

        viewmodel =
            ViewModelProvider(this, viewModelFactory)[RegisterIndividualViewModel::class.java]
        if(registrationType == "mobile") {
            if (customerType == "juridical") {
                toolbar_title.text = getString(R.string.register_with_mobile_and_voen)
            } else {
                toolbar_title.text = getString(R.string.register_with_mobile)
            }
        } else if (registrationType == "asan" || registrationType == "asanJuridical") {
            toolbar_title.text = getString(R.string.register_with_asan)
        }
        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        toolbar_back_button.setOnClickListener { onBackPressed() }
        submitButton.setOnClickListener {

            if(checkValidation()){
                if (registrationType != "asanJuridical") {
                    if (customerType == "juridical") {
                        if(password.text.toString().trim() == repeatPassword.text.toString().trim()){
                            viewmodel.inputs.registerJuridicalCustomer(
                                pin,
                                taxNo,
                                username.text.toString().replace("\\s".toRegex(), ""),
                                password.text.toString().trim(),
                                repeatPassword.text.toString().trim()
                            )
                        } else {
                            repeatPassword.error = getString(R.string.repeat_wrong)
                        }

                    } else {
                        if(password.text.toString().trim() == repeatPassword.text.toString().trim()){
                            viewmodel.inputs.submitForms(
                                username.text.toString().trim(),
                                password.text.toString().trim(),
                                repeatPassword.text.toString().trim(),
                                custId,
                                registrationType
                            )
                        } else {
                            repeatPassword.error = getString(R.string.repeat_wrong)
                        }
                    }
                } else {
                    viewmodel.inputs.registerJuridicalAsan(
                        pin,
                        taxNo,
                        custId,
                        compId,
                        username.text.toString(),
                        password.text.toString(),
                        repeatPassword.text.toString()
                    )
                }
            }
        }
    }

    private fun checkValidation(): Boolean {
        var isValid = true
        if(username.text.toString().isNullOrEmpty()){
            AlertDialogMapper(this, 1997).showAlertDialog()
            isValid = false
        }
        if(password.text.toString().isNullOrEmpty()){
            AlertDialogMapper(this, 1998).showAlertDialog()
            isValid = false
        }
        if(repeatPassword.text.toString().isNullOrEmpty()){
            AlertDialogMapper(this, 1999).showAlertDialog()
            isValid = false
        }
        return isValid
    }

    private fun setOutputListeners() {
        viewmodel.outputs.showProgress().subscribe {
            showProgressBar(it)
        }.addTo(subscriptions)

        viewmodel.outputs.onError()
            .subscribe {
                showProgressBar(false)
                AlertDialogMapper(this, it).showAlertDialog()
            }.addTo(subscriptions)

        viewmodel.outputs.registrationOnSuccess().subscribe {
            if(it){
                val intent = Intent(this, SuccessfulPageViewActivity::class.java)
                intent.putExtra("finishOnly", false)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this, ErrorPageViewActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }
        }.addTo(subscriptions)
    }

    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        username.isClickable = !show
        password.isClickable = !show
        repeatPassword.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
