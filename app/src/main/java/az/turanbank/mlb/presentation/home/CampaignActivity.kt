package az.turanbank.mlb.presentation.home

import android.os.Bundle
import android.util.Base64
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseActivity
import com.bumptech.glide.Glide
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_campaign.*
import javax.inject.Inject

class CampaignActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CampaignViewModel
    private val campaignId: Long by lazy { intent.getLongExtra("campaignId", 0L) }

    lateinit var dialog: MlbProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_campaign)
        dialog = MlbProgressDialog(this)
        viewModel =
            ViewModelProvider(this, factory)[CampaignViewModel::class.java]

        closeButton.setOnClickListener{
            finish()
        }

        setOutputListeners()
        setInputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.campaignsSuccess().subscribe { campaignList ->

            var campaignPosition = 0

            for (i in 0 until campaignList.size) {
                if (campaignId == campaignList[i].campaignId) {
                    val byteArray = Base64.decode(campaignList[i].image, Base64.DEFAULT)
                    Glide
                        .with(this)
                        .load(byteArray)
                        .into(campaignImage)

                    campaignPosition = i
                    break
                }
            }

            leftPortion.setOnClickListener {
                if (campaignPosition - 1 >= 0) {
                    campaignPosition -= 1

                    val byteArray =
                        Base64.decode(campaignList[campaignPosition].image, Base64.DEFAULT)
                    Glide
                        .with(this)
                        .load(byteArray)
                        .into(campaignImage)

                }
            }

            rightPortion.setOnClickListener {
                if (campaignPosition + 1 <= campaignList.size - 1) {

                    campaignPosition += 1

                    val byteArray =
                        Base64.decode(campaignList[campaignPosition].image, Base64.DEFAULT)
                    Glide
                        .with(this)
                        .load(byteArray)
                        .into(campaignImage)
                }
            }
        }.addTo(subscriptions)

        viewModel.outputs.onCampaigSuccess().subscribe {
            val byteArray = Base64.decode(it.image, Base64.DEFAULT)
            Glide
                .with(this)
                .load(byteArray)
                .centerCrop()
                .into(campaignImage)
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getCampaigns()
        //    viewModel.inputs.getTheCampaign(intent.getLongExtra("campaignId", 0L))
    }
}
