package az.turanbank.mlb.presentation.ips.accounts.myaccounts

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.response.IPSMLBAccountResponseModel
import kotlinx.android.synthetic.main.account_item.view.*
import kotlinx.android.synthetic.main.activity_ipsaliases.view.*
import kotlinx.android.synthetic.main.activity_my_linked_accounts.*

class MyLinkedAccountsAdapter(context: Context, private val accounts: ArrayList<IPSMLBAccountResponseModel>,
private val onClickListener: (position:Int) -> Unit)
    : RecyclerView.Adapter<MyLinkedAccountsAdapter.MyLinkedAccountsViewHolder>() {

    class MyLinkedAccountsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(account: IPSMLBAccountResponseModel, onClickListener: (position: Int) -> Unit) {
            itemView.setOnClickListener { onClickListener.invoke(adapterPosition) }

            itemView.accountName.text = account.iban
            itemView.aliasType.text = account.bic
            itemView.aliastoaccount.text = account.authMethod

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyLinkedAccountsViewHolder {
        return MyLinkedAccountsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.account_item,parent,false))
    }

    override fun getItemCount() = accounts.size

    override fun onBindViewHolder(holder: MyLinkedAccountsViewHolder, position: Int) {
        holder.bind(accounts[position],onClickListener)
    }
}