package az.turanbank.mlb.presentation.payments


import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.payments.history.PaymentHistoryFragment
import az.turanbank.mlb.presentation.payments.pay.categories.PaymentCategoriesFragment
import az.turanbank.mlb.presentation.payments.template.PaymentTemplatesFragment
import az.turanbank.mlb.presentation.resources.account.AccountTransfersPagerAdapter
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class PaymentsFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: PaymentsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_payments, container, false)

        viewModel =
            ViewModelProvider(this, factory)[PaymentsViewModel::class.java]

        val viewPager : ViewPager = view.findViewById(R.id.view_pager)

        val tabs: TabLayout = view.findViewById(R.id.tabs)

        val adapter = AccountTransfersPagerAdapter(
            arrayListOf(
                PaymentCategoriesFragment(),
                PaymentTemplatesFragment(),
                PaymentHistoryFragment()
            ),
            childFragmentManager,
            requireContext()
        )

        viewPager.adapter = adapter

        viewPager.offscreenPageLimit = 3

        tabs.setupWithViewPager(viewPager)

        val tab = if (arguments != null && arguments!!.getBoolean("fromTemplate", false)) {
            tabs.getTabAt(1)
        } else {
            tabs.getTabAt(0)
        }
        tabs.selectTab(tab)

        setOutputListener()
        setInputListener()


        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    private fun setOutputListener() {
        viewModel.outputs.getProfileImageSuccess().subscribe {
            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)
            val navImage = requireActivity().findViewById<ImageView>(R.id.imageView)
            Glide
                .with(this)
                .load(byteArray)
                .centerCrop()
                .into(navImage)
        }.addTo(subscriptions)
    }

    private fun setInputListener() {
        viewModel.inputs.getProfileImage()
    }
    
}
