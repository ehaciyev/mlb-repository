package az.turanbank.mlb.presentation.ips.pay.debitorchoice

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_ipstransfer.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class IPSTransferActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ipstransfer)

        toolbar_title.text = getString(R.string.pay)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        view_pager.adapter =
            IPSTransferPagerAdapter(
                this,
                supportFragmentManager
            )
        tabs.setupWithViewPager(view_pager)
    }

}