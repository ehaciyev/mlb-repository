package az.turanbank.mlb.presentation.activity.settings

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_about_app.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class AboutAppActivity : BaseActivity() {

    lateinit var viewmodel: AboutViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_app)

        viewmodel = ViewModelProvider(this, factory) [AboutViewModel::class.java]

        toolbar_title.text = getString(R.string.about_the_app)

        toolbar_back_button.setOnClickListener { onBackPressed() }

        val manager = this.packageManager
        val info = manager.getPackageInfo(this.packageName, PackageManager.GET_ACTIVITIES)
        val appVersionString = getString(R.string.app_version, info.versionName)
        appVersion.text = appVersionString
    }
}
