package az.turanbank.mlb.presentation.ips.payrequest.sender.account


import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.widget.AppCompatEditText
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.BankListAdapter
import az.turanbank.mlb.presentation.ips.pay.preconfirm.TransferWithIPSActivity
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class ChooseIPSSenderAccountFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ChooseIPSSenderAccountViewModel
    private lateinit var bankContainer: LinearLayout
    private lateinit var progressImage: ImageView
    private lateinit var submitText: TextView

    lateinit var submitButton: LinearLayout

    lateinit var bank: AutoCompleteTextView

    lateinit var account: AppCompatEditText
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_ipscreditor_account_choice, container, false)

        viewModel = ViewModelProvider(this, factory)[ChooseIPSSenderAccountViewModel::class.java]

        initViews(view)

        bank.hint = getString(R.string.versus_bank)
        account.hint = getString(R.string.sender_iban)

        account.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(28))

        setOutputListeners()
        setInputListeners()
        return view
    }

    private fun setInputListeners() {
        viewModel.inputs.getBankList()
        submitButton.setOnClickListener {
            if (account.text.toString().trim().length < 28) {
                AlertDialogMapper(requireActivity(), 124008).showAlertDialog()
            } else{
                var intent = Intent(requireActivity(), TransferWithIPSActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onBankListSuccess().subscribe { banks ->
            val list = arrayOfNulls<String>(banks.size)

            for (i in 0 until banks.size) {
                list[i] = banks[i]
            }
            val adapter =
                BankListAdapter(requireContext(), banks)
            bank.threshold = 0
            adapter.notifyDataSetChanged()
            bank.setAdapter(adapter)
        }.addTo(subscriptions)
    }

    private fun initViews(view: View) {
        bankContainer = view.findViewById(R.id.bankContainer)

        progressImage = view.findViewById(R.id.progressImage)

        submitText = view.findViewById(R.id.submitText)

        submitButton = view.findViewById(R.id.submitButton)

        bank = view.findViewById(R.id.bank)

        account = view.findViewById(R.id.account)
    }

}
