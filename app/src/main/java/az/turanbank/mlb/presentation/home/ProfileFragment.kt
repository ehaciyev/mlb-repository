package az.turanbank.mlb.presentation.home

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.SplashActivity
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.CircleImageView
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.login.activities.pin.LoginPinActivity
import az.turanbank.mlb.presentation.profile.LanguageChangeActivity
import az.turanbank.mlb.presentation.profile.PasswordChangeActivity
import az.turanbank.mlb.presentation.view.ColorChooserDialog
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import com.asksira.bsimagepicker.BSImagePicker
import com.asksira.bsimagepicker.Utils
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import io.reactivex.rxkotlin.addTo
import java.io.ByteArrayOutputStream
import java.io.File
import javax.inject.Inject

class ProfileFragment : BaseFragment(), BSImagePicker.OnSingleImageSelectedListener,
    BSImagePicker.ImageLoaderDelegate {
    @Inject
    lateinit var factory: ViewModelProviderFactory
    lateinit var viewModel: ProfileFragmentViewModel
    private lateinit var profilePhoto: CircleImageView
    lateinit var themeColor: CircleImageView
    lateinit var dialog: MlbProgressDialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        val changeUserPassword = view.findViewById<LinearLayout>(R.id.changeUserPassword)
        val changeUserPin = view.findViewById<LinearLayout>(R.id.changeUserPin)
        val changeUserLang = view.findViewById<LinearLayout>(R.id.changeUserLang)
        val changeTheme = view.findViewById<LinearLayout>(R.id.changeThemeUser)
        themeColor = view.findViewById<CircleImageView>(R.id.themeColor)

        profilePhoto = view.findViewById(R.id.profilePhoto)

        changeUserPassword.setOnClickListener {
            startActivity(Intent(requireActivity(), PasswordChangeActivity::class.java))
        }
        changeUserPin.setOnClickListener {
            startActivity(Intent(requireActivity(), LoginPinActivity::class.java).apply {
                putExtra("updatingCurrentPin", true)
            })
        }

        changeUserLang.setOnClickListener {
            val intent = Intent(requireActivity(), LanguageChangeActivity::class.java)
            intent.putExtra("isSignIn", true)
            startActivity(intent)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            changeTheme.setOnClickListener{
                val dialog = ColorChooserDialog(context)
                dialog.setTitle(R.string.app_name)
                dialog.setColorListener { v, color ->
                    //do whatever you want to with the values
                    if(color == -16680494){
                        viewModel.updateTheme("blue")
                        themeColor.setImageResource(R.color.colorPrimary)
                        val intent = Intent(requireActivity(), SplashActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        requireActivity().finish()
                    } else if(color == -1315861){
                        viewModel.updateTheme("white")
                        themeColor.setImageResource(R.color.white)
                        val intent = Intent(requireActivity(), SplashActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        requireActivity().finish()
                    }

                }
                //customize the dialog however you want
                //customize the dialog however you want
                dialog.show()
            }
        } else {changeTheme.visibility = View.GONE}


        viewModel = ViewModelProvider(this, factory)[ProfileFragmentViewModel::class.java]
        dialog =
            MlbProgressDialog(requireActivity())
        val username = view.findViewById<AppCompatTextView>(R.id.username)
        val editProfilePhoto = view.findViewById<LinearLayout>(R.id.editProfilePhoto)
        setOutputListeners()
        setInputListeners()
        editProfilePhoto.setOnClickListener {
            BSImagePicker.Builder("az.turanbank.mlb.fp")
                .setSpanCount(5) //Default: 3. This is the number of columns
                .setGridSpacing(Utils.dp2px(2)) //Default: 2dp. Remember to pass in a value in pixel.
                .setPeekHeight(Utils.dp2px(360)) //Default: 360dp. This is the initial height of the dialog.
                .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                .setTag("A request ID") //Default: null. Set this if you need to identify which picker is calling back your fragment / activity.
                .build()
                .show(childFragmentManager, "picker")
            /*ImagePicker
                .create(this)
                .returnMode(ReturnMode.ALL)
                .folderMode(true)
                .toolbarFolderTitle("Folder")
                .toolbarImageTitle("Tap to select")
                .toolbarArrowColor(Color.WHITE)
                .single()
                .limit(1)
                .showCamera(false)
                .imageDirectory("Camera")
                .enableLog(false)
                .start()*/
        }
        username.text = viewModel.fullName()

        return view
    }

    private fun setInputListeners() {
        viewModel.inputs.getProfileImage()
    }

    private fun setOutputListeners() {
        if(viewModel.themeName() == "blue"){
            themeColor.setImageResource(R.color.colorPrimary)
        } else {
            themeColor.setImageResource(R.color.white)
        }
        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(requireActivity(), it).showAlertDialog()
        }.addTo(subscriptions)
        viewModel.outputs.getProfileImageSuccess().subscribe {
            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)
            val navImage = requireActivity().findViewById<ImageView>(R.id.imageView)
            Glide
                .with(this)
                .load(byteArray)
                .centerCrop()
                .into(profilePhoto)
            Glide
                .with(this)
                .load(byteArray)
                .centerCrop()
                .into(navImage)
        }.addTo(subscriptions)
        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.updateProfileImageSuccess().subscribe {
            viewModel.inputs.getProfileImage()
        }.addTo(subscriptions)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val image = ImagePicker.getFirstImageOrNull(data)
            val imgFile = File(image.path)
            if (imgFile.exists()) {
                val myBitmap = BitmapFactory.decodeFile(imgFile.absolutePath)
                val stream = ByteArrayOutputStream()
                myBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val byteArray: ByteArray = stream.toByteArray()
                myBitmap.recycle()
                val bytesStr = Base64.encodeToString(byteArray, Base64.DEFAULT)
                Glide
                    .with(this)
                    .load(byteArray)
                    .centerCrop()
                    .into(profilePhoto)
                viewModel.inputs.updateProfileImage(bytesStr)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSingleImageSelected(uri: Uri?, tag: String?) {
        val imgFile = File(uri?.path!!)
        if (imgFile.exists()) {
          //  val myBitmap = flipImage(BitmapFactory.decodeFile(imgFile.absolutePath))
            val myBitmap = decodeFile(imgFile.absolutePath)
            val stream = ByteArrayOutputStream()
            myBitmap?.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val byteArray: ByteArray = stream.toByteArray()
            myBitmap?.recycle()
            val bytesStr = Base64.encodeToString(byteArray, Base64.DEFAULT)
            /* Glide
                 .with(this)
                 .load(byteArray)
                 .centerCrop()
                 .into(profilePhoto)
 */
            viewModel.inputs.updateProfileImage(bytesStr)
        }
    }


    private fun decodeFile(path: String?): Bitmap? { // this method is for avoiding the image rotation
        val orientation: Int
        try {
            if (path == null) {
                return null
            }
            // decode image size
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            // Find the correct scale value. It should be the power of 2.

            val requiredSize = 70
            var widthTmp = o.outWidth
            var heightTmp = o.outHeight

            var scale = 4
            while (true) {
                if (widthTmp / 2 < requiredSize || heightTmp / 2 < requiredSize)
                    break
                widthTmp /= 2
                heightTmp /= 2
                scale++
            }
            // decode with inSampleSize
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale

            val bm = BitmapFactory.decodeFile(path, o2)

            var bitmap = bm
            val exif = ExifInterface(path)
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)
            Log.e("orientation", "" + orientation)
            val m = Matrix()
            when (orientation) {
                3 -> {
                    m.postRotate(180f)
                    m.postScale(bm.width.toFloat(), bm.height.toFloat())
        //               if(m.preRotate(90)){
                    Log.e("in orientation", "" + orientation)
                    bitmap = Bitmap.createBitmap(bm, 0, 0, bm.width, bm.height, m, true)
                    return bitmap
                }
                6 -> {
                    m.postRotate(90f)
                    Log.e("in orientation", "" + orientation)
                    bitmap = Bitmap.createBitmap(bm, 0, 0, bm.width, bm.height, m, true)
                    return bitmap
                }
                8 -> {
                    m.postRotate(270f)
                    Log.e("in orientation", "" + orientation)
                    bitmap = Bitmap.createBitmap(bm, 0, 0, bm.width, bm.height, m, true)
                    return bitmap
                }
                else -> return bitmap
            }
        } catch (e: Exception) {
        }
        return null
    }


    private fun fixOrientation(bitmap: Bitmap): Int {
        return if (bitmap.width > bitmap.height) {
            -90
        } else 0
    }

    private fun flipImage(bitmap: Bitmap): Bitmap {
        val matrix = Matrix()
        val rotation = fixOrientation(bitmap)
        matrix.postRotate(rotation.toFloat())
        matrix.preScale(-1f, -1f)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    override fun loadImage(imageFile: File?, ivImage: ImageView?) {
        Glide
            .with(this)
            .load(imageFile)
            .centerCrop()
            .into(ivImage!!)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }
}