package az.turanbank.mlb.presentation.ips.paidToMe.paidtomechoice

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetPaymentDocByOperIdResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.GetAsanDocByOperIdUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetPaymentDocByOperIdUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface SinglePaindToMeInputs: BaseViewModelInputs{
    fun getPaymentDocByOperId(operId: Long)
    fun getAsanDocByOperId(operId: Long)

}
interface SinglePaidToMeOutputs: BaseViewModelOutputs{
    fun paymentDoc(): PublishSubject<GetPaymentDocByOperIdResponseModel>
    fun paymentADoc(): PublishSubject<GetPaymentDocByOperIdResponseModel>
    fun showProgress(): PublishSubject<Boolean>
}
class SinglePaidToMeViewModel @Inject constructor(sharedPrefs: SharedPreferences,
                                                  private val getAsanDocByOperIdUseCase: GetAsanDocByOperIdUseCase,
                                                  private val paymentDocByOperIdUseCase: GetPaymentDocByOperIdUseCase
)
    :BaseViewModel(), SinglePaindToMeInputs, SinglePaidToMeOutputs{


    override val inputs: SinglePaindToMeInputs = this
    override val outputs: SinglePaidToMeOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val showProgress = PublishSubject.create<Boolean>()

    private val operationDeleted = CompletableSubject.create()

    override fun showProgress() = showProgress
    override fun paymentDoc() = paymentDoc

    private val paymentDoc = PublishSubject.create<GetPaymentDocByOperIdResponseModel>()
    private val paymentAsanDoc = PublishSubject.create<GetPaymentDocByOperIdResponseModel>()


    override fun getAsanDocByOperId(operId: Long) {
        showProgress.onNext(true)
        getAsanDocByOperIdUseCase.execute(custId, token, operId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    paymentAsanDoc.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun paymentADoc() = paymentAsanDoc

    override fun getPaymentDocByOperId(operId: Long) {
        showProgress.onNext(true)

        paymentDocByOperIdUseCase.execute(custId, token, operId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    paymentDoc.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }


}