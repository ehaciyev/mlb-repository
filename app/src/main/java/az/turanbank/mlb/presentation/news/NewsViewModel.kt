package az.turanbank.mlb.presentation.news

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.CampaignModel
import az.turanbank.mlb.domain.user.usecase.login.GetCampaignListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.ArrayList
import javax.inject.Inject

interface NewsViewModelInputs: BaseViewModelInputs{
    fun getCampaigns()
}
interface NewsViewModelOutputs: BaseViewModelOutputs{
    fun campaignsSuccess(): PublishSubject<ArrayList<CampaignModel>>
}
class NewsViewModel @Inject constructor(
    private val getCampaignListUseCase: GetCampaignListUseCase,
    sharedPreferences: SharedPreferences
) : BaseViewModel(),
        NewsViewModelInputs,
        NewsViewModelOutputs{
    override val inputs: NewsViewModelInputs = this

    override val outputs: NewsViewModelOutputs = this

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ


    private val campaigns = PublishSubject.create<ArrayList<CampaignModel>>()

    override fun getCampaigns() {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        getCampaignListUseCase.execute(enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    campaigns.onNext(it.respCampaignList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }


    override fun campaignsSuccess() = campaigns
}