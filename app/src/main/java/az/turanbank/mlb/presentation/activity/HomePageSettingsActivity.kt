package az.turanbank.mlb.presentation.activity

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.settings.AboutAppActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.profile.LanguageChangeActivity
import kotlinx.android.synthetic.main.activity_home_page_settings.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class HomePageSettingsActivity : BaseActivity() {

    lateinit var viewmodel: HomePageSettingsViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page_settings)

        viewmodel = ViewModelProvider(this, factory) [HomePageSettingsViewModel::class.java]

        setListeners()
    }

    private fun setListeners() {
        aboutApp.setOnClickListener { startActivity(Intent(this, AboutAppActivity::class.java) )}
        changeLangContainer.setOnClickListener{
            var intent = Intent(this,LanguageChangeActivity::class.java)
            intent.putExtra("isSignIn", false)
            startActivity(intent)
        }
        toolbar_back_button.setOnClickListener { onBackPressed() }
        toolbar_title.text = getString(R.string.settings)
    }
    private fun whenButtonClicked(azView: TextView, ruView: TextView, enView: TextView) {
        val selectedBackgroundDrawable = ContextCompat.getDrawable(baseContext, R.drawable.rounded_corners_primary)
        val selectedTextColor = ContextCompat.getColor(baseContext, R.color.white)

        val unSelectedBackgroundDrawable = ContextCompat.getDrawable(baseContext, R.drawable.rounded_corners)
        val unSelectedTextColor = ContextCompat.getColor(baseContext, R.color.login_text_color)
        azView.setOnClickListener {
            azView.setTextColor(selectedTextColor)
            azView.background = selectedBackgroundDrawable
            ruView.setTextColor(unSelectedTextColor)
            ruView.background = unSelectedBackgroundDrawable
            enView.setTextColor(unSelectedTextColor)
            enView.background = unSelectedBackgroundDrawable
        }
        ruView.setOnClickListener {
            azView.setTextColor(unSelectedTextColor)
            azView.background = unSelectedBackgroundDrawable
            ruView.setTextColor(selectedTextColor)
            ruView.background = selectedBackgroundDrawable
            enView.setTextColor(unSelectedTextColor)
            enView.background = unSelectedBackgroundDrawable
        }
        enView.setOnClickListener {
            azView.setTextColor(unSelectedTextColor)
            azView.background = unSelectedBackgroundDrawable
            ruView.setTextColor(unSelectedTextColor)
            ruView.background = unSelectedBackgroundDrawable
            enView.setTextColor(selectedTextColor)
            enView.background = selectedBackgroundDrawable
        }
    }
}
