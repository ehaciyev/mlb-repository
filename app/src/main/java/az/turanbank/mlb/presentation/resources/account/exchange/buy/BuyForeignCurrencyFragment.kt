package az.turanbank.mlb.presentation.resources.account.exchange.buy


import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.widget.AppCompatEditText
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.exchange.CashlessResponseModel
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.resources.account.AccountTemplateResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.resources.account.exchange.BuyForeignCurrencyViewModel
import az.turanbank.mlb.presentation.resources.account.exchange.success.BuyCurrencySuccessActivity
import az.turanbank.mlb.presentation.resources.account.exchange.verify.BuyCurrencyVerifyActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_buy_foreign_currency.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import javax.inject.Inject

class BuyForeignCurrencyFragment : BaseFragment() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: BuyForeignCurrencyViewModel

    private val foreignAccountList = arrayListOf<AccountListModel>()

    private val manatAccountList = arrayListOf<AccountListModel>()

    private val cashlessList = arrayListOf<CashlessResponseModel>()

    lateinit var dialog: MlbProgressDialog

    private lateinit var buyAmount: AppCompatEditText

    private lateinit var sellAmount: AppCompatEditText

    private lateinit var buyCurrency: AppCompatEditText

    private lateinit var sellCurrency: AppCompatEditText

    private lateinit var buyAccount: AppCompatEditText

    private lateinit var sellAccount: AppCompatEditText

    private lateinit var rateAmount: AppCompatEditText

    private lateinit var customizeExchangeRate: LinearLayout
    private lateinit var submitButton: LinearLayout

//    private val operId: Long by lazy { requireActivity().intent.getLongExtra("operId", 0L) }

    private val decimalFormat = DecimalFormat("0.00")
    private val symbols = DecimalFormatSymbols()
    private var buyFocus = false
    private var sellFocus = false
    private var rateFocus = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = if (arguments?.getInt("type") == 0) {
            inflater.inflate(R.layout.fragment_buy_foreign_currency, container, false)
        } else {
            inflater.inflate(R.layout.fragment_sell_foreign_currency, container, false)
        }
        symbols.decimalSeparator = '.'
        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        decimalFormat.decimalFormatSymbols = symbols

        dialog =
            MlbProgressDialog(requireActivity())
        dialog.showDialog()

        viewModel =
            ViewModelProvider(this, factory)[BuyForeignCurrencyViewModel::class.java]


        buyAmount = view.findViewById(R.id.buyAmount)
        sellAmount = view.findViewById(R.id.sellAmount)

        buyCurrency = view.findViewById(R.id.buyCurrency)
        sellCurrency = view.findViewById(R.id.sellCurrency)

        buyAccount = view.findViewById(R.id.buyAccount)
        sellAccount = view.findViewById(R.id.sellAccount)

        rateAmount = view.findViewById(R.id.customExchange)
        submitButton = view.findViewById(R.id.submitButton)
        customizeExchangeRate = view.findViewById(R.id.customizeExchangeRate)

        setOutputListeners()
        setInputListeners()

        setFocusAndWatcherListeners()

        checkTemplateOrNot()

        return view
    }

    private fun checkTemplateOrNot() {
        if (requireActivity().intent.getSerializableExtra("template") != null) {
            fillFormsWithTemplate(requireActivity().intent.getSerializableExtra("template") as AccountTemplateResponseModel)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun fillFormsWithTemplate(template: AccountTemplateResponseModel) {
        buyAccount.setText(template.dtIban + " / " + template.dtCcy)
        buyAmount.setText(template.dtAmt.toString())
        buyCurrency.setText(template.dtCcy)

        sellAccount.setText(template.crIban + " / " + template.crCcy)
        sellAmount.setText(template.crAmt.toString())
        sellCurrency.setText(template.crCcy)
    }

    private fun setFocusAndWatcherListeners() {

        buyAmount.setOnFocusChangeListener { _, hasFocus ->
            buyFocus = hasFocus
        }

        sellAmount.setOnFocusChangeListener { _, hasFocus ->
            sellFocus = hasFocus
        }
        rateAmount.setOnFocusChangeListener { _, hasFocus ->
            rateFocus = hasFocus
        }


        buyAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (buyFocus) {
                    makeExchange(s.toString(), 0)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        rateAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (rateFocus) {
                    changeRate(s)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        sellAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (sellFocus) {
                    makeExchange(s.toString(), 1)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

    }

    private fun changeRate(rateAmount: Editable?) {
        if (rateAmount.toString().trim().isNotEmpty()
            && rateAmount.toString().substring(0, 1) != "."
            && rateAmount.toString().substring(0, 1) != ","
            && rateAmount.toString().toDouble() > 0
            && buyAmount.text.toString().trim().isNotEmpty()
            && sellAmount.text.toString().trim().isNotEmpty()
            && buyAmount.text.toString().toDouble() > 0.00
            && sellAmount.text.toString().toDouble() > 0.00
        ) {
            viewModel.inputs.changeRate(
                buyAmount.text.toString().toDouble(),
                sellAmount.text.toString().toDouble(),
                rateAmount.toString().toDouble()
            )
        }
    }

    private fun makeExchange(amount: String, convertType: Int) {
        if (!TextUtils.isEmpty(amount) && rateAmount.text.toString().trim().isNotEmpty())
            viewModel.inputs.convertCurrency(
                amount.toDouble(),
                rateAmount.text.toString().toDouble(),
                convertType
            )
    }

    private fun setInputListeners() {
        viewModel.inputs.getAccountList()

        submitButton.setOnClickListener {
            val exchangeOperationType = if (arguments?.getInt("type") == 0) {
                5
            } else {
                6
            }
            if (buyAmount.text.toString().trim().isNotEmpty()
                && sellAmount.text.toString().trim().isNotEmpty()
                && rateAmount.text.toString().trim().isNotEmpty()
            ) {

                val intent = Intent(requireActivity(), BuyCurrencyVerifyActivity::class.java)
                if (arguments?.getInt("type") == 0) {
                    intent.putExtra(
                        "dtAccountId",
                        viewModel.getAccountIdByIban(buyAccount.text.toString().split(" ")[0])
                    )
                    intent.putExtra("dtAccount", buyAccount.text.toString().split(" ")[0])
                    intent.putExtra("dtAmount", buyAmount.text.toString().toDouble())
                    intent.putExtra(
                        "dtBranchId",
                        viewModel.getBranchIdByIban(buyAccount.text.toString().split(" ")[0])
                    )
                    intent.putExtra(
                        "crBranchId",
                        viewModel.getBranchIdByIban(sellAccount.text.toString().split(" ")[0])
                    )
                    intent.putExtra("crAmount", sellAmount.text.toString().toDouble())
                    intent.putExtra("crIban", sellAccount.text.toString().split(" ")[0])
                    intent.putExtra("currency", sellCurrency.text.toString())
                } else {
                    intent.putExtra(
                        "dtAccountId",
                        viewModel.getAccountIdByIban(sellAccount.text.toString().split(" ")[0])
                    )
                    intent.putExtra("dtAccount", sellAccount.text.toString().split(" ")[0])
                    intent.putExtra("dtAmount", sellAmount.text.toString().toDouble())
                    intent.putExtra(
                        "dtBranchId",
                        viewModel.getBranchIdByIban(sellAccount.text.toString().split(" ")[0])
                    )
                    intent.putExtra(
                        "crBranchId",
                        viewModel.getBranchIdByIban(buyAccount.text.toString().split(" ")[0])
                    )
                    intent.putExtra("crAmount", buyAmount.text.toString().toDouble())
                    intent.putExtra("crIban", buyAccount.text.toString().split(" ")[0])
                    intent.putExtra("currency", buyCurrency.text.toString())
                }

                intent.putExtra("exchangeRate", customExchange.text.toString().toDouble())
                intent.putExtra("exchangeOperationType", exchangeOperationType)
                startActivityForResult(intent, 12)
            } else {
                checkAmounts()
            }
        }
    }

    private fun String.showAlertDialog() {
        AlertDialog.Builder(requireActivity())
            .setTitle(getString(R.string.error_message))
            .setMessage(this)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .setCancelable(true)
            .show()
    }

    private fun checkAmounts() {
        if (buyAmount.text.toString().trim().isEmpty()) {
            buyAmount.error = getString(R.string.enter_amount)
        } else {
            buyAmount.error = null
        }
        if (sellAmount.text.toString().trim().isEmpty()) {
            sellAmount.error = getString(R.string.enter_amount)
        } else {
            sellAmount.error = null
        }
        if (rateAmount.text.toString().trim().isEmpty()) {
            getString(R.string.amount_not_found_for_entered_currency).showAlertDialog()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 12) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent(requireActivity(), BuyCurrencySuccessActivity::class.java)
                intent.putExtra("exchangeResponse", data?.getSerializableExtra("exchangeResponse"))
                intent.putExtra("exchangeRate", rateAmount.text.toString().toDouble())

                startActivity(intent)

                requireActivity().finish()
            }
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onError().subscribe{
            dialog.hideDialog()
            AlertDialogMapper(requireActivity(),it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
        viewModel.outputs.onConvertGot().subscribe {
            sellAmount.setText(decimalFormat.format(it).toString())
        }.addTo(subscriptions)
        viewModel.outputs.onRevertGot().subscribe {
            buyAmount.setText(decimalFormat.format(it).toString())
        }.addTo(subscriptions)
        viewModel.outputs.onCashlessGot().subscribe { cashlessList ->
            this.cashlessList.clear()
            this.cashlessList.addAll(cashlessList)
            for (i in 0 until cashlessList.size) {
                if (viewModel.sellAccountList.value!!.currName == cashlessList[i].currency) {
                    if (arguments?.getInt("type") == 0) {
                        rateAmount.setText(cashlessList[i].cashlessSell.toString())
                    } else {
                        rateAmount.setText(cashlessList[i].cashlessBuy.toString())
                    }
                }

            }
        }.addTo(subscriptions)
        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)
        viewModel.outputs.onAccountSuccess().subscribe { accountListModel ->

            manatAccountList.clear()
            foreignAccountList.clear()
            for (i in 0 until accountListModel.size) {
                if (accountListModel[i].currName == "AZN") {
                    manatAccountList.add(accountListModel[i])
                } else {
                    foreignAccountList.add(accountListModel[i])
                }
            }

            if (manatAccountList.isNotEmpty())
                setAccount(buyAccount, buyAccountContainer, buyCurrency, manatAccountList, false)

            if (foreignAccountList.isNotEmpty())
                setAccount(
                    sellAccount,
                    sellAccountContainer,
                    sellCurrency,
                    foreignAccountList,
                    true
                )

            setCurrencies()
            viewModel.buyAccountList.onNext(
                viewModel.getAccountByIban(
                    buyAccount.text.toString().split(
                        " "
                    )[0]
                )!!
            )
            viewModel.sellAccountList.onNext(
                viewModel.getAccountByIban(
                    sellAccount.text.toString().split(
                        " "
                    )[0]
                )!!
            )
        }.addTo(subscriptions)
    }

    @SuppressLint("SetTextI18n")
    private fun setCurrencies() {
        viewModel.buyAccountList.subscribe {
            buyCurrency.setText(it.currName)
            buyAccount.setText(it.iban + " / " + it.currName)
        }.addTo(subscriptions)
        viewModel.sellAccountList.subscribe {
            sellCurrency.setText(it.currName)
            sellAccount.setText(it.iban + " / " + it.currName)
        }.addTo(subscriptions)
    }

    private fun setAccount(
        account: AppCompatEditText,
        accountContainer: LinearLayout,
        currency: AppCompatEditText,
        accountList: ArrayList<AccountListModel>,
        changeRate: Boolean
    ) {
        // account.setText(accountList[0].iban)
        accountContainer.setOnClickListener {
            val accountNameArray = arrayOfNulls<String>(accountList.size)
            val accountCurrencyArray = arrayOfNulls<String>(accountList.size)
            val accountDialog: AlertDialog.Builder =
                AlertDialog.Builder(requireContext())
            accountDialog.setTitle(getString(R.string.select_account))

            for (i in 0 until accountList.size) {
                accountNameArray[i] = (accountList[i].iban + " / " + accountList[i].currName)
                accountCurrencyArray[i] = (accountList[i].currName)
            }
            accountDialog.setItems(accountNameArray) { _, which ->
                account.setText(accountNameArray[which])
                currency.setText(accountCurrencyArray[which])
                try {
                    if (changeRate) {
                        for (i in 0 until cashlessList.size) {
                            if (cashlessList[i].currency == accountCurrencyArray[which]) {
                                if (arguments?.getInt("type") == 0) {
                                    rateAmount.setText(cashlessList[i].cashlessSell.toString())
                                    sellAmount.setText(decimalFormat.format(buyAmount.text.toString().toDouble() / cashlessList[i].cashlessSell))
                                } else {
                                    rateAmount.setText(cashlessList[i].cashlessBuy.toString())
                                    buyAmount.setText(decimalFormat.format(sellAmount.text.toString().toDouble() * cashlessList[i].cashlessBuy))
                                }
                                break
                            } else {
                                rateAmount.text = null
                            }
                        }
                    }
                } catch (exc: Exception) {
                    exc.printStackTrace()
                }
            }
            val dialog = accountDialog.create()
            dialog.show()

        }
    }
}
