package az.turanbank.mlb.presentation.register.asan.individual

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.RELATIVE_TO_SELF
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.register.mobile.individual.IndividualRegisterMobileSubmitActivity
import az.turanbank.mlb.util.addPrefix
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_login_with_asan_individual.asanId
import kotlinx.android.synthetic.main.activity_login_with_asan_individual.asanPhoneNumber
import kotlinx.android.synthetic.main.content_individual_register_asan.progressImage
import kotlinx.android.synthetic.main.content_individual_register_asan.submitButton
import kotlinx.android.synthetic.main.content_individual_register_asan.submitText
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class IndividualRegisterAsanActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewmodel: IndividualRegisterAsanViewModel

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual_register_asan)

        toolbar_title.text = getString(R.string.register_with_asan)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewmodel = ViewModelProvider(
            this,
            viewModelFactory
        )[IndividualRegisterAsanViewModel::class.java]

        setupInputListeners()
        setupOutputListeners()
    }

    private fun setupInputListeners() {

        submitButton.setOnClickListener {
            if (asanPhoneNumber.text.toString().trim().isNotEmpty()) {
                if (asanId.text.toString().trim().isNotEmpty()) {
                    showProgressBar(true)
                    viewmodel.inputs.onSubmitClicked(
                        asanPhoneNumber.text.toString().addPrefix(),
                        asanId.text.toString()
                    )
                } else {
                    AlertDialogMapper(this, 124001).showAlertDialog()
                }
            } else {
                AlertDialogMapper(this, 124002).showAlertDialog()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        asanPhoneNumber.isClickable = !show
        asanId.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            RELATIVE_TO_SELF,
            0.5f,
            RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun setupOutputListeners() {
        viewmodel.outputs.registrationNotFinished()
            .subscribe { custId ->
                val intent = Intent(this, IndividualRegisterMobileSubmitActivity::class.java)

                intent.putExtra("registrationType", "asan")
                intent.putExtra("custId", custId)

                startActivity(intent)
                finish()
            }.addTo(subscriptions)

        viewmodel.outputs.verificationCode()
            .subscribe { getAuthInfoResponseModel ->
                val intent = Intent(
                    this@IndividualRegisterAsanActivity,
                    IndividualRegisterAsanPinActivity::class.java
                )

                intent.putExtra("verificationCode", getAuthInfoResponseModel.verificationCode)
                intent.putExtra("certificate", getAuthInfoResponseModel.certificate)
                intent.putExtra("challenge", getAuthInfoResponseModel.challenge)
                intent.putExtra("transactionId", getAuthInfoResponseModel.transactionId)
                intent.putExtra("custCode", viewmodel.custCode())
                intent.putExtra("pin", viewmodel.custPin())

                startActivity(intent)
                finish()
            }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

}
