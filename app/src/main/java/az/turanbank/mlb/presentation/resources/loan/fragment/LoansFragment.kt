package az.turanbank.mlb.presentation.resources.loan.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.resources.loan.LoanListModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.resources.loan.activity.LoanActivity
import com.facebook.shimmer.ShimmerFrameLayout
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class LoansFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var listViewModel: LoanListViewModel

    private lateinit var recyclerView: RecyclerView
    private lateinit var failMessage: TextView

    private lateinit var loanAdapter: LoanListAdapter

    lateinit var shimmerCardContainer: ShimmerFrameLayout
    private var loanList: ArrayList<LoanListModel> = arrayListOf()

    private lateinit var progress: ImageView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_cards, container, false)

        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL

        shimmerCardContainer = view.findViewById(R.id.shimmerCardContainer)
        recyclerView = view.findViewById(R.id.cardsRecyclerView)
        progress = view.findViewById(R.id.progress)
        failMessage = view.findViewById(R.id.failMessage)

        recyclerView.layoutManager = llm

        loanAdapter = LoanListAdapter(
            loanList
        ) {
            val intent = Intent(
                requireActivity(),
                LoanActivity::class.java
            )
            intent.putExtra("loanId", loanList[it].id)
            startActivity(intent)
        }

        recyclerView.adapter = loanAdapter
        setOutputListeners()
        setInputListeners()
        return view
    }

    private fun setOutputListeners() {
        listViewModel.outputs.onLoanListGot().subscribe {
            setRecyclerView(it)
            progress.clearAnimation()
            progress.visibility = View.GONE
            failMessage.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }.addTo(subscriptions)

        listViewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            failMessage.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
            shimmerCardContainer.stopShimmerAnimation()
            shimmerCardContainer.visibility = View.GONE
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
        }.addTo(subscriptions)
    }

    private fun setRecyclerView(loans: ArrayList<LoanListModel>) {
        loanList.clear()
        loanList.addAll(loans)
        recyclerView.adapter?.notifyDataSetChanged()
        shimmerCardContainer.stopShimmerAnimation()
        shimmerCardContainer.visibility = View.GONE
    }

    private fun setInputListeners() {
        animateProgressImage()
        loanList.clear()
        recyclerView.adapter?.notifyDataSetChanged()
        listViewModel.inputs.getLoans()
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        listViewModel = ViewModelProvider(this, factory)[LoanListViewModel::class.java]
    }
    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }

    override fun onResume() {
        super.onResume()
        shimmerCardContainer.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmerCardContainer.stopShimmerAnimation()
    }
}
