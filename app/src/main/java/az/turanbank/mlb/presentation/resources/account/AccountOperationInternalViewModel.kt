package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.response.GetCustInfoByIbanResponseModel
import az.turanbank.mlb.domain.user.usecase.GetCustInfoByIbanUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.*
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountOperationInternalViewModelInputs : BaseViewModelInputs {
    fun getAccountList()
    fun getCustInfoByIban(iban: String)
    fun deleteTemp(tempId: Long)
}

interface AccountOperationInternalViewModelOutputs : BaseViewModelOutputs {
    fun onAccountSuccess(): BehaviorSubject<ArrayList<AccountListModel>>
    fun custInfo(): PublishSubject<GetCustInfoByIbanResponseModel>
    fun showProgress(): PublishSubject<Boolean>
    fun onTemplateDeleted(): CompletableSubject
}

class AccountOperationInternalViewModel @Inject constructor(
    private val getCustInfoByIbanUseCase: GetCustInfoByIbanUseCase,
    private val getIndividualAccountListUseCase: GetNoCardAccountListForIndividualCustomerUseCase,
    private val getJuridicalAccountListUseCase: GetNoCardAccountListForJuridicalCustomerUseCase,
    private val deleteOperationTemplateUseCase: DeleteOperationTemplateUseCase,
    private val sharedPrefs: SharedPreferences

) : BaseViewModel(), AccountOperationInternalViewModelInputs,
    AccountOperationInternalViewModelOutputs {

    override fun custInfo() = custInfo

    override val inputs: AccountOperationInternalViewModelInputs = this
    override val outputs: AccountOperationInternalViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val accountList = BehaviorSubject.create<ArrayList<AccountListModel>>()
    private val showProgress = PublishSubject.create<Boolean>()
    private val custInfo = PublishSubject.create<GetCustInfoByIbanResponseModel>()
    private val delete = CompletableSubject.create()

    override fun getAccountList() {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getJuridicalAccountListUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                lang = EnumLangType.AZ
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getIndividualAccountListUseCase.execute(custId, token,EnumLangType.AZ)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun getCustInfoByIban(iban: String){
        getCustInfoByIbanUseCase.execute(custId, token, iban)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                custInfo.onNext(it)
                if(it.status.statusCode == 1) {
                    custInfo.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            },{
                error.onNext(1878)
                it.printStackTrace()
            })
            .addTo(subscriptions)
    }

    override fun deleteTemp(tempId: Long) {
        deleteOperationTemplateUseCase
            .execute(custId, token, tempId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    delete.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {

            }).addTo(subscriptions)
    }

    override fun onTemplateDeleted() = delete

    override fun onAccountSuccess() = accountList

    override fun showProgress() = showProgress

    fun getDtAccountId(iban: String): Long {
        accountList.value?.let {
            for(i in 0 until it.size) {
                if(it[i].iban == iban)
                    return it[i].accountId
            }
        }
        return 0L
    }
    fun getAccountCurrency(iban: String): String {
        accountList.value?.let {
            for(i in 0 until it.size) {
                if(it[i].iban == iban)
                    return it[i].currName
            }
        }
        return ""
    }
}