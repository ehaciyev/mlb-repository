package az.turanbank.mlb.presentation.exchange.fragment.cash

import az.turanbank.mlb.data.remote.model.exchange.ExchangeCashResponseModel
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ExchangeCashViewModelInputs: BaseViewModelInputs {
    fun getCashExchange(exchangeDate: String?)
}
interface ExchangeCashViewModelOutputs: BaseViewModelOutputs {
    fun onCashExchangeGot(): PublishSubject<ExchangeCashResponseModel>
}

class ExchangeCashViewModel @Inject constructor(
    private val exchangeCashUseCase: ExchangeCashUseCase
): BaseViewModel(),
    ExchangeCashViewModelInputs,
    ExchangeCashViewModelOutputs {
    override val inputs: ExchangeCashViewModelInputs = this

    override val outputs: ExchangeCashViewModelOutputs = this


    private val cashExchangeList = PublishSubject.create<ExchangeCashResponseModel>()
    override fun getCashExchange(exchangeDate: String?) {
        exchangeCashUseCase.execute(exchangeDate)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({
                if (it.status.statusCode == 1) {
                    cashExchangeList.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }
            ).addTo(subscriptions)
    }

    override fun onCashExchangeGot() = cashExchangeList
}