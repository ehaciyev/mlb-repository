package az.turanbank.mlb.presentation.ips.pay.debitorchoice

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.account.IPSAccountChoiceFragment
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.alias.IPSAliasChoiceFragment

private val TAB_TITLES = arrayOf(
    R.string.account_title,
    R.string.alias
)
private val TAB_FRAGMENTS = arrayOf(
    IPSAccountChoiceFragment(),
    IPSAliasChoiceFragment()
)

class IPSTransferPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return TAB_FRAGMENTS[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return TAB_FRAGMENTS.size
    }
}