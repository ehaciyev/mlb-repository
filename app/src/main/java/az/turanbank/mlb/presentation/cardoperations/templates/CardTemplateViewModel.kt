package az.turanbank.mlb.presentation.cardoperations.templates

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.cardoperations.CardTemplateResponseModel
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.domain.user.usecase.cardoperations.DeleteCardOperationTempUseCase
import az.turanbank.mlb.domain.user.usecase.cardoperations.GetCardOperationsTempByIdUseCase
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashlessUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetIndividualCardListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.cardoperations.cardtocard.CardToCardActivity
import az.turanbank.mlb.presentation.cardoperations.othercard.CardToOtherCardActivity
import az.turanbank.mlb.presentation.cardoperations.cashbycode.CashByCodeActivity
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import java.util.ArrayList
import javax.inject.Inject

interface CardTemplateViewModelInputs: BaseViewModelInputs{
    fun getTheTemplate(tempId: Long)
    fun deleteTemp(tempId: Long)
}
interface CardTemplateViewModelOutputs: BaseViewModelOutputs{
    fun onTemplateSuccess(): PublishSubject<CardTemplateResponseModel>
    fun cardListSuccess(): PublishSubject<ArrayList<CardListModel>>
    fun currencyListSuccess(): PublishSubject<ArrayList<String>>
    fun currencyAndCardSuccess(): PublishSubject<Boolean>
    fun templateDeleted(): CompletableSubject
}
class CardTemplateActivityViewModel @Inject constructor(
    private val getCardOperationsTempByIdUseCase: GetCardOperationsTempByIdUseCase,
    private val getIndividualCardListUseCase: GetIndividualCardListUseCase,
    private val getCashlessUseCase: ExchangeCashlessUseCase,
    private val deleteCardOperationTempUseCase: DeleteCardOperationTempUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
        CardTemplateViewModelInputs,
        CardTemplateViewModelOutputs{
    override val inputs: CardTemplateViewModelInputs = this

    override val outputs: CardTemplateViewModelOutputs = this

    private val cardList = PublishSubject.create<ArrayList<CardListModel>>()
    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)

    private val template = PublishSubject.create<CardTemplateResponseModel>()
    private val currencyList = PublishSubject.create<ArrayList<String>>()
    private val currencyAndCard = PublishSubject.create<Boolean>()
    private val delete = CompletableSubject.create()
    private lateinit var templateModel: CardTemplateResponseModel
    var destinationCardNumber = ""

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private fun getCards() {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        getIndividualCardListUseCase
            .execute(custId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    cardList.onNext(it.cardList)
                    getCurrencies()
                }
            },{

            }).addTo(subscriptions)
    }
    override fun getTheTemplate(tempId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        currencyAndCard.onNext(false)
        getCardOperationsTempByIdUseCase
            .execute(tempId, custId, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    destinationCardNumber = it.destinationCardNumber
                    template.onNext(it)
                    templateModel = it
                    getCards()
                }
            },{

            }).addTo(subscriptions)
    }
    private fun getCurrencies() {
        val list = arrayListOf("AZN")
        getCashlessUseCase.execute("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    for (i in 0 until it.exchangeCashlessList.size) { // TODO check it
                        list.add(it.exchangeCashlessList[i].currency)
                    }
                }
            }, {

            },{
                currencyList.onNext(list)
                currencyAndCard.onNext(true)
            }).addTo(subscriptions)
    }

    override fun deleteTemp(tempId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        deleteCardOperationTempUseCase
            .execute(tempId, custId, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    delete.onComplete()
                }
            }, {

            }).addTo(subscriptions)
    }

    fun getIntentWithData(activity: Activity, amount: Double): Intent {
        lateinit var intent: Intent

        when (templateModel.cardOperationType) {
            1 -> {
                intent = Intent(activity, CardToCardActivity::class.java)
            }
            2 -> {
                intent = Intent(activity, CardToOtherCardActivity::class.java)
            }
            3 -> {
                intent = Intent(activity, CashByCodeActivity::class.java)
            }
        }
        intent.putExtra("fromTemplate", true)
        intent.putExtra("fromCard", templateModel.requestorCardNumber)
        intent.putExtra("toCardOrMobile", templateModel.destinationCardNumber)
        intent.putExtra("amount", amount)
        intent.putExtra("currency", templateModel.currency)

        return intent
    }

    override fun onTemplateSuccess() = template

    override fun cardListSuccess() = cardList

    override fun currencyListSuccess() = currencyList

    override fun currencyAndCardSuccess() = currencyAndCard

    override fun templateDeleted() = delete
}