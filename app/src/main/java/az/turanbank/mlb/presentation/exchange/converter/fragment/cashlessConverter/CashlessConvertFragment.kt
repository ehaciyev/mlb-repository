package az.turanbank.mlb.presentation.exchange.converter.fragment.cashlessConverter

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.widget.AppCompatEditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.local.ExchangeFlagMapper
import az.turanbank.mlb.data.remote.model.exchange.CashlessResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.exchange.converter.fragment.cashConverter.ExchangeFlagMapperConverter
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.text.DecimalFormat
import javax.inject.Inject

class CashlessConvertFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CashlessConvertViewModel

    private lateinit var fromExchange: AppCompatEditText

    private lateinit var toExchange: TextView

    private lateinit var fromExchangeLinearLayout: LinearLayout
    private lateinit var toExchangeLinearLayout: LinearLayout

    private lateinit var toRecyclerView: RecyclerView
    private lateinit var fromRecyclerView: RecyclerView

    private lateinit var fromImageView: ImageView
    private lateinit var toImageView: ImageView

    private lateinit var failView: TextView
    private lateinit var mainView: ScrollView

    private lateinit var progress: ImageView

    private lateinit var convert: ImageButton

    private lateinit var fromName: TextView
    private lateinit var toName: TextView

    private var decimalFormat = DecimalFormat("0.00")

    private var currencyList: ArrayList<CashlessResponseModel> = arrayListOf()
    private var currencyCashList: ArrayList<CashlessResponseModel> = arrayListOf()

    private var fromCurrency = BehaviorSubject.create<CashlessResponseModel>()
    private var toCurrency = BehaviorSubject.create<CashlessResponseModel>()
    private lateinit var currencySave: CashlessResponseModel

    private lateinit var toCurrencyAdapter: ConvertCashlessAdapter
    private lateinit var fromCurrencyAdapter: ConvertCashlessAdapter

    private var fromRecyclerVisibility = PublishSubject.create<Boolean>()
    private var toRecyclerVisibility = PublishSubject.create<Boolean>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_cash_convert, container, false)
        initViews(view)

        animateProgressImage()

        setInputListeners()
        setOutputListeners()

        setRecyclers()

        return view
    }

    private fun setRecyclers() {

        val toLLm = LinearLayoutManager(requireContext())
        toLLm.orientation = LinearLayoutManager.VERTICAL
        fromRecyclerView.layoutManager = toLLm

        val fromLLm = LinearLayoutManager(requireContext())
        fromLLm.orientation = LinearLayoutManager.VERTICAL

        toRecyclerView.layoutManager = fromLLm

        toCurrencyAdapter = ConvertCashlessAdapter(requireContext(), currencyList) {
            toCurrency.onNext(currencyList[it])
        }
        toRecyclerView.adapter = toCurrencyAdapter

        fromCurrencyAdapter = ConvertCashlessAdapter(requireContext(), currencyList) {
            fromCurrency.onNext(currencyList[it])
        }
        fromRecyclerView.adapter = fromCurrencyAdapter
    }

    private fun setOutputListeners() {

        viewModel.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE

            failView.text = getString(ErrorMessageMapper().getErrorMessage(it))
            mainView.visibility = View.GONE
            failView.visibility = View.VISIBLE
        }.addTo(subscriptions)

        viewModel.outputs.onConvertGot().subscribe {
            toExchange.text = decimalFormat.format(it)
        }.addTo(subscriptions)

        viewModel.onValuesFetched().subscribe {
            val list = it.exchangeCashlessList
            val azn = CashlessResponseModel("AZN", 1.0, 1.0)
            list.add(azn)
            currencyCashList.clear()
            currencyCashList.addAll(list)
            fromCurrency.onNext(list[0])
            toCurrency.onNext(list[0])

            progress.clearAnimation()
            progress.visibility = View.GONE

            mainView.visibility = View.VISIBLE
            failView.visibility = View.GONE
            setRecyclerView(it.exchangeCashlessList)
        }.addTo(subscriptions)
        viewModel.onError().subscribe {
        }.addTo(subscriptions)

        fromRecyclerVisibility.subscribe {
            if (it)
                fromRecyclerView.visibility = View.VISIBLE
            else
                fromRecyclerView.visibility = View.GONE
        }.addTo(subscriptions)

        toRecyclerVisibility.subscribe {
            if (it)
                toRecyclerView.visibility = View.VISIBLE
            else
                toRecyclerView.visibility = View.GONE
        }.addTo(subscriptions)
    }

    private fun setRecyclerView(currencies: ArrayList<CashlessResponseModel>) {
        currencyList.clear()
        currencyList.addAll(currencies)
        fromRecyclerView.adapter?.notifyDataSetChanged()
        toRecyclerView.adapter?.notifyDataSetChanged()
    }

    private fun setInputListeners() {
        toCurrency.subscribe {
            toRecyclerVisibility.onNext(false)
            toName.text = it.currency
            toImageView.setImageResource(ExchangeFlagMapperConverter().resolveFlag(it.currency))
            makeExchange(fromExchange.text)
        }.addTo(subscriptions)

        fromCurrency.subscribe {
            fromRecyclerVisibility.onNext(false)
            fromName.text = it.currency
            fromImageView.setImageResource(ExchangeFlagMapperConverter().resolveFlag(it.currency))
            makeExchange(fromExchange.text)
        }.addTo(subscriptions)

        fromExchange.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                makeExchange(s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        convert.setOnClickListener {
            currencySave = fromCurrency.value!!
            fromCurrency.onNext(toCurrency.value!!)
            toCurrency.onNext(currencySave)
            makeExchange(fromExchange.text)
        }

        fromExchangeLinearLayout.setOnClickListener {
            fromRecyclerVisibility.onNext(true)
        }
        toExchangeLinearLayout.setOnClickListener {
            toRecyclerVisibility.onNext(true)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = ViewModelProvider(this, factory)[CashlessConvertViewModel::class.java]
        viewModel.getExchangValues()
    }

    private fun makeExchange(fromExchange: Editable?) {
        if (!(TextUtils.isEmpty(fromExchange.toString()) || currencyCashList.isEmpty()))
            viewModel.inputs.convertCurrency(
                fromCurrency.value!!,
                toCurrency.value!!,
                fromExchange.toString().toDouble()
            )
    }

    private fun initViews(view: View) {
        fromExchange = view.findViewById(R.id.fromExchange)
        fromExchangeLinearLayout = view.findViewById(R.id.fromExchangeLinearLayout)

        toExchange = view.findViewById(R.id.toExchange)
        toExchangeLinearLayout = view.findViewById(R.id.toExchangeLinearLayout)

        toImageView = view.findViewById(R.id.toImageView)
        fromImageView = view.findViewById(R.id.fromImageView)

        toName = view.findViewById(R.id.toName)
        fromName = view.findViewById(R.id.fromName)

        convert = view.findViewById(R.id.convert)

        toRecyclerView = view.findViewById(R.id.toRecyclerView)
        fromRecyclerView = view.findViewById(R.id.fromRecyclerView)

        failView = view.findViewById(R.id.failMessage)
        mainView = view.findViewById(R.id.mainView)

        progress = view.findViewById(R.id.progress)
    }
    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progress.startAnimation(rotate)
    }
}
