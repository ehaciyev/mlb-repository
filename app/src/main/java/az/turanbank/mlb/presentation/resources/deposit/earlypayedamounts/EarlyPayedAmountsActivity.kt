package az.turanbank.mlb.presentation.resources.deposit.earlypayedamounts

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.resources.deposit.EarlyPaysResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_early_payed_amounts.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.util.*
import javax.inject.Inject

class EarlyPayedAmountsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: EarlyPayedAmountsViewModel

    private val paymentList = arrayListOf<EarlyPaysResponseModel>()

    private val depositId: Long by lazy { intent.getLongExtra("depositId", 0L) }
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_early_payed_amounts)

        viewModel =
            ViewModelProvider(this, factory)[EarlyPayedAmountsViewModel::class.java]

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        toolbar_title.text =  getString(R.string.withdraw_percentages)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        recyclerView.layoutManager = llm
        val paymentAdapter =
            EarlyPayedAmountsAdapter(
                this.paymentList
            ) {

            }

        recyclerView.adapter = paymentAdapter
        setInputListeners()
        setOutputListeners()
    }
    private fun setOutputListeners() {
        viewModel.outputs.onError().subscribe {
            fail.text = getString(ErrorMessageMapper().getErrorMessage(it))
            fail.visibility = View.VISIBLE
            descriptionView.visibility = View.GONE
            recyclerView.visibility = View.GONE
            progress.clearAnimation()
            progress.visibility = View.GONE
        }.addTo(subscriptions)
        viewModel.onEarlyPayedAmountsSuccess().subscribe {
            fail.visibility = View.GONE
            updateRecyclerView(it)
            descriptionView.visibility = View.VISIBLE
            recyclerView.visibility = View.VISIBLE
            progress.clearAnimation()
            progress.visibility = View.GONE
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        animateProgressImage()
        viewModel.inputs.getEarlyPayedAmounts(depositId)
    }
    private fun updateRecyclerView(paymentPlanList: ArrayList<EarlyPaysResponseModel>) {
        this.paymentList.clear()
        this.paymentList.addAll(paymentPlanList)
        recyclerView.adapter?.notifyDataSetChanged()
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
