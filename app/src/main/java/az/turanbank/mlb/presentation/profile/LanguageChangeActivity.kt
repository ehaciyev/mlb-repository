package az.turanbank.mlb.presentation.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.home.MlbHomeActivity
import az.turanbank.mlb.presentation.login.activities.LoginActivity
import kotlinx.android.synthetic.main.activity_language_change.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class LanguageChangeActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: LanguageChangeViewModel

    private val isSignIn: Boolean by lazy { intent.getBooleanExtra("isSignIn",false) }
    lateinit var activity:Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language_change)

        viewmodel = ViewModelProvider(this, factory) [LanguageChangeViewModel::class.java]

        activity = if(isSignIn){
            MlbHomeActivity()
        } else{
            LoginActivity()
        }

        when(viewmodel.getLang()) {
            "az" -> {
                langAzSelected.visibility = View.VISIBLE
            }

            "en" -> {
                langEngSelected.visibility = View.VISIBLE
            }

            "ru" -> {
                langRuSelected.visibility = View.VISIBLE
            }

            else -> {
                langAzSelected.visibility = View.VISIBLE
            }
        }

        changeToAz.setOnClickListener {
            viewmodel.setLang("az")
            restartApp(activity)
        }

        changeToEng.setOnClickListener {
            viewmodel.setLang("en")
            restartApp(activity)
        }

        changeToRu.setOnClickListener {
            viewmodel.setLang("ru")
            restartApp(activity)
        }

        toolbar_title.text =resources.getString(R.string.lang_choice)
        toolbar_back_button.setOnClickListener {onBackPressed()}
    }

    private fun restartApp(activity: Activity) {
        val intent = Intent(this, activity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }
}
