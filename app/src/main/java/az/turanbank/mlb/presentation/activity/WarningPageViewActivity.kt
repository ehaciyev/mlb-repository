package az.turanbank.mlb.presentation.activity

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import az.turanbank.mlb.R
import az.turanbank.mlb.SplashActivity
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.login.activities.pin.LoginPinActivity
import kotlinx.android.synthetic.main.activity_warning_page_view.*

class WarningPageViewActivity : BaseActivity() {

    private val tokenExpired: Boolean by lazy { intent.getBooleanExtra("tokenExpired", false) }
    private val fromDeleteAlias: Boolean by lazy { intent.getBooleanExtra("fromDeleteAlias", false) }
    private val description: String by lazy { intent.getStringExtra("description") }
    private val isCheckVersion: Boolean by lazy { intent.getBooleanExtra("isCheckVersion", false) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_warning_page_view)

        if(isCheckVersion){
            urlToUpdate.visibility = View.VISIBLE
            urlToUpdate.setOnClickListener{
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
            }
        } else {
            urlToUpdate.visibility = View.GONE
        }

        warning_description.text = description
        if (fromDeleteAlias) {
            close.text = getString(R.string.remove)

        }
        close.setOnClickListener {
            if (fromDeleteAlias) {
                setResult(Activity.RESULT_OK)
                finish()
            } else {
                if (tokenExpired) {
                    startActivity(Intent(this, SplashActivity::class.java))
                } else {
                }
                finish()
            }
        }

        if(description == "aliasDeleteError"){
            var deleted = intent.getSerializableExtra("deleted") as ArrayList<MLBAliasResponseModel>
            var undDleted = intent.getSerializableExtra("undDeleted") as ArrayList<MLBAliasResponseModel>
            if(undDleted.size == 0){
                warning_description.text = deleted.size.toString() + " " + getString(R.string.alias_delete) + "\n"
            } else {
                warning_description.text = deleted.size.toString() + " " + getString(R.string.alias_delete) + "\n" + undDleted.size.toString() + getString(R.string.alias_not_deleted)
            }

            close.text = getString(R.string.more)


            var messageText = ""
            undDleted.forEach {
                messageText = messageText + it.ipsValue + " - " + getString(ErrorMessageMapper().getErrorMessage(it.respStatus.statusCode)) + "\n"
            }

            close.setOnClickListener {
                AlertDialog.Builder(this)
                    .setTitle(R.string.more)
                    .setMessage(messageText)
                    .setPositiveButton(R.string.ok) { _, _ ->
                        finish()
                    }
                    .setCancelable(true)
                    .show()
            }
        }


    }
}
