package az.turanbank.mlb.presentation.resources.account

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_requisites.*
import javax.inject.Inject

class AccountRequisitesActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: AccountRequisitesActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_requisites)

        viewModel = ViewModelProvider(this, factory) [AccountRequisitesActivityViewModel::class.java]

        setInputListeners()
        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.showProgress().subscribe {
            if(it) {
                animateProgressImage()
            } else {
                accReqProgressImage.clearAnimation()
                accReqProgressImage.visibility = View.GONE
                accReqScrollView.visibility = View.VISIBLE
            }
        }.addTo(subscriptions)

        viewModel.outputs.onAccountRequisitesSuccess().subscribe {responseModel ->
            accReqScrollView.visibility = View.VISIBLE


            failMessage.visibility = View.GONE
            cardView.visibility = View.VISIBLE
            accReqShare.setOnClickListener {
                it.isClickable = true
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type="text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, responseModel.toString())
                startActivity(Intent.createChooser(shareIntent, "Göndər..."))
            }

            accReqBranchCode.text = responseModel.branchCode.toString()
            accReqBranchName.text = responseModel.branchName
            accReqBranchInn.text = responseModel.branchInn
            accReqCorrespondentAccount.text = responseModel.correspondentAccount
            accReqSwift.text = responseModel.swift
            accReqAddress.text = responseModel.address
            accReqPhone.text = responseModel.phone
            accReqAccountName.text = responseModel.accountName
            accReqCurrName.text = responseModel.currName
            accReqIban.text = responseModel.iban

            if(responseModel.accountInn != null) {
                accReqAccountInn.text = responseModel.accountInn
                accReqAccountInn.visibility = View.VISIBLE
                accReqAccountDesc.visibility = View.VISIBLE
            }
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
            accReqProgressImage.clearAnimation()
            accReqProgressImage.visibility = View.GONE
            cardView.visibility = View.GONE
            failMessage.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        toolbar_back_button.setOnClickListener { onBackPressed() }
        animateProgressImage()
        viewModel.inputs.getAccountRequisites(intent.getLongExtra("accountId", 0L))
    }

    private fun animateProgressImage() {
        accReqProgressImage.visibility = View.VISIBLE
        failMessage.visibility = View.GONE
        cardView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        accReqProgressImage.startAnimation(rotate)
    }
}
