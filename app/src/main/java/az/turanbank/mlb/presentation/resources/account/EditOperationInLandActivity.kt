package az.turanbank.mlb.presentation.resources.account

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.adapter.GetBankListAdapter
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_edit_operation_in_land.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class EditOperationInLandActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: EditOperationInLandViewModel
    lateinit var dialog: MlbProgressDialog

    private var selectedAccountId: String = ""
    private lateinit var textWatcher: TextWatcher

    private val operId: Long by lazy { intent.getLongExtra("operId", 0L) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_operation_in_land)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        opInCrAccId.filters = arrayOf(InputFilter.AllCaps())

        dialog = MlbProgressDialog(this)

        toolbar_title.text = getString(R.string.domestic_transfer)

        viewmodel = ViewModelProvider(this, factory) [EditOperationInLandViewModel::class.java]
        initTextWatcher()

        buttonEnabled(false)
        setOutputListeners()
        setInputListeners()

      /*  submitButton.setOnClickListener {

            val dtAccountId = selectedAccountId
            val crIban = opInCrAccId.text.toString()
            val crCustTaxid = opInLandCrAccTaxNo.text.toString()
            val crCustName = opInCrAccName.text.toString()
            val crBankCode = viewmodel.getSelectedBankCode()
            val crBankName = crBankId.text.toString()
            val crBankTaxid = opInLandBankIban.text.toString()
            val crBankCorrAcc = opInLandCorrespondentAcc.text.toString()
            val budgetCode = viewmodel.getSelectedBudgetClassCode()
            val budgetLvl = viewmodel.getSelectedBudgetLevelCode()
            val amount = opInLandAmount.text.toString()
            val purpose = opInLandPurpose.text.toString()
            val note = opInLandExtraInfo.text.toString()

            viewmodel.inputs.updateOperationById (
                dtAccountId = dtAccountId.toLong(),
                crIban = crIban,
                crCustTaxid = crCustTaxid,
                crCustName = crCustName,
                crBankCode = crBankCode,
                crBankName = crBankName,
                crBankTaxid = crBankTaxid,
                crBankCorrAcc = crBankCorrAcc,
                budgetCode = budgetCode,
                budgetLvl = budgetLvl,
                amount = amount.toDouble(),
                purpose = purpose,
                note = note,
                operationType = 3,
                operId = operId,
                operNameId = 2L
            )
        }*/
    }

    private fun initTextWatcher() {
        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                if(opInLandCrAccTaxNo.text.toString().length in 1..9) {
                    opInLandCrAccTaxNoContainer.background = ContextCompat.getDrawable(this@EditOperationInLandActivity, R.drawable.incorrect_input_item_background)
                    buttonEnabled(false)
                }
                else if(opInCrAccId.text.toString().length in 1..27) {
                    opInLandCrAccIdContainer.background = ContextCompat.getDrawable(this@EditOperationInLandActivity, R.drawable.incorrect_input_item_background)
                    buttonEnabled(false)
                }  else {
                    if (opInLandBudgetClassCodeContainer.visibility == View.VISIBLE) {
                        if (!opInLandAccountId.text.isNullOrEmpty() &&
                            !crBankId.text.isNullOrEmpty() &&
                            !opInLandBankIban.text.isNullOrEmpty() &&
                            !opInLandCorrespondentAcc.text.isNullOrEmpty() &&
                            !opInLandSwift.text.isNullOrEmpty() &&
                            !opInCrAccId.text.isNullOrEmpty() &&
                            !opInCrAccName.text.isNullOrEmpty() &&
                            !opInLandAmount.text.isNullOrEmpty() &&
                            !opInLandPurpose.text.isNullOrEmpty() &&
                            !opInLandBudgetClassCode.text.isNullOrEmpty() &&
                            !opInLandBudgetLevelCode.text.isNullOrEmpty()) {

                            buttonEnabled(true)
                            submitButton.setOnClickListener {

                                val dtAccountId = selectedAccountId

                                val crIban = opInCrAccId.text.toString()
                                val crCustTaxid = opInLandCrAccTaxNo.text.toString()
                                val crCustName = opInCrAccName.text.toString()
                                val crBankCode = viewmodel.getSelectedBankCode()
                                val crBankName = crBankId.text.toString()
                                val crBankTaxid = opInLandBankIban.text.toString()
                                val crBankCorrAcc = opInLandCorrespondentAcc.text.toString()
                                val budgetCode = viewmodel.getSelectedBudgetClassCode()
                                val budgetLvl = viewmodel.getSelectedBudgetLevelCode()
                                val amount = opInLandAmount.text.toString()
                                val purpose = opInLandPurpose.text.toString()
                                val note = opInLandExtraInfo.text.toString()

                                viewmodel.inputs.updateOperationById (
                                    dtAccountId = dtAccountId.toLong(),
                                    crIban = crIban,
                                    crCustTaxid = crCustTaxid,
                                    crCustName = crCustName,
                                    crBankCode = crBankCode,
                                    crBankName = crBankName,
                                    crBankTaxid = crBankTaxid,
                                    crBankCorrAcc = crBankCorrAcc,
                                    budgetCode = budgetCode,
                                    budgetLvl = budgetLvl,
                                    amount = amount.toDouble(),
                                    purpose = purpose,
                                    note = note,
                                    operationType = 3,
                                    operId = operId,
                                    operNameId = 2L
                                )
                            }
                        } else {
                            buttonEnabled(false)
                        }
                    } else {
                        if (!opInLandAccountId.text.isNullOrEmpty() &&
                            !crBankId.text.isNullOrEmpty() &&
                            !opInLandBankIban.text.isNullOrEmpty() &&
                            !opInLandCorrespondentAcc.text.isNullOrEmpty() &&
                            !opInLandSwift.text.isNullOrEmpty() &&
                            !opInCrAccId.text.isNullOrEmpty() &&
                            !opInCrAccName.text.isNullOrEmpty() &&
                            !opInLandAmount.text.isNullOrEmpty() &&
                            !opInLandPurpose.text.isNullOrEmpty()) {

                            buttonEnabled(true)

                            submitButton.setOnClickListener {

                                val dtAccountId = selectedAccountId
                                val crIban = opInCrAccId.text.toString()
                                val crCustTaxid = opInLandCrAccTaxNo.text.toString()
                                val crCustName = opInCrAccName.text.toString()
                                val crBankCode = viewmodel.getSelectedBankCode()
                                val crBankName = crBankId.text.toString()
                                val crBankTaxid = opInLandBankIban.text.toString()
                                val crBankCorrAcc = opInLandCorrespondentAcc.text.toString()
                                val budgetCode = viewmodel.getSelectedBudgetClassCode()
                                val budgetLvl = viewmodel.getSelectedBudgetLevelCode()
                                val amount = opInLandAmount.text.toString()
                                val purpose = opInLandPurpose.text.toString()
                                val note = opInLandExtraInfo.text.toString()

                                viewmodel.inputs.updateOperationById (
                                    dtAccountId = dtAccountId.toLong(),
                                    crIban = crIban,
                                    crCustTaxid = crCustTaxid,
                                    crCustName = crCustName,
                                    crBankCode = crBankCode,
                                    crBankName = crBankName,
                                    crBankTaxid = crBankTaxid,
                                    crBankCorrAcc = crBankCorrAcc,
                                    budgetCode = budgetCode,
                                    budgetLvl = budgetLvl,
                                    amount = amount.toDouble(),
                                    purpose = purpose,
                                    note = note,
                                    operationType = getOperationType(),
                                    operId = operId,
                                    operNameId = 2L
                                )
                            }
                        } else {
                            buttonEnabled(false)
                        }
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        opInLandAccountId.addTextChangedListener(textWatcher)
        crBankId.addTextChangedListener(textWatcher)
        opInLandBankIban.addTextChangedListener(textWatcher)
        opInLandCorrespondentAcc.addTextChangedListener(textWatcher)
        opInLandSwift.addTextChangedListener(textWatcher)
        opInCrAccId.addTextChangedListener(textWatcher)
        opInCrAccName.addTextChangedListener(textWatcher)
        opInLandAmount.addTextChangedListener(textWatcher)
        opInLandPurpose.addTextChangedListener(textWatcher)
        opInLandBudgetLevelCode.addTextChangedListener(textWatcher)
        opInLandBudgetClassCode.addTextChangedListener(textWatcher)
    }

    private fun setInputListeners() {
        viewmodel.inputs.getAccountList()
    }
    private fun hideProgressImages(progressImage: ImageView, hide: Boolean) {
        if (hide) {
            progressImage.visibility = View.GONE
        } else {
            progressImage.visibility = View.VISIBLE
        }
    }

    private fun getOperationType(): Int {
        return if (opInLandAzipsSwitch.isChecked) 3
        else 2
    }

    private fun revertProgressImageAnimation(progressImage: ImageView) {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage(progressImage: ImageView) {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun showProgressImages(progressImage: ImageView, show: Boolean) {
        hideProgressImages(progressImage, show)

        if (show) {
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }

    private fun setOutputListeners() {
        viewmodel.outputs.onAccountSuccess().subscribe { accountListModel ->
            var selectedAccountCurr: String

            viewmodel.inputs.getOperationById(operId)

            //   opInLandAccountId.setText(accountListModel[0].iban)
            selectedAccountId = accountListModel[0].accountId.toString()
            selectedAccountCurr = accountListModel[0].currName
            viewmodel.inputs.getBankList(selectedAccountCurr)

            opInLandDtAccountIdContainer.setOnClickListener {
                val accountNameArray = arrayOfNulls<String>(accountListModel.size)
                val b: AlertDialog.Builder =
                    AlertDialog.Builder(this@EditOperationInLandActivity)
                b.setTitle(getString(R.string.select_account))

                for (i in 0 until accountListModel.size) {
                    accountNameArray[i] = (accountListModel[i].iban)
                }
                b.setItems(accountNameArray) { _, which ->
                    opInLandAccountId.setText(accountNameArray[which])

                    if(!opInLandAccountId.text.toString().endsWith("944")) {
                        opInLandAzipsSwitchContainer.visibility = View.GONE
                    } else {
                        opInLandAzipsSwitchContainer.visibility = View.VISIBLE
                    }

                    selectedAccountId = accountListModel[which].accountId.toString()
                    selectedAccountCurr = accountListModel[which].currName
                    viewmodel.inputs.getBankList(selectedAccountCurr)
                }
                val dialog = b.create()
                dialog.show()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.operationDetails().subscribe {

            selectedAccountId = viewmodel.getAccountId(it.dtIban).toString()
            opInLandAccountId.setText(it.dtIban)
            if(!it.dtIban.endsWith("944")) {
                opInLandAzipsSwitchContainer.visibility = View.GONE
            } else {
                opInLandAzipsSwitchContainer.visibility = View.VISIBLE
            }

            crBankId.setText(it.crBankName)
            opInLandBankIban.setText(it.crBankTaxId)
            opInLandCorrespondentAcc.setText(it.crBankCorrAcc)
            opInLandSwift.setText(it.crBankSwift)
            opInCrAccId.setText(it.crIban)
            opInCrAccName.setText(it.crCustName)
            opInLandCrAccTaxNo.setText(it.crCustTaxid)
            opInLandAmount.setText(it.amt.toString())
            opInLandPurpose.setText(it.operPurpose)
            opInLandExtraInfo.setText(it.note)
            viewmodel.setSelectedBankCode(it.crBankCode)
            viewmodel.setSelectedBudgetClassCode(it.budgetCode)
            viewmodel.setSelectedBudgetLevelCode(it.budgetLvl)
            opInLandAzipsSwitch.isChecked = it.operTypeId == 3

            if(it.crBankCode == "210005") {
                opInLandBudgetClassCodeContainer.visibility = View.VISIBLE
                opInLandBudgetLevelCodeContainer.visibility = View.VISIBLE
                opInLandAzipsSwitchContainer.visibility = View.GONE

                opInLandAzipsSwitch.isChecked = it.operTypeId == 3

                opInLandBudgetClassCode.setText(it.budgetCodeValue)
                opInLandBudgetLevelCode.setText(it.budgetLvlValue)

                /*  opInCrAccName.isFocusable = false
                    opInLandCrAccTaxNo.isFocusable = false */

                opInCrAccId.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        if (s.toString().length == 28) {
                            viewmodel.inputs.getBudgetAccountByIban(s.toString().trim())
                        }
                    }

                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {

                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {

                    }
                })
                viewmodel.inputs.getBudgetClassificationCode()
                viewmodel.inputs.getBudgetLevelCode()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.showGetBudgetClassificationCodeProgress().subscribe {
            showProgressImages(opInLandBudgetClassCodeProgress, it)
        }.addTo(subscriptions)

        viewmodel.outputs.showGetBudgetLevelCodeProgress().subscribe {
            showProgressImages(opInLandBudgetLevelCodeProgress, it)
        }.addTo(subscriptions)

        viewmodel.outputs.onGetBankListSuccess().subscribe {
            val adapter = GetBankListAdapter(this, it)

            crBankId.threshold = 1
            crBankId.setAdapter(adapter)
            crBankId.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                    opInLandBankIban.setText(it[position].bankTaxid)
                    opInLandCorrespondentAcc.setText(it[position].bankCorrAcc)
                    opInLandSwift.setText(it[position].bankSwift)

                    viewmodel.setSelectedBankCode(it[position].bankCode)

                    when {
                        it[position].bankCode == "210005" -> {
                            opInLandBudgetClassCodeContainer.visibility = View.VISIBLE
                            opInLandBudgetLevelCodeContainer.visibility = View.VISIBLE
                            opInLandAzipsSwitchContainer.visibility = View.GONE

                            /*opInCrAccName.isFocusable = false
                            opInLandCrAccTaxNo.isFocusable = false*/

                            opInCrAccId.addTextChangedListener(object : TextWatcher {
                                override fun afterTextChanged(s: Editable?) {
                                    if (s.toString().length == 28) {
                                        viewmodel.inputs.getBudgetAccountByIban(s.toString().trim())
                                    }
                                }

                                override fun beforeTextChanged(
                                    s: CharSequence?,
                                    start: Int,
                                    count: Int,
                                    after: Int
                                ) {

                                }

                                override fun onTextChanged(
                                    s: CharSequence?,
                                    start: Int,
                                    before: Int,
                                    count: Int
                                ) {

                                }
                            })
                            viewmodel.inputs.getBudgetClassificationCode()
                            viewmodel.inputs.getBudgetLevelCode()
                        }
                        it[position].bankCode == "210027" -> {
                            opInLandAzipsSwitchContainer.visibility = View.GONE
                            opInCrAccName.isFocusable = true
                            opInLandCrAccTaxNo.isFocusable = true
                            opInLandAzipsSwitchContainer.visibility = View.GONE
                        }
                        else -> {
                            opInCrAccName.isFocusable = true
                            opInLandCrAccTaxNo.isFocusable = true
                            opInLandBudgetClassCodeContainer.visibility = View.GONE
                            opInLandBudgetLevelCodeContainer.visibility = View.GONE
                            viewmodel.setSelectedBudgetLevelCode(null)
                            viewmodel.setSelectedBudgetClassCode(null)
                            opInLandAzipsSwitchContainer.visibility = View.VISIBLE
                        }
                    }
                  viewmodel.setSelectedBankCode(it[position].bankCode)
                }
        }.addTo(subscriptions)

        viewmodel.outputs.getBudgetClassificationCodeSuccess().subscribe { budgetCodeList ->

            opInLandBudgetClassCode.isFocusable = true
            val accountNameArray = arrayOfNulls<String>(budgetCodeList.size)

            for (i in 0 until budgetCodeList.size) {
                accountNameArray[i] = (budgetCodeList[i].budgetValue)
            }

            val adapter =
                ArrayAdapter<String>(this, android.R.layout.select_dialog_item, accountNameArray)
            opInLandBudgetClassCode.setAdapter(adapter)

            opInLandBudgetClassCode.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                     viewmodel.setSelectedBudgetClassCode(budgetCodeList[position].budgetId)
                }

        }.addTo(subscriptions)

        viewmodel.outputs.getBudgetLevelCodeSuccess().subscribe { budgetCodeList ->
            opInLandBudgetLevelCode.isFocusable = true

            val accountNameArray = arrayOfNulls<String>(budgetCodeList.size)

            for (i in 0 until budgetCodeList.size) {
                accountNameArray[i] = (budgetCodeList[i].budgetValue)
            }

            val adapter =
                ArrayAdapter<String>(this, android.R.layout.select_dialog_item, accountNameArray)
            opInLandBudgetLevelCode.setAdapter(adapter)

            opInLandBudgetLevelCode.onItemClickListener =
                AdapterView.OnItemClickListener { _, _, position, _ ->
                   viewmodel.setSelectedBudgetLevelCode(budgetCodeList[position].budgetId)
                }
        }.addTo(subscriptions)

        viewmodel.outputs.getBudgetAccountByIbanSuccess().subscribe {
            opInCrAccName.setText(it.name)
            opInLandCrAccTaxNo.setText(it.taxNumber)
        }.addTo(subscriptions)

        viewmodel.outputs.operationUpdated().subscribe {
            Toast.makeText(this, getString(R.string.transfer_successfully_updated), Toast.LENGTH_LONG).show()
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(subscriptions)
    }

    private fun buttonEnabled(show: Boolean) {
        if (!show) {
            submitButton.isFocusable = false
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
        } else {
            submitButton.isFocusable = true
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
    }
}
