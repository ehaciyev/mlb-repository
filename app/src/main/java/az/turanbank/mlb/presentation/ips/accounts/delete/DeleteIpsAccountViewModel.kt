package az.turanbank.mlb.presentation.ips.accounts.delete

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.IPSMLBAccountResponseModel
import az.turanbank.mlb.data.remote.model.response.DeleteIPSAccount
import az.turanbank.mlb.data.remote.model.response.DeleteIPSAccountResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.DeleteIpsAccountUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAccountsUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface DeleteIpsAccountViewModelInputs: BaseViewModelInputs{
    fun getAccounts()
    fun deleteAccount(accountId: ArrayList<Long>)
}
interface DeleteIpsAccountViewModelOutputs: BaseViewModelOutputs{
    fun onAccounts(): PublishSubject<ArrayList<IPSMLBAccountResponseModel>>
    fun accountDelete(): PublishSubject<DeleteIPSAccountResponseModel>
    fun unSuccessFullyDeletedAccount(): PublishSubject<ArrayList<DeleteIPSAccount>>
}
class DeleteIpsAccountViewModel @Inject constructor(
    private val getMLBAccountsUseCase: GetMLBAccountsUseCase,
    private val deleteIpsAccountUseCase: DeleteIpsAccountUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
        DeleteIpsAccountViewModelInputs,
        DeleteIpsAccountViewModelOutputs{
    override val inputs: DeleteIpsAccountViewModelInputs = this

    override val outputs: DeleteIpsAccountViewModelOutputs = this


    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    var unSuccessFullDeleteAccounts = PublishSubject.create<ArrayList<DeleteIPSAccount>>()

    private val accounts = PublishSubject.create<ArrayList<IPSMLBAccountResponseModel>>()
    private val accountDelete = PublishSubject.create<DeleteIPSAccountResponseModel>()
    override fun getAccounts() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        when(lang)
        {
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        getMLBAccountsUseCase
            .execute(custId, compId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    accounts.onNext(it.accounts)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun deleteAccount(accountId: ArrayList<Long>) {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        when(lang)
        {
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        deleteIpsAccountUseCase
            .execute(compId, custId, token, accountId, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    accountDelete.onNext(it)
                } else if(it.status.statusCode == 310){
                    var unDeleteAccounts = arrayListOf<DeleteIPSAccount>()
                    it.accountList.forEach {
                        if(it.status.statusCode !=1){
                            unDeleteAccounts.add(it)
                        }
                    }
                    unSuccessFullDeleteAccounts.onNext(unDeleteAccounts)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onAccounts() = accounts
    override fun accountDelete() = accountDelete
    override fun unSuccessFullyDeletedAccount() = unSuccessFullDeleteAccounts
}