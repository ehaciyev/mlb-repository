package az.turanbank.mlb.presentation.resources.deposit.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.resources.deposit.earlypayedamounts.EarlyPayedAmountsActivity
import az.turanbank.mlb.presentation.resources.deposit.payedamounts.PayedAmountsActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_deposit.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import javax.inject.Inject

class DepositActivity : BaseActivity(){
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: DepositViewModel

    private val symbols = DecimalFormatSymbols()
    private val decimalFormat = DecimalFormat("0.00")
    private val depositId: Long by lazy { intent.getLongExtra("depositId", 0L) }
    private var currencyText = "AZN"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposit)

        toolbar_title.text = ""
        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = '.'
        decimalFormat.decimalFormatSymbols = symbols
        viewModel =
            ViewModelProvider(this, factory)[DepositViewModel::class.java]
        setInputListeners()
        setOutputListeners()
        changeToolbar()
    }

    private fun changeToolbar() {
        toolbar_back_button.setOnClickListener { onBackPressed() }

    }
    @SuppressLint("SetTextI18n")
    private fun setOutputListeners() {
        viewModel.outputs.onDepositSuccess().subscribe {

            this.currencyText = it.currency
            plasticCardAmountInteger.text = firstPartOfMoney(it.amount)
            plasticCardAmountReminder.text = secondPartOfMoney(it.amount) + it.currency

            plasticContractNumber.text = it.iban
            toolbar_title.text = getString(R.string.deposit) + "(" + it.currency +")"
            clientName.text = it.holder
            branchName.text = it.branchName
            depositType.text = it.name
            depositAmountInteger.text = firstPartOfMoney(it.amount)
            depositAmountReminder.text = secondPartOfMoney(it.amount)
            depositPercent.text ="%"+ it.yearPercent.toString()
            currency.text = it.currency
            paymentFrequency.text = it.paymentFrequency
            openDate.text = it.openDate
            closeDate.text = it.endDate
            iban.text = it.iban
            stopAnimation()
        }.addTo(subscriptions)
        viewModel.outputs.onDepositCardSuccess().subscribe {
            depositCard.text = it
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }
    private fun stopAnimation(){
        progress.clearAnimation()
        progress.visibility = View.GONE
        constraintContainer.visibility = View.VISIBLE
        plasticCardInclude.visibility = View.VISIBLE
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
    private fun firstPartOfMoney(amount: Double) = decimalFormat.format(amount).toString().split(".")[0] +","
    private fun secondPartOfMoney(amount: Double) = decimalFormat.format(amount).toString().split(".")[1] +" "
    private fun setInputListeners() {
        animateProgressImage()
        constraintContainer.visibility = View.GONE
        plasticCardInclude.visibility = View.GONE
        viewModel.inputs.getTheDeposit(depositId)
        payedAmounts.setOnClickListener {
            val intent = Intent(this, PayedAmountsActivity::class.java)
            intent.putExtra("depositId", depositId)
            intent.putExtra("currency", currencyText)
            startActivity(intent)
        }
        earlyPayedAmounts.setOnClickListener {
            val intent = Intent(this, EarlyPayedAmountsActivity::class.java)
            intent.putExtra("depositId", depositId)
            startActivity(intent) }
    }
}