package az.turanbank.mlb.presentation.resources.account

import android.content.Context
import android.content.SharedPreferences
import az.turanbank.mlb.presentation.base.BaseViewModel
import javax.inject.Inject

class AccountTransfersActivityViewModel  @Inject constructor(private val context: Context,
                                                             private val sharedPrefs: SharedPreferences
): BaseViewModel() {
}