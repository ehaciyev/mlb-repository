package az.turanbank.mlb.presentation.login.activities.pin

import az.turanbank.mlb.domain.user.data.MainRepository
import javax.inject.Inject

class CheckInternetConnectionUseCase @Inject constructor(
    private val repository: MainRepository
){
    fun execute() = repository.checkInternetConnection()
}
