package az.turanbank.mlb.presentation.resources.loan.pay

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.resources.account.AccountOperationInternalResultActivity
import az.turanbank.mlb.presentation.resources.account.AccountOperationInternalVerifyActivity
import az.turanbank.mlb.presentation.resources.loan.pay.choose.ChoosePayTypeActivity
import az.turanbank.mlb.presentation.resources.loan.pay.success.SuccessLoanPaymentActivity
import az.turanbank.mlb.presentation.resources.loan.pay.verify.VerifyLoanPaymentActivity
import kotlinx.android.synthetic.main.activity_pay_loan.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import javax.inject.Inject

class PayLoanActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: PayLoanViewModel
    private val loanId: Long by lazy { intent.getLongExtra("loanId", 0L) }
    private val payForMonth: Double by lazy { intent.getDoubleExtra("payForMonth", 0.00) }
    private val rest: Double by lazy { intent.getDoubleExtra("rest", 0.00) }
    private val currencyText: String by lazy { intent.getStringExtra("currency") }
    private val loanType: String by lazy { intent.getStringExtra("loanType") }
    private val loanIban: String by lazy { intent.getStringExtra("loanIban") }

    private var accountId = 0L
    private var payType = 0

    private val decimalFormat = DecimalFormat("0.00")
    private val symbols = DecimalFormatSymbols()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay_loan)

        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = '.'
        decimalFormat.decimalFormatSymbols = symbols

        viewModel =
            ViewModelProvider(this, factory)[PayLoanViewModel::class.java]

        initToolbarAndDebts()

        setOutputListeners()
        setInputListeners()


    }

    private fun setOutputListeners() {

    }

    private fun setInputListeners() {
        dtAccountIdContainer.setOnClickListener {
            startActivityForResult(
                Intent(
                    this,
                    ChoosePayTypeActivity::class.java
                ), 12
            )
        }
        submitButton.setOnClickListener {
            if (dtAccountId.text.toString() != "Ödəmək") {
                if (amount.text.toString().trim().isNotEmpty() && amount.text.toString().toDouble() > 0.0) {
                    if(payType == 1) {
                        val intent = Intent(this, VerifyLoanPaymentActivity::class.java)
                        intent.putExtra("dtAccountId", accountId)
                        intent.putExtra("amount", amount.text.toString().toDouble())
                        intent.putExtra("payType", payType)
                        intent.putExtra("loanId", loanId)
                        intent.putExtra("currency", currency.text.toString())
                        intent.putExtra("account", dtAccountId.text.toString())
                        intent.putExtra("loanType", loanType)
                        startActivityForResult(intent, 11)
                    } else if (payType == 2){
                        val intent = Intent(this, AccountOperationInternalVerifyActivity::class.java)
                        intent.putExtra("dtAccountId", accountId)
                        intent.putExtra("dtIban", dtAccountId.text.toString())
                        intent.putExtra("crIban", loanIban)
                        intent.putExtra("amount", amount.text.toString())
                        intent.putExtra("currency", currency.text.toString())
                        intent.putExtra("purpose", getString(R.string.credit_payment))
                        intent.putExtra("accountName", loanType)
                        if (viewModel.getTaxNo() != "")
                            intent.putExtra("taxNo", viewModel.getTaxNo())

                        startActivityForResult(intent, 7)
                    }
                } else {
                    showAlertDialog(getString(R.string.enter_amount_please))
                }
            } else {
                showAlertDialog(getString(R.string.enter_payment_method_please))
            }
        }
    }

    private fun showAlertDialog(text: String) {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.error_message))
            .setMessage(text)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .setCancelable(true)
            .show()
    }

    @SuppressLint("SetTextI18n")
    private fun initToolbarAndDebts() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            home_screen_toolbar.setTitleTextColor(getColor(R.color.white))
            home_screen_toolbar.setSubtitleTextColor(getColor(R.color.white))
        }
        setSupportActionBar(home_screen_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.credit_payment)

        restInteger.text = firstPartOfMoney(rest)
        restReminder.text = secondPartOfMoney(rest) + currencyText

        monthlyInteger.text = firstPartOfMoney(payForMonth)
        monthlyReminder.text = secondPartOfMoney(payForMonth) + currencyText

        amount.setText(payForMonth.toString())

    }

    private fun firstPartOfMoney(amount: Double) =
        decimalFormat.format(amount).toString().split(".")[0] + ","

    private fun secondPartOfMoney(amount: Double) =
        decimalFormat.format(amount).toString().split(".")[1] + " "

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 12) {
            if (resultCode == Activity.RESULT_OK) {
                dtAccountId.text = data?.getStringExtra("dtNumber")
                currency.text = data?.getStringExtra("currency")
                payType = data?.getIntExtra("payType", 0)!!
                accountId = data.getLongExtra("dtAccountId", 0L)
            }
        }
        if (requestCode == 11) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent (this, SuccessLoanPaymentActivity::class.java)
                intent.putExtra("loanResponse", data?.getSerializableExtra("loanResponse"))

                startActivity(intent)
                finish()
            }
        }
        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent(this, AccountOperationInternalResultActivity::class.java)

                intent.putExtra(
                    "dtAccountId",
                    accountId
                )
                intent.putExtra("crIban", loanIban)
                intent.putExtra("dtIban", dtAccountId.text.toString())
                intent.putExtra("amount", amount.text.toString())
                intent.putExtra("accountName", loanType)
                intent.putExtra("operationNo", data?.getStringExtra("operationNo"))
                intent.putExtra("accountResponse", data?.getSerializableExtra("accountResponse"))
                intent.putExtra("operationStatus", data?.getStringExtra("operationStatus"))
                if (!intent.getStringExtra("taxNo").isNullOrEmpty()) {
                    intent.putExtra("taxNo", viewModel.getTaxNo())
                }
                startActivity(intent)
                finish()
            }
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
