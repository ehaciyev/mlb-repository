package az.turanbank.mlb.presentation.ips.outgoing

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.OperationModel
import kotlinx.android.synthetic.main.ips_payments_list_view.view.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class IPSPaymentsAdapter(
    private val payments: ArrayList<OperationModel>,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<IPSPaymentsAdapter.IPSPaymentViewHolder>() {

    class IPSPaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(payment: OperationModel, clickListener: (position: Int) -> Unit){

            itemView.setOnClickListener { clickListener.invoke(adapterPosition) }

            val decimalFormat = DecimalFormat("0.00")
            decimalFormat.roundingMode = RoundingMode.HALF_EVEN
            val symbols = DecimalFormatSymbols()
            symbols.decimalSeparator = '.'
            decimalFormat.decimalFormatSymbols = symbols

            itemView.opHistoryState.text = payment.operState
            itemView.amountInteger.text =
                "${decimalFormat.format(payment.amount).toString().split(".")[0]},"
            itemView.amountReminder.text =
                decimalFormat.format(payment.amount).toString().split(".")[1] + " " + payment.currency
            itemView.opHistoryPurpose.text = payment.operPurpose
            itemView.opHistoryName.text = payment.operName

            when(payment.operStateId) {
                1,2 -> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_1_or_2))
                }
                6,7 -> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_6_or_7))
                }
                3,4 -> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_3_or_4))
                }
                5 -> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_5))
                }
                8-> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_8))
                }
            }
        }
    }

    fun getItemAt(position: Int) = payments[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        IPSPaymentViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.ips_payments_list_view,
                parent,
                false
            )
        )

    override fun getItemCount() = payments.size

    override fun onBindViewHolder(holder: IPSPaymentViewHolder, position: Int) {
        holder.bind(payments[position], onClickListener)
    }
}