package az.turanbank.mlb.presentation.data

import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel

data class SelectAccountModel(
    val account: AccountListModel,
    var checked: Boolean
) {
    override fun toString(): String {
        return "SelectAccountModel(account=$account, checked=$checked)"
    }
}