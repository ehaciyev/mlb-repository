package az.turanbank.mlb.presentation.ips.accounts.setdefault.account

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.ips.aliases.SetDefaultUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import javax.inject.Inject

interface ChooseAccountForDefaultViewModelInputs : BaseViewModelInputs {
    fun setDefault(compoundId: Long)
}

interface ChooseAccountForDefaultViewModelOutputs : BaseViewModelOutputs {
    fun onDefaultCompleted(): CompletableSubject
}

class ChooseAccountForDefaultViewModel @Inject constructor(
    private val setDefaultUseCase: SetDefaultUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    ChooseAccountForDefaultViewModelInputs,
    ChooseAccountForDefaultViewModelOutputs {
    override val inputs: ChooseAccountForDefaultViewModelInputs = this

    override val outputs: ChooseAccountForDefaultViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)

    private val setCompleted = CompletableSubject.create()

    override fun setDefault(compoundId: Long) {
        setDefaultUseCase
            .execute(custId, compoundId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    setCompleted.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onDefaultCompleted() = setCompleted
}