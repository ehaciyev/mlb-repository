package az.turanbank.mlb.presentation.resources.account

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.widget.ActionMenuView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.activity.SuccessfulPageViewActivity
import az.turanbank.mlb.presentation.activity.WarningPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_authorization.*
import kotlinx.android.synthetic.main.auth_activity_toolbar.*
import javax.inject.Inject

class AuthorizationMultiDocsActivity : BaseActivity() {

    lateinit var amvMenu: ActionMenuView

    lateinit var viewModel: AuthMultiDocsViewModel

    lateinit var adapter: AuthorizationMultiSelectRecyclerViewAdapter

    lateinit var menuBuilder: MenuBuilder

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var dialog: MlbProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorization)

        dialog = MlbProgressDialog(this)

        viewModel = ViewModelProvider(this, factory)[AuthMultiDocsViewModel::class.java]

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        authOpRecyclerView.layoutManager = llm

        adapter =
            AuthorizationMultiSelectRecyclerViewAdapter(this, arrayListOf()) { selectedItemCount ->
                if (selectedItemCount > 0) {
                    authOperationSelectedItemCount.visibility = View.VISIBLE
                    authOperationSelectedItemCount.text = selectedItemCount.toString()
                    authOperationDeselectAll.visibility = View.VISIBLE
                    toolbar_back_button.visibility = View.GONE
                } else {
                    authOperationSelectedItemCount.visibility = View.GONE
                    authOperationSelectedItemCount.text = selectedItemCount.toString()
                    authOperationDeselectAll.visibility = View.GONE
                    toolbar_back_button.visibility = View.VISIBLE
                }
            }

        authOperationDeselectAll.setOnClickListener {
           // adapter.setData(arrayListOf())
            adapter.deselectAll()
            authOperationSelectedItemCount.visibility = View.GONE
        }

        authOpRecyclerView.adapter = adapter

        val toolbar = findViewById<Toolbar>(R.id.authactivity_toolbar)
        amvMenu = toolbar.findViewById(R.id.amvMenu)
        setSupportActionBar(toolbar)
        menuBuilder = amvMenu.menu as MenuBuilder

        toolbar_back_button.setOnClickListener { onBackPressed() }

        setOutputListeners()
        setInputListeners()
    }

    private fun setOutputListeners() {

        viewModel.outputs.onOperationNotFound().subscribe {
            if (it) {
                adapter.setData(arrayListOf())
                (authOpRecyclerView.adapter as AuthorizationMultiSelectRecyclerViewAdapter).deselectAll()
            }
        }.addTo(subscriptions)

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.getAuthOperationListSuccess().subscribe {
            adapter.setData(it)
            (authOpRecyclerView.adapter as AuthorizationMultiSelectRecyclerViewAdapter).deselectAll()

            menuBuilder.setCallback(object : MenuBuilder.Callback {
                override fun onMenuItemSelected(menu: MenuBuilder?, item: MenuItem?): Boolean {
                    return onOptionsItemSelected(item!!)
                }

                override fun onMenuModeChange(menu: MenuBuilder?) {
                }
            })

        }.addTo(subscriptions)

        viewModel.outputs.getSignInfoSuccess().subscribe { getSignInfoSuccess ->
            val intent = Intent(this@AuthorizationMultiDocsActivity, SignFileActivity::class.java)

            intent.putExtra("successOperId",getSignInfoSuccess.successOperId)
            intent.putExtra("failOperId",getSignInfoSuccess.failOperId)
            intent.putExtra("verificationCode", getSignInfoSuccess.verificationCode)
            intent.putExtra("transactionId", getSignInfoSuccess.transactionId)
            intent.putExtra("operationIdsJson", adapter.getAllSelectedItems())

            startActivityForResult(intent, 8)

        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
               dialog.hideDialog()
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.getAuthOperationList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.auth_activity_menu, amvMenu.menu)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 8) {

            viewModel.inputs.getAuthOperationList()

            when (resultCode) {
                Activity.RESULT_OK -> {
                    val intent = Intent(this, SuccessfulPageViewActivity::class.java)
                    intent.putExtra("description", getString(R.string.docs_sign_success))
                    intent.putExtra("finishOnly", true)
                    startActivity(intent)
                }
                Activity.RESULT_FIRST_USER -> {
                    val intent = Intent(this, WarningPageViewActivity::class.java)
                    intent.putExtra("description", data?.getStringExtra("description"))
                    startActivity(intent)
                }
                Activity.RESULT_CANCELED -> {
                    val intent = Intent(this, ErrorPageViewActivity::class.java)
                    intent.putExtra("description", getString(R.string.docs_sign_fail))
                    startActivity(intent)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_select_all -> {
                adapter.setAllSelected()
                true
            }
            R.id.sign_docs -> {
                if (adapter.getAllSelectedItems().size > 0) {
                    viewModel.inputs.getSignInfo(adapter.getAllSelectedItems())
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
