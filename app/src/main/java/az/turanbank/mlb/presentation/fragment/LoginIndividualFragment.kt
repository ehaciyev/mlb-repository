package az.turanbank.mlb.presentation.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.login.asan.individual.LoginAsanIndividualActivity
import az.turanbank.mlb.presentation.login.username.LoginUsernamePasswordActivity

class LoginIndividualFragment : Fragment() {
    private lateinit var loginUsername: CardView
    private val intentUsername by lazy {
        Intent(requireActivity(), LoginUsernamePasswordActivity::class.java)
    }
    private lateinit var loginAsan: CardView
    private val intentAsan by lazy {
        Intent(requireActivity(), LoginAsanIndividualActivity::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login_individual, container, false)
        loginUsername = view.findViewById(R.id.withUsername)
        loginAsan = view.findViewById(R.id.withAsan)

        intentUsername.putExtra("customerType", "individual")
        handleListeners()
        return view
    }

    private fun handleListeners() {
        loginUsername.setOnClickListener { startActivity(intentUsername) }
        loginAsan.setOnClickListener { startActivity(intentAsan) }
    }
}
