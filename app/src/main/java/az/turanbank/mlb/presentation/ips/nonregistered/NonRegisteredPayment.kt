package az.turanbank.mlb.presentation.ips.nonregistered

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.NonRegisteredIPSTransferActivity
import az.turanbank.mlb.presentation.ips.nonregistered.pay.debitor.NonRegisteredIPSAccountChoiceActivity
import az.turanbank.mlb.presentation.ips.outgoing.MyIncomePaymentsActivity
import az.turanbank.mlb.presentation.ips.paidToMe.PaidToMeActivity
import kotlinx.android.synthetic.main.activity_non_registered_payment.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class NonRegisteredPayment : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: NonRegisteredPaymentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_non_registered_payment)

        viewModel = ViewModelProvider(this,factory)[NonRegisteredPaymentViewModel::class.java]

        toolbar_title.text = getString(R.string.instant_payments)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        payRequestContainer.setOnClickListener{
            startActivityForResult(Intent(this, NonRegisteredIPSAccountChoiceActivity::class.java),7)
        }


        myPaymentsContainer.setOnClickListener {
            startActivityForResult(Intent(this, MyIncomePaymentsActivity::class.java),7)
        }

        paidToMeContainer.setOnClickListener {
            startActivityForResult(Intent(this, PaidToMeActivity::class.java),7)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}