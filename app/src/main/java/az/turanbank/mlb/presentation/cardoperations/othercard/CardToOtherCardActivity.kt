package az.turanbank.mlb.presentation.cardoperations.othercard

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.cardoperations.cardtocard.SuccessCardToCardActivity
import az.turanbank.mlb.presentation.cardoperations.cardtocard.VerifyCardToCardActivity
import az.turanbank.mlb.util.formatDecimal
import com.facebook.shimmer.ShimmerFrameLayout
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_transfer_to_another_bank.*
import java.util.*
import javax.inject.Inject

class CardToOtherCardActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardToOtherCardViewModel

    private var fromCardModel = arrayListOf<CardListModel>()

    private var fromCardList = arrayListOf<String>()

    private var currencies = arrayListOf<String>()

    private var debitorPosition = 0

    private val fromTemplate: Boolean by lazy { intent.getBooleanExtra("fromTemplate", false) }

    lateinit var cardToOtherShimmer: ShimmerFrameLayout

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfer_to_another_bank)
        viewModel =
            ViewModelProvider(this, factory)[CardToOtherCardViewModel::class.java]

        cardToOtherShimmer = findViewById(R.id.cashByCodeShimmer)

        setInputListeners()
        setOutputListeners()


        toolbar_title.text = getString(R.string.to_other_card)

        toolbar_back_button.setOnClickListener { onBackPressed() }
    }

    private fun checkFromHomeMenu() {
        val cardId = intent.getLongExtra("cardId", 0L)
        if (cardId != 0L) {
            for (i in 0 until fromCardModel.size) {
                if (fromCardModel[i].cardId == cardId) {
                    fromCardTextView.text = fromCardList[i]
                    debitorPosition = i
                    break
                }
            }
        }
    }

    private fun checkTemplateOrNot() {
        if (fromTemplate) {
            fillBlanksFromTemplate()
            deleteTemp.visibility = View.VISIBLE
        } else {
            deleteTemp.visibility = View.GONE
        }
    }

    private fun showDeleteTemp(tempId: Long) {

        val alert = AlertDialog.Builder(this)
        alert.setMessage(getString(R.string.template_delete_confirmation_message))

        alert.setCancelable(true)
        alert.setPositiveButton(
            getString(R.string.yes)
        ) { _, _ ->
            animateProgressImage()
            viewModel.inputs.deleteTemp(tempId)
        }

        alert.setNegativeButton(
            getString(R.string.no)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }

    private fun fillBlanksFromTemplate() {

        deleteTemp.setOnClickListener { showDeleteTemp(intent.getLongExtra("tempId", 0L)) }

        val fromCardVar: String by lazy { intent.getStringExtra("fromCard") }
        val toCardOrMobile: String by lazy { intent.getStringExtra("toCardOrMobile") }
        val amountVar: Double by lazy { intent.getDoubleExtra("amount", 0.00) }
        val currency: String by lazy { intent.getStringExtra("currency") }
        for (i in 0 until fromCardModel.size) {
            if (fromCardModel[i].cardNumber == fromCardVar) {
                fromCardTextView.text = fromCardList[i]
                debitorPosition = i
                break
            }
        }
        toCard.setText(toCardOrMobile)
        amount.setText(amountVar.toString())

        for (i in 0 until currencies.size) {
            if (currencies[i] == currency) {
                currencySpinner.setSelection(i)
                break
            }
        }

    }

    private fun setInputListeners() {
        //animateProgressImage()
        mainView.visibility = View.GONE

        viewModel.inputs.getCardList()

        submitButton.setOnClickListener {
            if (toCard.text.toString().trim().isNotEmpty() && toCard.text.toString().trim().length == 16) {
                if (!TextUtils.isEmpty(amount.text.toString())&&(amount.text.toString().toDouble() >0.00)) {
                    showProgressBar(true)
                    val intent = Intent(this, VerifyCardToCardActivity::class.java)
                    intent.putExtra(
                        "fromCard",
                        fromCardModel[debitorPosition].cardNumber.replaceRange(
                            4,
                            12,
                            "********"
                        )
                    )
                    intent.putExtra("currency", currencySpinner.selectedItem?.toString())
                    intent.putExtra("amount", amount.text.toString())

                    intent.putExtra(
                        "fromCardId",
                        fromCardModel[debitorPosition].cardId
                    )
                    intent.putExtra("toCard", toCard.text.toString())
                    intent.putExtra("amountToSend", amount.text?.toString()?.toDouble())
                    intent.putExtra("root", "otherCard")

                    startActivityForResult(intent, 7)
                    showProgressBar(false)

                } else {
                    AlertDialogMapper(this, 124003).showAlertDialog()
                }
            } else {
                android.app.AlertDialog.Builder(this)
                    .setTitle(getString(R.string.error_message))
                    .setMessage(getString(R.string.card_number_is_not_valid))
                    .setPositiveButton(R.string.ok) { _, _ -> }
                    .setCancelable(true)
                    .show()
            }
        }

        viewModel.outputs
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent(this, SuccessCardToCardActivity::class.java)
                intent.putExtra("receiptNo", data?.getStringExtra("receiptNo"))
                intent.putExtra("operationDate", data?.getStringExtra("operationDate"))
                intent.putExtra("dtCardNumber", data?.getStringExtra("dtCardNumber"))
                intent.putExtra("amount", data?.getDoubleExtra("amount", 0.00))
                intent.putExtra("feeAmount", data?.getDoubleExtra("feeAmount", 0.00))
                intent.putExtra("operationName", data?.getStringExtra("operationName"))
                intent.putExtra("note", data?.getStringExtra("note"))
                intent.putExtra("currency", data?.getStringExtra("currency"))
                intent.putExtra("requestorCardNumber", data?.getStringExtra("requestorCardNumber"))
                intent.putExtra("root", "otherCard")


                startActivityForResult(intent, 12)
            }
        }
        if (requestCode == 12) {
            if (resultCode == Activity.RESULT_OK) {
                setResult(Activity.RESULT_OK)
                finish()
            }
            if (resultCode == Activity.RESULT_FIRST_USER) {
                finish()
            }
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.templateDeleted().subscribe {
            Toast.makeText(this, getString(R.string.template_successfully_deleted), Toast.LENGTH_LONG).show()
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.cardListSuccess().subscribe {

            cardToOtherShimmer.visibility = View.GONE
            cardToOtherShimmer.stopShimmerAnimation()
            mainView.visibility = View.VISIBLE

            val filteredList = arrayListOf<CardListModel>()
            it.forEach { card ->
                if (card.cardStatus != "0") {
                    filteredList.add(card)
                }
            }
            attachListsToSpinners(filteredList)
        }.addTo(subscriptions)

        viewModel.outputs.currencyListSuccess().subscribe {

            cardToOtherShimmer.visibility = View.GONE
            cardToOtherShimmer.stopShimmerAnimation()
            mainView.visibility = View.VISIBLE

            currencies.clear()
            currencies.addAll(it)
            val currencyAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, it)
            currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            currencySpinner.adapter = currencyAdapter
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            //showProgressBar(false)

            cardToOtherShimmer.visibility = View.GONE
            cardToOtherShimmer.stopShimmerAnimation()
            mainView.visibility = View.GONE

            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)

        viewModel.outputs.currencyAndCardSuccess().subscribe {
            if (it) {
                mainProgress.clearAnimation()
                mainProgress.visibility = View.GONE
                mainView.visibility = View.VISIBLE
                cardToOtherShimmer.visibility = View.GONE
                cardToOtherShimmer.stopShimmerAnimation()
                checkFromHomeMenu()
                checkTemplateOrNot()
            } else {
                animateProgressImage()
                mainView.visibility = View.GONE
            }
        }.addTo(subscriptions)
    }

    private fun attachListsToSpinners(cardList: ArrayList<CardListModel>) {
        fromCardModel.clear()
        fromCardModel.addAll(cardList)

        fromCardList.clear()


        for (i in 0 until cardList.size) {
            fromCardList.add(
                cardList[i].cardNumber.replaceRange(4, 12, "********") + " / " +
                        cardList[i].balance.formatDecimal() + " " + cardList[i].currency
            )
        }

        debitorCardToOtherContainer.setOnClickListener {
            val builder: android.app.AlertDialog.Builder =
                android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.depitor_card))

            val fromList = arrayOfNulls<String>(fromCardList.size)

            for (i in 0 until fromCardList.size) {
                fromList[i] = fromCardList[i]
            }
            builder.setItems(fromList) { _, which ->
                fromCardTextView.text = fromCardList[which]
                debitorPosition = which
            }
            val dialog = builder.create()
            dialog.show()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        debitorCardToOtherContainer.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateSubmitImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.t_sd_ql)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateSubmitImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun animateProgressImage() {
        mainProgress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        mainProgress.startAnimation(rotate)
    }

    override fun onResume() {
        super.onResume()
        cardToOtherShimmer.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        cardToOtherShimmer.stopShimmerAnimation()
    }

}
