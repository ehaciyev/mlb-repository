package az.turanbank.mlb.presentation.resources.card.sms

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.annotation.RequiresApi
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_sms.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class SmsActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: SmsViewModel
    private val cardId: Long by lazy { intent.getLongExtra("cardId", 0L) }
    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sms)

        viewModel = ViewModelProvider(this, factory)[SmsViewModel::class.java]

        setInputListeners()
        setOutputListeners()


        toolbar_title.text = getString(R.string.sms_service)

        toolbar_back_button.setOnClickListener { onBackPressed() }
    }

    override fun onResume() {
        super.onResume()
        viewModel.inputs.getNumber(cardId)
        animateProgressImage()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setOutputListeners() {
        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            smsBackground.visibility = View.GONE
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
        viewModel.outputs.onNumberNotFound().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            smsBackground.visibility = View.VISIBLE
            if (it) {
                addNumber.visibility = View.VISIBLE
                plus.visibility = View.VISIBLE
                numberCard.visibility = View.GONE
                smsBackground.background = getDrawable(R.drawable.sms_empty_background)
            } else {
                numberCard.visibility = View.VISIBLE
                smsBackground.background = getDrawable(R.color.transparent)
                addNumber.visibility = View.GONE
                plus.visibility = View.GONE
            }
        }.addTo(subscriptions)
        viewModel.outputs.onNumberSuccess().subscribe {
            number.text = it
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        smsBackground.setOnClickListener {
            val intent = Intent(this, AddSmsNumberActivity::class.java)
            intent.putExtra("cardId", cardId)
            intent.putExtra("edit", false)
            startActivityForResult(intent, 10)
        }
        numberCard.setOnClickListener {
            val intent = Intent(this, DeleteOrUpdateSmsNotificationActivity::class.java)
            intent.putExtra("number", number.text.toString())
            intent.putExtra("cardId", cardId)
            startActivity(intent)
        }
        animateProgressImage()
        viewModel.inputs.getNumber(cardId)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 10){
            if(resultCode==Activity.RESULT_OK){
                animateProgressImage()
                viewModel.inputs.getNumber(cardId)
            }
        }
    }
    private fun animateProgressImage() {
        smsBackground.visibility = View.GONE
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }

}
