package az.turanbank.mlb.presentation.payments.pay.check.custom

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.ChildInvoiceResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.InvoiceResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.payments.pay.check.CheckPaymentActivity
import kotlinx.android.synthetic.main.activity_choose_custom_invoice_child.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class ChooseInvoiceWithSubChildActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModelWithSub: ChooseInvoiceWithSubChildActivityViewModel

    private val invoice: InvoiceResponseModel by lazy { intent.getSerializableExtra("invoice") as InvoiceResponseModel }
    private val respChildInvoice: ArrayList<ChildInvoiceResponseModel> by lazy { intent.getSerializableExtra("respChildInvoice") as ArrayList<ChildInvoiceResponseModel> }


   /* private val serviceName: String by lazy { intent.getStringExtra("serviceName") }
    private val serviceCode: Long by lazy { intent.getLongExtra("serviceCode",0L) }
    private val fullName: String by lazy { intent.getStringExtra("fullName") }
    private val transactionNumber: String by lazy { intent.getStringExtra("transactionNumber") }
    private val payerCode: String by lazy { intent.getStringExtra("payerCode") }*/


    private val merchantDisplayName: String by lazy { intent.getStringExtra("merchantDisplayName") }
    private val providerId: Long by lazy { intent.getLongExtra("providerId", 0L) }
    private val merchantId: Long by lazy { intent.getLongExtra("merchantId", 0L) }
    private val merchantName: String by lazy { intent.getStringExtra("merchantName") }
    private val categoryId: Long by lazy { intent.getLongExtra("categoryId", 0L) }
    private val categoryName: String by lazy { intent.getStringExtra("categoryName") }
    private val identificationCode: ArrayList<IdentificationCodeRequestModel> by lazy { intent.getSerializableExtra("identificationCode") as ArrayList<IdentificationCodeRequestModel> }
    private val identificationType: String by lazy { intent.getStringExtra("identificationType") }
    private val transactionId: String by lazy { intent.getStringExtra("transactionId") }
    private val transactionNumber: String by lazy { intent.getStringExtra("transactionNumber") }

    private val serviceCodeUser: String by lazy { intent.getStringExtra("serviceCodeUser") }

    lateinit var recyclerViewChildContainer: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_custom_invoice_child)

        viewModelWithSub = ViewModelProvider(this, factory)[ChooseInvoiceWithSubChildActivityViewModel::class.java]

        toolbar_title.text = merchantDisplayName
        toolbar_back_button.setOnClickListener { onBackPressed() }

        subscriberName.text = invoice.fullName

        recyclerViewChildContainer = findViewById(R.id.recyclerViewChildContainer)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerViewChildContainer.layoutManager = llm
        val adapter =
            ChooseInvoiceWithSubChildAdapter(this, respChildInvoice) {

            }
        recyclerViewChildContainer.adapter = adapter

        submitButton.setOnClickListener {
            startActivityForResult(
                Intent(this, CheckPaymentActivity::class.java)
                    .putExtra("merchantDisplayName",merchantDisplayName)
                    .putExtra("isInvoice", true)
                    .putExtra("providerId", providerId)
                    .putExtra("invoice", invoice)
                    .putExtra("transactionId",transactionId)
                    .putExtra("transactionNumber",transactionNumber)
                    .putExtra("merchantId", merchantId)
                    .putExtra("merchantName", merchantName)
                    .putExtra("categoryId", categoryId)
                    .putExtra("categoryName", categoryName)
                    .putExtra("identificationCode", identificationCode)
                    .putExtra("identificationType", identificationType)
                    .putExtra("serviceCodeUser",serviceCodeUser)
                    .putExtra("isRespChildInvoice",true)
                    .putExtra("respChildInvoice",respChildInvoice),7
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}
