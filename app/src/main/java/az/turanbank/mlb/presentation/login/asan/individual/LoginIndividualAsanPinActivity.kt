package az.turanbank.mlb.presentation.login.asan.individual

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.home.MlbHomeActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_individual_register_asan_pin.*
import javax.inject.Inject

class LoginIndividualAsanPinActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewmodel: LoginIndividualAsanPinViewModel

    private val transactionId by lazy { intent.getLongExtra("transactionId", 0L) }
    private val challenge: String by lazy { intent.getStringExtra("challenge") }
    private val certificate: String by lazy { intent.getStringExtra("certificate") }
    private val pin: String by lazy { intent.getStringExtra("pin") }
    private val custId: Long by lazy { intent.getLongExtra("custId", 0L )}

    private val phoneNumber: String by lazy { intent.getStringExtra("phoneNumber") }
    private val asanId: String by lazy { intent.getStringExtra("asanId") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual_register_asan_pin)

        verificationCode.setText(intent.getStringExtra("verificationCode"))

        viewmodel = ViewModelProvider(
            this,
            viewModelFactory
        )[LoginIndividualAsanPinViewModel::class.java]

        showProgressBar(true)

        setupInputListeners()
        setupOutputListeners()
    }

    private fun setupOutputListeners() {
        viewmodel.outputs.onLogiIndividualAsan()
            .subscribe {
                viewmodel.inputs.storeUserPhone(phoneNumber)
                viewmodel.inputs.storeUserId(asanId)

                val intent = Intent(this, MlbHomeActivity::class.java)
                intent.putExtra("phoneNumber", phoneNumber)
                intent.putExtra("pin", pin)
                intent.putExtra("asanId", asanId)
                intent.putExtra("loggedInWithAsan", true)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }.addTo(subscriptions)

        viewmodel.outputs.onError()
            .subscribe {
              //  AlertDialogMapper(this, it).showAlertDialog()
                val intent = Intent(this, ErrorPageViewActivity::class.java)
                intent.putExtra("description", getString(ErrorMessageMapper().getErrorMessage(it)))
                startActivity(intent)
                finish()
            }.addTo(subscriptions)
    }

    private fun setupInputListeners() {
        viewmodel.inputs.loginIndividualForAsan(phoneNumber, asanId, transactionId, certificate, challenge, custId, Build.MODEL, "Azerbaijan")
    }

    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}