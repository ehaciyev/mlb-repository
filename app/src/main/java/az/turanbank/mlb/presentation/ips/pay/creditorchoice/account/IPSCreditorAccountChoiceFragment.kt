package az.turanbank.mlb.presentation.ips.pay.creditorchoice.account

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.BankListAdapter
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.moreinfo.AccountAndCustomerInfoActivity
import az.turanbank.mlb.util.animateProgressImage
import az.turanbank.mlb.util.revertProgressImageAnimation
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_ipscreditor_account_choice.*
import javax.inject.Inject


class IPSCreditorAccountChoiceFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: IPSCreditorAccountChoiceViewModel

    private lateinit var bankContainer: LinearLayout
    private lateinit var accountContainer: LinearLayout

    private lateinit var progressImage: ImageView
    private lateinit var submitText: TextView
    lateinit var submitButton: LinearLayout

    lateinit var bank: AutoCompleteTextView
    lateinit var account: AppCompatEditText

    private var receiverName: String? = null
    private var receiverSurname: String? = null
    private var taxNumber: String? = null

    var selectedBankId = 0L

    private val senderFullName: String by lazy { requireActivity().intent.getStringExtra("senderFullName") }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_ipscreditor_account_choice, container, false)

        viewModel =
            ViewModelProvider(this, factory)[IPSCreditorAccountChoiceViewModel::class.java]

        initViews(view)

        setOutputListeners()
        setInputListeners()

        return view
    }

    private fun setOutputListeners() {
        viewModel.outputs.onBankListSuccess().subscribe { banks ->
            val list = arrayListOf<String>()
            val bankHash = HashMap<String, String>()
            val bankCodeHash = HashMap<String, String>()
            val swiftcode = HashMap<String, String>()
            val testSwiftCode = HashMap<String, String>()

            for (i in 0 until banks.bankInfos.size) {
                swiftcode[banks.bankInfos[i].name] = banks.bankInfos[i].swiftCode
                testSwiftCode[banks.bankInfos[i].name] = banks.bankInfos[i].testSwiftCode
                bankHash[banks.bankInfos[i].name] = banks.bankInfos[i].id.toString()
                bankCodeHash[banks.bankInfos[i].name] = banks.bankInfos[i].code
                list.add(banks.bankInfos[i].name)
            }
            val adapter = BankListAdapter(requireContext(), list)
            bank.threshold = 0
            adapter.notifyDataSetChanged()

            bank.onItemClickListener = AdapterView.OnItemClickListener {_, _, position, _ ->
                viewModel.setSelectedSwiftCode(swiftcode[adapter.getItem(position)])
                viewModel.setTestSelectedSwiftCode(testSwiftCode[adapter.getItem(position)])
                viewModel.setSelectedBankId(bankHash[adapter.getItem(position)]!!.toLong())
                selectedBankId = bankHash[adapter.getItem(position)]!!.toLong()
                viewModel.setSelectedBankCode(bankCodeHash[adapter.getItem(position)])
            }

            bank.setAdapter(adapter)
        }.addTo(subscriptions)


        viewModel.outputs.onReceiverCustomerSuccess().subscribe{
            receiverName = it.name
            receiverSurname = it.surname
            taxNumber = it.taxNumber
            showProgressBar(false)
            sendIntent(AccountAndCustomerInfoActivity(), true)
        }.addTo(subscriptions)

        viewModel.outputs.getBudgetAccountByIbanSuccess().subscribe{
            receiverName = it.name
            receiverSurname = ""
            taxNumber = it.taxNumber
            showProgressBar(false)
            sendIntent(AccountAndCustomerInfoActivity(), true)
        }.addTo(subscriptions)


        viewModel.outputs.onError().subscribe{
            //account not found error code
            if(it == 33311){
                sendIntent(AccountAndCustomerInfoActivity(), false)
            } else {
                AlertDialogMapper(requireActivity(), it).showAlertDialog()
            }
        }.addTo(subscriptions)

    }


    private fun setInputListeners() {
        viewModel.inputs.getBankList()
        submitButton.setOnClickListener {
            showProgressBar(true)
            when {
                bank.text.toString().trim().isEmpty() -> {
                    AlertDialogMapper(requireActivity(), 124020).showAlertDialog()
                    showProgressBar(false)
                }
                account.text.toString().trim().isEmpty() -> {
                    account.error = getString(R.string.select_receiver_account_error)
                    showProgressBar(false)
                }
                account.text.toString().length < 28 -> {
                    account.error = getString(R.string.account_validation)
                    showProgressBar(false)
                }
                else -> {
                    if(selectedBankId == 4L){
                        viewModel.inputs.getBudgetAccountByIban(account.text.toString())
                    } else {
                        viewModel.inputs.getReceiverCustomerInfo(account.text.toString(),viewModel.getSelectedSwiftCode(),viewModel.getSelectedTestSwiftCode())
                    }
                    showProgressBar(false)
                }
            }
        }
    }

    private fun initViews(view: View) {
        bankContainer = view.findViewById(R.id.bankContainer)

        accountContainer = view.findViewById(R.id.accountContainer)

        progressImage = view.findViewById(R.id.progressImage)

        submitText = view.findViewById(R.id.submitText)

        submitButton = view.findViewById(R.id.submitButton)

        bank = view.findViewById(R.id.bank)
        account = view.findViewById(R.id.account)

        account.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(28))
    }

    private fun sendIntent(activity: Activity, isRegistered: Boolean){
        val intent = Intent(requireActivity(), activity::class.java)
        intent.putExtra("isRegistered", isRegistered)
        intent.putExtra("senderAccountOrAlias", requireActivity().intent.getStringExtra("senderAccountOrAlias"))
        intent.putExtra("senderFullName", requireActivity().intent.getStringExtra("senderFullName"))
        intent.putExtra("senderIBAN", requireActivity().intent.getStringExtra("senderIBAN"))
        intent.putExtra("isGovernmentPayment", viewModel.isGovernmentPayment())
        intent.putExtra("receiverId", viewModel.getSelectedBankId())
        intent.putExtra("receiverAccountOrAlias", account.text.toString())
        intent.putExtra("fromAccount",requireActivity().intent.getBooleanExtra("fromAccount",true))
        intent.putExtra("toAccount",true)
        intent.putExtra("receiverCustomerName", receiverName)
        intent.putExtra("receiverCustomerSurname", receiverSurname)
        intent.putExtra("senderFullName", senderFullName)
        intent.putExtra("voenCode",taxNumber)
        startActivityForResult(intent,7)
    }

    private fun showProgressBar(show: Boolean) {
        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        bankContainer.isClickable = !show
        accountContainer.isClickable = !show
        bank.isClickable = !show
        account.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this.activity!!, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this.activity!!, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this.activity!!, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this.activity!!, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                requireActivity().setResult(Activity.RESULT_OK)
                requireActivity().finish()
            }
        }
    }
}
