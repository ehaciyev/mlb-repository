package az.turanbank.mlb.presentation.ips.aliases.delete

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.content.ContextCompat


import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import kotlinx.android.synthetic.main.ips_single_delete_alias_list.view.*

class DeleteSingleSelectAliasAdapter(
    private var context: Context,
    private var alias: ArrayList<MLBAliasResponseModel>,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<DeleteSingleSelectAliasAdapter.AliasSingleSelectViewHolder>() {

    private var selected: RadioButton? = null
    private var selectedItem :MLBAliasResponseModel? = null
    private var selectedPosition: Int? = null

    class AliasSingleSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AliasSingleSelectViewHolder =
        AliasSingleSelectViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.ips_single_delete_alias_list, parent, false
            )
        )

    override fun getItemCount() = alias.size

    fun updateRecords(mlbAliasResponseModel: ArrayList<MLBAliasResponseModel>){
        this.alias = mlbAliasResponseModel
        notifyDataSetChanged()
    }


    fun setData(list: ArrayList<MLBAliasResponseModel>) {
        alias.clear()
        alias.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: AliasSingleSelectViewHolder, position: Int) {
        /*holder.bind(context,alias[position], onClickListener)*/
        var alias = alias[position]
        holder.itemView.aliasName.text = alias.ipsValue
        holder.itemView.aliasTypeAndAccount.text = alias.ipsType

        if(alias.isSelected){
            holder.itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.checked_account))
            holder.itemView.selectedAlias.isChecked = true
        } else {
            holder.itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.unchecked_account))
            holder.itemView.selectedAlias.isChecked = false
        }

        holder.itemView.setOnClickListener { onClickListener.invoke(position) }
        holder.itemView.selectedAlias.setOnClickListener { onClickListener.invoke(position) }
    }

    fun getSelectedItem() = selectedItem

    fun getSelectedPosition() = selectedPosition
}