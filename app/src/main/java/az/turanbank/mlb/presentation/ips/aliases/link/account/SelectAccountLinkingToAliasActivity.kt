package az.turanbank.mlb.presentation.ips.aliases.link.account

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.ips.request.AliasAndAccountRequestModel
import az.turanbank.mlb.data.remote.model.ips.response.GetIPSAccounts
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.data.SelectIPSMLBAccountModel
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_ips_account_multi_select.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class SelectAccountLinkingToAliasActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: SelectAccountLinkingToAliasViewModel

    lateinit var adapter: InstantPaymentAccountSingleSelectAdapter

    private val accountList = arrayListOf<GetIPSAccounts>()

    lateinit var dialog: MlbProgressDialog
    //private val aliasList by lazy { intent.getSerializableExtra("aliasList") as ArrayList<MLBAliasSelectResponseModel> }
    private val aliasValue: String by lazy { intent.getStringExtra("aliasValue") }
    private val aliasId: Long by lazy { intent.getLongExtra("aliasId", 0L) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ips_account_multi_select)

        viewModel =
            ViewModelProvider(this, factory)[SelectAccountLinkingToAliasViewModel::class.java]

        dialog = MlbProgressDialog(this)
        toolbar_back_button.setOnClickListener {
            onBackPressed()
        }
        toolbar_back_button.setOnClickListener { onBackPressed() }
        toolbar_title.text = getString(R.string.link_alias)
        message.text = getString(R.string.alias_to_account_view)

        recyclerViewShimmer.visibility = View.VISIBLE
        recyclerViewShimmer.startShimmerAnimation()


        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        adapter = InstantPaymentAccountSingleSelectAdapter(accountList, this,{
            //accountList[it].checked = !accountList[it].checked
            //adapter.notifyDataSetChanged()
        },{

        }
        )

        recyclerView.adapter = adapter

        setOutputListeners()
        setInputListeners()
    }


    private fun setInputListeners() {
        viewModel.inputs.getAccountsByAliasId(aliasId)

        submitButton.setOnClickListener {
            val list = arrayListOf<AliasAndAccountRequestModel>()
            /*accountList.forEach {
                if (it.checked){
                    if(adapter.getSelectedDefaultAccount()!!.isDefault == 0){
                        list.add(AliasAndAccountRequestModel(aliasId, it.accountId, false))
                    } else {
                        list.add(AliasAndAccountRequestModel(aliasId, it.accountId, true))
                    }
                }
            }*/

            adapter.getSelectedItems().forEach {
                list.add(AliasAndAccountRequestModel(aliasId,it.accountId,it.isDefault==1))
            }

            if(adapter.getSelectedItems().size !=0 ){
                dialog.showDialog()
                viewModel.inputs.linkAccountToAlias(list)
            } else {
                if(adapter.getSelecteditemsFromServer().size == accountList.size){
                    //list.add(AliasAndAccountRequestModel(aliasId, adapter.getSelectedDefaultAccount()!!.accountId,adapter.getSelectedDefaultAccount()!!.isDefault ==1))
                    AlertDialogMapper(this, 8002).showAlertDialog()
                } else {
                    AlertDialogMapper(this, 8003).showAlertDialog()
                }
            }
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onAccountListSuccess().subscribe {
            accountList.clear()
            accountList.addAll(it)
            message.visibility = View.VISIBLE
            message.text = getString(R.string.alias_to_account_view)
            noDataAvailable.visibility = View.GONE
            recyclerViewShimmer.visibility = View.INVISIBLE
            recyclerViewShimmer.stopShimmerAnimation()
            recyclerView.adapter?.notifyDataSetChanged()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            dialog.hideDialog()
            recyclerViewShimmer.visibility = View.INVISIBLE
            recyclerViewShimmer.stopShimmerAnimation()
            noDataAvailable.visibility = View.GONE
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
        viewModel.outputs.linkCompleted().subscribe {
            dialog.hideDialog()

            recyclerViewShimmer.visibility = View.INVISIBLE
            recyclerViewShimmer.stopShimmerAnimation()

            noDataAvailable.visibility = View.GONE
            setResult(Activity.RESULT_OK,Intent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            //startActivityForResult(Intent(this, ChooseDefaultAlias::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),7)
            finish()
        }.addTo(subscriptions)
    }
}
