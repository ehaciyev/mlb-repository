package az.turanbank.mlb.presentation.resources.card.sms

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.resources.card.sms.DisableSmsNotificationForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.sms.DisableSmsNotificationForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface DeleteOrUpdateSmsNotificationViewModelInputs: BaseViewModelInputs{
    fun disableSms(mobile: String, cardId: Long)
}
interface DeleteOrUpdateSmsNotificationViewModelOutputs: BaseViewModelOutputs{
    fun onNotificationDisabled(): PublishSubject<Boolean>
}
class DeleteOrUpdateSmsNotificationViewModel @Inject constructor(
    private val disableSmsNotificationForIndividualCustomerUseCase: DisableSmsNotificationForIndividualCustomerUseCase,
    private val disableSmsNotificationForJuridicalCustomerUseCase: DisableSmsNotificationForJuridicalCustomerUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
    DeleteOrUpdateSmsNotificationViewModelInputs,
    DeleteOrUpdateSmsNotificationViewModelOutputs {

    override val inputs: DeleteOrUpdateSmsNotificationViewModelInputs = this

    override val outputs: DeleteOrUpdateSmsNotificationViewModelOutputs = this


    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val smsDisabled = PublishSubject.create<Boolean>()
    override fun disableSms(mobile: String, cardId: Long) {
        if (isCustomerJuridical){
            disableSmsNotificationForJuridicalCustomerUseCase
                .execute(mobile, mobile, custId, compId, token, cardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        smsDisabled.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {

                }).addTo(subscriptions)
        } else {
            disableSmsNotificationForIndividualCustomerUseCase
                .execute(mobile, mobile, custId, token, cardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        smsDisabled.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {

                }).addTo(subscriptions)
        }
    }

    override fun onNotificationDisabled() = smsDisabled
}