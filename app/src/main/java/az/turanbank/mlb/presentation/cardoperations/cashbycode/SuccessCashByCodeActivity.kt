package az.turanbank.mlb.presentation.cardoperations.cashbycode

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_success_cash_by_code.*
import kotlinx.android.synthetic.main.add_to_templates_dialog_view.view.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import javax.inject.Inject

class SuccessCashByCodeActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: SuccessCashByCodeViewModel

    private val receiptNumber: Long by lazy { intent.getLongExtra("cashCode", 0L) }

    private val requestorCardNumber: String by lazy { intent.getStringExtra("requestorCardNumber") }

    private val mobileToShow: String by lazy { intent.getStringExtra("mobile") }

    private val date: String by lazy { intent.getStringExtra("operationDate") }
    private val noteToShow: String by lazy { intent.getStringExtra("note") }

    private val destinationCard: String by lazy { intent.getStringExtra("dtCardNumber") }
    private val amountToShow: Double by lazy { intent.getDoubleExtra("amount", 0.00) }
    private val curr: String by lazy { intent.getStringExtra("currency") }

    private var fullAmountInteger = ""
    private var fullAmountReminder = ""

    lateinit var paidStamp: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_cash_by_code)

        val decimalFormat = DecimalFormat("0.00")

        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        val symbols = DecimalFormatSymbols()
        symbols.decimalSeparator = '.'
        decimalFormat.decimalFormatSymbols = symbols

        viewModel =
            ViewModelProvider(this, factory)[SuccessCashByCodeViewModel::class.java]
        fullAmountInteger = decimalFormat.format(amountToShow).toString().split(".")[0] + ","
        fullAmountReminder = decimalFormat.format(amountToShow).toString().split(".")[1] + " " + curr

        initViews()
        makeViewWithData()
        setOutputListeners()
        setInputListeners()
    }

    private fun initViews() {
        paidStamp = findViewById(R.id.paid_stamp)
    }

    private fun setInputListeners() {
        close.setOnClickListener {
            setResult(Activity.RESULT_FIRST_USER)
            finish()
        }

        share.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, generateShareString())

            startActivity(Intent.createChooser(shareIntent, "Göndər..."))
        }

        addToTemplate.setOnClickListener {
            showSaveTempEditText()
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.saveTempSuccess().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            Toast.makeText(this, getString(R.string.template_successfully_created), Toast.LENGTH_LONG).show()
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            cardView.visibility = View.VISIBLE
            Toast.makeText(this, getString(ErrorMessageMapper().getErrorMessage(it)), Toast.LENGTH_LONG).show()
        }.addTo(subscriptions)
    }

    @SuppressLint("SetTextI18n")
    private fun makeViewWithData() {
        receiptNo.text = receiptNumber.toString()
        operationDate.text = date
        dtCardNumber.text = destinationCard
        mobile.text = mobileToShow
        amountInteger.text = fullAmountInteger
        amountReminder.text = fullAmountReminder
        note.text = noteToShow

        when {
            viewModel.getLang() == "en" -> {
                paidStamp.setImageResource(R.drawable.pechat_eng)
            }
            viewModel.getLang() == "ru" -> {
                paidStamp.setImageResource(R.drawable.pechat_rus)
            }
            viewModel.getLang() == "az" -> {
                paidStamp.setImageResource(R.drawable.pechat_az)
            }
        }
    }


    @SuppressLint("InflateParams")
    private fun showSaveTempEditText() {

        val alert = AlertDialog.Builder(this)
        alert.setTitle("Şablonun adı")

        val view =
            LayoutInflater.from(this).inflate(R.layout.add_to_templates_dialog_view, null, false)
        alert.setView(view)

        alert.setCancelable(false)
        alert.setPositiveButton(
            getString(R.string.remember_message)
        ) { _, _ ->
            if (!TextUtils.isEmpty(view.tempNameEditText.text.toString())) {
                animateProgressImage()
                viewModel.inputs.saveTemp(
                    view.tempNameEditText.text.toString(),
                    requestorCardNumber = requestorCardNumber,
                    destinationPhoneNumber = mobileToShow,
                    amount = amountToShow,
                    currency = curr
                )
            }
        }

        alert.setNegativeButton(
            getString(R.string.cancel_reason)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }

    private fun generateShareString(): String {

        return "Əməliyyatın tipi: CashByCode əməliyyatı" + "\n" +
                "Əməliyyatın nömrəsi: " + intent.getStringExtra("receiptNo") + "\n" +
                "Alanın mobil nömrəsi: " + mobileToShow + "\n" +
                "Vəsait tutulan kart: " + destinationCard + "\n" +
                "Məbləğ: " + amountToShow + "\n" +
                "Valyuta: " + curr + "\n"
    }
    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        cardView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
