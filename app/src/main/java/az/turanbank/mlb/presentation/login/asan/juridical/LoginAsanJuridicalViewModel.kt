package az.turanbank.mlb.presentation.login.asan.juridical

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetAuthInfoResponseModel
import az.turanbank.mlb.domain.user.register.usecase.GetAuthInfoUseCase
import az.turanbank.mlb.domain.user.usecase.login.CheckPinForAsanJuridicalLoginUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.GetPinFromAsanUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface LoginAsanJuridicalViewModelInputs : BaseViewModelInputs {
    fun getPinFromAsan(phoneNumber: String, userId: String)
}

interface LoginAsanJuridicalViewModelOutputs : BaseViewModelOutputs {
    fun verificationCode(): Observable<GetAuthInfoResponseModel>
    fun onUserBlocked(): CompletableSubject
    fun showProgress(): PublishSubject<Boolean>
}

class LoginAsanJuridicalViewModel @Inject constructor(
    private val getPinFromAsanUseCase: GetPinFromAsanUseCase,
    private val checkPinForAsanJuridicalLoginUseCase: CheckPinForAsanJuridicalLoginUseCase,
    private val getAuthInfoUseCase: GetAuthInfoUseCase,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel(),
    LoginAsanJuridicalViewModelInputs,
    LoginAsanJuridicalViewModelOutputs {

    private val verificationCode = PublishSubject.create<GetAuthInfoResponseModel>()
    override val inputs: LoginAsanJuridicalViewModelInputs = this

    override val outputs: LoginAsanJuridicalViewModelOutputs = this

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val userBlocked = CompletableSubject.create()
    private val showProgress = PublishSubject.create<Boolean>()

    private var custCode: Long = 0L
    private lateinit var custPin: String
    private var custId = 0L

    fun custCode() = custCode

    fun custPin() = custPin

    fun custId() = custId
    override fun getPinFromAsan(phoneNumber: String, userId: String) {
        getPinFromAsanUseCase.execute(phoneNumber, userId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.status.statusCode == 1) {
                    checkPinForForAsanJuridical(it.pin, phoneNumber, userId)
                } else {
                    error.onNext(it.status.statusCode)                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    private fun checkPinForForAsanJuridical(pin: String, phoneNumber: String, userId: String) {
        showProgress.onNext(true)

        checkPinForAsanJuridicalLoginUseCase.execute(pin)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)

                keepExistingPinOrAdd(
                    companyName = null,
                    compId = it.compId,
                    custId = it.custId,
                    token = it.token,
                    name = it.name,
                    surname = it.surname,
                    patronymic = it.patronymic
                )
                custCode = it.custCode
                custPin = it.pin
                custId = it.custId
                when (it.status.statusCode) {
                    1 -> getAuthInfo(phoneNumber, userId)
                    119 -> userBlocked.onComplete()
                    else -> error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    private fun keepExistingPinOrAdd(
        custId: Long,
        token: String,
        compId: Long?,
        companyName: String?,
        name: String,
        surname: String,
        patronymic: String
    ) {
        sharedPreferences.edit().putLong("custId", custId).apply()
        sharedPreferences.edit().putString("token", token).apply()
        sharedPreferences.edit().putString("fullname", "$surname $name $patronymic").apply()

        compId?.let {
            sharedPreferences.edit().putLong("compId", compId).apply()
        }
    }

    private fun getAuthInfo(phoneNumber: String, userId: String) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        getAuthInfoUseCase.execute(phoneNumber, userId,enumLangType)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                showProgress.onNext(false)

                if (it.status.statusCode == 1) {
                    verificationCode.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun verificationCode(): Observable<GetAuthInfoResponseModel> = verificationCode
    override fun onUserBlocked() = userBlocked
    override fun showProgress() = showProgress
}