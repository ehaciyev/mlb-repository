package az.turanbank.mlb.presentation.view

public interface SwipeControllerActions {
    fun onLeftClicked(position: Int) {}

    fun onRightClicked(position: Int) {}
}