package az.turanbank.mlb.presentation.payments.pay.check.custom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.InvoiceResponseModel
import kotlinx.android.synthetic.main.check_item_list_view_custom.view.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class ChooseInvoiceWithChildAdapter(
    private val context: Context,
    private val invoice: ArrayList<InvoiceResponseModel>,
    private val onClickAction: (position: Int) -> Unit
) : RecyclerView.Adapter<ChooseInvoiceWithChildAdapter.ChoosePaymentViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) =
        ChoosePaymentViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.check_item_list_view_custom,
                parent,
                false
            )
        )

    override fun getItemCount() = invoice.size

    override fun onBindViewHolder(
        holder: ChoosePaymentViewHolder,
        position: Int
    ) {
        holder.bindInvoice(invoice[position], onClickAction, context)
    }

    class ChoosePaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindInvoice(invoice: InvoiceResponseModel, onClickAction: (position: Int) -> Unit, context: Context) {
            val decimalFormat = DecimalFormat("0.00")
            decimalFormat.roundingMode = RoundingMode.HALF_EVEN
            val symbols = DecimalFormatSymbols()
            symbols.decimalSeparator = '.'
            decimalFormat.decimalFormatSymbols = symbols

            itemView.setOnClickListener {  onClickAction.invoke(adapterPosition) }
            itemView.name.text = invoice.fullName

            itemView.amountInteger.text = decimalFormat.format(invoice.amount).split(".")[0] + ","
            itemView.amountReminder.text = decimalFormat.format(invoice.amount).split(".")[1]
        }
    }
}
