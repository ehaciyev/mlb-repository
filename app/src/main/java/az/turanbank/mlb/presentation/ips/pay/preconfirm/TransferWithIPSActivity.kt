package az.turanbank.mlb.presentation.ips.pay.preconfirm

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Instrumentation
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.SuccessfulPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.BankListAdapter
import az.turanbank.mlb.presentation.ips.pay.preconfirm.confirm.ConfirmWithIPSActivity
import az.turanbank.mlb.presentation.payments.pay.result.PaymentResultActivity
import az.turanbank.mlb.util.animateProgressImage
import az.turanbank.mlb.util.revertProgressImageAnimation
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_card_to_card.*
import kotlinx.android.synthetic.main.activity_card_to_card.amount
import kotlinx.android.synthetic.main.activity_check_payment.*
import kotlinx.android.synthetic.main.activity_transfer_with_ips.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap

class TransferWithIPSActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: TransferWithIPSViewModel

    lateinit var submitButton: LinearLayout

    lateinit var submitText: TextView

    lateinit var progressImage: ImageView

    lateinit var ipsAmount: AppCompatEditText

    lateinit var ipsPurpose: AppCompatEditText

    lateinit var ipsMessage: AppCompatEditText

    private val isPayRequest: Boolean by lazy { intent.getBooleanExtra("isPayRequest",false) }
    private val senderAccountOrAlias: String by lazy { intent.getStringExtra("senderAccountOrAlias") }
    private val receiverAccountOrAlias: String by lazy { intent.getStringExtra("receiverAccountOrAlias") }
    private val isGovernmentPayment: Boolean by lazy { intent.getBooleanExtra("isGovernmentPayment", false) }
    private val receiverId: Long by lazy { intent.getLongExtra("receiverId",0L ) }
    private val senderIBAN: String by lazy { intent.getStringExtra("senderIBAN" ) }
    private val fromAccount: Boolean by lazy { intent.getBooleanExtra("fromAccount",true) }
    private val toAccount: Boolean by lazy { intent.getBooleanExtra("toAccount",true) }
    private val receiverCustomerName: String by lazy { intent.getStringExtra("receiverCustomerName") }
    private val receiverCustomerSurname: String by lazy { intent.getStringExtra("receiverCustomerSurname") }
    private val voenCode: String by lazy { intent.getStringExtra("voenCode") }
    private val senderFullName: String by lazy { intent.getStringExtra("senderFullName") }
    private val receiverIban: String by lazy { intent.getStringExtra("receiverIban") }

    private var senderAliasTypeId: Int = 0
    private var businessProcessId: Long = 0L

    private var ipsPaymentType = 1

    private var dateCalendar: Calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfer_with_ips)

        viewModel = ViewModelProvider(this, factory)[TransferWithIPSViewModel::class.java]
        initViews()
        toolbar_title.text = getString(R.string.pay)
        toolbar_back_button.setOnClickListener{ onBackPressed()}

        if(isPayRequest){
            paymentHistoryContainer.visibility = View.VISIBLE
            ipsPurposeContainer.visibility = View.GONE
            calendarContainer.setOnClickListener {
                val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    dateCalendar.set(Calendar.YEAR, year)
                    dateCalendar.set(Calendar.MONTH, monthOfYear)
                    dateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    paymentHistory.setText(
                        simpleDateFormat().format(
                            dateCalendar.time
                        ).replace("-", ".")
                    )
                }

                DatePickerDialog(
                    this, R.style.DateDialogTheme, date, dateCalendar
                        .get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH),
                    dateCalendar.get(Calendar.DAY_OF_MONTH)
                ).show()
            }
        }

        if(isGovernmentPayment) {
            ipsBudgetClassCodeContainer.visibility = View.VISIBLE
            ipsBudgetLevelCodeContainer.visibility = View.VISIBLE
        }

        if(!toAccount){
            senderAliasTypeId = intent.getIntExtra("senderAliasTypeId",0)
        }

        submitButton.setOnClickListener{
            showProgressBar(true)
            sendValues()
            showProgressBar(false)
        }

        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        if(isGovernmentPayment){
            viewModel.inputs.getAllBudgetLevelUseCase()
            viewModel.inputs.getAllBudgetCodeUseCase()
            ipsPaymentType = 2
        }
    }

    private fun setOutputListeners() {

        viewModel.outputs.onSuccessGetAllBudgetLevelUseCase().subscribe { banks ->
            val list = arrayListOf<String>()
            val bankHash = HashMap<String, String>()
            val bankCodeHash = HashMap<String, String>()

            for (i in 0 until banks.bankInfos.size) {
                list.add(banks.bankInfos[i].budgetId)
                bankHash[banks.bankInfos[i].budgetId] = banks.bankInfos[i].budgetId.toString()
                bankCodeHash[banks.bankInfos[i].budgetValue] = banks.bankInfos[i].budgetValue
            }
            val adapter = BankListAdapter(this, list)
            ipsBudgetLevelCode.threshold = 0
            adapter.notifyDataSetChanged()

            ipsBudgetLevelCode.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                viewModel.setSelectedLevelId(bankHash[adapter.getItem(position)]!!.toString())
            }

            ipsBudgetLevelCode.setAdapter(adapter)
        }.addTo(subscriptions)

        viewModel.outputs.onSuccessGetAllBudgetCodeUseCase().subscribe { banks ->
            val list = arrayListOf<String>()
            val bankHash = HashMap<String, String>()
            val bankCodeHash = HashMap<String, String>()

            for (i in 0 until banks.bankInfos.size) {
                list.add(banks.bankInfos[i].budgetId)
                bankHash[banks.bankInfos[i].budgetId] = banks.bankInfos[i].budgetId.toString()
                bankCodeHash[banks.bankInfos[i].budgetValue] = banks.bankInfos[i].budgetValue
            }
            val adapter = BankListAdapter(this, list)
            ipsBudgetClassCode.threshold = 0
            adapter.notifyDataSetChanged()

            ipsBudgetClassCode.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                viewModel.setSelectedCodeId(bankHash[adapter.getItem(position)]!!.toString())
            }

            ipsBudgetClassCode.setAdapter(adapter)
        }.addTo(subscriptions)


        viewModel.outputs.onError().subscribe{
            AlertDialogMapper(this,it).showAlertDialog()
        }.addTo(subscriptions)

    }

    private fun sendValues(){
        if(checkInput()){
            businessProcessId = if(isGovernmentPayment) 1L else 2L

            var intent = Intent(this, ConfirmWithIPSActivity::class.java)
            intent.putExtra("senderAccountOrAlias", senderAccountOrAlias)
            intent.putExtra("senderIBAN", senderIBAN)
            intent.putExtra("receiverAccountOrAlias", receiverAccountOrAlias)
            intent.putExtra("receiverId", receiverId)
            intent.putExtra("amount", ipsAmount.text.toString().toDoubleOrNull())
            intent.putExtra("ipsPurpose", ipsPurpose.text.toString())
            intent.putExtra("ipsMessage",ipsMessage.text.toString())
            intent.putExtra("fromAccount", fromAccount)
            intent.putExtra("toAccount", toAccount)
            intent.putExtra("businessProcessId",businessProcessId)
            intent.putExtra("senderAliasTypeId",senderAliasTypeId)
            intent.putExtra("receiverCustomerName",receiverCustomerName)
            intent.putExtra("receiverCustomerSurname",receiverCustomerSurname)
            intent.putExtra("voenCode",voenCode)
            intent.putExtra("senderFullName",senderFullName)
            intent.putExtra("budgetLvl",viewModel.getSelectedLevelId())
            intent.putExtra("budgetCode",viewModel.getSelectedCodeId())
            intent.putExtra("receiverIban",receiverIban)
            intent.putExtra("ipsPaymentType",ipsPaymentType)
            startActivityForResult(intent, 7)
        }
    }

    private fun checkInput(): Boolean{
        var isvalid = true
        if (ipsAmount.text.toString().trim().isEmpty()) {
            ipsAmount.error = getString(R.string.amount_is_wrong)
            isvalid = false
        }

        if(isPayRequest){
            if(paymentHistory.text.toString().trim().isEmpty()){
                paymentHistory.error = getString(R.string.enter_date)
                isvalid = false
            }
        } else {
            if(ipsPurpose.text.toString().trim().isEmpty()){
                ipsPurpose.error = getString(R.string.enter_creditor_purpose)
                isvalid = false
            }
        }

        if(isGovernmentPayment){
            if(ipsBudgetClassCode.text.toString().trim().isEmpty()){
                AlertDialogMapper(this, 1994).showAlertDialog()
                isvalid = false
            }
            if(ipsBudgetLevelCode.text.toString().trim().isEmpty()){
                AlertDialogMapper(this, 1995).showAlertDialog()
                isvalid = false
            }
        }
        return isvalid
    }

    private fun initViews() {
        ipsAmount = findViewById(R.id.ipsAmount)
        ipsPurpose = findViewById(R.id.ipsPurpose)
        ipsMessage = findViewById(R.id.ipsMessage)
        submitButton = findViewById(R.id.submitButton)
        submitText = findViewById(R.id.submitText)
        progressImage = findViewById(R.id.progressImage)
    }

    private fun showProgressBar(show: Boolean) {
        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        ipsAmount.isClickable = !show
        ipsPurpose.isClickable = !show
        ipsMessage.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent(this, SuccessfulPageViewActivity::class.java)
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.putExtra("description", data?.getSerializableExtra("description"))
                intent.putExtra("finishOnly",false)
                intent.putExtra("goToPage","myPayment")
                setResult(Activity.RESULT_OK)
                startActivity(intent)
                finish()
            }
        }
    }

    private fun simpleDateFormat(): SimpleDateFormat {
        val myFormat = "dd-MM-yyyy"
        return SimpleDateFormat(myFormat, Locale.US)
    }
}