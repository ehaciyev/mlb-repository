package az.turanbank.mlb.presentation.ips.aliases

import android.content.Intent
import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.aliases.create.CreateNewAliasActivity
import az.turanbank.mlb.presentation.ips.aliases.delete.DeleteIPSAliasActivity
import az.turanbank.mlb.presentation.ips.aliases.link.alias.SelectAliasLinkingToAccountActivity
import az.turanbank.mlb.presentation.ips.aliases.myaliases.MyAliasesActivity
import kotlinx.android.synthetic.main.activity_ipsaliases.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class IPSAliasesActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ipsaliases)

        toolbar_title.text = getString(R.string.my_aliases)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        deleteAlias.setOnClickListener { startActivity(Intent(this, DeleteIPSAliasActivity::class.java)) }
        connect.setOnClickListener { startActivity(Intent(this, SelectAliasLinkingToAccountActivity::class.java)) }
        create.setOnClickListener { startActivity(Intent(this, CreateNewAliasActivity::class.java)) }
        //ips_aliases.setOnClickListener { startActivity(Intent(this, MyAliasesActivity::class.java)) }
    }
}