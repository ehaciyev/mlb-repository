package az.turanbank.mlb.presentation.payments.pay.result

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.param.MerchantParamListResponseModel
import az.turanbank.mlb.domain.user.usecase.pg.param.GetMerchantParamListIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.param.GetMerchantParamListJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.pg.template.CreatePaymentTemplateIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.template.CreatePaymentTemplateJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface PaymentResultViewModelInputs : BaseViewModelInputs {
    fun saveTemplate(
        tempName: String,
        merchantId: Long,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        amount: Double,
        currency: String,
        providerId: Long,
        categoryId: Long
    )
}

interface PaymentResultViewModelOutputs : BaseViewModelOutputs {
    fun saveTemplateSuccess(): CompletableSubject
}

class PaymentResultViewModel @Inject constructor(
    private val createPaymentTemplateIndividualUseCase: CreatePaymentTemplateIndividualUseCase,
    private val createPaymentTemplateJuridicalUseCase: CreatePaymentTemplateJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    PaymentResultViewModelInputs,
    PaymentResultViewModelOutputs {
    override val inputs: PaymentResultViewModelInputs = this

    override val outputs: PaymentResultViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val templateSaved = CompletableSubject.create()

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun saveTemplate(
        tempName: String,
        merchantId: Long,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        amount: Double,
        currency: String,
        providerId: Long,
        categoryId: Long
    ) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        val respond = if (isCustomerJuridical){
            createPaymentTemplateJuridicalUseCase
                .execute(custId, compId, token, tempName, merchantId, identificationType, identificationCode, amount, currency, providerId, categoryId, enumLangType)
        } else {
            createPaymentTemplateIndividualUseCase
                .execute(custId, token, tempName, merchantId, identificationType, identificationCode, amount, currency, providerId, categoryId, enumLangType)
        }

        respond.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    templateSaved.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun saveTemplateSuccess() = templateSaved
}