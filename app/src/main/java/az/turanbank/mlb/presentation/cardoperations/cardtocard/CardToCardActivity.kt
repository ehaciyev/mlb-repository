package az.turanbank.mlb.presentation.cardoperations.cardtocard

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.formatDecimal
import com.facebook.shimmer.ShimmerFrameLayout
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_card_to_card.*
import java.util.*
import javax.inject.Inject


@Suppress("UNCHECKED_CAST")
class CardToCardActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardToCardViewModel

    private var toCardList = arrayListOf<String>()

    private var fromCardList = arrayListOf<String>()

    private var fromCardModel = arrayListOf<CardListModel>()

    private var toCardModel = arrayListOf<CardListModel>()

    private var currencies = arrayListOf<String>()

    private var debitorPosition = 0

    private var creditorPosition = 0

    lateinit var cardToCardShimmer: ShimmerFrameLayout

    private val fromTemplate: Boolean by lazy { intent.getBooleanExtra("fromTemplate", false) }
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_to_card)

        viewModel =
            ViewModelProvider(this, factory)[CardToCardViewModel::class.java]

        cardToCardShimmer = findViewById(R.id.cardToCardShimmer)

        toolbar_title.text = getString(R.string.between_my_own_cards)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        setOutputListeners()
        setInputListeners()

    }

    private fun checkFromHomeMenu() {
        val cardId = intent.getLongExtra("cardId", 0L)
        Log.i("CardID", cardId.toString())
        if (cardId != 0L) {
            for (i in 0 until fromCardModel.size) {
                if (fromCardModel[i].cardId == cardId) {
                    fromCardTextView.text = fromCardList[i]

                    debitorPosition = i
                    toCardModel.removeAt(i)
                    toCardList.removeAt(i)
                    break
                }
            }
        }
    }

    private fun checkTemplateOrNot() {
        if (fromTemplate) {
            fillBlanksFromTemplate()
            deleteTemp.visibility = View.VISIBLE
        } else {
            deleteTemp.visibility = View.GONE
        }
    }

    private fun showDeleteTemp(tempId: Long) {

        val alert = AlertDialog.Builder(this)
        alert.setMessage(getString(R.string.ask_delete_template))

        alert.setCancelable(true)
        alert.setPositiveButton(
            getString(R.string.yes)
        ) { _, _ ->
            animateProgressImage()
            viewModel.inputs.deleteTemp(tempId)
        }

        alert.setNegativeButton(
            getString(R.string.no)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }

    @SuppressLint("SetTextI18n")
    private fun fillBlanksFromTemplate() {
        deleteTemp.setOnClickListener { showDeleteTemp(intent.getLongExtra("tempId", 0L)) }

        val fromCard: String by lazy { intent.getStringExtra("fromCard") }
        val toCardOrMobile: String by lazy { intent.getStringExtra("toCardOrMobile") }
        val amountVar: Double by lazy { intent.getDoubleExtra("amount", 0.00) }
        val currency: String by lazy { intent.getStringExtra("currency") }

        for (i in 0 until fromCardModel.size) {
            if (fromCardModel[i].cardNumber == fromCard) {
                fromCardTextView.text = fromCardList[i]
                debitorPosition = i
                toCardModel.removeAt(i)
                toCardList.removeAt(i)
                break
            }
        }
        for (i in 0 until toCardModel.size) {
            if (toCardModel[i].cardNumber == toCardOrMobile) {
                toCardOrMobileTextView.text = toCardList[i]
                creditorPosition = i
            }
        }

        amount.setText(amountVar.toString())

        for (i in 0 until currencies.size) {
            if (currencies[i] == currency) {
                currencySpinner.setSelection(i)
                break
            }
        }

    }

    private fun setInputListeners() {
        //animateProgressImage()
        mainView.visibility = View.GONE

        viewModel.inputs.getCardList()

        submitButton.setOnClickListener {
            if (!TextUtils.isEmpty(amount.text.toString())&&(amount.text.toString().toDouble() >0.00)) {
                if (toCardOrMobileTextView.text.toString() != "Alan kart") {
                    if (toCardOrMobileTextView.text.toString() != getString(R.string.depitor_card)) {
                        showProgressBar(true)
                        val intent = Intent(this, VerifyCardToCardActivity::class.java)
                        intent.putExtra(
                            "fromCard",
                            fromCardModel[debitorPosition].cardNumber.replaceRange(
                                4,
                                12,
                                "********"
                            )
                        )
                        intent.putExtra("toCard", toCardModel[creditorPosition].cardNumber)
                        intent.putExtra("currency", currencySpinner.selectedItem?.toString())
                        intent.putExtra("amount", amount.text.toString())

                        intent.putExtra(
                            "fromCardId",
                            fromCardModel[debitorPosition].cardId
                        )
                        intent.putExtra(
                            "toCardId",
                            toCardModel[creditorPosition].cardId
                        )
                        intent.putExtra("amountToSend", amount.text?.toString()?.toDouble())
                        intent.putExtra("root", "cardToCard")

                        startActivityForResult(intent, 7)
                        showProgressBar(false)
                    } else {
                        AlertDialogMapper(this, 124005).showAlertDialog()
                    }
                } else {
                    AlertDialogMapper(this, 124006).showAlertDialog()
                }
            } else {
                AlertDialogMapper(this, 124003).showAlertDialog()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent(this, SuccessCardToCardActivity::class.java)
                intent.putExtra("receiptNo", data?.getStringExtra("receiptNo"))
                intent.putExtra("operationDate", data?.getStringExtra("operationDate"))
                intent.putExtra("dtCardNumber", data?.getStringExtra("dtCardNumber"))
                intent.putExtra("amount", data?.getDoubleExtra("amount", 0.00))
                intent.putExtra("feeAmount", data?.getDoubleExtra("feeAmount", 0.00))
                intent.putExtra("operationName", data?.getStringExtra("operationName"))
                intent.putExtra("note", data?.getStringExtra("note"))
                intent.putExtra("currency", data?.getStringExtra("currency"))
                intent.putExtra("requestorCardNumber", data?.getStringExtra("requestorCardNumber"))
                intent.putExtra("root", "cardToCard")

                startActivityForResult(intent, 12)
            }
        }
        if (requestCode == 12) {
            if (resultCode == Activity.RESULT_OK) {
                setResult(Activity.RESULT_OK)
                finish()
            }
            if (resultCode == Activity.RESULT_FIRST_USER) {
                finish()
            }
        }
    }

    private fun setOutputListeners() {

        viewModel.outputs.templateDeleted().subscribe {
            Toast.makeText(this, getString(R.string.template_successfully_deleted), Toast.LENGTH_LONG).show()
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.cardListSuccess().subscribe {

            cardToCardShimmer.visibility = View.GONE
            cardToCardShimmer.stopShimmerAnimation()
            mainView.visibility = View.VISIBLE

            val filteredList = arrayListOf<CardListModel>()
            it.forEach { card ->
                if (card.cardStatus != "0") {
                    filteredList.add(card)
                }
            }
            attachListsToSpinners(filteredList)
        }.addTo(subscriptions)

        viewModel.outputs.currencyListSuccess().subscribe {

            cardToCardShimmer.visibility = View.GONE
            cardToCardShimmer.stopShimmerAnimation()
            mainView.visibility = View.VISIBLE

            currencies.clear()
            currencies.addAll(it)
            val currencyAdapter =
                ArrayAdapter(this, android.R.layout.simple_spinner_item, currencies)
            currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            currencySpinner.adapter = currencyAdapter
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            cardToCardShimmer.visibility = View.GONE
            cardToCardShimmer.stopShimmerAnimation()
            mainView.visibility = View.GONE
            //showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
        viewModel.outputs.currencyAndCardSuccess().subscribe {
            if (it) {
                checkTemplateOrNot()
                checkFromHomeMenu()
                mainProgress.clearAnimation()
                mainProgress.visibility = View.GONE

                cardToCardShimmer.visibility = View.GONE
                cardToCardShimmer.stopShimmerAnimation()
                mainView.visibility = View.VISIBLE
            } else {
                //animateProgressImage()
                cardToCardShimmer.visibility = View.GONE
                cardToCardShimmer.stopShimmerAnimation()
                mainView.visibility = View.GONE
            }
        }.addTo(subscriptions)

    }

    @SuppressLint("SetTextI18n")
    private fun attachListsToSpinners(cardList: ArrayList<CardListModel>) {
        fromCardModel.clear()
        fromCardModel.addAll(cardList)

        fromCardList.clear()

        for (i in 0 until cardList.size) {
            fromCardList.add(
                cardList[i].cardNumber.replaceRange(4, 12, "********") + " / " +
                        cardList[i].balance.formatDecimal() + "  " + cardList[i].currency
            )
        }
        toCardList.clear()
        for (i in 0 until cardList.size) {
            toCardList.add(
                cardList[i].cardNumber.substring(0, 4) + " "
                        + cardList[i].cardNumber.substring(
                    4,
                    8
                ) + " " + cardList[i].cardNumber.substring(
                    8,
                    12
                ) + " " + cardList[i].cardNumber.substring(12, 16)
            )
        }

        toCardModel.clear()
        toCardModel.addAll(cardList)

        debitorCardToCardContainer.setOnClickListener {
            val builder: android.app.AlertDialog.Builder =
                android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.depitor_card))

            val fromList = arrayOfNulls<String>(fromCardList.size)

            for (i in 0 until fromCardList.size) {
                fromList[i] = fromCardList[i]
            }
            builder.setItems(fromList) { _, which ->
                fromCardTextView.text = fromCardList[which]
                toCardList.clear()
                for (i in 0 until cardList.size) {
                    toCardList.add(
                        cardList[i].cardNumber.substring(0, 4) + " "
                                + cardList[i].cardNumber.substring(
                            4,
                            8
                        ) + " " + cardList[i].cardNumber.substring(
                            8,
                            12
                        ) + " " + cardList[i].cardNumber.substring(12, 16)
                    )
                }

                toCardList.removeAt(which)
                toCardModel.clear()
                toCardModel.addAll(cardList)
                toCardModel.removeAt(which)
                debitorPosition = which
                toCardOrMobileTextView.text = getString(R.string.creditor_card)
            }
            val dialog = builder.create()
            dialog.show()
        }

        creditorCardToCardContainer.setOnClickListener {
            val builder: android.app.AlertDialog.Builder =
                android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.creditor_card))
            val list = arrayOfNulls<String>(toCardList.size)

            for (i in 0 until toCardList.size) {
                list[i] = toCardList[i].replaceRange(4, 15, "********") + " / " + cardList[i].balance + " " + cardList[i].currency
            }
            builder.setItems(list) { _, which ->
                toCardOrMobileTextView.text = toCardList[which].replaceRange(4, 15, "********")
                creditorPosition = which
            }
            val dialog = builder.create()
            dialog.show()
        }

    }


    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        debitorCardToCardContainer.isClickable = !show
        creditorCardToCardContainer.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateSubmitImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.confirm_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateSubmitImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun animateProgressImage() {
        mainProgress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        mainProgress.startAnimation(rotate)
    }

    override fun onResume() {
        super.onResume()
        cardToCardShimmer.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        cardToCardShimmer.stopShimmerAnimation()
    }

}
