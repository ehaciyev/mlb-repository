package az.turanbank.mlb.presentation.ips.payrequest.requestor

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.ips.payrequest.requestor.account.ChooseIPSRequestedAccountFragment
import az.turanbank.mlb.presentation.ips.payrequest.requestor.alias.ChooseIPSRequestedAliasFragment

private val TAB_TITLES = arrayOf(
    R.string.account_title,
    R.string.alias
)
private val TAB_FRAGMENTS = arrayOf(
    ChooseIPSRequestedAccountFragment(),
    ChooseIPSRequestedAliasFragment()
)
class IPSPayRequestPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return TAB_FRAGMENTS[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount() = 2
}