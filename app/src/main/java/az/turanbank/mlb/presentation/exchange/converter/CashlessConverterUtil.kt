package az.turanbank.mlb.presentation.exchange.converter

import az.turanbank.mlb.data.remote.model.exchange.CashlessResponseModel
import javax.inject.Inject

class CashlessConverterUtil @Inject constructor(
) {
    fun convertedAmount(
        fromCurrency: CashlessResponseModel,
        toCurrency: CashlessResponseModel,
        amount: Double): Double{
        return (fromCurrency.cashlessSell*amount)/toCurrency.cashlessBuy
    }
}