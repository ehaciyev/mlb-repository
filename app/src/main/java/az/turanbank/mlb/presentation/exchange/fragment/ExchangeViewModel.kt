package az.turanbank.mlb.presentation.exchange.fragment

import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ExchangeViewModelInputs: BaseViewModelInputs {
    fun changeDate(choosenDate: String)
}
interface ExchangeViewModelOutputs: BaseViewModelOutputs {
    fun onDateChanged(): PublishSubject<String>
}

class ExchangeViewModel @Inject constructor(): BaseViewModel(),
    ExchangeViewModelInputs,
    ExchangeViewModelOutputs{

    override val inputs: ExchangeViewModelInputs = this

    override val outputs: ExchangeViewModelOutputs = this

    private val date = PublishSubject.create<String>()

    override fun changeDate(choosenDate: String) {
        date.onNext(choosenDate)
    }

    override fun onDateChanged() = date
}