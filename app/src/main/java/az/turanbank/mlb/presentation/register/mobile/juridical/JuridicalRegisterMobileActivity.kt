package az.turanbank.mlb.presentation.register.mobile.juridical

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.register.mobile.individual.VerifyIndividualRegisterMobileActivity
import az.turanbank.mlb.util.addPrefix
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_individual_register_mobile.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class JuridicalRegisterMobileActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: JuridicalRegisterMobileViewModel

    private val customerType by lazy { intent.getStringExtra("customerType") }

    private var compId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual_register_mobile)

        viewmodel =
            ViewModelProvider(this, factory)[JuridicalRegisterMobileViewModel::class.java]

        toolbar_title.text = getString(R.string.register_with_mobile)
        setTaxNumberVisibility()

        setInputListeners()
        setOutputListeners()

    }

    private fun setTaxNumberVisibility() {
        voen_holder.visibility = View.VISIBLE
    }

    private fun setOutputListeners() {

        viewmodel.outputs.showProgress().subscribe {
            showProgressBar(it)
        }.addTo(subscriptions)

        viewmodel.outputs.getCompId().subscribe{
            compId = it
        }.addTo(subscriptions)

        viewmodel.outputs.verifyOtpCode().subscribe {
            val intent = Intent(this, VerifyIndividualRegisterMobileActivity::class.java)
            intent.putExtra("custId", it)
            intent.putExtra("mobile", mobileNumber.text.toString().addPrefix())
            intent.putExtra("customerType", customerType)
            intent.putExtra("pin", pin.text.toString())
            intent.putExtra("taxNo", voen.text.toString())
            intent.putExtra("compId", compId)
            startActivity(intent)
            finish()
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        pin.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(7))
        submitButton.setOnClickListener {
            viewmodel.inputs.onCheckJuridicalCustomer(
                pin.text.toString(),
                mobileNumber.text.toString().addPrefix(),
                voen.text.toString()
            )
        }


        toolbar_back_button.setOnClickListener { onBackPressed() }
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        pin.isClickable = !show
        mobileNumber.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

}
