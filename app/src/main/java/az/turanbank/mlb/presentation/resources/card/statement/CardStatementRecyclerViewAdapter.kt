package az.turanbank.mlb.presentation.resources.card.statement

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.CardStatementModel
import kotlinx.android.synthetic.main.account_statement_item.view.*

class CardStatementRecyclerViewAdapter(
    private val cardStatementList: ArrayList<CardStatementModel>
): RecyclerView.Adapter<CardStatementRecyclerViewAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.account_statement_item,
            parent,
            false)
        )

    override fun getItemCount() = cardStatementList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(cardStatementList[position])

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(cardStatement: CardStatementModel) {
            itemView.accountStatementTransactionPurpose.text = cardStatement.purpose
            itemView.accountStatementTransactionCurrency.text = cardStatement.currency
            itemView.accountStatementTransactionDate.text = cardStatement.trDate

            if(cardStatement.crAmount == 0.0) {
                itemView.accountStatementTransactionAmount.text = cardStatement.dtAmount.toString()
                itemView.accountStatementTransactionCard.text = cardStatement.cardNumber
            } else if (cardStatement.dtAmount == 0.0) {
                itemView.accountStatementTransactionAmount.text = cardStatement.crAmount.toString()
                itemView.accountStatementTransactionCard.text = cardStatement.cardNumber
            }
        }
    }
}