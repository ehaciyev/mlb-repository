package az.turanbank.mlb.presentation.resources.account.exchange

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.exchange.CashlessResponseModel
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.response.GetOperationByIdResponseModel
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashlessUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.*
import az.turanbank.mlb.domain.user.usecase.resources.account.exchange.UpdateExchangeOperationUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface BuyForeignCurrencyViewModelInputs : BaseViewModelInputs {
    fun getAccountList()
    fun convertCurrency(buyAmount: Double, currencyAmount: Double, typeConverter: Int)
    fun changeRate(buyAmount: Double, sellAmount: Double, changeRate: Double)
    fun getOperationById(operId: Long)
    fun updateOperationById(
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int,
        operId: Long,
        operNameId: Long
    )
}

interface BuyForeignCurrencyViewModelOutputs : BaseViewModelOutputs {
    fun onAccountSuccess(): BehaviorSubject<ArrayList<AccountListModel>>
    fun onCashlessGot(): BehaviorSubject<ArrayList<CashlessResponseModel>>
    fun showProgress(): PublishSubject<Boolean>
    fun onConvertGot(): PublishSubject<Double>
    fun onRevertGot(): PublishSubject<Double>
    fun operationUpdated(): CompletableSubject
    fun operationDetails(): PublishSubject<GetOperationByIdResponseModel>
}

class BuyForeignCurrencyViewModel @Inject constructor(
    private val getIndividualAccountListUseCase: GetNoCardAccountListForIndividualCustomerUseCase,
    private val getJuridicalAccountListUseCase: GetNoCardAccountListForJuridicalCustomerUseCase,
    private val exchangeCashlessUseCase: ExchangeCashlessUseCase,
    private val getOperationByIdUseCase: GetOperationByIdUseCase,
    private val updateExchangeOperationUseCase: UpdateExchangeOperationUseCase,
    private val convertUtil: ConvertUtil,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    BuyForeignCurrencyViewModelInputs,
    BuyForeignCurrencyViewModelOutputs {
    override val inputs: BuyForeignCurrencyViewModelInputs = this

    override val outputs: BuyForeignCurrencyViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val showProgress = PublishSubject.create<Boolean>()

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val accountList = BehaviorSubject.create<ArrayList<AccountListModel>>()
    private val cashlessList = BehaviorSubject.create<ArrayList<CashlessResponseModel>>()

    private val operationDetails = PublishSubject.create<GetOperationByIdResponseModel>()
    val buyAccountList = BehaviorSubject.create<AccountListModel>()
    val sellAccountList = BehaviorSubject.create<AccountListModel>()
    private val converted = PublishSubject.create<Double>()
    private val reverted = PublishSubject.create<Double>()

    private val operationUpdated = CompletableSubject.create()

    override fun operationUpdated() = operationUpdated

    override fun getAccountList() {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getJuridicalAccountListUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                lang = enumLangType
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                        getCashless()
                    } else {
                        showProgress.onNext(false)
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getIndividualAccountListUseCase.execute(custId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                        getCashless()
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    error.onNext(1878)
                }, {
                    showProgress.onNext(false)
                }).addTo(subscriptions)
        }
    }

    override fun updateOperationById(
        dtAccountId: Long,
        dtAmount: Double,
        crAmount: Double,
        crIban: String,
        dtBranchId: Long,
        crBranchId: Long,
        exchangeRate: Double,
        exchangeOperationType: Int,
        operId: Long,
        operNameId: Long
    ) {
        showProgress.onNext(true)
        var custCompId: Long? = null

        if (isCustomerJuridical) {
            custCompId = compId
        }
        updateExchangeOperationUseCase
            .execute(
                custId,
                custCompId,
                token,
                dtAccountId,
                dtAmount,
                crAmount,
                crIban,
                dtBranchId,
                crBranchId,
                exchangeRate,
                exchangeOperationType,
                operId,
                operNameId
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            }, {
                showProgress.onNext(false)
            }).addTo(subscriptions)
    }

    private fun getCashless() {
        exchangeCashlessUseCase.execute("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    cashlessList.onNext(it.exchangeCashlessList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                showProgress.onNext(false)
            }).addTo(subscriptions)
    }

    override fun onCashlessGot() = cashlessList

    fun getAccountByIban(iban: String): AccountListModel? {
        accountList.value?.let {
            for (i in 0 until it.size) {
                if (it[i].iban == iban)
                    return it[i]
            }
        }
        return null
    }

    fun getBranchIdByIban(iban: String): Long? {
        accountList.value?.let {
            for (i in 0 until it.size) {
                if (it[i].iban == iban)
                    return it[i].branchId
            }
        }
        return 0L
    }


    fun getAccountIdByIban(iban: String): Long? {
        accountList.value?.let {
            for (i in 0 until it.size) {
                if (it[i].iban == iban)
                    return it[i].accountId
            }
        }
        return 0L
    }

    override fun convertCurrency(
        buyAmount: Double,
        currencyAmount: Double,
        typeConverter: Int
    ) {
        if (typeConverter == 0) {
            converted.onNext(convertUtil.convertedAmount(buyAmount, currencyAmount))
        } else if (typeConverter == 1) {
            reverted.onNext(convertUtil.revertedAmount(buyAmount, currencyAmount))
        }
    }

    override fun changeRate(buyAmount: Double, sellAmount: Double, changeRate: Double) {
        converted.onNext(
            convertUtil.onRateChangedAmount(
                sellAmount,
                buyAmount,
                changeRate
            ).split("|")[0].toDouble()
        )
    }

    override fun getOperationById(operId: Long) {
        showProgress.onNext(true)
        getOperationByIdUseCase.execute(operId, custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    operationDetails.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                showProgress.onNext(false)
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    override fun operationDetails() = operationDetails
    override fun showProgress() = showProgress
    override fun onConvertGot() = converted
    override fun onRevertGot() = reverted
    override fun onAccountSuccess() = accountList
}