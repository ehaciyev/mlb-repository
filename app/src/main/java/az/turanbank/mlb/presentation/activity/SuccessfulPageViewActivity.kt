package az.turanbank.mlb.presentation.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.outgoing.MyIncomePaymentsActivity
import az.turanbank.mlb.presentation.login.activities.LoginActivity
import kotlinx.android.synthetic.main.activity_non_registered_payment.*
import kotlinx.android.synthetic.main.activity_successful_page_view.*

class SuccessfulPageViewActivity : BaseActivity() {

    private val descriptionText : String by lazy { intent.getStringExtra("description") }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_successful_page_view)


        if(!TextUtils.isEmpty(descriptionText)){
            description.text = descriptionText
        }

        close.setOnClickListener {
            when {
                intent.getBooleanExtra("finishOnly", false) -> {
                    finish()
                }
                intent.getStringExtra("goToPage") == "myPayment" -> {
                    startActivity(Intent(this, MyIncomePaymentsActivity::class.java).putExtra("toHome",true))
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    setResult(Activity.RESULT_OK,Intent())
                    finish()
                }
                else -> {
                    val intent = Intent(this, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    setResult(Activity.RESULT_OK,Intent())
                    finish()
                }
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}
