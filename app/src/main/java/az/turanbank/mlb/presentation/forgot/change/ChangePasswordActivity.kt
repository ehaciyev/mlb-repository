package az.turanbank.mlb.presentation.forgot.change

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_change_password.repeatPassword
import kotlinx.android.synthetic.main.activity_login_username_password.password
import kotlinx.android.synthetic.main.content_individual_register_asan.progressImage
import kotlinx.android.synthetic.main.content_individual_register_asan.submitButton
import kotlinx.android.synthetic.main.content_individual_register_asan.submitText
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class ChangePasswordActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewmodel: ChangePasswordViewModel

    private val custId: Long by lazy { intent.getLongExtra("custId", 0L) }
    private val compId: Long by lazy { intent.getLongExtra("compId", 0L) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewmodel = ViewModelProvider(this, factory)[ChangePasswordViewModel::class.java]

        toolbar_title.text = getString(R.string.change_password)
        setInputListeners()
        setOutputListeners()
    }

    private fun setInputListeners() {
        submitButton.setOnClickListener {
            showProgressBar(true)
            viewmodel.inputs.changePassword(
                custId,
                compId,
                password.text.toString(),
                repeatPassword.text.toString()
            )
        }
    }

    private fun setOutputListeners() {
        viewmodel.outputs.onPasswordChanged().subscribe {
            Toast.makeText(this, getString(R.string.password_successfully_updated), Toast.LENGTH_LONG).show()
            finish()
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }


    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        password.isClickable = !show
        repeatPassword.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
