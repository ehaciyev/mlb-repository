package az.turanbank.mlb.presentation.activity

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_user_agreement.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

const val AGREED = 1
const val DISAGREED = -1

class UserAgreementActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_agreement)

        viewModel = ViewModelProvider(this, factory)[UserAgreementActivityViewModel::class.java]

        toolbar_back_button.setOnClickListener { onBackPressed() }
        toolbar_title.text = resources.getString(R.string.register_with_mobile)

        agree.setOnClickListener {
            setResult(AGREED)
            finish()
        }

        disagree.setOnClickListener {
            setResult(DISAGREED)
            finish()
        }
    }
}
