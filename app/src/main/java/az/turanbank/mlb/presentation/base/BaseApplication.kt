package az.turanbank.mlb.presentation.base

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.multidex.MultiDex
import az.turanbank.mlb.RuntimeLocaleChanger
import az.turanbank.mlb.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection

import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject


class BaseApplication : DaggerApplication(), HasAndroidInjector {
    @Inject
    lateinit var mFragmentInjector: DispatchingAndroidInjector<Any>

    /*override fun supportFragmentInjector(): AndroidInjector<Any> {
        return mFragmentInjector
    }*/

    override fun androidInjector(): AndroidInjector<Any> {
        return mFragmentInjector
    }

    override fun onCreate() {
        super.onCreate()
        RxJavaPlugins.setErrorHandler {
            it.printStackTrace()
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(RuntimeLocaleChanger.wrapContext(base))
        MultiDex.install(this);
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}