package az.turanbank.mlb.presentation.branch.fragment.list

import android.annotation.SuppressLint
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.branch.BranchMapResponseModel
import kotlinx.android.synthetic.main.branch_list_view.view.*


class BranchListAdapter(
    private val branchList: ArrayList<BranchMapResponseModel>,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<BranchListAdapter.BranchListViewHolder>(),
    Filterable {
    private var filteredBranchList = branchList
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BranchListViewHolder = BranchListViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.branch_list_view,
            parent,
            false
        )
    )

    override fun getItemCount() = filteredBranchList.size

    override fun onBindViewHolder(holder: BranchListViewHolder, position: Int) {
        holder.bind(filteredBranchList[position], clickListener)
    }


    class BranchListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(branch: BranchMapResponseModel, clickListener: (position: Int) -> Unit) {
            itemView.branchName.text = branch.branchName
            itemView.branchAddress.text = branch.address
            itemView.setOnClickListener { clickListener.invoke(adapterPosition) }

        }
    }

    override fun getFilter(): Filter? {
        return object : Filter() {
            @SuppressLint("DefaultLocale")
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val filterResult = arrayListOf<BranchMapResponseModel>()
                if(TextUtils.isEmpty(charSequence) || charSequence.trim().isEmpty()){
                    filterResult.addAll(branchList)
                } else {
                    val searchText = charSequence.toString().toLowerCase().trim()

                    branchList.forEach { branch ->
                        if (branch.address.toLowerCase().contains(searchText)
                            ||branch.branchName.toLowerCase().contains(searchText)){
                            filterResult.add(branch)
                        }
                    }
                }
                val filterResults = FilterResults()
                filterResults.values = filterResult
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                filteredBranchList = filterResults.values as ArrayList<BranchMapResponseModel>
                // refresh the list with filtered data
                notifyDataSetChanged()
            }
        }
    }
}