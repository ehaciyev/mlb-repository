package az.turanbank.mlb.presentation.register.asan.juridical

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetAuthInfoResponseModel
import az.turanbank.mlb.domain.user.register.usecase.GetAuthInfoUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.CheckPinForAsanJuridicalRegisterUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.GetPinFromAsanUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface JuridicalRegisterAsanViewModelInputs : BaseViewModelInputs {
    fun onSubmitClicked(phoneNumber: String, userId: String)
}

interface JuridicalRegisterAsanViewModelOutputs : BaseViewModelOutputs {
    fun verificationCode(): Observable<GetAuthInfoResponseModel>
    fun showProgress(): PublishSubject<Boolean>
}


class JuridicalRegisterAsanViewModel @Inject constructor(
    private val getPinFromAsanUseCase: GetPinFromAsanUseCase,
    private val checkPinForAsanJuridicalRegisterUseCase: CheckPinForAsanJuridicalRegisterUseCase,
    private val getAuthInfoUseCase: GetAuthInfoUseCase,
    sharedPreferences: SharedPreferences

) :
    BaseViewModel(),
    JuridicalRegisterAsanViewModelInputs,
    JuridicalRegisterAsanViewModelOutputs {

    override val inputs: JuridicalRegisterAsanViewModelInputs = this
    override val outputs: JuridicalRegisterAsanViewModelOutputs = this
    val showProgress = PublishSubject.create<Boolean>()

    private val verificationCode = PublishSubject.create<GetAuthInfoResponseModel>()

    private var custCode: Long = 0L
    private lateinit var custPin: String

    fun custCode() = custCode

    fun custPin() = custPin

    override fun showProgress() = showProgress

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun onSubmitClicked(phoneNumber: String, userId: String) {
        showProgress.onNext(true)
        getPinFromAsanUseCase.execute(phoneNumber, userId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                showProgress.onNext(false)

                if(it.status.statusCode == 1) {
                    checkPinForAsanJuridicalRegister (it.pin, phoneNumber, userId)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            })
            .addTo(subscriptions)
    }

    override fun verificationCode(): Observable<GetAuthInfoResponseModel> = verificationCode

    private fun checkPinForAsanJuridicalRegister (pin: String, phoneNumber: String, userId: String){
        showProgress.onNext(true)

        checkPinForAsanJuridicalRegisterUseCase.execute(pin)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    showProgress.onNext(false)

                    custCode = it.custCode
                    custPin = it.pin

                    if(it.status.statusCode == 1) {
                        getAuthInfo(phoneNumber, userId)
                    } else {
                        error.onNext(it.status.statusCode)                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
    }

    private fun getAuthInfo(phoneNumber: String, userId: String) {
        showProgress.onNext(true)
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        getAuthInfoUseCase.execute(phoneNumber, userId, enumLangType)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                showProgress.onNext(false)

                verificationCode.onNext(it)
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }
}