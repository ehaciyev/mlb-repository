package az.turanbank.mlb.presentation.cardoperations.cashbycode

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.CurrencySignMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.formatDecimal
import az.turanbank.mlb.util.secondPartOfMoney
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_verify_card_to_card.*
import kotlinx.android.synthetic.main.activity_verify_card_to_card.amount
import kotlinx.android.synthetic.main.activity_verify_card_to_card.bigAmountInteger
import kotlinx.android.synthetic.main.activity_verify_card_to_card.bigAmountReminder
import kotlinx.android.synthetic.main.activity_verify_card_to_card.progressImage
import kotlinx.android.synthetic.main.activity_verify_card_to_card.submitButton
import kotlinx.android.synthetic.main.activity_verify_card_to_card.submitText
import kotlinx.android.synthetic.main.activity_verify_card_to_card.toCardOrMobileText
import kotlinx.android.synthetic.main.activity_verify_payment.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class VerifyCashByCodeActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: VerifyCashByCodeViewModel

    private val fromCardNumber: String by lazy { intent.getStringExtra("fromCard") }
    private val currency: String by lazy { intent.getStringExtra("currency") }
    private val amountText: String by lazy { intent.getStringExtra("amount") }
    private val requestorCardNumber: String by lazy { intent.getStringExtra("requestorCardNumber") }

    private val fromCardId: Long by lazy { intent.getLongExtra("fromCardId", 0L) }
    private val toNumber: String by lazy { intent.getStringExtra("toNumber") }
    private val amountToSend: Double by lazy { intent.getDoubleExtra("amountToSend", 0.00) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_card_to_card)

        viewModel =
                ViewModelProvider(this, factory)[VerifyCashByCodeViewModel::class.java]
        // Make a little bit change to the view for making right UI
        viewChanging()


        setOutputListeners()
        setInputListeners()


        toolbar_title.text = getString(R.string.cash_by_code)

        toolbar_back_button.setOnClickListener { onBackPressed() }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onSuccessCashByCode().subscribe {
            showProgressBar(false)
            val intent = Intent()
            intent.putExtra("cashCode", it.cashCode)
            intent.putExtra("receiptNo", it.receiptNo)
            intent.putExtra("operationDate", it.operationDate)
            intent.putExtra("dtCardNumber", it.dtCardNumber)
            intent.putExtra("requestorCardNumber", requestorCardNumber)
            intent.putExtra("mobile", it.mobile)
            intent.putExtra("amount", it.amount)
            intent.putExtra("feeAmount", it.feeAmount)
            intent.putExtra("operationName", it.operationName)
            intent.putExtra("note", it.note)
            intent.putExtra("currency", it.currency)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }.addTo(subscriptions)
        viewModel.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        submitButton.setOnClickListener {
            showProgressBar(true)
            viewModel.inputs.submitCashByCode(fromCardId, toNumber, currency, amountToSend)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun viewChanging() {
        val curr = CurrencySignMapper().getCurrencySign(currency)

        toCardOrMobileText.text = getString(R.string.receiver_name)

        toCardOrMobileSpinner.text = toNumber

        fromCardSpinner.text = fromCardNumber

        amount.text = amountText.toDouble().formatDecimal()+" "+ currency

        bigAmountInteger.text = curr + firstPartOfMoney(amountText.toDouble())
        bigAmountReminder.text = secondPartOfMoney(amountText.toDouble())

    }



    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateSubmitImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateSubmitImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

}
