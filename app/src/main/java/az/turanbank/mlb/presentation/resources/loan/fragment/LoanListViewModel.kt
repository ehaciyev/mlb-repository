package az.turanbank.mlb.presentation.resources.loan.fragment

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.loan.LoanListModel
import az.turanbank.mlb.domain.user.usecase.resources.loan.list.GetIndividualLoansListUseCase
import az.turanbank.mlb.domain.user.usecase.resources.loan.list.GetJuridicalLoansListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface LoanListViewModelInputs : BaseViewModelInputs {
    fun getLoans()
}

interface LoanListViewModelOutputs : BaseViewModelOutputs {
    fun onLoanListGot(): PublishSubject<ArrayList<LoanListModel>>
}

class LoanListViewModel @Inject constructor(
    private val getIndividualLoansListUseCase: GetIndividualLoansListUseCase,
    private val getJuridicalLoansListUseCase: GetJuridicalLoansListUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
    LoanListViewModelInputs,
    LoanListViewModelOutputs {
    override val inputs: LoanListViewModelInputs = this

    override val outputs: LoanListViewModelOutputs = this

    private val loanList = PublishSubject.create<ArrayList<LoanListModel>>()

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    override fun getLoans() {
        if (isCustomerJuridical) {
            getJuridicalLoansListUseCase
                .execute(custId, compId, token, EnumLangType.EN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        loanList.onNext(it.loans)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },
                    {
                        error.onNext(1878)
                    }).addTo(subscriptions)
        } else {
            getIndividualLoansListUseCase.execute(custId, token,EnumLangType.EN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        loanList.onNext(it.loans)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },
                    {
                        error.onNext(1878)
                    }).addTo(subscriptions)
        }
    }

    override fun onLoanListGot() = loanList

}