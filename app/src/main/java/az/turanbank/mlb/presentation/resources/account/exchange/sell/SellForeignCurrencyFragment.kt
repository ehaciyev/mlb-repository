package az.turanbank.mlb.presentation.resources.account.exchange.sell


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseFragment

class SellForeignCurrencyFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_sell_foreign_currency, container, false)

        return view
    }


}
