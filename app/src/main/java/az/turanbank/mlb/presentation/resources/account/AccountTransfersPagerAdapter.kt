package az.turanbank.mlb.presentation.resources.account

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseFragment
private val TAB_TITLES = arrayOf(
    R.string.new_keyword,
    R.string.templates,
    R.string.history
)
class AccountTransfersPagerAdapter(
    private val pages: ArrayList<BaseFragment>, fm: FragmentManager,
    private val context: Context
) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int) = pages[position]

    override fun getPageTitle(position: Int) = context.getString(TAB_TITLES[position])

    override fun getCount() = pages.size


}