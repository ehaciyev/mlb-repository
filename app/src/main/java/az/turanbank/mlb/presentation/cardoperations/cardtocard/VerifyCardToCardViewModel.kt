package az.turanbank.mlb.presentation.cardoperations.cardtocard

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.cardoperations.CardToCardResponseModel
import az.turanbank.mlb.domain.user.usecase.cardoperations.CardToCardUseCase
import az.turanbank.mlb.domain.user.usecase.cardoperations.CardToOtherCardUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface VerifyCardToCardViewModelInputs: BaseViewModelInputs{
    fun cardToCard(requestorCardId: Long, destinationCardId: Long, currency: String, amount: Double?)
    fun cardToOtherCard(requestorCardId: Long, destinationCardNumber: String, currency: String, amount: Double?)
}
interface VerifyCardToCardViewModelOutputs: BaseViewModelOutputs{
    fun operationSuccess(): PublishSubject<CardToCardResponseModel>
}
class VerifyCardToCardViewModel @Inject constructor(
    private val cardToCardUseCase: CardToCardUseCase,
    private val cardToOtherCardUseCase: CardToOtherCardUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    VerifyCardToCardViewModelInputs,
    VerifyCardToCardViewModelOutputs {


    override val inputs: VerifyCardToCardViewModelInputs = this

    override val outputs: VerifyCardToCardViewModelOutputs = this

    val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ


    private val cardOperationSuccess = PublishSubject.create<CardToCardResponseModel>()

    override fun cardToCard(requestorCardId: Long, destinationCardId: Long, currency: String, amount: Double?) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        cardToCardUseCase
            .execute(custId, requestorCardId, destinationCardId, currency, amount, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    cardOperationSuccess.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun cardToOtherCard(
        requestorCardId: Long,
        destinationCardNumber: String,
        currency: String,
        amount: Double?
    ) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        cardToOtherCardUseCase
            .execute(custId, requestorCardId, destinationCardNumber, currency, amount, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    cardOperationSuccess.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }
    override fun operationSuccess() = cardOperationSuccess
}