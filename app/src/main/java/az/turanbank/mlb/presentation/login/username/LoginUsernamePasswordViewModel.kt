package az.turanbank.mlb.presentation.login.username

import android.annotation.SuppressLint
import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.login.IndividualLoginUsernamePasswordUseCase
import az.turanbank.mlb.domain.user.usecase.login.JuridicalLoginUsernamePasswordUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface LoginWithUsernamePasswordViewModelInputs : BaseViewModelInputs {
    fun checkUsernameAndPassword(username: String, password: String, customerType: String)
    fun keepExistingPinOrAdd(
        username: String,
        custId: Long,
        token: String,
        compId: Long?,
        companyName: String?,
        name: String?,
        surname: String?,
        patronymic: String?
    )
}

interface LoginWithUsernamePasswordViewModelOutputs : BaseViewModelOutputs {
    fun onUserLoggedIn(): CompletableSubject
    fun onUserBlocked(): CompletableSubject
    fun showPinDialog(): CompletableSubject
    fun showPinScreen(): Observable<Boolean>
}

class LoginUsernamePasswordViewModel @Inject constructor(
    private val individualLoginUsernamePasswordUseCase: IndividualLoginUsernamePasswordUseCase,
    private val juridicalLoginUsernamePasswordUseCase: JuridicalLoginUsernamePasswordUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
    LoginWithUsernamePasswordViewModelInputs,
    LoginWithUsernamePasswordViewModelOutputs {

    override fun showPinScreen() = showPinScreen

    override fun keepExistingPinOrAdd(
        username: String,
        custId: Long,
        token: String,
        compId: Long?,
        companyName: String?,
        name: String?,
        surname: String?,
        patronymic: String?
    ) {
        sharedPrefs.edit().putLong("custId", custId).apply()
        sharedPrefs.edit().putString("username", username).apply()
        sharedPrefs.edit().putString("token", token).apply()
        sharedPrefs.edit().putString("fullname", "$surname $name $patronymic").apply()

        compId?.let {
            sharedPrefs.edit().putLong("compId", compId).apply()
        }
        companyName?.let {
            sharedPrefs.edit().putString("companyName", companyName).apply()
        }
    }

    private val userLoggedIn = CompletableSubject.create()
    private val userBlocked = CompletableSubject.create()
    private val showPinDialog = CompletableSubject.create()
    private val showPinScreen = PublishSubject.create<Boolean>()

    override val inputs: LoginWithUsernamePasswordViewModelInputs = this
    override val outputs: LoginWithUsernamePasswordViewModelOutputs = this

    @SuppressLint("CheckResult")
    override fun checkUsernameAndPassword(
        username: String,
        password: String,
        customerType: String
    ) {
        if (customerType == "individual") {
            individualLoginUsernamePasswordUseCase.execute(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    when (it.status.statusCode) {
                        1 -> {

                            sharedPrefs.edit().putBoolean("isCustomerJuridical", false).apply()
                            sharedPrefs.edit().putBoolean("loggedInWithAsan", false).apply()
                            sharedPrefs.edit().putLong("compId", 0L).apply()

                            if (pinIsSet()) {
                                if (shouldShowPinScreen(username, it.custId, null)) {
                                    keepExistingPinOrAdd(
                                        username,
                                        it.custId,
                                        it.token,
                                        null,
                                        null,
                                        name = it.name,
                                        surname = it.surname,
                                        patronymic = it.patronymic
                                    )

                                    showPinScreen.onNext(false)
                                } else {
                                    keepExistingPinOrAdd(
                                        username,
                                        it.custId,
                                        it.token,
                                        null,
                                        null,
                                        name = it.name,
                                        surname = it.surname,
                                        patronymic = it.patronymic
                                    )
                                    showPinDialog.onComplete()
                                }
                            }
                            else {
                                showPinScreen.onNext(true)
                                keepExistingPinOrAdd(
                                    username,
                                    it.custId,
                                    it.token,
                                    null,
                                    null,
                                    name = it.name,
                                    surname = it.surname,
                                    patronymic = it.patronymic
                                )
                            }
                            userLoggedIn.onComplete()
                        }
                        119 -> userBlocked.onComplete()
                        else -> error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                })

        } else if (customerType == "juridical") {
            juridicalLoginUsernamePasswordUseCase.execute(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    when (it.status.statusCode) {
                        1 -> {
                            sharedPrefs.edit().putBoolean("isCustomerJuridical", true).apply()
                            sharedPrefs.edit().putString("taxNo", it.company.taxNo).apply()
                            sharedPrefs.edit().putBoolean("loggedInWithAsan", false).apply()

                            if (pinIsSet()) {
                                if (shouldShowPinScreen(
                                        username,
                                        it.customer.custId,
                                        it.company.id
                                    )
                                ) {
                                    keepExistingPinOrAdd(
                                        username = username,
                                        compId = it.company.id,
                                        companyName = it.company.name,
                                        custId = it.customer.custId,
                                        token = it.customer.token,
                                        name = it.customer.name,
                                        surname = it.customer.surname,
                                        patronymic = it.customer.patronymic
                                    )
                                    showPinScreen.onNext(false)
                                } else {
                                    keepExistingPinOrAdd(
                                        username = username,
                                        compId = it.company.id,
                                        companyName = it.company.name,
                                        custId = it.customer.custId,
                                        token = it.customer.token,
                                        name = it.customer.name,
                                        surname = it.customer.surname,
                                        patronymic = it.customer.patronymic
                                    )
                                    showPinDialog.onComplete()
                                }
                            } else {
                                keepExistingPinOrAdd(
                                    username = username,
                                    compId = it.company.id,
                                    companyName = it.company.name,
                                    custId = it.customer.custId,
                                    token = it.customer.token,
                                    name = it.customer.name,
                                    surname = it.customer.surname,
                                    patronymic = it.customer.patronymic
                                )
                                showPinScreen.onNext(true)
                            }
                            userLoggedIn.onComplete()
                        }
                        119 -> userBlocked.onComplete()
                        else -> error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
        }
    }

    private fun shouldShowPinScreen(username: String, custId: Long, compId: Long?): Boolean {
        return (username == sharedPrefs.getString("username", null)
                && custId == sharedPrefs.getLong("custId", 0L)
                && compId == sharedPrefs.getLong("compId", 0L))
    }

    private fun pinIsSet() = sharedPrefs.getBoolean("pinIsSet", false)

    override fun onUserLoggedIn() = userLoggedIn

    override fun onUserBlocked() = userBlocked

    override fun showPinDialog() = showPinDialog
}
