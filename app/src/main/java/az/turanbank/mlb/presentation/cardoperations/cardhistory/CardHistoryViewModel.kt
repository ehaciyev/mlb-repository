package az.turanbank.mlb.presentation.cardoperations.cardhistory

import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import javax.inject.Inject

interface CardHistoryViewModelInputs: BaseViewModelInputs {

}
interface CardHistoryViewModelOutputs: BaseViewModelOutputs{

}
class CardHistoryViewModel @Inject constructor(

): BaseViewModel(),
        CardHistoryViewModelInputs,
        CardHistoryViewModelOutputs{
    override val inputs: CardHistoryViewModelInputs = this

    override val outputs: CardHistoryViewModelOutputs = this
}