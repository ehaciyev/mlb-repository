package az.turanbank.mlb.presentation.resources.card.list

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.domain.user.usecase.resources.card.GetCardListForJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetIndividualCardListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface CardViewModelInputs : BaseViewModelInputs {
    fun getIndividualCards()
}

interface CardViewModelOutputs : BaseViewModelOutputs {
    fun onCardListGot(): PublishSubject<ArrayList<CardListModel>>
}

class CardViewModel @Inject constructor(
    private val getIndividualCardListUseCase: GetIndividualCardListUseCase,
    private val getJuridicalCardListUseCase: GetCardListForJuridicalUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
    CardViewModelInputs,
    CardViewModelOutputs {
    override val inputs: CardViewModelInputs = this

    override val outputs: CardViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    val cardList = PublishSubject.create<ArrayList<CardListModel>>()
    override fun getIndividualCards() {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if (isCustomerJuridical) {
            getJuridicalCardListUseCase.execute(custId, compId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        cardList.onNext(it.cardList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },
                    {
                        error.onNext(1878)
                    }).addTo(subscriptions)
        } else {
            getIndividualCardListUseCase.execute(custId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        cardList.onNext(it.cardList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },
                    {
                        error.onNext(1878)
                    }).addTo(subscriptions)
        }
    }

    override fun onCardListGot() = cardList

}