package az.turanbank.mlb.presentation.ips.register.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.AccountResponseModel
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAccountsUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetUnregisteredIpsAccountsUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetNoCardAccountListForIndividualCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.data.SelectAccountModel
import az.turanbank.mlb.presentation.data.SelectUnregisteredAccountModel
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface InstantPaymentAccountMultiSelectViewModelInputs: BaseViewModelInputs{
    fun getAccounts()
}
interface InstantPaymentAccountMultiSelectViewModelOutputs: BaseViewModelOutputs{
    fun onAccountListSuccess(): PublishSubject<ArrayList<SelectUnregisteredAccountModel>>
}

class InstantPaymentAccountMultiSelectViewModel @Inject constructor(
    sharedPrefs: SharedPreferences,
    private val getUnregisteredIpsAccountsUseCase: GetUnregisteredIpsAccountsUseCase
): BaseViewModel(),
InstantPaymentAccountMultiSelectViewModelInputs,
InstantPaymentAccountMultiSelectViewModelOutputs{

    override val inputs: InstantPaymentAccountMultiSelectViewModelInputs = this

    override val outputs: InstantPaymentAccountMultiSelectViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)

    private val accountList = PublishSubject.create<ArrayList<SelectUnregisteredAccountModel>>()
    private val accounts = arrayListOf<SelectUnregisteredAccountModel>()
    private val compId = sharedPrefs.getLong("compId", 0L)

    override fun getAccounts() {
        if (!isCustomerJuridical){
            getUnregisteredIpsAccountsUseCase
                .execute(custId, token, EnumLangType.AZ)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({accountsResponse ->
                    if (accountsResponse.status.statusCode == 1) {
                        val filteredList = arrayListOf<AccountResponseModel>()
                        accountsResponse.accountList.forEach {
                            if (it.currName == "AZN")
                                filteredList.add(it)
                        }
                        filteredList.forEach { accounts.add(SelectUnregisteredAccountModel(it, false)) }
                        accountList.onNext(accounts)
                    } else {
                        error.onNext(accountsResponse.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onAccountListSuccess() = accountList
}