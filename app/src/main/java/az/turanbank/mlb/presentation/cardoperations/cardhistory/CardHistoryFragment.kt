package az.turanbank.mlb.presentation.cardoperations.cardhistory


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import javax.inject.Inject

class CardHistoryFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardHistoryViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_card_history, container, false)

        viewModel =
            ViewModelProvider(this, factory)[CardHistoryViewModel::class.java]


        setOutputListeners()

        setInputListeners()

        return view
    }

    private fun setInputListeners() {

    }

    private fun setOutputListeners() {
        
    }


}
