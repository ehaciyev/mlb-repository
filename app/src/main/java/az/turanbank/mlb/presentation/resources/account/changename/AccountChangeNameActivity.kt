package az.turanbank.mlb.presentation.resources.account.changename

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_change_name.*
import javax.inject.Inject

class AccountChangeNameActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: AccountChangeNameActivityViewModel

    private val accountId: Long by lazy { intent.getLongExtra("accountId",0L) }
    private val accountName: String by lazy { intent.getStringExtra("accountName") }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_change_name)

        viewModel = ViewModelProvider(this,factory)[AccountChangeNameActivityViewModel::class.java]

        toolbar_title.setText(R.string.change_name_text)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        changeNameText.setText(accountName)

        okButton.setOnClickListener{
            changeAccountName(accountId, changeNameText.text.toString())
        }

        setInputListeners()
        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onChangeNameSuccess().subscribe{
            if(it){
                Toast.makeText(this,getString(R.string.successfull_changed_name), Toast.LENGTH_SHORT).show()
                finish()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe{
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {

    }

    private fun changeAccountName(accountId: Long, accountName: String) {
        if (checkValidation()){
            viewModel.inputs.changeName(accountId, accountName)
        }
    }

    private fun checkValidation(): Boolean {
        var isValid = true
        if(changeNameText.text.toString().isNullOrEmpty()){
            changeNameText.error = getString(R.string.empty_name)
            return isValid
        }
        return isValid
    }
}