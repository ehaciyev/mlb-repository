package az.turanbank.mlb.presentation.exchange.fragment.centralbank

import az.turanbank.mlb.data.remote.model.exchange.ExchangeCentralBankResponseModel
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCentralBankUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ExchangeCentralBankViewModelInputs: BaseViewModelInputs {
    fun getCentralBankExchange(exchangeDate: String?)
}
interface ExchangeCentralBankViewModelOutputs: BaseViewModelOutputs {
    fun onCashExchangeGot(): PublishSubject<ExchangeCentralBankResponseModel>
}

class ExchangeCentralBankViewModel @Inject constructor(
    private val exchangCentralBankUseCase: ExchangeCentralBankUseCase
): BaseViewModel(),
    ExchangeCentralBankViewModelInputs,
    ExchangeCentralBankViewModelOutputs {
    override val inputs: ExchangeCentralBankViewModelInputs = this

    override val outputs: ExchangeCentralBankViewModelOutputs = this


    private val centralBankExchange = PublishSubject.create<ExchangeCentralBankResponseModel>()
    override fun getCentralBankExchange(exchangeDate: String?) {

        exchangCentralBankUseCase.execute(exchangeDate)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({
                if (it.status.statusCode == 1) {
                    centralBankExchange.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }
            ).addTo(subscriptions)
    }

    override fun onCashExchangeGot() = centralBankExchange
}