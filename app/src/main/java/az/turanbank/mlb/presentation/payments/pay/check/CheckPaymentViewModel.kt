package az.turanbank.mlb.presentation.payments.pay.check

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.CheckMerchantResponseModel
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.domain.user.usecase.pg.check.CheckMerchantIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetCardListForJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.GetIndividualCardListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

interface CheckPaymentViewModelInputs : BaseViewModelInputs {
    fun getCards()
    fun check(
        merchantId: Long,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        categoryId: Long,
        providerId: Long
    )
}

interface CheckPaymentViewModelOutputs : BaseViewModelOutputs {
    fun cardListSuccess(): PublishSubject<ArrayList<CardListModel>>
    fun onCheckCompleted(): PublishSubject<CheckMerchantResponseModel>
    fun cardListNull(): PublishSubject<Boolean>
    fun showProgress(): PublishSubject<Boolean>
}

class CheckPaymentViewModel @Inject constructor(
    private val getIndividualCardListUseCase: GetIndividualCardListUseCase,
    private val getCardListForJuridicalUseCase: GetCardListForJuridicalUseCase,
    private val checkMerchantIndividualUseCase: CheckMerchantIndividualUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(), CheckPaymentViewModelInputs, CheckPaymentViewModelOutputs {
    override val inputs: CheckPaymentViewModelInputs = this

    override val outputs: CheckPaymentViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val lang = sharedPrefs.getString("lang", "az")
    var enumLangType = EnumLangType.AZ
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val cardList = PublishSubject.create<ArrayList<CardListModel>>()

    private var isCardListNull = PublishSubject.create<Boolean>()

    private val check = PublishSubject.create<CheckMerchantResponseModel>()

    private val showProgress = PublishSubject.create<Boolean>()

    override fun check(
        merchantId: Long,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        categoryId: Long,
        providerId: Long
    ) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        showProgress.onNext(true)
        checkMerchantIndividualUseCase
            .execute(
                custId,
                token,
                identificationType,
                identificationCode,
                merchantId,
                categoryId,
                enumLangType,
                providerId
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    check.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun showProgress() = showProgress
    override fun onCheckCompleted() = check
    override fun cardListNull()  = isCardListNull

    override fun getCards() {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        val respond = if (isCustomerJuridical) {
            getCardListForJuridicalUseCase.execute(custId, compId, token, enumLangType)
        } else {
            getIndividualCardListUseCase.execute(custId, token,enumLangType)
        }
        respond.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.cardList == null){
                    isCardListNull.onNext(true)
                } else{
                    cardList.onNext(it.cardList)
                    isCardListNull.onNext(false)
                }
            }, {
                isCardListNull.onNext(false)

            }).addTo(subscriptions)
    }

    override fun cardListSuccess() = cardList
}