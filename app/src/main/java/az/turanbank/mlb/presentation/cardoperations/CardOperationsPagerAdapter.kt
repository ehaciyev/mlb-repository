package az.turanbank.mlb.presentation.cardoperations

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.cardoperations.templates.CardTemplateListFragment

private val TAB_TITLES = arrayOf(
    R.string.new_keyword,
    R.string.templates
   // R.string.operations
)
private val TAB_FRAGMENTS = arrayOf(
    NewOperationsFragment(),
    CardTemplateListFragment()
    //CardHistoryFragment()
)

class CardOperationsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return TAB_FRAGMENTS[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return 2
    }
}