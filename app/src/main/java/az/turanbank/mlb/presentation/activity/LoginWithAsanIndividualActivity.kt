package az.turanbank.mlb.presentation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import az.turanbank.mlb.R
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class LoginWithAsanIndividualActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_with_asan_individual)
        toolbar_back_button.setOnClickListener { onBackPressed() }
    }
}
