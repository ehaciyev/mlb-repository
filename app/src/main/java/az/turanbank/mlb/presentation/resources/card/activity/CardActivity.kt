package az.turanbank.mlb.presentation.resources.card.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.local.PlasticCardMapper
import az.turanbank.mlb.data.remote.model.resources.card.CardListResponseModel
import az.turanbank.mlb.data.remote.model.response.CardStatementModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.adapter.MlbHomeCardStatementRecyclerViewAdapter
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.cardoperations.CardOperationsActivity
import az.turanbank.mlb.presentation.resources.card.requisit.CardRequisitesActivity
import az.turanbank.mlb.presentation.resources.card.sms.SmsActivity
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementInternalActivity
import az.turanbank.mlb.presentation.resources.card.statement.CardStatementTabbedActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RapidFloatingActionContentLabelList
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_card.*
import kotlinx.android.synthetic.main.activity_card.date
import kotlinx.android.synthetic.main.activity_card.pay
import kotlinx.android.synthetic.main.activity_card.payImage
import kotlinx.android.synthetic.main.activity_card.requisite
import kotlinx.android.synthetic.main.activity_card.requisiteImage
import kotlinx.android.synthetic.main.activity_card.statement
import kotlinx.android.synthetic.main.activity_card.statementImage
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class CardActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardActivityViewModel

    private val cardId: Long by lazy { intent.getLongExtra("cardId", 0L) }
    private val shortCardNumber: String by lazy { intent.getStringExtra("shortCardNumber") }
    private var blockReasons: Array<String> = arrayOf()

    private lateinit var card: CardListResponseModel

    private var cardBlockedForListeners = false

    private var blockReasonPos = -1

    lateinit var fabAdd: FloatingActionButton
    lateinit var requisiteFabContainer: LinearLayout
    lateinit var pinFabContainer: LinearLayout
    lateinit var limitFabContainer: LinearLayout
    lateinit var extensionContainer: LinearLayout

    private val symbols = DecimalFormatSymbols()
    private val decimalFormat = DecimalFormat("0.00")

    var isFabOpen = false


    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card)

        viewModel = ViewModelProvider(this, factory)[CardActivityViewModel::class.java]


        /*val rfaContent = RapidFloatingActionContentLabelList(applicationContext)
        rfaContent.setOnRapidFloatingActionContentLabelListListener(this)*/

        fabAdd = findViewById(R.id.fabAdd)
        requisiteFabContainer = findViewById(R.id.requisiteFabContainer)
        pinFabContainer = findViewById(R.id.pinFabContainer)
        limitFabContainer = findViewById(R.id.limitFabContainer)
        extensionContainer = findViewById(R.id.extensionContainer)

        cardBlockedForListeners = intent.getBooleanExtra("cardBlocked", false)

        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = '.'
        decimalFormat.decimalFormatSymbols = symbols

        /*defaultFab()
        fabAdd.setOnClickListener{
            if(isFabOpen){
                closeAnimFab()
            } else {
                openAnimFab()
            }
        }*/


        setOutputListeners()
        setInputListeners()

        writeDataToView()

        //clickListeners()

    }

    private fun clickListeners() {
        requisiteFabContainer.setOnClickListener{

        }

        pinFabContainer.setOnClickListener{

        }

        limitFabContainer.setOnClickListener{

        }

        extensionContainer.setOnClickListener{

        }
    }

    private fun openAnimFab() {
        cardBackgroundColor.visibility = View.VISIBLE
        cardBackgroundColor.setOnClickListener{
            closeAnimFab()
        }
        fabAdd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_forward))
        requisiteFabContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_open))
        pinFabContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_open))
        limitFabContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_open))
        extensionContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_open))
        requisiteFabContainer.isClickable = true
        pinFabContainer.isClickable = true
        limitFabContainer.isClickable = true
        extensionContainer.isClickable = true
        isFabOpen = true;
    }

    private fun closeAnimFab() {
        cardBackgroundColor.visibility = View.GONE
        fabAdd.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_backward))
        requisiteFabContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_close))
        pinFabContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_close))
        limitFabContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_close))
        extensionContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_close))
        requisiteFabContainer.isClickable = false
        pinFabContainer.isClickable = false
        limitFabContainer.isClickable = false
        extensionContainer.isClickable = false
        isFabOpen = false
    }


    private fun defaultFab() {
        requisiteFabContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_close))
        pinFabContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_close))
        limitFabContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_close))
        extensionContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fab_close))
        requisiteFabContainer.isClickable = false
        pinFabContainer.isClickable = false
        limitFabContainer.isClickable = false
        extensionContainer.isClickable = false
    }

    @SuppressLint("SetTextI18n")
    private fun writeDataToView() {
        toolbar_title.text = "MasterCard"
        toolbar_back_button.setOnClickListener { onBackPressed() }

        plasticCardNumber.text = shortCardNumber

        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val currentDate = sdf.format(Date()).replace("-", ".")
        date.text = currentDate
    }

    @SuppressLint("SetTextI18n")
    private fun setViewForBlockedCard(block: Boolean) {
        if (block) {
            view.visibility = View.VISIBLE
            payImage.setColorFilter(ContextCompat.getColor(this, R.color.disabled_color))
            requisiteImage.setColorFilter(ContextCompat.getColor(this, R.color.disabled_color))
            statementImage.setColorFilter(ContextCompat.getColor(this, R.color.disabled_color))
            smsImage.setColorFilter(ContextCompat.getColor(this, R.color.disabled_color))
            blockImage.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.disabled_block_color
                )
            )

            payText.setTextColor(ContextCompat.getColor(this, R.color.disabled_color))
            requisiteText.setTextColor(ContextCompat.getColor(this, R.color.disabled_color))
            statementText.setTextColor(ContextCompat.getColor(this, R.color.disabled_color))
            smsText.setTextColor(ContextCompat.getColor(this, R.color.disabled_color))
            blockText.text = getString(R.string.unblock)
        } else {
            view.visibility = View.GONE
            if (viewModel.isCustomerJuridical) {
                payImage.setColorFilter(ContextCompat.getColor(this, R.color.disabled_color))
                payText.setTextColor(ContextCompat.getColor(this, R.color.disabled_color))
            } else {
                payImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
                payText.setTextColor(ContextCompat.getColor(this, R.color.login_text_color))
            }
            requisiteImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
            statementImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
            smsImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
            blockImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))

            requisiteText.setTextColor(ContextCompat.getColor(this, R.color.login_text_color))
            statementText.setTextColor(ContextCompat.getColor(this, R.color.login_text_color))
            smsText.setTextColor(ContextCompat.getColor(this, R.color.login_text_color))
            blockText.text = getString(R.string.block)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setOutputListeners() {
        viewModel.outputs.onCardSuccess().subscribe {
            plastic_card.setImageResource(PlasticCardMapper().resolveCard(it.cardType))
            card = it
            viewModel.inputs.getCardStatement(card.cardNumber)
            plasticCardAmountInteger.text = firstPartOfMoney(card.balance)
            plasticCardAmountReminder.text = secondPartOfMoney(card.balance) + card.currency
            //plasticCardAmount.text = splitString(card.balance.formatDecimal(),'.').toString() + " " + card.currency
                //card.balance.formatDecimal() + " " + card.currency
            plasticAccountNumber.text = card.correspondentAccount
            plasticCardExpireDate.text =
                card.expiryDate.substring(5, 7) + "/" + card.expiryDate.substring(2, 4)

            val blocked: Boolean = it.cardStatus == "0"
            setViewForBlockedCard(blocked)
            cardBlockedForListeners = blocked

            progress.clearAnimation()
            progress.visibility = View.GONE
            constraintContainer.visibility = View.VISIBLE
            plasticCardInclude.visibility = View.VISIBLE
        }.addTo(subscriptions)

        viewModel.outputs.cardStatementSuccess().subscribe {
            setStatementAdapter(it)
        }.addTo(subscriptions)

        viewModel.outputs.blockReasonsSuccess().subscribe {
            blockReasons = it
        }.addTo(subscriptions)

        viewModel.outputs.blockCardSuccess().subscribe {
            cardBlockedForListeners = it
            if (it) {
                Toast.makeText(this, getString(R.string.card_blocked), Toast.LENGTH_LONG)
                    .show()
            } else {
                Toast.makeText(this, R.string.card_unblocked, Toast.LENGTH_LONG)
                    .show()
            }
            viewModel.inputs.getTheCard(cardId)
            animateProgressImage()
        }.addTo(subscriptions)

        viewModel.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            constraintContainer.visibility = View.VISIBLE
            plasticCardInclude.visibility = View.VISIBLE
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)

        viewModel.outputs.onStatementError().subscribe {
            recyclerView.visibility = View.GONE
            recyclerViewFail.text = getString(ErrorMessageMapper().getErrorMessage(it))
            recyclerViewFail.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    override fun onResume() {
        super.onResume()
        /*viewModel.inputs.getTheCard(cardId)
        viewModel.inputs.getBlockReasons()*/
    }

    private fun firstPartOfMoney(amount: Double) =
        decimalFormat.format(amount).toString().split(".")[0] + ","

    private fun secondPartOfMoney(amount: Double) =
        decimalFormat.format(amount).toString().split(".")[1] + " "

    private fun setStatementAdapter(list: ArrayList<CardStatementModel>) {
        val adapter =
            MlbHomeCardStatementRecyclerViewAdapter(list) {
                val intent = Intent(this, CardStatementInternalActivity::class.java)
                intent.putExtra("date", list[it].trDate)
                intent.putExtra("cardNumber", list[it].cardNumber)
                intent.putExtra("debetAmount", list[it].dtAmount)
                intent.putExtra("creditAmount", list[it].crAmount)
                intent.putExtra("purpose", list[it].purpose)
                intent.putExtra("currency", list[it].currency)
                intent.putExtra("fee", list[it].fee)
                startActivity(intent)
            }
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = adapter
    }

    private fun setInputListeners() {
        animateProgressImage()
        constraintContainer.visibility = View.GONE
        plasticCardInclude.visibility = View.GONE

        viewModel.inputs.getTheCard(cardId)

        viewModel.inputs.getBlockReasons()
        block.setOnClickListener {
            if (!cardBlockedForListeners) {
                openBlockCardActions()
            } else {
                constraintContainer.visibility = View.GONE
                plasticCardInclude.visibility = View.GONE
                animateProgressImage()
                viewModel.inputs.unblockCard(card.cardId)
            }
        }
        sms.setOnClickListener {
            if (!cardBlockedForListeners) {
                val intent = Intent(this, SmsActivity::class.java)
                intent.putExtra("cardId", card.cardId)
                startActivity(intent)
            }
        }
        requisite.setOnClickListener {
            if (!cardBlockedForListeners) {
                val intent = Intent(this, CardRequisitesActivity::class.java)
                intent.putExtra("cardId", card.cardId)
                intent.putExtra("cardNumber", card.cardNumber)
                startActivity(intent)
            }
        }
        statement.setOnClickListener {
            if (!cardBlockedForListeners) {
                val intent = Intent(this, CardStatementTabbedActivity::class.java)
                intent.putExtra("cardId", card.cardId)
                intent.putExtra("cardNumber", card.cardNumber)
                startActivity(intent)
            }
        }
        pay.setOnClickListener {
            if (!cardBlockedForListeners && !viewModel.isCustomerJuridical) {
                val intent = Intent(this, CardOperationsActivity::class.java)
                intent.putExtra("cardId", card.cardId)
                intent.putExtra("cardNumber", card.cardNumber)
                startActivity(intent)
            }
        }
    }

    private fun openBlockCardActions() {
        val blockDialogBuilder = AlertDialog.Builder(this)
        blockDialogBuilder.setTitle(getString(R.string.block_reason))

        blockDialogBuilder.setSingleChoiceItems(
            blockReasons, -1
        ) { _, item ->
            blockReasonPos = item
        }
        blockDialogBuilder.setNegativeButton(getString(R.string.cancel_all_caps)) { dialog, _ ->
            dialog.dismiss()
        }
        blockDialogBuilder.setPositiveButton(getString(R.string.proceed_all_caps)) { _, _ ->
            if (blockReasonPos >= 0) {
                constraintContainer.visibility = View.GONE
                plasticCardInclude.visibility = View.GONE
                animateProgressImage()
                viewModel.inputs.blockCard(card.cardId, blockReasons[blockReasonPos])
            } else {
                Toast.makeText(this, R.string.please_choose_block_reason, Toast.LENGTH_LONG)
                    .show()
            }
        }
        val alert = blockDialogBuilder.create()
        alert.show()
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
