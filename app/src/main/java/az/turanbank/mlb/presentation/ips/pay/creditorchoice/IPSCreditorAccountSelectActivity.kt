package az.turanbank.mlb.presentation.ips.pay.creditorchoice

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.activity.SuccessfulPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_ipscreditor_account_select.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class IPSCreditorAccountSelectActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ipscreditor_account_select)
        toolbar_title.text = getString(R.string.pay_receiver)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        view_pager.adapter =
            IPSCreditorAccountSelectPagerAdapter(
                this,
                supportFragmentManager
            )
        tabs.setupWithViewPager(view_pager)
    }
}