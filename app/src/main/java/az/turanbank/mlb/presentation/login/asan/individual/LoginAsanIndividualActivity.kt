package az.turanbank.mlb.presentation.login.asan.individual

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.addPrefix
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_login_asan_individual.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class LoginAsanIndividualActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewModel: LoginAsanIndividualViewModel

    private lateinit var textWatcher: TextWatcher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_asan_individual)

        viewModel = ViewModelProvider(this, factory)[LoginAsanIndividualViewModel::class.java]

        toolbar_back_button.setOnClickListener { onBackPressed() }
        buttonEnabled(false)

        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!asanPhoneNumber.text.toString().isNullOrEmpty() && !asanId.text.toString().isNullOrEmpty()) {
                    buttonEnabled(true)
                    submitButton.setOnClickListener {
                        viewModel.inputs.getPinFromAsan(
                            asanPhoneNumber.text.toString().addPrefix(),
                            asanId.text.toString()
                        )
                    }
                } else {
                    buttonEnabled(false)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        asanPhoneNumber.addTextChangedListener(textWatcher)
        asanId.addTextChangedListener(textWatcher)

        setInputListeners()
        setOutputListeners()
    }

    private fun buttonEnabled(enabled: Boolean) {
        if (!enabled) {
            submitButton.isClickable = false
            submitButton.isEnabled = false
            submitButton.isFocusable = false
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
        } else {
            submitButton.isClickable = true
            submitButton.isEnabled = true
            submitButton.isFocusable = true
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
    }

    private fun setOutputListeners() {

        viewModel.outputs.showProgress().subscribe {
            if(it) {
                showProgressBar(true)
            } else {
                showProgressBar(false)
            }
        }.addTo(subscriptions)

        viewModel.outputs.verificationCode().subscribe { getAuthInfo ->
            run {
                val intent = Intent(this, LoginIndividualAsanPinActivity::class.java)

                intent.putExtra("verificationCode", getAuthInfo.verificationCode)
                intent.putExtra("certificate", getAuthInfo.certificate)
                intent.putExtra("challenge", getAuthInfo.challenge)
                intent.putExtra("transactionId", getAuthInfo.transactionId)
                intent.putExtra("custCode", viewModel.custCode())
                intent.putExtra("pin", viewModel.custPin())
                intent.putExtra("custId", viewModel.custId())
                intent.putExtra("phoneNumber", asanPhoneNumber.text.toString().addPrefix())
                intent.putExtra("asanId", asanId.text.toString())

                startActivity(intent)
                showProgressBar(false)
            }
        }.addTo(subscriptions)

        viewModel.outputs.onUserBlocked().subscribe {
            val intent = Intent(this, ErrorPageViewActivity::class.java)
            intent.putExtra("customerType", "individual")
            intent.putExtra("unblock", true)
            startActivity(intent)
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
     /*   submitButton.setOnClickListener {
            showProgressBar(true)
            viewModel.inputs.getPinFromAsan(
                asanPhoneNumber.text.toString().addPrefix(),
                asanId.text.toString()
            )
        }*/
    }
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if(show)
        {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
