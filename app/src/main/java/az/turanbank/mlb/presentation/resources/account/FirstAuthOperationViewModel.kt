package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.request.GetSignInfoResponseModel
import az.turanbank.mlb.data.remote.model.response.BatchOperation
import az.turanbank.mlb.domain.user.usecase.resources.account.GetFirstAuthOperationListUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetFirstSignInfoUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


interface FirstAuthOperationViewModelInputs : BaseViewModelInputs {
    fun getFirstAuthOperationList()
    fun getFirstSignInfo(batchId: Long)
}

interface FirstAuthOperationViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun authOperationListSuccess(): PublishSubject<ArrayList<BatchOperation>>
    fun getFirstSignInfoSuccess(): PublishSubject<GetSignInfoResponseModel>
    fun onOperationNotFound(): PublishSubject<Boolean>
}

class FirstAuthOperationViewModel @Inject constructor(
    private val getFirstAuthOperationListUseCase: GetFirstAuthOperationListUseCase,
    private val getFirstSignInfoUseCase: GetFirstSignInfoUseCase,
    private val sharedPrefs: SharedPreferences
) :
    BaseViewModel(),
    FirstAuthOperationViewModelInputs,
    FirstAuthOperationViewModelOutputs {

    override fun getFirstSignInfo(batchId: Long) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        showProgress.onNext(true)
        getFirstSignInfoUseCase.execute(
            custId,
            compId,
            token,
            userId,
            phoneNumber,
            certCode,
            enumLangType,
            batchId
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    getSignInfoSuccess.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun getFirstSignInfoSuccess() = getSignInfoSuccess

    private val showProgress = PublishSubject.create<Boolean>()
    private var operationList = PublishSubject.create<ArrayList<BatchOperation>>()
    private val getSignInfoSuccess = PublishSubject.create<GetSignInfoResponseModel>()
    private val operationNotFound = PublishSubject.create<Boolean>()

    override fun authOperationListSuccess() = operationList
    override fun onOperationNotFound() = operationNotFound

    override val inputs: FirstAuthOperationViewModelInputs = this
    override val outputs: FirstAuthOperationViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val phoneNumber = sharedPrefs.getString("phoneNumber", "")
    val userId = sharedPrefs.getString("userId", "")
    val certCode = sharedPrefs.getString("certCode", "")
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    override fun getFirstAuthOperationList() {
        showProgress.onNext(true)
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        getFirstAuthOperationListUseCase.execute(custId, compId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    operationList.onNext(it.batchOperationList)
                } else {
                    error.onNext(it.status.statusCode)
                    operationNotFound.onNext(true)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun showProgress() = showProgress
}