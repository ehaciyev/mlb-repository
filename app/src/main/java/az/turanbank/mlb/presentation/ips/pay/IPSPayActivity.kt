package az.turanbank.mlb.presentation.ips.pay

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.nonregistered.pay.debitor.NonRegisteredIPSAccountChoiceActivity
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.IPSTransferActivity
import kotlinx.android.synthetic.main.activity_i_p_s_pay.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class IPSPayActivity : BaseActivity() {
    
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: IPSPayViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_i_p_s_pay)

        viewModel = ViewModelProvider(this,factory)[IPSPayViewModel::class.java]

        toolbar_title.text = getString(R.string.pay)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        registeredPayment.setOnClickListener {
            startActivityForResult(Intent(this, IPSTransferActivity::class.java),7)
        }

        unregisteredPayment.setOnClickListener {
            startActivityForResult(Intent(this, NonRegisteredIPSAccountChoiceActivity::class.java),7)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}