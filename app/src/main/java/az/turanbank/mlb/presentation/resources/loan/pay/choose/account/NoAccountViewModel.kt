package az.turanbank.mlb.presentation.resources.loan.pay.choose.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.domain.user.usecase.resources.account.GetNoCardAccountListForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetNoCardAccountListForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface NoAccountViewModelInputs : BaseViewModelInputs {
    fun getAccounts()
}

interface NoAccountViewModelOutputs : BaseViewModelOutputs {
    fun onAccountSuccess(): PublishSubject<ArrayList<AccountListModel>>
}

class NoAccountViewModel @Inject constructor(
    private val getIndividualAccountListUseCase: GetNoCardAccountListForIndividualCustomerUseCase,
    sharedPrefs: SharedPreferences,
    private val getJuridicalAccountListUseCase: GetNoCardAccountListForJuridicalCustomerUseCase
) : BaseViewModel(),
    NoAccountViewModelInputs,
    NoAccountViewModelOutputs {

    override val inputs: NoAccountViewModelInputs = this
    override val outputs: NoAccountViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val accountList = PublishSubject.create<ArrayList<AccountListModel>>()

    override fun getAccounts() {
        if (isCustomerJuridical) {
            getJuridicalAccountListUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                lang = EnumLangType.EN
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)

        } else {
            getIndividualAccountListUseCase.execute(custId, token,EnumLangType.EN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }


    }

    override fun onAccountSuccess() = accountList

}