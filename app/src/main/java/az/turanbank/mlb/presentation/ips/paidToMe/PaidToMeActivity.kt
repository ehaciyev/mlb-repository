package az.turanbank.mlb.presentation.ips.paidToMe

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.ReceivePaymentsModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.paidToMe.paidtomechoice.SinglePaidToMeActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_paid_to_me.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class PaidToMeActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: PaidToMeViewModel

    lateinit var adapter: IPSPaymentsAdapter

    private val paymentList = arrayListOf<ReceivePaymentsModel>()

    lateinit var dialog: MlbProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paid_to_me)

        dialog = MlbProgressDialog(this)

        viewModel = ViewModelProvider(this,factory)[PaidToMeViewModel::class.java]

        toolbar_title.text = getString(R.string.paid_to_me)
        toolbar_back_button.setOnClickListener{onBackPressed()}

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        adapter = IPSPaymentsAdapter(
            paymentList
        ) {
            val payment = adapter.getItemAt(it)
            val intent = Intent(this, SinglePaidToMeActivity::class.java)

            intent.putExtra("paymentItem", payment.operId)
            intent.putExtra("dtCustName", payment.dtCustName)
            intent.putExtra("dtIban", payment.dtIban)
            intent.putExtra("crIban", payment.crIban)
            intent.putExtra("amount", payment.amount)
            intent.putExtra("currency", payment.currency)
            intent.putExtra("createdDate", payment.createdDate)
            intent.putExtra("operStateName",payment.operStateName)
            intent.putExtra("operStateId", payment.operStateId)
            intent.putExtra("purpose",payment.purpose)
            startActivity(intent)
        }
        recyclerView.adapter = adapter

        setOutputListeners()
        setInputListeners()

    }

    private fun setOutputListeners() {

        viewModel.outputs.showProgress().subscribe{
            if(it){
                dialog.showDialog()
            } else{
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onPaymentsArrived().subscribe{
            paymentList.clear()
            paymentList.addAll(it)
            adapter.notifyDataSetChanged()
            noDataAvailable.visibility = View.GONE
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe{
            noDataAvailable.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getPayments()
    }
}
