package az.turanbank.mlb.presentation.register.mobile.individual

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.register.mobile.CheckIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.CheckJuridicalCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.SendOTPCodeUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface IndividualRegisterMobileViewModelInputs : BaseViewModelInputs {
    fun onCheckIndividualCustomer(pin: String, mobile: String)
    fun onCheckJuridicalCustomer(pin: String, mobile: String, taxNo: String)
}

interface IndividualRegisterMobileViewModelOutputs : BaseViewModelOutputs {
    fun verifyOtpCode(): Observable<Long>
    fun showProgress(): PublishSubject<Boolean>
}

class IndividualRegisterMobileViewModel @Inject constructor(
    private val checkJuridicalCustomerUseCase: CheckJuridicalCustomerUseCase,
    private val registerWithAsanCheckIndividualCustomerUseCase: CheckIndividualCustomerUseCase,
    private val sendOTPCodeUseCase: SendOTPCodeUseCase,
    val sharedPreferences: SharedPreferences
) : BaseViewModel(),
    IndividualRegisterMobileViewModelInputs,
    IndividualRegisterMobileViewModelOutputs {

    private val verifyOtpCode = PublishSubject.create<Long>()
    private val showProgress = PublishSubject.create<Boolean>()

    override val inputs: IndividualRegisterMobileViewModelInputs = this

    override val outputs: IndividualRegisterMobileViewModelOutputs = this

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun onCheckIndividualCustomer(pin: String, mobile: String) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        showProgress.onNext(true)
        registerWithAsanCheckIndividualCustomerUseCase.execute(pin, mobile, enumLangType)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.status.statusCode == 1 || it.status.statusCode == 113) {
                    sendOTPCode(it.custId, mobile)
                } else {
                    error.onNext(it.status.statusCode)
                    showProgress.onNext(false)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    private fun sendOTPCode(custId: Long, mobile: String) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        sendOTPCodeUseCase.execute(custId, null, mobile,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                when (it.status.statusCode) {
                    1 -> {
                        verifyOtpCode.onNext(custId)
                    }
                    else -> error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun onCheckJuridicalCustomer(pin: String, mobile: String, taxNo: String) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        showProgress.onNext(true)

        checkJuridicalCustomerUseCase.execute(pin, mobile, taxNo, enumLangType)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.status.statusCode == 1 || it.status.statusCode == 113) {
                    sendOTPCode(it.custId, mobile)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun verifyOtpCode() = verifyOtpCode

    override fun showProgress() = showProgress
}