package az.turanbank.mlb.presentation.login.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.lifecycle.ViewModelProvider
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.ContactsWithBankActivity
import az.turanbank.mlb.presentation.activity.HomePageSettingsActivity
import az.turanbank.mlb.presentation.adapter.LoginPageAdapter
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.branch.BranchNetworkTabbedActivity
import az.turanbank.mlb.presentation.exchange.ExchangeActivity
import az.turanbank.mlb.presentation.home.MlbHomeActivity
import az.turanbank.mlb.presentation.login.activities.pin.LoginPinActivity
import az.turanbank.mlb.presentation.news.NewsActivity
import az.turanbank.mlb.presentation.register.RegisterActivity
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login_drawer.*
import javax.inject.Inject

class LoginActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var viewmodel: LoginViewModel

    private val description: String by lazy { intent.getStringExtra("description") }

    @Inject
    lateinit var factory: ViewModelProviderFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_drawer)

        viewmodel = ViewModelProvider(this, factory)[LoginViewModel::class.java]

        val sectionsPagerAdapter =
            LoginPageAdapter(this, supportFragmentManager)

        handleListeners()

        view_pager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(view_pager)
        nav_view.setNavigationItemSelectedListener(this)

        if (viewmodel.pinIsSet && viewmodel.userLoggedIn)
            showPinScreen()
    }

    private fun showPinScreen() {
        val intent = Intent(this, LoginPinActivity::class.java)
        intent.putExtra("creatingNewPin", false)
        intent.putExtra("description", description)
        startActivityForResult(intent, 2)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            navigateToHome()
        }
    }

    private fun navigateToHome() {
        val intent = Intent(this, MlbHomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
    }

    private fun handleListeners() {
        register.setOnClickListener {
            val intent = Intent(
                this,
                RegisterActivity::class.java
            )
            intent.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP or
                        Intent.FLAG_ACTIVITY_SINGLE_TOP
            )
            startActivity(
                intent
            )
        }

        call_bank.setOnClickListener {call_bank() }
        //call_bank2.setOnClickListener {call_bank() }

        val actionBarDrawerToggle: ActionBarDrawerToggle =
            object : ActionBarDrawerToggle(this, home_drawer_layout, R.string.yes, R.string.no) {
                override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                    super.onDrawerSlide(drawerView, slideOffset)
                    val slideX = drawerView.width * slideOffset
                    mainContentNoRegistration.translationX = slideX
                }
            }
        home_drawer_layout.addDrawerListener(actionBarDrawerToggle)
        nav_drawer_button.setOnClickListener { home_drawer_layout.openDrawer(GravityCompat.START) }

        //nav_drawer_button.setOnClickListener { home_drawer_layout.openDrawer(GravityCompat.START) }
        nav_view.itemIconTintList = null // TODO
    }

    private fun call_bank(){
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CALL_PHONE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.CALL_PHONE),
                1
            )
        } else {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "012 935"))
            startActivity(intent)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        //to prevent current item select over and over
        if (item.isChecked) {
            home_drawer_layout.closeDrawer(GravityCompat.START)
            return false
        }

        when (id) {
            R.id.exchanges -> startActivity(
                Intent(
                    applicationContext,
                    ExchangeActivity::class.java
                )
            )
            R.id.branches -> startActivity(
                Intent(
                    applicationContext,
                    BranchNetworkTabbedActivity::class.java
                )
            )
            R.id.contact -> startActivity(
                Intent(
                    applicationContext,
                    ContactsWithBankActivity::class.java
                )
            )
            R.id.campaigns -> startActivity(
                Intent(
                    applicationContext,
                    NewsActivity::class.java
                )
            )
            R.id.settings -> startActivity(
                Intent(
                    applicationContext,
                    HomePageSettingsActivity::class.java
                )
            )
        }

        home_drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
