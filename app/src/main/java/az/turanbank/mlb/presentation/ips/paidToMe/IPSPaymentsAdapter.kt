package az.turanbank.mlb.presentation.ips.paidToMe

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.ReceivePaymentsModel
import kotlinx.android.synthetic.main.ips_payments_list_view2.view.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class IPSPaymentsAdapter(private val payments: ArrayList<ReceivePaymentsModel>,
                         private val onClickListener: (position: Int) -> Unit
): RecyclerView.Adapter<IPSPaymentsAdapter.IPSPaymentViewHolder>() {

    class IPSPaymentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(payment: ReceivePaymentsModel, onClickListener: (position: Int) -> Unit) {
            itemView.setOnClickListener { onClickListener.invoke(adapterPosition) }

            val decimalFormat = DecimalFormat("0.00")
            decimalFormat.roundingMode = RoundingMode.HALF_EVEN
            val symbols = DecimalFormatSymbols()
            symbols.decimalSeparator = '.'
            decimalFormat.decimalFormatSymbols = symbols

            itemView.bank.text = payment.operStateName

            itemView.amountInteger.text =
                "${decimalFormat.format(payment.amount).toString().split(".")[0]},"
            itemView.amountReminder.text =
                decimalFormat.format(payment.amount).toString().split(".")[1] + " " + payment.currency
            itemView.iban.text = payment.dtIban
            itemView.date.text = payment.createdDate
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        IPSPaymentViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.ips_payments_list_view2, parent, false))

    override fun getItemCount() = payments.size

    fun getItemAt(position: Int) = payments[position]

    override fun onBindViewHolder(holder: IPSPaymentViewHolder, position: Int) {
        holder.bind(payments[position], onClickListener)
    }
}