package az.turanbank.mlb.presentation.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_contacts_with_bank.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.lang.Exception
import javax.inject.Inject


class ContactsWithBankActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ContactsWithBankViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts_with_bank)

        toolbar_title.text = getString(R.string.contact_with_bank)
        toolbar_back_button.setOnClickListener{onBackPressed()}

        viewModel = ViewModelProvider(this, factory)[ContactsWithBankViewModel::class.java]

        val whatsappUri = Uri.parse("https://api.whatsapp.com/send?phone=+994502687222")
        val whatsappIntent = Intent(Intent.ACTION_VIEW, whatsappUri)

        val fbIntent = Intent()
        fbIntent.action = Intent.ACTION_VIEW
        fbIntent.setPackage("com.facebook.orca")
        fbIntent.data = Uri.parse("https://m.me/" + "TuranBankASC")

        call_bank.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.CALL_PHONE),
                    1
                )
            } else {
                val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "012 935"))
                startActivity(intent)
            }
        }

        whatsapp.setOnClickListener {
            try {
                startActivity(whatsappIntent)
            } catch (exc: Exception) {
                exc.printStackTrace()
            }
        }
        messenger.setOnClickListener {
            try {
                startActivity(fbIntent)
            } catch (exc: Exception) {
                exc.printStackTrace()
            }
        }
    }
}
