package az.turanbank.mlb.presentation.exchange.fragment.cash

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.exchange.CurrencyResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import dagger.android.support.AndroidSupportInjection
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class ExchangeCashFragment : BaseFragment() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ExchangeCashViewModel

    lateinit var currencyAdapter: CurrencyAdapter

    private lateinit var progress: ImageView

    private var currencyList: ArrayList<CurrencyResponseModel> = arrayListOf()

    lateinit var currencyRecyclerView: RecyclerView

    lateinit var successView: ConstraintLayout

    lateinit var failView: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_exchange_cash, container, false)

        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL
        currencyRecyclerView = view.findViewById(R.id.currencyRecyclerView)
        successView = view.findViewById(R.id.success)
        failView = view.findViewById(R.id.fail)
        progress = view.findViewById(R.id.progress)

        animateProgressImage()

        currencyRecyclerView.layoutManager = llm

        currencyAdapter = CurrencyAdapter(currencyList)
        currencyRecyclerView.adapter = currencyAdapter

        setInputListeners()
        setOutputListeners()
        return view
    }
    private fun setOutputListeners() {
        viewModel.outputs.onCashExchangeGot().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE

            successView.visibility = View.VISIBLE
            failView.visibility = View.GONE
            setupRecyclerView(it.exchangeCashList)
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE

            failView.text = getString(ErrorMessageMapper().getErrorMessage(it))
            failView.visibility = View.VISIBLE
            successView.visibility = View.GONE
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getCashExchange(arguments?.getString("date"))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
        viewModel = ViewModelProvider(this, factory)[ExchangeCashViewModel::class.java]
    }

    private fun setupRecyclerView(currencies: ArrayList<CurrencyResponseModel>) {
        currencyList.clear()
        for(i in 0 until currencies.size){
            if(currencies[i].currency == "AZN"){
                currencies.remove(currencies[i])
                break
            }
        }
        currencyList.addAll(currencies)
        currencyRecyclerView.adapter?.notifyDataSetChanged()
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progress.startAnimation(rotate)
    }
}
