package az.turanbank.mlb.presentation.ips

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.request.AliasRequestModel
import az.turanbank.mlb.domain.user.usecase.ips.aliases.CreateAliasUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.VerifyEmailOtpUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.VerifyOtpUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import javax.inject.Inject

interface SendOtpCodeForAliasViewModelInputs : BaseViewModelInputs {
    fun verifyOTP(confirmCode: String)
    fun verifyOtpCodeForEmail(confirmCode: String)
    fun createAlias(aliases: ArrayList<AliasRequestModel>)
}

interface SendOtpCodeForAliasViewModelOutputs : BaseViewModelOutputs {
    fun aliasCreated(): CompletableSubject
    fun onVerifyOTPSuccess(): CompletableSubject
    fun onVerifyOtpCodeForEmailSuccess(): CompletableSubject
}

class SendOtpCodeForAliasViewModel @Inject constructor(
    private val createAliasUseCase: CreateAliasUseCase,
    private val verifyOTPUseCase: VerifyOtpUseCase,
    private val verifyEmailOtpUseCase: VerifyEmailOtpUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    SendOtpCodeForAliasViewModelInputs,
    SendOtpCodeForAliasViewModelOutputs {
    override val inputs: SendOtpCodeForAliasViewModelInputs = this

    override val outputs: SendOtpCodeForAliasViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val aliasCreated = CompletableSubject.create()
    private val onVerifyOTPSuccess = CompletableSubject.create()
    private val onverifyOtpCodeForEmailSuccess = CompletableSubject.create()

    override fun verifyOTP(confirmCode: String) {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        verifyOTPUseCase.execute(custId, compId, token, confirmCode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode ==1){
                    onVerifyOTPSuccess.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            },{
                error.onNext(1878)
                it.printStackTrace()
            })
    }

    override fun verifyOtpCodeForEmail(confirmCode: String) {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        verifyEmailOtpUseCase.execute(custId, compId, token, confirmCode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode ==1){
                    onverifyOtpCodeForEmailSuccess.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            },{
                error.onNext(1878)
                it.printStackTrace()
            })
    }

    override fun createAlias(aliases: ArrayList<AliasRequestModel>) {

        createAliasUseCase
            .execute(custId, token, aliases)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    aliasCreated.onComplete()
                } else {
                    error.onNext(it.aliases[0].respStatus.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun aliasCreated() = aliasCreated
    override fun onVerifyOTPSuccess() = onVerifyOTPSuccess
    override fun onVerifyOtpCodeForEmailSuccess() = onverifyOtpCodeForEmailSuccess
}