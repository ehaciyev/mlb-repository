package az.turanbank.mlb.presentation.resources.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.resources.account.AccountTemplateResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.resources.account.template.AccountTemplateListAdapter
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class AccountTemplateFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: OperationTemplateViewModel
    private var templateList = arrayListOf<AccountTemplateResponseModel>()

    private lateinit var recyclerView: RecyclerView
    private lateinit var templateAdapter: AccountTemplateListAdapter

    private lateinit var progress: ImageView
    private lateinit var failMessage: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_transfer_template, container, false)

        viewModel =
            ViewModelProvider(this, factory)[OperationTemplateViewModel::class.java]

        recyclerView = view.findViewById(R.id.recyclerView)
        progress = view.findViewById(R.id.progress)
        failMessage = view.findViewById(R.id.failMessage)

        makeRecyclerView()

        setOutputListeners()

        setInputListeners()

        return view
    }

    companion object {
        fun newInstance() = AccountTemplateFragment()
    }


    private fun makeRecyclerView() {
        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm

        templateAdapter = AccountTemplateListAdapter(templateList){
            startActivity(viewModel.getIntentWithData(requireActivity(), templateList[it]))
        }

        recyclerView.adapter = templateAdapter

    }

    override fun onResume() {
        super.onResume()
        viewModel.inputs.getTemplateList()
        setHasOptionsMenu(isVisible)
        setMenuVisibility(false)
    }

    private fun setOutputListeners() {
        viewModel.outputs.onTemplateListSuccess().subscribe{
            failMessage.visibility = View.GONE
            progress.clearAnimation()
            progress.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            updateRecyclerView(it)
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            recyclerView.visibility = View.GONE
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
            failMessage.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    private fun updateRecyclerView(accountList: ArrayList<AccountTemplateResponseModel>) {
        this.templateList.clear()
        this.templateList.addAll(accountList)
        recyclerView.adapter?.notifyDataSetChanged()
    }

    private fun setInputListeners() {
        animateProgressImage()
        viewModel.inputs.getTemplateList()
    }
    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }

}
