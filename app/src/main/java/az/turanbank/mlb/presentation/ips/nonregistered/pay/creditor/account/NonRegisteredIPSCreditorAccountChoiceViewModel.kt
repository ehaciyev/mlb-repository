package az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.GetAccountAndCustomerInfoResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.GetAllBanksResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.banks.GetAllBanksUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetAccountAndCustomerInfoUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

interface IPSCreditorAccountChoiceViewModelInputs : BaseViewModelInputs {
    fun getBankList()
    fun getReceiverCustomerInfo(iban: String)
}

interface IPSCreditorAccountChoiceViewModelOutputs : BaseViewModelOutputs {
    fun onBankListSuccess(): BehaviorSubject<GetAllBanksResponseModel>
    fun onReceiverCustomerSuccess() : PublishSubject<GetAccountAndCustomerInfoResponseModel>
}

class NonRegisteredIPSCreditorAccountChoiceViewModel @Inject constructor(
    private val getAllBanksUseCase: GetAllBanksUseCase,
    private val getAccountAndCustomerInfoUseCase: GetAccountAndCustomerInfoUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    IPSCreditorAccountChoiceViewModelInputs,
    IPSCreditorAccountChoiceViewModelOutputs {
    override val inputs: IPSCreditorAccountChoiceViewModelInputs = this

    override val outputs: IPSCreditorAccountChoiceViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val bankList = BehaviorSubject.create<GetAllBanksResponseModel>()
    private var selectedBankId: Long? = null

    private var isGovernmentPayment: Boolean = false

    private val getReceiverCustomerInfo = PublishSubject.create<GetAccountAndCustomerInfoResponseModel>()

    override fun getBankList() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getAllBanksUseCase
            .execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    bankList.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getReceiverCustomerInfo(iban: String) {
        getAccountAndCustomerInfoUseCase
            .execute(iban,null,null,null,null,custId,token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it.status.statusCode) {
                    1 -> {
                        getReceiverCustomerInfo.onNext(it)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            },{
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onBankListSuccess() = bankList
    override fun onReceiverCustomerSuccess() = getReceiverCustomerInfo

    fun setSelectedBankId(id: Long) {
        if(id == 210005L || id == 210027L) {
            isGovernmentPayment = true
        }
        this.selectedBankId = id
    }

    fun setSelectedBankCode(bankCode: String?) {
        if(bankCode == "210005") {
            isGovernmentPayment = true
        }
    }

    fun getSelectedBankId() = selectedBankId

    fun isGovernmentPayment() = isGovernmentPayment
}