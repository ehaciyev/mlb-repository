package az.turanbank.mlb.presentation.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.fragment.IndividualRegisterFragment
import az.turanbank.mlb.presentation.fragment.JuridicalRegisterFragment

class RegisterPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> {
                IndividualRegisterFragment()
            }
            else -> JuridicalRegisterFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> {
                context.resources.getString(R.string.individual)
            }
            1 -> {
                context.resources.getString(R.string.juridical)
            }
            else -> null
        }
    }

    override fun getCount(): Int {
        return 2
    }
}