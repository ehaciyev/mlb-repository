package az.turanbank.mlb.presentation.exchange.converter.fragment.cashConverter

import az.turanbank.mlb.R


open class ExchangeFlagMapperConverter {
    open fun resolveFlag(flagCode: String): Int{
        when (flagCode){
            "AZN" -> return R.drawable.aze
            "RUB" -> return R.drawable.rubl
            "EUR" -> return R.drawable.ic_eu_convert
            "USD" -> return R.drawable.ic_usa_currency
            "TRY" -> return R.drawable.turkey
            "AED" -> return R.drawable.aed
            "GBP" -> return R.drawable.ic_gbp_currency
            "FRF" -> return R.drawable.french
            "DEM" -> return R.drawable.currency_german
            "CHF" -> return R.drawable.currency_sweden
            "GEL" -> return R.drawable.currency_georgia
            "YEN" -> return R.drawable.currency_japan
            "CNY" -> return R.drawable.currency_china
            "KWD" -> return R.drawable.currency_kuwait
            "SAR" -> return R.drawable.currency_saud_arabia
            "UAH" -> return R.drawable.currency_ukraina
        }
        return 0
    }
}