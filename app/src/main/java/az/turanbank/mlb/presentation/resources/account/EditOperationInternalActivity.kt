package az.turanbank.mlb.presentation.resources.account

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_operation_internal.*
import kotlinx.android.synthetic.main.activity_edit_operation_internal.crAccTaxContainer
import kotlinx.android.synthetic.main.activity_edit_operation_internal.crIban
import kotlinx.android.synthetic.main.activity_edit_operation_internal.dtAccountId
import kotlinx.android.synthetic.main.activity_edit_operation_internal.dtAccountIdContainer
import kotlinx.android.synthetic.main.activity_edit_operation_internal.opAmount
import kotlinx.android.synthetic.main.activity_edit_operation_internal.opCrAccName
import kotlinx.android.synthetic.main.activity_edit_operation_internal.opCrAccTaxNo
import kotlinx.android.synthetic.main.activity_edit_operation_internal.opCrAccountNameProgres
import kotlinx.android.synthetic.main.activity_edit_operation_internal.opPurpose
import kotlinx.android.synthetic.main.activity_edit_operation_internal.opTaxProgress
import kotlinx.android.synthetic.main.activity_edit_operation_internal.submitButton
import kotlinx.android.synthetic.main.activity_edit_operation_internal.submitText
import javax.inject.Inject

class EditOperationInternalActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: EditOperationInternalViewModel

    lateinit var dialog: MlbProgressDialog
    private lateinit var textWatcher: TextWatcher

    private val operId: Long by lazy { intent.getLongExtra("operId", 0L) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_operation_internal)
        crIban.filters = arrayOf(InputFilter.AllCaps())

        toolbar_title.text = getString(R.string.internal_transfer)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        dialog = MlbProgressDialog(this)

        buttonEnabled(false)

        viewmodel = ViewModelProvider(this, factory) [EditOperationInternalViewModel::class.java]
        initTextWatcher()
        crIban.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 28) {
                    showProgressImages(true)
                    viewmodel.inputs.getCustInfoByIban(s.toString())
                    observeCustomerInfo()
                } else {
                    crAccTaxContainer.visibility = View.GONE
                    opCrAccName.setText("")
                    opCrAccTaxNo.setText("")
                    hideProgressImages(true)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
/*
        submitButton.setOnClickListener {
            println(viewmodel.getDtAccountId(dtAccountId.text.toString()))

            viewmodel.inputs.updateInternalOperation(
                operId = operId,
                operNameId = 1,
                dtAccountId = viewmodel.getDtAccountId(dtAccountId.text.toString()),
                crIban = crIban.text.toString(),
                amount = opAmount.text.toString().toDouble(),
                purpose = opPurpose.text.toString(),
                taxNo = taxNo()
            )
        }*/

        setOutputListeners()
        setInputListeners()
    }

    private fun taxNo(): String? {
        return if (opCrAccTaxNo.text.toString().isNullOrEmpty())
            null
        else opCrAccTaxNo.text.toString()
    }

    private fun initTextWatcher() {
        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!crIban.text.isNullOrEmpty() &&
                    !opAmount.text.isNullOrEmpty() &&
                    !dtAccountId.text.isNullOrEmpty() &&
                    !opPurpose.text.isNullOrEmpty() &&
                    !opCrAccName.text.isNullOrEmpty()
                ) {
                    buttonEnabled(true)

                    submitButton.setOnClickListener {
                        viewmodel.inputs.updateInternalOperation(
                            operId = operId,
                            operNameId = 1,
                            dtAccountId = viewmodel.getDtAccountId(dtAccountId.text.toString()),
                            crIban = crIban.text.toString(),
                            amount = opAmount.text.toString().toDouble(),
                            purpose = opPurpose.text.toString(),
                            taxNo = taxNo()
                        )
                    }
                } else {
                    buttonEnabled(false)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        crIban.addTextChangedListener(textWatcher)
        opAmount.addTextChangedListener(textWatcher)
        dtAccountId.addTextChangedListener(textWatcher)
        opPurpose.addTextChangedListener(textWatcher)
        opCrAccName.addTextChangedListener(textWatcher)
    }

    private fun buttonEnabled(show: Boolean) {
        if (!show) {
            submitButton.isClickable = false
            submitButton.isEnabled = false
            submitButton.isFocusable = false
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
        } else {
            submitButton.isClickable = true
            submitButton.isEnabled = true
            submitButton.isFocusable = true
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
    }

    private fun observeCustomerInfo() {
        viewmodel.outputs.custInfo().subscribe {
            if (it.status.statusCode == 1) {
                it.taxNumber?.let { taxNumber ->
                    crAccTaxContainer.visibility = View.VISIBLE
                    opCrAccTaxNo.setText(taxNumber)
                }
                showProgressImages(false)
                buttonEnabled(true)
                opCrAccName.setText(it.custName)
            } else {
                showProgressImages(false)
                buttonEnabled(false)
            }
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewmodel.inputs.getAccountList()
        viewmodel.inputs.getOperationById(operId)
    }

    private fun setOutputListeners() {
        viewmodel.outputs.operationUpdated().subscribe {
            Toast.makeText(this, getString(R.string.transfer_successfully_updated), Toast.LENGTH_LONG).show()
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(subscriptions)

        viewmodel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.onAccountSuccess().subscribe { accountListModel ->
            dtAccountIdContainer.setOnClickListener {

                val accountNameArray = arrayOfNulls<String>(accountListModel.size)
                val b: AlertDialog.Builder =
                    AlertDialog.Builder(this@EditOperationInternalActivity)
                b.setTitle(getString(R.string.select_account))

                for (i in 0 until accountListModel.size) {
                    accountNameArray[i] = (accountListModel[i].iban)
                }

                b.setItems(accountNameArray) { _, which ->
                    dtAccountId.setText(accountNameArray[which])
                }
                val dialog = b.create()
                dialog.show()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.operationDetails().subscribe {
            dtAccountId.setText(it.dtIban) //TODO set selected dtaccountid
            crIban.setText(it.crIban)
            opCrAccName.setText(it.crCustName)
            opAmount.setText(it.amt.toString())
            opPurpose.setText(it.operPurpose)

           // intent.putExtra("dtAccountId", viewmodel.getDtAccountId(dtAccountId.text.toString()))
           /* intent.putExtra("crIban", crIban.text.toString())
            intent.putExtra("amount", opAmount.text.toString())
            intent.putExtra("purpose", opPurpose.text.toString())
            intent.putExtra("accountName", opCrAccName.text.toString())*/

        }.addTo(subscriptions)

        viewmodel.outputs.updateInternalOperationSuccess().subscribe {
            if (it.status.statusCode == 1) {
                val intent = Intent()
                intent.putExtra("accountResponse", it)
                intent.putExtra("operationNo", it.operationNo)
                intent.putExtra("operationStatus", it.operationStatus)
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
               /* showProgressBar(false)
                showAlertDialog(R.string.default_error_message)
            */}
        }.addTo(subscriptions)
    }

    private fun hideProgressImages(hide: Boolean) {
        if (hide) {
            opCrAccountNameProgres.visibility = View.GONE
            opTaxProgress.visibility = View.GONE
        } else {
            opCrAccountNameProgres.visibility = View.VISIBLE
            opTaxProgress.visibility = View.VISIBLE
        }
    }

    private fun showProgressImages(show: Boolean) {
        hideProgressImages(show)

        if (show) {
            opCrAccountNameProgres.visibility = View.VISIBLE
            opTaxProgress.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            opCrAccountNameProgres.visibility = View.GONE
            opTaxProgress.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        opCrAccountNameProgres.clearAnimation()
        opTaxProgress.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        opCrAccountNameProgres.startAnimation(rotate)
        opTaxProgress.startAnimation(rotate)
    }
}
