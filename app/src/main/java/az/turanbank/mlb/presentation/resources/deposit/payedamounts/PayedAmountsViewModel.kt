package az.turanbank.mlb.presentation.resources.deposit.payedamounts

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.deposit.PayedAmountResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetPayedAmountsByDepositIdIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetPayedAmountsByDepositIdJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface PayedAmountsViewModelInputs: BaseViewModelInputs{
    fun getPayedAmounts(depositId: Long)
}
interface PayedAmountsViewModelOutputs: BaseViewModelOutputs{
    fun onPayedAmountsSuccess(): PublishSubject<ArrayList<PayedAmountResponseModel>>
}
class PayedAmountsViewModel @Inject constructor(
    private val payedAmountsByDepositIdIndividualUseCase: GetPayedAmountsByDepositIdIndividualUseCase,
    private val payedAmountsByDepositIdJuridicalUseCase: GetPayedAmountsByDepositIdJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    PayedAmountsViewModelInputs,
    PayedAmountsViewModelOutputs {
    override val inputs: PayedAmountsViewModelInputs = this

    override val outputs: PayedAmountsViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val payedList = PublishSubject.create<ArrayList<PayedAmountResponseModel>>()

    override fun getPayedAmounts(depositId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if (isCustomerJuridical){
            payedAmountsByDepositIdJuridicalUseCase
                .execute(custId, compId, depositId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        payedList.onNext(it.respDepositPayedAmounts)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {

                }).addTo(subscriptions)
        } else {
            payedAmountsByDepositIdIndividualUseCase
                .execute(custId, depositId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        payedList.onNext(it.respDepositPayedAmounts)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {

                }).addTo(subscriptions)
        }
    }

    override fun onPayedAmountsSuccess() = payedList
}