package az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.alias


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.ips.request.EnumAliasType
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.BankListAdapter
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.moreinfo.AccountAndCustomerInfoActivity
import az.turanbank.mlb.presentation.ips.pay.preconfirm.TransferWithIPSActivity
import az.turanbank.mlb.util.animateProgressImage
import az.turanbank.mlb.util.revertProgressImageAnimation
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class NonRegisteredIPSAliasChoiceFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: NonRegisteredIPSCreditorAliasChoiceViewModel


    private lateinit var bankContainer: LinearLayout
    private lateinit var progressImage: ImageView
    private lateinit var submitText: TextView

    private lateinit var aliasTypeContainer: LinearLayout
    private lateinit var aliasContainer: LinearLayout

    lateinit var submitButton: LinearLayout

    private var receiverName: String? = null
    private var receiverSurname: String? = null
    private var taxNumber: String? = null

    lateinit var bank: AutoCompleteTextView
    lateinit var aliasType: AppCompatTextView

    lateinit var dialog: MlbProgressDialog

    lateinit var alias: AppCompatEditText

    private val senderFullName: String by lazy { requireActivity().intent.getStringExtra("senderFullName") }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_ipscreditor_alias_choice, container, false)

        viewModel =
            ViewModelProvider(this, factory)[NonRegisteredIPSCreditorAliasChoiceViewModel::class.java]
        initViews(view)

        setOutputListeners()
        setInputListeners()

        return view
    }

    private fun setOutputListeners() {

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        val typeList = arrayOf("PIN",
            "TIN",
            "EMAIL",
            "MOBILE")

        aliasTypeContainer.setOnClickListener{
            val builder: android.app.AlertDialog.Builder =
                android.app.AlertDialog.Builder(requireContext())
            builder.setTitle(getString(R.string.select_alias_type))

            builder.setItems(typeList){_, which ->
                viewModel.setSelectedAliasType(which)
                aliasType.text = typeList[which]
            }

            val dialog = builder.create()
            dialog.show()
        }

        viewModel.outputs.onBankListSuccess().subscribe { banks ->
            val list = arrayListOf<String>()
            val bankHash = HashMap<String, String>()
            val bankCodeHash = HashMap<String, String>()

            for (i in 0 until banks.bankInfos.size) {
                list.add(banks.bankInfos[i].name)
                bankHash[banks.bankInfos[i].name] = banks.bankInfos[i].id.toString()
                bankCodeHash[banks.bankInfos[i].name] = banks.bankInfos[i].code
            }
            val adapter =
                BankListAdapter(requireContext(), list)
            bank.threshold = 0
            adapter.notifyDataSetChanged()

            bank.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                viewModel.setSelectedBankId(bankHash[adapter.getItem(position)]!!.toLong())
                viewModel.setSelectedBankCode(bankCodeHash[adapter.getItem(position)])
            }
            bank.setAdapter(adapter)

        }.addTo(subscriptions)

        viewModel.outputs.onAccountAndCustomerInfoByAlias().subscribe{
            receiverName = it.name
            receiverSurname = it.surname
            taxNumber = it.taxNumber
            sendIntent(TransferWithIPSActivity())

        }.addTo(subscriptions)

        viewModel.outputs.onAccountInfoByAliasNotFound().subscribe{
            if(it ==33311){
                sendIntent(AccountAndCustomerInfoActivity())
            }
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe{
            AlertDialogMapper(requireActivity(), it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getBankList()
        submitButton.setOnClickListener {
            showProgressBar(true)
            when {
                bank.text.toString().trim().isEmpty() -> {
                    AlertDialogMapper(requireActivity(), 124020).showAlertDialog()
                    showProgressBar(false)
                }
                aliasType.text.toString().trim().isEmpty() -> {
                    AlertDialogMapper(requireActivity(), 124023).showAlertDialog()
                    showProgressBar(false)
                }
                alias.text.toString().trim().isEmpty() -> {
                    AlertDialogMapper(requireActivity(), 124022).showAlertDialog()
                    showProgressBar(false)
                }
                else -> {
                    viewModel.inputs.getAccountAndCustomerInfoByAlias(viewModel.getSelectedAliasType() as EnumAliasType,alias.text.toString())
                    showProgressBar(false)
                }
            }
        }
    }

    private fun sendIntent(activity: Activity){
        val intent = Intent(requireActivity(), activity::class.java)
        intent.putExtra("receiverAccountOrAlias", alias.text.toString())
        intent.putExtra("receiverId", viewModel.getSelectedBankId())
        intent.putExtra("isGovernmentPayment", viewModel.isGovernmentPayment())
        intent.putExtra("senderAccountOrAlias", requireActivity().intent.getStringExtra("senderAccountOrAlias"))
        intent.putExtra("senderId" , requireActivity().intent.getLongExtra("senderId",0L))
        intent.putExtra("fromAccount",requireActivity().intent.getBooleanExtra("fromAccount",false))
        intent.putExtra("toAccount",false)
        intent.putExtra("senderAliasTypeId", viewModel.getSelectedAliasType() )
        intent.putExtra("receiverCustomerName", receiverName)
        intent.putExtra("receiverCustomerSurname", receiverSurname)
        intent.putExtra("senderFullName",senderFullName)
        intent.putExtra("voenCode",taxNumber)
        startActivity(intent)
    }

    private fun initViews(view: View) {
        dialog = MlbProgressDialog(requireActivity())

        aliasTypeContainer = view.findViewById(R.id.aliasTypeContainer)

        aliasType = view.findViewById(R.id.aliasType)

        bankContainer = view.findViewById(R.id.bankContainer)

        aliasContainer = view.findViewById(R.id.aliasContainer)

        progressImage = view.findViewById(R.id.progressImage)

        submitText = view.findViewById(R.id.submitText)

        submitButton = view.findViewById(R.id.submitButton)

        bank = view.findViewById(R.id.bank)

        alias = view.findViewById(R.id.alias)
    }

    private fun showProgressBar(show: Boolean) {
        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        bankContainer.isClickable = !show
        aliasTypeContainer.isClickable = !show
        aliasContainer.isClickable = !show
        bank.isClickable = !show
        aliasType.isClickable = !show
        alias.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }
}
