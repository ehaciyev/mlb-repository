package az.turanbank.mlb.presentation.register.asan.individual

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetAuthInfoResponseModel
import az.turanbank.mlb.domain.user.usecase.register.asan.CheckPinForAsanRegisterUseCase
import az.turanbank.mlb.domain.user.register.usecase.GetAuthInfoUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.GetPinFromAsanUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface IndividualRegisterAsanViewModelInputs : BaseViewModelInputs {
    fun onSubmitClicked(phoneNumber: String, userId: String)
}

interface IndividualRegisterAsanViewModelOutputs : BaseViewModelOutputs {
    fun registrationNotFinished(): Observable<Long>
    fun verificationCode(): Observable<GetAuthInfoResponseModel>
}

class IndividualRegisterAsanViewModel @Inject constructor(
    private val getPinFromAsanUseCase: GetPinFromAsanUseCase,
    private val checkPinForAsanUseCase: CheckPinForAsanRegisterUseCase,
    private val getAuthInfoUseCase: GetAuthInfoUseCase,
    sharedPreferences: SharedPreferences
) : BaseViewModel(),
    IndividualRegisterAsanViewModelInputs,
    IndividualRegisterAsanViewModelOutputs {

    override val inputs: IndividualRegisterAsanViewModelInputs = this
    override val outputs: IndividualRegisterAsanViewModelOutputs = this

    private val registrationNotFinished = PublishSubject.create<Long>()

    private val verificationCode = PublishSubject.create<GetAuthInfoResponseModel>()

    private var custCode: Long = 0L
    private lateinit var custPin: String

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    fun custCode() = custCode

    fun custPin() = custPin

    override fun onSubmitClicked(phoneNumber: String, userId: String) {
        getPinFromAsanUseCase.execute(phoneNumber, userId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if(it.status.statusCode == 1) {
                    checkPinForAsanRegister(it.pin, phoneNumber, userId)
                }
                else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    private fun checkPinForAsanRegister(pin: String, phoneNumber: String, userId: String) {
        checkPinForAsanUseCase.execute(pin)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                custCode = it.custCode
                custPin = it.pin
                when (it.status.statusCode) {
                    113 -> {
                        registrationNotFinished.onNext(it.custId)
                    }

                    1 -> {
                        getAuthInfo(phoneNumber, userId)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun registrationNotFinished(): Observable<Long> = registrationNotFinished

    private fun getAuthInfo(phoneNumber: String, userId: String) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        getAuthInfoUseCase.execute(phoneNumber, userId, enumLangType)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.status.statusCode == 1) {
                    verificationCode.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun verificationCode(): Observable<GetAuthInfoResponseModel> = verificationCode
}