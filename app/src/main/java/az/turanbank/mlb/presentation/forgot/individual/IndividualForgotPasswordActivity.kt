package az.turanbank.mlb.presentation.forgot.individual

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.forgot.verify.ForgotPasswordVerifyOtpCodeActivity
import az.turanbank.mlb.util.addPrefix
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_individual_register_mobile.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class IndividualForgotPasswordActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: IndividualForgotPasswordViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual_register_mobile)

        viewmodel =
            ViewModelProvider(this, factory)[IndividualForgotPasswordViewModel::class.java]

        toolbar_title.text = getString(R.string.forgot_password)
        setInputListeners()
        setOutputListeners()
    }

    private fun setOutputListeners() {
        toolbar_title.text = getString(R.string.forgot_password)
        auth_message.visibility = View.GONE
        viewmodel.outputs.verifyOtpCode().subscribe {
            val intent = Intent(this, ForgotPasswordVerifyOtpCodeActivity::class.java)
            intent.putExtra("custId", it)
            intent.putExtra("mobile", mobileNumber.text.toString().addPrefix())
            startActivity(intent)
            finish()
        }.addTo(subscriptions)
        viewmodel.outputs.onError().subscribe {
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        toolbar_back_button.setOnClickListener { onBackPressed() }
        pin.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(7))
        submitButton.setOnClickListener {
           if(checkvalidation()){
               showProgressBar(true)
               viewmodel.inputs.checkForgotPassword(
                   pin = pin.text.toString(),
                   mobile = mobileNumber.text.toString().addPrefix()
               )
           }
        }
    }

    private fun checkvalidation(): Boolean{
        var isValid = true
        if(pin.text.toString().isNullOrEmpty()){
            pin.error = "enter pin"
            isValid = false
        }
        if(mobileNumber.text.toString().isNullOrEmpty()){
            mobileNumber.error = "enter number"
            isValid = false
        }
        return isValid
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        pin.isClickable = !show
        mobileNumber.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
