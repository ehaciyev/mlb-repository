package az.turanbank.mlb.presentation.resources.loan.pay.success

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.loan.LoanPaymentResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_success_loan_payment.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import javax.inject.Inject

class SuccessLoanPaymentActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: SuccessLoanPaymentViewModel

    private val decimalFormat = DecimalFormat("0.00")
    private val symbols = DecimalFormatSymbols()
    private lateinit var data: LoanPaymentResponseModel
    lateinit var paidStamp: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_loan_payment)

        viewModel = ViewModelProvider(this, factory)[SuccessLoanPaymentViewModel::class.java]

        initView()

        when {
            viewModel.getLang() == "az" -> {
                paidStamp.setImageResource(R.drawable.pechat_az)
            }
            viewModel.getLang() == "en" -> {
                paidStamp.setImageResource(R.drawable.pechat_eng)
            }
            viewModel.getLang() == "ru" -> {
                paidStamp.setImageResource(R.drawable.pechat_rus)
            }
        }

        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = ','
        decimalFormat.decimalFormatSymbols = symbols
        data = intent.getSerializableExtra("loanResponse") as LoanPaymentResponseModel

        setViewWithIntent()

        setListeners()
    }

    private fun initView() {
        paidStamp = findViewById(R.id.paid_stamp)
    }


    private fun setListeners() {
        detailOpClose.setOnClickListener { finish() }
        detailOpShare.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, generateShareString())

            startActivity(Intent.createChooser(shareIntent, getString(R.string.send)))
        }
    }
    private fun generateShareString(): String {

        return getString(R.string.card_to_card_share_oper_type)+ getString(R.string.credit_payment) + "\n" +
                getString(R.string.share_contract_number) + data.contractNumber + "\n" +
                getString(R.string.share_oper_date) + data.trDate + "\n" +
                getString(R.string.share_customer_name) + data.fullName + "\n" +
                getString(R.string.card_to_card_share_amount) + data.amount+" "+data.currency + "\n" +
                getString(R.string.share_purpose) + data.description + "\n"
    }
    @SuppressLint("SetTextI18n")
    private fun setViewWithIntent() {
        receiptNo.text = data.contractNumber
        operationDate.text = data.trDate
        customerName.text = data.fullName
//        note.text = data.description
        amountInteger.text = decimalFormat.format(data.amount).toString().split(",")[0] + ","
        amountReminder.text = decimalFormat.format(data.amount).toString().split(",")[1] +" "+data.currency


    }
}
