package az.turanbank.mlb.presentation.payments.pay.subcategories


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.pg.response.payments.merchant.MerchantResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.picasso.PicassoTrustAll
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.payments.pay.params.PaymentParametersActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_payment_subcategories.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class PaymentMerchantsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    @Inject
    lateinit var picassoTrustAll: PicassoTrustAll

    lateinit var viewModel: PaymentMerchantsViewModel

    lateinit var adapter : PaymentSubcategoriesAdapter

    private val subcategories = arrayListOf<MerchantResponseModel>()

    private val categoryId: Long by lazy { intent.getLongExtra("categoryId", 0L) }
    private val displayName: String by lazy { intent.getStringExtra("displayName") }
    private val categoryName: String by lazy { intent.getStringExtra("categoryName") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_payment_subcategories)

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        container.layoutManager = llm

        toolbar_title.text = displayName
        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewModel =
            ViewModelProvider(this, factory)[PaymentMerchantsViewModel::class.java]
        //animateProgressImage()
        adapter = PaymentSubcategoriesAdapter(picassoTrustAll,this, subcategories){
            val intent = Intent(this, PaymentParametersActivity::class.java)

            intent.putExtra("merchantId", subcategories[it].merchantId)
            intent.putExtra("categoryId", categoryId)
            intent.putExtra("providerMerchantId", subcategories[it].providerMerchantId)
            intent.putExtra("displayName", subcategories[it].displayName)
            intent.putExtra("merchantName", subcategories[it].merchantName)
            intent.putExtra("merchantDisplayName",subcategories[it].displayName)
            intent.putExtra("categoryName", categoryName)
            startActivityForResult(intent,7)
        }

        container.adapter = adapter

        setOutputListeners()
        setInputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onMerchants().subscribe {
            subcategories.clear()
            subcategories.addAll(it)
            adapter.notifyDataSetChanged()

            categoryContainerShimmer.stopShimmerAnimation()
            categoryContainerShimmer.visibility = View.GONE

           /* progress.clearAnimation()
            progress.visibility = View.GONE*/
            container.visibility = View.VISIBLE
        }.addTo(subscriptions)
        viewModel.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }

    private fun animateProgressImage() {
        container.visibility = View.GONE
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
    private fun setInputListeners() {
        viewModel.inputs.getMerchants(categoryId)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        categoryContainerShimmer.startShimmerAnimation()
    }

    override fun onDestroy() {
        super.onDestroy()
        categoryContainerShimmer.stopShimmerAnimation()
    }

}
