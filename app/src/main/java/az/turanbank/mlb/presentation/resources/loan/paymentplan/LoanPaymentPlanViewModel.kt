package az.turanbank.mlb.presentation.resources.loan.paymentplan

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.loan.LoanPaymentPlanModel
import az.turanbank.mlb.domain.user.usecase.resources.loan.plan.LoanPaymentPlanIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.loan.plan.LoanPaymentPlanJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface LoanPaymentPlanViewModelInputs: BaseViewModelInputs{
    fun getPaymentPlan(loanId: Long)
}
interface LoanPaymentPlanViewModelOutputs: BaseViewModelOutputs{
    fun onPaymentPlanSuccess(): PublishSubject<ArrayList<LoanPaymentPlanModel>>
}
class LoanPaymentPlanViewModel @Inject constructor(
    private val loanPaymentPlanIndividualUseCase: LoanPaymentPlanIndividualUseCase,
    private val loanPaymentPlanJuridicalUseCase: LoanPaymentPlanJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    LoanPaymentPlanViewModelInputs,
    LoanPaymentPlanViewModelOutputs {

    override val inputs: LoanPaymentPlanViewModelInputs = this

    override val outputs: LoanPaymentPlanViewModelOutputs = this


    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val paymentList = PublishSubject.create<ArrayList<LoanPaymentPlanModel>>()
    override fun getPaymentPlan(loanId: Long) {
        if (isCustomerJuridical){
            loanPaymentPlanJuridicalUseCase
                .execute(custId, compId, loanId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        paymentList.onNext(it.loanPaymentPlan)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },{
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            loanPaymentPlanIndividualUseCase
                .execute(custId, loanId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        paymentList.onNext(it.loanPaymentPlan)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                },{
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onPaymentPlanSuccess() = paymentList
}