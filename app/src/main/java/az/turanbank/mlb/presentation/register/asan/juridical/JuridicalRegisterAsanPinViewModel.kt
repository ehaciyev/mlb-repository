package az.turanbank.mlb.presentation.register.asan.juridical

import az.turanbank.mlb.domain.user.usecase.register.asan.LoginWithAsanUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface JuridicalRegisterAsanPinViewModelInputs : BaseViewModelInputs {
    fun loginWithAsan(transactionId: Long, certificate: String, challenge: String)
}

interface JuridicalRegisterAsanPinViewModelOutputs : BaseViewModelOutputs {
    fun loginWithAsanSuccess(): Observable<Boolean>
}

class JuridicalRegisterAsanPinViewModel @Inject constructor(
    private val loginWithAsanUseCase: LoginWithAsanUseCase
) :
    BaseViewModel(),
    JuridicalRegisterAsanPinViewModelInputs,
    JuridicalRegisterAsanPinViewModelOutputs {

    override val inputs: JuridicalRegisterAsanPinViewModelInputs = this
    override val outputs: JuridicalRegisterAsanPinViewModelOutputs = this

    private val loginWithAsanSuccess = PublishSubject.create<Boolean>()

    override fun loginWithAsan(transactionId: Long, certificate: String, challenge: String) {
        loginWithAsanUseCase.execute(
            transactionId = transactionId,
            certificate = certificate,
            challenge = challenge
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    loginWithAsanSuccess.onNext(true)
                } else {
                    loginWithAsanSuccess.onNext(false)
                    error.onNext(it.status.statusCode)                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun loginWithAsanSuccess() = loginWithAsanSuccess
}