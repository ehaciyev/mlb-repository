package az.turanbank.mlb.presentation.register.mobile.juridical

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.register.mobile.CheckIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.CheckJuridicalCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.SendOTPCodeUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface JuridicalRegisterMobileViewModelInputs : BaseViewModelInputs {
    fun onCheckJuridicalCustomer(pin: String, mobile: String, taxNo: String)
}

interface JuridicalRegisterMobileViewModelOutputs : BaseViewModelOutputs {
    fun verifyOtpCode(): Observable<Long>
    fun showProgress(): PublishSubject<Boolean>
    fun getCompId(): Observable<Long>
}

class JuridicalRegisterMobileViewModel @Inject constructor(
    private val checkJuridicalCustomerUseCase: CheckJuridicalCustomerUseCase,
    private val registerWithAsanCheckIndividualCustomerUseCase: CheckIndividualCustomerUseCase,
    private val sendOTPCodeUseCase: SendOTPCodeUseCase,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel(),
    JuridicalRegisterMobileViewModelInputs,
    JuridicalRegisterMobileViewModelOutputs {


    private val verifyOtpCode = PublishSubject.create <Long>()
    private val getCompId = PublishSubject.create <Long>()

    override val inputs: JuridicalRegisterMobileViewModelInputs = this

    override val outputs: JuridicalRegisterMobileViewModelOutputs = this

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val showProgress = PublishSubject.create<Boolean>()

    private fun sendOTPCode(custId: Long, mobile: String, compId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        showProgress.onNext(true)
        sendOTPCodeUseCase.execute(custId, compId, mobile, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                when(it.status.statusCode) {
                    1 -> {
                        verifyOtpCode.onNext(custId)
                    }
                    else -> error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }
    override fun onCheckJuridicalCustomer(pin: String, mobile: String, taxNo: String) {
        showProgress.onNext(true)
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        checkJuridicalCustomerUseCase.execute(pin, mobile, taxNo, enumLangType)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1 || it.status.statusCode == 113) {
                    sendOTPCode(it.custId, mobile, it.compId)
                    getCompId.onNext(it.compId)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1872)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun verifyOtpCode(): Observable<Long> = verifyOtpCode

    override fun showProgress() = showProgress
    override fun getCompId() = getCompId
}