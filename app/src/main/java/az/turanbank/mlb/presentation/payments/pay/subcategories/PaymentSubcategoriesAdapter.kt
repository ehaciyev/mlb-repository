package az.turanbank.mlb.presentation.payments.pay.subcategories

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.response.payments.merchant.MerchantResponseModel
import az.turanbank.mlb.picasso.PicassoTrustAll
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.subcategory_list_view.view.storyImage
import kotlinx.android.synthetic.main.subcategory_list_view.view.firstText

class PaymentSubcategoriesAdapter(
    private val picassoTrustAll: PicassoTrustAll,
    private val context: Context,
    private val categoryNames: List<MerchantResponseModel>,
    private val clickListeners: (position: Int) -> Unit
) : RecyclerView.Adapter<PaymentSubcategoriesAdapter.PaymentSubcategoriesViewHolder>() {
    val inflater: LayoutInflater = LayoutInflater.from(context)

//    @SuppressLint("ViewHolder")
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View{
//        val view = inflater.inflate(R.layout.subcategory_list_view, parent, false)
//
//        Picasso.with(context).load(categoryNames[position].img).into(view.storyImage)
//        view.firstText.text = categoryNames[position].displayName
//
//        view.setOnClickListener { clickListeners.invoke(position) }
//
//        return view
//    }
//
//    override fun getItem(position: Int) = null
//
//    override fun getItemId(position: Int) = 0L
//
//    override fun getCount() = categoryNames.size
    class PaymentSubcategoriesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(picassoTrustAll: PicassoTrustAll, subcategory: MerchantResponseModel, context: Context, clickListeners: (position: Int) -> Unit){
            itemView.firstText.text = subcategory.displayName
            if(subcategory.img.contains("asanpay")){
                Picasso.with(context).load(subcategory.img).into(itemView.storyImage)
            } else if(subcategory.img.contains("mlb/static")){
                picassoTrustAll.getInstance(context).load(subcategory.img).into(itemView.storyImage)
            } else {
                itemView.storyImage.background = null
            } /*else {
                Picasso.with(context).load(subcategory.img).into(itemView.storyImage)
                //picassoTrustAll.getInstance(context).load(subcategory.img).into(itemView.storyImage)

            }*/
            //Picasso.with(context).load(subcategory.img).into(itemView.storyImage.storyImage)
            itemView.setOnClickListener { clickListeners.invoke(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PaymentSubcategoriesViewHolder = PaymentSubcategoriesViewHolder(
        inflater.inflate(R.layout.subcategory_list_view, parent, false)
    )

    override fun getItemCount() = categoryNames.size

    override fun onBindViewHolder(holder: PaymentSubcategoriesViewHolder, position: Int) {
        holder.bind(picassoTrustAll, categoryNames[position], context, clickListeners)
    }
}