package az.turanbank.mlb.presentation.exchange.fragment.centralbank

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.exchange.ExchangeCentralBankListResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import dagger.android.support.AndroidSupportInjection
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class ExchangeCentralBankFragment : BaseFragment() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ExchangeCentralBankViewModel

    private lateinit var currencyRecyclerView: RecyclerView
    private lateinit var progress: ImageView

    private lateinit var currencyAdapter: CentralBankAdapter

    private var currencyList: ArrayList<ExchangeCentralBankListResponseModel> = arrayListOf()
    private lateinit var failView: TextView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_exchange_central_bank, container, false)

        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL
        currencyRecyclerView = view.findViewById(R.id.central_recycler_view)
        failView = view.findViewById(R.id.fail)
        progress = view.findViewById(R.id.progress)

        animateProgressImage()

        currencyRecyclerView.layoutManager = llm

        currencyAdapter = CentralBankAdapter(currencyList)
        currencyRecyclerView.adapter = currencyAdapter

        setInputListeners()
        setOutputListeners()
        return view
    }

    private fun setOutputListeners() {
        viewModel.outputs.onCashExchangeGot().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE

            failView.visibility = View.GONE

            currencyRecyclerView.visibility = View.VISIBLE
            setRecyclerView(it.exchangeCentralBankList)
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE

            failView.text = getString(ErrorMessageMapper().getErrorMessage(it))
            failView.visibility = View.VISIBLE
            currencyRecyclerView.visibility = View.GONE
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getCentralBankExchange(arguments?.getString("date"))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
        viewModel = ViewModelProvider(this, factory)[ExchangeCentralBankViewModel::class.java]
    }

    private fun setRecyclerView(currencies: ArrayList<ExchangeCentralBankListResponseModel>) {
        currencyList.clear()
        for (i in 0 until currencies.size) {
            if (currencies[i].currency == "AZN") {
                currencies.remove(currencies[i])
                break
            }
        }
        currencyList.addAll(currencies)
        currencyAdapter.notifyDataSetChanged()
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progress.startAnimation(rotate)
    }

}
