package az.turanbank.mlb.presentation.ips.pay.preconfirm.confirm

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.CreateIpsOperationResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.pay.CreateAccountToAccountIpsOperationUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.CreateAccountToAliasIpsOperationUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.CreateAliasToAccountIpsOperationUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.CreateAliasToAliasIpsOperationUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ConfirmWithIPSViewModelInputs : BaseViewModelInputs {
    fun createAccounttoAccountOperation(
        receiverBankId: Long,
        ipsPaymentTypeId:Int,
        branchId: Int,
        branchName: String,
        paymentAmount: Double,
        senderCustomerName: String,
        receiverCustomerName: String,
        senderCustomerIban: String,
        receiverCustomerIban: String,
        additionalInfoAboutPayment: String,
        additionalInfFromCustomer: String,
        remittanceInfo: String,
        budgetLvl: String,
        budgetCode: String
    )
}

interface ConfirmWithIPSViewModelOutputs : BaseViewModelOutputs {
    fun onCreateOperationSucess(): PublishSubject<CreateIpsOperationResponseModel>
    fun showProgress(): PublishSubject<Boolean>
}

class ConfirmWithIPSViewModel @Inject constructor(
    private val accountToAccountIpsOperationUseCase: CreateAccountToAccountIpsOperationUseCase,
    sharedPrefs: SharedPreferences

) : BaseViewModel(), ConfirmWithIPSViewModelInputs, ConfirmWithIPSViewModelOutputs {

    private val showProgress = PublishSubject.create<Boolean>()

    private val createAccount = PublishSubject.create<CreateIpsOperationResponseModel>()

    override fun showProgress() = showProgress

    private val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun createAccounttoAccountOperation(
        receiverBankId: Long,
        ipsPaymentTypeId:Int,
        branchId: Int,
        branchName: String,
        paymentAmount: Double,
        senderCustomerName: String,
        receiverCustomerName: String,
        senderCustomerIban: String,
        receiverCustomerIban: String,
        additionalInfoAboutPayment: String,
        additionalInfFromCustomer: String,
        remittanceInfo: String,
        budgetLvl: String,
        budgetCode: String
    ) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }

        showProgress.onNext(true)
        val compId = if (isCustomerJuridical) compId else null
        accountToAccountIpsOperationUseCase.execute(
            custId,
            compId,
            token,
            enumLangType,
            receiverBankId,
            ipsPaymentTypeId,
            branchId,
            branchName,
            paymentAmount,
            senderCustomerName,
            receiverCustomerName,
            senderCustomerIban,
            receiverCustomerIban,
            additionalInfoAboutPayment,
            additionalInfFromCustomer,
            remittanceInfo,
            budgetLvl,
            budgetCode
        ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
            showProgress.onNext(false)
            if(it.status.statusCode == 1){
                createAccount.onNext(it)
            } else {
                error.onNext(it.status.statusCode)
            }
        },{
            showProgress.onNext(false)
            error.onNext(1878)
        }).addTo(subscriptions)
    }

    override fun onCreateOperationSucess() = createAccount

    override val inputs: ConfirmWithIPSViewModelInputs = this
    override val outputs: ConfirmWithIPSViewModelOutputs = this
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)

    private val compId = sharedPrefs.getLong("compId", 0L)

    private fun runUseCase(){

    }

}