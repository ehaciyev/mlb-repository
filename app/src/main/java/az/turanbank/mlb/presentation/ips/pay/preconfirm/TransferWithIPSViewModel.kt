package az.turanbank.mlb.presentation.ips.pay.preconfirm

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.CreateIpsOperationResponseModel
import az.turanbank.mlb.data.remote.model.service.GetBankInfos
import az.turanbank.mlb.domain.user.usecase.ips.banks.GetAllBanksUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.CreateAccountToAccountIpsOperationUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetAllBudgetCodesUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetAllBudgetLevelUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface TransferWithIPSViewModelInputs : BaseViewModelInputs {
    fun createAccountToAccountIpsOperation(
        receiverBankId: Long,
        ipsPaymentTypeId:Int,
        branchId: Int,
        branchName: String,
        paymentAmount: Double,
        senderCustomerName: String,
        receiverCustomerName: String,
        senderCustomerIban: String,
        receiverCustomerIban: String,
        additionalInfoAboutPayment: String,
        additionalInfFromCustomer: String,
        remittanceInfo: String,
        budgetLvl: String,
        budgetCode: String
    )

    fun getAllBudgetLevelUseCase()
    fun getAllBudgetCodeUseCase()
}

interface TransferWithIPSViewModelOutputs : BaseViewModelOutputs {
    fun createAccountToAccountIpsOperationSuccess(): PublishSubject<CreateIpsOperationResponseModel>
    fun showProgress(): PublishSubject<Boolean>
    fun onSuccessGetAllBudgetLevelUseCase(): PublishSubject<GetBankInfos>
    fun onSuccessGetAllBudgetCodeUseCase(): PublishSubject<GetBankInfos>
}

class TransferWithIPSViewModel @Inject constructor(
    private val createAccountToAccountIpsOperationUseCase: CreateAccountToAccountIpsOperationUseCase,
    private val getAllBudgetLevelsUseCase: GetAllBudgetLevelUseCase,
    private val getAllBudgetCodesUseCase: GetAllBudgetCodesUseCase,
    sharedPrefs: SharedPreferences
) :
    BaseViewModel(),
    TransferWithIPSViewModelInputs,
    TransferWithIPSViewModelOutputs {

    override val inputs: TransferWithIPSViewModelInputs = this

    override val outputs: TransferWithIPSViewModelOutputs = this
    private val createAccToAccIpsOperationSuccess =
        PublishSubject.create<CreateIpsOperationResponseModel>()

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val lang = sharedPrefs.getString("lang", "az")
    var enumLangType = EnumLangType.AZ

    private val showProgress = PublishSubject.create<Boolean>()

    private var selectedCodeId: String? = null
    private var selectedLevelId: String? = null

    private var isGovernmentPayment: Boolean = false

    private val getAllBudgetLevelUseCase = PublishSubject.create<GetBankInfos>()

    private val getAllBudgetCodeUseCase = PublishSubject.create<GetBankInfos>()

    override fun createAccountToAccountIpsOperation(
        receiverBankId: Long,
        ipsPaymentTypeId:Int,
        branchId: Int,
        branchName: String,
        paymentAmount: Double,
        senderCustomerName: String,
        receiverCustomerName: String,
        senderCustomerIban: String,
        receiverCustomerIban: String,
        additionalInfoAboutPayment: String,
        additionalInfFromCustomer: String,
        remittanceInfo: String,
        budgetLvl: String,
        budgetCode: String
    ) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }

        showProgress.onNext(true)

        var userCompId: Long? = null

        if (isCustomerJuridical) userCompId = compId

        createAccountToAccountIpsOperationUseCase.execute(
            custId = custId,
            compId = userCompId,
            token = token,
            receiverBankId = receiverBankId,
            ipsPaymentTypeId = ipsPaymentTypeId,
            branchId = branchId,
            branchName = branchName,
            paymentAmount = paymentAmount,
            senderCustomerName = senderCustomerName,
            receiverCustomerName = receiverCustomerName,
            senderCustomerIban = senderCustomerIban,
            receiverCustomerIban = receiverCustomerIban,
            additionalInfoAboutPayment = additionalInfoAboutPayment,
            additionalInfFromCustomer = additionalInfFromCustomer,
            remittanceInfo = remittanceInfo,
            budgetLvl = budgetLvl,
            budgetCode = budgetCode,
            lang = enumLangType
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    createAccToAccIpsOperationSuccess.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            } , {
                showProgress.onNext(false)
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getAllBudgetLevelUseCase() {
        var userCompId: Long? = null

        if (isCustomerJuridical) userCompId = compId

        getAllBudgetLevelsUseCase.execute(
            custId = custId,
            compId = userCompId,
            token = token
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    getAllBudgetLevelUseCase.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            } , {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getAllBudgetCodeUseCase() {
        var userCompId: Long? = null
        if (isCustomerJuridical) userCompId = compId

        getAllBudgetCodesUseCase.execute(
            custId = custId,
            compId = userCompId,
            token = token
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    getAllBudgetCodeUseCase.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            } , {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun createAccountToAccountIpsOperationSuccess() = createAccToAccIpsOperationSuccess

    override fun showProgress() = showProgress

    override fun onSuccessGetAllBudgetLevelUseCase() = getAllBudgetLevelUseCase
    override fun onSuccessGetAllBudgetCodeUseCase() = getAllBudgetCodeUseCase

    fun setSelectedLevelId(id: String) {
        this.selectedLevelId = id
    }

    fun setSelectedCodeId(id: String) {
        this.selectedCodeId = id
    }

    fun getSelectedLevelId() = selectedLevelId
    fun getSelectedCodeId() = selectedCodeId

}