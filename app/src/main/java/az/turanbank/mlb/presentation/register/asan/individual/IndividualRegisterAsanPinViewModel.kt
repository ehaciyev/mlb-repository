package az.turanbank.mlb.presentation.register.asan.individual

import az.turanbank.mlb.domain.user.usecase.register.asan.LoginWithAsanUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.RegisterWithAsanUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface IndividualRegisterAsanPinViewModelInputs : BaseViewModelInputs {
    fun registerWithAsan(transactionId: Long,
                         certificate: String,
                         challenge: String,
                         custCode: Long,
                         pin: String)
    fun loginWithAsan(transactionId: Long,
                      certificate: String,
                      challenge: String)
}

interface IndividualRegisterAsanPinViewModelOutputs : BaseViewModelOutputs {
    fun registerIndividualAsan(): Observable<Long>
    fun onLogiIndividualAsan(): CompletableSubject
}

class IndividualRegisterAsanPinViewModel @Inject constructor(
    private val registerWithAsan: RegisterWithAsanUseCase,
    private val loginWithAsanUseCase: LoginWithAsanUseCase
) :
    BaseViewModel(),
    IndividualRegisterAsanPinViewModelInputs,
    IndividualRegisterAsanPinViewModelOutputs {

    override val inputs: IndividualRegisterAsanPinViewModelInputs = this
    override val outputs: IndividualRegisterAsanPinViewModelOutputs = this

    private val registerIndividualAsan = PublishSubject.create<Long>()

    private val loginIndividualAsan = CompletableSubject.create()

    override fun registerWithAsan(transactionId: Long,
                                  certificate: String,
                                  challenge: String,
                                  custCode: Long,
                                  pin: String
    ) {
        registerWithAsan.execute(transactionId, certificate, challenge, custCode, pin)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    registerIndividualAsan.onNext(it.custId)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun loginWithAsan(
        transactionId: Long,
        certificate: String,
        challenge: String
    ) {
        loginWithAsanUseCase.execute(transactionId, certificate, challenge)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    loginIndividualAsan.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }


    override fun registerIndividualAsan() = registerIndividualAsan

    override fun onLogiIndividualAsan() = loginIndividualAsan
}