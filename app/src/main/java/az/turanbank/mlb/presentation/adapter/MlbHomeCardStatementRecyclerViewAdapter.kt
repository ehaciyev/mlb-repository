package az.turanbank.mlb.presentation.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.CardStatementModel
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.secondPartOfMoney
import kotlinx.android.synthetic.main.statement_item_layout.view.*
import java.util.ArrayList

class MlbHomeCardStatementRecyclerViewAdapter(
        private val cardStatementModelList: ArrayList<CardStatementModel>,
        private val clickListener: (position: Int) -> Unit): RecyclerView.Adapter<MlbHomeCardStatementRecyclerViewAdapter.StatementViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatementViewHolder =
        StatementViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.statement_item_layout,
                parent,
                false
            )
        )

    override fun getItemCount() = cardStatementModelList.size

    override fun onBindViewHolder(holder: StatementViewHolder, position: Int) =
            holder.bind(cardStatementModelList[position], clickListener)

    class StatementViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(cardStatementModel: CardStatementModel, clickListener: (position: Int) -> Unit) {
            itemView.transactionPurpose.text = cardStatementModel.purpose
            itemView.transactionCurrency.text = cardStatementModel.currency
            itemView.transactionDate.text = cardStatementModel.trDate
            itemView.transactionCard.text = cardStatementModel.cardNumber.replaceRange(4,12,"**** ****")

            if(cardStatementModel.crAmount == 0.0) {
                itemView.transactionAmount.text = firstPartOfMoney(cardStatementModel.dtAmount)
                itemView.transactionAmountReminder.text = secondPartOfMoney(cardStatementModel.dtAmount)
            } else if (cardStatementModel.dtAmount == 0.0) {
                itemView.transactionAmount.text = firstPartOfMoney(cardStatementModel.crAmount)
                itemView.transactionAmountReminder.text = secondPartOfMoney(cardStatementModel.crAmount)
                itemView.transactionAmount.setTextColor(Color.parseColor("#009b53"))
                itemView.transactionAmountReminder.setTextColor(Color.parseColor("#0dd276"))
                itemView.transactionCurrency.setTextColor(Color.parseColor("#0dd276"))
            }
            itemView.setOnClickListener {
                clickListener.invoke(adapterPosition)
            }
        }

    }
}