package az.turanbank.mlb.presentation.exchange.fragment.centralbank

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ExchangeFlagMapper
import az.turanbank.mlb.data.remote.model.exchange.ExchangeCentralBankListResponseModel
import kotlinx.android.synthetic.main.turan_currency_list_item.view.*

class CentralBankAdapter(private val currencyList: ArrayList<ExchangeCentralBankListResponseModel>) :
    RecyclerView.Adapter<CentralBankAdapter.CurrencyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CurrencyViewHolder = CurrencyViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.central_currency_list_item,
            parent,
            false
        )
    )

    override fun getItemCount() = currencyList.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencyList[position])
    }

    class CurrencyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(currency: ExchangeCentralBankListResponseModel) {
            with(currency) {
                itemView.currency_name.text = this.currency
                itemView.currency_sell.text = this.centralBank.toString()
                itemView.currency_icon.setImageResource(ExchangeFlagMapper().resolveFlag(this.currency))
            }
        }
    }
}