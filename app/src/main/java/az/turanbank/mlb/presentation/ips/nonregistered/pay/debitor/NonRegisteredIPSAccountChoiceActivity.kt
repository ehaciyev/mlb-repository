package az.turanbank.mlb.presentation.ips.nonregistered.pay.debitor

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.nonregistered.pay.creditor.NonRegisteredIPSTransferActivity
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.IPSCreditorAccountSelectActivity
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.account.IPSAccountChoiceViewModel
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.util.animateProgressImage
import az.turanbank.mlb.util.revertProgressImageAnimation
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_non_registered_ips_account_choice.*
import kotlinx.android.synthetic.main.mlb_toolbar_with_filter.*
import javax.inject.Inject

class NonRegisteredIPSAccountChoiceActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: NonRegisteredIPSAccountChoiceViewModel

    lateinit var dialog: MlbProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_non_registered_ips_account_choice)

        viewModel =
            ViewModelProvider(this, factory)[NonRegisteredIPSAccountChoiceViewModel::class.java]

        toolbar_title.text = getString(R.string.pay)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        account.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(28))

        setOutputListeners()
        setInputListeners()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_OK)
    }


    private fun setOutputListeners() {

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.accountsSuccess().subscribe { accountList ->
            accountContainer.setOnClickListener {
                val builder: android.app.AlertDialog.Builder =
                    android.app.AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.select_account))

                val list = arrayOfNulls<Long>(accountList.size)
                val nameList = arrayOfNulls<String>(accountList.size)

                for (i in 0 until accountList.size) {
                    list[i] = accountList[i].accountId
                    nameList[i] = accountList[i].iban
                }

                builder.setItems(nameList) { _, which ->
                    account.text = nameList[which]
                    viewModel.setSelectedAccount(nameList[which])
                    viewModel.setSelectedAccountId(list[which])
                }

                val dialog = builder.create()
                dialog.show()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onSenderSuccess().subscribe{
            val intent = Intent(this, IPSCreditorAccountSelectActivity::class.java)
            intent.putExtra("senderAccountOrAlias", viewModel.getSelectedAccount())
            intent.putExtra("senderId", viewModel.getSelectedAccountId())
            intent.putExtra("senderFullName",it.accountName)
            intent.putExtra("fromAccount",true)
            startActivityForResult(intent,7)
            showProgressBar(false)
        }.addTo(subscriptions)
        
        viewModel.outputs.onError().subscribe{
            AlertDialogMapper(this, 126).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getNonRegisteredAccounts()
        submitButton.setOnClickListener {
            showProgressBar(true)
            if(account.text.trim().isEmpty()){
                AlertDialogMapper(this, 124018).showAlertDialog()
                showProgressBar(false)
            } else {
                viewModel.inputs.getSenderInfo(viewModel.getSelectedAccountId())
            }
        }
    }

    private fun showProgressBar(show: Boolean) {
        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        accountContainer.isClickable = !show
        account.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}