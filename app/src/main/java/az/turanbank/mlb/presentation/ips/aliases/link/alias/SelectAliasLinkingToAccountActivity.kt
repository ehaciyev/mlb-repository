package az.turanbank.mlb.presentation.ips.aliases.link.alias

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasSelectResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.aliases.link.account.SelectAccountLinkingToAliasActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_delete_ipsalias.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class SelectAliasLinkingToAccountActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: LinkAliasToAccountViewModel

    lateinit var adapter: InstantPaymentSingleSelectAliasAdapter

    var preSelectedIndex = -1

    private val aliases = arrayListOf<MLBAliasResponseModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_ipsalias)

        viewModel =
            ViewModelProvider(this, factory)[LinkAliasToAccountViewModel::class.java]

        toolbar_title.text = getString(R.string.link_alias)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        message.text = getString(R.string.select_alias_want_to_link)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        adapter =
            InstantPaymentSingleSelectAliasAdapter(
                applicationContext,
                aliases
            ) {
                /*var mlbAliasResponseModel: MLBAliasResponseModel = aliases[it]
                mlbAliasResponseModel.isSelected = true

                aliases[it] = mlbAliasResponseModel

                if(preSelectedIndex > -1){
                    var preMLBAliasResponseModel: MLBAliasResponseModel = aliases[preSelectedIndex]
                    preMLBAliasResponseModel.isSelected = false

                    aliases[preSelectedIndex] = preMLBAliasResponseModel
                }

                preSelectedIndex = it

                adapter.updateRecords(aliases)*/

                aliases[it].isSelected = !aliases[it].isSelected
                adapter.notifyDataSetChanged()
            }

        recyclerView.adapter = adapter

        //animateProgress()

        setOutputListeners()
        setInputListeners()
    }

    override fun onResume() {
        super.onResume()
        recyclerViewShimmer.startShimmerAnimation()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 12){
            if (resultCode == Activity.RESULT_OK){
                recyclerViewShimmer.stopShimmerAnimation()
                recyclerViewShimmer.visibility = View.GONE
                Toast.makeText(this, ErrorMessageMapper().getErrorMessage(124012), Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }
    private fun setOutputListeners() {
        viewModel.outputs.onAliases().subscribe {
            recyclerViewShimmer.stopShimmerAnimation()
            recyclerViewShimmer.visibility = View.GONE
            //progress.clearAnimation()
            //progress.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            it.forEach { model ->
                Log.i("MODEL", model.toString())
            }
            aliases.clear()
            aliases.addAll(it)
            adapter.setData(it)
            recyclerView.adapter?.notifyDataSetChanged()
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            recyclerViewShimmer.stopShimmerAnimation()
            recyclerViewShimmer.visibility = View.GONE
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getAliases()
        submitButton.setOnClickListener {
            if(adapter.getSelectedPosition() != null){
                val intent = Intent(this, SelectAccountLinkingToAliasActivity::class.java)
                intent.putExtra("aliasId", adapter.getSelectedItem()!!.id)
                intent.putExtra("aliasValue",adapter.getSelectedItem()!!.ipsValue)
                startActivityForResult(intent, 12)
            } else {
                AlertDialogMapper(this,8000).showAlertDialog()
            }
            /*var isSelected = false
            val list = arrayListOf<MLBAliasSelectResponseModel>()
            aliases.forEach {
                if (it.isSelected){
                    isSelected = true
                    list.add(
                        MLBAliasSelectResponseModel(
                            it.id,
                            it.ipsValue
                        )
                    )
                }
            }*/

           /* if(isSelected){

            } else {
                AlertDialogMapper(this, 8000).showAlertDialog()
            }*/
        }
    }

    private fun animateProgress() {
        recyclerView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progress.startAnimation(rotate)
    }

    override fun onDestroy() {
        super.onDestroy()
        recyclerViewShimmer.stopShimmerAnimation()
    }

}