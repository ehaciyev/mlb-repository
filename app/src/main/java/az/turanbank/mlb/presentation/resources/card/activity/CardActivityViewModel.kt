package az.turanbank.mlb.presentation.resources.card.activity

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.card.CardListResponseModel
import az.turanbank.mlb.data.remote.model.response.CardStatementModel
import az.turanbank.mlb.domain.user.usecase.resources.card.*
import az.turanbank.mlb.domain.user.usecase.resources.card.blocking.*
import az.turanbank.mlb.domain.user.usecase.resources.card.statement.GetCardStatementForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.statement.GetCardStatementForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

interface CardActivityViewModelInputs : BaseViewModelInputs {
    fun getCardStatement(cardNumber: String)
    fun blockCard(cardId: Long, blockReason: String)
    fun getBlockReasons()
    fun getTheCard(cardId: Long)
    fun unblockCard(cardId: Long)
}

interface CardActivityViewModelOutputs : BaseViewModelOutputs {
    fun onCardSuccess(): PublishSubject<CardListResponseModel>
    fun cardStatementSuccess(): PublishSubject<ArrayList<CardStatementModel>>
    fun blockReasonsSuccess(): PublishSubject<Array<String>>
    fun blockCardSuccess(): BehaviorSubject<Boolean>
    fun onStatementError(): PublishSubject<Int>
}

class CardActivityViewModel @Inject constructor(
    private val statementForIndividualCustomerUseCase: GetCardStatementForIndividualCustomerUseCase,
    private val statementForJuridicalCustomerUseCase: GetCardStatementForJuridicalCustomerUseCase,

    private val cardIndividualByIdIndividualUseCase: GetCardByIdIndividualUseCase,
    private val cardIndividualByIdJuridicalUseCase: GetCardByIdJuridicalUseCase,

    private val blockReasonsUseCase: BlockReasonsUseCase,

    private val blockCardForIndividualCustomerUseCase: BlockCardForIndividualCustomerUseCase,
    private val blockCardForJuridicalCustomerUseCase: BlockCardForJuridicalCustomerUseCase,

    private val unblockCardForIndividualCustomerUseCase: UnblockCardForIndividualCustomerUseCase,
    private val unblockCardForJuridicalCustomerUseCase: UnblockCardForJuridicalCustomerUseCase,

    private val sharedPrefs: SharedPreferences

) : BaseViewModel(),
    CardActivityViewModelInputs,
    CardActivityViewModelOutputs {


    override val inputs: CardActivityViewModelInputs = this

    override val outputs: CardActivityViewModelOutputs = this


    private val statement = PublishSubject.create<ArrayList<CardStatementModel>>()
    private val blockReason = PublishSubject.create<Array<String>>()
    private val blockCard = BehaviorSubject.create<Boolean>()
    val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val card = PublishSubject.create<CardListResponseModel>()
    private val statementError = PublishSubject.create<Int>()

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang", "en")
    var enumLangType = EnumLangType.EN

    override fun getTheCard(cardId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        if (isCustomerJuridical) {
            cardIndividualByIdJuridicalUseCase
                .execute(custId, compId, cardId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        card.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            cardIndividualByIdIndividualUseCase
                .execute(custId, cardId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        card.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun blockCard(cardId: Long, blockReason: String) {
        if (isCustomerJuridical) {
            blockCardForJuridicalCustomerUseCase
                .execute(custId, compId, token, cardId, blockReason)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        blockCard.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {

                }).addTo(subscriptions)
        } else {
            blockCardForIndividualCustomerUseCase
                .execute(custId, token, cardId, blockReason)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        blockCard.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun unblockCard(cardId: Long) {
        if (isCustomerJuridical) {
            unblockCardForJuridicalCustomerUseCase
                .execute(custId, compId, token, cardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        blockCard.onNext(false)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            unblockCardForIndividualCustomerUseCase
                .execute(custId, token, cardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        blockCard.onNext(false)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun getCardStatement(cardNumber: String) {
        val cDate = Date()
        val fDate = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(cDate).replace("-", ".")

        if (isCustomerJuridical) {
            //TODO change back go juridical & add compId
            //TODO Use fdate for start and end date
            statementForJuridicalCustomerUseCase.execute(
                custId = custId,
                compId = compId,
                cardNumber = cardNumber,
                token = token,
                startDate = fDate,
                endDate = fDate
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        statement.onNext(it.cardStamentList)
                    } else {
                        statementError.onNext(it.status.statusCode)
                    }
                }
                    , {
                        error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)

        } else {
            statementForIndividualCustomerUseCase.execute(
                custId = custId,
                cardNumber = cardNumber,
                token = token,
                startDate = fDate,
                endDate = fDate
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        statement.onNext(it.cardStamentList)
                    } else {
                        statementError.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                })
                .addTo(subscriptions)
        }
    }

    override fun getBlockReasons() {
        if (lang != null) {
            when(lang){
                "az" -> enumLangType = EnumLangType.AZ
                "ru" -> enumLangType = EnumLangType.RU
                "en" -> enumLangType = EnumLangType.EN
            }
            blockReasonsUseCase.execute(custId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        blockReason.onNext(it.blockReasonList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun blockReasonsSuccess() = blockReason

    override fun cardStatementSuccess() = statement

    override fun blockCardSuccess() = blockCard

    override fun onCardSuccess() = card

    override fun onStatementError() = statementError
}