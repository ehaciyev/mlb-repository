package az.turanbank.mlb.presentation.ips.aliases.delete

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.WarningPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_delete_ipsalias.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class DeleteIPSAliasActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: DeleteIPSAliasViewModel

    lateinit var adapter: DeleteSingleSelectAliasAdapter

    private val aliases = arrayListOf<MLBAliasResponseModel>()

    private var globalPosition: Int? = null

    private var selectedAliasId = arrayListOf<Long>()

    private var deletedAliasId = arrayListOf<Long>()
    private var deletedAccountIban = ""

    //private var deletedAliasId = 0L
    //private var deletedAliasValue = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_ipsalias)

        viewModel =
            ViewModelProvider(this, factory)[DeleteIPSAliasViewModel::class.java]

        toolbar_title.text = getString(R.string.delete_aias)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        adapter = DeleteSingleSelectAliasAdapter(applicationContext,aliases) { position ->
            //globalPosition = position
            //deletedAliasId = aliases[position].id
            //deletedAliasValue = aliases[position].ipsValue
            //animateProgress()

            aliases[position].isSelected = !aliases[position].isSelected
            adapter.notifyDataSetChanged()
        }

        recyclerView.adapter = adapter

        //animateProgress()

        recyclerViewShimmer.visibility = View.VISIBLE
        recyclerViewShimmer.startShimmerAnimation()

        setOutputListeners()
        setInputListeners()
    }

    override fun onResume() {
        super.onResume()
        viewModel.inputs.getAliases()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        recyclerViewShimmer.stopShimmerAnimation()
        recyclerViewShimmer.visibility = View.GONE
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                viewModel.inputs.deleteAlias(deletedAliasId)
            }
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onAliases().subscribe {
            recyclerViewShimmer.stopShimmerAnimation()
            recyclerViewShimmer.visibility = View.GONE

            showProgressBar(false)

            recyclerView.visibility = View.VISIBLE
            it.forEach { model ->
                Log.i("MODEL", model.toString())
            }
            aliases.clear()
            aliases.addAll(it)
            recyclerView.adapter?.notifyDataSetChanged()
        }.addTo(subscriptions)

       /* viewModel.outputs.defaultAccountFound().subscribe {
            val intent = Intent(this, WarningPageViewActivity::class.java)
            val description = if(it == "no"){
                getString(R.string.are_you_sure_to_delete_alias,  adapter.getSelectedItem()!!.ipsValue)
            } else {
                getString(R.string.the_account_linked_the_alias, it, adapter.getSelectedItem()!!.ipsValue)
            }
            intent.putExtra("fromDeleteAlias", true)
            intent.putExtra("description", description)
            intent.putExtra("account", it)
            startActivityForResult(intent, 7)

        }.addTo(subscriptions)*/
        viewModel.outputs.aliasDeleted().subscribe {
            if (it) {
                //deletedAliasId = 0
                Toast.makeText(
                    this,
                    ErrorMessageMapper().getErrorMessage(124010),
                    Toast.LENGTH_LONG
                ).show()
                viewModel.inputs.getAliases()
                //animateProgress()
                /*recyclerViewShimmer.visibility = View.VISIBLE
                recyclerViewShimmer.startShimmerAnimation()*/
                /*Toast.makeText(
                    this,
                    ErrorMessageMapper().getErrorMessage(124010),
                    Toast.LENGTH_LONG
                ).show()*/
            }
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            if(it == 126){
                message.visibility = View.GONE
                recyclerView.visibility = View.GONE
                submitButton.visibility = View.GONE
                noDataAvailable.visibility = View.VISIBLE
            } else {
                AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
            }
        }.addTo(subscriptions)

        viewModel.outputs.unSuccessfullAliases().subscribe{ it ->

            val deleted = arrayListOf<MLBAliasResponseModel>()
            val unDeleted = arrayListOf<MLBAliasResponseModel>()


            it.forEach {
                if(it.respStatus.statusCode == 1){
                    deleted.add(it)
                } else {
                    unDeleted.add(it)
                }
            }

            showProgressBar(false)
            val intent = Intent(this, WarningPageViewActivity::class.java)
            intent.putExtra("description", "aliasDeleteError" )
            intent.putExtra("deleted",deleted)
            intent.putExtra("undDeleted",unDeleted)
            startActivityForResult(intent,7)

        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getAliases()

        submitButton.setOnClickListener {
            /*if(adapter.getSelectedPosition() == null){
                AlertDialogMapper(this,8000).showAlertDialog()
            } else {
                viewModel.inputs.getDefaultAccount(aliases[adapter.getSelectedPosition()!!].ipsType, aliases[adapter.getSelectedPosition()!!].ipsValue)
            }*/

            var isSelected = false
            val list = arrayListOf<MLBAliasResponseModel>()
            deletedAliasId = arrayListOf()
            aliases.forEach {
                if (it.isSelected) {
                    isSelected = true
                    list.add(it)
                    deletedAliasId.add(it.id)
                }
            }

            if (deletedAliasId.size != 0) {
                showProgressBar(true)
                viewModel.inputs.deleteAlias(deletedAliasId)
            } else {
                AlertDialogMapper(this, 8000).showAlertDialog()
            }
        }

    }

    override fun onPause() {
        super.onPause()
        //progress.clearAnimation()
        //progress.visibility = View.GONE

        recyclerViewShimmer.stopShimmerAnimation()
        recyclerViewShimmer.visibility = View.GONE

        recyclerView.visibility = View.VISIBLE

    }

    private fun animateProgressImage() {
        progressImage.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            animateProgressImage()
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            progressImage.clearAnimation()
        }
    }
}
