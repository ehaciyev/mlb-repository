package az.turanbank.mlb.presentation.register.mobile.individual

import android.annotation.SuppressLint
import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.block.BlockIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.block.BlockJuridicalCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.block.UnblockIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.block.UnblockJuridicalCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.SendOTPCodeUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.VerifyOTPCodeUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface VerifyIndividualRegisterMobileViewModelInputs : BaseViewModelInputs {
    fun verifyOtpCode(custId: Long, compId: Long?, confirmCode: String)
    fun sendPinAgain(custId: Long, compId: Long?, mobile: String)
    fun blockUser(custId: Long, compId: Long)
    fun unblockIndividualCustomer(custId: Long)
    fun unblockJuridicalCustomer(custId: Long, compId: Long)
}

interface VerifyIndividualRegisterMobileViewModelOutputs : BaseViewModelOutputs {
    fun onCodeCorrect(): CompletableSubject
    fun onSendPinAgain(): PublishSubject<Boolean>
    fun onCustomerUnblocked(): CompletableSubject
    fun onCustomerBlocked(): CompletableSubject
}

class VerifyIndividualRegisterMobileViewModel @Inject constructor(
    private val verifyOTPCodeUseCase: VerifyOTPCodeUseCase,
    private val sendOTPCodeUseCase: SendOTPCodeUseCase,
    private val blockIndividualCustomerUseCase: BlockIndividualCustomerUseCase,
    private val blockJuridicalCustomerUseCase: BlockJuridicalCustomerUseCase,
    private val unblockIndividualCustomerUseCase: UnblockIndividualCustomerUseCase,
    private val unblockJuridicalCustomerUseCase: UnblockJuridicalCustomerUseCase,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel(),
    VerifyIndividualRegisterMobileViewModelInputs,
    VerifyIndividualRegisterMobileViewModelOutputs {

    private val correctCode = CompletableSubject.create()

    private val sendPinAgain = PublishSubject.create<Boolean>()
    private val unblockCustomer = CompletableSubject.create()
    private val blockCustomer = CompletableSubject.create()

    override val inputs: VerifyIndividualRegisterMobileViewModelInputs = this

    override val outputs: VerifyIndividualRegisterMobileViewModelOutputs = this

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    @SuppressLint("CheckResult")
    override fun verifyOtpCode(custId: Long, compId: Long?, confirmCode: String) {
        verifyOTPCodeUseCase.execute(custId, compId, confirmCode, 1)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.status.statusCode == 1) {
                    correctCode.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            })
    }

    override fun sendPinAgain(custId: Long, compId: Long?, mobile: String) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        sendOTPCodeUseCase.execute(custId, compId, mobile, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it.status.statusCode) {
                    1 -> {
                        sendPinAgain.onNext(true)
                    }
                    else -> error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun unblockIndividualCustomer(custId: Long) {
        unblockIndividualCustomerUseCase.execute(custId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    unblockCustomer.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun unblockJuridicalCustomer(custId: Long, compId: Long) {
        unblockJuridicalCustomerUseCase.execute(custId, compId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    unblockCustomer.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun blockUser(custId: Long, compId: Long) {
        if (compId == 0L) {
            blockIndividualCustomerUseCase.execute(custId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        blockCustomer.onComplete()
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                }).addTo(subscriptions)
        } else {
            blockJuridicalCustomerUseCase.execute(custId, compId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        blockCustomer.onComplete()
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                    it.printStackTrace()
                }).addTo(subscriptions)
        }
    }

    override fun onCustomerUnblocked() = unblockCustomer

    override fun onCodeCorrect() = correctCode

    override fun onSendPinAgain() = sendPinAgain

    override fun onCustomerBlocked() = blockCustomer
}