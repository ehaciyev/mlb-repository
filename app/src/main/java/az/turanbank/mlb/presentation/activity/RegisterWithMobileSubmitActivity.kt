package az.turanbank.mlb.presentation.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import az.turanbank.mlb.R
import kotlinx.android.synthetic.main.mlb_toolbar_layout.toolbar_back_button
import kotlinx.android.synthetic.main.mlb_toolbar_layout.toolbar_title

class RegisterWithMobileSubmitActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_with_mobile_submit)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        toolbar_title.text = resources.getString(R.string.register_with_mobile)
    }
}
