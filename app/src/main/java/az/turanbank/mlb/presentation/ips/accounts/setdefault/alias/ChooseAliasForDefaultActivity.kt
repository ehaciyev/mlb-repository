package az.turanbank.mlb.presentation.ips.accounts.setdefault.alias

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.ips.AliasAccountCompoundModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.accounts.setdefault.ChooseAliasForDefaultViewModel
import az.turanbank.mlb.presentation.ips.accounts.setdefault.account.ChooseAccountForDefaultActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_delete_ipsalias.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class ChooseAliasForDefaultActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ChooseAliasForDefaultViewModel

    lateinit var adapter: ChooseAliasForDefaultAdapter

    private val aliases = arrayListOf<AliasAccountCompoundModel>()


    val linkedAccountList = arrayListOf<AliasAccountCompoundModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_ipsalias)

        viewModel =
            ViewModelProvider(this, factory)[ChooseAliasForDefaultViewModel::class.java]

        message.visibility = View.GONE
        submitButton.visibility = View.GONE
        //message.text = getString(R.string.choose_default_alias_for_account)
        toolbar_title.text = getString(R.string.choose_default_account)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        adapter =
            ChooseAliasForDefaultAdapter(
                aliases
            ) { position ->
                aliases.forEach {
                    if (aliases[position].alias == it.alias) {
                        linkedAccountList.add(it)
                    }
                }
            }

        recyclerView.adapter = adapter

        //animateProgress()
        recyclerViewShimmer.visibility = View.VISIBLE
        recyclerViewShimmer.startShimmerAnimation()


        setOutputListeners()
        setInputListeners()

    }

    private fun setOutputListeners() {
        var ibans = arrayListOf<String>()

        viewModel.outputs.onCompounds().subscribe {
            recyclerViewShimmer.stopShimmerAnimation()
            recyclerViewShimmer.visibility = View.GONE
            progress.clearAnimation()
            progress.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            aliases.clear()
            aliases.addAll(it)
            recyclerView.adapter?.notifyDataSetChanged()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            recyclerViewShimmer.stopShimmerAnimation()
            recyclerViewShimmer.visibility = View.GONE
            message.visibility = View.GONE
            recyclerView.visibility = View.GONE
            if(it==285){
                noDataAvailable.visibility = View.VISIBLE
            } else {
                AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
            }

        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getCompounds()
        submitButton.setOnClickListener {
            startActivity(Intent(this, ChooseAccountForDefaultActivity::class.java).putExtra("linkedAccountList", linkedAccountList))
        }
    }

    override fun onPause() {
        super.onPause()
        progress.clearAnimation()
        progress.visibility = View.GONE

        recyclerViewShimmer.stopShimmerAnimation()
        recyclerViewShimmer.visibility = View.GONE

        recyclerView.visibility = View.VISIBLE
    }

    private fun animateProgress() {
        progress.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progress.startAnimation(rotate)
    }
}
