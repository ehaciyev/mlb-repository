package az.turanbank.mlb.presentation.register.asan.juridical

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.response.CompanyModel
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.register.mobile.individual.IndividualRegisterMobileSubmitActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_juridical_register_companies_list.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class JuridicalRegisterCompaniesListActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewmodel: JuridialRegisterCompaniesListViewModel

    private val pin: String by lazy { intent.getStringExtra("pin") }
    private val mobile: String by lazy { intent.getStringExtra("phoneNumber") }
    private val userId: String by lazy { intent.getStringExtra("asanId") }

    private var mCompanyList: ArrayList<CompanyModel> = arrayListOf()
    lateinit var mCompanyListAdapter: CompaniesListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_juridical_register_companies_list)

        toolbar_back_button.setOnClickListener { onBackPressed() }

        viewmodel = ViewModelProvider(
            this,
            viewModelFactory
        )[JuridialRegisterCompaniesListViewModel::class.java]

        toolbar_title.text = getString(R.string.register_with_asan)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        companiesRecyclerView.layoutManager = llm

        mCompanyListAdapter = CompaniesListAdapter(mCompanyList) {
            viewmodel.inputs.getCustCompany(
                pin,
                mCompanyList[it].taxNo,
                mobile,
                userId
            )
        }

        companiesRecyclerView.adapter = mCompanyListAdapter

        setupInputListeners()
        setupOutputListeners()
    }

    private fun setupOutputListeners() {
        viewmodel.outputs.companiesError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)

        viewmodel.outputs.companies().subscribe {
            setupRecyclerView(it)
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)

        viewmodel.outputs.custCompany().subscribe {
            val intent = Intent(this@JuridicalRegisterCompaniesListActivity, IndividualRegisterMobileSubmitActivity::class.java)

            intent.putExtra("pin", pin)
            intent.putExtra("taxNo", it.company.taxNo)
            intent.putExtra("custId", it.customer.custId)
            intent.putExtra("compId", it.company.id)
            intent.putExtra("registrationType", "asanJuridical")

            startActivity(intent)
            finish()
        }.addTo(subscriptions)
    }

    private fun setupRecyclerView(companies: List<CompanyModel>) {
        mCompanyList.clear()
        mCompanyList.addAll(companies)
        companiesRecyclerView.adapter?.notifyDataSetChanged()
    }

    private fun setupInputListeners() {
        viewmodel.inputs.getCompaniesCert(pin,mobile, userId)
    }
}
