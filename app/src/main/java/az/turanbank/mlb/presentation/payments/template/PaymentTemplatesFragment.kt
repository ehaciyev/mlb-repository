package az.turanbank.mlb.presentation.payments.template


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.pg.response.payments.template.PaymentTemplateResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.payments.pay.params.PaymentParametersActivity
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class PaymentTemplatesFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: PaymentTemplatesViewModel

    private val templateList = arrayListOf<PaymentTemplateResponseModel>()

    lateinit var failMessage: TextView
    lateinit var recyclerView: RecyclerView
    private lateinit var progress: ImageView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_payment_templates, container, false)

        failMessage = view.findViewById(R.id.failMessage)
        recyclerView = view.findViewById(R.id.recyclerView)

        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = PaymentTemplateAdapter(templateList) {
            val intent = Intent(requireActivity(), PaymentParametersActivity::class.java)
            intent.putExtra("merchantId", templateList[it].merchantId)
            intent.putExtra("templateId", templateList[it].id)
            intent.putExtra("merchantName", templateList[it].tempName)
            intent.putExtra("description", templateList[it].description)
            intent.putExtra("categoryId", templateList[it].categoryId)
            intent.putExtra("fromTemplate", true)
            intent.putExtra("amount", templateList[it].amount)
            startActivity(intent)
        }

        viewModel =
            ViewModelProvider(this, factory)[PaymentTemplatesViewModel::class.java]

        setOutputListeners()
        setInputListeners()

        return view
    }

    override fun onResume() {
        super.onResume()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onTemplateSuccess().subscribe {
            failMessage.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            templateList.clear()
            templateList.addAll(it)
            recyclerView.adapter?.notifyDataSetChanged()
        }
        viewModel.outputs.onError().subscribe {
            failMessage.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getTemplates()
    }
}
