package az.turanbank.mlb.presentation.resources.account

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_sign_file_first_auth.*
import javax.inject.Inject

class SignFileFirstAuthActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory
    private val successOperId: ArrayList<Long> by lazy { intent.getSerializableExtra("successOperId") as ArrayList<Long> }
    private val failOperId: ArrayList<Long> by lazy { intent.getSerializableExtra("failOperId") as ArrayList<Long> }
    private val vCode: String by lazy { intent.getStringExtra("verificationCode") }
    private val transactionId: Long by lazy { intent.getLongExtra("transactionId", 0L) }
    private val batchId: Long by lazy { intent.getLongExtra("batchId", 0L) }

    lateinit var viewmodel: SignFileFirstAuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_file_first_auth)

        viewmodel = ViewModelProvider(this, factory) [SignFileFirstAuthViewModel::class.java]
        firstAuthVerificationCode.setText(vCode)
        showProgressBar(true)

        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        viewmodel.outputs.showProgress().subscribe {
            showProgressBar(it)
        }.addTo(subscriptions)

        viewmodel.inputs.signFile(transactionId, batchId,successOperId,failOperId)
    }

    private fun setOutputListeners() {
        viewmodel.outputs.signFileSuccess().subscribe {
            if(it.status.statusCode == 1) {
                val intent = Intent()
                intent.putExtra("description", getString(R.string.docs_sign_success))
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
                val intent = Intent()
                intent.putExtra("description", getString(R.string.docs_sign_fail))
                setResult(Activity.RESULT_CANCELED, intent)
                finish()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            val intent = Intent(this, ErrorPageViewActivity::class.java)
            intent.putExtra("description", getString(R.string.default_error_message))
            startActivity(intent)
            finish()
        }.addTo(subscriptions)
    }

    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
