package az.turanbank.mlb.presentation.ips.payrequest.requestor.account


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.payrequest.sender.IPSPaySenderActivity
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class ChooseIPSRequestedAccountFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ChooseRequestedAccountViewModel

    private lateinit var accountContainer: LinearLayout
    private lateinit var progressImage: ImageView
    private lateinit var submitText: TextView

    lateinit var submitButton: LinearLayout

    lateinit var account: AppCompatTextView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_choose_requested_account, container, false)

        viewModel =
            ViewModelProvider(this, factory)[ChooseRequestedAccountViewModel::class.java]

        accountContainer = view.findViewById(R.id.accountContainer)
        submitButton = view.findViewById(R.id.submitButton)
        progressImage = view.findViewById(R.id.progressImage)
        submitText = view.findViewById(R.id.submitText)
        account = view.findViewById(R.id.account)

        setInputListeners()
        setOutputListeners()


        return view
    }

    private fun setOutputListeners() {
        viewModel.outputs.accountsSuccess().subscribe { accountList ->
            accountContainer.setOnClickListener {
                val builder: android.app.AlertDialog.Builder =
                    android.app.AlertDialog.Builder(requireContext())
                builder.setTitle(getString(R.string.select_account))
                val list = arrayOfNulls<String>(accountList.size)

                for (i in 0 until accountList.size) {
                    list[i] = accountList[i]
                }
                builder.setItems(list) { _, which ->
                    account.text = accountList[which]
                }
                val dialog = builder.create()
                dialog.show()
            }
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getAccounts()
        submitButton.setOnClickListener {
            showProgressBar(true)
            if (account.text.toString() == getString(R.string.select_account)) {
                AlertDialogMapper(requireActivity(), 124008).showAlertDialog()
                showProgressBar(false)
            } else {
                val intent = Intent(requireActivity(), IPSPaySenderActivity::class.java)
                intent.putExtra("creditorAccountOrAlias", account.text.toString())
                startActivity(intent)
                requireActivity().finish()
            }
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        accountContainer.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.light_gray
                )
            )
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.colorPrimary
                )
            )
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }
}
