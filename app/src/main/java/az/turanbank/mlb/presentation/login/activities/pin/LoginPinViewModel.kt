package az.turanbank.mlb.presentation.login.activities.pin

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetProfileImageResponseModel
import az.turanbank.mlb.domain.user.usecase.GetProfileImageUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface LoginPinViewModelInputs : BaseViewModelInputs {
    fun savePin(pin: String)
    fun checkPin(pin: String)
    fun fingerprintIsSet(authenticated: Boolean)
    fun checkIfFingerPrintIsSet()
    fun login()
    fun getProfileImage()
    fun checkInternet()
}

interface LoginPinViewModelOutputs : BaseViewModelOutputs {
    fun pinIsCorrect(): Observable<Boolean>
    fun fingerprintIsSet(): Observable<Boolean>
    fun fullName(): String?
    fun getProfileImageSuccess(): PublishSubject<GetProfileImageResponseModel>
    fun onSavePinSuccess(): PublishSubject<Boolean>
    fun onInternetSuccess(): PublishSubject<Boolean>
    fun showProgress(): PublishSubject<Boolean>

}

class LoginPinViewModel @Inject constructor(
    private val getProfileImageUseCase: GetProfileImageUseCase,
    private val sharedPreferences: SharedPreferences,
    private val checkInternetConnectionUseCase: CheckInternetConnectionUseCase
):
    BaseViewModel(),
        LoginPinViewModelInputs,
        LoginPinViewModelOutputs {
    private val isCustomerJuridical = sharedPreferences.getBoolean("isCustomerJuridical", false)
    private val getProfileImageSuccess = PublishSubject.create<GetProfileImageResponseModel>()

    private val showProgress = PublishSubject.create<Boolean>()

    private val onInternetSuccess = PublishSubject.create<Boolean>()


    val token = sharedPreferences.getString("token", "")
    val custId = sharedPreferences.getLong("custId", 0L)
    val compId = sharedPreferences.getLong("compId", 0L)

    private val onSavePinSuccess = PublishSubject.create<Boolean>()
    override fun getProfileImageSuccess() = getProfileImageSuccess
    override fun onSavePinSuccess() = onSavePinSuccess
    override fun onInternetSuccess() = onInternetSuccess
    override fun showProgress() = showProgress


    override fun getProfileImage() {
        showProgress.onNext(true)
        var custCompId: Long? = null

        if (isCustomerJuridical) custCompId = compId

        getProfileImageUseCase.execute(custId, custCompId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    showProgress.onNext(false)
                    getProfileImageSuccess.onNext(it)
                }
            }, {
                showProgress.onNext(false)
                it.printStackTrace()
                //error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun checkInternet() {
        checkInternetConnectionUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    onInternetSuccess.onNext(true)
                } else {
                    onInternetSuccess.onNext(false)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    fun getCurrentPin(): String? = existingPin

    override fun login() {
        sharedPreferences.edit().putBoolean("userLoggedIn", true).apply()
    }

    override fun fullName() = sharedPreferences.getString("fullname", "")

    override fun checkIfFingerPrintIsSet() {
            fingerprintIsSet.onNext(sharedPreferences.getBoolean("fingerprintIsSet", false))
    }

    override fun fingerprintIsSet(): Observable<Boolean> = fingerprintIsSet

    override fun fingerprintIsSet(authenticated: Boolean) {
        sharedPreferences.edit().putBoolean("fingerprintIsSet", authenticated).apply()
    }

    override fun pinIsCorrect() = pinIsCorrect

    override fun checkPin(pin: String) {
      if(pin == existingPin) {
          pinIsCorrect.onNext(true)
      } else {
          pinIsCorrect.onNext(false)
      }
    }

    private val existingPin: String? by lazy {
        sharedPreferences.getString("loginPin" , null)
    }

    override val inputs: LoginPinViewModelInputs = this
    override val outputs: LoginPinViewModelOutputs = this
    private var pinIsCorrect = PublishSubject.create<Boolean>()
    private var fingerprintIsSet = PublishSubject.create<Boolean>()

    override fun savePin(pin: String) {
        sharedPreferences.edit().putBoolean("pinIsSet", true).apply()
        sharedPreferences.edit().putString("loginPin", pin).apply()
        onSavePinSuccess.onNext(true)
    }
}