package az.turanbank.mlb.presentation.register.asan.juridical

import az.turanbank.mlb.data.remote.model.response.CompanyModel
import az.turanbank.mlb.data.remote.model.response.GetCustCompanyResponseModel
import az.turanbank.mlb.domain.user.usecase.register.asan.GetCustCompanyUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.JuridicalRegisterGetCompaniesCertUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface JuridialRegisterCompaniesListViewModelInputs : BaseViewModelInputs {
    fun getCompaniesCert(
        pin: String,
        mobile: String,
        userId: String
    )

    fun getCustCompany(
        pin: String,
        taxNo: String,
        mobile: String,
        userId: String
    )
}

interface JuridialRegisterCompaniesListViewModelOutputs : BaseViewModelOutputs {
    fun companies(): Observable<List<CompanyModel>>
    fun companiesError(): PublishSubject<Int>
    fun custCompany(): Observable<GetCustCompanyResponseModel>
}

class JuridialRegisterCompaniesListViewModel @Inject constructor(
    private val juridicalRegisterGetCompaniesCertUseCase: JuridicalRegisterGetCompaniesCertUseCase,
    private val getCustCompanyUseCase: GetCustCompanyUseCase
) :
    BaseViewModel(),
    JuridialRegisterCompaniesListViewModelInputs,
    JuridialRegisterCompaniesListViewModelOutputs
{
    override val inputs: JuridialRegisterCompaniesListViewModelInputs = this
    override val outputs: JuridialRegisterCompaniesListViewModelOutputs = this

    private val companies = PublishSubject.create<List<CompanyModel>>()
    private val custCompany = PublishSubject.create<GetCustCompanyResponseModel>()
    private val companiesError = PublishSubject.create<Int>()

    override fun getCompaniesCert(pin: String, mobile: String, userId: String) {
        juridicalRegisterGetCompaniesCertUseCase.execute(pin, mobile, userId, 2)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    companies.onNext(it.company)
                } else {
                    companiesError.onNext(it.status.statusCode)
                    error.onNext(it.status.statusCode)
                }
            }, {

            }).addTo(subscriptions)
    }

    override fun companies() = companies
    override fun companiesError() = companiesError

    override fun getCustCompany(pin: String, taxNo: String, mobile: String, userId: String) {
        getCustCompanyUseCase.execute(pin, taxNo, mobile, userId, 2)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    custCompany.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            })
            .addTo(subscriptions)
    }

    override fun custCompany() = custCompany
}