package az.turanbank.mlb.presentation.data

import az.turanbank.mlb.data.remote.model.ips.response.AccountResponseModel

class SelectUnregisteredAccountModel (
    val account: AccountResponseModel,
    var checked: Boolean
) {
    override fun toString(): String {
        return "SelectAccountModel(account=$account, checked=$checked)"
    }
}