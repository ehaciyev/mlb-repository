package az.turanbank.mlb.presentation.branch.fragment.map


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.branch.BranchMapResponseModel
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.model.Marker


class CustomMarkerInfoWindowView(context: Context?, branch: BranchMapResponseModel) : InfoWindowAdapter {

    private var markerItemView : View =
        LayoutInflater.from(context).inflate(R.layout.marker_info_window, null)

    override fun getInfoWindow(marker: Marker): View {
        //val branchMarkerResponse: BranchMapResponseModel = marker.tag as BranchMapResponseModel

        val itembranchName: TextView = markerItemView.findViewById(R.id.branchName)
        val itemstreetName: TextView = markerItemView.findViewById(R.id.streetName)
        val itemdistance: TextView = markerItemView.findViewById(R.id.distance)

        //itembranchName.text = marker.title
        //itemstreetName.text = branchMarkerResponse.address
        //itemdistance.text = "150m"
        return markerItemView // 4
    }

    override fun getInfoContents(marker: Marker): View? {
        return null
    }

}