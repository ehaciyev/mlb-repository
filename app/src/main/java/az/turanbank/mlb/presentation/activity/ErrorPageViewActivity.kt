package az.turanbank.mlb.presentation.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.SplashActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.login.unblock.UnblockUserActivity
import kotlinx.android.synthetic.main.activity_error_page_view.*

class ErrorPageViewActivity : BaseActivity() {
    val unblock: Boolean by lazy { intent.getBooleanExtra("unblock", false) }
    private val tokenExpired: Boolean by lazy { intent.getBooleanExtra("tokenExpired", false) }
    val customerType: String by lazy { intent.getStringExtra("customerType") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_error_page_view)
        setListeners()
    }

    @SuppressLint("SetTextI18n")
    private fun setListeners() {
        if (unblock) {
            title_message.text = getString(R.string.warning)
            error_description.text = getString(R.string.you_blocked_in_mlb)
            close.text = getString(R.string.unblock)
            close.setOnClickListener {
                val intent = Intent(this, UnblockUserActivity::class.java)
                intent.putExtra("customerType", customerType)
                startActivity(intent)
                finish()
            }
        } else {
            close.text = getString(R.string.close)
            intent.getStringExtra("description")?.let {
                error_description.text = it
            }

            if (tokenExpired) {
                close.setOnClickListener {
                    startActivity(Intent(this, SplashActivity::class.java))
                    finish()
                }
            } else {
                close.setOnClickListener { finish() }
            }
        }
    }

    override fun onBackPressed() {
        if (tokenExpired) {
            startActivity(Intent(this, SplashActivity::class.java))
            finish()
        } else {
            super.onBackPressed()
        }
    }
}
