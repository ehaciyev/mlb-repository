package az.turanbank.mlb.presentation.ips.pay.creditorchoice.alias

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.request.EnumAliasType
import az.turanbank.mlb.data.remote.model.ips.response.GetAccountAndCustomerInfoByAliasResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.GetAllBanksResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.banks.GetAllBanksUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetAccountAndCustomerInfoByAliasUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

interface IPSCreditorAliasChoiceViewModelInputs : BaseViewModelInputs {
    fun getBankList()
    fun getAccountAndCustomerInfoByAlias(aliasTypeId:EnumAliasType, value:String)
}

interface IPSCreditorAliasChoiceViewModelOutputs : BaseViewModelOutputs {
    fun onBankListSuccess(): PublishSubject<GetAllBanksResponseModel>
    fun onAccountAndCustomerInfoByAlias(): PublishSubject<GetAccountAndCustomerInfoByAliasResponseModel>
    fun showProgress(): PublishSubject<Boolean>
}

class IPSCreditorAliasChoiceViewModel @Inject constructor(
    private val getAllBanksUseCase: GetAllBanksUseCase,
    private val getAccountAndCustomerInfoByAliasUseCase: GetAccountAndCustomerInfoByAliasUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    IPSCreditorAliasChoiceViewModelInputs,
    IPSCreditorAliasChoiceViewModelOutputs {
    override val inputs: IPSCreditorAliasChoiceViewModelInputs = this

    override val outputs: IPSCreditorAliasChoiceViewModelOutputs = this

    private var selectedBankId: Long? = null

    private var isGovernmentPayment: Boolean = false

    private var selectedAliasType: Int = 0

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val bankList = PublishSubject.create<GetAllBanksResponseModel>()

    private val accountAndCustomerInfoByAlias = PublishSubject.create<GetAccountAndCustomerInfoByAliasResponseModel>()

    private val showProgress = PublishSubject.create<Boolean>()

    override fun getBankList() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getAllBanksUseCase
            .execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    bankList.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getAccountAndCustomerInfoByAlias(aliasTypeId:EnumAliasType, value:String) {
        showProgress.onNext(true)
        getAccountAndCustomerInfoByAliasUseCase
            .execute(custId,token,aliasTypeId,value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                when (it.status.statusCode) {
                    1 -> {
                        accountAndCustomerInfoByAlias.onNext(it)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            },{
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onBankListSuccess() = bankList
    override fun onAccountAndCustomerInfoByAlias() = accountAndCustomerInfoByAlias
    override fun showProgress() = showProgress

    fun setSelectedBankId(id: Long) {
        if(id == 210005L || id == 210027L) {
            isGovernmentPayment = true
        }
        this.selectedBankId = id
    }

    fun setSelectedBankCode(bankCode: String?) {
        if(bankCode == "210005") {
            isGovernmentPayment = true
        }
    }

    fun getSelectedBankId() = selectedBankId

    fun isGovernmentPayment() = isGovernmentPayment

    fun getSelectedAliasType() = selectedAliasType

    fun setSelectedAliasType(list: Int){
        when(list){
            0 -> {selectedAliasType = 1}
            1 -> {selectedAliasType = 2}
            2 -> {selectedAliasType = 3}
            3 -> {selectedAliasType = 4}
        }
    }
}