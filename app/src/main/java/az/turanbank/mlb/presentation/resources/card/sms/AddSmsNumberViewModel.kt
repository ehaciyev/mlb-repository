package az.turanbank.mlb.presentation.resources.card.sms

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.resources.card.sms.EnableSmsNotificationForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.sms.EnableSmsNotificationForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AddSmsNumberViewModelInputs : BaseViewModelInputs {
    fun enableSms(mobile: String, repeatMobile: String, cardId: Long)
}

interface AddSmsNumberViewModelOutputs : BaseViewModelOutputs {
    fun onNumberAdded(): PublishSubject<Boolean>
}

class AddSmsNumberViewModel @Inject constructor(
    private val enableSmsNotificationForIndividualCustomerUseCase: EnableSmsNotificationForIndividualCustomerUseCase,
    private val enableSmsNotificationForJuridicalCustomerUseCase: EnableSmsNotificationForJuridicalCustomerUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    AddSmsNumberViewModelInputs,
    AddSmsNumberViewModelOutputs {

    override val inputs: AddSmsNumberViewModelInputs = this

    override val outputs: AddSmsNumberViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val smsNotificationSuccess = PublishSubject.create<Boolean>()

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    override fun enableSms(
        mobile: String,
        repeatMobile: String,
        cardId: Long
    ) {
        if (isCustomerJuridical) {
            enableSmsNotificationForJuridicalCustomerUseCase
                .execute(mobile, repeatMobile, custId, compId, token, cardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        if (it.status.statusCode == 1) {
                            smsNotificationSuccess.onNext(true)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    },
                    {
                        error.onNext(1878)
                    }
                ).addTo(subscriptions)
        } else {
            enableSmsNotificationForIndividualCustomerUseCase
                .execute(mobile, repeatMobile, custId, token, cardId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        smsNotificationSuccess.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onNumberAdded() = smsNotificationSuccess
}