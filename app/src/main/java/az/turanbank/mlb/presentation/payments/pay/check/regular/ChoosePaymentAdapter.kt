package az.turanbank.mlb.presentation.payments.pay.check.regular

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.AvansResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.InvoiceResponseModel
import kotlinx.android.synthetic.main.check_item_list_view.view.*
import kotlinx.android.synthetic.main.check_item_list_view.view.amountInteger
import kotlinx.android.synthetic.main.check_item_list_view.view.amountReminder
import kotlinx.android.synthetic.main.check_item_list_view_custom.view.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class ChoosePaymentAdapter(
    private val context: Context,
    private val invoice: ArrayList<InvoiceResponseModel>,
    private val avans: ArrayList<AvansResponseModel>,
    private val onClickAction: (position: Int) -> Unit,
    var longClickListener: (position:Int) -> Unit
) : RecyclerView.Adapter<ChoosePaymentAdapter.ChoosePaymentViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) =
        ChoosePaymentViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.check_item_list_view_custom,
                parent,
                false
            )
        )

    override fun getItemCount() = avans.size + invoice.size

    override fun onBindViewHolder(
        holder: ChoosePaymentViewHolder,
        position: Int
    ) {
        if (position >= invoice.size){
            holder.bindAvans(avans[position-invoice.size], onClickAction, context)
        } else {
            holder.bindInvoice(invoice[position], onClickAction, context, longClickListener = longClickListener)
        }
    }

    class ChoosePaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindInvoice(invoice: InvoiceResponseModel, onClickAction: (position: Int) -> Unit, context: Context, longClickListener: (position: Int) -> Unit) {
            val decimalFormat = DecimalFormat("0.00")
            decimalFormat.roundingMode = RoundingMode.HALF_EVEN
            val symbols = DecimalFormatSymbols()
            symbols.decimalSeparator = '.'
            decimalFormat.decimalFormatSymbols = symbols

            itemView.setOnClickListener {  onClickAction.invoke(adapterPosition) }
            itemView.amountContainer.setOnClickListener { onClickAction.invoke(adapterPosition) }

            itemView.name.text = invoice.serviceName


           /* if(invoice.serviceName.split("-").size > 1 ){
                itemView.numberText.text = invoice.serviceName.split("-")[0] + " - "
                itemView.description.text = invoice.serviceName.split("-")[1]
            } else {
                itemView.numberText.visibility = View.GONE
                itemView.description.text = invoice.serviceName
            }*/

            itemView.amountInteger.text = decimalFormat.format(invoice.amount).split(".")[0] + ","
            itemView.amountReminder.text = decimalFormat.format(invoice.amount).split(".")[1]

            itemView.setOnLongClickListener {
                longClickListener.invoke(adapterPosition)
                return@setOnLongClickListener true
            }
        }

        fun bindAvans(
            avans: AvansResponseModel,
            onClickAction: (position: Int) -> Unit,
            context: Context
        ) {
            val decimalFormat = DecimalFormat("0.00")
            decimalFormat.roundingMode = RoundingMode.HALF_EVEN
            val symbols = DecimalFormatSymbols()
            symbols.decimalSeparator = '.'
            decimalFormat.decimalFormatSymbols = symbols

            itemView.setOnClickListener {  onClickAction.invoke(adapterPosition) }

            itemView.name.text = avans.serviceName


           /* if(avans.serviceName.split("-").size > 1 ){
                itemView.numberText.text = avans.serviceName.split("-")[0] + " - "
                itemView.description.text = avans.serviceName.split("-")[1]
            } else {
                itemView.numberText.visibility = View.GONE
                itemView.description.text = avans.serviceName
            }
*/
            //itemView.description.text = avans.serviceName

            itemView.amountInteger.text = decimalFormat.format(avans.amount).split(".")[0] + ","
            itemView.amountReminder.text = decimalFormat.format(avans.amount).split(".")[1]

        }
    }
}
