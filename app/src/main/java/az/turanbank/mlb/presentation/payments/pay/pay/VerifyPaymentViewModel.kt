package az.turanbank.mlb.presentation.payments.pay.pay

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.request.ReqPaymentDataList
import az.turanbank.mlb.data.remote.model.pg.response.payments.pay.PayResponseModel
import az.turanbank.mlb.domain.user.usecase.pg.pay.PayIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.pay.PayJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface VerifyPaymentViewModelInputs : BaseViewModelInputs {
    fun pay(
        transactionId: String?,
        categoryId: Long,
        merchantId: Long,
        providerId: Long,
        cardId: Long,
        reqPaymentDataList: ArrayList<ReqPaymentDataList>
    )
}

interface VerifyPaymentViewModelOutputs : BaseViewModelOutputs {
    fun paySuccess(): PublishSubject<PayResponseModel>
}

class VerifyPaymentViewModel @Inject constructor(
    private val payIndividualUseCase: PayIndividualUseCase,
    private val payJuridicalUseCase: PayJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    VerifyPaymentViewModelInputs,
    VerifyPaymentViewModelOutputs {
    override val inputs: VerifyPaymentViewModelInputs = this
    override val outputs: VerifyPaymentViewModelOutputs = this
    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val paid = PublishSubject.create<PayResponseModel>()

    override fun pay(
        transactionId: String?,
        categoryId: Long,
        merchantId: Long,
        providerId: Long,
        cardId: Long,
        reqPaymentDataList: ArrayList<ReqPaymentDataList>
    ) {
        val respond = if (isCustomerJuridical) {
            payJuridicalUseCase.execute(
                custId,
                compId,
                token,
                transactionId,
                categoryId,
                merchantId,
                providerId,
                cardId,
                reqPaymentDataList
            )
        } else {
            payIndividualUseCase.execute(
                custId,
                token,
                transactionId,
                categoryId,
                merchantId,
                providerId,
                cardId,
                reqPaymentDataList
            )
        }
        respond.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    paid.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun paySuccess() = paid

}