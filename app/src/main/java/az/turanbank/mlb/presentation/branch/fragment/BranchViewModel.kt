package az.turanbank.mlb.presentation.branch.fragment

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.branch.BranchMapResponseModel
import az.turanbank.mlb.domain.user.usecase.branch.GetBranchMapListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface BranchMapViewModelInputs : BaseViewModelInputs {
    fun getBranches()
}

interface BranchMapViewModelOutputs : BaseViewModelOutputs {
    fun onBranchesSuccess(): PublishSubject<ArrayList<BranchMapResponseModel>>
}

class BranchViewModel @Inject constructor(
    private val getBranchMapListUseCase: GetBranchMapListUseCase,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel(),
    BranchMapViewModelInputs,
    BranchMapViewModelOutputs {
    override val inputs: BranchMapViewModelInputs = this

    override val outputs: BranchMapViewModelOutputs = this

    val lang = sharedPreferences.getString("lang","ru")
    var enumLangType = EnumLangType.RU

    private val branchList = PublishSubject.create<ArrayList<BranchMapResponseModel>>()
    override fun getBranches() {
        if (lang != null) {
            when (lang) {
                "az" -> {
                    enumLangType = EnumLangType.AZ
                }
                "en" -> {
                    enumLangType = EnumLangType.EN
                }
                "ru" -> {
                    enumLangType = EnumLangType.RU
                }
            }
            getBranchMapListUseCase
                .execute(enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        branchList.onNext(it.respBranchMapList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onBranchesSuccess() = branchList
}