package az.turanbank.mlb.presentation.payments.pay.categories

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.CategoryImageMapper
import az.turanbank.mlb.data.remote.model.pg.response.payments.category.CategoryResponseModel
import kotlinx.android.synthetic.main.category_list_view.view.*

class PaymentCategoriesAdapter(
    context: Context,
    private val categoryNames: List<CategoryResponseModel>,
    private val clickListeners: (position: Int) -> Unit
) : BaseAdapter() {
    val inflater = LayoutInflater.from(context)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View{
        val view = inflater.inflate(R.layout.category_list_view, parent, false)

        view.firstImage.setImageResource(CategoryImageMapper().getCategoryImage(categoryNames[position].categoryName))
        view.firstText.text = categoryNames[position].displayName

        view.setOnClickListener { clickListeners.invoke(position) }

        return view
    }

    override fun getItem(position: Int) = null

    override fun getItemId(position: Int) = 0L

    override fun getCount() = categoryNames.size
}