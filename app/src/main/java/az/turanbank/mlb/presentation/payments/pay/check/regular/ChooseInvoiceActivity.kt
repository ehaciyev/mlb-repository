package az.turanbank.mlb.presentation.payments.pay.check.regular

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.AvansResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.InvoiceResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.payments.pay.check.CheckPaymentActivity
import kotlinx.android.synthetic.main.activity_choose_invoice.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class ChooseInvoiceActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ChooseInvoiceViewModel

    private val isInvoice: Boolean by lazy { intent.getBooleanExtra("isInvoice",false) }
    private val invoice: ArrayList<InvoiceResponseModel> by lazy { intent.getSerializableExtra("invoice") as ArrayList<InvoiceResponseModel> }
    private val avans: ArrayList<AvansResponseModel> by lazy { intent.getSerializableExtra("avans") as ArrayList<AvansResponseModel> }
    private val identificationCode: ArrayList<IdentificationCodeRequestModel> by lazy { intent.getSerializableExtra("identificationCode") as ArrayList<IdentificationCodeRequestModel> }
    private val identificationType: String by lazy { intent.getStringExtra("identificationType") }

    private val providerId: Long by lazy { intent.getLongExtra("providerId", 0L) }
    private val merchantId: Long by lazy { intent.getLongExtra("merchantId", 0L) }
    private val merchantDisplayName: String by lazy { intent.getStringExtra("merchantDisplayName") }
    private val transactionId: String by lazy { intent.getStringExtra("transactionId") }
    private val transactionNumberfromIntent: String by lazy { intent.getStringExtra("transactionNumber") }

    private val merchantName: String by lazy { intent.getStringExtra("merchantName") }
    private val categoryName: String by lazy { intent.getStringExtra("categoryName") }
    private val categoryId: Long by lazy { intent.getLongExtra("categoryId", 0L) }
    private val serviceCodeUser: String by lazy { intent.getStringExtra("serviceCodeUser") }
    private val amountTemplate: String by lazy { intent.getStringExtra("amountTemplate") }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_invoice)

        viewModel = ViewModelProvider(this, factory)[ChooseInvoiceViewModel::class.java]

        toolbar_title.text = merchantDisplayName
        toolbar_back_button.setOnClickListener { onBackPressed() }
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        val adapter =
            ChoosePaymentAdapter(this, invoice, avans, {
                    if (it >= invoice.size) {
                        startActivityForResult(
                            Intent(this, CheckPaymentActivity::class.java)
                                .putExtra("isInvoice", false)
                                .putExtra("merchantDisplayName", merchantDisplayName)
                                .putExtra("avans", avans[it - invoice.size])
                                .putExtra("providerId", providerId)
                                .putExtra("merchantId", merchantId)
                                .putExtra("merchantName", merchantName)
                                .putExtra("categoryId", categoryId)
                                .putExtra("identificationCode", identificationCode)
                                .putExtra("categoryName", categoryName)
                                .putExtra("identificationType", identificationType)
                                .putExtra("amountTemplate", amountTemplate)
                                .putExtra("transactionId", transactionId)
                                .putExtra("transactionNumber",transactionNumberfromIntent)
                                .putExtra("serviceCodeUser", serviceCodeUser),
                            7
                        )

                        /*.putExtra("feeCalculationMethod", avans[it - invoice.size].feeCalculationMethod)
                        .putExtra("feePercent", avans[it - invoice.size].feePercent)
                        .putExtra("amount", avans[it - invoice.size].amount)
                        .putExtra("feeMinAmount", avans[it - invoice.size].feeMinAmount)
                        .putExtra("minAllowed", avans[it - invoice.size].minAllowed)
                        .putExtra("maxAllowed", avans[it - invoice.size].maxAllowed)
                        .putExtra("serviceCode", avans[it - invoice.size].serviceCode)

                        .putExtra("serviceName", avans[it - invoice.size].serviceName)
                        .putExtra("subscriberName", avans[it - invoice.size].fullName)
                        .putExtra("feeMaxAmount", avans[it - invoice.size].feeMaxAmount)*/

                    } else {
                        startActivityForResult(
                            Intent(this, CheckPaymentActivity::class.java)
                                .putExtra("isInvoice", true)
                                .putExtra("merchantDisplayName", merchantDisplayName)
                                .putExtra("providerId", providerId)
                                .putExtra("invoice", invoice[it])
                                .putExtra("merchantId", merchantId)
                                .putExtra("merchantName", merchantName)
                                .putExtra("categoryId", categoryId)
                                .putExtra("categoryName", categoryName)
                                .putExtra("identificationCode", identificationCode)
                                .putExtra("identificationType", identificationType)
                                .putExtra("amountTemplate", amountTemplate)
                                .putExtra("transactionNumber", transactionNumberfromIntent)
                                .putExtra("transactionId", transactionId)
                                .putExtra("serviceCodeUser", serviceCodeUser),
                            7
                        )

                            /*.putExtra("feeCalculationMethod", invoice[it].feeCalculationMethod)
                            .putExtra("feePercent", invoice[it].feePercent)
                            .putExtra("feeMinAmount", invoice[it].feeMinAmount)
                            .putExtra("amount", invoice[it].amount)
                            .putExtra("serviceName", invoice[it].serviceName)
                            .putExtra("partialPayment", invoice[it].partialPayment)
                            .putExtra("minAllowed", invoice[it].minAllowed)
                            .putExtra("serviceCode", invoice[it].serviceCode)

                            .putExtra("serviceName", invoice[it].serviceName)
                            .putExtra("subscriberName", invoice[it].fullName)
                            .putExtra("maxAllowed", invoice[it].maxAllowed)
                            .putExtra("feeMaxAmount", invoice[it].feeMaxAmount)*/
                    }
                },
                {

                }
            )
        recyclerView.adapter = adapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}
