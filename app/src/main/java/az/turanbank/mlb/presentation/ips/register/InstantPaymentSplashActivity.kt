package az.turanbank.mlb.presentation.ips.register

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.AGREED
import az.turanbank.mlb.presentation.activity.WarningPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.InstantPaymentActivity
import az.turanbank.mlb.presentation.ips.aliases.create.CreateNewAliasActivity
import az.turanbank.mlb.presentation.ips.nonregistered.NonRegisteredPayment
import az.turanbank.mlb.presentation.ips.register.account.InstantPaymentAccountMultiSelectActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_instant_payment_splash.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class InstantPaymentSplashActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var dialog: MlbProgressDialog

    lateinit var viewModel: InstantPaymentSplashViewModel
    @SuppressLint("SetTextI18n")

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    var isRegistered = false

    var isJuridical = false

    private var firstTimeRegistered = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instant_payment_splash)

        viewModel = ViewModelProvider(this, factory)[InstantPaymentSplashViewModel::class.java]

        toolbar_title.text = getString(R.string.instant_payments)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        dialog = MlbProgressDialog(this)

        isJuridical = sharedPreferences.getBoolean("isCustomerJuridical", false)

        mainContainer.visibility = View.GONE
        //animateProgressImage()
        setOutputListeners()
        setInputListeners()

    }

    private fun setInputListeners() {

        viewModel.inputs.userRegisterExist()

        if(isJuridical){
            mainContainer.visibility = View.GONE
            viewModel.inputs.checkJuridicalUserRegistered()
        } else {
            register.setOnClickListener {
                isRegistered = true
                if(sharedPreferences.getBoolean("userAgreement",false)){
                    firstTimeRegistered = true
                    //animateProgressImage()
                    viewModel.inputs.checkUserRegistered()
                    //viewModel.inputs.ipsIndividualRegister()
                    //chooseAccountsToRegister()
                } else {
                    startActivityForResult(Intent(this, InstantPaymentRegisterRulesActivity::class.java), 1)
                }

            }

            nonregistered.setOnClickListener{
                isRegistered = false

                if(sharedPreferences.getBoolean("userAgreement",false)){
                    startActivity(Intent(this, NonRegisteredPayment::class.java))
                } else {
                    startActivityForResult(Intent(this, InstantPaymentRegisterRulesActivity::class.java), 1)
                }

            }
        }
    }

    private fun setOutputListeners() {

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.customerHasAll().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            startActivityForResult(Intent(this, InstantPaymentActivity::class.java),7)
        }.addTo(subscriptions)
        viewModel.outputs.accountsRegistered().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE

            val intent = if (firstTimeRegistered){
                Intent(this, InstantPaymentActivity::class.java)
            } else {
                Intent(this, CreateNewAliasActivity::class.java)
                    .putExtra("fromRegister", true)
            }
            startActivityForResult(intent,7)
        }.addTo(subscriptions)
        viewModel.outputs.customerIsJuridicalAndNotRegistered() .subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            val intent = Intent(this, WarningPageViewActivity::class.java)
            intent.putExtra("description", getString(R.string.juridical_accounts_registering_in_bank))
            startActivityForResult(intent,7)
        }.addTo(subscriptions)
        viewModel.outputs.customerIsIndividualButNoAccount().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            chooseAccountsToRegister()
        }.addTo(subscriptions)
        viewModel.outputs.userRegistered().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            chooseAccountsToRegister()
        }.addTo(subscriptions)

        viewModel.outputs.userNotRegistered().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            //viewGroupContainer.visibility = View.VISIBLE
            viewModel.inputs.ipsIndividualRegister()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            if(it == 123456){
                mainContainer.visibility = View.VISIBLE
            } else {
                AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
            }
        }.addTo(subscriptions)

        viewModel.outputs.userExist().subscribe{
            progress.clearAnimation()
            progress.visibility = View.GONE
            //chooseAccountsToRegister()
        }

        viewModel.outputs.userRegisteredDefault().subscribe{
            progress.clearAnimation()
            progress.visibility = View.GONE
            goToMainMenu()
        }.addTo(subscriptions)

    }

    private fun goToMainMenu() {
        val intent = Intent(this, InstantPaymentActivity::class.java)
        intent.putExtra("againRegister", false)
        startActivityForResult(intent,9)
    }


    private fun chooseAccountsToRegister() {
        val intent = Intent(this, InstantPaymentAccountMultiSelectActivity::class.java)
        intent.putExtra("againRegister", false)
        startActivityForResult(intent,2)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1){
            if(resultCode == AGREED){
                if(isRegistered){
                    firstTimeRegistered = true
                    animateProgressImage()

                    viewModel.inputs.checkUserRegistered()
                    //viewModel.inputs.ipsIndividualRegister()
                } else {
                    startActivity(Intent(this, NonRegisteredPayment::class.java))
                    finish()
                }
            }
        }
        if (requestCode == 2){
            if(resultCode == Activity.RESULT_OK){
                if(isRegistered){
                    @Suppress("UNCHECKED_CAST") val list = data?.getParcelableArrayListExtra<Parcelable>("accountsToRegisterIpsSystem") as ArrayList<Long>
                    viewModel.inputs.registerAccounts(list)
                    animateProgressImage()
                } else {

                }

            } else if(resultCode == Activity.RESULT_CANCELED){
                AlertDialogMapper(this, 124007).showAlertDialogWithCloseOption()
                finish()
            }
        }

        if (requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }

        if (requestCode == 9){
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        if (requestCode == 8){
            if(resultCode == Activity.RESULT_CANCELED){
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        }
    }
    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        viewGroupContainer.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
