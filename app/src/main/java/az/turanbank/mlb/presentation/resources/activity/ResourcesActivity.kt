package az.turanbank.mlb.presentation.resources.activity

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.ConnectivityLiveData
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_mlb_home.*
import kotlinx.android.synthetic.main.activity_resources.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class ResourcesActivity : BaseActivity() {

    lateinit var viewmodel: ResourcesViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private val getConnectivity: ConnectivityLiveData by lazy {
        ConnectivityLiveData(connectivityManager = baseContext.getSystemService(
            Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        )
    }

    private var isOnlineCount = 0

    private val tabPosition: Int by lazy { intent.getIntExtra("tabPosition", 0) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewmodel = ViewModelProvider(this, factory)[ResourcesViewModel::class.java]

        setContentView(R.layout.activity_resources)
        val sectionsPagerAdapter =
            ResourcesPagerAdapter(
                this,
                supportFragmentManager
            )
        view_pager.adapter = sectionsPagerAdapter
        view_pager.offscreenPageLimit = 4
        tabs.setupWithViewPager(view_pager)
        val tab = tabs.getTabAt(tabPosition)
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
               changeIconForTabPosition(tab!!.position)
            }

        })
        changeIconForTabPosition(tabPosition)
        tabs.selectTab(tab)
        toolbar_title.text = getString(R.string.my_resources)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        getConnectivity.observe(this, androidx.lifecycle.Observer {
            if(!it){
                isOnlineCount = 1
                Snackbar.make(resourceContainer, getString(R.string.offline), Snackbar.LENGTH_LONG).show()
            } else {
                isOnlineCount *= 2
                if(isOnlineCount == 2){
                    Snackbar.make(resourceContainer, getString(R.string.online), Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun changeIconForTabPosition(position: Int) {
        when (position){
            0 -> centerIcon.setImageResource(R.drawable.ic_credit_card)
            1 -> centerIcon.setImageResource(R.drawable.ic_account_resources)
            2 -> centerIcon.setImageResource(R.drawable.ic_percent_resources)
            3 -> centerIcon.setImageResource(R.drawable.ic_deposit_resources)
        }
    }
}