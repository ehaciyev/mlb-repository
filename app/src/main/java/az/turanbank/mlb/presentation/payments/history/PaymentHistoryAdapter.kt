package az.turanbank.mlb.presentation.payments.history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.response.payments.list.PaymentResponseModel
import kotlinx.android.synthetic.main.operation_history_list_item.view.*

class PaymentHistoryAdapter(
    private val payments: ArrayList<PaymentResponseModel>,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<PaymentHistoryAdapter.PaymentViewHolder>() {

    class PaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(payment: PaymentResponseModel, onClickListener: (position: Int) -> Unit) {
            itemView.opHistoryState.text = payment.merchantName
            itemView.opHistoryAmount.text = payment.amount.toString() + " " + payment.currency
            itemView.opHistoryPurpose.text = payment.billingDate
            itemView.opHistoryName.text = payment.cardNumber?.replaceRange(4,12, " **** **** ")

            itemView.setOnClickListener { onClickListener.invoke(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PaymentViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.operation_history_list_item,
            parent,
            false
        )
    )

    override fun getItemCount() = payments.size

    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        holder.bind(payments[position], onClickListener)
    }
}