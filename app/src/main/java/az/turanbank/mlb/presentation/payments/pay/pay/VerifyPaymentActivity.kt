package az.turanbank.mlb.presentation.payments.pay.pay

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.CurrencySignMapper
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.request.ReqPaymentDataList
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.AvansResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.ChildInvoiceResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.InvoiceResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.*
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_verify_payment.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import javax.inject.Inject

class VerifyPaymentActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    @Inject
    lateinit var viewModel: VerifyPaymentViewModel

    private val decimalFormat = DecimalFormat("0.00")
    private val symbols = DecimalFormatSymbols()

    private val currencyText: String by lazy { intent.getStringExtra("currency") }
    private val providerId: Long by lazy { intent.getLongExtra("providerId", 0L) }
    private val respChildInvoice: ArrayList<ChildInvoiceResponseModel> by lazy { intent.getSerializableExtra("respChildInvoice") as ArrayList<ChildInvoiceResponseModel>}
    private val plasticCard: String by lazy { intent.getStringExtra("plasticCard") }
    private val identificationCode: ArrayList<IdentificationCodeRequestModel> by lazy { intent.getSerializableExtra("identificationCode") as ArrayList<IdentificationCodeRequestModel> }
    private val identificationType: String by lazy { intent.getStringExtra("identificationType") }
    private val amountText: Double by lazy { intent.getDoubleExtra("amount", 0.0) }
    private val serviceText: String by lazy { intent.getStringExtra("service") }
    private val codePayer: Long by lazy { intent.getLongExtra("codePayer",0L) }
    private val code: String by lazy { intent.getStringExtra("code") }
    private val merchantId: Long by lazy { intent.getLongExtra("merchantId", 0L) }
    private val cardId: Long by lazy { intent.getLongExtra("cardId", 0L) }
    private val merchantName: String by lazy { intent.getStringExtra("merchantName") }
    private val merchantDisplayName: String by lazy { intent.getStringExtra("merchantDisplayName") }
    private val isInvoice: Boolean by lazy { intent.getBooleanExtra("isInvoice",false) }
    private val avans: AvansResponseModel by lazy { intent.getSerializableExtra("avans") as AvansResponseModel }
    private val invoice: InvoiceResponseModel by lazy { intent.getSerializableExtra("invoice") as InvoiceResponseModel }
    private val serviceCodeUser: String by lazy { intent.getStringExtra("serviceCodeUser") }
    private val pCode: String by lazy { intent.getStringExtra("payerCode") }
    private val invoiceCode: String by lazy { intent.getStringExtra("invoiceCode") }
    private val categoryId: Long by lazy { intent.getLongExtra("categoryId", 0)}
    private val serviceName: String by lazy { intent.getStringExtra("serviceName") }
    private val transactionId: String by lazy { intent.getStringExtra("transactionId") }
    private val transactionNumber: String by lazy { intent.getStringExtra("transactionNumber") }

    private val paymentReceiver: Long by lazy { intent.getLongExtra("paymentReceiver",0L) }
    private val paymentReceiverDescription: String by lazy { intent.getStringExtra("paymentReceiverDescription") }

    private var transactionNum: String = ""
    private var feeCalculationMethod = ""
    private var feePercent = 0.0
    private var feeMinAmount = 0.0
    private var feeMaxAmount = 1000000.0
    private var invoiceNumber: String? = null
    private var transactionIdSend: String? = null



    /*private val feeCalculationMethod: String by lazy { intent.getStringExtra("feeCalculationMethod") }
    private val feePercent: Double by lazy { intent.getDoubleExtra("feePercent",0.0 )}
    private val feeMinAmount: Double by lazy { intent.getDoubleExtra("feeMinAmount",0.0)}
    private val feeMaxAmount: Double by lazy { intent.getDoubleExtra("feeMaxAmount", 0.0)}*/
    //private val transactionNum: String by lazy { intent.getStringExtra("transactionNum") }

    private val serviceCode: Long by lazy { intent.getLongExtra("serviceCode",0L)}
    private val subscriberName: String by lazy { intent.getStringExtra("subscriberName") }
    private val reqPaymentDataList: ArrayList<ReqPaymentDataList> = arrayListOf()
    private var serviceCodeFromInvoiceOrAvans = 0L
    private var paymentReceiverLong :Long? = 0L
    private var payerCod: Long? = null


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_payment)

        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = ','
        decimalFormat.decimalFormatSymbols = symbols

        payerCod = if(codePayer == 0L){
            null
        } else {
            codePayer
        }

        paymentReceiverLong = if(paymentReceiver == 0L){
            null
        } else {
            paymentReceiver
        }


         if(isInvoice){
             feeCalculationMethod = invoice.feeCalculationMethod
             transactionNum = invoice.transactionNumber
             feePercent = invoice.feePercent
             feeMaxAmount = invoice.feeMaxAmount
             feeMinAmount = invoice.feeMinAmount
             service.text = invoice.fullName
             serviceCodeFromInvoiceOrAvans = invoice.serviceCode
             invoiceNumber = invoice.invoiceNumber

        } else {
             feeCalculationMethod = avans.feeCalculationMethod
             transactionNum = avans.transactionNumber
             feePercent = avans.feePercent
             feeMaxAmount = avans.feeMaxAmount
             feeMinAmount = avans.feeMinAmount
             service.text = avans.fullName
             serviceCodeFromInvoiceOrAvans = avans.serviceCode
        }

        serviceCodeText.text = serviceCodeUser

        val curr = CurrencySignMapper().getCurrencySign(currencyText)

        //bigViewAmount.text = curr + " " + decimalFormat.format(amountText)
        bigAmountInteger.text = firstPartOfMoney(amountText)
        bigAmountReminder.text = secondPartOfMoney(amountText)
        cardNumber.text = plasticCard
        //service.text = serviceText
        payerCode.text = invoiceCode
        amount.text = decimalFormat.format(amountText) +" "+currencyText

        var feeAmount = 0.0

        transactionIdSend = if(transactionId == "null" || transactionId.isNullOrEmpty()){
            null
        } else {
            transactionId
        }
        if(respChildInvoice.isNotEmpty()){
            respChildInvoice.forEach {
                val feeAmountFunc = feeAmountCalc(it.feeCalculationMethod,it.amount,it.feePercent,it.feeMinAmount,it.feeMaxAmount)
                reqPaymentDataList.add(ReqPaymentDataList(
                    it.invoiceNumber, it.paymentReceiver, it.paymentReceiverDescription, subscriberName, it.serviceName, it.serviceCode.toString(),
                    it.amount, identificationCode, identificationType, it.transactionNumber, it.feeCalculationMethod, feeAmountFunc, transactionNum))
                feeAmount += feeAmountFunc
            }
        } else {
            if(feeCalculationMethod == "WITHOUT_FEE"){
                feeAmount = 0.0
            } else {
                feeAmount = (roundedAmount(amountText) * feePercent) /100
                if(feeAmount <= feeMinAmount){
                    feeAmount = feeMinAmount
                } else if(feeAmount >= feeMaxAmount){
                    feeAmount = feeMaxAmount
                }
            }
            reqPaymentDataList.add(ReqPaymentDataList(
                invoiceNumber, paymentReceiver, paymentReceiverDescription,  subscriberName, serviceName, serviceCodeFromInvoiceOrAvans.toString(),
                roundedAmount(amountText) ,identificationCode,identificationType,transactionNumber,feeCalculationMethod,feeAmount,transactionNumber))
        }

        val amount = decimalFormat(feeAmount,DecimalFormat("0.00"),',')
        amountFee.text = "${splitString(amount,',')} AZN"


        setOutputListeners()
        setInputListeners()

    }

    private fun roundedAmount(amountText: Double): Double {
        val symbols = DecimalFormatSymbols()
        val decimalFormat = DecimalFormat("0.00")
        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = '.'
        decimalFormat.decimalFormatSymbols = symbols
        return decimalFormat.format(amountText).toDouble()
    }

    private fun setInputListeners() {
        submitButton.setOnClickListener {
            showProgressBar(true)
            viewModel.inputs.pay(transactionIdSend, categoryId, merchantId, providerId, cardId, reqPaymentDataList )
        }
    }

    private fun setOutputListeners() {
        toolbar_title.text = merchantDisplayName
        toolbar_back_button.setOnClickListener { onBackPressed() }

        viewModel.outputs.paySuccess().subscribe {
            showProgressBar(false)
            setResult(Activity.RESULT_OK, Intent().putExtra("payment", it).putExtra("currency",currencyText))
            finish()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.VISIBLE
            animateSubmitImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateSubmitImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}
