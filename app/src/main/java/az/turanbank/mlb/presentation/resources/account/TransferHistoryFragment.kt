package az.turanbank.mlb.presentation.resources.account

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.widget.ActionMenuView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import com.facebook.shimmer.ShimmerFrameLayout
import io.reactivex.rxkotlin.addTo
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class TransferHistoryFragment : BaseFragment() {

    lateinit var viewmodel: TransferHistoryViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory
    lateinit var adapter: OperationHistoryRecyclerViewAdapter
    private lateinit var operationHistoryRecyclerView: RecyclerView
    private lateinit var opHistoryProgressImage: ImageView
    private lateinit var failMessage: TextView
    private lateinit var amvMenu: ActionMenuView
    private lateinit var menuBuilder: MenuBuilder

    lateinit var cardStatementShimmerContainer: ShimmerFrameLayout

    private var startDateCalendar: Calendar = Calendar.getInstance()
    private var endDateCalendar: Calendar = Calendar.getInstance()

    @SuppressLint("RestrictedApi")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_transfer_history, container, false)
        setHasOptionsMenu(true)

        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbar2)

        amvMenu = toolbar.findViewById(R.id.amvMenu)
        menuBuilder = amvMenu.menu as MenuBuilder

        cardStatementShimmerContainer = view.findViewById(R.id.cardStatementShimmerContainer)
        cardStatementShimmerContainer.startShimmerAnimation()

        menuBuilder.setCallback(object : MenuBuilder.Callback {
            override fun onMenuItemSelected(menu: MenuBuilder?, item: MenuItem?): Boolean {
                return onOptionsItemSelected(item!!)
            }

            override fun onMenuModeChange(menu: MenuBuilder?) {
            }
        })

        opHistoryProgressImage = view.findViewById(R.id.opHistoryProgressImage)
        failMessage = view.findViewById(R.id.failMessage)

        operationHistoryRecyclerView = view.findViewById(R.id.operationHistoryRecyclerView)
        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL
        operationHistoryRecyclerView.layoutManager = llm

        adapter = OperationHistoryRecyclerViewAdapter(arrayListOf()) { position ->
            val operation = adapter.getItemAt(position)

            val intent = Intent(requireActivity(), SingleOperationDetailsActivity::class.java)
            val currency: String = if (operation.operTypeId == 5 || operation.operTypeId == 6) {
                operation.crCcy
            } else {
                operation.currency
            }
            val amount: String = if (operation.operTypeId == 5 || operation.operTypeId == 6) {
                operation.crAmt.toString()
            } else {
                operation.amount.toString()
            }
            intent.putExtra("operId", operation.operId)
            intent.putExtra("operName", operation.operName)
            intent.putExtra("dtIban", operation.dtIban)
            intent.putExtra("crName", operation.crName)
            intent.putExtra("crIban", operation.crIban)
            intent.putExtra("amount", amount)
            intent.putExtra("operPurpose", operation.operPurpose)
            intent.putExtra("operPurpose", operation.operPurpose)
            intent.putExtra("currency", currency)
            intent.putExtra("createdDate", operation.createdDate)
            intent.putExtra("operStateId", operation.operStateId)
            intent.putExtra("operTypeId", operation.operTypeId)
            intent.putExtra("operState", operation.operState)

            startActivityForResult(intent, 5)
        }
        operationHistoryRecyclerView.adapter = adapter

        viewmodel = ViewModelProvider(this, factory)[TransferHistoryViewModel::class.java]
        //animateProgressImage()

        setOutputListeners()
        setInputListeners()

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                adapter.setItems(arrayListOf())
                viewmodel.inputs.getAllOperationList()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (amvMenu.menu.size() == 0)
            inflater.inflate(R.menu.transfer_history_menu, amvMenu.menu)
        amvMenu.menu.findItem(R.id.transfer_history_filter).isVisible = true
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun simpleDateFormat(): SimpleDateFormat {
        val myFormat = "dd-MM-yyyy"
        return SimpleDateFormat(myFormat, Locale.US)
    }

    @SuppressLint("InflateParams")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.transfer_history_filter) {

            val dialogLayout = layoutInflater.inflate(R.layout.transfer_history_search_layout, null)

            val startDateContainer =
                dialogLayout.findViewById<LinearLayout>(R.id.transferHistoryStartDateCalendar)
            val endDateContainer =
                dialogLayout.findViewById<LinearLayout>(R.id.transferHistoryEndDateCalendar)
            val transferHistoryAccountIdContainer =
                dialogLayout.findViewById<LinearLayout>(R.id.transferHistoryAccountIdContainer)
            val transferHistoryOperStatus =
                dialogLayout.findViewById<EditText>(R.id.transferHistoryOperStatus)
            val transferHistoryOperStatusContainer =
                dialogLayout.findViewById<LinearLayout>(R.id.transferHistoryOperStatusContainer)

            val startDate =
                dialogLayout.findViewById<EditText>(R.id.transferHistoryStartDateEditText)
            val endDate = dialogLayout.findViewById<EditText>(R.id.transferHistoryEndDateEditText)
            val fromAmountEditText = dialogLayout.findViewById<EditText>(R.id.fromAmount)
            val toAmountEditText = dialogLayout.findViewById<EditText>(R.id.toAmount)
            val transferHistoryAccountId =
                dialogLayout.findViewById<EditText>(R.id.transferHistoryAccountId)
            val transferHistoryAccountIdProgress =
                dialogLayout.findViewById<ImageView>(R.id.transferHistoryAccountIdProgress)

            val transferHistoryOperStatusProgress =
                dialogLayout.findViewById<ImageView>(R.id.transferHistoryOperStatusProgress)

            var selectedOperStateId: Long? = null

            startDateContainer.setOnClickListener {

                val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    startDateCalendar.set(Calendar.YEAR, year)
                    startDateCalendar.set(Calendar.MONTH, monthOfYear)
                    startDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    startDate.setText(
                        simpleDateFormat().format(
                            startDateCalendar.time
                        ).replace("-", ".")
                    )
                }

                DatePickerDialog(
                    requireContext(), R.style.DateDialogTheme, date, startDateCalendar
                        .get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH),
                    startDateCalendar.get(Calendar.DAY_OF_MONTH)
                ).show()
            }

            endDateContainer.setOnClickListener {

                val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    endDateCalendar.set(Calendar.YEAR, year)
                    endDateCalendar.set(Calendar.MONTH, monthOfYear)
                    endDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    endDate.setText(
                        simpleDateFormat().format(
                            endDateCalendar.time
                        ).replace("-", ".")
                    )
                }

                DatePickerDialog(
                    requireContext(), R.style.DateDialogTheme, date, endDateCalendar
                        .get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH),
                    endDateCalendar.get(Calendar.DAY_OF_MONTH)
                ).show()
            }

            AlertDialog.Builder(requireContext())
                .setTitle(getString(R.string.advanced_search))
                .setView(dialogLayout)
                .setPositiveButton(getString(R.string.proceed_all_caps)) { _, _ ->
                    val minAmt = fromAmountEditText.text.toString().trim()
                    val maxAmt = toAmountEditText.text.toString().trim()
                    adapter.setItems(arrayListOf())
                    val account: String? =
                        if (transferHistoryAccountId.text.toString() == getString(R.string.all_accounts)) {
                            null
                        } else {
                            transferHistoryAccountId.text.toString()
                        }
                    viewmodel.inputs.operationAdvancedSearch(
                        account,
                        getAmount(minAmt),
                        getAmount(maxAmt),
                        getDate(startDate.text.toString()),
                        getDate(endDate.text.toString()),
                        selectedOperStateId?.toInt()
                    )
                }
                .setNegativeButton(getString(R.string.cancel_all_caps)) { _, _ ->

                }
                .setCancelable(true)
                .show()

            viewmodel.outputs.getOperationStateProgress().subscribe {
                showProgressImages(transferHistoryOperStatusProgress, it)
            }.addTo(subscriptions)

            viewmodel.outputs.getOperationStateSuccess().subscribe { operationStateList ->
                val operStatusNameArray = arrayOfNulls<String>(operationStateList.size)
                val operStatusIdArray = arrayOfNulls<Long>(operationStateList.size)

                selectedOperStateId = null
                //operStatusNameArray[0] = getString(R.string.all_statuses)
                //transferHistoryOperStatus.setText(operStatusNameArray[0])

                operStatusIdArray[0] = null
                for (i in 0 until operationStateList.size) {
                    operStatusNameArray[i] = operationStateList[i].stateValue
                    operStatusIdArray[i] = operationStateList[i].stateId
                }

                transferHistoryOperStatusContainer.setOnClickListener {

                    val b: AlertDialog.Builder = AlertDialog.Builder(requireContext())
                    b.setTitle(getString(R.string.select_status))
                    b.setItems(operStatusNameArray) { _, which ->
                        transferHistoryOperStatus.setText(operStatusNameArray[which])
                        selectedOperStateId = if (which == 0) {
                            null
                        } else {
                            operStatusIdArray[which]!!
                        }
                    }
                    val dialog = b.create()
                    dialog.show()
                }

                transferHistoryOperStatus.setOnClickListener {
                    val b: AlertDialog.Builder = AlertDialog.Builder(requireContext())

                    b.setTitle(getString(R.string.select_status))

                    b.setItems(operStatusNameArray) { _, which ->
                        transferHistoryOperStatus.setText(operStatusNameArray[which])
                        selectedOperStateId = if (which == 0) {
                            null
                        } else {
                            operStatusIdArray[which]!!
                        }
                    }
                    val dialog = b.create()
                    dialog.show()
                }

            }.addTo(subscriptions)

            viewmodel.outputs.showAccountProgress().subscribe {
                showProgressImages(transferHistoryAccountIdProgress, it)
            }.addTo(subscriptions)

            viewmodel.outputs.onAccountSuccess().subscribe { accountListModel ->

                failMessage.visibility = View.GONE
                val accountNameArray = arrayOfNulls<String>(accountListModel.size)
                //accountNameArray[0] = getString(R.string.all_accounts)
                //transferHistoryAccountId.setText(accountNameArray[0])
                transferHistoryAccountIdContainer.setOnClickListener {

                    val b: AlertDialog.Builder =
                        AlertDialog.Builder(requireContext())
                    b.setTitle(getString(R.string.select_account))

                    for (i in 0 until accountListModel.size) {
                        accountNameArray[i] = (accountListModel[i].iban)
                    }

                    b.setItems(accountNameArray) { _, which ->
                        transferHistoryAccountId.setText(accountNameArray[which])
                    }
                    val dialog = b.create()
                    dialog.show()
                }
            }.addTo(subscriptions)

            viewmodel.inputs.getAccountList()
           // viewmodel.inputs.getAllOperationList()
            viewmodel.inputs.getOperationStateList()

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getDate(value: String?): String? {
        return if (value.isNullOrEmpty()) null
        else value
    }

    private fun revertProgressImageAnimation(progressImage: ImageView) {
        progressImage.clearAnimation()
    }

    override fun onDetach() {
        super.onDetach()
        amvMenu.menu.findItem(R.id.transfer_history_filter).isVisible = false
    }

    private fun getAmount(value: String?): Double? {
        return if (value.isNullOrEmpty()) null
        else value.toDouble()
    }

    private fun animateProgressImage(progressImage: ImageView) {
        failMessage.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun showProgressImages(progressImage: ImageView, show: Boolean) {
        hideProgressImages(progressImage, show)

        if (show) {
            //progressImage.visibility = View.VISIBLE
            //animateProgressImage(progressImage)
            cardStatementShimmerContainer.startShimmerAnimation()
            cardStatementShimmerContainer.visibility = View.VISIBLE
        } else {
            //progressImage.visibility = View.GONE
            //revertProgressImageAnimation(progressImage)
            cardStatementShimmerContainer.stopShimmerAnimation()
            cardStatementShimmerContainer.visibility = View.GONE
        }
    }

    private fun hideProgressImages(progressImage: ImageView, hide: Boolean) {
        if (hide) {
            progressImage.visibility = View.GONE
        } else {
            progressImage.visibility = View.VISIBLE
        }
    }

    private fun setOutputListeners() {

        viewmodel.outputs.onError().subscribe {
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
            failMessage.visibility = View.VISIBLE
            opHistoryProgressImage.clearAnimation()
            opHistoryProgressImage.visibility = View.GONE
            operationHistoryRecyclerView.visibility = View.GONE
            cardStatementShimmerContainer.stopShimmerAnimation()
            cardStatementShimmerContainer.visibility = View.GONE
        }.addTo(subscriptions)

        viewmodel.outputs.showProgress().subscribe {
            if (it) {
                //animateProgressImage()
                cardStatementShimmerContainer.startShimmerAnimation()
                cardStatementShimmerContainer.visibility = View.VISIBLE
            } else {
                cardStatementShimmerContainer.stopShimmerAnimation()
                cardStatementShimmerContainer.visibility = View.GONE
                //revertAnimation()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.getAllOperationListSuccess().subscribe { operationListModel ->
            menuBuilder.setCallback(object : MenuBuilder.Callback {
                override fun onMenuItemSelected(menu: MenuBuilder?, item: MenuItem?): Boolean {
                    return onOptionsItemSelected(item!!)
                }

                override fun onMenuModeChange(menu: MenuBuilder?) {
                }
            })
            operationHistoryRecyclerView.visibility = View.VISIBLE
            adapter.setItems(operationListModel.operationList)

        }.addTo(subscriptions)
    }

    private fun revertAnimation() {
        opHistoryProgressImage.clearAnimation()
        opHistoryProgressImage.visibility = View.GONE
    }

    private fun animateProgressImage() {
        opHistoryProgressImage.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        opHistoryProgressImage.startAnimation(rotate)
    }

    private fun setInputListeners() {
        viewmodel.inputs.getAllOperationList()
    }
}
