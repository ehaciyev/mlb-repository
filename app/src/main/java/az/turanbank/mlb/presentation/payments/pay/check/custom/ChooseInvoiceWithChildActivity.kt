package az.turanbank.mlb.presentation.payments.pay.check.custom

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.AvansResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.ChildInvoiceResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.InvoiceResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_choose_invoice.recyclerView
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class ChooseInvoiceWithChildActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ChooseInvoiceWithChildActivityViewModel

    private val invoice: ArrayList<InvoiceResponseModel> by lazy { intent.getSerializableExtra("invoice") as ArrayList<InvoiceResponseModel> }
    private val respChildInvoice: ArrayList<ArrayList<ChildInvoiceResponseModel>> by lazy { intent.getSerializableExtra("respChildInvoice") as ArrayList<ArrayList<ChildInvoiceResponseModel>> }

    private val transactionId: String by lazy { intent.getStringExtra("transactionId") }
    private val transactionNumberfromIntent: String by lazy { intent.getStringExtra("transactionNumber") }

    private val identificationCode: ArrayList<IdentificationCodeRequestModel> by lazy { intent.getSerializableExtra("identificationCode") as ArrayList<IdentificationCodeRequestModel> }
    private val merchantId: Long by lazy { intent.getLongExtra("merchantId", 0L) }
    private val providerId: Long by lazy { intent.getLongExtra("providerId", 0L) }
    private val identificationType: String by lazy { intent.getStringExtra("identificationType") }
    private val categoryName: String by lazy { intent.getStringExtra("categoryName") }

    private val merchantName: String by lazy { intent.getStringExtra("merchantName") }
    private val merchantDisplayName: String by lazy { intent.getStringExtra("merchantDisplayName") }
    private val categoryId: Long by lazy { intent.getLongExtra("categoryId", 0L) }
    private val serviceCodeUser: String by lazy { intent.getStringExtra("serviceCodeUser") }


    lateinit var adapterInvoice: ChooseInvoiceWithChildAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_custom_invoice)

        viewModel = ViewModelProvider(this, factory)[ChooseInvoiceWithChildActivityViewModel::class.java]

        toolbar_title.text = merchantDisplayName
        toolbar_back_button.setOnClickListener { onBackPressed() }

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        adapterInvoice = ChooseInvoiceWithChildAdapter(this, invoice) {
            startActivityForResult(
                Intent(this, ChooseInvoiceWithSubChildActivity::class.java)
                    .putExtra("merchantDisplayName", merchantDisplayName)
                    .putExtra("providerId", providerId)
                    .putExtra("respChildInvoice", respChildInvoice[it])
                    .putExtra("invoice", invoice[it])
                    .putExtra("merchantId", merchantId)
                    .putExtra("merchantName", merchantName)
                    .putExtra("categoryId", categoryId)
                    .putExtra("categoryName", categoryName)
                    .putExtra("identificationCode", identificationCode)
                    .putExtra("identificationType", identificationType)
                    .putExtra("transactionId",transactionId)
                    .putExtra("transactionNumber",transactionNumberfromIntent)
                    .putExtra("serviceCodeUser", serviceCodeUser),
                7
            )
        }
        recyclerView.adapter = adapterInvoice
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}
