package az.turanbank.mlb.presentation.branch.fragment.map

data class BranchMarkerResponse (
    val branchName: String,
    val street: String,
    val distance: String
)
