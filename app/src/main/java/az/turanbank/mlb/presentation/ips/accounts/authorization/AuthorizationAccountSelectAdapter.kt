package az.turanbank.mlb.presentation.ips.accounts.authorization

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton

import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R

import az.turanbank.mlb.data.remote.model.ips.response.RespAuthMethod
import kotlinx.android.synthetic.main.auth_single_select_account.view.*
import kotlinx.android.synthetic.main.check_item_list_view_custom.view.*


class AuthorizationAccountSelectAdapter(private val authMethods: ArrayList<RespAuthMethod>,
                                        private val context: Context,
                                        private val onClickListener: (position: Int) -> Unit):
    RecyclerView.Adapter<AuthorizationAccountSelectAdapter.AuthorizationAccountHolder>() {

    private var selected: RadioButton? = null
    private var selectedItem : RespAuthMethod? = null
    private var selectedPosition: Int? = null

    class AuthorizationAccountHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        /*fun bind(
            selectAuthorizationAccountModel: SelectAuthorizationAccountModel,
            onClickListener: (position: Int) -> Unit,
            context: Context) {

            if(selectAuthorizationAccountModel.checked){
                itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.checked_account))
                itemView.checkBox.isChecked = true
            } else {
                itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.unchecked_account))
                itemView.checkBox.isChecked = false
            }
            //itemView.iban.text = selectAuthorizationAccountModel.account.iban
            //itemView.currency.text = selectAuthorizationAccountModel.account.currName
            itemView.setOnClickListener { onClickListener.invoke(adapterPosition) }
            itemView.checkBox.setOnClickListener { onClickListener.invoke(adapterPosition) }
        }*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AuthorizationAccountHolder (
        LayoutInflater.from(parent.context).inflate(R.layout.auth_single_select_account,parent,false)
    )

    override fun getItemCount() = authMethods.size

    override fun onBindViewHolder(holder: AuthorizationAccountHolder, position: Int) {
        //holder.bind(accountList[position],onClickListener,context)

        val item = authMethods[position]

        holder.itemView.nameText.text = item.name

        holder.itemView.checkBox.setOnClickListener {
            selectedPosition = position
            if (selected != null) {
                selectedItem!!.checked = false
                selected!!.isChecked = false
            }
            holder.itemView.checkBox.isChecked = true
            item.checked = true
            selectedItem = item
            selected = holder.itemView.checkBox
        }
    }

    fun getSelectedItem() = selectedItem

    fun getSelectedPosition() = selectedPosition

}