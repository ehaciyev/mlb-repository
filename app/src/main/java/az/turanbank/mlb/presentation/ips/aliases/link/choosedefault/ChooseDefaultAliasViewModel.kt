package az.turanbank.mlb.presentation.ips.aliases.link.choosedefault

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.AliasAccountCompoundModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetCompoundsUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAliasesUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs

import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ChooseDefaultAliasInputs: BaseViewModelInputs{
    fun getCompounds()
}
interface ChooseDefaultAliasOutputs: BaseViewModelOutputs{
    fun onCompounds(): PublishSubject<ArrayList<AliasAccountCompoundModel>>
}
class ChooseDefaultAliasViewModel @Inject constructor(
    private val getMLBAliasesUseCase: GetMLBAliasesUseCase,
    private val getCompoundsUseCase: GetCompoundsUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
    ChooseDefaultAliasInputs,
    ChooseDefaultAliasOutputs
{

    override val inputs: ChooseDefaultAliasInputs = this

    override val outputs: ChooseDefaultAliasOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val aliases = PublishSubject.create<ArrayList<AliasAccountCompoundModel>>()

    override fun getCompounds() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getCompoundsUseCase.execute(custId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ aliases ->
                if (aliases.status.statusCode == 1) {
                    this.aliases.onNext(aliases.compounds)
                } else {
                    error.onNext(aliases.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun onCompounds() = aliases
}