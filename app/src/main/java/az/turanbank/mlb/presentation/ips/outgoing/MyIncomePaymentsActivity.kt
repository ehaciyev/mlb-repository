package az.turanbank.mlb.presentation.ips.outgoing

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.OperationModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.home.MlbHomeActivity
import az.turanbank.mlb.presentation.ips.outgoing.paymentchoice.SinglePaymentActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_my_income_payments.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class MyIncomePaymentsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: MyIncomePaymentsViewModel

    lateinit var adapter: IPSPaymentsAdapter

    private val paymentList = arrayListOf<OperationModel>()

    private val toHome: Boolean by lazy { intent.getBooleanExtra("toHome", false) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_income_payments)

      viewModel =
            ViewModelProvider(this, factory)[MyIncomePaymentsViewModel::class.java]
        toolbar_title.text = getString(R.string.my_payments)

        if(toHome){
            toolbar_back_button.setOnClickListener{
                startActivity(Intent(this@MyIncomePaymentsActivity, MlbHomeActivity::class.java))
                finish()
            }
        } else {
            toolbar_back_button.setOnClickListener{onBackPressed()}
        }

        categoryContainerShimmer.visibility = View.VISIBLE
        categoryContainerShimmer.startShimmerAnimation()

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        adapter = IPSPaymentsAdapter(
            paymentList
        ) {
            val payment = adapter.getItemAt(it)
            val intent = Intent(this, SinglePaymentActivity::class.java)
            val currency: String = if (payment.operTypeId == 5 || payment.operTypeId == 6) {
                payment.crCcy
            } else {
                payment.currency
            }
            val amount: String = if (payment.operTypeId == 5 || payment.operTypeId == 6) {
                payment.crAmt.toString()
            } else {
                payment.amount.toString()
            }

            intent.putExtra("paymentItem", payment.operId)
            intent.putExtra("operName", payment.operName)
            intent.putExtra("dtIban", payment.dtIban)
            intent.putExtra("crName", payment.crName)
            intent.putExtra("crIban", payment.crIban)
            intent.putExtra("amount", amount)
            intent.putExtra("operPurpose", payment.operPurpose)
            intent.putExtra("operPurpose", payment.operPurpose)
            intent.putExtra("currency", currency)
            intent.putExtra("createdDate", payment.createdDate)
            intent.putExtra("operStateId", payment.operStateId)
            intent.putExtra("operTypeId", payment.operTypeId)
            intent.putExtra("operState", payment.operState)
            startActivity(intent)

        }
        recyclerView.adapter = adapter
        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        viewModel.inputs.getPayments()
    }


    private fun setOutputListeners() {
        viewModel.outputs.onPaymentsArrived().subscribe {
            categoryContainerShimmer.visibility = View.GONE
            categoryContainerShimmer.stopShimmerAnimation()

            paymentList.clear()
            paymentList.addAll(it)
            adapter.notifyDataSetChanged()
            noDataAvailable.visibility = View.GONE
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe(){
            categoryContainerShimmer.visibility = View.GONE
            categoryContainerShimmer.stopShimmerAnimation()

            noDataAvailable.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    override fun onDestroy() {
        super.onDestroy()
        categoryContainerShimmer.stopShimmerAnimation()
    }

    override fun onResume() {
        super.onResume()
        viewModel.inputs.getPayments()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        if(toHome){
            startActivity(Intent(this@MyIncomePaymentsActivity, MlbHomeActivity::class.java))
            finish()
        }
    }
}
