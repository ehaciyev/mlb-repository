package az.turanbank.mlb.presentation.resources.account

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.account.AccountStatement
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.secondPartOfMoney
import kotlinx.android.synthetic.main.account_statement_item.view.*

class AccountStatementRecyclerViewAdapter(
        private val accountStatementList: ArrayList<AccountStatement>
): RecyclerView.Adapter<AccountStatementRecyclerViewAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.account_statement_item,
                    parent,
                    false)
    )

    override fun getItemCount() = accountStatementList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bind(accountStatementList[position])

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(accountStatement: AccountStatement) {
            itemView.accountStatementTransactionPurpose.text = accountStatement.purpose
            itemView.accountStatementTransactionCurrency.text = accountStatement.currName
            itemView.accountStatementTransactionDate.text = accountStatement.trDate

            if(accountStatement.crAmount == 0.0) {
                itemView.accountStatementTransactionAmount.text = firstPartOfMoney(accountStatement.dtAmount)
                itemView.accountStatementTransactionAmountReminder.text = secondPartOfMoney(accountStatement.dtAmount)
                itemView.accountStatementTransactionCard.text = accountStatement.crAccount
            } else if (accountStatement.dtAmount == 0.0) {
                itemView.accountStatementTransactionAmount.text = firstPartOfMoney(accountStatement.crAmount)
                itemView.accountStatementTransactionAmountReminder.text = secondPartOfMoney(accountStatement.crAmount)
                itemView.accountStatementTransactionCard.text = accountStatement.dtAccount
            }
        }
    }
}