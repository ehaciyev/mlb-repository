package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetOperationByIdResponseModel
import az.turanbank.mlb.data.remote.model.response.GetPaymentDocByOperIdResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.DeleteOperationUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetAsanDocByOperIdUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetOperationByIdUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetPaymentDocByOperIdUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface OperationDetailsViewModelInputs : BaseViewModelInputs {
    fun getOperationById(operId: Long)
    fun deleteOperation(operId: Long)
    fun getPaymentDocByOperId(operId: Long)
    fun getAsanDocByOperId(operId: Long)
}

interface OperationDetailsViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun operationDeleted(): CompletableSubject
    fun paymentDoc(): PublishSubject<GetPaymentDocByOperIdResponseModel>
    fun paymentADoc(): PublishSubject<GetPaymentDocByOperIdResponseModel>
    fun operationDetails(): PublishSubject<GetOperationByIdResponseModel>
}

class OperationDetailsViewModel @Inject constructor(
    private val deleteOperationUseCase: DeleteOperationUseCase,
    private val paymentDocByOperIdUseCase: GetPaymentDocByOperIdUseCase,
    private val getAsanDocByOperIdUseCase: GetAsanDocByOperIdUseCase,
    private val getOperationByIdUseCase: GetOperationByIdUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(), OperationDetailsViewModelInputs, OperationDetailsViewModelOutputs {

    override fun operationDetails() = operationDetails

    override fun paymentDoc() = paymentDoc

    override fun getPaymentDocByOperId(operId: Long) {
        showProgress.onNext(true)

        paymentDocByOperIdUseCase.execute(custId, token, operId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                        paymentDoc.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    override fun operationDeleted() = operationDeleted

    override fun getOperationById(operId: Long) {
        showProgress.onNext(true)
        getOperationByIdUseCase.execute(operId, custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    operationDetails.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    override val inputs: OperationDetailsViewModelInputs = this
    override val outputs: OperationDetailsViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val phoneNumber = sharedPrefs.getString("phoneNumber", "")
    val userId = sharedPrefs.getString("userId", "")
    val certCode = sharedPrefs.getString("certCode", "")
    private val operationDeleted = CompletableSubject.create()

    private val showProgress = PublishSubject.create<Boolean>()
    private val paymentDoc = PublishSubject.create<GetPaymentDocByOperIdResponseModel>()
    private val paymentAsanDoc = PublishSubject.create<GetPaymentDocByOperIdResponseModel>()
    private val operationDetails = PublishSubject.create<GetOperationByIdResponseModel>()

    override fun showProgress() = showProgress

    override fun deleteOperation(operId: Long) {
        showProgress.onNext(true)
        deleteOperationUseCase.execute(custId, token, operId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    operationDeleted.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getAsanDocByOperId(operId: Long) {
        showProgress.onNext(true)
        getAsanDocByOperIdUseCase.execute(custId, token, operId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    paymentAsanDoc.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun paymentADoc() = paymentAsanDoc
}