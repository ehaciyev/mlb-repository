package az.turanbank.mlb.presentation.resources.deposit.earlypayedamounts

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.deposit.EarlyPaysResponseModel
import kotlinx.android.synthetic.main.early_payed_amount_list.view.*

class EarlyPayedAmountsAdapter (private val payedAmounts: ArrayList<EarlyPaysResponseModel>, private val clickListener: (position: Int) -> Unit) : RecyclerView.Adapter<EarlyPayedAmountsAdapter.EarlyPayedAmountViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EarlyPayedAmountViewHolder =
        EarlyPayedAmountViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.early_payed_amount_list,
                parent,
                false
            )
        )

    override fun getItemCount() = payedAmounts.size

    override fun onBindViewHolder(holder: EarlyPayedAmountViewHolder, position: Int) {
        holder.bind(payedAmounts[position], clickListener)
    }
    class EarlyPayedAmountViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(loansPayments: EarlyPaysResponseModel, clickListener: (position: Int) -> Unit) {
            with(loansPayments) {
                itemView.fromDate.text = this.fromPeriod.toString()
                itemView.tillDate.text = "-"+this.toPeriod.toString()
                itemView.percent.text = this.percent.toString()+"%"
                itemView.setOnClickListener {
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}