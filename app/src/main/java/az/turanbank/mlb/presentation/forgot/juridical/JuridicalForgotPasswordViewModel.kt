package az.turanbank.mlb.presentation.forgot.juridical

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.forgot.CheckForgotPassJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.SendOTPCodeUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface JuridicalForgotPasswordViewModelInputs: BaseViewModelInputs {
    fun checkForgotPassword(pin: String, mobile: String, taxNo: String)
}
interface JuridicalForgotPasswordViewModelOutputs: BaseViewModelOutputs {
    fun verifyOtpCode(): Observable<Long>
}

class JuridicalForgotPasswordViewModel @Inject constructor(
    private val checkForgotPassJuridicalUseCase: CheckForgotPassJuridicalUseCase,
    private val sendOTPCodeUseCase: SendOTPCodeUseCase,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel(),
    JuridicalForgotPasswordViewModelInputs,
    JuridicalForgotPasswordViewModelOutputs {

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val verifyOtpCode = PublishSubject.create <Long>()
    override val inputs: JuridicalForgotPasswordViewModelInputs = this

    override val outputs: JuridicalForgotPasswordViewModelOutputs = this
    var compId = 0L
    override fun checkForgotPassword(pin: String, mobile: String, taxNo: String) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        checkForgotPassJuridicalUseCase.execute(pin, mobile, taxNo,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    sendOtpCode(it.custId, mobile, it.compId)
                    compId = it.compId
                }else{
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    private fun sendOtpCode(custId: Long, mobile: String, compId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        sendOTPCodeUseCase.execute(custId, compId, mobile,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when(it.status.statusCode) {
                    1 -> {
                        verifyOtpCode.onNext(custId)
                    }
                    else -> error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun verifyOtpCode(): Observable<Long> = verifyOtpCode
}