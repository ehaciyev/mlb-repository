package az.turanbank.mlb.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.BankInfoModel

class GetBankListAdapter(
    private val ctx: Context,
    private val bankList: ArrayList<BankInfoModel>
) : ArrayAdapter<BankInfoModel>(ctx, 0, bankList) {
    private val bankListFull = ArrayList(bankList)

    private val filter = object : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filterResult = FilterResults()
            val suggestions: ArrayList<BankInfoModel> = ArrayList()

            if(constraint == null || constraint.isEmpty()) {
                suggestions.addAll(bankListFull)
            } else {
                val filterPattern = constraint.toString().toLowerCase().trim()

                for(bank in bankListFull) {
                    if(bank.bankNameFull.toLowerCase().contains(filterPattern)) {
                        suggestions.add(bank)
                    }
                }
            }
            filterResult.values = suggestions
            filterResult.count = suggestions.size

            return filterResult
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            clear()
            addAll(results?.values as List<BankInfoModel>)
            notifyDataSetChanged()
        }

        override fun convertResultToString(resultValue: Any?): CharSequence {
            return  ((resultValue as BankInfoModel)).bankNameFull
        }
    }

    override fun getFilter() = filter

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val cV: View?

        return if(convertView == null ){
            cV = LayoutInflater.from(ctx).inflate(R.layout.bank_item_row, parent, false)

            val bankName = cV?.findViewById<TextView>(R.id.bankNameText)

            bankName?.text = bankList[position].bankNameFull

            cV
        } else {
            val bankName = convertView.findViewById<TextView>(R.id.bankNameText)

            bankName?.text = bankList[position].bankNameFull

            convertView
        }
    }
}