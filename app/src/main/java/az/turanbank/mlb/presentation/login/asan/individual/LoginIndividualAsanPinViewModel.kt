package az.turanbank.mlb.presentation.login.asan.individual

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.login.LoginIndividualForAsanUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.LoginWithAsanUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import javax.inject.Inject

interface LoginIndividualAsanPinViewModelInputs : BaseViewModelInputs {
    fun loginWithAsan(
        transactionId: Long,
        certificate: String,
        challenge: String
    )

    fun loginIndividualForAsan(
        phoneNumber: String,
        asanId: String,
        transactionId: Long,
        certificate: String,
        challenge: String,
        custId: Long,
        device: String,
        location: String
    )

    fun storeUserPhone(phoneNumber: String)
    fun storeUserId(userId: String)
}

interface LoginIndividualAsanPinViewModelOutputs : BaseViewModelOutputs {
    fun onLogiIndividualAsan(): CompletableSubject
}

class LoginIndividualAsanPinViewModel @Inject constructor(
    private val loginWithAsanUseCase: LoginWithAsanUseCase,
    private val loginIndividualForAsanUseCase: LoginIndividualForAsanUseCase,
    private val sharedPreferences: SharedPreferences
) :
    BaseViewModel(),
    LoginIndividualAsanPinViewModelInputs,
    LoginIndividualAsanPinViewModelOutputs {

    override fun storeUserId(userId: String) {
        sharedPreferences.edit().putString("userId", userId).apply()
    }

    override fun storeUserPhone(phoneNumber: String) {
        sharedPreferences.edit().putString("phoneNumber", phoneNumber).apply()
    }

    override fun loginIndividualForAsan(
        phoneNumber: String,
        asanId: String,
        transactionId: Long,
        certificate: String,
        challenge: String,
        custId: Long,
        device: String,
        location: String
    ) {
        loginIndividualForAsanUseCase.execute(
            asanId,
            phoneNumber,
            transactionId,
            certificate,
            challenge,
            custId,
            device,
            location
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    sharedPreferences.edit().putBoolean("isCustomerJuridical", false).apply()
                    sharedPreferences.edit().putBoolean("loggedInWithAsan", true).apply()
                    sharedPreferences.edit().putString("certCode", it.certCode).apply()
                    sharedPreferences.edit().putString("phoneNumber", phoneNumber).apply()
                    sharedPreferences.edit().putString("userId", asanId).apply()

                    storeUserData(
                        it.custId,
                        it.token,
                        name = it.name,
                        surname = it.surname,
                        patronymic = it.patronymic
                    )

                    loginIndividualAsan.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    private fun storeUserData(
        custId: Long,
        token: String,
        name: String,
        surname: String,
        patronymic: String
    ) {
        sharedPreferences.edit().putLong("custId", custId).apply()
        sharedPreferences.edit().putString("token", token).apply()
        sharedPreferences.edit().putString("fullname", "$surname $name $patronymic").apply()
    }

    override val inputs: LoginIndividualAsanPinViewModelInputs = this
    override val outputs: LoginIndividualAsanPinViewModelOutputs = this

    private val loginIndividualAsan = CompletableSubject.create()

    override fun loginWithAsan(
        transactionId: Long,
        certificate: String,
        challenge: String
    ) {
        loginWithAsanUseCase.execute(transactionId, certificate, challenge)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    sharedPreferences.edit().putBoolean("isCustomerJuridical", false).apply()
                    sharedPreferences.edit().putBoolean("loggedInWithAsan", true).apply()
                    loginIndividualAsan.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun onLogiIndividualAsan() = loginIndividualAsan
}