package az.turanbank.mlb.presentation.resources.card.requisit

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_card_requisities.*
import javax.inject.Inject

class CardRequisitesActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardRequisitesViewModel

    @Inject
    lateinit var sharedPrefs: SharedPreferences
    private val cardId: Long by lazy { intent.getLongExtra("cardId", 0L) }
    private val cardNumberText: String by lazy { intent.getStringExtra("cardNumber") }
    private var isCustomerJuridical: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_requisities)

        viewModel = ViewModelProvider(this, factory)[CardRequisitesViewModel::class.java]

        isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

        setInputListeners()
        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onRequisiteSuccess().subscribe {
                cardRequisites ->

            scrollView.visibility = View.VISIBLE
            failMessage.visibility = View.GONE

            branchCode.text = cardRequisites.branchCode.toString()
            branchInn.text = cardRequisites.branchInn
            correspondentAccount.text = cardRequisites.correspondentAccount
            swift.text = cardRequisites.swift
            address.text = cardRequisites.address
            phone.text = cardRequisites.phone
            cardNumber.text = cardNumberText.replaceRange(4, 12, " **** **** ")
            currName.text = cardRequisites.currName
            iban.text = cardRequisites.iban
            accountName.text = cardRequisites.accountName
            if (isCustomerJuridical) {
                accountInn.text = cardRequisites.accountInn
            } else {
                accountInn.visibility = View.GONE
                client_voen.visibility = View.GONE
            }
            progress.clearAnimation()
            progress.visibility = View.GONE
            scrollView.visibility = View.VISIBLE

            cardReqShare.setOnClickListener {
                it.isClickable = true
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type="text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, cardRequisites.toString())
                startActivity(Intent.createChooser(shareIntent, getString(R.string.send)))
            }
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
            scrollView.visibility = View.GONE
            failMessage.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        scrollView.visibility = View.GONE
        animateProgressImage()
        viewModel.inputs.getCardRequisites(cardId)
        toolbar_back_button.setOnClickListener { onBackPressed() }
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
