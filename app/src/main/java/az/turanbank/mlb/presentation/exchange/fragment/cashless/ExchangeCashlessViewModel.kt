package az.turanbank.mlb.presentation.exchange.fragment.cashless

import az.turanbank.mlb.data.remote.model.exchange.ExchangeCashlessResponseModel
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashlessUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ExchangeCashlessViewModelInputs: BaseViewModelInputs {
    fun getCashlessExchange(exchangeDate: String?)
}
interface ExchangeCashlessViewModelOutputs: BaseViewModelOutputs {
    fun onCashExchangeGot(): PublishSubject<ExchangeCashlessResponseModel>
}

class ExchangeCashlessViewModel @Inject constructor(
    private val exchangelessCashUseCase: ExchangeCashlessUseCase
): BaseViewModel(),
    ExchangeCashlessViewModelInputs,
    ExchangeCashlessViewModelOutputs {
    override val inputs: ExchangeCashlessViewModelInputs = this

    override val outputs: ExchangeCashlessViewModelOutputs = this


    private val cashExchangeList = PublishSubject.create<ExchangeCashlessResponseModel>()
    override fun getCashlessExchange(exchangeDate: String?) {
        exchangelessCashUseCase.execute(exchangeDate)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({
                if (it.status.statusCode == 1) {
                    cashExchangeList.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }
            ).addTo(subscriptions)
    }

    override fun onCashExchangeGot() = cashExchangeList
}