package az.turanbank.mlb.presentation.news

import android.content.Context
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.CampaignModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.news_list_view.view.*

class NewsAdapter(private val context: Context, private val newsList: ArrayList<CampaignModel>, private val clickListener: (position: Int) -> Unit) :
    RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {


    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(news: CampaignModel, context: Context, clickListener: (position: Int) -> Unit) {
            val byteArray = Base64.decode(news.image, Base64.DEFAULT)
            Glide
                .with(context)
                .load(byteArray)
                .centerCrop()
                .into(itemView.campaignImage)
            itemView.title.text = news.name
            itemView.description.text = news.note
            itemView.setOnClickListener { clickListener.invoke(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.news_list_view,
                parent,
                false
            )
        )

    override fun getItemCount() = newsList.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(newsList[position], context, clickListener)
    }
}