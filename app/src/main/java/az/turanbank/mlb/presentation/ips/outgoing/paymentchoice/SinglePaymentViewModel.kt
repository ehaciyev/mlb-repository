package az.turanbank.mlb.presentation.ips.outgoing.paymentchoice

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetPaymentDocByOperIdResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.DeleteOperationUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetAsanDocByOperIdUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetPaymentDocByOperIdUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface SinglePaymentOutputViewModel : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun operationDeleted(): CompletableSubject
    fun paymentDoc(): PublishSubject<GetPaymentDocByOperIdResponseModel>
    fun paymentADoc(): PublishSubject<GetPaymentDocByOperIdResponseModel>

}

interface SinglePaymentInputViewModel : BaseViewModelInputs {
    fun getPaymentDocByOperId(operId: Long)
    fun getAsanDocByOperId(operId: Long)
    fun deleteOperation(operId: Long)
}

class SinglePaymentViewModel @Inject constructor(
    sharedPrefs: SharedPreferences,
    private val getAsanDocByOperIdUseCase: GetAsanDocByOperIdUseCase,
    private val paymentDocByOperIdUseCase: GetPaymentDocByOperIdUseCase,
    private val deleteOperationUseCase: DeleteOperationUseCase
) : BaseViewModel(),
    SinglePaymentOutputViewModel,
    SinglePaymentInputViewModel {

    override val inputs: SinglePaymentInputViewModel = this
    override val outputs: SinglePaymentOutputViewModel = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val showProgress = PublishSubject.create<Boolean>()

    private val operationDeleted = CompletableSubject.create()

    override fun showProgress() = showProgress
    override fun paymentDoc() = paymentDoc

    private val paymentDoc = PublishSubject.create<GetPaymentDocByOperIdResponseModel>()
    private val paymentAsanDoc = PublishSubject.create<GetPaymentDocByOperIdResponseModel>()

    override fun operationDeleted(): CompletableSubject = operationDeleted

    override fun deleteOperation(operId: Long) {
        showProgress.onNext(true)
        deleteOperationUseCase.execute(custId, token, operId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    operationDeleted.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getAsanDocByOperId(operId: Long) {
        showProgress.onNext(true)
        getAsanDocByOperIdUseCase.execute(custId, token, operId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    paymentAsanDoc.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun paymentADoc() = paymentAsanDoc

    override fun getPaymentDocByOperId(operId: Long) {
        showProgress.onNext(true)

        paymentDocByOperIdUseCase.execute(custId, token, operId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    paymentDoc.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

}
