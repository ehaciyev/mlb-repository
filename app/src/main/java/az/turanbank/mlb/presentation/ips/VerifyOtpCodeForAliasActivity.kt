package az.turanbank.mlb.presentation.ips

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.ips.request.AliasRequestModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_verify_individual_register_mobile.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class VerifyOtpCodeForAliasActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: SendOtpCodeForAliasViewModel

    lateinit var dialog: MlbProgressDialog
//    private var counter = 0
    private val decimalFormat = DecimalFormat("00")
    private val countDown = object : CountDownTimer(180000, 1000) {

        @SuppressLint("SetTextI18n")
        override fun onTick(millisUntilFinished: Long) {
            decimalFormat.roundingMode = RoundingMode.CEILING
            timer.text = ((millisUntilFinished / 1000) / 60).toString() + ":" + decimalFormat.format((millisUntilFinished / 1000) % 60).toString()
        }

        override fun onFinish() {
            startActivity(
                Intent(
                    this@VerifyOtpCodeForAliasActivity,
                    ErrorPageViewActivity::class.java
                )
            )
            finish()
        }
    }

    private var confirmed = PublishSubject.create<Boolean>()
    private val mobileOrEmail by lazy { intent.getStringExtra("mobileOrEmail") }
    private val broadcastType by lazy { intent.getStringExtra("broadcastType") }
    private val message by lazy { intent.getStringExtra("message") }
    private val numberOrEmail by lazy { intent.getStringExtra("numberOrEmail") }
    private val fromRegister : Boolean by lazy { intent.getBooleanExtra("fromRegister", false) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_individual_register_mobile)

        viewModel =
            ViewModelProvider(this, factory)[SendOtpCodeForAliasViewModel::class.java]
        toolbar_title.text = getString(R.string.new_alias)

        dialog = MlbProgressDialog(this)

        setOutputListeners()
        setInputListeners()

        submitText.text = getString(R.string.confirm)
        countDown.start()
    }

    private fun setOutputListeners() {
        viewModel.outputs.aliasCreated().subscribe {
            dialog.hideDialog()
            Toast.makeText(this, ErrorMessageMapper().getErrorMessage(124015), Toast.LENGTH_LONG).show()
            if (fromRegister){
                startActivity(Intent(this, InstantPaymentActivity::class.java))
            }
            finish()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            dialog.hideDialog()
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)

        viewModel.outputs.onVerifyOTPSuccess().subscribe{
            confirmed.onNext(true)
        }.addTo(subscriptions)

        viewModel.outputs.onVerifyOtpCodeForEmailSuccess().subscribe{
            confirmed.onNext(true)
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        confirmed.subscribe {
            if(it){
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
                val startDate = sdf.format(Date())
                viewModel.inputs.createAlias(arrayListOf(AliasRequestModel(broadcastType, mobileOrEmail, startDate, null,  1)))
            }
        }.addTo(subscriptions)

        sendOtpAgain.visibility = View.GONE

        submitButton.setOnClickListener {
            if (pinCodeView.text.toString().trim().isNotEmpty()) {
                dialog.showDialog()
                when (broadcastType) {
                    "MOBILE" -> {
                        viewModel.inputs.verifyOTP(pinCodeView.text.toString().trim())
                    }
                    "EMAIL" -> {
                        viewModel.inputs.verifyOtpCodeForEmail(pinCodeView.text.toString().trim())
                    }
                    else -> {
                        viewModel.inputs.verifyOTP(pinCodeView.text.toString().trim())
                    }
                }

            } else {
                AlertDialogMapper(this, 124004).showAlertDialog()
            }
        }
        number.text = numberOrEmail
        auth_message.text = message

        toolbar_back_button.setOnClickListener { onBackPressed() }
    }

    override fun onDestroy() {
        super.onDestroy()
        countDown.cancel()
    }

}
