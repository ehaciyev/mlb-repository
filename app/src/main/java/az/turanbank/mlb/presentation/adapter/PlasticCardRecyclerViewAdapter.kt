package az.turanbank.mlb.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.PlasticCardMapper
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.formatDecimal
import az.turanbank.mlb.util.secondPartOfMoney
import az.turanbank.mlb.util.splitString
import kotlinx.android.synthetic.main.plastic_card_item.view.*
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import kotlin.collections.ArrayList

class PlasticCardRecyclerViewAdapter(
        private val plasticCardList: ArrayList<CardListModel>,
        var clickListener: (position: Int) -> Unit,
        var longClickListener: (position:Int) -> Unit
) : RecyclerView.Adapter<PlasticCardRecyclerViewAdapter.PlasticCardViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PlasticCardViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.plastic_card_item,
                parent,
                false
            )
        )

    override fun getItemCount() = plasticCardList.size

    override fun onBindViewHolder(holder: PlasticCardViewHolder, position: Int) =
            holder.bind(plasticCard = plasticCardList[position], clickListener = clickListener, longClickListener = longClickListener)

    class PlasticCardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(plasticCard: CardListModel, clickListener: (position: Int) -> Unit, longClickListener: (position: Int) -> Unit) {


            val expiryDate = plasticCard.expiryDate.substring(5, 7) + "/" + plasticCard.expiryDate.substring(2, 4)
            val cardNumber = plasticCard.cardNumber.substring(0, 4) + "******" + plasticCard.cardNumber.substring(12)
            val balanceWithCurrency = plasticCard.balance.formatDecimal() + " " + plasticCard.currency

            if (plasticCard.cardStatus == "0"){
                itemView.view.visibility = View.VISIBLE
            } else {
                itemView.view.visibility = View.GONE
            }

            itemView.plastic_card.setImageResource(PlasticCardMapper().resolveCard(plasticCard.cardType))
            itemView.plasticCardAmountInteger.text = firstPartOfMoney(plasticCard.balance)
            itemView.plasticCardAmountReminder.text = secondPartOfMoney(plasticCard.balance) + " " + plasticCard.currency

            itemView.plasticCardExpireDate.text = expiryDate
            itemView.plasticAccountNumber.text = plasticCard.correspondentAccount
            itemView.plasticCardNumber.text = cardNumber
            itemView.setOnClickListener {
                clickListener.invoke(adapterPosition)
            }
            itemView.setOnLongClickListener {
                longClickListener.invoke(adapterPosition)
                return@setOnLongClickListener true
            }
        }
    }


}
