package az.turanbank.mlb.presentation.register

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.adapter.RegisterPagerAdapter
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class RegisterActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        val sectionsPagerAdapter =
            RegisterPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)

        toolbar_title.text = resources.getText(R.string.register)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
    }
}