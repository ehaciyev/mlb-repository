package az.turanbank.mlb.presentation.resources.account.abroad

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.CurrencySignMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.secondPartOfMoney
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_operation_abroad_verify.*
import kotlinx.android.synthetic.main.activity_card_requisities.*
import javax.inject.Inject

class AccountOperationAbroadVerifyActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private val dtAccountId: Long by lazy { intent.getLongExtra("dtAccountId", 0L) }
    private val crBankName: String by lazy { intent.getStringExtra("crBankName") }
    private val amount: Double by lazy { intent.getStringExtra("amount")!!.toDouble() }
    private val crBankSwift: String by lazy { intent.getStringExtra("crBankSwift") }
    private val crCustName: String by lazy { intent.getStringExtra("crCustName") }
    private val crCustAddress: String by lazy { intent.getStringExtra("crCustAddress") }
    private val crIban: String by lazy { intent.getStringExtra("crIban") }
    private val purpose: String by lazy { intent.getStringExtra("purpose") }
    private val crBankBranch: String by lazy { intent.getStringExtra("crBankBranch") }
    private val crBankAddress: String by lazy { intent.getStringExtra("crBankAddress") }
    private val crBankCountry: String by lazy { intent.getStringExtra("crBankCountry") }
    private val crBankCity: String by lazy { intent.getStringExtra("crBankCity") }
    private val crCustPhone: String by lazy { intent.getStringExtra("crCustPhone") }
    private val note: String by lazy { intent.getStringExtra("note") }
    private val crCorrBankName: String by lazy { intent.getStringExtra("crCorrBankName") }
    private val crCorrBankCountry: String by lazy { intent.getStringExtra("crCorrBankCountry") }
    private val crCorrBankCity: String by lazy { intent.getStringExtra("crCorrBankCity") }
    private val crCorrBankSwift: String by lazy { intent.getStringExtra("crCorrBankSwift") }
    private val crCorrBankAccount: String by lazy { intent.getStringExtra("crCorrBankAccount") }
    private val crCorrBankBranch: String by lazy { intent.getStringExtra("crCorrBankBranch") }
    private val currency: String by lazy { intent.getStringExtra("currency") }
    lateinit var viewModel: AccountOperationAbroadVerifyViewModel
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_operation_abroad_verify)

        viewModel =
            ViewModelProvider(this, factory)[AccountOperationAbroadVerifyViewModel::class.java]

        toolbar_title.text = getString(R.string.international_transfer)

        toolbar_back_button.setOnClickListener { onBackPressed() }
        setInputListeners()

        setOutputListeners()
    }

    @SuppressLint("SetTextI18n")
    private fun setOutputListeners() {
        senderName.text = intent.getStringExtra("debitorName")
        receiverName.text = crCustName
        amountEditText.text = amount.toString() + currency
        senderBankBranch.text = crBankName


        val curr = CurrencySignMapper().getCurrencySign(currency)

        bigAmountInteger.text = curr + firstPartOfMoney(amount)
        bigAmountReminder.text = secondPartOfMoney(amount)
        //bigViewAmount.text = "$curr $amount"

        viewModel.outputs.onOperationSuccess().subscribe {
            Toast.makeText(this, getString(R.string.payment_is_successful), Toast.LENGTH_LONG).show()
            val intent = Intent()
            intent.putExtra("accountResponse", it)
            intent.putExtra("operationNo", it.operationNo)

            setResult(Activity.RESULT_OK, intent)
            finish()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            showProgressBar(false)
            Toast.makeText(this, ErrorMessageMapper().getErrorMessage(it), Toast.LENGTH_LONG).show()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        submitButton.setOnClickListener {
            showProgressBar(true)
            viewModel.inputs.createAbroadOperation(
                dtAccountId,
                amount,
                crBankName,
                crBankSwift,
                crCustName,
                crCustAddress,
                crIban,
                purpose,
                crBankBranch,
                crBankAddress,
                crBankCountry,
                crBankCity,
                crCustPhone,
                note,
                crCorrBankName,
                crCorrBankCountry,
                crCorrBankCity,
                crCorrBankSwift,
                crCorrBankAccount,
                crCorrBankBranch
            )
        }
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }
}
