package az.turanbank.mlb.presentation.resources.account.abroad

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.DomesticOperationResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.abroad.CreateAbroadOperationIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.abroad.CreateAbroadOperationJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountOperationAbroadVerifyViewModelInputs : BaseViewModelInputs {
    fun createAbroadOperation(
        dtAccountId: Long,
        amount: Double,
        crBankName: String,
        crBankSwift: String,
        crCustName: String,
        crCustAddress: String,
        crIban: String,
        purpose: String,
        crBankBranch: String,
        crBankAddress: String,
        crBankCountry: String,
        crBankCity: String,
        crCustPhone: String,
        note: String,
        crCorrBankName: String,
        crCorrBankCountry: String,
        crCorrBankCity: String,
        crCorrBankSwift: String,
        crCorrBankAccount: String,
        crCorrBankBranch: String
    )
}

interface AccountOperationAbroadVerifyViewModelOutputs : BaseViewModelOutputs {
    fun onOperationSuccess(): PublishSubject<DomesticOperationResponseModel>
}

class AccountOperationAbroadVerifyViewModel @Inject constructor(
    private val createAbroadOperationIndividualUseCase: CreateAbroadOperationIndividualUseCase,
    private val createAbroadOperationJuridicalUseCase: CreateAbroadOperationJuridicalUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
    AccountOperationAbroadVerifyViewModelInputs,
    AccountOperationAbroadVerifyViewModelOutputs {
    override val inputs: AccountOperationAbroadVerifyViewModelInputs = this

    override val outputs: AccountOperationAbroadVerifyViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val createOperationAbroadSuccess: PublishSubject<DomesticOperationResponseModel> =
        PublishSubject.create()
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun createAbroadOperation(
        dtAccountId: Long,
        amount: Double,
        crBankName: String,
        crBankSwift: String,
        crCustName: String,
        crCustAddress: String,
        crIban: String,
        purpose: String,
        crBankBranch: String,
        crBankAddress: String,
        crBankCountry: String,
        crBankCity: String,
        crCustPhone: String,
        note: String,
        crCorrBankName: String,
        crCorrBankCountry: String,
        crCorrBankCity: String,
        crCorrBankSwift: String,
        crCorrBankAccount: String,
        crCorrBankBranch: String
    ) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if (isCustomerJuridical) {
            createAbroadOperationJuridicalUseCase
                .execute(
                    custId,
                    compId,
                    token,
                    dtAccountId,
                    amount,
                    crBankName,
                    crBankSwift,
                    crCustName,
                    crCustAddress,
                    crIban,
                    purpose,
                    crBankBranch,
                    crBankAddress,
                    crBankCountry,
                    crBankCity,
                    crCustPhone,
                    note,
                    crCorrBankName,
                    crCorrBankCountry,
                    crCorrBankCity,
                    crCorrBankSwift,
                    crCorrBankAccount,
                    crCorrBankBranch,
                    enumLangType
                ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        createOperationAbroadSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {

                }).addTo(subscriptions)
        } else {
            createAbroadOperationIndividualUseCase
                .execute(
                    custId,
                    token,
                    dtAccountId,
                    amount,
                    crBankName,
                    crBankSwift,
                    crCustName,
                    crCustAddress,
                    crIban,
                    purpose,
                    crBankBranch,
                    crBankAddress,
                    crBankCountry,
                    crBankCity,
                    crCustPhone,
                    note,
                    crCorrBankName,
                    crCorrBankCountry,
                    crCorrBankCity,
                    crCorrBankSwift,
                    crCorrBankAccount,
                    crCorrBankBranch,
                    enumLangType
                ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        createOperationAbroadSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {

                }).addTo(subscriptions)
        }
    }

    override fun onOperationSuccess() = createOperationAbroadSuccess
}