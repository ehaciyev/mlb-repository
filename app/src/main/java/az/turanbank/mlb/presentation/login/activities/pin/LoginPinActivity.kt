package az.turanbank.mlb.presentation.login.activities.pin

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.CircleImageView
import az.turanbank.mlb.presentation.MlbPinNumberBoard
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.home.MlbHomeActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.view.StcPinView
import com.bumptech.glide.Glide
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_login_pin.*
import kotlinx.android.synthetic.main.mlb_number_keyboard.*
import javax.inject.Inject

class LoginPinActivity : BaseActivity() {

    lateinit var viewmodel: LoginPinViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var pinCode: String
    private var pinStep: Int = 1

    lateinit var profileImage: CircleImageView

    lateinit var dialog: MlbProgressDialog

    var creatingNewPin: Boolean = false
    var isPinView : Boolean = false
    val updatingCurrentPin: Boolean by lazy { intent.getBooleanExtra("updatingCurrentPin", false) }

    //private val noInternet: String by lazy { intent.getStringExtra("description") }


    private var isOnlineCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_pin)

        viewmodel = ViewModelProvider(this, factory)[LoginPinViewModel::class.java]
        fullName.text = viewmodel.fullName()
        creatingNewPin = intent.getBooleanExtra("creatingNewPin", false)

        dialog = MlbProgressDialog(this)

        profileImage = findViewById(R.id.profileImage)
        profileImage.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        val pinKeyBoard = findViewById<MlbPinNumberBoard>(R.id.pinKeyBoard)
        //pinKeyBoard.setListener(this)
        val pin = findViewById<StcPinView>(R.id.pin)

        buttonCancel.setOnClickListener {
            finish()
        }
        setOutputListeners()
        setInputListeners()

        if (creatingNewPin) {
            disablePinCancelling()
            showBiometricPrompt(true)
        } else {
            viewmodel.inputs.checkIfFingerPrintIsSet()
        }

        pin.setTextIsSelectable(true)

        if (updatingCurrentPin) {
            Toast.makeText(
                this@LoginPinActivity,
                getString(R.string.enter_current_pin),
                Toast.LENGTH_LONG
            ).show()
        } else if(creatingNewPin){
            Toast.makeText(this@LoginPinActivity, getString(R.string.enter_pin), Toast.LENGTH_SHORT).show()
        }

        val ic: InputConnection = pin.onCreateInputConnection(EditorInfo())
        pinKeyBoard.setInputConnection(ic)

        pin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (creatingNewPin) {
                    if (s.toString().length == 4) {
                        if (pinStep == 1) {
                            pinCode = s.toString()
                            pin.setText("")
                            pinStep = 2
                            Toast.makeText(
                                this@LoginPinActivity,
                                getString(R.string.repeat_pin),
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            if (s.toString() == pinCode) {
                                isPinView = false
                                dialog.showDialog()
                                pin.setText("")
                                if(!isPinView){
                                    viewmodel.inputs.savePin(pinCode)
                                    navigateToHome()
                                } else {
                                    viewmodel.inputs.checkPin(pinCode)
                                }
                            } else {
                                dialog.hideDialog()
                                showDialog(getString(R.string.repeat_pin_is_wrong))
                                pinStep = 1
                                pinCode = ""
                                //pin.setText("")
                            }
                        }
                    }
                } else if (updatingCurrentPin) {
                    if (s.toString().length == 4) {
                        //pin.setText("")
                        if (s.toString() == viewmodel.getCurrentPin()) {
                            Toast.makeText(
                                this@LoginPinActivity,
                                getString(R.string.enter_new_pin),
                                Toast.LENGTH_SHORT
                            ).show()
                            creatingNewPin = true
                        }
                    }
                } else {
                    if (s.toString().length == 4) {
                        isPinView = true
                        pinCode = s.toString()
                        //pin.setText("")

                        if(!isPinView){
                            viewmodel.inputs.savePin(pinCode)
                            navigateToHome()
                        } else {
                            viewmodel.inputs.checkPin(pinCode)
                        }

                        //viewmodel.inputs.checkInternet()
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun showDialog(message: String) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok)) { _, _ ->
            }
            .show()
    }
    private fun setInputListeners() {
        viewmodel.inputs.getProfileImage()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            pin.showSoftInputOnFocus = false
        } else {
            pin.setTextIsSelectable(false)
        }
    }

    private fun disablePinCancelling() {
        buttonCancel.isEnabled = false
    }

    private fun setOutputListeners() {

        viewmodel.outputs.getProfileImageSuccess().subscribe {
            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)

            Glide
                .with(this)
                .load(byteArray)
                .centerCrop()
                .into(profileImage)

        }.addTo(subscriptions)

        viewmodel.outputs.fingerprintIsSet().subscribe { isSet ->
            if (isSet)
                showBiometricPrompt(isSet)
        }.addTo(subscriptions)

        viewmodel.outputs.pinIsCorrect().subscribe { pinIsCorrect ->
            if (pinIsCorrect) {
                navigateToHome()
                finish()
            } else {
                dialog.hideDialog()
                showDialog(getString(R.string.pin_is_wrong))
                pin.setText("")
            }
        }.addTo(subscriptions)

        viewmodel.outputs.onSavePinSuccess().subscribe{
            navigateToHome()
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe{
            dialog.hideDialog()
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)

        viewmodel.outputs.onInternetSuccess().subscribe{
            if(it){
                if(!isPinView){
                    viewmodel.inputs.savePin(pinCode)
                    navigateToHome()
                } else {
                    viewmodel.inputs.checkPin(pinCode)
                }
            } else {
                dialog.hideDialog()
                AlertDialogMapper(this, 1878).showAlertDialog()
            }
        }.addTo(subscriptions)
    }

    private fun navigateToHome() {
        if (!updatingCurrentPin) {
            viewmodel.inputs.login()
            val intent = Intent(this, MlbHomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            dialog.hideDialog()
            startActivity(intent)

        }
        finish()
    }

    private fun showBiometricPrompt(fingerPrintIsSet: Boolean) {
        if (BiometricManager.from(this).canAuthenticate() == BiometricManager.BIOMETRIC_SUCCESS) {
            val promptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle(getString(R.string.biometric_login))
                .setSubtitle(getString(R.string.login_with_biometric_credentials))
                .setNegativeButtonText(getString(R.string.cancel_all_caps))
                .build()

            val biometricPrompt = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                BiometricPrompt(this, mainExecutor,
                    object : BiometricPrompt.AuthenticationCallback() {

                        override fun onAuthenticationError(
                            errorCode: Int,
                            errString: CharSequence
                        ) {
                            super.onAuthenticationError(errorCode, errString)
                            /*Toast.makeText(
                                applicationContext,
                                getString(R.string.error_message), Toast.LENGTH_SHORT
                            ).show()*/
                        }

                        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                            super.onAuthenticationSucceeded(result)
                            // User has verified the signature, cipher, or message
                            // authentication code (MAC) associated with the crypto object,
                            // so you can use it in your app's crypto-driven workflows.
                            if (!updatingCurrentPin) {
                                if (!creatingNewPin) {
                                    navigateToHome()
                                } else {
                                    viewmodel.inputs.fingerprintIsSet(true)
                                }
                            }
                        }

                        override fun onAuthenticationFailed() {
                            super.onAuthenticationFailed()
                            Toast.makeText(
                                applicationContext, getString(R.string.authentication_failed),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }).authenticate(promptInfo)
            } else {

            }
        }
    }

   /* private lateinit var inputConnection: InputConnection

    override fun onNumberClicked(number: Int) {
        val selectedText: CharSequence = inputConnection.getExtractedText(ExtractedTextRequest(), 0).text
        inputConnection.setComposingText(selectedText.toString() + number, 1)
    }

    override fun onLeftAuxButtonClicked() {
        finish()
    }

    override fun onRightAuxButtonClicked() {
        val selectedText: CharSequence = inputConnection.getExtractedText(ExtractedTextRequest(), 0).text
        if (TextUtils.isEmpty(selectedText)) {
            inputConnection.deleteSurroundingText(1, 0)
        } else {
            inputConnection.setComposingText(selectedText.substring(0, selectedText.length - 1), 1)
        }
    }

    fun setInputConnection(iC: InputConnection) {
        inputConnection = iC
    }*/
}
