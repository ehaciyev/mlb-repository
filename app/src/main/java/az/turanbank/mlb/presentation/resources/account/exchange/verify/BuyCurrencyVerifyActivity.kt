package az.turanbank.mlb.presentation.resources.account.exchange.verify

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.CurrencySignMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.secondPartOfMoney
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_buy_currency_verify.*
import kotlinx.android.synthetic.main.activity_card_requisities.*
import javax.inject.Inject

class BuyCurrencyVerifyActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: BuyCurrencyVerifyViewModel

    private val dtAccountId: Long by lazy { intent.getLongExtra("dtAccountId", 0L) }
    private val dtAccount: String by lazy { intent.getStringExtra("dtAccount") }
    private val crIban: String by lazy { intent.getStringExtra("crIban") }
    private val dtAmount: Double by lazy { intent.getDoubleExtra("dtAmount", 0.00) }
    private val crAmount: Double by lazy { intent.getDoubleExtra("crAmount", 0.00) }
    private val dtBranchId: Long by lazy { intent.getLongExtra("dtBranchId", 0L) }
    private val crBranchId: Long by lazy { intent.getLongExtra("crBranchId", 0L) }
    private val currency: String by lazy { intent.getStringExtra("currency") }
    private val exchangeRate: Double by lazy { intent.getDoubleExtra("exchangeRate", 0.00) }
    private val exchangeOperationType: Int by lazy { intent.getIntExtra("exchangeOperationType", 0) }
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_currency_verify)

        toolbar_title.text = getString(R.string.exchange)

        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewModel =
            ViewModelProvider(this, factory)[BuyCurrencyVerifyViewModel::class.java]

        val curr = CurrencySignMapper().getCurrencySign(currency)
        senderName.text = dtAccount
        receiverName.text = crIban
        amountEditText.text = crAmount.toString()
        currencyEditText.text = currency
        bigAmountInteger.text = curr + firstPartOfMoney(crAmount)
        bigAmountReminder.text = secondPartOfMoney(crAmount)
        //bigViewAmount.text = "$curr $crAmount"
        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        submitButton.setOnClickListener {
            showProgressBar(true)
            viewModel.inputs.makeExchange(
                dtAccountId,
                dtAmount,
                crAmount,
                crIban,
                dtBranchId,
                crBranchId,
                exchangeRate,
                exchangeOperationType
            )
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onExchangeSuccess().subscribe {
            showProgressBar(false)
            Toast.makeText(this, getString(R.string.convertation_successfully_created), Toast.LENGTH_LONG).show()
            val intent = Intent()
            intent.putExtra("exchangeResponse", it)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
            finish()
        }.addTo(subscriptions)
    }
    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }
}
