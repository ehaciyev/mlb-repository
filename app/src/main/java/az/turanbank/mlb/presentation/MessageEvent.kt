package az.turanbank.mlb.presentation

data class MessageEvent(
    val messageString: String,
    val messageLong: Long,
    val messageFloat: Float,
    val messageBoolean: Boolean
)