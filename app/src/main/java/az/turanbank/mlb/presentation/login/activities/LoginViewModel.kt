package az.turanbank.mlb.presentation.login.activities

import android.content.SharedPreferences
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import javax.inject.Inject

interface LoginViewModelInputs : BaseViewModelInputs {
}

interface LoginViewModelOutputs : BaseViewModelOutputs {
}

class LoginViewModel @Inject constructor(
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    LoginViewModelInputs,
    LoginViewModelOutputs {

    override val inputs: LoginViewModelInputs = this
    override val outputs: LoginViewModelOutputs = this
    val pinIsSet = sharedPrefs.getBoolean("pinIsSet", false)
    val userLoggedIn = sharedPrefs.getBoolean("userLoggedIn", false)
}