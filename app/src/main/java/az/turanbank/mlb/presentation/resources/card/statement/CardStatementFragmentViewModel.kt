package az.turanbank.mlb.presentation.resources.card.statement

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.CardStatementModel
import az.turanbank.mlb.domain.user.usecase.resources.card.statement.*
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


interface CardStatementActivityViewModelInputs : BaseViewModelInputs {
    fun getCardStatement(cardId: Long, cardNumber: String?, startDate: String, endDate: String, type: Int)
}

interface CardStatementActivityViewModelOutputs : BaseViewModelOutputs {
    fun cardStatement(): Observable<ArrayList<CardStatementModel>>
    fun showProgress(): Observable<Boolean>
}

class CardStatementFragmentViewModel @Inject constructor(
    private val getCardStatementForIndividualCustomerUseCase: GetCardStatementForIndividualCustomerUseCase,
    private val getCardStatementForJuridicalCustomerUseCase: GetCardStatementForJuridicalCustomerUseCase,
    private val getCardCreditStatementForIndividualCustomerUseCase: GetCardCreditStatementForIndividualCustomerUseCase,
    private val getCardCreditStatementForJuridicalCustomerUseCase: GetCardCreditStatementForJuridicalCustomerUseCase,
    private val getCardDebitStatementForIndividualCustomerUseCase: GetCardDebitStatementForIndividualCustomerUseCase,
    private val getCardDebitStatementForJuridicalCustomerUseCase: GetCardDebitStatementForJuridicalCustomerUseCase,
    private val sharedPrefs: SharedPreferences
):
    BaseViewModel(),
    CardStatementActivityViewModelInputs,
    CardStatementActivityViewModelOutputs {

    override fun showProgress() = showProgress

    override val inputs: CardStatementActivityViewModelInputs = this
    override val outputs: CardStatementActivityViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val cardStatementList = PublishSubject.create<ArrayList<CardStatementModel>>()
    private val showProgress = PublishSubject.create<Boolean>()

    override fun getCardStatement(cardId: Long, cardNumber: String?, startDate: String, endDate: String, type: Int) {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            when (type){
                0 -> {
                    getCardStatementForJuridicalCustomerUseCase
                        .execute(custId, compId, token, cardNumber, startDate, endDate)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            showProgress.onNext(false)
                            if (it.status.statusCode == 1) {
                                cardStatementList.onNext(it.cardStamentList)
                            } else {
                                error.onNext(it.status.statusCode)
                            }
                        }, {
                            it.printStackTrace()
                            error.onNext(1878)
                        }).addTo(subscriptions)
                }
                1 ->{
                    getCardCreditStatementForJuridicalCustomerUseCase
                        .execute(custId, compId, token, cardNumber, startDate, endDate)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            showProgress.onNext(false)
                            if (it.status.statusCode == 1) {
                                cardStatementList.onNext(it.cardStamentList)
                            } else {
                                error.onNext(it.status.statusCode)
                            }
                        }, {
                            it.printStackTrace()
                            error.onNext(1878)
                        }).addTo(subscriptions)
                }
                2 ->{
                    getCardDebitStatementForJuridicalCustomerUseCase
                        .execute(custId, compId, token, cardNumber!!, startDate, endDate)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            showProgress.onNext(false)
                            if (it.status.statusCode == 1) {
                                cardStatementList.onNext(it.cardStamentList)
                            } else {
                                error.onNext(it.status.statusCode)
                            }
                        }, {
                            it.printStackTrace()
                            error.onNext(1878)
                        }).addTo(subscriptions)
                }
            }

        } else {
            when (type){
                0 ->{
                    getCardStatementForIndividualCustomerUseCase
                        .execute(custId, token, cardNumber, startDate, endDate)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            showProgress.onNext(false)
                            if (it.status.statusCode == 1) {
                                cardStatementList.onNext(it.cardStamentList)
                            } else {
                                error.onNext(it.status.statusCode)
                            }
                        }, {
                            it.printStackTrace()
                            error.onNext(1878)
                        })
                        .addTo(subscriptions)
                }
                1 ->{
                    getCardCreditStatementForIndividualCustomerUseCase
                        .execute(custId, token, cardNumber, startDate, endDate)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            showProgress.onNext(false)
                            if (it.status.statusCode == 1) {
                                cardStatementList.onNext(it.cardStamentList)
                            } else {
                                error.onNext(it.status.statusCode)
                            }
                        }, {
                            it.printStackTrace()
                            error.onNext(1878)
                        })
                        .addTo(subscriptions)
                }
                2 ->{
                    getCardDebitStatementForIndividualCustomerUseCase
                        .execute(custId, token, cardNumber, startDate, endDate)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            showProgress.onNext(false)
                            if (it.status.statusCode == 1) {
                                cardStatementList.onNext(it.cardStamentList)
                            } else {
                                error.onNext(it.status.statusCode)
                            }
                        }, {
                            it.printStackTrace()
                            error.onNext(1878)
                        })
                        .addTo(subscriptions)
                }
            }

        }
    }

    override fun cardStatement(): Observable<ArrayList<CardStatementModel>> = cardStatementList
}