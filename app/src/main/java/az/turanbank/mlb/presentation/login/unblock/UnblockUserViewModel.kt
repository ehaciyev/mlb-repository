package az.turanbank.mlb.presentation.login.unblock

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.block.CheckUnblockIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.block.CheckUnblockJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.SendOTPCodeUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface UnblockUserViewModelInputs : BaseViewModelInputs {
    fun checkUnblockJuridical(pin: String, taxNo: String, mobile: String)

    fun checkUnblockIndividual(pin: String, mobile: String)
}

interface UnblockUserViewModelOutputs : BaseViewModelOutputs {
    fun onCheckingCustomerComplete(): PublishSubject<Long>
}

class UnblockUserViewModel @Inject constructor(
    private val checkUnblockIndividualUseCase: CheckUnblockIndividualUseCase,
    private val checkUnblockJuridicalUseCase: CheckUnblockJuridicalUseCase,
    private val sendOTPCodeUseCase: SendOTPCodeUseCase,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel(),
    UnblockUserViewModelInputs,
    UnblockUserViewModelOutputs {

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override val inputs: UnblockUserViewModelInputs = this

    override val outputs: UnblockUserViewModelOutputs = this

    private val verifyOtpCode = PublishSubject.create<Long>()
    var compId = 0L
    override fun checkUnblockIndividual(pin: String, mobile: String) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        checkUnblockIndividualUseCase.execute(pin, mobile,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    sendOtpCode(it.custId, mobile, it.compId)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun checkUnblockJuridical(pin: String, taxNo: String, mobile: String) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        checkUnblockJuridicalUseCase.execute(pin, taxNo, mobile,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    sendOtpCode(it.custId, mobile, it.compId)
                    compId = it.compId
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    private fun sendOtpCode(custId: Long, mobile: String, compId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        sendOTPCodeUseCase.execute(custId, compId, mobile,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when(it.status.statusCode) {
                    1 -> {
                        verifyOtpCode.onNext(custId)
                    }
                    else -> error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun onCheckingCustomerComplete() = verifyOtpCode
}