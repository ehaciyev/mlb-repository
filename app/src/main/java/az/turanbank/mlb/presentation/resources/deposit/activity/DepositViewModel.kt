package az.turanbank.mlb.presentation.resources.deposit.activity

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.deposit.DepositByIdResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetDepositByIdIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetDepositByIdJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetDepositCardNumbersIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.deposit.GetDepositCardNumbersJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface DepositViewModelInputs: BaseViewModelInputs{
    fun getTheDeposit(depositId: Long)
}
interface DepositViewModelOutputs: BaseViewModelOutputs{
    fun onDepositSuccess(): PublishSubject<DepositByIdResponseModel>
    fun onDepositCardSuccess(): PublishSubject<String>
}
class DepositViewModel @Inject constructor(
    private val getDepositByIdIndividualUseCase: GetDepositByIdIndividualUseCase,
    private val getDepositByIdJuridicalUseCase: GetDepositByIdJuridicalUseCase,
    private val getDepositCardNumbersIndividualUseCase: GetDepositCardNumbersIndividualUseCase,
    private val getDepositCardNumbersJuridicalUseCase: GetDepositCardNumbersJuridicalUseCase,
    sharedPrefs: SharedPreferences
    ) : BaseViewModel(),
    DepositViewModelInputs,
    DepositViewModelOutputs {

    override val inputs: DepositViewModelInputs = this

    override val outputs: DepositViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val deposit = PublishSubject.create<DepositByIdResponseModel>()
    private val cardNumber = PublishSubject.create<String>()

    override fun getTheDeposit(depositId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if (isCustomerJuridical) {
            getDepositByIdJuridicalUseCase
                .execute(custId, compId, depositId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        deposit.onNext(it)
                        getTheCard(depositId)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {

                }).addTo(subscriptions)
        } else {
            getDepositByIdIndividualUseCase
                .execute(custId, depositId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        deposit.onNext(it)
                        getTheCard(depositId)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {

                }).addTo(subscriptions)
        }
    }
    private fun getTheCard(depositId: Long){
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        if(isCustomerJuridical){
            getDepositCardNumbersJuridicalUseCase
                .execute(custId, compId, depositId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        cardNumber.onNext(it.cards[0].cardNumber)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getDepositCardNumbersIndividualUseCase
                .execute(custId, depositId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        cardNumber.onNext(it.cards[0].cardNumber)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onDepositSuccess() = deposit
    override fun onDepositCardSuccess() = cardNumber
}