package az.turanbank.mlb.presentation.ips.accounts.authchoice

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.data.SelectUnregisteredAccountModel
import kotlinx.android.synthetic.main.account_multi_select_list_view.view.*

class InstantPaymentAccountAuthSelectingAdapter(private val accountList: ArrayList<SelectUnregisteredAccountModel>,
                                                private val context: Context,
                                                private val onClickListener: (position: Int) -> Unit) :
    RecyclerView.Adapter<InstantPaymentAccountAuthSelectingAdapter.AccountViewHolder>() {


    class AccountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(account: SelectUnregisteredAccountModel, onClickListener: (position: Int) -> Unit, context: Context){
            if(account.checked){
                itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.checked_account))
                itemView.selectedAccount.isChecked = true
            } else {
                itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.unchecked_account))
                itemView.selectedAccount.isChecked = false
            }
            itemView.iban.text = account.account.iban
            itemView.currency.text = account.account.currName
            itemView.setOnClickListener { onClickListener.invoke(adapterPosition) }
            itemView.selectedAccount.setOnClickListener { onClickListener.invoke(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AccountViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.account_multi_select_list_view,
            parent,
            false
        )
    )

    override fun getItemCount() = accountList.size

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        holder.bind(accountList[position], onClickListener, context)
    }
}