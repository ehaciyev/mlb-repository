package az.turanbank.mlb.presentation.view;

import android.view.View;

public interface ColorListener{
    public void OnColorClick(View v, int color);
}