package az.turanbank.mlb.presentation.ips

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import az.turanbank.mlb.R
import java.util.*
import kotlin.collections.ArrayList


class BankListAdapter(
    private val ctx: Context,
    private val bankList: ArrayList<String>
) : ArrayAdapter<String>(ctx, 0, bankList) {

    val banks = ArrayList(bankList)

    private val filter = object : Filter() {

        @SuppressLint("DefaultLocale")
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filterResult = FilterResults()
            val suggestions: ArrayList<String> = ArrayList()
            if (constraint == null || constraint.isEmpty()) {
                suggestions.addAll(banks)
            } else {
                val filterPattern = constraint.toString().toLowerCase()

                for (bank in banks) {
                    if ( bank.contains(filterPattern)
                        || bank.startsWith(filterPattern)
                        || bank.toLowerCase(Locale.getDefault()).contains(filterPattern)) {
                        suggestions.add(bank)
                    }
                }
            }
            filterResult.values = suggestions
            filterResult.count = suggestions.size

            return filterResult
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            clear()
            addAll(results?.values as ArrayList<String>)
            notifyDataSetChanged()
        }
    }

    override fun getFilter() = filter
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val cV: View?

        return if (convertView == null) {
            cV = LayoutInflater.from(ctx).inflate(R.layout.bank_item_row, parent, false)

            val bankName = cV?.findViewById<TextView>(R.id.bankNameText)

            bankName?.text = bankList[position]

            cV
        } else {
            val bankName = convertView.findViewById<TextView>(R.id.bankNameText)

            bankName?.text = bankList[position]

            convertView
        }
    }
}