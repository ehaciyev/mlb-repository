package az.turanbank.mlb.presentation.ips.accounts.setdefault.alias

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.AliasAccountCompoundModel
import kotlinx.android.synthetic.main.ips_default_view_item.view.*


class ChooseAliasForDefaultAdapter(
    private val alias: ArrayList<AliasAccountCompoundModel>,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<ChooseAliasForDefaultAdapter.AliasSingleSelectViewHolder>() {

    class AliasSingleSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(model: AliasAccountCompoundModel, onClickListener: (position: Int) -> Unit) {
            itemView.aliasName.text = model.alias
            itemView.aliasTypeAndAccount.text = model.aliasType
            itemView.setOnClickListener {
                onClickListener.invoke(adapterPosition)
            }
            itemView.aliasTypeAndAccountIban.text = model.iban
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        AliasSingleSelectViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.ips_default_view_item, parent, false
            )
        )

    override fun getItemCount() = alias.size

    override fun onBindViewHolder(holder: AliasSingleSelectViewHolder, position: Int) {
        holder.bind(alias[position], onClickListener)
    }
}