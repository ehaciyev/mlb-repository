package az.turanbank.mlb.presentation.login.asan.juridical

import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.response.CompanyModel
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.home.MlbHomeActivity
import az.turanbank.mlb.presentation.register.asan.juridical.CompaniesListAdapter
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_juridical_register_companies_list.*
import javax.inject.Inject

class LoginAsanJuridicalCompaniesListActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewmodel: JuridialLoginCompaniesListViewModel

    private val pin: String by lazy { intent.getStringExtra("pin") }
    private val mobile: String by lazy { intent.getStringExtra("phoneNumber") }
    private val userId: String by lazy { intent.getStringExtra("asanId") }

    private var mCompanyList: ArrayList<CompanyModel> = arrayListOf()
    lateinit var mCompanyListAdapter: CompaniesListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_juridical_register_companies_list)

        viewmodel = ViewModelProvider(
            this,
            viewModelFactory
        )[JuridialLoginCompaniesListViewModel::class.java]

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        companiesRecyclerView.layoutManager = llm

        mCompanyListAdapter = CompaniesListAdapter(mCompanyList) {

            viewmodel.inputs.setSignLevel(mCompanyList[it].signLevel)

            viewmodel.inputs.setSignCount(mCompanyList[it].signCount)

            viewmodel.inputs.selectCustCompany(
                pin,
                mCompanyList[it].taxNo,
                mobile,
                userId,
                Build.MODEL,
                getString(R.string.azerbaijan)
            )
        }

        companiesRecyclerView.adapter = mCompanyListAdapter

        setupInputListeners()
        setupOutputListeners()
    }

    private fun setupOutputListeners() {
        viewmodel.outputs.companies().subscribe {
            setupRecyclerView(it)
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)

        viewmodel.outputs.custCompany().subscribe {
            val intent = Intent(this, MlbHomeActivity::class.java)

            intent.putExtra("pin", it.pin)
            intent.putExtra("custId", it.custId)
            intent.putExtra("loggedInWithAsan", true)

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        }.addTo(subscriptions)
    }

    private fun setupRecyclerView(companies: List<CompanyModel>) {
        mCompanyList.clear()
        mCompanyList.addAll(companies)
        companiesRecyclerView.adapter?.notifyDataSetChanged()
    }

    private fun setupInputListeners() {
        viewmodel.inputs.getCompaniesCert(pin, mobile, userId)
    }

    private fun showAlertDialog(stringResource: Int) {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.error_message))
            .setMessage(stringResource)
            .setPositiveButton(R.string.ok) { _, _ -> finish() }
            .setCancelable(true)
            .show()
    }
}