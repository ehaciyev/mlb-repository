package az.turanbank.mlb.presentation.resources.account

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.app.AlertDialog
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.response.DomesticOperationResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_operation_internal_result.*
import kotlinx.android.synthetic.main.add_to_templates_dialog_view.view.*
import javax.inject.Inject

class AccountOperationInternalResultActivity : BaseActivity() {

    lateinit var viewmodel: AccountOperationInternalResultViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var accountResponse: DomesticOperationResponseModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_operation_internal_result)

        accountResponse = intent.getSerializableExtra("accountResponse") as DomesticOperationResponseModel

        viewmodel = ViewModelProvider(
            this,
            factory
        )[AccountOperationInternalResultViewModel::class.java]

        if (!intent.getStringExtra("taxNo").isNullOrEmpty()) {
            opResultCrTaxContainer.visibility = View.VISIBLE
            opResultCrTax.text = intent.getStringExtra("taxNo")
        }

        opCrAccountName.text = intent.getStringExtra("accountName")
        dtAccount.text = intent.getStringExtra("dtIban")
        opAmount.text = intent.getStringExtra("amount")
        opCrAccountId.text = intent.getStringExtra("crIban")
        opResultOpNo.text = accountResponse.operationNo
        opStatus.text = accountResponse.operationStatus
        operationType.text = accountResponse.operationName
        opCurr.text = accountResponse.currency

        setOutputListeners()
        setInputListeners()

    }

    private fun setInputListeners() {
        opResultClose.setOnClickListener {
            finish()
        }

        saveTemp.setOnClickListener {
                showSaveTempEditText()
        }

        opResultShare.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, generateShareString())

            startActivity(Intent.createChooser(shareIntent, getString(R.string.send)))
        }
    }
    @SuppressLint("InflateParams")
    private fun showSaveTempEditText(){

        val alert = AlertDialog.Builder(this)
        alert.setTitle(getString(R.string.template_name))

        val view = LayoutInflater.from(this).inflate(R.layout.add_to_templates_dialog_view, null, false)
        alert.setView(view)

        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.save_all_caps)
        ) { _, _ ->
            if (!TextUtils.isEmpty(view.tempNameEditText.text.toString())) {
                animateProgressImage()
                viewmodel.inputs.saveTemplate(
                    view.tempNameEditText.text.toString(),
                    accountResponse.operationId)
//                viewmodel.inputs.saveTemplate(
//                    tempNameEditText.text.toString(),
//                    accountResponse
//                )
            }
        }

        alert.setNegativeButton(getString(R.string.cancel_camel_case)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }
    private fun generateShareString(): String {
        var string =
            "Status: " + opStatus.text.toString() + "\n" +
                    "Əməliyyatın tipi: Bankdaxili köçürmə" + "\n" +
                    "Əməliyyatın nömrəsi: " + intent.getStringExtra("operationNo") + "\n" +
                    "Alanın adı: " + opCrAccountName.text + "\n" +
                    "Alanın hesabı: " + opCrAccountId.text + "\n" +
                    "Vəsait tutulan hesab: " + dtAccount.text + "\n" +
                    "Məbləğ: " + opAmount.text + "\n" +
                    "Valyuta: " + opCurr.text + "\n"

        if (!intent.getStringExtra("taxNo").isNullOrEmpty()) {
            string = string + "Alanın VÖEN-i: " + intent.getStringExtra("taxNo")
        }

        return string
    }

    private fun setOutputListeners() {
        viewmodel.outputs.saveTemplateSuccess().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            AccountTransfersActivity().finish()
            val intent = Intent(this, AccountTransfersActivity::class.java)
            intent.putExtra("fromTemp", true)
            startActivity(intent)
            finish()
        }.addTo(subscriptions)
        viewmodel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            cardView.visibility = View.VISIBLE
            Toast.makeText(this, getString(ErrorMessageMapper().getErrorMessage(it)), Toast.LENGTH_LONG).show()
        }.addTo(subscriptions)
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        cardView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
