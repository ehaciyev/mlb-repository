package az.turanbank.mlb.presentation.ips.pay.debitorchoice.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.GetAccountAndCustomerInfoResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.IPSMLBAccountResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAccountsUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetAccountAndCustomerInfoUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface IPSAccountChoiceViewModelInputs : BaseViewModelInputs {
    fun getAccounts()
    fun getSenderInfo(iban: String)
}

interface IPSAccountChoiceViewModelOutputs : BaseViewModelOutputs {
    fun accountsSuccess(): PublishSubject<ArrayList<IPSMLBAccountResponseModel>>
    fun showProgress(): PublishSubject<Boolean>
    fun onSenderSuccess() : PublishSubject<GetAccountAndCustomerInfoResponseModel>
}

class IPSAccountChoiceViewModel @Inject constructor(
    private val getMLBAccountsUseCase: GetMLBAccountsUseCase,
    private val getAccountAndCustomerInfoUseCase: GetAccountAndCustomerInfoUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    IPSAccountChoiceViewModelInputs,
    IPSAccountChoiceViewModelOutputs {
    private val showProgress = PublishSubject.create<Boolean>()

    override fun showProgress() = showProgress
    override fun onSenderSuccess() = onSenderSuccess

    override val inputs: IPSAccountChoiceViewModelInputs = this

    override val outputs: IPSAccountChoiceViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private var selectedAccount: String? = null
    private var selectedAccountId: Long? = null

    private val onSenderSuccess = PublishSubject.create<GetAccountAndCustomerInfoResponseModel>()
    val accountList = PublishSubject.create<ArrayList<IPSMLBAccountResponseModel>>()

    override fun getAccounts() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        showProgress.onNext(true)
        getMLBAccountsUseCase
            .execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    accountList.onNext(it.accounts)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun getSenderInfo(iban: String) {
        getAccountAndCustomerInfoUseCase
            .execute(iban,null,null,null, null, custId,token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it.status.statusCode) {
                    1 -> {
                        onSenderSuccess.onNext(it)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            },{
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun accountsSuccess() = accountList

    fun setSelectedAccount(account: String?) {
        this.selectedAccount = account
    }

    fun setSelectedAccountId(accountId: Long?) {
        this.selectedAccountId = accountId
    }

    fun getSelectedAccountId() = this.selectedAccountId

    fun getSelectedAccount() = this.selectedAccount
}