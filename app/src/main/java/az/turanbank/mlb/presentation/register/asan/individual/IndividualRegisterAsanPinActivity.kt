package az.turanbank.mlb.presentation.register.asan.individual

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.register.asan.juridical.JuridicalRegisterCompaniesListActivity
import az.turanbank.mlb.presentation.register.mobile.individual.IndividualRegisterMobileSubmitActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_individual_register_asan_pin.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class IndividualRegisterAsanPinActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewmodel: IndividualRegisterAsanPinViewModel

    private val transactionId by lazy { intent.getLongExtra("transactionId", 0L) }
    private val challenge: String by lazy { intent.getStringExtra("challenge") }
    private val certificate: String by lazy { intent.getStringExtra("certificate") }
    private val custCode by lazy { intent.getLongExtra("custCode", 0L) }
    private val pin: String by lazy { intent.getStringExtra("pin") }
    private val authType: String by lazy { intent.getStringExtra("authType") }
    private val customerType: String by lazy { intent.getStringExtra("customerType") }

    private val phoneNumber: String by lazy { intent.getStringExtra("phoneNumber") }
    private val asanId: String by lazy { intent.getStringExtra("asanId") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual_register_asan_pin)

        verificationCode.setText(intent.getStringExtra("verificationCode"))
        toolbar_back_button.setOnClickListener { finish() }
        toolbar_title.text = getString(R.string.register_with_asan)
        viewmodel = ViewModelProvider(
            this,
            viewModelFactory
        )[IndividualRegisterAsanPinViewModel::class.java]

        showProgressBar(true)

        setupInputListeners()
        setupOutputListeners()
    }

    private fun setupOutputListeners() {
        viewmodel.outputs.registerIndividualAsan()
            .subscribe {
                val intent = Intent(this, IndividualRegisterMobileSubmitActivity::class.java)
                intent.putExtra("registrationType", "asan")
                intent.putExtra("custId", it)
                startActivity(intent)
                finish()
            }.addTo(subscriptions)

        viewmodel.outputs.onLogiIndividualAsan()
            .subscribe {
                if (customerType == "juridical") {
                    val intent = Intent(this@IndividualRegisterAsanPinActivity, JuridicalRegisterCompaniesListActivity::class.java)
                    intent.putExtra("phoneNumber", phoneNumber)
                    intent.putExtra("pin", pin)
                    intent.putExtra("asanId", asanId)
                    intent.putExtra("authType", authType)

                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(
                        this,
                        getString(R.string.individual_register_success),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }.addTo(subscriptions)
        viewmodel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setupInputListeners() {
        viewmodel.inputs.registerWithAsan(transactionId, certificate, challenge, custCode, pin)

        viewmodel.outputs.onError()
            .subscribe {
                val intent = Intent(this@IndividualRegisterAsanPinActivity, ErrorPageViewActivity::class.java)
                startActivity(intent)
                finish()
            }.addTo(subscriptions)
    }

    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if(show)
        {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
