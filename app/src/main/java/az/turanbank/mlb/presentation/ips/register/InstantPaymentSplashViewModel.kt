package az.turanbank.mlb.presentation.ips.register

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.AccountResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetUnregisteredIpsAccountsUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.CheckUserAccountsRegisteredUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.CheckUserRegisteredUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.IpsIndividualRegisterUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.RegisterAccountsIpsSystemUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.data.SelectUnregisteredAccountModel
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface InstantPaymentSplashViewModelInputs : BaseViewModelInputs {
    fun ipsIndividualRegister()
    fun checkUserRegistered()
    fun userRegisterExist()
    fun checkJuridicalUserRegistered()
    fun registerAccounts(accountId: ArrayList<Long>)
}

interface InstantPaymentSplashViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun userRegistered(): CompletableSubject
    fun userRegisteredDefault(): CompletableSubject
    fun userExist(): CompletableSubject
    fun customerIsJuridicalAndNotRegistered(): CompletableSubject
    fun customerIsIndividualButNoAccount(): CompletableSubject
    fun customerHasAll(): CompletableSubject
    fun accountsRegistered(): CompletableSubject
    fun userNotRegistered(): CompletableSubject
}

class InstantPaymentSplashViewModel @Inject constructor(
    private val getUnregisteredIpsAccountsUseCase: GetUnregisteredIpsAccountsUseCase,
    private val ipsIndividualRegisterUseCase: IpsIndividualRegisterUseCase,
    private val checkUserRegisteredUseCase: CheckUserRegisteredUseCase,
    private val checkUserAccountsRegisteredUseCase: CheckUserAccountsRegisteredUseCase,
    private val registerAccountsIpsSystemUseCase: RegisterAccountsIpsSystemUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    InstantPaymentSplashViewModelInputs,
    InstantPaymentSplashViewModelOutputs {
    override val inputs: InstantPaymentSplashViewModelInputs = this

    override val outputs: InstantPaymentSplashViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val showProgress = PublishSubject.create<Boolean>()

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val userRegistered = CompletableSubject.create()
    private val userRegisteredDefault = CompletableSubject.create()

    private val customerIsJuridicalAndNotRegistered = CompletableSubject.create()
    private val customerIsIndividualButNoAccount = CompletableSubject.create()
    private val customerHasAll = CompletableSubject.create()
    private val accountsRegistered = CompletableSubject.create()
    private val userNotRegistered = CompletableSubject.create()
    private val userExist = CompletableSubject.create()

    private val accountList = PublishSubject.create<ArrayList<SelectUnregisteredAccountModel>>()
    private val accounts = arrayListOf<SelectUnregisteredAccountModel>()

    override fun ipsIndividualRegister() {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN

        }
        ipsIndividualRegisterUseCase
            .execute(custId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    userRegistered.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun checkUserRegistered() {
        showProgress.onNext(true)
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN

        }
        checkUserRegisteredUseCase.exectue(custId, compId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    if (!isCustomerJuridical) {
                        checkAccountsRegistered()
                    } else {
                        customerHasAll.onComplete()
                    }
                } else if (it.status.statusCode == 115 && isCustomerJuridical) {
                    customerIsJuridicalAndNotRegistered.onComplete()
                } else if(it.status.statusCode == 115) {
                    userNotRegistered.onComplete()
                } else if(it.status.statusCode == 180) {
                    userExist.onComplete()
                }
                else{
                    error.onNext(it.status.statusCode)
                }
            }, {
                showProgress.onNext(false)
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun userRegisterExist() {
        showProgress.onNext(true)
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN

        }

        checkUserRegisteredUseCase.exectue(custId, compId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    userRegisteredDefault.onComplete()
                } else {
                    error.onNext(123456)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun checkJuridicalUserRegistered() {
        showProgress.onNext(true)
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN

        }

        checkUserRegisteredUseCase.exectue(custId,  compId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    customerHasAll.onComplete()
                }
                else if (it.status.statusCode == 115) {
                    customerIsJuridicalAndNotRegistered.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun registerAccounts(accountId: ArrayList<Long>) {
        registerAccountsIpsSystemUseCase
            .execute(accountId, custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    accountsRegistered.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    private fun checkAccountsRegistered() {
        checkUserAccountsRegisteredUseCase
            .execute(custId, null, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it.status.statusCode) {
                    1 -> {
                        customerHasAll.onComplete()
                    }
                    126 -> {
                        customerIsIndividualButNoAccount.onComplete()
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun showProgress() = showProgress

    override fun userRegistered() = userRegistered
    override fun userRegisteredDefault() = userRegisteredDefault



    override fun userExist() = userExist
    override fun customerIsJuridicalAndNotRegistered() = customerIsJuridicalAndNotRegistered
    override fun customerIsIndividualButNoAccount() = customerIsIndividualButNoAccount
    override fun customerHasAll() = customerHasAll
    override fun accountsRegistered() = accountsRegistered
    override fun userNotRegistered() = userNotRegistered
}