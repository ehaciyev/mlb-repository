package az.turanbank.mlb.presentation.ips.payrequest.sender.account

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.ips.banks.GetAllBanksUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ChooseIPSSenderAccountViewModelInputs: BaseViewModelInputs {
    fun getBankList()
}
interface ChooseIPSSenderAccountViewModelOutputs: BaseViewModelOutputs {
    fun onBankListSuccess(): PublishSubject<ArrayList<String>>
}
class ChooseIPSSenderAccountViewModel @Inject constructor(
    private val getAllBanksUseCase: GetAllBanksUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
    ChooseIPSSenderAccountViewModelInputs,
    ChooseIPSSenderAccountViewModelOutputs {
    override val inputs: ChooseIPSSenderAccountViewModelInputs = this

    override val outputs: ChooseIPSSenderAccountViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val bankList = PublishSubject.create<ArrayList<String>>()

    override fun getBankList() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getAllBanksUseCase
            .execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    val nameList = arrayListOf<String>()
                    it.bankInfos.forEach { bank ->
                        nameList.add(bank.name)
                    }
                    bankList.onNext(nameList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onBankListSuccess() = bankList

}