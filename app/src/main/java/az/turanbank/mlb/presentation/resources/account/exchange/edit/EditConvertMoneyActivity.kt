package az.turanbank.mlb.presentation.resources.account.exchange.edit

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.LinearLayout
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.widget.AppCompatEditText
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.exchange.CashlessResponseModel
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_buy_foreign_currency.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import javax.inject.Inject

class EditConvertMoneyActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: EditConvertViewModel

    private val foreignAccountList = arrayListOf<AccountListModel>()

    private val manatAccountList = arrayListOf<AccountListModel>()

    private val cashlessList = arrayListOf<CashlessResponseModel>()

    lateinit var dialog: MlbProgressDialog

    private val operId: Long by lazy { intent.getLongExtra("operId", 0L) }

    private val operTypeId: Int by lazy { intent.getIntExtra("operTypeId", 0) }

    private val decimalFormat = DecimalFormat("0.00")
    private val symbols = DecimalFormatSymbols()
    private var buyFocus = false
    private var sellFocus = false
    private var rateFocus = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(operTypeId == 5) {
            setContentView(R.layout.activity_edit_convert_money)
        } else if (operTypeId == 6) {
            setContentView(R.layout.activity_edit_convert_money_sell)
        }

        viewModel =
            ViewModelProvider(this, factory)[EditConvertViewModel::class.java]
        symbols.decimalSeparator = '.'
        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        decimalFormat.decimalFormatSymbols = symbols

        dialog = MlbProgressDialog(this)
        dialog.showDialog()

        setOutputListeners()
        setInputListeners()

        setFocusAndWatcherListeners()

        toolbar_title.text = getString(R.string.convertation)

        toolbar_back_button.setOnClickListener { onBackPressed() }

    }

    private fun setFocusAndWatcherListeners() {

        buyAmount.setOnFocusChangeListener { _, hasFocus ->
            buyFocus = hasFocus
        }

        sellAmount.setOnFocusChangeListener { _, hasFocus ->
            sellFocus = hasFocus
        }
        customExchange.setOnFocusChangeListener { _, hasFocus ->
            rateFocus = hasFocus
        }


        buyAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (buyFocus) {
                    makeExchange(s.toString(), 0)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        customExchange.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (rateFocus) {
                    changeRate(s)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        sellAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (sellFocus) {
                    makeExchange(s.toString(), 1)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

    }

    private fun changeRate(rateAmount: Editable?) {
        if (!rateAmount.toString().trim().isNullOrEmpty()
            && rateAmount.toString().substring(0, 1) != "."
            && rateAmount.toString().substring(0, 1) != ","
            && rateAmount.toString().toDouble() > 0
            && !buyAmount.text.toString().trim().isNullOrEmpty()
            && !sellAmount.text.toString().trim().isNullOrEmpty()
        ) {
            viewModel.inputs.changeRate(
                buyAmount.text.toString().toDouble(),
                sellAmount.text.toString().toDouble(),
                rateAmount.toString().toDouble()
            )
        }
    }

    private fun makeExchange(amount: String, convertType: Int) {
        if (!TextUtils.isEmpty(amount) && customExchange.text.toString().isNotEmpty())
            viewModel.inputs.convertCurrency(
                amount.toDouble(),
                customExchange.text.toString().toDouble(),
                convertType
            )
    }

    private fun setInputListeners() {
        viewModel.inputs.getOperationById(operId)

        viewModel.inputs.getAccountList()

        submitButton.setOnClickListener {
            if (buyAmount.text.toString().trim().isNotEmpty()
                && sellAmount.text.toString().trim().isNotEmpty()
                && customExchange.text.toString().trim().isNotEmpty()
            ) {
                viewModel.inputs.updateOperationById(
                    viewModel.getAccountIdByIban(buyAccount.text.toString().split(" ")[0])!!,
                    buyAmount.text.toString().toDouble(),
                    sellAmount.text.toString().toDouble(),
                    sellAccount.text.toString().split(" ")[0],
                    viewModel.getBranchIdByIban(buyAccount.text.toString().split(" ")[0])!!,
                    viewModel.getBranchIdByIban(sellAccount.text.toString().split(" ")[0])!!,
                    customExchange.text.toString().toDouble(),
                    operTypeId,
                    operId,
                    4L
                )
            } else {
                checkAmounts()
            }
        }

    }

    private fun checkAmounts() {
        buyAmount.error = if (buyAmount.text.toString().trim().isEmpty()) {
            getString(R.string.enter_amount)
        } else {
            null
        }
        sellAmount.error = if (sellAmount.text.toString().trim().isEmpty()) {
            getString(R.string.enter_amount)
        } else {
            null
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.operationUpdated().subscribe {
            Toast.makeText(this, getString(R.string.convertation_operation_successfully_updated), Toast.LENGTH_LONG).show()
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)

        viewModel.outputs.operationDetails().subscribe {
            if (it.operTypeId == 5) {
                sellAmount.setText(it.crAmt.toString())
                sellAccount.setText(it.crIban)
                sellCurrency.setText(it.crCcy)

                buyAmount.setText(it.dtAmt.toString())
                buyAccount.setText(it.dtIban)
                buyCurrency.setText(it.dtCcy)
            } else if (operTypeId == 6){
                sellAmount.setText(it.dtAmt.toString())
                sellAccount.setText(it.dtIban)
                sellCurrency.setText(it.dtCcy)

                buyAmount.setText(it.crAmt.toString())
                buyAccount.setText(it.crIban)
                buyCurrency.setText(it.crCcy)
            }

            customExchange.setText(it.custExchRate.toString())

        }.addTo(subscriptions)

        viewModel.outputs.onConvertGot().subscribe {
            sellAmount.setText(decimalFormat.format(it).toString())
        }.addTo(subscriptions)
        viewModel.outputs.onRevertGot().subscribe {
            buyAmount.setText(decimalFormat.format(it).toString())
        }.addTo(subscriptions)
        viewModel.outputs.onCashlessGot().subscribe { cashlessList ->
            this.cashlessList.clear()
            this.cashlessList.addAll(cashlessList)
            for (i in 0 until cashlessList.size) {
                if (viewModel.sellAccountList.value!!.currName == cashlessList[i].currency) {
                    if (operTypeId == 5) {
                        customExchange.setText(cashlessList[i].cashlessSell.toString())
                    } else if (operTypeId == 6) {
                        customExchange.setText(cashlessList[i].cashlessBuy.toString())
                    }
                }
            }
        }.addTo(subscriptions)
        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)
        viewModel.outputs.onAccountSuccess().subscribe { accountListModel ->

            manatAccountList.clear()
            foreignAccountList.clear()
            for (i in 0 until accountListModel.size) {
                if (accountListModel[i].currName == "AZN") {
                    manatAccountList.add(accountListModel[i])
                } else {
                    foreignAccountList.add(accountListModel[i])
                }
            }

            if (manatAccountList.isNotEmpty())
                setAccount(buyAccount, buyAccountContainer, buyCurrency, manatAccountList, false)

            if (foreignAccountList.isNotEmpty())
                setAccount(
                    sellAccount,
                    sellAccountContainer,
                    sellCurrency,
                    foreignAccountList,
                    true
                )

            //setCurrencies()
            viewModel.buyAccountList.onNext(viewModel.getAccountByIban(buyAccount.text.toString())!!)
            viewModel.sellAccountList.onNext(viewModel.getAccountByIban(sellAccount.text.toString())!!)
        }.addTo(subscriptions)
    }

    @SuppressLint("SetTextI18n")
    private fun setCurrencies() {
        viewModel.buyAccountList.subscribe {
            buyCurrency.setText(it.currName)
            buyAccount.setText(it.iban + " / " + it.currName)
        }.addTo(subscriptions)
        viewModel.sellAccountList.subscribe {
            sellCurrency.setText(it.currName)
            sellAccount.setText(it.iban + " / " + it.currName)
        }.addTo(subscriptions)
    }

    private fun setAccount(
        account: AppCompatEditText,
        accountContainer: LinearLayout,
        currency: AppCompatEditText,
        accountList: ArrayList<AccountListModel>,
        changeRate: Boolean
    ) {
        accountContainer.setOnClickListener {
            val accountNameArray = arrayOfNulls<String>(accountList.size)
            val accountCurrencyArray = arrayOfNulls<String>(accountList.size)
            val accountDialog: AlertDialog.Builder =
                AlertDialog.Builder(this)
            accountDialog.setTitle(getString(R.string.select_account))

            for (i in 0 until accountList.size) {
                accountNameArray[i] = (accountList[i].iban + " / " + accountList[i].currName)
                accountCurrencyArray[i] = (accountList[i].currName)
            }
            accountDialog.setItems(accountNameArray) { _, which ->
                account.setText(accountNameArray[which])
                currency.setText(accountCurrencyArray[which])
                try {
                    if (changeRate) {
                        for (i in 0..cashlessList.size) {
                            if (cashlessList[i].currency == accountCurrencyArray[which]) {
                                if (operTypeId == 5) {
                                    customExchange.setText(cashlessList[i].cashlessSell.toString())
                                    sellAmount.setText(decimalFormat.format(buyAmount.text.toString().toDouble() / cashlessList[i].cashlessSell))
                                } else if (operTypeId == 6) {
                                    customExchange.setText(cashlessList[i].cashlessBuy.toString())
                                    buyAmount.setText(decimalFormat.format(sellAmount.text.toString().toDouble() * cashlessList[i].cashlessBuy))
                                }
                                break
                            }
                        }
                    }
                } catch (exc: Exception) {
                    exc.printStackTrace()
                }
            }
            val dialog = accountDialog.create()
            dialog.show()
        }
    }
}