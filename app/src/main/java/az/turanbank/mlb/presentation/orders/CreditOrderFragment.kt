package az.turanbank.mlb.presentation.orders

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.activity.SuccessfulPageViewActivity
import az.turanbank.mlb.presentation.base.BaseFragment
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_credit_order.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class CreditOrderFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: CreditOrderViewModel
    lateinit var dialog: MlbProgressDialog
    private var selectedBranchId: Long = 0L
    private var selectedOrderKindId: Long = 0L
    private var selectedOrderKindTypeId: Long? = null
    private var selectedOrderPeriodId: Int = 0

    private var selectedDuration: Int = 1
    private lateinit var creditDuration: EditText
    private lateinit var orderKind: EditText
    private lateinit var orderAmount: EditText
    private lateinit var orderCurrency: EditText
    private lateinit var orderKindType: EditText
    private lateinit var branch: EditText
    private lateinit var submitText: TextView
    private lateinit var submitButton: LinearLayout
    private lateinit var creditDurationDialog: AlertDialog
    private lateinit var orderKindIcon: ImageView
    private lateinit var orderTypeIcon: ImageView
    private lateinit var orderKindProgress: ImageView
    private lateinit var orderTypeProgress: ImageView
    private lateinit var orderPeriodProgress: ImageView
    private lateinit var currencyProgress: ImageView
    private lateinit var orderKindContainer: LinearLayout
    private lateinit var creditDurationContainer: LinearLayout
    private lateinit var orderCurrencyContainer: LinearLayout
    private lateinit var orderAmountContainer: LinearLayout
    private lateinit var orderTypeContainer: LinearLayout
    private lateinit var selectedCurrency: String
    private lateinit var textWatcher: TextWatcher

    private var orderType: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            orderType = it.getInt("orderType")
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_credit_order, container, false)

        viewmodel = ViewModelProvider(this, factory)[CreditOrderViewModel::class.java]

        activity?.toolbar_title?.text = getString(R.string.orders)

        dialog =
            MlbProgressDialog(requireActivity())
        creditDurationContainer = view.findViewById(R.id.creditDurationContainer)
        creditDuration = view.findViewById(R.id.creditDuration)
        orderKindIcon = view.findViewById(R.id.orderKindIcon)
        orderTypeIcon = view.findViewById(R.id.orderTypeIcon)
        orderKindContainer = view.findViewById(R.id.orderKindContainer)
        orderTypeContainer = view.findViewById(R.id.orderTypeContainer)
        orderAmountContainer = view.findViewById(R.id.orderAmountContainer)
        orderCurrencyContainer = view.findViewById(R.id.orderCurrencyContainer)
        orderKind = view.findViewById(R.id.orderKind)
        orderKindType = view.findViewById(R.id.orderType)
        branch = view.findViewById(R.id.branch)
        submitText = view.findViewById(R.id.submitText)
        orderAmount = view.findViewById(R.id.orderAmount)
        submitButton = view.findViewById(R.id.submitButton)
        orderKindProgress = view.findViewById(R.id.orderKindProgress)
        orderTypeProgress = view.findViewById(R.id.orderTypeProgress)
        currencyProgress = view.findViewById(R.id.currencyProgress)
        orderPeriodProgress = view.findViewById(R.id.orderPeriodProgress)
        orderCurrency = view.findViewById(R.id.orderCurrency)

        if (orderType == 3) {
            orderKind.hint = getString(R.string.card_kind)
            orderTypeContainer.visibility = View.VISIBLE
            orderKindIcon.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.credit_card_dark
                )
            )
            orderTypeIcon.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.credit_card_dark
                )
            )
            viewmodel.inputs.getOrderKindTypeList(orderType)
        } else {
            orderAmountContainer.visibility = View.VISIBLE
        }

        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (orderAmountContainer.visibility == View.VISIBLE) {
                    if (orderAmount.text.toString().trim().isNotEmpty() &&
                        !orderKind.text.isNullOrEmpty() &&
                        !orderCurrency.text.isNullOrEmpty() &&
                        !branch.text.isNullOrEmpty() &&
                        !creditDuration.text.isNullOrEmpty() &&
                        (orderTypeContainer.visibility == View.GONE || !orderKindType.text.isNullOrEmpty())
                    ) {

                        buttonEnabled(true)

                        submitButton.setOnClickListener {
                            var amount = 0.0
                            if (orderAmountContainer.visibility == View.VISIBLE) {
                                amount = orderAmount.text.toString().trim().toDouble()
                            }

                            viewmodel.inputs.createOrder(
                                selectedBranchId,
                                orderType,
                                selectedOrderKindId,
                                selectedOrderPeriodId,
                                amount,
                                selectedCurrency
                            )
                        }
                    } else {
                        buttonEnabled(false)
                    }
                } else {
                    if (!orderKind.text.isNullOrEmpty() &&
                        !orderCurrency.text.isNullOrEmpty() &&
                        !branch.text.isNullOrEmpty() &&
                        !creditDuration.text.isNullOrEmpty() &&
                        !orderKindType.text.isNullOrEmpty()
                    ) {

                        buttonEnabled(true)

                        submitButton.setOnClickListener {
                            var amount = 0.0
                            if (orderAmountContainer.visibility == View.VISIBLE) {
                                amount = orderAmount.text.toString().trim().toDouble()
                            }

                            viewmodel.inputs.createOrder(
                                selectedBranchId,
                                orderType,
                                selectedOrderKindId,
                                selectedOrderPeriodId,
                                amount,
                                selectedCurrency
                            )
                        }
                    } else {
                        buttonEnabled(false)
                    }
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
            }
        }

        orderAmount.addTextChangedListener(textWatcher)
        orderKind.addTextChangedListener(textWatcher)
        creditDuration.addTextChangedListener(textWatcher)
        orderCurrency.addTextChangedListener(textWatcher)
        branch.addTextChangedListener(textWatcher)

        setOutputListeners()
        setInputListeners()

        return view
    }

    private fun initCreditDurationDialog(durationArray: Array<String>) {
        creditDurationDialog = creditDurationDialog(durationArray)
        creditDurationContainer.setOnClickListener {
            creditDurationDialog.show()
        }
        creditDuration.setOnClickListener {
            creditDurationDialog.show()
        }
    }

    private fun buttonEnabled(show: Boolean) {

        if (!show) {
            submitButton.isClickable = false
            submitButton.isEnabled = false
            submitButton.isFocusable = false
            submitButton.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.light_gray
                )
            )
            submitText.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        } else {
            submitButton.isClickable = true
            submitButton.isEnabled = true
            submitButton.isFocusable = true
            submitButton.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimary
                )
            )
            submitText.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        }
    }

    private fun creditDurationDialog(durationArray: Array<String>): AlertDialog {
        val b: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
        b.setTitle(getString(R.string.select_credit_period))

        b.setItems(durationArray) { _, which ->
            branch.setText(durationArray[which])
            selectedDuration = which + 1
        }
        return b.create()
    }

    private fun setInputListeners() {
        if(orderType == 1)        viewmodel.inputs.getOrderPeriodList(orderType, selectedOrderKindTypeId)

        viewmodel.inputs.getBranchList()
        viewmodel.inputs.getCurrencies()
        if (orderType != 3) viewmodel.inputs.getOrderKindList(
            orderType,
            selectedOrderKindTypeId
        )
    }

    private fun setOutputListeners() {
        viewmodel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.branchList().subscribe { branchList ->
            branch.setText(branchList[0].branchName)
            selectedBranchId = branchList[0].branchId

            branchContainer.setOnClickListener {
                val branchNameArray = arrayOfNulls<String>(branchList.size)

                val b: AlertDialog.Builder = AlertDialog.Builder(requireActivity())

                b.setTitle(getString(R.string.select_branch))

                for (i in 0 until branchList.size) {
                    branchNameArray[i] = branchList[i].branchName
                }

                b.setItems(branchNameArray) { _, which ->
                    branch.setText(branchList[which].branchName)
                    selectedBranchId = branchList[which].branchId
                }

                val dialog = b.create()
                dialog.show()
            }

        }.addTo(subscriptions)

        viewmodel.outputs.showOrderKindListProgress().subscribe {
            showProgressImages(orderKindProgress, it)
        }.addTo(subscriptions)

        viewmodel.outputs.orderKindList().subscribe { orderKindList ->
            orderKind.setText(orderKindList[0].name)

            selectedOrderKindId = orderKindList[0].id

            orderKind.setOnClickListener {
                val orderKindNameArray = arrayOfNulls<String>(orderKindList.size)

                val b: AlertDialog.Builder = AlertDialog.Builder(requireActivity())

                if(orderType == 1) {
                    b.setTitle(getString(R.string.select_credit_kind))
                } else {
                    b.setTitle(getString(R.string.select_card_kind))
                }

                for (i in 0 until orderKindList.size) {
                    orderKindNameArray[i] = orderKindList[i].name
                }

                b.setItems(orderKindNameArray) { _, which ->
                    orderKind.setText(orderKindList[which].name)
                    selectedOrderKindId = orderKindList[which].id
                }

                val dialog = b.create()
                dialog.show()
            }

            orderKindContainer.setOnClickListener {
                val orderKindNameArray = arrayOfNulls<String>(orderKindList.size)

                val b: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
                if(orderType == 1) {
                    b.setTitle(getString(R.string.select_credit_kind))
                } else {
                    b.setTitle(getString(R.string.select_card_kind))
                }
                for (i in 0 until orderKindList.size) {
                    orderKindNameArray[i] = orderKindList[i].name
                }

                b.setItems(orderKindNameArray) { _, which ->
                    orderKind.setText(orderKindList[which].name)
                    selectedOrderKindId = orderKindList[which].id
                }

                val dialog = b.create()
                dialog.show()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.getCurrenciesProgress().subscribe {
            showProgressImages(currencyProgress, it)
        }.addTo(subscriptions)

        viewmodel.outputs.currencyListSuccess().subscribe { currencyList ->
            selectedCurrency = currencyList[0]
            orderCurrency.setText(currencyList[0])

            orderCurrencyContainer.setOnClickListener {
                val currencyNameArray = arrayOfNulls<String>(currencyList.size)

                val b: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
                b.setTitle(getString(R.string.currency))

                for (i in 0 until currencyList.size) {
                    currencyNameArray[i] = currencyList[i]
                }

                b.setItems(currencyNameArray) { _, which ->
                    orderCurrency.setText(currencyList[which])
                    selectedCurrency = currencyList[which]
                }

                val dialog = b.create()
                dialog.show()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.createOrderSuccess().subscribe {
            if (it) {
                val intent =
                    Intent(requireActivity(), SuccessfulPageViewActivity::class.java)
                intent.putExtra(
                    "description",
                    getString(R.string.orders_operation_success)
                )
                intent.putExtra("finishOnly", true)
                startActivity(intent)
                activity?.finish()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.createOrderError().subscribe {
            val intent =
                Intent(requireActivity(), ErrorPageViewActivity::class.java)
            intent.putExtra(
                "description",
                getString(R.string.error_message)
            )
            startActivity(intent)
            activity?.finish()

        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            AlertDialogMapper(
                requireActivity(),
                it
            ).showAlertDialog()
        }.addTo(subscriptions)

        viewmodel.outputs.orderKindTypeListProgress().subscribe {
            showProgressImages(orderTypeProgress, it)
        }.addTo(subscriptions)

        viewmodel.outputs.orderPeriodProgress().subscribe {
            showProgressImages(orderPeriodProgress, it)
        }.addTo(subscriptions)

        viewmodel.outputs.orderPeriodList().subscribe { orderPeriodList ->
            creditDuration.setText(orderPeriodList[0].name)
            selectedOrderPeriodId = orderPeriodList[0].id.toInt() //TODO

            creditDurationContainer.setOnClickListener {
                val periodNameArray = arrayOfNulls<String>(orderPeriodList.size)

                val b: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
                b.setTitle(getString(R.string.select))

                for (i in 0 until orderPeriodList.size) {
                    periodNameArray[i] = orderPeriodList[i].name
                }

                b.setItems(periodNameArray) { _, which ->
                    creditDuration.setText(orderPeriodList[which].name)
                    selectedOrderPeriodId = orderPeriodList[which].id.toInt() //TODO
                }

                val dialog = b.create()
                dialog.show()
            }


        }.addTo(subscriptions)

        viewmodel.outputs.orderKindTypeList().subscribe { orderKindTypeList ->
            orderKindType.setText(orderKindTypeList[0].name)
            selectedOrderKindTypeId = orderKindTypeList[0].id

            //CREDIT
            if (orderType == 3) {
                if (selectedOrderKindTypeId == 2L) {
                    //make amount visible
                    orderAmountContainer.visibility = View.VISIBLE
                    //  initCreditDurationDialog(orderDurationArrayWit2Years)

                    // make period 1 year & 2 years
                } else {
                    orderAmountContainer.visibility = View.GONE
                }
            } else {
                orderAmountContainer.visibility = View.VISIBLE
            }
            viewmodel.inputs.getOrderKindList(orderType, selectedOrderKindTypeId)
            viewmodel.inputs.getOrderPeriodList(orderType, selectedOrderKindTypeId)

            orderTypeContainer.setOnClickListener {
                val orderTypeNameArray = arrayOfNulls<String>(orderKindTypeList.size)

                val b: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
                if(orderType == 1) {
                    b.setTitle(getString(R.string.select_credit_type))
                } else {
                    b.setTitle(getString(R.string.select_card_type))
                }

                for (i in 0 until orderKindTypeList.size) {
                    orderTypeNameArray[i] = orderKindTypeList[i].name
                }

                b.setItems(orderTypeNameArray) { _, which ->
                    orderKindType.setText(orderKindTypeList[which].name)
                    selectedOrderKindTypeId = orderKindTypeList[which].id

                    if (orderType == 3) {
                        if (selectedOrderKindTypeId == 2L) {
                            //make amount visible
                            orderAmountContainer.visibility = View.VISIBLE
                            // make period 1 year & 2 years
                        } else {
                            orderAmountContainer.visibility = View.GONE
                        }

                    }
                    viewmodel.inputs.getOrderKindList(orderType, selectedOrderKindTypeId)
                    viewmodel.inputs.getOrderPeriodList(orderType, selectedOrderKindTypeId)
                }

                val dialog = b.create()
                dialog.show()
            }

            orderKindType.setOnClickListener {
                val orderTypeNameArray = arrayOfNulls<String>(orderKindTypeList.size)

                val b: AlertDialog.Builder = AlertDialog.Builder(requireActivity())

                if(orderType == 1) {
                    b.setTitle(getString(R.string.select_credit_type))
                } else {
                    b.setTitle(getString(R.string.select_card_type))
                }
                for (i in 0 until orderKindTypeList.size) {
                    orderTypeNameArray[i] = orderKindTypeList[i].name
                }

                b.setItems(orderTypeNameArray) { _, which ->
                    orderKindType.setText(orderKindTypeList[which].name)
                    selectedOrderKindTypeId = orderKindTypeList[which].id

                    if (orderType == 3) {
                        if (selectedOrderKindTypeId == 2L) {
                            //make amount visible
                            orderAmountContainer.visibility = View.VISIBLE
                            // make period 1 year & 2 years
                        } else {
                            orderAmountContainer.visibility = View.GONE
                        }
                    }
                    viewmodel.inputs.getOrderKindList(orderType, selectedOrderKindTypeId)
                    viewmodel.inputs.getOrderPeriodList(orderType, selectedOrderKindTypeId)
                }

                val dialog = b.create()
                dialog.show()
            }
        }.addTo(subscriptions)
    }

    private fun showProgressImages(progressImage: ImageView, show: Boolean) {
        hideProgressImages(progressImage, show)

        if (show) {
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }

    private fun revertProgressImageAnimation(progressImage: ImageView) {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage(progressImage: ImageView) {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun hideProgressImages(progressImage: ImageView, hide: Boolean) {
        if (hide) {
            progressImage.visibility = View.GONE
        } else {
            progressImage.visibility = View.VISIBLE
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            CreditOrderFragment()

        @JvmStatic
        fun newInstance(orderType: Int) =
            CreditOrderFragment().apply {
                arguments = Bundle().apply {
                    putInt("orderType", orderType)
                }
            }
    }
}
