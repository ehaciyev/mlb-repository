package az.turanbank.mlb.presentation.payments

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.payments.pay.categories.PaymentCategoriesFragment
import az.turanbank.mlb.presentation.payments.template.PaymentTemplatesFragment
import az.turanbank.mlb.presentation.payments.history.PaymentHistoryFragment

private val TAB_TITLES = arrayOf(
    R.string.new_keyword,
    R.string.templates,
    R.string.history
)


private val TAB_FRAGMENTS = arrayOf(
    PaymentCategoriesFragment(),
    PaymentTemplatesFragment(),
    PaymentHistoryFragment()
)

class PaymentsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int) = TAB_FRAGMENTS[position]

    override fun getCount() = 3

    override fun getPageTitle(position: Int) = context.getString(TAB_TITLES[position])

}