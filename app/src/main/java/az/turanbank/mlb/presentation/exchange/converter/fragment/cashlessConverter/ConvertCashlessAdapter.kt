package az.turanbank.mlb.presentation.exchange.converter.fragment.cashlessConverter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ExchangeFlagMapper
import az.turanbank.mlb.data.remote.model.exchange.CashlessResponseModel
import az.turanbank.mlb.presentation.exchange.converter.fragment.cashConverter.ExchangeFlagMapperConverter
import kotlinx.android.synthetic.main.turan_currency_list_item.view.currency_icon
import kotlinx.android.synthetic.main.turan_currency_list_item.view.currency_name

class ConvertCashlessAdapter (private val context: Context, private val currencyList: ArrayList<CashlessResponseModel>, private val clickListener: (position: Int) -> Unit) : RecyclerView.Adapter<ConvertCashlessAdapter.CurrencyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CurrencyViewHolder = CurrencyViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.convert_currency_item_view,
            parent,
            false))

    override fun getItemCount() = currencyList.size

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencyList[position], clickListener)
    }
    class CurrencyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(currency: CashlessResponseModel, clickListener: (position: Int) -> Unit) {
            with(currency) {
                itemView.isClickable = true
                itemView.currency_name.text = this.currency
                itemView.currency_icon.setImageResource(ExchangeFlagMapperConverter().resolveFlag(this.currency))
                itemView.setOnClickListener{
//                    itemView.setBackgroundColor(R.color.selectedRecyclerViewItemColor)
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}