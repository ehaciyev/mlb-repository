package az.turanbank.mlb.presentation.cardoperations.templates


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.cardoperations.CardTemplateModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class CardTemplateListFragment : BaseFragment() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardTemplateListViewModel

    private var templateList = arrayListOf<CardTemplateModel>()

    private lateinit var recyclerView: RecyclerView
    private lateinit var templateAdapter: CardTemplateListAdapter

    private lateinit var progress: ImageView
    private lateinit var failMessage: TextView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_templates, container, false)

        viewModel =
            ViewModelProvider(this, factory)[CardTemplateListViewModel::class.java]

        recyclerView = view.findViewById(R.id.recyclerView)
        progress = view.findViewById(R.id.progress)
        failMessage = view.findViewById(R.id.failMessage)

        makeRecyclerView()

        setOutputListeners()

        setInputListeners()

        return view
    }

    private fun makeRecyclerView() {
        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm

        templateAdapter = CardTemplateListAdapter(templateList){
            startActivity(viewModel.getIntentWithData(requireActivity(), templateList[it]))
        }

        recyclerView.adapter = templateAdapter

    }

    override fun onResume() {
        super.onResume()
        viewModel.inputs.getTemplateList()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onTemplateListSuccess().subscribe{
            failMessage.visibility = View.GONE
            progress.clearAnimation()
            progress.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            updateRecyclerView(it)
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            recyclerView.visibility = View.GONE
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
            failMessage.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    private fun updateRecyclerView(templateList: ArrayList<CardTemplateModel>) {
        this.templateList.clear()
        this.templateList.addAll(templateList)
        recyclerView.adapter?.notifyDataSetChanged()
    }

    private fun setInputListeners() {
        animateProgressImage()
        viewModel.inputs.getTemplateList()
    }
    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }

}
