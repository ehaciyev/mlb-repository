package az.turanbank.mlb.presentation.ips.accounts.delete

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.ips.request.AliasAndAccountRequestModel
import az.turanbank.mlb.data.remote.model.ips.response.IPSMLBAccountResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.WarningPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.aliases.link.account.SelectAccountLinkingToAliasActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_delete_ips_account.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.toolbar_back_button
import kotlinx.android.synthetic.main.mlb_toolbar_layout.toolbar_title
import javax.inject.Inject

class DeleteIpsAccountActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: DeleteIpsAccountViewModel

    lateinit var adapter: DeleteIpsAccountAdapter

    private val accounts = arrayListOf<IPSMLBAccountResponseModel>()

    private var selectedAccountId = arrayListOf<Long>()

    private var deletedAccountId = arrayListOf<Long>()
    private var deletedAccountIban = ""

    var ibans = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_ips_account)

        viewModel =
            ViewModelProvider(this, factory)[DeleteIpsAccountViewModel::class.java]

        initViews()

        adapter = DeleteIpsAccountAdapter(applicationContext, accounts) {
            accounts[it].isSelected = !accounts[it].isSelected
            adapter.notifyDataSetChanged()
        }

        recyclerView.adapter = adapter

        //animateProgress()

        setOutputListeners()
        setInputListeners()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                viewModel.inputs.deleteAccount(deletedAccountId)
            }
        }
    }

    private fun setInputListeners() {
        viewModel.inputs.getAccounts()
        submitButton.setOnClickListener {
            var ibans = ""
            var isSelected = false
            val list = arrayListOf<IPSMLBAccountResponseModel>()
            deletedAccountId = arrayListOf()
            accounts.forEach {
                if (it.isSelected) {
                    isSelected = true
                    list.add(it)
                    deletedAccountId.add(it.id)
                }
            }

            if (deletedAccountId.size != 0) {
                showProgressBar(true)
                viewModel.inputs.deleteAccount(deletedAccountId)
            } else {
                AlertDialogMapper(this, 8000).showAlertDialog()
            }
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.accountDelete().subscribe {
            Toast.makeText(
                this,
                ErrorMessageMapper().getErrorMessage(124010),
                Toast.LENGTH_LONG
            ).show()

            showProgressBar(false)
            viewModel.inputs.getAccounts()

        }.addTo(subscriptions)

        viewModel.outputs.unSuccessFullyDeletedAccount().subscribe {
            showProgressBar(false)

            it.forEach { dlt ->
                selectedAccountId.add(dlt.accountId)
            }

            for(i in 0 until accounts.size){
                for (j in 0 until selectedAccountId.size){
                    if(accounts[i].id == selectedAccountId[j])
                        ibans =  accounts[i].iban + "\n" + ibans
                }
            }

            AlertDialog.Builder(this)
                .setTitle(R.string.delete_error)
                .setMessage(ibans)
                .setPositiveButton(R.string.ok) { _, _ ->
                    viewModel.inputs.deleteAccount(deletedAccountId)
                }
                .setCancelable(false)
                .show()
        }.addTo(subscriptions)
        viewModel.outputs.onAccounts().subscribe {
            accounts.clear()
            accounts.addAll(it)
            adapter.notifyDataSetChanged()
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            //progress.clearAnimation()
            //progress.visibility = View.GONE
            showProgressBar(false)
            if(it == 126){
                message.visibility = View.GONE
                recyclerView.visibility = View.GONE
                submitButton.visibility = View.GONE
                noDataAvailable.visibility = View.VISIBLE
            } else {
                AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
            }

        }.addTo(subscriptions)
    }


    override fun onPause() {
        super.onPause()
        progressImage.clearAnimation()
        progressImage.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
    }


    private fun initViews() {
        toolbar_title.text = getString(R.string.delete_account)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
    }

    private fun animateProgressImage() {
        progressImage.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            animateProgressImage()
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            progressImage.clearAnimation()
        }
    }

}
