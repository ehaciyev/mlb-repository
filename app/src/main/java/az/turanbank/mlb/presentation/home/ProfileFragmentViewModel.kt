package az.turanbank.mlb.presentation.home

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import androidx.core.content.ContextCompat.startActivity
import az.turanbank.mlb.data.remote.model.response.GetProfileImageResponseModel
import az.turanbank.mlb.data.remote.model.response.ServerResponseModel
import az.turanbank.mlb.domain.user.usecase.GetProfileImageUseCase
import az.turanbank.mlb.domain.user.usecase.UpdateProfileImageUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ProfileFragmentViewModelInputs : BaseViewModelInputs {
    fun updateProfileImage(bytesStr: String)
    fun getProfileImage()
    fun updateTheme(colorName:String)
}

interface ProfileFragmentViewModelOutputs : BaseViewModelOutputs {
    fun fullName(): String?
    fun showProgress(): PublishSubject<Boolean>
    fun updateProfileImageSuccess(): PublishSubject<ServerResponseModel>
    fun getProfileImageSuccess(): PublishSubject<GetProfileImageResponseModel>
    fun themeName(): String?
}

class ProfileFragmentViewModel @Inject constructor(
    private val updateProfileImageUseCase: UpdateProfileImageUseCase,
    private val getProfileImageUseCase: GetProfileImageUseCase,
    private val sharedPrefs: SharedPreferences

) : BaseViewModel(),
    ProfileFragmentViewModelInputs,
    ProfileFragmentViewModelOutputs {

    override val inputs: ProfileFragmentViewModelInputs = this

    override val outputs: ProfileFragmentViewModelOutputs = this

    override fun themeName() = sharedPrefs.getString("theme", "")

    private val showProgress = PublishSubject.create<Boolean>()
    private val updateProfileImageSuccess = PublishSubject.create<ServerResponseModel>()
    private val getProfileImageSuccess = PublishSubject.create<GetProfileImageResponseModel>()

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    val theme = sharedPrefs.getString("theme", "")

    private val updateTheme = PublishSubject.create<Int>()
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    override fun fullName() = sharedPrefs.getString("fullname", "")

    override fun showProgress() = showProgress

    override fun updateProfileImageSuccess() = updateProfileImageSuccess
    override fun getProfileImageSuccess() = getProfileImageSuccess

    override fun updateTheme(colorName: String) {
        sharedPrefs.edit().putString("theme", colorName).apply()
    }

    override fun updateProfileImage(bytesStr: String) {
        var custCompId: Long? = null
        if (isCustomerJuridical) custCompId = compId
        showProgress.onNext(true)
        updateProfileImageUseCase.execute(custId, token, custCompId, bytesStr)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    updateProfileImageSuccess.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun getProfileImage() {
        var custCompId: Long? = null
        if (isCustomerJuridical) custCompId = compId

       // showProgress.onNext(true)

        getProfileImageUseCase.execute(custId, custCompId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
             //   showProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    getProfileImageSuccess.onNext(it)
                }
            }, {
                it.printStackTrace()
            })
            .addTo(subscriptions)
    }

}