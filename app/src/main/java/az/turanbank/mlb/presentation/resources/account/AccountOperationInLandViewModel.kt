package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.response.*
import az.turanbank.mlb.domain.user.usecase.resources.account.*
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountOperationInLandViewModelInputs : BaseViewModelInputs {
    fun getAccountList()
    fun getBudgetClassificationCode()
    fun getBudgetLevelCode()
    fun getBudgetAccountProgress(): PublishSubject<Boolean>
    fun getBudgetAccountByIban(iban: String)
    fun getBankList(ccy: String)
    fun deleteTemp(tempId: Long)
}

interface AccountOperationInLandViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun onTemplateDeleted(): CompletableSubject
    fun onAccountSuccess(): BehaviorSubject<ArrayList<AccountListModel>>
    fun onGetBankListSuccess(): BehaviorSubject<ArrayList<BankInfoModel>>
    fun getBudgetClassificationCodeSuccess(): PublishSubject<ArrayList<BudgetCode>>
    fun showGetBudgetClassificationCodeProgress(): PublishSubject<Boolean>
    fun showGetBudgetLevelCodeProgress(): PublishSubject<Boolean>
    fun getBudgetLevelCodeSuccess(): PublishSubject<ArrayList<BudgetCode>>
    fun getBudgetAccountByIbanSuccess(): PublishSubject<BudgetAccountModel>
    fun crAccountTaxNoProgress(): PublishSubject<Boolean>
    fun crAccountNameProgress(): PublishSubject<Boolean>
}

class AccountOperationInLandViewModel @Inject constructor(
    private val getIndividualAccountListUseCase: GetNoCardAccountListForIndividualCustomerUseCase,
    private val getBankListForIndividualUseCase: GetBankListForIndividualUseCase,
    private val getBankListForJuridicalUseCase: GetBankListForJuridicalUseCase,
    private val getJuridicalAccountListUseCase: GetNoCardAccountListForJuridicalCustomerUseCase,
    private val getBudgetLevelCodeUseCase: GetBudgetLevelCodeUseCase,
    private val getBudgetClassificationCodeUseCase: GetBudgetClassificationCodeUseCase,
    private val getBudgetAccountByIbanUseCase: GetBudgetAccountByIbanUseCase,
    private val deleteOperationTemplateUseCase: DeleteOperationTemplateUseCase,
    sharedPrefs: SharedPreferences
) :
    BaseViewModel(),
    AccountOperationInLandViewModelInputs,
    AccountOperationInLandViewModelOutputs {

    override fun crAccountTaxNoProgress() = crAccountTaxNoProgress
    override fun crAccountNameProgress() = crAccountNameProgress

    override fun getBudgetAccountByIbanSuccess() = getBudgetAccountByIban

    override fun getBudgetAccountProgress() = getBudgetAccountByIbanProgress

    override fun getBudgetAccountByIban(iban: String) {
        crAccountTaxNoProgress.onNext(true)
        crAccountNameProgress.onNext(true)
        getBudgetAccountByIbanUseCase.execute(custId, token, iban)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                crAccountTaxNoProgress.onNext(false)
                crAccountNameProgress.onNext(false)
                getBudgetAccountByIbanProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    getBudgetAccountByIban.onNext(it.budgetAccount)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun showGetBudgetClassificationCodeProgress() = showGetBudgetClassificationCodeProgress

    override fun showGetBudgetLevelCodeProgress() = showGetBudgetLevelCodeProgress

    override fun getBudgetClassificationCodeSuccess() = bankClassificationCode

    override fun getBudgetLevelCodeSuccess() = bankLevelCode

    override fun getBudgetClassificationCode() {
        showGetBudgetClassificationCodeProgress.onNext(true)
        getBudgetClassificationCodeUseCase.execute(custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    showGetBudgetClassificationCodeProgress.onNext(false)
                    bankClassificationCode.onNext(it.budgetCodeList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getBudgetLevelCode() {
        showGetBudgetLevelCodeProgress.onNext(true)
        getBudgetLevelCodeUseCase.execute(custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    showGetBudgetLevelCodeProgress.onNext(false)
                    bankLevelCode.onNext(it.budgetCodeList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override val inputs: AccountOperationInLandViewModelInputs = this
    override val outputs: AccountOperationInLandViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private var budgetClassCode: String = ""
    private var selectedBankCode: String = ""
    private var budgetLevelCode: String = ""

   fun setSelectedBudgetClassCode (value: String) {
       this.budgetClassCode = value
   }

    fun getSelectedBudgetClassCode() = budgetClassCode

    fun setSelectedBudgetLevelCode (value: String) {
       this.budgetLevelCode = value
   }

    fun getSelectedBudgetLevelCode(): String = budgetLevelCode

    fun setSelectedBankCode (value: String) {
       this.selectedBankCode = value
   }

    fun getSelectedBankCode() = selectedBankCode

    private val showGetBudgetLevelCodeProgress = PublishSubject.create<Boolean>()
    private val showGetBudgetClassificationCodeProgress = PublishSubject.create<Boolean>()
    private val showProgress = PublishSubject.create<Boolean>()
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val accountList = BehaviorSubject.create<ArrayList<AccountListModel>>()
    private val bankList = BehaviorSubject.create<ArrayList<BankInfoModel>>()
    private val bankClassificationCode = PublishSubject.create<ArrayList<BudgetCode>>()
    private val bankLevelCode = PublishSubject.create<ArrayList<BudgetCode>>()
    private val crAccountTaxNoProgress = PublishSubject.create<Boolean>()
    private val crAccountNameProgress = PublishSubject.create<Boolean>()
    private val getBudgetAccountByIbanProgress = PublishSubject.create<Boolean>()
    private val getBudgetAccountByIban = PublishSubject.create<BudgetAccountModel>()
    private val delete = CompletableSubject.create()

    private var banks = arrayListOf<BankInfoModel>()

    override fun getAccountList() {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getJuridicalAccountListUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                lang = EnumLangType.AZ
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                    //    getBankList(custId, token, compId)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getIndividualAccountListUseCase.execute(custId, token,EnumLangType.AZ)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountList.onNext(it.accountList)
                  //      getBankList(custId, token!!, null)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun showProgress() = showProgress

    override fun onAccountSuccess() = accountList

    override fun onGetBankListSuccess() = bankList

    override fun getBankList(ccy: String) {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getBankListForJuridicalUseCase.execute(ccy, custId, compId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        banks = it.bankInfoList
                        bankList.onNext(it.bankInfoList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getBankListForIndividualUseCase.execute(ccy, custId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        banks = it.bankInfoList
                        bankList.onNext(it.bankInfoList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    showProgress.onNext(false)
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun deleteTemp(tempId: Long) {
        deleteOperationTemplateUseCase
            .execute(custId, token, tempId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    delete.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onTemplateDeleted() = delete
    fun getBankPosition(bankCode: String): Int{
        var bankPosition =0
        for (i in 0 until banks.size){
            if (banks[i].bankCode == bankCode){
                bankPosition = i
                break
            }
        }
        return bankPosition
    }
}
