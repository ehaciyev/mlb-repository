package az.turanbank.mlb.presentation.resources.account.abroad

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.data.remote.model.resources.account.AccountTemplateResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.resources.account.AccountOperationInternalResultActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_operation_outland.*
import javax.inject.Inject

class AccountOperationAbroadActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: AccountOperationOutLandViewModel

    private var accountList = arrayListOf<AccountListModel>()

    lateinit var dialog: MlbProgressDialog

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_operation_outland)

        viewModel =
            ViewModelProvider(this, factory)[AccountOperationOutLandViewModel::class.java]

        crIban.filters = arrayOf(InputFilter.AllCaps())

        dialog = MlbProgressDialog(this)
        dialog.showDialog()

        toolbar_title.text = getString(R.string.international_transfer)

        toolbar_back_button.setOnClickListener {
            onBackPressed()
        }
        setInputListeners()

        setOutputListeners()
    }

    @SuppressLint("SetTextI18n")
    private fun setOutputListeners() {
        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onTemplateDeleted().subscribe {
            Toast.makeText(this, getString(R.string.template_successfully_deleted), Toast.LENGTH_LONG).show()
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.onAccountSuccess().subscribe { accountListModel ->

            val accountIbanFromIntent = intent.getStringExtra("accountIban")
            val accountCurrNameFromIntent = intent.getStringExtra("currName")

            for (i in 0 until accountListModel.size) {
                if (accountListModel[i].currName != "AZN") {
                    accountList.add(accountListModel[i])
                }
            }

            if (accountList.isNotEmpty()) {
                checkFromTemplateOrNot()
                if (intent.getSerializableExtra("template") == null) {
                    if(accountIbanFromIntent!=null&&accountCurrNameFromIntent!= "AZN"){
                        debitorAccountId.setText("$accountIbanFromIntent / $accountCurrNameFromIntent")
                    }
                }
                val accountNameArray = arrayOfNulls<String>(accountList.size)

                for (i in 0 until accountList.size) {
                    accountNameArray[i] =
                        (accountList[i].iban + " / " + accountList[i].currName)
                }
                debitorAccountIdContainer.setOnClickListener {
                    val builder =
                        AlertDialog.Builder(this@AccountOperationAbroadActivity)
                    builder.setTitle(getString(R.string.select_account))
                    builder.setItems(accountNameArray) { _, which ->
                        debitorAccountId.setText(accountNameArray[which])
                    }
                    val dialog = builder.create()
                    dialog.show()
                }
            }

        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getAccountList()
        submitButton.setOnClickListener {
            val intent = Intent(this, AccountOperationAbroadVerifyActivity::class.java)
            if (
                debitorAccountId.text.toString().trim().isNotEmpty()
                && amount.text.toString().trim().isNotEmpty()
                && amount.text.toString().toDouble() > 0.00
                && creditorBankName.text.toString().trim().isNotEmpty()
                && swiftEditText.text.toString().trim().isNotEmpty()
                && creditorCustomerName.text.toString().trim().isNotEmpty()
                && crIban.text.toString().trim().isNotEmpty()
                && purpose.text.toString().trim().isNotEmpty()
            ) {
                intent.putExtra(
                    "debitorName",
                    viewModel.getDtAccountName(debitorAccountId.text.toString().split(" ")[0])
                )
                intent.putExtra("amount", amount.text.toString())
                intent.putExtra(
                    "dtAccountId",
                    viewModel.getDtAccountId(debitorAccountId.text.toString().split(" ")[0])
                )
                intent.putExtra("crBankName", creditorBankName.text.toString())
                intent.putExtra("crBankSwift", swiftEditText.text.toString())
                intent.putExtra("crCustName", creditorCustomerName.text.toString())
                intent.putExtra("crCustAddress", toAddress.text.toString())
                intent.putExtra("crIban", crIban.text.toString())
                intent.putExtra("purpose", purpose.text.toString())
                intent.putExtra("crBankBranch", toBranch.text.toString())
                intent.putExtra("crBankAddress", toAddress.text.toString())
                intent.putExtra("crBankCountry", toCountry.text.toString())
                intent.putExtra("crBankCity", toCity.text.toString())
                intent.putExtra("crCustPhone", creditorCustomerPhone.text.toString())
                intent.putExtra("note", note.text.toString())
                intent.putExtra("crCorrBankName", mediatorBankName.text.toString())
                intent.putExtra("crCorrBankCountry", mediatorCountry.text.toString())
                intent.putExtra("crCorrBankCity", mediatorCity.text.toString())
                intent.putExtra("crCorrBankSwift", mediatorSwiftEditText.text.toString())
                intent.putExtra("crCorrBankAccount", reporterAccount.text.toString())
                intent.putExtra("crCorrBankBranch", mediatorBranch.text.toString())
                intent.putExtra("currency", viewModel.getCrAccountCurrency(debitorAccountId.text.toString().split(" ")[0]))

                startActivityForResult(intent, 12)
            } else {
                checkInputs()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 12) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent(this, AccountOperationInternalResultActivity::class.java)

                intent.putExtra(
                    "dtAccountId",
                    viewModel.getDtAccountId(debitorAccountId.text.toString().split(" ")[0])
                )

                intent.putExtra("crIban", crIban.text.toString())
                intent.putExtra("dtIban", debitorAccountId.text.toString().split(" ")[0])
                intent.putExtra("currency", viewModel.getCrAccountCurrency(debitorAccountId.text.toString().split(" ")[0])) //TODO
                intent.putExtra("amount", amount.text.toString())
                intent.putExtra("accountName", creditorCustomerName.text.toString())
                intent.putExtra("operationNo", data?.getStringExtra("operationNo"))
                intent.putExtra("accountResponse", data?.getSerializableExtra("accountResponse"))
                intent.putExtra("operationStatus", data?.getStringExtra("operationStatus"))
                startActivity(intent)
                finish()
            }
        }
    }

    private fun checkFromTemplateOrNot() {
        if (intent.getSerializableExtra("template") != null) {
            deleteTemp.visibility = View.VISIBLE
            fillFormAlongWithTemplate(intent.getSerializableExtra("template") as AccountTemplateResponseModel)
        } else {
            deleteTemp.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun fillFormAlongWithTemplate(template: AccountTemplateResponseModel) {
        deleteTemp.setOnClickListener {
            showDeleteTemp(template.id)
        }
        crIban.setText(template.crIban)
        amount.setText(template.amt.toString())
        purpose.setText(template.operPurpose)
        swiftEditText.setText(template.crBankSwift)
        creditorBankName.setText(template.crBankName)
        creditorCustomerName.setText(template.crCustName)
        reporterAccount.setText(template.crBankCorrAcc)
        note.setText(template.note)
        toCountry.setText(template.crBankCountry)
        toCity.setText(template.crBankCity)
        toBranch.setText(template.crAccountBranch)
        toAddress.setText(template.crCustAddress)
        creditorCustomerPhone.setText(template.crCustPhone)
        mediatorBankName.setText(template.crCorrBankName)
        mediatorCountry.setText(template.crCorrBankCountry)
        mediatorCity.setText(template.crCorrBankCity)
        mediatorBranch.setText(template.crCorrBankBranch)
        mediatorSwiftEditText.setText(template.crCorrBankSwift)



        for (i in 0 until accountList.size) {
            if (accountList[i].iban == template.dtIban) {
                debitorAccountId.setText(template.dtIban + " / "+template.amtCcy)
                break
            }
        }
    }

    private fun showDeleteTemp(tempId: Long) {

        val alert = androidx.appcompat.app.AlertDialog.Builder(this)
        alert.setMessage(getString(R.string.template_delete_confirmation_message))

        alert.setCancelable(true)
        alert.setPositiveButton(
            getString(R.string.yes)
        ) { _, _ ->
            viewModel.inputs.deleteTemp(tempId)
        }

        alert.setNegativeButton(
            getString(R.string.no)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }

    private fun checkInputs() {
        if (amount.text.toString().trim().isEmpty()) {
            amount.error = getString(R.string.amount_is_wrong)
        } else {
            amount.error = null
        }
        if (creditorBankName.text.toString().trim().isEmpty()) {
            creditorBankName.error = getString(R.string.enter_creditor_bank)
        } else {
            creditorBankName.error = null
        }
        if (swiftEditText.text.toString().trim().isEmpty()) {
            swiftEditText.error = getString(R.string.enter_creditor_bank_swift)
        } else {
            swiftEditText.error = null
        }
        if (creditorCustomerName.text.toString().trim().isEmpty()) {
            creditorCustomerName.error = getString(R.string.enter_creditor_name)
        } else {
            creditorCustomerName.error = null
        }
        if (crIban.text.toString().trim().isEmpty()) {
            crIban.error = getString(R.string.enter_creditor_iban)
        } else {
            crIban.error = null
        }
        if (purpose.text.toString().trim().isEmpty()) {
            purpose.error = getString(R.string.enter_creditor_purpose)
        } else {
            purpose.error = null
        }
    }

}
