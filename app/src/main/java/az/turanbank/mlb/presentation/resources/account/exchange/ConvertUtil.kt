package az.turanbank.mlb.presentation.resources.account.exchange

import javax.inject.Inject

class ConvertUtil @Inject constructor(
) {
    fun convertedAmount(
        buy: Double,
        amount: Double) = buy/amount
    fun revertedAmount(
        sell: Double,
        amount: Double) = amount*sell
    fun onRateChangedAmount(
        sell: Double,
        buy: Double,
        rate: Double) = (buy/rate).toString()+"|"+(sell*rate)
}