package az.turanbank.mlb.presentation.cardoperations

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_card_operations.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class CardOperationsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardOperationsViewModel

    private val fromTemp: Boolean by lazy { intent.getBooleanExtra("fromTemp", false) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, factory)[CardOperationsViewModel::class.java]

        setContentView(R.layout.activity_card_operations)
        val sectionsPagerAdapter =
            CardOperationsPagerAdapter(
                this,
                supportFragmentManager
            )
        view_pager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(view_pager)

        if(fromTemp) {
            tabs.selectTab(tabs.getTabAt(1))
        }

        toolbar_title.text = getString(R.string.card_operations)
        toolbar_back_button.setOnClickListener { onBackPressed() }
    }
}