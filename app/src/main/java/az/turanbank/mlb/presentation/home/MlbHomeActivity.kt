package az.turanbank.mlb.presentation.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Base64
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import az.turanbank.mlb.ConnectivityLiveData
import az.turanbank.mlb.R
import az.turanbank.mlb.RateThisApp
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.ContactsWithBankActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.branch.BranchNetworkTabbedActivity
import az.turanbank.mlb.presentation.cardoperations.CardOperationsActivity
import az.turanbank.mlb.presentation.exchange.ExchangeActivity
import az.turanbank.mlb.presentation.ips.register.InstantPaymentSplashActivity
import az.turanbank.mlb.presentation.news.NewsActivity
import az.turanbank.mlb.presentation.orders.OrdersActivity
import az.turanbank.mlb.presentation.payments.PaymentsFragment
import az.turanbank.mlb.presentation.resources.account.AccountTransfersFragment
import az.turanbank.mlb.presentation.resources.account.AuthorizationMultiDocsActivity
import az.turanbank.mlb.presentation.resources.account.FirstAuthOperationListActivity
import az.turanbank.mlb.presentation.resources.activity.ResourcesActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.ui.home.HomeFragment
import com.bumptech.glide.Glide
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_login_drawer.nav_view
import kotlinx.android.synthetic.main.activity_mlb_home.*
import kotlinx.android.synthetic.main.app_bar_mlb_home.*
import kotlinx.android.synthetic.main.nav_header_mlb_home.*
import javax.inject.Inject


class MlbHomeActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration

    lateinit var viewmodel: MlbHomeActivityViewModel

    lateinit var dialog: MlbProgressDialog

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var resourceIntent: Intent

    var byteArray: ByteArray? = null

    private val getConnectivity: ConnectivityLiveData by lazy {
        ConnectivityLiveData(connectivityManager = baseContext.getSystemService(
            Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        )
    }

    private var isOnlineCount = 0

    private var fromHome = false
    private var fromTemp = false


    private val byteArrayFrom = PublishSubject.create<ByteArray>()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mlb_home)

        val toolbar: Toolbar = findViewById(R.id.toolbar2)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        dialog = MlbProgressDialog(this)

        viewmodel = ViewModelProvider(this, factory)[MlbHomeActivityViewModel::class.java]
        resourceIntent = Intent(applicationContext, ResourcesActivity::class.java)

        getConnectivity.observe(this, androidx.lifecycle.Observer {
            if(!it){
                isOnlineCount = 1
                Snackbar.make(drawer_layout, getString(R.string.offline), Snackbar.LENGTH_LONG).show()
            } else {
                isOnlineCount *= 2
                if(isOnlineCount == 2){
                    Snackbar.make(drawer_layout, getString(R.string.online), Snackbar.LENGTH_LONG).show()
                }
            }
        })

        setOutputListeners()
        setInputListeners()

        if (intent.getBooleanExtra("fromTemplate", false)){
            bottomNavigationView.selectedItemId = R.id.bottom_payment
        } else {
            setFragment()
        }
        val headerView: View = nav_view.getHeaderView(0)
        val navMenuFullName = headerView.findViewById<TextView>(R.id.navMenuFullname)
        val companyName = headerView.findViewById<TextView>(R.id.navMenuCompanyName)

        if(viewmodel.companyName().isNullOrEmpty()) {
            companyName.text = viewmodel.fullName()
        } else {
            navMenuFullName.text = viewmodel.fullName()
            companyName.text = viewmodel.companyName()
        }

        nav_view.menu.findItem(R.id.card_operations).isVisible = !viewmodel.isCustomerJuridical

        checkIfAsan()

        nav_view.setNavigationItemSelectedListener(this)

        // Set custom criteria (optional)
        //RateThisApp.Config(3, 5)
        RateThisApp.onCreate(this)
        // Show a dialog if criteria is satisfied
        RateThisApp.showRateDialogIfNeeded(this)
    }

    private fun checkIfAsan() {
        if (!viewmodel.getSharedPreference().isNullOrEmpty()) {
            if (!viewmodel.isCustomerJuridical) {
                nav_view.menu.findItem(R.id.auth).isVisible = true
                nav_view.menu.findItem(R.id.auth_1).isVisible = false
                nav_view.menu.findItem(R.id.auth_2).isVisible = false
            } else {
                if (viewmodel.getSignCount() != viewmodel.getSignLevel()) {
                    nav_view.menu.findItem(R.id.auth_1).isVisible = true
                    nav_view.menu.findItem(R.id.auth_2).isVisible = false
                    nav_view.menu.findItem(R.id.auth).isVisible = false
                } else if (viewmodel.getSignCount() == 1 && viewmodel.getSignCount() == viewmodel.getSignLevel()) {
                    nav_view.menu.findItem(R.id.auth_2).isVisible = true
                    (nav_view.menu.findItem(R.id.auth_2).actionView as LinearLayout).findViewById<TextView>(
                        R.id.title
                    ).text = getString(R.string.authorization)
                    nav_view.menu.findItem(R.id.auth_1).isVisible = false
                    nav_view.menu.findItem(R.id.auth).isVisible = false
                } else if (viewmodel.getSignCount() == 2 && viewmodel.getSignCount() == viewmodel.getSignLevel()) {
                    nav_view.menu.findItem(R.id.auth_2).isVisible = true
                    nav_view.menu.findItem(R.id.auth_1).isVisible = false
                    nav_view.menu.findItem(R.id.auth).isVisible = false
                }
            }
        }
    }

    private fun setFragment() {
        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, HomeFragment()).commit()
    }

    override fun onResume() {
        super.onResume()
        //setFragment() //Not necessary to set because user sets it manually. Could be useful in the future
    }

    private fun setOutputListeners() {

        viewmodel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.getProfileImageSuccess().subscribe {
            byteArray = Base64.decode(it.bytes, Base64.DEFAULT)
            Glide
                .with(this)
                .load(byteArray)
                .centerCrop()
                .into(imageView)

        }.addTo(subscriptions)

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if(viewmodel.themeName() == "blue"){
                bottomNavigationView.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                bottomNavigationView.itemBackground = getDrawable(R.color.colorPrimary)
                bottomNavigationView.itemTextColor = getColorStateList(R.drawable.bottom_nav_text)
                bottomNavigationView.itemIconTintList = getColorStateList(R.drawable.bottom_nav_text)
            } else {
                bottomNavigationView.itemBackground = getDrawable(R.color.white)
                bottomNavigationView.setBackgroundColor(resources.getColor(R.color.white))
                bottomNavigationView.itemTextColor = getColorStateList(R.drawable.bottom_nav_text_white)
                bottomNavigationView.itemIconTintList = getColorStateList(R.drawable.bottom_nav_text_white)
            }
        }*/


        viewmodel.outputs.userLoggedOut().subscribe {
            if (it) {
                finish()
            }
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {

        viewmodel.inputs.getProfileImage()

        val actionBarDrawerToggle: ActionBarDrawerToggle =
            object : ActionBarDrawerToggle(this, drawer_layout, R.string.yes, R.string.no) {
                override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                    super.onDrawerSlide(drawerView, slideOffset)
                    val slideX = drawerView.width * slideOffset
                    mainContent.translationX = slideX
                }
            }
        drawer_layout.addDrawerListener(actionBarDrawerToggle)
        nav_drawer_button.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }


        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            val transaction = supportFragmentManager.beginTransaction()
            when (item.itemId) {
                R.id.bottom_main -> {
                    transaction.replace(R.id.nav_host_fragment, HomeFragment()).commit()
                }
                R.id.bottom_payment -> {
                    if (intent.getBooleanExtra("fromTemplate", false)){
                        val fragment = PaymentsFragment()
                        fragment.arguments = Bundle().apply {
                            putBoolean("fromTemplate", true)
                        }
                        transaction.replace(R.id.nav_host_fragment, fragment).commit()
                    } else {
                        transaction.replace(R.id.nav_host_fragment, PaymentsFragment()).commit()
                    }
                }
                R.id.bottom_transfer -> {
                    transaction
                        .replace(R.id.nav_host_fragment,
                            AccountTransfersFragment().apply {
                                arguments = Bundle().apply {
                                    putBoolean("fromHome", fromHome)
                                    putBoolean("fromTemp", fromTemp)
                                }
                            }
                        ).commit()

                    //transaction.replace(R.id.nav_host_fragment, AccountTransfersFragment()).commit()
                }
                R.id.bottom_profile -> {
                    transaction.replace(R.id.nav_host_fragment, ProfileFragment()).commit()
                }
            }
            true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        if (item.isChecked) {
            drawer_layout.closeDrawer(GravityCompat.START)
            return false
        }

        when (id) {
            R.id.nav_home -> bottomNavigationView.selectedItemId = R.id.bottom_main
            R.id.cards -> startResourcesActivity(0)
            R.id.ips -> startActivity(Intent(this, InstantPaymentSplashActivity::class.java))
            R.id.deposit -> startResourcesActivity(3)
            R.id.accounts -> startResourcesActivity(1)
            R.id.credits -> startResourcesActivity(2)
            R.id.card_operations -> startActivity(Intent(this, CardOperationsActivity::class.java))
            R.id.exchange -> startActivity(Intent(this, ExchangeActivity::class.java))
            R.id.contact_with_us -> startActivity(
                Intent(
                    this,
                    ContactsWithBankActivity::class.java
                )
            )
            R.id.auth, R.id.auth_2 -> startActivity(
                Intent(this, AuthorizationMultiDocsActivity::class.java)
            )
            R.id.auth_1 -> startActivity(Intent(this, FirstAuthOperationListActivity::class.java))
            R.id.payments_drawer -> {
                supportFragmentManager.beginTransaction().replace(R.id.nav_host_fragment, PaymentsFragment()).commit()
                bottomNavigationView.selectedItemId = R.id.bottom_payment
            }
            R.id.logout -> showLogoutDialog()
            R.id.orders -> startActivity(Intent(this, OrdersActivity::class.java))
            R.id.settings -> {
                bottomNavigationView.selectedItemId = R.id.bottom_profile
            }
            R.id.account_transfer ->{
                bottomNavigationView.selectedItemId = R.id.bottom_transfer

               /* if (savedInstanceState == null) {

                startActivity(
                Intent(this, AccountTransfersActivity::class.java))*/
            }
            R.id.account_history -> {
                /*supportFragmentManager.beginTransaction()
                    .replace(R.id.nav_host_fragment,
                        AccountTransfersFragment().apply {
                            arguments = Bundle().apply {
                                putBoolean("fromHome", true)
                                putBoolean("fromTemp", false)
                            }
                        }
                    ).commit()*/
                fromHome = true
                fromTemp = false
                bottomNavigationView.selectedItemId = R.id.bottom_transfer


               /* val intent = Intent(this, AccountTransfersActivity::class.java)
                intent.putExtra("fromHome", true)
                startActivity(intent)*/
            }
            R.id.branch -> {
                startActivity(Intent(this, BranchNetworkTabbedActivity::class.java))
            }
            R.id.campaigns -> {
                startActivity(Intent(this, NewsActivity::class.java))
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun showLogoutDialog() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.logout))
            .setMessage(R.string.logout_confirmation_message)
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                viewmodel.inputs.logout()
            }
            .setNegativeButton(getString(R.string.no), null)
            .show()
    }

    private fun startResourcesActivity(position: Int) {
        resourceIntent.putExtra("tabPosition", position)
        startActivity(resourceIntent)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog.hideDialog()
    }

    override fun onPause() {
        super.onPause()
        dialog.hideDialog()

    }

}
