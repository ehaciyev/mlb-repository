package az.turanbank.mlb.presentation.resources.card.statement

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import az.turanbank.mlb.R



/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 *
 */
class CardStatementPagerAdapter(private val context: Context, private val TAB_TITLES: Array<String>,fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        val bundle = Bundle()

        when (position) {
            0 -> {
                bundle.putInt("type", 0)
            }
            1-> {
                bundle.putInt("type", 1)
            }
            2 -> {
                bundle.putInt("type", 2)
            }
            else -> {
                bundle.putInt("type", 0)
            }
        }

        val fragment = CardStatementFragment()
        fragment.arguments = bundle

        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TAB_TITLES[position]
    }

    override fun getCount(): Int {
        return 3
    }
}