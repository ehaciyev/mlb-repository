package az.turanbank.mlb.presentation.login.asan.individual

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetAuthInfoResponseModel
import az.turanbank.mlb.domain.user.register.usecase.GetAuthInfoUseCase
import az.turanbank.mlb.domain.user.usecase.login.CheckPinForIndividualAsanLoginUseCase
import az.turanbank.mlb.domain.user.usecase.register.asan.GetPinFromAsanUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface LoginAsanIndividualViewModelInputs: BaseViewModelInputs {
    fun getPinFromAsan(phoneNumber: String, userId: String)
}

interface LoginAsanIndividualViewModelOutputs: BaseViewModelOutputs {
    fun verificationCode(): Observable<GetAuthInfoResponseModel>
    fun onUserBlocked(): CompletableSubject
    fun showProgress(): PublishSubject<Boolean>

}

class LoginAsanIndividualViewModel @Inject constructor(
    private val getPinFromAsanUseCase: GetPinFromAsanUseCase,
    private val checkPinForIndividualAsanLoginUseCase: CheckPinForIndividualAsanLoginUseCase,
    private val getAuthInfoUseCase: GetAuthInfoUseCase,
    sharedPreferences: SharedPreferences
) : BaseViewModel(),
    LoginAsanIndividualViewModelInputs,
    LoginAsanIndividualViewModelOutputs {

    private val verificationCode = PublishSubject.create<GetAuthInfoResponseModel>()
    override val inputs: LoginAsanIndividualViewModelInputs = this
    private val showProgress = PublishSubject.create<Boolean>()

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override val outputs: LoginAsanIndividualViewModelOutputs = this

    private val userBlocked = CompletableSubject.create()
    private var custCode: Long = 0L
    private var custId: Long = 0L
    private lateinit var custPin: String

    fun custCode() = custCode

    fun custId() = custId

    fun custPin() = custPin

    override fun getPinFromAsan(phoneNumber: String, userId: String) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        showProgress.onNext(true)
        getPinFromAsanUseCase.execute(phoneNumber, userId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.status.statusCode == 1) {
                    checkPinForIndividualAsanRegister(it.pin, phoneNumber, userId)
                } else {
                    showProgress.onNext(false)
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    private fun checkPinForIndividualAsanRegister(pin: String, phoneNumber: String, userId: String) {
        checkPinForIndividualAsanLoginUseCase.execute(pin)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                custCode = it.custCode
                custPin = it.pin
                custId = it.custId
                
                if(it.status.statusCode == 1){
                    getAuthInfo(phoneNumber, userId)
                } else if (it.status.statusCode == 119) {
                    showProgress.onNext(false)
                    userBlocked.onComplete()
                } else {
                    showProgress.onNext(false)
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }
    private fun  getAuthInfo(phoneNumber: String, userId: String){
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        getAuthInfoUseCase.execute(phoneNumber, userId, enumLangType)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.status.statusCode == 1) {
                    verificationCode.onNext(it)
                } else {
                    showProgress.onNext(false)
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun verificationCode(): Observable<GetAuthInfoResponseModel> = verificationCode
    override fun onUserBlocked() = userBlocked
    override fun showProgress() = showProgress
}