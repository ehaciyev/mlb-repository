package az.turanbank.mlb.presentation.ips.accounts.authchoice

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.data.SelectUnregisteredAccountModel
import az.turanbank.mlb.presentation.ips.register.account.InstantPaymentAccountMultiSelectViewModel
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_ips_account_multi_select.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class InstantPaymentAccountAuthSelectActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: InstantPaymentAccountMultiSelectViewModel

    lateinit var adapter: InstantPaymentAccountAuthSelectingAdapter
    private val accountList = arrayListOf<SelectUnregisteredAccountModel>()
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ips_account_auth_select)

        viewModel = ViewModelProvider(this, factory) [InstantPaymentAccountMultiSelectViewModel::class.java]

        toolbar_title.text = getString(R.string.accounts)
        toolbar_back_button.setOnClickListener{onBackPressed()}

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        adapter = InstantPaymentAccountAuthSelectingAdapter(accountList, this){
            accountList[it].checked = !accountList[it].checked
            adapter.notifyDataSetChanged()
        }

        recyclerView.adapter = adapter
        setOutputListeners()
        setInputListeners()

    }

    private fun setInputListeners() {
        viewModel.inputs.getAccounts()

        val accountsToRegisterIpsSystem = arrayListOf<Long>()
        submitButton.setOnClickListener {
            accountList.forEach {
                if(it.checked){
                    accountsToRegisterIpsSystem.add(it.account.accountId)
                }
            }

            if (accountsToRegisterIpsSystem.isEmpty()){
                AlertDialogMapper(this, 33312).showAlertDialog()
            } else {
                val intent = Intent()
                intent.putExtra("accountsToRegisterIpsSystem", accountsToRegisterIpsSystem)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }

        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onAccountListSuccess().subscribe {
            noDataAvailable.visibility = View.GONE
            message.visibility = View.VISIBLE
            accountList.clear()
            accountList.addAll(it)
            recyclerView.adapter?.notifyDataSetChanged()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            noDataAvailable.visibility = View.VISIBLE
            message.visibility = View.GONE
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
            /*setResult(Activity.RESULT_CANCELED)
            finish()*/
        }.addTo(subscriptions)
    }
}
