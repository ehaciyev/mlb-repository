package az.turanbank.mlb.presentation.exchange.converter

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseActivity
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class ExchangeConverterActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchange_converter)
        val sectionsPagerAdapter = ExchangeConverterPagerAdapter(
            this,
            supportFragmentManager
        )
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)

        toolbar_title.text = getString(R.string.exchanges)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        tabs.setupWithViewPager(viewPager)
    }
}
