package az.turanbank.mlb.presentation.resources.account

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.resources.account.changename.AccountChangeNameActivity
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.secondPartOfMoney
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class AccountActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: AccountActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)

        toolbar_title.text = getString(R.string.account)

        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewModel = ViewModelProvider(this, factory) [AccountActivityViewModel::class.java]
        val plasticCardInteger = intent.getDoubleExtra("accountBalance",0.0)
        val plasticCardReminder = intent.getStringExtra("currName")

        plasticCardAmountInteger.text = firstPartOfMoney(plasticCardInteger)
        plasticCardAmountReminder.text = secondPartOfMoney(plasticCardInteger) + plasticCardReminder
        //val accountBalanceWithCurrency = intent.getStringExtra("accountBalance") + " " + intent.getStringExtra("currName")
        //accountBalance.text = accountBalanceWithCurrency
        accountNumber.text = intent.getStringExtra("iban")

        accountActionStatement.setOnClickListener {
            val i = Intent(this, AccountStatementsTabbedActivity::class.java)
            i.putExtra("accountId", intent.getLongExtra("accountId",0L))
            startActivity(i)
        }

        accountActionRequisites.setOnClickListener {
            val i = Intent(this, AccountRequisitesActivity::class.java)
            i.putExtra("accountId", intent.getLongExtra("accountId",0L))
            startActivity(i)
        }

        accountActionTransfer.setOnClickListener {
            val i = Intent(this, AccountTransfersActivity::class.java)
            i.putExtra("accountId", intent.getLongExtra("accountId",0L))
            i.putExtra("accountIban", intent.getStringExtra("accountIban"))
            i.putExtra("currName", intent.getStringExtra("currName"))

            startActivity(i)
        }

        accountActionChangeName.setOnClickListener {
            val i = Intent(this, AccountChangeNameActivity::class.java)
            i.putExtra("accountId", intent.getLongExtra("accountId",0L))
            i.putExtra("accountIban", intent.getStringExtra("accountIban"))
            i.putExtra("accountName", intent.getStringExtra("accountName"))

            startActivity(i)
        }

        accountStatementDate.text = today()

        setupInputListeners()
        setupOutputListeners()
    }

    private fun setupInputListeners() {
        animateProgressImage()
        viewModel.inputs.getAccountStatement(intent.getLongExtra("accountId",0L))
    }

    private fun animateProgressImage() {
        accountStatementProgress.visibility = View.VISIBLE
        scrollView.visibility = View.GONE
        plasticCardInclude.visibility = View.GONE
        val rotate = RotateAnimation(
                0f,
                360f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        accountStatementProgress.startAnimation(rotate)
    }
    private fun today(): String = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date()).replace("-", ".")

    private fun setupOutputListeners() {
        viewModel.outputs.onError().subscribe {
            accountStatementRecyclerView.visibility = View.GONE
            recyclerViewFail.text = getString(ErrorMessageMapper().getErrorMessage(it))
            recyclerViewFail.visibility = View.VISIBLE
        }.addTo(subscriptions)

        viewModel.outputs.accountStatementFinished().subscribe {
            scrollView.visibility = View.VISIBLE
            plasticCardInclude.visibility = View.VISIBLE
            accountStatementProgress.clearAnimation()
            accountStatementProgress.visibility = View.GONE
        }.addTo(subscriptions)

        viewModel.outputs.accountStatement().subscribe {
            recyclerViewFail.visibility = View.GONE
            val adapter = AccountStatementRecyclerViewAdapter(it)

            val llm = LinearLayoutManager(this)
            llm.orientation = LinearLayoutManager.VERTICAL

            accountStatementRecyclerView.layoutManager = llm
            accountStatementRecyclerView.adapter = adapter
        }.addTo(subscriptions)
    }
}
