package az.turanbank.mlb.presentation.resources.account

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class AccountTransfersActivity : BaseActivity() {

    lateinit var viewmodel: AccountTransfersActivityViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private val fromHome: Boolean by lazy { intent.getBooleanExtra("fromHome", false) }
    private val fromTemp: Boolean by lazy { intent.getBooleanExtra("fromTemp", false) }
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.account_transfer_activity)

        viewmodel = ViewModelProvider(this, factory)[AccountTransfersActivityViewModel::class.java]

        toolbar_title.text = getString(R.string.transfers)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        val toolbar: Toolbar = findViewById(R.id.toolbar2)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container,
                    AccountTransfersFragment().apply {
                        arguments = Bundle().apply {
                            putBoolean("fromHome", fromHome)
                            putBoolean("fromTemp", fromTemp)
                        }
                    }
                ).commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return true
    }
}