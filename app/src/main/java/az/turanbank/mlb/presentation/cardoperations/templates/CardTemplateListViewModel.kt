package az.turanbank.mlb.presentation.cardoperations.templates

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.cardoperations.CardTemplateModel
import az.turanbank.mlb.domain.user.usecase.cardoperations.GetCardOperationTempListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.cardoperations.cardtocard.CardToCardActivity
import az.turanbank.mlb.presentation.cardoperations.othercard.CardToOtherCardActivity
import az.turanbank.mlb.presentation.cardoperations.cashbycode.CashByCodeActivity
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface CardTemplateListViewModelInputs : BaseViewModelInputs {
    fun getTemplateList()
}

interface CardTemplateListViewModelOutputs : BaseViewModelOutputs {
    fun onTemplateListSuccess(): PublishSubject<ArrayList<CardTemplateModel>>
}

class CardTemplateListViewModel @Inject constructor(
    private val getCardOperationTempListUseCase: GetCardOperationTempListUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    CardTemplateListViewModelInputs,
    CardTemplateListViewModelOutputs {

    override val inputs: CardTemplateListViewModelInputs = this

    override val outputs: CardTemplateListViewModelOutputs = this

    private var templateList = PublishSubject.create<ArrayList<CardTemplateModel>>()

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun getTemplateList() {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        getCardOperationTempListUseCase
            .execute(custId, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    templateList.onNext(it.cardTempList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    fun getIntentWithData(activity: Activity, template: CardTemplateModel): Intent {
        lateinit var intent: Intent

        when (template.cardOperationType) {
            1 -> {
                intent = Intent(activity, CardToCardActivity::class.java)
            }
            2 -> {
                intent = Intent(activity, CardToOtherCardActivity::class.java)
            }
            3 -> {
                intent = Intent(activity, CashByCodeActivity::class.java)
            }
        }
        intent.putExtra("fromTemplate", true)
        intent.putExtra("fromCard", template.requestorCardNumber)
        intent.putExtra("tempId", template.tempId)
        intent.putExtra("toCardOrMobile", template.destinationCardNumber)
        intent.putExtra("amount", template.amount)
        intent.putExtra("currency", template.currency)

        return intent
    }

    override fun onTemplateListSuccess() = templateList
}