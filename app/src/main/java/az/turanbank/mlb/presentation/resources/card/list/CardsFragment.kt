package az.turanbank.mlb.presentation.resources.card.list


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.resources.card.activity.CardActivity
import com.facebook.shimmer.ShimmerFrameLayout
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class CardsFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardViewModel

    private lateinit var recyclerView: RecyclerView

    private lateinit var cardAdapter: CardListAdapter

    private lateinit var progress: ImageView
    private lateinit var failMessage: TextView

    lateinit var shimmerCardContainer: ShimmerFrameLayout

    private var cardList: ArrayList<CardListModel> = arrayListOf()

    private val fromChoose: Boolean by lazy { arguments?.getBoolean("fromChoose")!! }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_cards, container, false)

        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL

        shimmerCardContainer = view.findViewById(R.id.shimmerCardContainer)
        recyclerView = view.findViewById(R.id.cardsRecyclerView)
        progress = view.findViewById(R.id.progress)
        failMessage = view.findViewById(R.id.failMessage)

        recyclerView.layoutManager = llm


        cardAdapter = CardListAdapter(
            cardList
        ) {
            if(!fromChoose) {
                val intent = Intent(
                    requireActivity(),
                    CardActivity::class.java
                )
                intent.putExtra("cardNumber", cardList[it].cardNumber)
                if (cardList[it].cardStatus == "0") {
                    intent.putExtra("cardBlocked", true)
                } else {
                    intent.putExtra("cardBlocked", false)
                }
                intent.putExtra("cardId", cardList[it].cardId)
                intent.putExtra(
                    "shortCardNumber",
                    cardList[it].cardNumber.replaceRange(4, 12, " **** **** ")
                )
                startActivity(intent)
            } else {
                val intent = Intent()
                intent.putExtra("dtAccountId", cardList[it].accountId)
                intent.putExtra("dtNumber", cardList[it].cardNumber)
                intent.putExtra("currency", cardList[it].currency)
                intent.putExtra("payType", 1)
                requireActivity().setResult(Activity.RESULT_OK, intent)
                requireActivity().finish()
            }
        }

        recyclerView.adapter = cardAdapter
        setOutputListeners()
        setInputListeners()
        return view
    }


    private fun setOutputListeners() {
        viewModel.outputs.onCardListGot().subscribe {
            setRecyclerView(it)
            failMessage.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            progress.clearAnimation()
            progress.visibility = View.GONE
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            failMessage.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
            shimmerCardContainer.stopShimmerAnimation()
            shimmerCardContainer.visibility = View.GONE
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
        }.addTo(subscriptions)
    }

    private fun setRecyclerView(cards: ArrayList<CardListModel>) {
        cardList.clear()
        cardList.addAll(cards)
        recyclerView.adapter?.notifyDataSetChanged()
        shimmerCardContainer.stopShimmerAnimation()
        shimmerCardContainer.visibility = View.GONE
    }

    private fun setInputListeners() {
        //animateProgressImage()
        cardList.clear()
        recyclerView.adapter?.notifyDataSetChanged()
        viewModel.inputs.getIndividualCards()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = ViewModelProvider(this, factory)[CardViewModel::class.java]
        retainInstance = true
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }

    override fun onResume() {
        super.onResume()
        shimmerCardContainer.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmerCardContainer.stopShimmerAnimation()
    }
}
