package az.turanbank.mlb.presentation.ips.aliases.link.alias

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAliasesUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface LinkAliasToAccountViewModelInputs: BaseViewModelInputs{
    fun getAliases()
}
interface LinkAliasToAccountViewModelOutputs: BaseViewModelOutputs{
    fun onAliases(): PublishSubject<ArrayList<MLBAliasResponseModel>>
}
class LinkAliasToAccountViewModel @Inject constructor(
    private val getMLBAliasesUseCase: GetMLBAliasesUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
    LinkAliasToAccountViewModelInputs,
    LinkAliasToAccountViewModelOutputs {
    override val inputs: LinkAliasToAccountViewModelInputs = this

    override val outputs: LinkAliasToAccountViewModelOutputs = this
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val aliases = PublishSubject.create<ArrayList<MLBAliasResponseModel>>()

    override fun getAliases() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        getMLBAliasesUseCase.execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ aliases ->
                if (aliases.status.statusCode == 1) {
                    this.aliases.onNext(aliases.aliases)
                } else {
                    error.onNext(aliases.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun onAliases() = aliases
}