package az.turanbank.mlb.presentation.payments.pay.categories

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.pg.response.payments.category.CategoryResponseModel
import az.turanbank.mlb.domain.user.usecase.pg.category.GetCategoryListIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.category.GetCategoryListJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface PaymentCategoriesViewModelInputs: BaseViewModelInputs{
    fun getCategories()
}
interface PaymentCategoriesViewModelOutputs: BaseViewModelOutputs{
    fun categoryNames(): PublishSubject<ArrayList<CategoryResponseModel>>
}
class PaymentCategoriesViewModel @Inject constructor(
    private val getCategoryListIndividualUseCase: GetCategoryListIndividualUseCase,
    private val getCategoryListJuridicalUseCase: GetCategoryListJuridicalUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
    PaymentCategoriesViewModelInputs,
    PaymentCategoriesViewModelOutputs {

    override val inputs: PaymentCategoriesViewModelInputs = this

    override val outputs: PaymentCategoriesViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val lang = sharedPrefs.getString("lang", "az")
    private var enumLangType = EnumLangType.AZ

    private val categories = PublishSubject.create<ArrayList<CategoryResponseModel>>()

    override fun getCategories() {
        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }
        if ( isCustomerJuridical ){
            getCategoryListJuridicalUseCase
                .execute(custId, compId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        categories.onNext(it.categoryList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getCategoryListIndividualUseCase
                .execute(custId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        categories.onNext(it.categoryList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun categoryNames() = categories
}