package az.turanbank.mlb.presentation.payments.history


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.pg.response.payments.list.PaymentResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.pay.PayResponseModel
import az.turanbank.mlb.data.remote.model.response.ServerStatusModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.payments.pay.result.PaymentResultActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_payment_history.*
import javax.inject.Inject

class PaymentHistoryFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: PaymentHistoryViewModel

    lateinit var recyclerView: RecyclerView

    val paymentList = arrayListOf<PaymentResponseModel>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_payment_history, container, false)

        viewModel =
            ViewModelProvider(this, factory)[PaymentHistoryViewModel::class.java]

        recyclerView = view.findViewById(R.id.recyclerView)

        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        recyclerView.adapter = PaymentHistoryAdapter(paymentList){
            val pay = PayResponseModel(
                paymentList[it].transactionNumber,
                paymentList[it].receiptNumber,
                paymentList[it].billingDate,
                paymentList[it].cardNumber,
                paymentList[it].merchantName,
                paymentList[it].amount,
                paymentList[it].description,
                ServerStatusModel(0, ""),
                paymentList[it].merchantId,
                paymentList[it].categoryId,
                paymentList[it].providerId
                )

            startActivity(Intent(requireActivity(), PaymentResultActivity::class.java).putExtra("payment", pay).putExtra("isFromHistory",true))
        }

        setOutputListeners()

        setInputListeners()
        return view
    }

    private fun setInputListeners() {
        viewModel.inputs.getPayments()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onPaymentListSuccess().subscribe {
            paymentList.clear()
            paymentList.addAll(it)
            recyclerView.adapter?.notifyDataSetChanged()
            noDataAvailable.visibility = View.GONE
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe{
            noDataAvailable.visibility = View.VISIBLE
        }.addTo(subscriptions)

    }


}
