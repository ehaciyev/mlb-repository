package az.turanbank.mlb.presentation.register.mobile.individual

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.addPrefix
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_individual_register_mobile.mobileNumber
import kotlinx.android.synthetic.main.activity_individual_register_mobile.pin
import kotlinx.android.synthetic.main.activity_individual_register_mobile.voen
import kotlinx.android.synthetic.main.content_individual_register_asan.progressImage
import kotlinx.android.synthetic.main.content_individual_register_asan.submitButton
import kotlinx.android.synthetic.main.content_individual_register_asan.submitText
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class IndividualRegisterMobileActivity : BaseActivity() {

    private lateinit var viewModel: IndividualRegisterMobileViewModel

    @Inject
    lateinit var resource: Resources

    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory

    private val customerType by lazy { intent.getStringExtra("customerType") }

    private lateinit var textWatcher: TextWatcher
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual_register_mobile)
        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(IndividualRegisterMobileViewModel::class.java)

        toolbar_title.text = resource.getString(R.string.register_with_mobile)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        pin.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(7))
        buttonEnabled(false)
        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!pin.text.toString().isNullOrEmpty() && !mobileNumber.text.toString().isNullOrEmpty()) {
                    buttonEnabled(true)
                    submitButton.setOnClickListener {
                        showProgressBar(true)
                        viewModel.inputs.onCheckIndividualCustomer(
                            pin.text.toString(),
                            mobileNumber.text.toString().addPrefix()
                        )
                    }
                } else {
                    buttonEnabled(false)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        mobileNumber.addTextChangedListener(textWatcher)
        mobileNumber.transformationMethod = object : PasswordTransformationMethod() {
            override fun getTransformation(source: CharSequence?, view: View?): CharSequence {
                source?.let {
                    return it
                }

                return super.getTransformation(source, view)
            }
        }
        pin.addTextChangedListener(textWatcher)
        setOutputListeners()
    }

    private fun buttonEnabled(enabled: Boolean) {
        if (!enabled) {
            submitButton.isClickable = false
            submitButton.isEnabled = false
            submitButton.isFocusable = false
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
        } else {
            submitButton.isClickable = true
            submitButton.isEnabled = true
            submitButton.isFocusable = true
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
    }

    private fun setOutputListeners() {

        viewModel.outputs.showProgress().subscribe {
            showProgressBar(it)
        }.addTo(subscriptions)

        viewModel.outputs.verifyOtpCode().subscribe {
            showProgressBar(false)
            val intent = Intent(this, VerifyIndividualRegisterMobileActivity::class.java)
            intent.putExtra("custId", it)
            intent.putExtra("mobile", mobileNumber.text.toString().addPrefix())
            intent.putExtra("customerType", customerType)
            intent.putExtra("pin", pin.text.toString())
            intent.putExtra("taxNo", voen.text.toString())
            startActivity(intent)
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
            showProgressBar(false)
        }.addTo(subscriptions)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        pin.isClickable = !show
        mobileNumber.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }
}
