package az.turanbank.mlb.presentation.resources.deposit.payedamounts

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.deposit.PayedAmountResponseModel
import kotlinx.android.synthetic.main.payment_plan_list.view.*

class PayedAmountsAdapter (private val currency: String, private val payedAmounts: ArrayList<PayedAmountResponseModel>, private val clickListener: (position: Int) -> Unit) : RecyclerView.Adapter<PayedAmountsAdapter.PayedAmountViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PayedAmountViewHolder =
        PayedAmountViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.payment_plan_list,
                parent,
                false
            )
        )

    override fun getItemCount() = payedAmounts.size

    override fun onBindViewHolder(holder: PayedAmountViewHolder, position: Int) {
        holder.bind(currency, payedAmounts[position], clickListener)
    }
    class PayedAmountViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(currency: String, loansPayments: PayedAmountResponseModel, clickListener: (position: Int) -> Unit) {
            with(loansPayments) {
                itemView.date.text = this.paymentDate
                itemView.amount.text = this.payedAmount.toString()
                itemView.mainLoan.text = currency
                itemView.setOnClickListener {
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}