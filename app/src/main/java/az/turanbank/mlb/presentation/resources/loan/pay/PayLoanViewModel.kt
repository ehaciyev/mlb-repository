package az.turanbank.mlb.presentation.resources.loan.pay

import android.content.SharedPreferences
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import javax.inject.Inject

interface PayLoanViewModelInputs: BaseViewModelInputs{

}
interface PayLoanViewModelOutputs: BaseViewModelOutputs{

}
class PayLoanViewModel @Inject constructor(
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(),
        PayLoanViewModelInputs,
        PayLoanViewModelOutputs{

    override val inputs: PayLoanViewModelInputs = this
    override val outputs: PayLoanViewModelOutputs = this

    fun getTaxNo() =
        sharedPrefs.getString("taxNo", "")

}