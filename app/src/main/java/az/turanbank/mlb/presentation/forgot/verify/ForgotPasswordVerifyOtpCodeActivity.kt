package az.turanbank.mlb.presentation.forgot.verify

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.forgot.change.ChangePasswordActivity
import az.turanbank.mlb.presentation.register.mobile.individual.VerifyIndividualRegisterMobileViewModel
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_verify_individual_register_mobile.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.math.RoundingMode
import java.text.DecimalFormat
import javax.inject.Inject

class ForgotPasswordVerifyOtpCodeActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: VerifyIndividualRegisterMobileViewModel


    @Inject
    lateinit var resource: Resources
    private var counter = 0

    private val decimalFormat = DecimalFormat("00")
    private val countDown = object : CountDownTimer(180000, 1000) {

        @SuppressLint("SetTextI18n")
        override fun onTick(millisUntilFinished: Long) {
            decimalFormat.roundingMode = RoundingMode.CEILING
            timer.text =
                ((millisUntilFinished / 1000) / 60).toString() + ":" + decimalFormat.format((millisUntilFinished / 1000) % 60).toString()
        }

        override fun onFinish() {
            startActivity(
                Intent(
                    this@ForgotPasswordVerifyOtpCodeActivity,
                    ErrorPageViewActivity::class.java
                )
            )
            finish()
        }
    }
    private val custId: Long by lazy { intent.getLongExtra("custId", 0L) }
    private val mobile by lazy { intent.getStringExtra("mobile") }
    private val compId by lazy { intent.getLongExtra("compId", 0L) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_individual_register_mobile)

        viewmodel = ViewModelProvider(
            this,
            factory
        )[VerifyIndividualRegisterMobileViewModel::class.java]

        toolbar_title.text = getString(R.string.forgot_password)
        setInputListeners()
        setOutputListeners()
        countDown.start()

    }

    private fun setInputListeners() {
        submitButton.setOnClickListener {
            showProgressBar(true)
            viewmodel.inputs.verifyOtpCode(
                custId,
                compId,
                pinCodeView.text.toString()
            )
        }
        number.text =
            resource.getString(R.string.code_sent_to, "+${mobile?.replaceRange(5, 10, "*****")}")

        sendOtpAgain.setOnClickListener {
            if (counter < 10) {
                viewmodel.inputs.sendPinAgain(custId, compId,mobile)
                counter++
            } else {
                blockUser()
            }
        }

        toolbar_back_button.setOnClickListener { onBackPressed() }
    }

    private fun blockUser() {
        viewmodel.inputs.blockUser(custId, compId)
    }

    private fun setOutputListeners() {
        viewmodel.outputs.onCodeCorrect().subscribe {
            val intent = Intent(this, ChangePasswordActivity::class.java)
            intent.putExtra("custId", custId)
            if (compId != 0L) {
                intent.putExtra("compId", compId)
            }
            countDown.cancel()
            finish()
            startActivity(intent)
        }.addTo(subscriptions)

        viewmodel.outputs.onSendPinAgain().subscribe {
            if (it) {
                pinCodeView.setText("")
                countDown.cancel()
                countDown.start()
                AlertDialog.Builder(this)
                    .setTitle(getString(R.string.error_message))
                    .setMessage(getString(R.string.new_otp_sent))
                    .setPositiveButton(R.string.ok) { _, _ -> }
                    .setCancelable(true)
                    .show()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            pinCodeView.setText("")
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
        viewmodel.outputs.onCustomerBlocked().subscribe {
            startActivity(
                Intent(
                    this@ForgotPasswordVerifyOtpCodeActivity,
                    ErrorPageViewActivity::class.java
                )
            )
            finish()
        }.addTo(subscriptions)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
//        asanPhoneNumber.isClickable = !show
//        asanId.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    override fun onDestroy() {
        super.onDestroy()
        countDown.cancel()
    }
}
