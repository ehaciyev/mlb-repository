package az.turanbank.mlb.presentation.resources.card.list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.card.CardListModel
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.formatDecimal
import az.turanbank.mlb.util.secondPartOfMoney
import kotlinx.android.synthetic.main.card_list_view.view.*


class CardListAdapter (private val currencyList: ArrayList<CardListModel>, private val clickListener: (position: Int) -> Unit) : RecyclerView.Adapter<CardListAdapter.CardViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CardViewHolder =
        CardViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_list_view,
                parent,
                false
            )
        )

    override fun getItemCount() = currencyList.size

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        holder.bind(currencyList[position], clickListener)
    }

    class CardViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(card: CardListModel, clickListener: (position: Int) -> Unit) {

            with(card) {
                itemView.cardNumber.text = this.cardNumber.replaceRange(4,12, " **** **** ")
                itemView.cardAmount.text = firstPartOfMoney(balance)
                itemView.cardAmountReminder.text = secondPartOfMoney(balance)
                itemView.currency.text = currency
                //itemView.cardName.text = cardType
                itemView.setOnClickListener {
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}