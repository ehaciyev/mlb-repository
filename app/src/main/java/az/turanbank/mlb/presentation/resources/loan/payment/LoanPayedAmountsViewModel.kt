package az.turanbank.mlb.presentation.resources.loan.payment

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.loan.LoanPayedAmountModel
import az.turanbank.mlb.domain.user.usecase.resources.loan.payed.LoanPayedAmountsIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.loan.payed.LoanPayedAmountsJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface LoanPaymentsViewModelInputs: BaseViewModelInputs{
    fun getThePayments(loanId: Long)
}
interface LoanPaymentsViewModelOutputs: BaseViewModelOutputs{
    fun onPaymentsSuccess(): PublishSubject<ArrayList<LoanPayedAmountModel>>
}
class LoanPayedAmountsViewModel @Inject constructor(
    private val loanPayedAmountsIndividualUseCase: LoanPayedAmountsIndividualUseCase,
    private val loanPayedAmountsJuridicalUseCase: LoanPayedAmountsJuridicalUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
    LoanPaymentsViewModelInputs,
    LoanPaymentsViewModelOutputs {
    override val inputs: LoanPaymentsViewModelInputs = this

    override val outputs: LoanPaymentsViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)

    private val paymentList = PublishSubject.create<ArrayList<LoanPayedAmountModel>>()

    override fun getThePayments(loanId: Long) {
        if (isCustomerJuridical){
            loanPayedAmountsJuridicalUseCase.execute(custId, compId, loanId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        paymentList.onNext(it.loanPayments)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                }).addTo(subscriptions)
        } else {
            loanPayedAmountsIndividualUseCase.execute(custId, loanId, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        paymentList.onNext(it.loanPayments)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                }).addTo(subscriptions)
        }
    }

    override fun onPaymentsSuccess() = paymentList
}