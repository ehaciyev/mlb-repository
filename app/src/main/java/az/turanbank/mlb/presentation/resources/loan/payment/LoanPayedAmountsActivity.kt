package az.turanbank.mlb.presentation.resources.loan.payment

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.resources.loan.LoanPayedAmountModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_loan_payment_plan.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.util.*
import javax.inject.Inject

class LoanPayedAmountsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: LoanPayedAmountsViewModel

    private var paymentList = arrayListOf<LoanPayedAmountModel>()
    private val loanId: Long by lazy { intent.getLongExtra("loanId", 0L) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_payment_plan)

        viewModel =
            ViewModelProvider(this, factory)[LoanPayedAmountsViewModel::class.java]

        animateProgressImage()

        toolbar_title.text = getString(R.string.payments)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm
        val paymentAdapter =
            LoanPayedAmountsAdapter(paymentList) {

            }

        recyclerView.adapter = paymentAdapter
        setInputListeners()
        setOutputListeners()
    }

    private fun setInputListeners() {
        viewModel.inputs.getThePayments(loanId)
    }

    private fun setOutputListeners() {
        viewModel.outputs.onError().subscribe {
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
            failMessage.visibility = View.VISIBLE
            descriptionView.visibility = View.GONE
            recyclerView.visibility = View.GONE
            progress.clearAnimation()
            progress.visibility = View.GONE
        }.addTo(subscriptions)
        viewModel.onPaymentsSuccess().subscribe {
            failMessage.visibility = View.GONE
            updateRecyclerView(it)
            descriptionView.visibility = View.VISIBLE
            recyclerView.visibility = View.VISIBLE
            progress.clearAnimation()
            progress.visibility = View.GONE
        }.addTo(subscriptions)
    }

    private fun updateRecyclerView(payedAmountList: ArrayList<LoanPayedAmountModel>) {
        this.paymentList.clear()
        this.paymentList.addAll(payedAmountList)
        recyclerView.adapter?.notifyDataSetChanged()
    }
    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
