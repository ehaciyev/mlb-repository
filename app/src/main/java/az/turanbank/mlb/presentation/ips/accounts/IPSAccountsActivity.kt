package az.turanbank.mlb.presentation.ips.accounts

import android.content.Intent
import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.accounts.delete.DeleteIpsAccountActivity
import az.turanbank.mlb.presentation.ips.accounts.register.RegisterAccountIpsSystemActivity
import az.turanbank.mlb.presentation.ips.accounts.setdefault.alias.ChooseAliasForDefaultActivity
import kotlinx.android.synthetic.main.activity_ipsaccounts.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*

class IPSAccountsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ipsaccounts)
        toolbar_title.text = getString(R.string.my_accounts)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        add.setOnClickListener { startActivity(Intent(this, RegisterAccountIpsSystemActivity::class.java)) }
        delete.setOnClickListener { startActivity(Intent(this, DeleteIpsAccountActivity::class.java)) }
        setDefault.setOnClickListener { startActivity(Intent(this, ChooseAliasForDefaultActivity::class.java)) }
        //accountsContainer.setOnClickListener { startActivity(Intent(this, MyLinkedAccountsActivity::class.java )) }
    }
}
