package az.turanbank.mlb.presentation.register.mobile.individual

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_verify_individual_register_mobile.*
import kotlinx.android.synthetic.main.content_individual_register_asan.progressImage
import kotlinx.android.synthetic.main.content_individual_register_asan.submitButton
import kotlinx.android.synthetic.main.content_individual_register_asan.submitText
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.math.RoundingMode
import java.text.DecimalFormat
import javax.inject.Inject


class VerifyIndividualRegisterMobileActivity : BaseActivity() {

    private lateinit var viewModel: VerifyIndividualRegisterMobileViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory

    @Inject
    lateinit var resource: Resources
    private var counter = 0
    private val decimalFormat = DecimalFormat("00")
    private val countDown = object : CountDownTimer(180000, 1000) {

        @SuppressLint("SetTextI18n")
        override fun onTick(millisUntilFinished: Long) {
            decimalFormat.roundingMode = RoundingMode.CEILING
            timer.text =
                ((millisUntilFinished / 1000) / 60).toString() + ":" + decimalFormat.format((millisUntilFinished / 1000) % 60).toString()
        }

        override fun onFinish() {
            startActivity(
                Intent(
                    this@VerifyIndividualRegisterMobileActivity,
                    ErrorPageViewActivity::class.java
                )
            )
            finish()
        }
    }

    private val custId by lazy { intent.getLongExtra("custId", 0L) }
    private val mobile by lazy { intent.getStringExtra("mobile") }
    private val customerType by lazy { intent.getStringExtra("customerType") }
    private val pin by lazy { intent.getStringExtra("pin") }
    private val taxNo by lazy { intent.getStringExtra("taxNo") }
    private val goal by lazy { intent.getStringExtra("goal") }
    private val compId by lazy { intent.getLongExtra("compId", 0L) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_individual_register_mobile)

        viewModel = ViewModelProvider(this, viewModelFactory)[VerifyIndividualRegisterMobileViewModel::class.java]
        toolbar_title.text = resource.getString(R.string.register_with_mobile)
        setOutputListeners()
        setInputListeners()
        submitText.text = getString(R.string.confirm)
        countDown.start()
    }

    private fun setInputListeners() {
        val compID = if(compId == 0L){
            null
        } else {
            compId
        }
        submitButton.setOnClickListener {
            if (pinCodeView.text.toString().trim().isNotEmpty()) {
                showProgressBar(true)
                viewModel.inputs.verifyOtpCode(
                    custId,
                    compID,
                    pinCodeView.text.toString()
                )
            } else {
                AlertDialogMapper(this, 124004).showAlertDialog()
            }
        }
        number.text =
            resource.getString(R.string.code_sent_to, "+${mobile?.replaceRange(5, 10, "*****")}")

        sendOtpAgain.setOnClickListener {
            if (counter < 10) {
                viewModel.inputs.sendPinAgain(custId, compID, mobile)
                counter++
            } else {
                blockUser()
            }
        }

        toolbar_back_button.setOnClickListener { onBackPressed() }
    }

    private fun blockUser() {
        viewModel.inputs.blockUser(custId, compId)
    }

    private fun setOutputListeners() {
        viewModel.outputs.onCodeCorrect().subscribe {
            if (goal == "unblock") {
                if (customerType == "individual")
                    viewModel.inputs.unblockIndividualCustomer(custId)
                else
                    viewModel.inputs.unblockJuridicalCustomer(custId, compId)
            } else {
                val intent = Intent(this, IndividualRegisterMobileSubmitActivity::class.java)
                intent.putExtra("customerType", customerType)
                intent.putExtra("taxNo", taxNo)
                intent.putExtra("pin", pin)
                intent.putExtra("custId", custId)
                intent.putExtra("registrationType", "mobile")
                countDown.cancel()
                finish()
                startActivity(intent)
            }
        }.addTo(subscriptions)

        viewModel.outputs.onCustomerUnblocked().subscribe {
            Toast.makeText(this, R.string.user_successfully_unblocked, Toast.LENGTH_LONG).show()
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.onSendPinAgain().subscribe {
            if (it){
                pinCodeView.setText("")
                countDown.cancel()
                countDown.start()
                AlertDialog.Builder(this)
                    .setTitle(getString(R.string.error_message))
                    .setMessage(getString(R.string.new_otp_sent))
                    .setPositiveButton(R.string.ok) { _, _ -> }
                    .setCancelable(true)
                    .show()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            pinCodeView.setText("")
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
        viewModel.outputs.onCustomerBlocked().subscribe {
            startActivity(Intent(this@VerifyIndividualRegisterMobileActivity, ErrorPageViewActivity::class.java))
            finish()
        }.addTo(subscriptions)
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
//        asanPhoneNumber.isClickable = !show
//        asanId.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    override fun onDestroy() {
        super.onDestroy()
        countDown.cancel()
    }
}
