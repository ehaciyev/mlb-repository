package az.turanbank.mlb.presentation.resources.card.sms

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_add_sms_number.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class AddSmsNumberActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: AddSmsNumberViewModel

    private val cardId: Long by lazy { intent.getLongExtra("cardId", 0L) }

    private val edit: Boolean by lazy { intent.getBooleanExtra("edit", false) }
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_sms_number)

        viewModel = ViewModelProvider(this, factory)[AddSmsNumberViewModel::class.java]

        toolbar_title.text = getString(R.string.sms_service)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        setRepeatVisibility()
        setInputListeners()
        setOutputListeners()
    }

    private fun setRepeatVisibility() {
        if (edit) {
            repeatNumberHolder.visibility = View.GONE
        } else {
            repeatNumberHolder.visibility = View.VISIBLE
        }
    }

    private fun setOutputListeners() {
        viewModel.onError().subscribe {
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
        viewModel.onNumberAdded().subscribe {
            showProgressBar(false)
            if (it) {
                Toast.makeText(this, getString(R.string.number_added_to_sms_service), Toast.LENGTH_LONG)
                    .show()
                val intent = Intent()
                intent.putExtra("cardId", cardId)
                intent.putExtra("number", "994" + number.text.toString())
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {

        submitButton.setOnClickListener {
            showProgressBar(true)
            if (edit) {
                viewModel.inputs.enableSms(
                    "994"+number.text.toString(),
                    "994"+number.text.toString(),
                    cardId
                )
            } else {
                viewModel.inputs.enableSms(
                    "994"+number.text.toString(),
                    "994"+repeatNumber.text.toString(),
                    cardId
                )
            }
        }
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun showAlertDialog(stringResource: Int) {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.error_message))
            .setMessage(stringResource)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .setCancelable(true)
            .show()
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        number.isClickable = !show
        repeatNumber.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }
}
