package az.turanbank.mlb.presentation.exchange.converter.fragment.cashlessConverter

import az.turanbank.mlb.data.remote.model.exchange.CashlessResponseModel
import az.turanbank.mlb.data.remote.model.exchange.ConverterCurrencyListModel
import az.turanbank.mlb.data.remote.model.exchange.ExchangeCashlessResponseModel
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashlessUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.exchange.converter.CashlessConverterUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface CashlessConvertViewModelInputs : BaseViewModelInputs {
    fun convertCurrency(
        fromCurrency: CashlessResponseModel,
        toCurrency: CashlessResponseModel,
        currencyAmount: Double
    )

    fun getExchangValues()
}

interface CashlessConvertViewModelOutputs : BaseViewModelOutputs {
    fun onConvertGot(): PublishSubject<Double>
    fun onValuesFetched(): PublishSubject<ExchangeCashlessResponseModel>
}

class CashlessConvertViewModel @Inject constructor(
    private val exchangeCashlessUseCase: ExchangeCashlessUseCase,
    private val convertUtil: CashlessConverterUtil
) : BaseViewModel(),
    CashlessConvertViewModelInputs,
    CashlessConvertViewModelOutputs {


    override val inputs: CashlessConvertViewModelInputs = this

    override val outputs: CashlessConvertViewModelOutputs = this


    private val currencyList = PublishSubject.create<ConverterCurrencyListModel>()
    private val currencyValues = PublishSubject.create<ExchangeCashlessResponseModel>()

    private val converted = PublishSubject.create<Double>()

    override fun convertCurrency(
        fromCurrency: CashlessResponseModel,
        toCurrency: CashlessResponseModel,
        currencyAmount: Double
    ) {
        converted.onNext(convertUtil.convertedAmount(fromCurrency, toCurrency, currencyAmount))
    }

    override fun getExchangValues() {
        exchangeCashlessUseCase.execute("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    currencyValues.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }
            ).addTo(subscriptions)
    }

    override fun onConvertGot() = converted
    override fun onValuesFetched() = currencyValues
}