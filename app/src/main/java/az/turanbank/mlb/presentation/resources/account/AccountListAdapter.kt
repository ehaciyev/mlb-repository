package az.turanbank.mlb.presentation.resources.account

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.account.AccountListModel
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.formatDecimal
import az.turanbank.mlb.util.secondPartOfMoney
import kotlinx.android.synthetic.main.card_list_view.view.*

class AccountListAdapter(
    private val accountList: ArrayList<AccountListModel>,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<AccountListAdapter.AccountViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AccountViewHolder = AccountViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.card_list_view,
            parent,
            false
        )
    )

    override fun getItemCount() = accountList.size

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        holder.bind(accountList[position], clickListener)
    }

    class AccountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(account: AccountListModel, clickListener: (position: Int) -> Unit) {

            itemView.cardName.text = account.accountName
            itemView.cardNumber.text = account.iban
            itemView.cardAmount.text = firstPartOfMoney(account.currentBalance)
            itemView.cardAmountReminder.text = secondPartOfMoney(account.currentBalance)
            itemView.currency.text = account.currName

            itemView.setOnClickListener {
                clickListener.invoke(adapterPosition)
            }
        }
    }
}