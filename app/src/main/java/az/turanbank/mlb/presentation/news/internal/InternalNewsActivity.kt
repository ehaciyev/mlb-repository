package az.turanbank.mlb.presentation.news.internal

import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.home.CampaignViewModel
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_internal_news.*
import javax.inject.Inject

class InternalNewsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CampaignViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_internal_news)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            home_screen_toolbar.setTitleTextColor(getColor(R.color.white))
            home_screen_toolbar.setSubtitleTextColor(getColor(R.color.white))
        }
        setSupportActionBar(home_screen_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = ""
        viewModel =
            ViewModelProvider(this, factory)[CampaignViewModel::class.java]

        setInputListeners()
        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onCampaigSuccess().subscribe {
            val byteArray = Base64.decode(it.image, Base64.DEFAULT)

            val bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
            campaignImage.setImageBitmap(bmp)
            title = it.name
            titleText.text = it.name
            description.text = it.note
            progress.clearAnimation()
            progress.visibility = View.GONE
            campaign.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        animateProgressImage()
        viewModel.inputs.getTheCampaign(intent.getLongExtra("campaignId", 0L))
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        campaign.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
