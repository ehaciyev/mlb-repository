package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.GetAccountRequisitesResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.GetAccountRequisitesForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.GetAccountRequisitesForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountRequisitesActivityViewModelInputs : BaseViewModelInputs {
    fun getAccountRequisites(accountId: Long)
}

interface AccountRequisitesActivityViewModelOutputs : BaseViewModelOutputs {
    fun onAccountRequisitesSuccess(): PublishSubject<GetAccountRequisitesResponseModel>
    fun showProgress(): Observable<Boolean>
}

class AccountRequisitesActivityViewModel @Inject constructor(
    private val getAccountRequisitesForIndividualCustomerUseCase: GetAccountRequisitesForIndividualCustomerUseCase,
    private val getAccountRequisitesForJuridicalCustomerUseCase: GetAccountRequisitesForJuridicalCustomerUseCase,
    sharedPrefs: SharedPreferences
):
    BaseViewModel(),
    AccountRequisitesActivityViewModelInputs,
    AccountRequisitesActivityViewModelOutputs {

    override val inputs: AccountRequisitesActivityViewModelInputs = this
    override val outputs: AccountRequisitesActivityViewModelOutputs = this
    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val accountRequisites = PublishSubject.create<GetAccountRequisitesResponseModel>()
    private val showProgress = PublishSubject.create<Boolean>()

    override fun getAccountRequisites(accountId: Long) {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getAccountRequisitesForJuridicalCustomerUseCase
                .execute(
                    custId = custId,
                    compId = compId,
                    token = token,
                    accountId = accountId
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountRequisites.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getAccountRequisitesForIndividualCustomerUseCase
                .execute(
                   custId = custId,
                    accountId = accountId,
                    token = token
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountRequisites.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onAccountRequisitesSuccess() = accountRequisites

    override fun showProgress() = showProgress
}