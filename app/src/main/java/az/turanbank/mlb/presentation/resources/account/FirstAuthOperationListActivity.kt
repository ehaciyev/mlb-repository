package az.turanbank.mlb.presentation.resources.account

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.widget.ActionMenuView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.activity.SuccessfulPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_first_auth_operation_list.*
import kotlinx.android.synthetic.main.auth_activity_toolbar.*
import javax.inject.Inject

class FirstAuthOperationListActivity : BaseActivity() {

    lateinit var amvMenu: ActionMenuView

    lateinit var menuBuilder: MenuBuilder

    lateinit var viewmodel: FirstAuthOperationViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var dialog: MlbProgressDialog

    lateinit var adapter: FirstAuthOperationRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_auth_operation_list)

        dialog = MlbProgressDialog(this)

        viewmodel = ViewModelProvider(this, factory) [FirstAuthOperationViewModel::class.java]

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        firstAuthOpRecyclerView.layoutManager = llm

        val toolbar = findViewById<Toolbar>(R.id.auth_activity_toolbar)
        amvMenu = toolbar.findViewById(R.id.amvMenu)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        setSupportActionBar(toolbar)
        menuBuilder = amvMenu.menu as MenuBuilder

        adapter = FirstAuthOperationRecyclerViewAdapter(this, arrayListOf()) {

        }

        firstAuthOpRecyclerView.adapter = adapter

        setOutputListeners()
        setInputListeners()
    }

    private fun setInputListeners() {
        viewmodel.inputs.getFirstAuthOperationList()
    }

    private fun setOutputListeners() {

        viewmodel.outputs.onOperationNotFound().subscribe {
            if (it) {
                adapter.setData(arrayListOf())
            }
        }.addTo(subscriptions)

        viewmodel.outputs.authOperationListSuccess().subscribe{
            adapter.setData(it)

            menuBuilder.setCallback(object : MenuBuilder.Callback {
                override fun onMenuItemSelected(menu: MenuBuilder?, item: MenuItem?): Boolean {
                    return onOptionsItemSelected(item!!)
                }

                override fun onMenuModeChange(menu: MenuBuilder?) {
                }
            })

        }.addTo(subscriptions)

        viewmodel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.getFirstSignInfoSuccess().subscribe {getSignInfoSuccess ->
            val intent = Intent(this@FirstAuthOperationListActivity, SignFileFirstAuthActivity::class.java)
            intent.putExtra("successOperId",getSignInfoSuccess.successOperId)
            intent.putExtra("failOperId",getSignInfoSuccess.failOperId)
            intent.putExtra("transactionId", getSignInfoSuccess.transactionId)
            intent.putExtra("verificationCode", getSignInfoSuccess.verificationCode)
            intent.putExtra("batchId", adapter.getSelectedItem().batchId)

            startActivityForResult(intent, 9)
        }.addTo(subscriptions)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.first_auth_activity_menu, amvMenu.menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.sign_docs -> {
                viewmodel.inputs.getFirstSignInfo(adapter.getSelectedItem().batchId)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 9) {

            viewmodel.inputs.getFirstAuthOperationList()

            when(resultCode) {
                Activity.RESULT_OK -> {
                    val intent = Intent(this, SuccessfulPageViewActivity::class.java)
                    intent.putExtra("description", data?.getStringExtra("description"))
                    intent.putExtra("finishOnly", true)
                    startActivity(intent)
                }
                Activity.RESULT_CANCELED -> {
                    val intent = Intent(this, ErrorPageViewActivity::class.java)
                    intent.putExtra("description", data?.getStringExtra("description"))
                    startActivity(intent)
                }
            }
        }
    }
}
