package az.turanbank.mlb.presentation.ips.nonregistered.pay.debitor

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.AccountResponseModel
import az.turanbank.mlb.data.remote.model.response.GetAccountIdResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetUnregisteredIpsAccountsUseCase
import az.turanbank.mlb.domain.user.usecase.ips.nonregistered.GetAccountIdUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.data.SelectUnregisteredAccountModel
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface IPSAccountChoiceViewModelInputs : BaseViewModelInputs {
    fun getNonRegisteredAccounts()
    fun getSenderInfo(accountId: Long?)
}

interface IPSAccountChoiceViewModelOutputs : BaseViewModelOutputs {
    fun accountsSuccess(): PublishSubject<ArrayList<AccountResponseModel>>
    fun showProgress(): PublishSubject<Boolean>
    fun onSenderSuccess() : PublishSubject<GetAccountIdResponseModel>
}

class NonRegisteredIPSAccountChoiceViewModel @Inject constructor(
    private val getUnregisteredIpsAccountsUseCase: GetUnregisteredIpsAccountsUseCase,
    private val getAccountIdUseCase: GetAccountIdUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    IPSAccountChoiceViewModelInputs,
    IPSAccountChoiceViewModelOutputs {
    private val showProgress = PublishSubject.create<Boolean>()

    override fun showProgress() = showProgress
    override fun onSenderSuccess() = onSenderSuccess

    override val inputs: IPSAccountChoiceViewModelInputs = this

    override val outputs: IPSAccountChoiceViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private var selectedAccount: String? = null
    private var selectedAccountId: Long? = null

    private val accountList = PublishSubject.create<ArrayList<AccountResponseModel>>()
    private val accounts = arrayListOf<SelectUnregisteredAccountModel>()
    private val onSenderSuccess = PublishSubject.create<GetAccountIdResponseModel>()


    override fun getNonRegisteredAccounts() {
        showProgress.onNext(true)

        when(lang)
        {
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        getUnregisteredIpsAccountsUseCase
            .execute(custId, token, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({accountsResponse->
                showProgress.onNext(false)
                if (accountsResponse.status.statusCode == 1) {
                    accountList.onNext(accountsResponse.accountList)
                } else {
                    error.onNext(accountsResponse.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun getSenderInfo(accountId: Long?) {

        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        when(lang)
        {
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }

        getAccountIdUseCase
            .execute(compId,custId,token, accountId!!, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it.status.statusCode) {
                    1 -> {
                        onSenderSuccess.onNext(it)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            },{
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun accountsSuccess() = accountList

    fun setSelectedAccount(account: String?) {
        this.selectedAccount = account
    }

    fun setSelectedAccountId(accountId: Long?) {
        this.selectedAccountId = accountId
    }

    fun getSelectedAccountId() = this.selectedAccountId

    fun getSelectedAccount() = this.selectedAccount

}