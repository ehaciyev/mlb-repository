package az.turanbank.mlb.presentation.resources.account.exchange

import android.annotation.SuppressLint
import android.os.Bundle
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.account.AccountTemplateResponseModel
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_card_requisities.*
import kotlinx.android.synthetic.main.activity_convert_money.*

class ConvertMoneyActivity : BaseActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_convert_money)
        view_pager.adapter = CreateExchangePagerAdapter(
            this,
            supportFragmentManager
        )
        tabs.setupWithViewPager(view_pager)

        if(intent.getSerializableExtra("template") != null) {
            val tabPosition: Int =
                if ((intent.getSerializableExtra("template") as AccountTemplateResponseModel).operationTypeId == 5) {
                    0
                } else {
                    1
                }
            tabs.selectTab(tabs.getTabAt(tabPosition))
        }


        toolbar_title.text = getString(R.string.convertation)
        toolbar_back_button.setOnClickListener { onBackPressed() }
    }
}