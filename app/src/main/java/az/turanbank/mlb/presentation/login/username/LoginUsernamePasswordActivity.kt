package az.turanbank.mlb.presentation.login.username

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.forgot.individual.IndividualForgotPasswordActivity
import az.turanbank.mlb.presentation.forgot.juridical.JuridicalForgotPasswordActivity
import az.turanbank.mlb.presentation.login.activities.pin.LoginPinActivity
import az.turanbank.mlb.util.animateProgressImage
import az.turanbank.mlb.util.revertProgressImageAnimation
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_login_username_password.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class LoginUsernamePasswordActivity : BaseActivity() {

    lateinit var viewmodel: LoginUsernamePasswordViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private val customerType: String by lazy { intent.getStringExtra("customerType") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_username_password)

        viewmodel = ViewModelProvider(this, factory).get(LoginUsernamePasswordViewModel::class.java)

        toolbar_title.text = getString(R.string.login_with_username_and_password)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        setInputListeners()
        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewmodel.outputs.onUserLoggedIn().subscribe {
            /*startActivity(Intent(this, HomeActivity::class.java))
            finish()*/
        }.addTo(subscriptions)

        viewmodel.outputs.onUserBlocked().subscribe {
            showProgressBar(false)

            val intent = Intent(this, ErrorPageViewActivity::class.java)
            intent.putExtra("customerType", customerType)
            intent.putExtra("unblock" ,true)

            startActivity(intent)

            finish()
        }.addTo(subscriptions)

        viewmodel.outputs.onError().subscribe {
            Log.i("ERROR MAPPER", getString(ErrorMessageMapper().getErrorMessage(it)))
            showProgressBar(false)
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)

        viewmodel.outputs.showPinDialog().subscribe {
            showPinDialog()
        }.addTo(subscriptions)

        viewmodel.outputs.showPinScreen().subscribe {
            navigateToPinScreen(it)
        }.addTo(subscriptions)
    }

    private fun navigateToPinScreen(creatingNewPin: Boolean) {
        val intent = Intent(this@LoginUsernamePasswordActivity, LoginPinActivity::class.java)
        intent.putExtra("creatingNewPin", creatingNewPin)
        startActivity(intent)
        finish()
    }
    private fun showPinDialog() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.pin))
            .setMessage(getString(R.string.use_existing_pin_confirmation_message))
            .setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                navigateToPinScreen(false)
                dialog.dismiss()
            }
            .setNegativeButton(getString(R.string.no)) { dialog, _ ->
                dialog.dismiss()
                navigateToPinScreen(true)
            }
            .setCancelable(true)
            .show()
    }

    private fun setInputListeners() {
        submitButton.setOnClickListener {

            if(checkValidation()){
                showProgressBar(true)

                viewmodel.inputs.checkUsernameAndPassword(
                    username = username.text.toString(),
                    password = password.text.toString(),
                    customerType = customerType
                )
            }

        }
        if (customerType == "juridical") {
            forgot.setOnClickListener {
                startActivity(
                    Intent(
                        this,
                        JuridicalForgotPasswordActivity::class.java
                    )
                )
            }
        } else {
            forgot.setOnClickListener {
                startActivity(
                    Intent(
                        this,
                        IndividualForgotPasswordActivity::class.java
                    )
                )
            }
        }
    }

    private fun checkValidation(): Boolean {
        var check = true
        if(username.text.toString().isNullOrEmpty()){
            username.error = getString(R.string.please_enter_username)
            check = false
        }

        if(password.text.toString().isNullOrEmpty()){
            password.error = getString(R.string.please_enter_password)
            check = false
        }

        return check
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        username.isClickable = !show
        password.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }
}
