package az.turanbank.mlb.presentation.resources.account

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.BatchOperation
import kotlinx.android.synthetic.main.first_auth_operation_item.view.*

class FirstAuthOperationRecyclerViewAdapter(
    private val context: Context,
    private val operationList: ArrayList<BatchOperation>,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<FirstAuthOperationRecyclerViewAdapter.FirstAuthMultiSelectViewHolder>() {

    private var alreadySelectedItemExists = false
    private var selectedItemIndex = -1

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FirstAuthMultiSelectViewHolder =
        FirstAuthMultiSelectViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.first_auth_operation_item,
                parent,
                false
            )
        )

    override fun getItemCount() = operationList.size

    fun setData(list: ArrayList<BatchOperation>) {
        operationList.clear()
        operationList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: FirstAuthMultiSelectViewHolder, position: Int) {
        val batchOperation = operationList[position]

        holder.itemView.radioButton.isChecked = batchOperation.isSelected
        val docCountText = batchOperation.docCount.toString() + " sənəd"

        holder.itemView.firstAuthCrName.text = batchOperation.batchName
        holder.itemView.firstAuthDocCount.text = docCountText
        holder.itemView.firstAuthOpType.text = batchOperation.batchType
        holder.itemView.firstAuthCreatedDate.text = batchOperation.createdDate

        holder.itemView.setOnClickListener {
            if (!alreadySelectedItemExists) {
                alreadySelectedItemExists = true
                batchOperation.isSelected = true
                selectedItemIndex = position
                notifyDataSetChanged()
            } else {
                operationList[selectedItemIndex].isSelected = false
                batchOperation.isSelected = true
                selectedItemIndex = position
                notifyDataSetChanged()
            }
        }
    }

    fun getSelectedItem() = operationList[selectedItemIndex]

    class FirstAuthMultiSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}