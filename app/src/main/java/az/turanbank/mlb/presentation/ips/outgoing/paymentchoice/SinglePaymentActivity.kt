package az.turanbank.mlb.presentation.ips.outgoing.paymentchoice

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Base64
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_single_operation_details.*
import kotlinx.android.synthetic.main.activity_single_payment.*
import kotlinx.android.synthetic.main.activity_single_payment.detOpStatusImage
import kotlinx.android.synthetic.main.activity_single_payment.detailOpClose
import kotlinx.android.synthetic.main.activity_single_payment.operationActionDelete
import kotlinx.android.synthetic.main.activity_single_payment.operationActionDownloadAdoc
import kotlinx.android.synthetic.main.activity_single_payment.operationActionDownloadPdf
import java.io.*
import javax.inject.Inject


class SinglePaymentActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: SinglePaymentViewModel

    lateinit var dialog: MlbProgressDialog


    private val paymentItem by lazy { intent.getLongExtra("paymentItem", 0L) }
    private val operName by lazy { intent.getStringExtra("operName") }
    private val dtIban by lazy { intent.getStringExtra("dtIban") }
    private val crName by lazy { intent.getStringExtra("crName") }
    private val crIban by lazy { intent.getStringExtra("crIban") }
    private val amount by lazy { intent.getStringExtra("amount") }
    private val operPurpose by lazy { intent.getStringExtra("operPurpose") }
    private val currency by lazy { intent.getStringExtra("currency") }
    private val createdDate by lazy { intent.getStringExtra("createdDate") }
    private val operStateId by lazy { intent.getIntExtra("operStateId",1) }
    private val operTypeId by lazy { intent.getIntExtra("operTypeId", 0) }
    private val operState by lazy { intent.getStringExtra("operState") }

    private var enableDelete = true
    private var enableCancel = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_payment)

        dialog = MlbProgressDialog(this)

        detailOpClose.setOnClickListener {
            finish()
        }

        viewModel = ViewModelProvider(this, factory)[SinglePaymentViewModel::class.java]

        when (operStateId) {
            1 -> {
                disable(operationActionCancel2)
                enableCancel = false
                disable2(operationActionCancel)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_blue
                    )
                )

            }
            2 -> {
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_blue
                    )
                )
            }
            6 -> {
                disable(operationActionCancel2,operationActionDelete2)
                enableCancel = false
                enableDelete = false
                disable2(operationActionCancel,operationActionDelete)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_yellow
                    )
                )
            }
            7 -> {

                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_black
                    )
                )
            }
            3 -> {

                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_yellow
                    )
                )
            }

            4 -> {
                disable(operationActionDelete2)
                enableDelete = false
                disable2(operationActionDelete)
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_yellow
                    )
                )
            }
            5 -> {
                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_green
                    )
                )
            }
            8 -> {

                detOpStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.group_red
                    )
                )
            }
        }

        detailTitle.text = operState
        nameSurname.text = crName
        debitNumber.text = dtIban
        creditNumber.text = crIban
        amount_money.text = amount
        comission.text = operPurpose
        exchange.text = currency


        if(enableDelete){
            operationActionDelete.setOnClickListener{
                AlertDialog.Builder(this)
                    .setTitle(getString(R.string.attention))
                    .setMessage(getString(R.string.delete_confirmation_message))
                    .setPositiveButton(getString(R.string.yes)) { _, _ ->
                        viewModel.inputs.deleteOperation(paymentItem)
                    }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
            }
        }

        if(enableCancel){
            operationActionCancel.setOnClickListener {
                val dialogBuilder = AlertDialog.Builder(this).create()
                val inflater = this.layoutInflater
                val dialogView: View = inflater.inflate(R.layout.custom_alert_dialog, null)

                val editText: EditText = dialogView.findViewById(R.id.edt_comment)
                val yesText: TextView = dialogView.findViewById(R.id.textSubmit)
                val noText: TextView = dialogView.findViewById(R.id.textCancel)

                yesText.setOnClickListener{
                    dialogBuilder.dismiss()
                }
                noText.setOnClickListener{
                    dialogBuilder.dismiss()
                }
                dialogBuilder.setView(dialogView)
                dialogBuilder.show()
            }
        }


        operationActionDownloadAdoc.setOnClickListener {
            if (isStoragePermissionGranted(2)) {
                viewModel.inputs.getAsanDocByOperId(paymentItem)
            }
        }

        operationActionDownloadPdf.setOnClickListener {
            if (isStoragePermissionGranted(1)) {
                viewModel.inputs.getPaymentDocByOperId(paymentItem)
            }
        }

        setOutputListener()
        setInputListener()
    }

    private fun setInputListener() {

    }

    private fun setOutputListener() {
        viewModel.outputs.paymentDoc().subscribe {
            val fileName = "$paymentItem.pdf"
            val file = getFileDestination(fileName)
            //  val destPath = getExternalFilesDir(null)?.absolutePath
            //  val file = File(destPath, fileName)

            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                inputStream = ByteArrayInputStream(byteArray)

                file?.let {f ->
                    outputStream = FileOutputStream(f)
                }

                while (true) {
                    val read = inputStream.read(fileReader)

                    if (read == -1) {
                        break
                    }
                    outputStream?.write(fileReader, 0, read)
                }

                Toast.makeText(this, fileName + getString(R.string.successfully_downloaded), Toast.LENGTH_LONG)
                    .show()

                openDownloadedPdfFile(file)

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)

        viewModel.outputs.paymentADoc().subscribe {
            val fileName = "$paymentItem.adoc"

            //  val filePath = File(Environment.getExternalStorageDirectory(), null)
            val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName)
            //   val destPath = getExternalFilesDir(null)?.absolutePath
            //   val file = File(destPath, fileName)

            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)

            var iStream: InputStream? = null
            var oStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                iStream = ByteArrayInputStream(byteArray)
                oStream = FileOutputStream(file)

                while (true) {
                    val read = iStream.read(fileReader)

                    if (read == -1) {
                        break
                    }
                    oStream.write(fileReader, 0, read)
                }

                Toast.makeText(this, fileName + getString(R.string.successfully_downloaded), Toast.LENGTH_LONG)
                    .show()

                openDirectoryIntent(file)

                //openAdocFolder(Environment.getExternalStorageDirectory())

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                iStream?.close()
                oStream?.close()
            }
        }.addTo(subscriptions)

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.operationDeleted().subscribe{

            setResult(Activity.RESULT_OK, Intent())
            finish()
        }.addTo(subscriptions)
    }

    private fun disable(vararg actions: LinearLayout) {
        for(action in actions) {
            action.getChildAt(0).isClickable = false
            action.background = ContextCompat.getDrawable(this, R.drawable.operation_action_disabled_state)
            action.isClickable = false
            action.isFocusable = false
        }
    }

    private fun disable2(vararg actions: LinearLayout) {
        for(action in actions) {
            action.getChildAt(0).isClickable = false
            action.isClickable = false
            action.isFocusable = false
        }
    }

    private fun isStoragePermissionGranted(requestCode: Int): Boolean {
        return if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED
            )
                true
            else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    requestCode
                )
                false
            }
        } else {
            true
        }
    }


    fun openDirectoryIntent(file: File) {
        val selectedUri = FileProvider.getUriForFile(this, "$packageName.fp", file)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(selectedUri, "*/*")
        startActivity(intent)
    }

    private fun openDownloadedPdfFile(file: File?) {
        try {
            val apkURI = FileProvider.getUriForFile(
                this,
                applicationContext
                    .packageName + ".provider", file!!
            )

            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(apkURI, "application/pdf")
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val requestedPermissionsCount = permissions.size
        var grantedPermissionsCount = 0

        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                grantedPermissionsCount++
            }
        }

        if (requestedPermissionsCount == grantedPermissionsCount) {
            if (requestCode == 1) {
                viewModel.inputs.getPaymentDocByOperId(paymentItem)
            } else if (requestCode == 2) {
                viewModel.inputs.getAsanDocByOperId(paymentItem)
            }
        }
    }

    private fun getFileDestination(fileName: String): File? {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            return File(Environment.getExternalStorageDirectory(), fileName)
        }
        return getExternalFilesDir(null)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 88) {
            if (resultCode == Activity.RESULT_OK) {
                //viewModel.inputs.getOperationById(intent.getLongExtra("operId", 0L))

                detailOpClose.setOnClickListener {
                    setResult(resultCode)
                    finish()
                }
            }
        }
    }
}
