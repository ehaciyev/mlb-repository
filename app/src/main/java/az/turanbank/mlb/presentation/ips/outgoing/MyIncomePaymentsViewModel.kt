package az.turanbank.mlb.presentation.ips.outgoing

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.OperationModel
import az.turanbank.mlb.domain.user.usecase.ips.mpayments.IPSPaymentsUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface MyIncomePaymentsViewModelInputs : BaseViewModelInputs {
    fun getPayments()
}

interface MyIncomePaymentsViewModelOutputs : BaseViewModelOutputs {
    fun onPaymentsArrived(): PublishSubject<ArrayList<OperationModel>>
}

class MyIncomePaymentsViewModel @Inject constructor(
    private val ipsPaymentsUseCase: IPSPaymentsUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    MyIncomePaymentsViewModelInputs,
    MyIncomePaymentsViewModelOutputs {

    override val inputs: MyIncomePaymentsViewModel = this

    override val outputs: MyIncomePaymentsViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)

    private val compId = sharedPrefs.getLong("compId", 0L)

    val lang = sharedPrefs.getString("lang", "az")
    var enumLangType = EnumLangType.AZ

    private val payments = PublishSubject.create<ArrayList<OperationModel>>()

    override fun getPayments() {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        val compId = if (isCustomerJuridical){
            compId
        } else {
            null
        }

        ipsPaymentsUseCase
            .execute(custId, compId, token, 5, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    payments.onNext(it.operationList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onPaymentsArrived() = payments
}