package az.turanbank.mlb.presentation.resources.loan.paymentplan

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.resources.loan.LoanPaymentPlanModel
import kotlinx.android.synthetic.main.payment_plan_list.view.*

class LoanPaymentPlanAdapter (private val paymentPlanList: ArrayList<LoanPaymentPlanModel>, private val clickListener: (position: Int) -> Unit) : RecyclerView.Adapter<LoanPaymentPlanAdapter.LoanPaymentListViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): LoanPaymentListViewHolder =
        LoanPaymentListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.payment_plan_list,
                parent,
                false
            )
        )

    override fun getItemCount() = paymentPlanList.size

    override fun onBindViewHolder(holder: LoanPaymentListViewHolder, position: Int) {
        holder.bind(paymentPlanList[position], clickListener)
    }
    class LoanPaymentListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(loansPayments: LoanPaymentPlanModel, clickListener: (position: Int) -> Unit) {
            with(loansPayments) {
                itemView.date.text = this.payDate
                itemView.amount.text = this.totalPay.toString()
                itemView.mainLoan.text = this.rest.toString()
                itemView.setOnClickListener {
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}