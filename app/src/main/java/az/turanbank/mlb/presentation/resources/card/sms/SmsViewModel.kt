package az.turanbank.mlb.presentation.resources.card.sms

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.resources.card.sms.GetSmsNotificationNumberForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.card.sms.GetSmsNotificationNumberForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface SmsViewModelInputs: BaseViewModelInputs{
        fun getNumber(cardId: Long)
}
interface SmsViewModelOutputs: BaseViewModelOutputs{
        fun onNumberSuccess(): PublishSubject<String>
        fun onNumberNotFound(): PublishSubject<Boolean>
}

class SmsViewModel @Inject constructor(
        private val getSmsNotificationNumberForIndividualCustomerUseCase: GetSmsNotificationNumberForIndividualCustomerUseCase,
        private val getSmsNotificationNumberForJuridicalCustomerUseCase: GetSmsNotificationNumberForJuridicalCustomerUseCase,
        sharedPrefs: SharedPreferences
) : BaseViewModel(),
        SmsViewModelInputs,
        SmsViewModelOutputs{
        override val inputs: SmsViewModelInputs = this

        override val outputs: SmsViewModelOutputs = this


        private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

        val token = sharedPrefs.getString("token", "")
        val custId = sharedPrefs.getLong("custId", 0L)
        val compId = sharedPrefs.getLong("compId", 0L)
        val lang = sharedPrefs.getString("lang","az")
        var enumLangType = EnumLangType.AZ

        private val number = PublishSubject.create<String>()
        private val noNumber = PublishSubject.create<Boolean>()

        override fun getNumber(cardId: Long) {

                when(lang){
                        "az" -> enumLangType = EnumLangType.AZ
                        "ru" -> enumLangType = EnumLangType.RU
                        "en" -> enumLangType = EnumLangType.EN
                }
                if (isCustomerJuridical){
                        getSmsNotificationNumberForJuridicalCustomerUseCase
                                .execute(custId, compId, cardId, token,enumLangType)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                        if (it.status.statusCode == 1) {
                                                number.onNext(it.mobile)
                                                noNumber.onNext(false)
                                        } else if (it.status.statusCode == 136 || it.status.statusCode == 212){
                                                noNumber.onNext(true)
                                        } else {
                                                error.onNext(it.status.statusCode)
                                        }
                                }, {
                                }).addTo(subscriptions)
                } else {
                        getSmsNotificationNumberForIndividualCustomerUseCase
                                .execute(custId, cardId, token,enumLangType)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                        if (it.status.statusCode == 1) {
                                                number.onNext(it.mobile)
                                                noNumber.onNext(false)
                                        } else if (it.status.statusCode == 136 || it.status.statusCode == 212){
                                                noNumber.onNext(true)
                                        }else {
                                                error.onNext(it.status.statusCode)
                                        }
                                }, {
                                }).addTo(subscriptions)
                }
        }

        override fun onNumberSuccess() = number

        override fun onNumberNotFound() = noNumber
}