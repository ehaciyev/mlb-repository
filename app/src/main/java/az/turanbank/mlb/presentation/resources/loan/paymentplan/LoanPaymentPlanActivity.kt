package az.turanbank.mlb.presentation.resources.loan.paymentplan

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.resources.loan.LoanPaymentPlanModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_card_requisities.toolbar_back_button
import kotlinx.android.synthetic.main.activity_card_requisities.toolbar_title
import kotlinx.android.synthetic.main.activity_loan_payment_plan.*
import java.util.*
import javax.inject.Inject

class LoanPaymentPlanActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: LoanPaymentPlanViewModel

    private var paymentList = arrayListOf<LoanPaymentPlanModel>()
    private val loanId: Long by lazy { intent.getLongExtra("loanId", 0L) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_payment_plan)

        viewModel =
            ViewModelProvider(this, factory)[LoanPaymentPlanViewModel::class.java]

        animateProgressImage()
        toolbar_title.text = getString(R.string.payment_schedule)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm
        val paymentAdapter =
            LoanPaymentPlanAdapter(this.paymentList) {

            }

        recyclerView.adapter = paymentAdapter
        setInputListeners()
        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onError().subscribe {
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
            failMessage.visibility = View.VISIBLE
            descriptionView.visibility = View.GONE
            recyclerView.visibility = View.GONE
            progress.clearAnimation()
            progress.visibility = View.GONE
        }.addTo(subscriptions)
        viewModel.onPaymentPlanSuccess().subscribe {
            failMessage.visibility = View.GONE
            updateRecyclerView(it)
            descriptionView.visibility = View.VISIBLE
            recyclerView.visibility = View.VISIBLE
            progress.clearAnimation()
            progress.visibility = View.GONE
        }.addTo(subscriptions)
    }

    private fun updateRecyclerView(paymentPlanList: ArrayList<LoanPaymentPlanModel>) {
        this.paymentList.clear()
        this.paymentList.addAll(paymentPlanList)
        recyclerView.adapter?.notifyDataSetChanged()
    }

    private fun setInputListeners() {
        viewModel.inputs.getPaymentPlan(loanId)
    }
    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
