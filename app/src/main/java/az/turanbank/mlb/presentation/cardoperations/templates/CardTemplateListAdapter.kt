package az.turanbank.mlb.presentation.cardoperations.templates

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.cardoperations.CardTemplateModel
import az.turanbank.mlb.util.formatDecimal
import kotlinx.android.synthetic.main.card_template_list_view.view.*

class CardTemplateListAdapter (private val templateList: ArrayList<CardTemplateModel>, private val clickListener: (position: Int) -> Unit) : RecyclerView.Adapter<CardTemplateListAdapter.CardViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CardViewHolder = CardViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.card_template_list_view,
            parent,
            false))

    override fun getItemCount() = templateList.size

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        holder.bind(templateList[position], clickListener)
    }
    class CardViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(template: CardTemplateModel, clickListener: (position: Int) -> Unit) {

            itemView.name.text = template.tempName
            itemView.amountInteger.text = template.amount.toString().split(".")[0]+","
            itemView.amountReminder.text = template.amount.toString().split(".")[1]+" AZN"
            itemView.operationType.text = template.cardOperationTypeValue
            itemView.cardNumber.text = template.requestorCardNumber.replaceRange(4,12, " **** **** ")

//            itemView.cardAmount.text = "${card.currentBalance.toString().split(".")[0]},"
//            itemView.currency.text = card.currentBalance.toString().split(".")[1] +" "+ card.currName
            itemView.setOnClickListener {
                clickListener.invoke(adapterPosition)
            }
        }
    }
}