package az.turanbank.mlb.presentation.ips.pay.debitorchoice.alias

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.request.EnumAliasType
import az.turanbank.mlb.data.remote.model.ips.response.GetAccountAndCustomerInfoByAliasResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.IPSAccountResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import az.turanbank.mlb.domain.user.usecase.ips.aliases.GetIpsAccountByAliasUseCase
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetMLBAliasesUseCase
import az.turanbank.mlb.domain.user.usecase.ips.pay.GetAccountAndCustomerInfoByAliasUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

interface IPSAliasChoiceViewModelInputs : BaseViewModelInputs {
    fun getAliases()
    fun getAccountAndCustomerInfoByAlias(aliasTypeId:EnumAliasType, value:String)
}

interface IPSAliasChoiceViewModelOutputs : BaseViewModelOutputs {
    fun aliasesSuccess(): PublishSubject<ArrayList<MLBAliasResponseModel>>
    fun showProgress(): PublishSubject<Boolean>
    fun onAccountInfoByAliasNotFound(): Subject<Int>
    fun onAccountAndCustomerInfoByAlias(): PublishSubject<GetAccountAndCustomerInfoByAliasResponseModel>
}

class IPSAliasChoiceViewModel @Inject constructor(
    private val getMLBAliasesUseCase: GetMLBAliasesUseCase,
    private val getAccountAndCustomerInfoByAliasUseCase: GetAccountAndCustomerInfoByAliasUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    IPSAliasChoiceViewModelInputs,
    IPSAliasChoiceViewModelOutputs {
    override val inputs: IPSAliasChoiceViewModelInputs = this

    override val outputs: IPSAliasChoiceViewModelOutputs = this

    private val showProgress = PublishSubject.create<Boolean>()

    override fun showProgress() = showProgress

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private var selectedAliasId: Long? = null
    private var selectedAliasName: String? = null
    private var selectedAliasType: EnumAliasType = EnumAliasType.EMAIL

    private val aliasList = PublishSubject.create<ArrayList<MLBAliasResponseModel>>()

    private val accountAndCustomerInfoByAlias = PublishSubject.create<GetAccountAndCustomerInfoByAliasResponseModel>()

    private val accountInfoByAliasNotFound: Subject<Int> = PublishSubject.create()

    override fun getAliases() {
        val compId = if (isCustomerJuridical) {
            compId
        } else {
            null
        }
        showProgress.onNext(true)
        getMLBAliasesUseCase.execute(custId, compId, token, EnumLangType.AZ)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    aliasList.onNext(it.aliases)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun getAccountAndCustomerInfoByAlias(aliasTypeId:EnumAliasType, value:String) {
        showProgress.onNext(true)
        getAccountAndCustomerInfoByAliasUseCase
            .execute(custId,token,aliasTypeId,value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                when (it.status.statusCode) {
                    1 -> {
                        accountAndCustomerInfoByAlias.onNext(it)
                    }
                    33311 -> {
                        accountInfoByAliasNotFound.onNext(it.status.statusCode)
                    }
                    else -> {
                        error.onNext(it.status.statusCode)
                    }
                }
            },{
                error.onNext(1878)
            }).addTo(subscriptions)
    }


    override fun aliasesSuccess() = aliasList

    fun setSelectedAliasId(accountId: Long?) {
        this.selectedAliasId = accountId
    }

    fun getSelectedAliasName() = this.selectedAliasName

    fun setSelectedAliasName(selectedAliasName: String?) {
        this.selectedAliasName = selectedAliasName
    }

    fun getSelectedAliasId() = this.selectedAliasId

    override fun onAccountInfoByAliasNotFound() = accountInfoByAliasNotFound

    override fun onAccountAndCustomerInfoByAlias() = accountAndCustomerInfoByAlias
    fun getSelectedAliasType() = selectedAliasType

    fun setSelectedAliasType(list: Int){
        when(list){
            0 -> {selectedAliasType = EnumAliasType.PIN}
            1 -> {selectedAliasType = EnumAliasType.TIN}
            2 -> {selectedAliasType = EnumAliasType.EMAIL}
            3 -> {selectedAliasType = EnumAliasType.MOBILE}
        }
    }

}