package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.request.GetSignInfoResponseModel
import az.turanbank.mlb.data.remote.model.response.AuthOperation
import az.turanbank.mlb.domain.user.usecase.GetSignInfoUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetAuthOperationListIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetSecondAuthOperationListUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetSecondSignInfoUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AuthMultiDocsViewModelInputs : BaseViewModelInputs {
    fun getAuthOperationList()
    fun getSignInfo(operationList: ArrayList<AuthOperation>)
}

interface AuthMultiDocsViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun getAuthOperationListSuccess(): PublishSubject<ArrayList<AuthOperation>>
    fun getSignInfoSuccess(): PublishSubject<GetSignInfoResponseModel>
    fun onOperationNotFound(): PublishSubject<Boolean>
}

class AuthMultiDocsViewModel @Inject constructor(
    private val sharedPrefs: SharedPreferences,
    private val getAuthOperationListIndividualUseCase: GetAuthOperationListIndividualUseCase,
    private val getSecondAuthOperationListUseCase: GetSecondAuthOperationListUseCase,
    private val getSignInfoUseCase: GetSignInfoUseCase,
    private val getSecondSignInfoUseCase: GetSecondSignInfoUseCase
) :
    BaseViewModel(),
    AuthMultiDocsViewModelOutputs,
    AuthMultiDocsViewModelInputs {

    override fun onOperationNotFound() = operationNotFound

    override val inputs: AuthMultiDocsViewModelInputs = this
    override val outputs: AuthMultiDocsViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val phoneNumber = sharedPrefs.getString("phoneNumber", "")
    val userId = sharedPrefs.getString("userId", "")
    val certCode = sharedPrefs.getString("certCode", "")

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val operationList = PublishSubject.create<ArrayList<AuthOperation>>()
    private val showProgress = PublishSubject.create<Boolean>()
    private val getSignInfoSuccess = PublishSubject.create<GetSignInfoResponseModel>()
    private val operationNotFound = PublishSubject.create<Boolean>()

    val signCount = sharedPrefs.getInt("signCount", 0)
    val signLevel = sharedPrefs.getInt("signLevel", 0)

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    override fun getSignInfo(operationList: ArrayList<AuthOperation>) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        showProgress.onNext(true)
        val operationIds = arrayListOf<Long?>()

        for (op in operationList) {
            operationIds.add(op.operId)
        }

        if (isCustomerJuridical) {
            getSecondSignInfoUseCase.execute(
                operationIds,
                custId, compId, token, phoneNumber, userId, enumLangType, certCode
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        getSignInfoSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getSignInfoUseCase.execute(
                operationIds,
                custId, token, phoneNumber, userId, enumLangType, certCode
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        getSignInfoSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun getSignInfoSuccess() = getSignInfoSuccess

    override fun showProgress() = showProgress

    override fun getAuthOperationList() {
        showProgress.onNext(true)
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        if (!isCustomerJuridical) {
            getAuthOperationListIndividualUseCase.execute(custId, token,enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        operationList.onNext(it.operationList)
                    } else {
                        operationNotFound.onNext(true)
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else if (signCount == signLevel) {
            showProgress.onNext(true)
            getSecondAuthOperationListUseCase.execute(custId, compId, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        operationList.onNext(it.operationList)
                    } else {
                        operationNotFound.onNext(true)
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                })
                .addTo(subscriptions)
        }
    }

    override fun getAuthOperationListSuccess() = operationList
}