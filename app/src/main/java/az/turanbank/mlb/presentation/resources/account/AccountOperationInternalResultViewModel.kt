package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.resources.account.AddOperationTemplateIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.AddOperationTemplateJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountOperationInternalResultViewModelOutputs : BaseViewModelOutputs {
    fun showTax(): PublishSubject<Boolean>
    fun saveTemplateSuccess(): CompletableSubject
}

interface AccountOperationInternalResultViewModelInputs : BaseViewModelInputs {
    fun saveTemplate(tempName: String, operId: Long)
}

class AccountOperationInternalResultViewModel @Inject constructor(
    private val sharedPrefs: SharedPreferences,
    private val addOperationTemplateIndividualUseCase: AddOperationTemplateIndividualUseCase,
    private val addOperationTemplateJuridicalUseCase: AddOperationTemplateJuridicalUseCase
) : BaseViewModel(),
    AccountOperationInternalResultViewModelOutputs,
    AccountOperationInternalResultViewModelInputs {
    private val showTax = PublishSubject.create<Boolean>()

    override val outputs: AccountOperationInternalResultViewModelOutputs = this

    override val inputs: AccountOperationInternalResultViewModelInputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val templateSaved = CompletableSubject.create()
    override fun showTax(): PublishSubject<Boolean> {
        showTax.onNext(sharedPrefs.getBoolean("isCustomerJuridical", false))
        return showTax
    }

    override fun saveTemplate(tempName: String, operId: Long) {
        if (isCustomerJuridical) {
            addOperationTemplateJuridicalUseCase
                .execute(custId, compId, tempName, token, operId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        templateSaved.onComplete()
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            addOperationTemplateIndividualUseCase
                .execute(custId, tempName, token, operId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1){
                        templateSaved.onComplete()
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun saveTemplateSuccess() = templateSaved
}