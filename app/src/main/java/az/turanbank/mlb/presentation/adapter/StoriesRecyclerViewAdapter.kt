package az.turanbank.mlb.presentation.adapter

import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.CampaignModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.mlb_story_item.view.*


class StoriesRecyclerViewAdapter(
    private val storyList: ArrayList<CampaignModel>,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<StoriesRecyclerViewAdapter.StoryViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        StoryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.mlb_story_item,
                parent,
                false
            )
        )

    override fun getItemCount() = storyList.size

    override fun onBindViewHolder(holder: StoryViewHolder, position: Int) =
        holder.bind(storyList[position], clickListener)

    class StoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(stories: CampaignModel, clickListener: (position: Int) -> Unit) {
            with(stories) {
                val byteArray = Base64.decode(this.image, Base64.DEFAULT)
                Glide.with(itemView)
                    .load(byteArray)
                    .centerCrop()
                    .into(itemView.storyImage)

                itemView.setOnClickListener {
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }

}