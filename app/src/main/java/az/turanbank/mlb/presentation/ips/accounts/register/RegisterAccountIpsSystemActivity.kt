package az.turanbank.mlb.presentation.ips.accounts.register

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.ips.response.IPSMLBAccountResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.data.SelectUnregisteredAccountModel
import az.turanbank.mlb.presentation.ips.accounts.authorization.IPSAuthorizationActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_ips_account_multi_select.*
import kotlinx.android.synthetic.main.activity_ips_account_multi_select.progress
import kotlinx.android.synthetic.main.activity_ips_account_multi_select.progressImage
import kotlinx.android.synthetic.main.activity_ips_account_multi_select.recyclerView
import kotlinx.android.synthetic.main.activity_ips_account_multi_select.submitButton
import kotlinx.android.synthetic.main.activity_ips_account_multi_select.submitText
import kotlinx.android.synthetic.main.activity_payed_amounts.*
import kotlinx.android.synthetic.main.activity_sign_file_first_auth.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class RegisterAccountIpsSystemActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: RegisterAccountIpsSystemViewModel

    lateinit var adapter: UnregisteredAccountSelectingAdapter
    private val accountList = arrayListOf<SelectUnregisteredAccountModel>()

    private var selectedAccountId = arrayListOf<Long>()

    private var createdAccoutId = arrayListOf<Long>()

    var ibans = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ips_account_multi_select)

        viewModel =
            ViewModelProvider(this, factory)[RegisterAccountIpsSystemViewModel::class.java]
        defaultViewContainer.visibility = View.GONE
        toolbar_title.text = getString(R.string.accounts)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        recyclerViewShimmer.visibility = View.VISIBLE
        recyclerViewShimmer.startShimmerAnimation()

        submitText.setText(R.string.continue_login)

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        //animateProgressImage()
        adapter = UnregisteredAccountSelectingAdapter(accountList, this){
            accountList[it].checked = !accountList[it].checked
            adapter.notifyDataSetChanged()
        }

        recyclerView.adapter = adapter

        setOutputListeners()
        setInputListeners()

    }

    private fun setInputListeners() {
        viewModel.inputs.getAccounts()

        val accountsToRegisterIpsSystem = arrayListOf<Long>()
        submitButton.setOnClickListener {

            var ibans = ""
            var isSelected = false
            val list = arrayListOf<SelectUnregisteredAccountModel>()
            createdAccoutId = arrayListOf()
            accountList.forEach {
                if (it.checked) {
                    isSelected = true
                    list.add(it)
                    createdAccoutId.add(it.account.accountId)
                }
            }

            if (createdAccoutId.size != 0) {
                animateProgressImage()
                //viewModel.inputs.registerAccounts(createdAccoutId)
                var intent = Intent(this, IPSAuthorizationActivity::class.java)
                intent.putExtra("createdAccoutId", createdAccoutId)
                startActivityForResult(intent,7)
                progressImage.clearAnimation()
                progress.visibility = View.GONE
            } else {
                AlertDialogMapper(this, 8000).showAlertDialog()
            }


            /*animateProgressImage()
            accountList.forEach {
                if(it.checked){
                    accountsToRegisterIpsSystem.add(it.account.accountId)
                }
            }
            viewModel.inputs.registerAccounts(accountsToRegisterIpsSystem)*/
        }
    }

    private fun setOutputListeners() {
        viewModel.outputs.onAccountListSuccess().subscribe {
            noDataAvailable.visibility = View.GONE
            recyclerViewShimmer.visibility = View.INVISIBLE
            recyclerViewShimmer.stopShimmerAnimation()
            progress.clearAnimation()
            progress.visibility = View.GONE

            viewGroupContainer.visibility = View.VISIBLE
            accountList.clear()
            accountList.addAll(it)
            recyclerView.adapter?.notifyDataSetChanged()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            recyclerViewShimmer.visibility = View.INVISIBLE
            recyclerViewShimmer.stopShimmerAnimation()
            progress.clearAnimation()
            progress.visibility = View.GONE
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
        viewModel.outputs.accountsRegistered().subscribe {
            recyclerViewShimmer.visibility = View.INVISIBLE
            recyclerViewShimmer.stopShimmerAnimation()

            progress.clearAnimation()
            progress.visibility = View.GONE
            Toast.makeText(this, R.string.accounts_successfully_registered, Toast.LENGTH_LONG).show()
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.unCreatedAccounts().subscribe {

            it.forEach { dlt ->
                selectedAccountId.add(dlt.accountId)
            }

            for(i in 0 until accountList.size){
                for (j in 0 until selectedAccountId.size){
                    if(accountList[i].account.accountId == selectedAccountId[j])
                        ibans =  accountList[i].account.iban + "\n" + ibans
                }
            }

            progress.clearAnimation()
            progress.visibility = View.GONE

            AlertDialog.Builder(this)
                .setTitle(R.string.delete_error)
                .setMessage(ibans)
                .setPositiveButton(R.string.ok) { _, _ ->
                    viewModel.inputs.registerAccounts(createdAccoutId)
                }
                .setCancelable(false)
                .show()
        }.addTo(subscriptions)
    }
    private fun animateProgressImage() {
        progressImage.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progressImage.startAnimation(rotate)

        submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
        submitText.text = getString(R.string.pending_all_caps)
        submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}
