package az.turanbank.mlb.presentation.activity

import android.os.Bundle
import android.text.Editable
import androidx.appcompat.app.AppCompatActivity
import az.turanbank.mlb.R
import kotlinx.android.synthetic.main.activity_login_with_asan_individual_submit.asan_verification_code_edittext
import kotlinx.android.synthetic.main.activity_login_with_asan_individual_submit.accept_login_with_asan_submit
import kotlinx.android.synthetic.main.mlb_toolbar_layout.toolbar_back_button

class LoginWithAsanIndividualSubmitActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_with_asan_individual_submit)

        asan_verification_code_edittext.text = Editable.Factory.getInstance().newEditable(intent.getStringExtra("VerifyCode"))
        toolbar_back_button.setOnClickListener { onBackPressed() }
        accept_login_with_asan_submit.setOnClickListener {
        // Bura bir dialoq boks yazmaq lazımdır ki, taymer göstərsin
        }
    }
}
