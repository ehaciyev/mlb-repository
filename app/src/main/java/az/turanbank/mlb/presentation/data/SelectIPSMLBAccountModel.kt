package az.turanbank.mlb.presentation.data

import az.turanbank.mlb.data.remote.model.ips.response.IPSMLBAccountResponseModel

data class SelectIPSMLBAccountModel(
    val account: IPSMLBAccountResponseModel,
    var checked: Boolean,
    var isDefault: Boolean
)