package az.turanbank.mlb.presentation.ips.accounts.setdefault.account

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.AliasAccountCompoundModel
import kotlinx.android.synthetic.main.ips_single_select_list_view.view.*

class ChooseAccountForDefaultAdapter(
    private val alias: ArrayList<AliasAccountCompoundModel>,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<ChooseAccountForDefaultAdapter.AliasSingleSelectViewHolder>() {

    class AliasSingleSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(model: AliasAccountCompoundModel, onClickListener: (position: Int) -> Unit) {
            itemView.aliasName.text = model.iban
            itemView.aliasTypeAndAccount.text = "AZN"
            itemView.setOnClickListener {
                onClickListener.invoke(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        AliasSingleSelectViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.ips_single_select_list_view, parent, false
            )
        )

    override fun getItemCount() = alias.size

    override fun onBindViewHolder(holder: AliasSingleSelectViewHolder, position: Int) {
        holder.bind(alias[position], onClickListener)
    }
}