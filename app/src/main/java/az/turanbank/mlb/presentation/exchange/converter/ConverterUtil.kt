package az.turanbank.mlb.presentation.exchange.converter

import az.turanbank.mlb.data.remote.model.exchange.CurrencyResponseModel
import javax.inject.Inject

class ConverterUtil @Inject constructor(
) {
    fun convertedAmount(
        fromCurrency: CurrencyResponseModel,
        toCurrency: CurrencyResponseModel,
        amount: Double): Double{
        return (fromCurrency.cashSell*amount)/toCurrency.cashBuy
    }
}