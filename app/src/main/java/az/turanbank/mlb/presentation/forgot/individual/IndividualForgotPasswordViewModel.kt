package az.turanbank.mlb.presentation.forgot.individual

import android.content.SharedPreferences
import az.turanbank.mlb.domain.user.usecase.forgot.CheckForgotPassIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.register.mobile.SendOTPCodeUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface IndividualForgotPasswordViewModelInputs: BaseViewModelInputs {
    fun checkForgotPassword(pin: String, mobile: String)
}
interface IndividualForgotPasswordViewModelOutputs: BaseViewModelOutputs {
    fun verifyOtpCode(): Observable<Long>
}

class IndividualForgotPasswordViewModel @Inject constructor(
    private val checkForgotPassIndividualUseCase: CheckForgotPassIndividualUseCase,
    private val sendOTPCodeUseCase: SendOTPCodeUseCase,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel(),
    IndividualForgotPasswordViewModelInputs,
    IndividualForgotPasswordViewModelOutputs {

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val verifyOtpCode = PublishSubject.create <Long>()
    override val inputs: IndividualForgotPasswordViewModelInputs = this

    override val outputs: IndividualForgotPasswordViewModelOutputs = this

    override fun checkForgotPassword(pin: String, mobile: String) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        checkForgotPassIndividualUseCase.execute(pin, mobile, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    sendOTPCode(it.custId, mobile)
                }else{
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            })

            .addTo(subscriptions)
    }

    private fun sendOTPCode(custId: Long, mobile: String) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        sendOTPCodeUseCase.execute(custId, null, mobile,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when(it.status.statusCode) {
                    1 -> {
                        verifyOtpCode.onNext(custId)
                    }
                    else -> error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun verifyOtpCode(): Observable<Long> = verifyOtpCode
}