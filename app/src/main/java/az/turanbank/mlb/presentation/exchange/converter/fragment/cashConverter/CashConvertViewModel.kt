package az.turanbank.mlb.presentation.exchange.converter.fragment.cashConverter

import az.turanbank.mlb.data.remote.model.exchange.ConverterCurrencyListModel
import az.turanbank.mlb.data.remote.model.exchange.CurrencyResponseModel
import az.turanbank.mlb.data.remote.model.exchange.ExchangeCashResponseModel
import az.turanbank.mlb.domain.user.usecase.exchange.CashCurrenciesUseCase
import az.turanbank.mlb.domain.user.usecase.exchange.ConvertCashUseCase
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.exchange.converter.ConverterUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface CashConvertViewModelInputs : BaseViewModelInputs {
    fun getCashExchange()
    fun convertCurrency(
        fromCurrency: CurrencyResponseModel,
        toCurrency: CurrencyResponseModel,
        currencyAmount: Double
    )

    fun getExchangValues()
}

interface CashConvertViewModelOutputs : BaseViewModelOutputs {
    fun onCurrenciesGot(): PublishSubject<ConverterCurrencyListModel>
    fun onConvertGot(): PublishSubject<Double>
    fun onValuesFetched(): PublishSubject<ExchangeCashResponseModel>
}

class CashConvertViewModel @Inject constructor(
    private val cashCurrenciesUseCase: CashCurrenciesUseCase,
    private val convertCashUseCase: ConvertCashUseCase,
    private val exchangeCashUseCase: ExchangeCashUseCase,
    private val convertUtil: ConverterUtil
) : BaseViewModel(),
    CashConvertViewModelInputs,
    CashConvertViewModelOutputs {


    override val inputs: CashConvertViewModelInputs = this

    override val outputs: CashConvertViewModelOutputs = this


    private val currencyList = PublishSubject.create<ConverterCurrencyListModel>()
    private val currencyValues = PublishSubject.create<ExchangeCashResponseModel>()

    private val converted = PublishSubject.create<Double>()

    override fun convertCurrency(
        fromCurrency: CurrencyResponseModel,
        toCurrency: CurrencyResponseModel,
        currencyAmount: Double
    ) {
        converted.onNext(convertUtil.convertedAmount(fromCurrency, toCurrency, currencyAmount))
    }

    override fun getCashExchange() {
        cashCurrenciesUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    currencyList.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }
            ).addTo(subscriptions)
    }

    override fun getExchangValues() {
        exchangeCashUseCase.execute("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    currencyValues.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
                it.printStackTrace()
            }
            ).addTo(subscriptions)
    }

    override fun onCurrenciesGot() = currencyList
    override fun onConvertGot() = converted
    override fun onValuesFetched() = currencyValues
}