package az.turanbank.mlb.presentation.ips.accounts.myaccounts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.response.IPSMLBAccountResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_my_linked_accounts.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class MyLinkedAccountsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: MyLinkedAccountsViewModel

    lateinit var adapter: MyLinkedAccountsAdapter

    private var accounts: ArrayList<IPSMLBAccountResponseModel> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_linked_accounts)

        toolbar_back_button.setOnClickListener { onBackPressed() }
        //toolbar_title.setText("")

        viewModel = ViewModelProvider(this, factory)[MyLinkedAccountsViewModel::class.java]


        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        myLinkedAccountContainer.layoutManager = llm

        adapter = MyLinkedAccountsAdapter(applicationContext,accounts){

        }

        myLinkedAccountContainer.adapter = adapter

        setOutputListeners()
        setInputListeners()

    }

    private fun setInputListeners() {
        viewModel.inputs.getAccounts()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onAccountsSuccess().subscribe{
            accounts.clear()
            accounts.addAll(it)
            adapter.notifyDataSetChanged()

        }.addTo(subscriptions)

        viewModel.onError().subscribe{
            noDataAvailable.visibility = View.VISIBLE
            myLinkedAccountContainer.visibility = View.GONE

        }.addTo(subscriptions)
    }
}