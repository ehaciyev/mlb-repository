package az.turanbank.mlb.presentation.home

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.CampaignModel
import az.turanbank.mlb.domain.user.usecase.login.GetCampaignListUseCase
import az.turanbank.mlb.domain.user.usecase.main.GetCampaignByIdUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

interface CampaignViewModelInputs: BaseViewModelInputs{
    fun getTheCampaign(campaignId: Long)
    fun getCampaigns()
}
interface CampaignViewModelOutputs: BaseViewModelOutputs{
    fun onCampaigSuccess(): PublishSubject<CampaignModel>
    fun campaignsSuccess(): PublishSubject<ArrayList<CampaignModel>>
    fun showProgress(): PublishSubject<Boolean>
}
class CampaignViewModel @Inject constructor(
    private val getCampaignListUseCase: GetCampaignListUseCase,
    private val getCampaignByIdUseCase: GetCampaignByIdUseCase,
    sharedPreferences: SharedPreferences
) : BaseViewModel(),
        CampaignViewModelInputs,
        CampaignViewModelOutputs{
    override val inputs: CampaignViewModelInputs = this
    override val outputs: CampaignViewModelOutputs = this
    private val campaigns = PublishSubject.create<ArrayList<CampaignModel>>()
    private val campaignImage = PublishSubject.create<CampaignModel>()
    private val showProgress = PublishSubject.create<Boolean>()

    val lang = sharedPreferences.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun getCampaigns() {
        showProgress.onNext(true)

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        getCampaignListUseCase.execute(enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1){
                    campaigns.onNext(it.respCampaignList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun campaignsSuccess() = campaigns

    override fun showProgress() = showProgress

    override fun getTheCampaign(campaignId: Long) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        showProgress.onNext(true)

        getCampaignByIdUseCase
            .execute(enumLangType, campaignId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)

                if (it.status.statusCode == 1){
                    campaignImage.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)

                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun onCampaigSuccess() = campaignImage
}