package az.turanbank.mlb.presentation.ips.accounts.authorization

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.ips.response.RespAuthMethod
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_i_p_s_authorization.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class IPSAuthorizationActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: IPSAuthorizationViewModel

    lateinit var adapter: AuthorizationAccountSelectAdapter
    private val authMethods = arrayListOf<RespAuthMethod>()

    private val createdAccoutId by lazy { intent.getSerializableExtra("createdAccoutId") as ArrayList<Long>  }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_i_p_s_authorization)

        viewModel = ViewModelProvider(this,factory)[IPSAuthorizationViewModel::class.java]

        toolbar_title.text = getString(R.string.authorization_method)
        toolbar_back_button.setOnClickListener{onBackPressed()}

        recyclerViewShimmerContainer.visibility = View.VISIBLE
        recyclerViewShimmerContainer.startShimmerAnimation()


        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm

        adapter = AuthorizationAccountSelectAdapter(authMethods, this){
            authMethods[it].checked = !authMethods[it].checked
            adapter.notifyDataSetChanged()
        }

        recyclerView.adapter = adapter

        setOutputListener()
        setInputListener()

    }

    private fun setOutputListener() {

        viewModel.outputs.accountsRegistered().subscribe {
            //progress.clearAnimation()
            //progress.visibility = View.GONE
            Toast.makeText(this, R.string.accounts_successfully_registered, Toast.LENGTH_LONG).show()
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(subscriptions)

        viewModel.outputs.onGetIpsAuthMethods().subscribe {
            recyclerViewShimmerContainer.visibility = View.GONE
            recyclerViewShimmerContainer.stopShimmerAnimation()
            //progress.clearAnimation()
            //progress.visibility = View.GONE
            viewGroupContainer.visibility = View.VISIBLE
            authMethods.clear()
            authMethods.addAll(it)
            recyclerView.adapter?.notifyDataSetChanged()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            recyclerViewShimmerContainer.visibility = View.GONE
            recyclerViewShimmerContainer.stopShimmerAnimation()
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)

        viewModel.outputs.onAuthEmpty().subscribe{
            if(it){
                message.visibility = View.GONE
                recyclerView.visibility = View.GONE
                submitButton.visibility = View.GONE
                noDataAvailable.visibility = View.VISIBLE
            }
        }.addTo(subscriptions)
    }

    private fun setInputListener() {
        viewModel.inputs.getIpsAuthMethods()

        val accountsToRegisterIpsSystem = arrayListOf<Long>()
        submitButton.setOnClickListener {
            if(adapter.getSelectedPosition() != null){
                /*val intent = Intent(this, SelectAccountLinkingToAliasActivity::class.java)
                intent.putExtra("aliasId", adapter.getSelectedItem()!!.name)
                intent.putExtra("aliasValue",adapter.getSelectedItem()!!.type)
                startActivityForResult(intent, 12)*/
                viewModel.inputs.registerAccounts(createdAccoutId)
            } else {
                AlertDialogMapper(this,8000).showAlertDialog()
            }

        }
    }

    private fun animateProgressImage() {

        progressImage.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progressImage.startAnimation(rotate)
    }
}
