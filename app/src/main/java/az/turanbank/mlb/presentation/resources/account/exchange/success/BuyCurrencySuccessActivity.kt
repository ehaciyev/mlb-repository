package az.turanbank.mlb.presentation.resources.account.exchange.success

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.app.AlertDialog
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.data.remote.model.resources.account.exchange.CreateExchangeOperationResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.resources.account.AccountTransfersActivity
import az.turanbank.mlb.presentation.resources.account.exchange.ConvertMoneyActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_buy_currency_success.*
import kotlinx.android.synthetic.main.add_to_templates_dialog_view.view.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import javax.inject.Inject

class BuyCurrencySuccessActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: BuyCurrencySuccessViewModel
    private lateinit var exchange: CreateExchangeOperationResponseModel

    private val decimalFormat = DecimalFormat("0.00")
    private val decimalFormatWithoutRound = DecimalFormat("0.0000")
    private val symbols = DecimalFormatSymbols()

    lateinit var paidStamp: ImageView

    private val exchangeRate: Double by lazy { intent.getDoubleExtra("exchangeRate", 0.00) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_currency_success)

        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = '.'
        decimalFormat.decimalFormatSymbols = symbols
        decimalFormatWithoutRound.decimalFormatSymbols = symbols

        exchange = intent.getSerializableExtra("exchangeResponse") as CreateExchangeOperationResponseModel

        viewModel =
            ViewModelProvider(this, factory)[BuyCurrencySuccessViewModel::class.java]

        initViews()
        setDataToView()
        setOutputListeners()
        fillTheCheckoutWithObject()
        setInputListeners()
    }

    private fun setDataToView() {
        when {
            viewModel.getLang() == "az" -> {
                paidStamp.setImageResource(R.drawable.pechat_az)
            }
            viewModel.getLang() == "en" -> {
                paidStamp.setImageResource(R.drawable.pechat_eng)
            }
            viewModel.getLang() == "ru" -> {
                paidStamp.setImageResource(R.drawable.pechat_rus)
            }
        }
    }

    private fun initViews() {
        paidStamp = findViewById(R.id.paid_stamp)
    }

    @SuppressLint("SetTextI18n")
    private fun fillTheCheckoutWithObject() {
        opStatus.text = exchange.operationStatus
        operationType.text = exchange.operationName
        opResultOpNo.text = exchange.operationNo
        dtAccount.text = exchange.dtIban
        crAccount.text = exchange.crIban
        crAmountInteger.text = decimalFormat.format(exchange.crAmount).toString().split(".")[0]+","
        crAmountReminder.text = decimalFormat.format(exchange.crAmount).toString().split(".")[1]

        exchangeInteger.text = decimalFormatWithoutRound.format(exchangeRate).toString().split(".")[0]+","
        exchangeReminder.text = decimalFormatWithoutRound.format(exchangeRate).toString().split(".")[1]

        currency.text = exchange.crCurrency

    }

    private fun setInputListeners() {
        opResultClose.setOnClickListener {
            finish()
        }

        saveTemp.setOnClickListener {
            showSaveTempEditText()
        }

        opResultShare.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, generateShareString())

            startActivity(Intent.createChooser(shareIntent, getString(R.string.send)))
        }
    }

    @SuppressLint("InflateParams")
    private fun showSaveTempEditText(){

        val alert = AlertDialog.Builder(this)
        alert.setTitle(getString(R.string.template_name))

        val view = LayoutInflater.from(this).inflate(R.layout.add_to_templates_dialog_view, null, false)
        alert.setView(view)

        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.save_all_caps)
        ) { _, _ ->
            if (!TextUtils.isEmpty(view.tempNameEditText.text.toString())) {
                animateProgressImage()
                viewModel.inputs.saveTemplate(
                    view.tempNameEditText.text.toString(),
                    exchange.operationId)
            }
        }

        alert.setNegativeButton(getString(R.string.cancel_all_caps)
        ) { dialog, _ ->
            dialog.dismiss()
        }

        alert.show()
    }

    private fun generateShareString(): String {

        return getString(R.string.share_status) + opStatus.text.toString() + "\n" +
                getString(R.string.card_to_card_share_oper_type) + exchange.operationName + "\n" +
                getString(R.string.card_to_card_share_oper_number) + exchange.operationNo + "\n" +
                getString(R.string.share_creditor_account) + exchange.dtIban + "\n" +
                getString(R.string.share_debitor_account) + exchange.crIban + "\n" +
                getString(R.string.share_exchange_rate) + exchangeRate + "\n" +
                getString(R.string.share_amount) + exchange.crAmount + "\n" +
                getString(R.string.currency) + exchange.crCurrency + "\n"
    }
    private fun setOutputListeners() {
        viewModel.outputs.saveTemplateSuccess().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            ConvertMoneyActivity().finish()
            val intent = Intent(this, AccountTransfersActivity::class.java)
            intent.putExtra("fromTemp", true)
            startActivity(intent)
            finish()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe {
            progress.clearAnimation()
            progress.visibility = View.GONE
            cardView.visibility = View.VISIBLE
            Toast.makeText(this, getString(ErrorMessageMapper().getErrorMessage(it)), Toast.LENGTH_LONG).show()
        }.addTo(subscriptions)
    }

    private fun animateProgressImage() {
        progress.visibility = View.VISIBLE
        cardView.visibility = View.GONE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE

        progress.startAnimation(rotate)
    }
}
