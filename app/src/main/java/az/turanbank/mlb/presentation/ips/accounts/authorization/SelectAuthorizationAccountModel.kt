package az.turanbank.mlb.presentation.ips.accounts.authorization

data class SelectAuthorizationAccountModel(
    var value: String,
    var checked: Boolean
)