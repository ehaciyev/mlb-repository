package az.turanbank.mlb.presentation.resources.card.statement

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_card_statement_tabbed.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class CardStatementTabbedActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardStatementTabbedViewModel

   /* private val TAB_TITLES = arrayOf(
        R.string.full,
        R.string.import_text,
        R.string.export_text
    )*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_statement_tabbed)


        viewModel = ViewModelProvider(this,factory)[CardStatementTabbedViewModel::class.java]

        val TAB_TITLES = arrayOf(
            getString(R.string.full),
            getString(R.string.import_text),
            getString(R.string.export_text)
        )

        val sectionsPagerAdapter =
            CardStatementPagerAdapter(applicationContext,TAB_TITLES,
                supportFragmentManager
            )
        view_pager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(view_pager)
        view_pager.offscreenPageLimit = 3

        toolbar_title.text = getString(R.string.card_statement)

        toolbar_back_button.setOnClickListener { onBackPressed() }
    }
}