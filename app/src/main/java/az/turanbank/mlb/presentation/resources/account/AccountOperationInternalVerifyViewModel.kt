package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.DomesticOperationResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.CreateInternalOperationIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.CreateInternalOperationJuridicallUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountOperationInternalVerifyViewModelInputs : BaseViewModelInputs {
    fun createInternalOperation(
        dtAccountId: Long,
        crIban: String,
        amount: Double,
        purpose: String,
        taxNo: String?
    )
}

interface AccountOperationInternalVerifyViewModelOutputs : BaseViewModelOutputs {
    fun createInternalOperationSuccess(): PublishSubject<DomesticOperationResponseModel>
    fun showTax(): PublishSubject<Boolean>
}

class AccountOperationInternalVerifyViewModel @Inject constructor(
    private val createInternalOperationIndividualUseCase: CreateInternalOperationIndividualUseCase,
    private val createInternalOperationJuridicallUseCase: CreateInternalOperationJuridicallUseCase,
    private val sharedPrefs: SharedPreferences
) :
    BaseViewModel(),
    AccountOperationInternalVerifyViewModelInputs,
    AccountOperationInternalVerifyViewModelOutputs {

    override val inputs: AccountOperationInternalVerifyViewModelInputs = this
    override val outputs: AccountOperationInternalVerifyViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    private val showTax = PublishSubject.create<Boolean>()

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val createInternalOperationSuccess: PublishSubject<DomesticOperationResponseModel> = PublishSubject.create()
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    override fun createInternalOperation(
        dtAccountId: Long,
        crIban: String,
        amount: Double,
        purpose: String,
        taxNo: String?
    ) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if (isCustomerJuridical) {
            createInternalOperationJuridicallUseCase.execute(
                custId = custId,
                crIban = crIban,
                amount = amount,
                purpose = purpose,
                token = token,
                dtAccountId = dtAccountId,
                compId = compId,
                taxNo = taxNo,
                lang = enumLangType
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        createInternalOperationSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            createInternalOperationIndividualUseCase.execute(
                custId = custId,
                crIban = crIban,
                amount = amount,
                purpose = purpose,
                token = token,
                dtAccountId = dtAccountId,
                taxNo = taxNo,
                lang = enumLangType
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        createInternalOperationSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun showTax() = showTax

    override fun createInternalOperationSuccess() = createInternalOperationSuccess
}