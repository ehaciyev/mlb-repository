package az.turanbank.mlb.presentation.orders

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.*
import az.turanbank.mlb.domain.user.usecase.CreateOrderUseCase
import az.turanbank.mlb.domain.user.usecase.GetOrderPeriodUseCase
import az.turanbank.mlb.domain.user.usecase.exchange.ExchangeCashlessUseCase
import az.turanbank.mlb.domain.user.usecase.orders.GetBranchListUseCase
import az.turanbank.mlb.domain.user.usecase.orders.GetOrderKindListUseCase
import az.turanbank.mlb.domain.user.usecase.orders.GetOrderKindTypeListUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface CreditOrderViewModelInputs : BaseViewModelInputs {
    fun getBranchList()
    fun getOrderKindList(orderType: Int?, kindType: Long?)
    fun getCurrencies()
    fun createOrder(
        branchId: Long,
        orderType: Int?,
        kindId: Long,
        period: Int,
        amount: Double,
        ccy: String
    )
    fun getOrderKindTypeList(orderType: Int?)
    fun getOrderPeriodList(orderType: Int?, kindType: Long?)
}

interface CreditOrderViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun branchList(): PublishSubject<ArrayList<Branch>>
    fun showOrderKindListProgress(): PublishSubject<Boolean>
    fun orderKindList(): PublishSubject<ArrayList<OrderKind>>
    fun currencyListSuccess(): PublishSubject<java.util.ArrayList<String>>
    fun getCurrenciesProgress(): PublishSubject<Boolean>
    fun createOrderSuccess(): PublishSubject<Boolean>
    fun createOrderError(): CompletableSubject
    fun orderKindTypeList(): PublishSubject<ArrayList<OrderKind>>
    fun orderKindTypeListProgress(): PublishSubject<Boolean>
    fun orderPeriodProgress(): PublishSubject<Boolean>
    fun orderPeriodList(): PublishSubject<ArrayList<OrderPeriod>>
}

class CreditOrderViewModel @Inject constructor(
    private val getBranchListUseCase: GetBranchListUseCase,
    private val orderKindListUseCase: GetOrderKindListUseCase,
    private val getCashlessUseCase: ExchangeCashlessUseCase,
    private val getOrderKindTypeListUseCase: GetOrderKindTypeListUseCase,
    private val createOrderUseCase: CreateOrderUseCase,
    private val getOrderPeriodUseCase: GetOrderPeriodUseCase,
    private val sharedPrefs: SharedPreferences
) : BaseViewModel(), CreditOrderViewModelInputs, CreditOrderViewModelOutputs {

    override fun orderPeriodList() = orderPeriodList

    override fun orderPeriodProgress() = orderPeriodProgress


    override fun getOrderPeriodList(orderType: Int?, kindType: Long?) {
        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }

        orderPeriodProgress.onNext(true)
        getOrderPeriodUseCase.execute(custId, token, orderType, kindType, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                orderPeriodProgress.onNext(false)
                if(it.status.statusCode == 1) {
                    orderPeriodList.onNext(it.respOrderPeriodList) //TODO check response and correct if needed
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun orderKindTypeListProgress() = showOrderKindTypeListProgress

    override fun getOrderKindTypeList(orderType: Int?){
        showOrderKindTypeListProgress.onNext(true)

        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }

        getOrderKindTypeListUseCase.execute(custId, token, orderType, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status.statusCode == 1) {
                    showOrderKindTypeListProgress.onNext(false)
                    orderKindTypeList.onNext(it.respOrderTypeList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
            }).addTo(subscriptions)
    }

    override fun orderKindTypeList() = orderKindTypeList

    override fun createOrder(
        branchId: Long,
        orderType: Int?,
        kindId: Long,
        period: Int,
        amount: Double,
        ccy: String
    ) {
        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            createOrderUseCase.execute(
                custId,
                compId,
                token,
                branchId,
                orderType,
                kindId,
                period,
                amount,
                ccy,
                enumLangType
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        orderCreated.onNext(true)
                    } else {
                        createOrderError.onComplete()
                        error.onNext(it.status.statusCode)
                        orderCreated.onNext(false)
                    }
                }, {
                    it.printStackTrace()
                }).addTo(subscriptions)
        } else {
            createOrderUseCase.execute(
                custId,
                null,
                token,
                branchId,
                orderType,
                kindId,
                period,
                amount,
                ccy,
                enumLangType
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        orderCreated.onNext(true)
                    } else {
                        error.onNext(it.status.statusCode)
                        orderCreated.onNext(false)
                    }
                }, {
                    it.printStackTrace()
                }).addTo(subscriptions)
        }
    }

    override fun createOrderSuccess() = orderCreated

    override fun createOrderError() = createOrderError

    override fun getCurrenciesProgress() = getCurrenciesProgress

    override fun getCurrencies() {
        val list = arrayListOf("AZN")

        getCurrenciesProgress.onNext(true)

        getCashlessUseCase.execute("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                getCurrenciesProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    for (i in 0 until it.exchangeCashlessList.size) {
                        list.add(it.exchangeCashlessList[i].currency)
                    }
                }
            }, {
                it.printStackTrace()
            }, {
                currencyList.onNext(list)
            }).addTo(subscriptions)
    }

    override fun currencyListSuccess() = currencyList

    override fun orderKindList() = orderKindList

    override fun showOrderKindListProgress() = showOrderKindListProgress

    override fun getOrderKindList(orderType: Int?, kindType: Long?) {
        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }

        showOrderKindListProgress.onNext(true)
        orderKindListUseCase.execute(custId, token, orderType, kindType, enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showOrderKindListProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    orderKindList.onNext(it.respOrderTypeList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            })
            .addTo(subscriptions)
    }

    override fun showProgress() = showProgress
    private val currencyList = PublishSubject.create<java.util.ArrayList<String>>()

    override val inputs: CreditOrderViewModelInputs = this
    override val outputs: CreditOrderViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val lang = sharedPrefs.getString("lang", "az")
    var enumLangType = EnumLangType.AZ
    private val showOrderKindListProgress = PublishSubject.create<Boolean>()
    private val showOrderKindTypeListProgress = PublishSubject.create<Boolean>()
    private val getCurrenciesProgress = PublishSubject.create<Boolean>()
    private val orderPeriodProgress = PublishSubject.create<Boolean>()
    private val showProgress = PublishSubject.create<Boolean>()
    private val branchList = PublishSubject.create<ArrayList<Branch>>()
    private val orderCreated = PublishSubject.create<Boolean>()
    private val createOrderError = CompletableSubject.create()
    private val orderKindList = PublishSubject.create<ArrayList<OrderKind>>()
    private val orderKindTypeList = PublishSubject.create<ArrayList<OrderKind>>()
    private val orderPeriodList = PublishSubject.create<ArrayList<OrderPeriod>>()

    override fun getBranchList() {
        showProgress.onNext(true)
        when (lang) {
            "az" -> {
                enumLangType = EnumLangType.AZ
            }
            "en" -> {
                enumLangType = EnumLangType.EN
            }
            "ru" -> {
                enumLangType = EnumLangType.RU
            }
        }
        getBranchListUseCase.execute(custId, token,enumLangType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress.onNext(false)
                if (it.status.statusCode == 1) {
                    branchList.onNext(it.respBranchList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                it.printStackTrace()
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun branchList() = branchList
}