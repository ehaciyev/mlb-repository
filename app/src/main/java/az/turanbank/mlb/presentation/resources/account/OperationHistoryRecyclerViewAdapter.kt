package az.turanbank.mlb.presentation.resources.account

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.OperationModel
import kotlinx.android.synthetic.main.operation_history_list_item.view.*

class OperationHistoryRecyclerViewAdapter(
    private val operationModelList: ArrayList<OperationModel>,
    private val clickListener: (position: Int) -> Unit
) :
    RecyclerView.Adapter<OperationHistoryRecyclerViewAdapter.OperationHistoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        OperationHistoryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.operation_history_list_item,
                parent,
                false
            )
        )

    override fun getItemCount() = operationModelList.size

    fun getItemAt(position: Int) = operationModelList[position]

    fun setItems(list: ArrayList<OperationModel>) {
        operationModelList.clear()
        operationModelList.addAll(list)
        this.notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: OperationHistoryViewHolder, position: Int) =
        holder.bind(operationModelList[position], clickListener)

    class OperationHistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(operationModel: OperationModel, clickListener: (position: Int) -> Unit) {
            val currency: String = if(operationModel.operTypeId ==5 || operationModel.operTypeId ==6){
                operationModel.crCcy
            } else {
                operationModel.currency
            }
            val amount: String = if(operationModel.operTypeId ==5 || operationModel.operTypeId ==6){
                operationModel.crAmt.toString()
            } else {
                operationModel.amount.toString()
            }
            val fullAmount = "$amount $currency"
            itemView.opHistoryAmount.text = fullAmount
            itemView.opHistoryName.text = operationModel.operName
            itemView.opHistoryPurpose.text = operationModel.operPurpose
            itemView.opHistoryState.text = operationModel.operState

            when(operationModel.operStateId) {
                1,2 -> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_1_or_2))
                }
                6,7 -> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_6_or_7))
                }
                3,4 -> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_3_or_4))
                }
                5 -> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_5))
                }
                8-> {
                    itemView.operStatusIdIndicator.setColorFilter(
                        ContextCompat.getColor(itemView.operStatusIdIndicator.context, R.color.oper_status_8))
                }
            }

            itemView.setOnClickListener {
                clickListener.invoke(adapterPosition)
            }
        }
    }
}
