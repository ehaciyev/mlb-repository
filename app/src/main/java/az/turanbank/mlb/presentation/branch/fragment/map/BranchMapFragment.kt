package az.turanbank.mlb.presentation.branch.fragment.map


import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.Constants.Companion.LOCATION_PERMISSION_REQUEST_CODE
import az.turanbank.mlb.data.remote.model.branch.BranchMapResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.branch.fragment.BranchViewModel
import az.turanbank.mlb.util.bitmapDescriptorFromVector
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.fragment_branch_map.*
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import javax.inject.Inject


class BranchMapFragment : BaseFragment(), OnMapReadyCallback {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: BranchViewModel

    private lateinit var mapView: MapView

    private lateinit var map: GoogleMap

    lateinit var nearButton: ImageView

    lateinit var nearLocationContainer: LinearLayout

    private val latLng = BehaviorSubject.create<LatLng>()

    private val branchArrayList = ArrayList<BranchMapResponseModel>()

    private val locations = ArrayList<LatLng>()

    lateinit var currentLatLng: LatLng

    private val PERMISSION_REQUEST_CODE = 200

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    lateinit var mapFragment: SupportMapFragment


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_branch_map, container, false)

        viewModel =
            ViewModelProvider(this, factory)[BranchViewModel::class.java]

        val coordinate: LatLng = if(arguments?.getDouble("lat")!=null && arguments?.getDouble("long")!=null){
            LatLng(arguments?.getDouble("lat")!!, arguments?.getDouble("long")!!)
        } else {
            LatLng(40.3723215, 49.822034)
        }

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        latLng.onNext(coordinate)

        nearButton = view.findViewById(R.id.nearButton)

        /*val scaleDown: ObjectAnimator = ObjectAnimator.ofPropertyValuesHolder(
            nearButton,
            PropertyValuesHolder.ofFloat("scaleX", 1.2f),
            PropertyValuesHolder.ofFloat("scaleY", 1.2f)
        )
        scaleDown.duration = 310
        scaleDown.repeatCount = ObjectAnimator.INFINITE
        scaleDown.repeatMode = ObjectAnimator.REVERSE
        scaleDown.start()*/

        nearButton.setOnClickListener{

                if (checkSelfPermission(
                        requireActivity(),
                        ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(
                        arrayOf(ACCESS_FINE_LOCATION),
                        LOCATION_PERMISSION_REQUEST_CODE
                    )
                } else{
                    var distancesArray = arrayListOf<Float>()

                    val results = FloatArray(3)
                    var location = Location("currentLocation")
                    var locationB = Location("B")

                    locations.forEach {
                        location.latitude = currentLatLng.latitude
                        location.longitude = currentLatLng.longitude
                        locationB.latitude = it.latitude
                        locationB.longitude = it.longitude

                        distancesArray.add(location.distanceTo(locationB))

                       /* Location.distanceBetween(currentLatLng.latitude, currentLatLng.longitude,
                            it.latitude,it.longitude, results)
                        when (results.size) {
                            2 -> {
                                distancesArray.add(results[1])
                            }
                            1 -> {
                                distancesArray.add(results[0])
                            }
                            0 -> {
                            }
                            else -> {
                                distancesArray.add(results[2])
                            }
                        }*/
                        //distancesArray.add(results[0])
                    }

                    if(distancesArray.size>0){
                        var smallestDistance = distancesArray[0]
                        var position = 0
                        for(i in 0 until distancesArray.size){
                            if(distancesArray[i]< smallestDistance){
                                smallestDistance = distancesArray[i]
                                position = i
                            }
                        }

                        distanceValue.text = BigDecimal(smallestDistance.toString()).setScale( 0 , RoundingMode.CEILING ).toString() + " meter"
                        distanceValue.setTextColor(resources.getColor(R.color.colorPrimary))

                        val cameraPosition = CameraPosition.Builder()
                            .target(locations[position])
                            .zoom(18f)
                            .build()
                        val cu = CameraUpdateFactory.newCameraPosition(cameraPosition)
                        mMap.animateCamera(cu)
                    }
                }
        }

        nearLocationContainer = view.findViewById(R.id.nearLocationContainer)

        setOutputListeners()
        setInputListeners()

        return view
    }

    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        requireActivity().supportFragmentManager.beginTransaction().detach(this).attach(this).commit()
    }
    private fun setOutputListeners() {
        viewModel.outputs.onBranchesSuccess().subscribe {
            branchArrayList.addAll(it)
            populateMarker()
        }.addTo(subscriptions)
    }

    private fun populateMarker() {
        branchArrayList.forEach {
            mMap.addMarker(
                MarkerOptions()
                    .icon(bitmapDescriptorFromVector(requireActivity(), R.drawable.ic_marker))
                    .position(LatLng(it.latitude, it.longitude))
                    .title(it.branchName)
                    .snippet(it.address)
            )
            locations.add(LatLng(it.latitude, it.longitude))
        }
    }

    fun showBranchLocation(latitude: Double, longitude: Double) {

        val location = CameraUpdateFactory.newLatLngZoom(
            LatLng(latitude, longitude), 18f
        )
        mMap.moveCamera(location)
        mMap.animateCamera(location)
    }

    private fun setInputListeners() {
        viewModel.inputs.getBranches()
    }
    override fun onMapReady(p0: GoogleMap) {
        mMap = p0

        // zoom buttons control
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isCompassEnabled = false
        mMap.uiSettings.isMyLocationButtonEnabled = false
        mMap.uiSettings.isMapToolbarEnabled = false

        setUp()
    }

    private fun setUp(){
        if (checkSelfPermission(
                requireActivity(),
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
        } else{
            if (mMap != null) {
                enableLocation()
            }
        }
    }

    private fun enableLocation(){
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(requireActivity()) { location ->
            if (location != null) {
                lastLocation = location
                currentLatLng = LatLng(location.latitude, location.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return
        } else if (requestCode == LOCATION_PERMISSION_REQUEST_CODE){
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                setUp()
            }
        } else {
            if (!grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                Toast.makeText(requireActivity(), "Permission denied to access location", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
