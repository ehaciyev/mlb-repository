package az.turanbank.mlb.presentation.resources.account

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class OperationDetailsActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: OperationDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation_details)

        viewmodel = ViewModelProvider(this, factory) [OperationDetailsViewModel::class.java]

        setInputListeners()
        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewmodel.outputs.showProgress().subscribe {

        }.addTo(subscriptions)
    }

    private fun setInputListeners() {

    }
}
