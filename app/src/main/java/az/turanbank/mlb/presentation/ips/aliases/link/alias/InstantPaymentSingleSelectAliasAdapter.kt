package az.turanbank.mlb.presentation.ips.aliases.link.alias

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton

import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel
import kotlinx.android.synthetic.main.ips_single_delete_list_view.view.*


class InstantPaymentSingleSelectAliasAdapter(
    private var context: Context,
    private var alias: ArrayList<MLBAliasResponseModel>,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<InstantPaymentSingleSelectAliasAdapter.AliasSingleSelectViewHolder>() {

    private var selected: RadioButton? = null
    private var selectedItem :MLBAliasResponseModel? = null
    private var selectedPosition: Int? = null

    class AliasSingleSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        /*fun bind(context: Context,alias: MLBAliasResponseModel, onClickListener: (position: Int) -> Unit) {
            if(alias.isSelected){
                itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.checked_account))
                itemView.selectedAlias.isChecked = true
            } else {
                itemView.container.setBackgroundColor(ContextCompat.getColor(context, R.color.unchecked_account))
                itemView.selectedAlias.isChecked = false
            }
            itemView.container.setOnClickListener { onClickListener.invoke(adapterPosition) }
            itemView.selectedAlias.setOnClickListener { onClickListener.invoke(adapterPosition) }
            itemView.aliasName.text = alias.ipsValue
            itemView.aliasTypeAndAccount.text = alias.ipsType
        }*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AliasSingleSelectViewHolder =
        AliasSingleSelectViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.ips_single_delete_list_view, parent, false
            )
        )

    override fun getItemCount() = alias.size

    fun updateRecords(mlbAliasResponseModel: ArrayList<MLBAliasResponseModel>){
        this.alias = mlbAliasResponseModel
        notifyDataSetChanged()
    }

    /*fun getSelectedItem() = alias[selectedItemIndex]*/

    fun setData(list: ArrayList<MLBAliasResponseModel>) {
        alias.clear()
        alias.addAll(list)
        notifyDataSetChanged()
    }

    /*class AliasSingleSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)*/

    override fun onBindViewHolder(holder: AliasSingleSelectViewHolder, position: Int) {
        val item = alias[position]

        //holder.itemView.selectedAlias.isChecked = aliasSingle.isSelected
        holder.itemView.aliasName.text = item.ipsValue
        holder.itemView.aliasTypeAndAccount.text = item.ipsType

        holder.itemView.selectedAlias.setOnClickListener {
            selectedPosition = position
            if (selected != null) {
                selectedItem!!.isSelected = false
                selected!!.isChecked = false
            }
            holder.itemView.selectedAlias.isChecked = true
            item.isSelected = true
            selectedItem = item
            selected = holder.itemView.selectedAlias

        }
    }

    fun getSelectedItem() = selectedItem

    fun getSelectedPosition() = selectedPosition

}