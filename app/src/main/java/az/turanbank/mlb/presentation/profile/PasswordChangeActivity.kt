package az.turanbank.mlb.presentation.profile

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_password_change.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class PasswordChangeActivity : BaseActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewmodel: PasswordChangeViewModel

    lateinit var dialog: MlbProgressDialog

    lateinit var textWatcher: TextWatcher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_change)

        toolbar_title.text = getString(R.string.change_password)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        viewmodel = ViewModelProvider(this, factory) [PasswordChangeViewModel::class.java]

        dialog = MlbProgressDialog(this)
        buttonEnabled(false)

        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if( !currentPassword.text.toString().trim().isNullOrEmpty() &&
                    !newPassword.text.toString().trim().isNullOrEmpty() &&
                    !repeatPassword.text.toString().trim().isNullOrEmpty()) {

                    buttonEnabled(true)

                    submitButton.setOnClickListener {
                        viewmodel.inputs.changePassword(
                            currentPassword.text.toString().trim(),
                            newPassword.text.toString().trim(),
                            repeatPassword.text.toString().trim()
                        )
                    }

                } else {
                    buttonEnabled(false)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        currentPassword.addTextChangedListener(textWatcher)
        newPassword.addTextChangedListener(textWatcher)
        repeatPassword.addTextChangedListener(textWatcher)

        setOutputListeners()
    }

    private fun buttonEnabled(enabled: Boolean) {
        if (!enabled) {
            submitButton.isClickable = false
            submitButton.isEnabled = false
            submitButton.isFocusable = false
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
        } else {
            submitButton.isClickable = true
            submitButton.isEnabled = true
            submitButton.isFocusable = true
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
    }

    private fun setOutputListeners() {
        viewmodel.outputs.showProgress().subscribe {
            if(it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewmodel.outputs.changePasswordFinished().subscribe {
            if(it) {
                currentPassword.setText("")
                newPassword.setText("")
                repeatPassword.setText("")
                Toast.makeText(this, getString(R.string.password_successfully_updated), Toast.LENGTH_LONG).show()
            }
        }.addTo(subscriptions)

        viewmodel.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)

    }
}
