package az.turanbank.mlb.presentation.resources.card.statement

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.formatDecimal
import kotlinx.android.synthetic.main.activity_card_statement_internal.*
import javax.inject.Inject

class CardStatementInternalActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: CardStatementInternalViewModel

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    private val date: String by lazy { intent.getStringExtra("date") }
    private val cardNumber: String by lazy { intent.getStringExtra("cardNumber") }
    private val debetAmount: Double by lazy { intent.getDoubleExtra("debetAmount", 0.00) }
    private val creditAmount: Double by lazy { intent.getDoubleExtra("creditAmount", 0.00) }
    private val purpose: String by lazy { intent.getStringExtra("purpose") }
    private val currency: String by lazy { intent.getStringExtra("currency") }
    private val fee: Double by lazy { intent.getDoubleExtra("fee", 0.00) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_statement_internal)

        viewModel = ViewModelProvider(this, factory)[CardStatementInternalViewModel::class.java]

        when (sharedPreferences.getString("lang", "az")) {
            "az" -> {
                paidStamp.setImageResource(R.drawable.pechat_az)
            }
            "en" -> {
                paidStamp.setImageResource(R.drawable.pechat_eng)
            }
            "ru" -> {
                paidStamp.setImageResource(R.drawable.pechat_rus)
            }
        }
        bindDateToView()

        setInputListeners()
    }

    private fun setInputListeners() {
        close.setOnClickListener {
            finish()
        }
        share.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, generateShareString())

            startActivity(Intent.createChooser(shareIntent, getString(R.string.send)))
        }
    }

    @SuppressLint("SetTextI18n")
    private fun bindDateToView() {

        if(creditAmount > 0) {

        } else {

        }
        operationDate.text = date
        requestorCardNumber.text = cardNumber

        debetAmountInteger.text = debetAmount.toString().split(".")[0] + ","
        debetAmountReminder.text =
            debetAmount.toString().split(".")[1] + " " + currency

        creditInteger.text = creditAmount.toString().split(".")[0] + ","
        creditReminder.text =
            creditAmount.toString().split(".")[1] + " " + currency

        opPurpose.text = purpose

        feeInteger.text = fee.toString().split(".")[0] + ","
        feeReminder.text = fee.toString().split(".")[1] + " " + currency
    }

    private fun generateShareString(): String {

        return getString(R.string.share_oper_date) + date + "\n" +
                getString(R.string.share_oper_debit_card) + cardNumber + "\n" +
                getString(R.string.share_credit_amount) + creditAmount.toString() + "\n" +
                getString(R.string.share_debit_amount) + debetAmount.toString() + "\n" +
                getString(R.string.comission) + fee.toString() + "\n" +
                getString(R.string.card_to_card_share_curr) + currency + "\n"
    }
}
