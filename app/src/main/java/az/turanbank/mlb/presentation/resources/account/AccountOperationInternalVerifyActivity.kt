package az.turanbank.mlb.presentation.resources.account

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.lifecycle.ViewModelProvider
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.local.CurrencySignMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.secondPartOfMoney
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_account_operation_internal_verify.*
import kotlinx.android.synthetic.main.activity_card_requisities.toolbar_back_button
import kotlinx.android.synthetic.main.activity_card_requisities.toolbar_title
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import javax.inject.Inject

class AccountOperationInternalVerifyActivity : BaseActivity() {

    lateinit var viewmodel: AccountOperationInternalVerifyViewModel

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private val dtAccountId: Long by lazy { intent.getLongExtra("dtAccountId", 0L) }
    private val dtIban: String by lazy { intent.getStringExtra("dtIban") }
    private val crIban: String by lazy { intent.getStringExtra("crIban") }
    private val amount: String by lazy { intent.getStringExtra("amount") }
    private val accountName: String by lazy { intent.getStringExtra("accountName") }
    private val purpose: String by lazy { intent.getStringExtra("purpose") }
    private val currency: String by lazy { intent.getStringExtra("currency") }

    private val decimalFormat = DecimalFormat("0.00")
    private val symbols = DecimalFormatSymbols()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_operation_internal_verify)

        decimalFormat.roundingMode = RoundingMode.HALF_EVEN
        symbols.decimalSeparator = ','
        decimalFormat.decimalFormatSymbols = symbols

        toolbar_title.text = getString(R.string.internal_transfer)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        viewmodel = ViewModelProvider(
            this,
            factory
        )[AccountOperationInternalVerifyViewModel::class.java]
        val curr = CurrencySignMapper().getCurrencySign(currency)
        val bigAmount = "$curr ${decimalFormat.format(amount.toDouble())}"

        fromAccount.text = dtIban
        verifyToAccount.text = crIban //todo

        if (!intent.getStringExtra("taxNo").isNullOrEmpty()) {
            opVerifyCrIban.text = intent.getStringExtra("taxNo")
        }
        opVerifyAmount.text = decimalFormat.format(amount.toDouble())
        opVerifyPurpose.text = purpose
        opVerifyToAccountName.text = accountName
        //bigViewAmount.text = bigAmount
        when (curr) {
            "AZN" -> {
                currencyImage.setBackgroundResource(R.drawable.ic_manat_white)
            }
            "USD" -> {
                currencyImage.setBackgroundResource(R.drawable.ic_dollar)
            }
            "EUR" -> {
                currencyImage.setBackgroundResource(R.drawable.ic_euro)
            }
        }
        bigAmountInteger.text = firstPartOfMoney(amount.toDouble())
        bigAmountReminder.text = secondPartOfMoney(amount.toDouble())


        submitButton.setOnClickListener {
            showProgressBar(true)
            viewmodel.inputs.createInternalOperation(
                dtAccountId = dtAccountId,
                crIban = crIban,
                amount = amount.toDouble(),
                purpose = purpose,
                taxNo = taxNo()
            )
        }
        setOutputListeners()
    }

    private fun taxNo(): String? {
        return if (intent.getStringExtra("taxNo").isNullOrEmpty())
            null
        else intent.getStringExtra("taxNo")
    }

    private fun setOutputListeners() {
        viewmodel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialogWithCloseOption()
        }.addTo(subscriptions)
        viewmodel.outputs.createInternalOperationSuccess().subscribe {
            val intent = Intent()
            intent.putExtra("accountResponse", it)
            intent.putExtra("operationNo", it.operationNo)
            intent.putExtra("operationStatus", it.operationStatus)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }.addTo(subscriptions)

        viewmodel.outputs.showTax().subscribe {
            if (it) {
                opVerifyCrAccIbanContainer.visibility = View.VISIBLE
                opVerifyCrIban.text = intent.getStringExtra("taxNo")
            } else {
                opVerifyCrAccIbanContainer.visibility = View.GONE
            }
        }.addTo(subscriptions)
    }

    private fun animateProgressImage() {
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        progressImage.startAnimation(rotate)
    }

    private fun revertProgressImageAnimation() {
        progressImage.clearAnimation()
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressBar(show: Boolean) {

        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage()
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(this, R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation()
        }
    }
}
