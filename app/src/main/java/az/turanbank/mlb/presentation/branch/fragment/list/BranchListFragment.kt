package az.turanbank.mlb.presentation.branch.fragment.list


import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.branch.BranchMapResponseModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.branch.fragment.BranchViewModel
import com.google.android.material.tabs.TabLayout
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject


class BranchListFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: BranchViewModel

    lateinit var branchAdapter: BranchListAdapter

    private lateinit var recyclerListView: RecyclerView

    private lateinit var sendCoordinates: SendCoordinates

    private lateinit var tabs: TabLayout

    private val branchList = arrayListOf<BranchMapResponseModel>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_branch_list, container, false)

        viewModel =
            ViewModelProvider(this, factory)[BranchViewModel::class.java]

        setHasOptionsMenu(true)
        recyclerListView = view.findViewById(R.id.recyclerView)

        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL

        tabs = requireActivity().findViewById(R.id.tabs)
        recyclerListView.layoutManager = llm

        branchAdapter = BranchListAdapter(branchList) { position ->
            sendCoordinates.sendLatLong(branchList[position].latitude, branchList[position].longitude)
            tabs.selectTab(tabs.getTabAt(1))
        }

        recyclerListView.adapter = branchAdapter

        setOutputListeners()
        setInputListeners()

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            sendCoordinates = requireActivity() as SendCoordinates
        } catch (exc: Exception) {
            exc.printStackTrace()
        }
    }

    interface SendCoordinates {
        fun sendLatLong(latitude: Double, longitude: Double)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        searchView.maxWidth = Integer.MAX_VALUE


        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                branchAdapter.filter?.filter(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                branchAdapter.filter?.filter(newText)
                return true
            }

        })

    }

    private fun setOutputListeners() {
        viewModel.outputs.onBranchesSuccess().subscribe {
            updateAdapter(it)
        }.addTo(subscriptions)
    }

    private fun updateAdapter(branches: ArrayList<BranchMapResponseModel>) {
        branchList.clear()
        branchList.addAll(branches)
        recyclerListView.adapter?.notifyDataSetChanged()
    }

    private fun setInputListeners() {
        viewModel.inputs.getBranches()
    }


}
