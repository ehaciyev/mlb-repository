package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountStatement
import az.turanbank.mlb.domain.user.usecase.resources.account.GetAccountStatementForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetAccountStatementForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

interface AccountActivityViewModelInputs : BaseViewModelInputs {
    fun getAccountStatement(accountId: Long)
}

interface AccountActivityViewModelOutputs : BaseViewModelOutputs {
    fun accountStatement(): Observable<ArrayList<AccountStatement>>
    fun accountStatementFinished(): CompletableSubject
}

class AccountActivityViewModel @Inject constructor(
    private val getAccountStatementForIndividualCustomerUseCase: GetAccountStatementForIndividualCustomerUseCase,
    private val getCardStatementForJuridicalCustomerUseCase: GetAccountStatementForJuridicalCustomerUseCase,
    private val sharedPrefs: SharedPreferences
) :
        BaseViewModel(),
        AccountActivityViewModelInputs,
        AccountActivityViewModelOutputs {

    override fun accountStatementFinished() = accountStatementFinished

    override fun accountStatement() = accountStatementList

    override val inputs: AccountActivityViewModelInputs = this
    override val outputs: AccountActivityViewModelOutputs = this

    private val accountStatementList = PublishSubject.create<ArrayList<AccountStatement>>()
    private val accountStatementFinished = CompletableSubject.create()

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    override fun getAccountStatement(accountId: Long) {
        if (isCustomerJuridical) {
            getCardStatementForJuridicalCustomerUseCase.execute(
                    custId = custId,
                    compId = compId,
                    token = token,
                    accountId = accountId,
                    startDate = today(),
                    endDate = today()
            )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        accountStatementFinished.onComplete()
                        if (it.status.statusCode == 1) {
                            accountStatementList.onNext(it.accountStatementList)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    }).addTo(subscriptions)
        } else {
            getAccountStatementForIndividualCustomerUseCase.execute(
                    custId = custId,
                    token = token,
                    accountId = accountId,
                    beginDate = today(),
                    endDate = today()
            )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        accountStatementFinished.onComplete()
                        if (it.status.statusCode == 1) {
                            accountStatementList.onNext(it.accountStatementList)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    })
                    .addTo(subscriptions)
        }
    }

    private fun today(): String = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date()).replace("-", ".")
}