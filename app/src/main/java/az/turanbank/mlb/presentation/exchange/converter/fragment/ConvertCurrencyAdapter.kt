package az.turanbank.mlb.presentation.exchange.converter.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ExchangeFlagMapper
import az.turanbank.mlb.data.remote.model.exchange.CurrencyResponseModel
import az.turanbank.mlb.presentation.exchange.converter.fragment.cashConverter.ExchangeFlagMapperConverter
import kotlinx.android.synthetic.main.turan_currency_list_item.view.currency_icon
import kotlinx.android.synthetic.main.turan_currency_list_item.view.currency_name

class ConvertCurrencyAdapter (private val isConverter: Boolean, private val context: Context, private val currencyList: ArrayList<CurrencyResponseModel>,private val clickListener: (position: Int) -> Unit) : RecyclerView.Adapter<ConvertCurrencyAdapter.CurrencyViewHolder>() {
    var selectedItem = -1
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CurrencyViewHolder = CurrencyViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.convert_currency_item_view,
            parent,
            false))

    override fun getItemCount() = currencyList.size

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(isConverter, currencyList[position], clickListener)
    }
    class CurrencyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(isConverter: Boolean, currency: CurrencyResponseModel, clickListener: (position: Int) -> Unit) {
            with(currency) {
                itemView.isClickable = true
                itemView.currency_name.text = this.currency
                if(isConverter){
                    itemView.currency_icon.setImageResource(ExchangeFlagMapperConverter().resolveFlag(this.currency))
                } else {
                    itemView.currency_icon.setImageResource(ExchangeFlagMapper().resolveFlag(this.currency))
                }
                itemView.setOnClickListener{
//                    itemView.setBackgroundColor(R.color.selectedRecyclerViewItemColor)
                    clickListener.invoke(adapterPosition)
                }
            }
        }
    }
}