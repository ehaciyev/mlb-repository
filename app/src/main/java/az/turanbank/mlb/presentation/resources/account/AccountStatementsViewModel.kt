package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.resources.account.AccountStatement
import az.turanbank.mlb.domain.user.usecase.GetCreditStatementForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.GetCreditStatementForJuridicalCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.GetDebitStatementForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.GetDebitStatementForJuridicalCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetAccountStatementForIndividualCustomerUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.GetAccountStatementForJuridicalCustomerUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountStatementsActivityViewModelInputs : BaseViewModelInputs {
    fun getAccountStatement(accountId: Long, startDate: String, endDate: String)
    fun getAccountStatement(accountId: Long, startDate: String, endDate: String, statementType: Int)
}

interface AccountStatementsActivityViewModelOutputs : BaseViewModelOutputs {
    fun accountStatement(): Observable<ArrayList<AccountStatement>>
    fun showProgress(): Observable<Boolean>
}

class AccountStatementsViewModel @Inject constructor(
    private val getAccountStatementForIndividualCustomerUseCase: GetAccountStatementForIndividualCustomerUseCase,
    private val getAccountStatementForJuridicalCustomerUseCase: GetAccountStatementForJuridicalCustomerUseCase,
    private val getCreditStatementForIndividualCustomerUseCase: GetCreditStatementForIndividualCustomerUseCase,
    private val getCreditStatementForJuridicalCustomerUseCasee: GetCreditStatementForJuridicalCustomerUseCase,
    private val getDebitStatementForIndividualCustomerUseCase: GetDebitStatementForIndividualCustomerUseCase,
    private val getDebitStatementForJuridicalCustomerUseCase: GetDebitStatementForJuridicalCustomerUseCase,
    private val sharedPrefs: SharedPreferences
) :
    BaseViewModel(),
    AccountStatementsActivityViewModelInputs,
    AccountStatementsActivityViewModelOutputs {

    override fun showProgress() = showProgress

    override val inputs: AccountStatementsActivityViewModelInputs = this
    override val outputs: AccountStatementsActivityViewModelOutputs = this

    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)

    private val accountStatementList = PublishSubject.create<ArrayList<AccountStatement>>()
    private val showProgress = PublishSubject.create<Boolean>()

    override fun getAccountStatement(accountId: Long, startDate: String, endDate: String) {
        showProgress.onNext(true)
        if (isCustomerJuridical) {
            getAccountStatementForJuridicalCustomerUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                accountId = accountId,
                startDate = startDate,
                endDate = endDate
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountStatementList.onNext(it.accountStatementList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getAccountStatementForIndividualCustomerUseCase.execute(
                custId = custId,
                token = token,
                accountId = accountId,
                beginDate = startDate,
                endDate = startDate
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    showProgress.onNext(false)
                    if (it.status.statusCode == 1) {
                        accountStatementList.onNext(it.accountStatementList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                })
                .addTo(subscriptions)
        }
    }

    override fun accountStatement(): Observable<ArrayList<AccountStatement>> = accountStatementList

    override fun getAccountStatement(
        accountId: Long,
        startDate: String,
        endDate: String,
        statementType: Int
    ) {
        showProgress.onNext(true)
        if (isCustomerJuridical) {

            //All
            when (statementType) {
                0 -> getAccountStatementForJuridicalCustomerUseCase.execute(
                    custId = custId,
                    compId = compId,
                    token = token,
                    accountId = accountId,
                    startDate = startDate,
                    endDate = endDate
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        showProgress.onNext(false)
                        if (it.status.statusCode == 1) {
                            accountStatementList.onNext(it.accountStatementList)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    }).addTo(subscriptions)
                1 -> getCreditStatementForJuridicalCustomerUseCasee.execute(
                    custId = custId,
                    compId = compId,
                    token = token,
                    accountId = accountId,
                    beginDate = startDate,
                    endDate = endDate
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        showProgress.onNext(false)
                        if (it.status.statusCode == 1) {
                            accountStatementList.onNext(it.accountStatementList)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    }).addTo(subscriptions)
                2 -> getDebitStatementForJuridicalCustomerUseCase.execute(
                    custId = custId,
                    compId = compId,
                    token = token,
                    accountId = accountId,
                    beginDate = startDate,
                    endDate = endDate
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        showProgress.onNext(false)
                        if (it.status.statusCode == 1) {
                            accountStatementList.onNext(it.accountStatementList)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    }).addTo(subscriptions)
            }
        } else {
            when (statementType) {
                0 -> getAccountStatementForIndividualCustomerUseCase.execute(
                    custId = custId,
                    token = token,
                    accountId = accountId,
                    beginDate = startDate,
                    endDate = endDate
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        showProgress.onNext(false)
                        if (it.status.statusCode == 1) {
                            accountStatementList.onNext(it.accountStatementList)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    })
                    .addTo(subscriptions)
                1 -> getCreditStatementForIndividualCustomerUseCase.execute(
                    custId = custId,
                    token = token,
                    accountId = accountId,
                    beginDate = startDate,
                    endDate = endDate
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        showProgress.onNext(false)
                        if (it.status.statusCode == 1) {
                            accountStatementList.onNext(it.accountStatementList)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    })
                    .addTo(subscriptions)
                2 -> getDebitStatementForIndividualCustomerUseCase.execute(
                    custId = custId,
                    token = token,
                    accountId = accountId,
                    beginDate = startDate,
                    endDate = endDate
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        showProgress.onNext(false)
                        if (it.status.statusCode == 1) {
                            accountStatementList.onNext(it.accountStatementList)
                        } else {
                            error.onNext(it.status.statusCode)
                        }
                    }, {
                        it.printStackTrace()
                        error.onNext(1878)
                    })
                    .addTo(subscriptions)
            }
        }
    }
}