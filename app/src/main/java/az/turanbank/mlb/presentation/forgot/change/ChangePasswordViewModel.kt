package az.turanbank.mlb.presentation.forgot.change

import az.turanbank.mlb.domain.user.usecase.forgot.ChangePasswordIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.forgot.ChangePasswordJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import javax.inject.Inject

interface ChangePasswordViewModelInputs: BaseViewModelInputs {
    fun changePassword(custId: Long, compId: Long, password: String, repeatPassword: String)
}
interface ChangePasswordViewModelOutputs: BaseViewModelOutputs {
    fun onPasswordChanged(): CompletableSubject
}

class ChangePasswordViewModel @Inject constructor(
    private val changePasswordIndividualUseCase: ChangePasswordIndividualUseCase,
    private val changePasswordJuridicalUseCase: ChangePasswordJuridicalUseCase
) : BaseViewModel(),
        ChangePasswordViewModelInputs,
        ChangePasswordViewModelOutputs{

    override val inputs: ChangePasswordViewModelInputs = this

    override val outputs: ChangePasswordViewModelOutputs = this

    private var passwordChanged = CompletableSubject.create()
    override fun changePassword(custId: Long, compId: Long, password: String, repeatPassword: String) {

        if (compId == 0L) {
            changePasswordIndividualUseCase.execute(custId, password, repeatPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        passwordChanged.onComplete()
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            changePasswordJuridicalUseCase.execute(custId, compId, password, repeatPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        passwordChanged.onComplete()
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onPasswordChanged() = passwordChanged

}