package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.AuthOperation
import az.turanbank.mlb.data.remote.model.response.SignFileResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.SignFileSecondAuthUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.SignFileUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface SignFileViewModelInputs : BaseViewModelInputs {
    fun signFile(transactionId: Long, operationIds: ArrayList<AuthOperation>,successOperId: ArrayList<Long>, failOperId: ArrayList<Long>)
}

interface SignFileViewModelOutputs : BaseViewModelOutputs {
    fun showProgress(): PublishSubject<Boolean>
    fun signFileSuccess(): PublishSubject<SignFileResponseModel>
}

class SignFileViewModel @Inject constructor(
    private val signFileUseCase: SignFileUseCase,
    private val signFileSecondAuthUseCase: SignFileSecondAuthUseCase,
    private val sharedPrefs: SharedPreferences
) :
    BaseViewModel(),
    SignFileViewModelInputs,
    SignFileViewModelOutputs {

    override fun signFileSuccess() = signFileSuccess

    override val inputs: SignFileViewModelInputs = this
    override val outputs: SignFileViewModelOutputs = this

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    val phoneNumber = sharedPrefs.getString("phoneNumber", "")
    val userId = sharedPrefs.getString("userId", "")
    val certCode = sharedPrefs.getString("certCode", "")
    val lang = sharedPrefs.getString("lang", "az")
    var enumLangType = EnumLangType.AZ
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val showProgress = PublishSubject.create<Boolean>()
    private val signFileSuccess = PublishSubject.create<SignFileResponseModel>()

    override fun showProgress() = showProgress

    override fun signFile(transactionId: Long, operationIds: ArrayList<AuthOperation>, successOperId: ArrayList<Long>, failOperId: ArrayList<Long>) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }

        val operIds = arrayListOf<Long>()

        for (op in operationIds) {
            operIds.add(op.operId!!)
        }

        if (isCustomerJuridical) {
            signFileSecondAuthUseCase.execute(
                custId = custId,
                compId = compId,
                token = token,
                operationIds = operIds,
                transactionId = transactionId,
                phoneNumber = phoneNumber,
                userId = userId,
                certCode = certCode,
                lang = enumLangType,
                successOperId = successOperId,
                failOperId = failOperId
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        signFileSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            signFileUseCase.execute(
                custId = custId,
                token = token,
                operationIds = operIds,
                transactionId = transactionId,
                phoneNumber = phoneNumber,
                userId = userId,
                certCode = certCode,
                lang = enumLangType,
                successOperId = successOperId,
                failedOperId = failOperId
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        signFileSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }


    }


}