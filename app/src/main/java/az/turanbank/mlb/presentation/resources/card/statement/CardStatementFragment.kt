package az.turanbank.mlb.presentation.resources.card.statement

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.adapter.MlbHomeCardStatementRecyclerViewAdapter
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.view.SwipeController
import az.turanbank.mlb.presentation.view.SwipeControllerActions
import com.facebook.shimmer.ShimmerFrameLayout
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.account_statements_fragment.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class CardStatementFragment : BaseFragment() {

    private var startDateCalendar: Calendar = Calendar.getInstance()
    private var endDateCalendar: Calendar = Calendar.getInstance()

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var accountStatementsEndDateCalendar: LinearLayout
    private lateinit var accountStatementsStartDateCalendar: LinearLayout
    private lateinit var accountStatementRecyclerView: RecyclerView
    private lateinit var accountStatementEndDateEditText: EditText
    private lateinit var accountStatementStartDateEditText: EditText
    private lateinit var accountStatementProgressImage: ImageView
    lateinit var viewModel: CardStatementFragmentViewModel

    lateinit var shimmerCardContainer: ShimmerFrameLayout

    lateinit var swipeController: SwipeController


    @SuppressLint("SetTextI18n")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.account_statements_fragment, container, false)


        viewModel = ViewModelProvider(this, factory)[CardStatementFragmentViewModel::class.java]
        shimmerCardContainer = view.findViewById(R.id.shimmerCardContainer)
        accountStatementsEndDateCalendar = view.findViewById(R.id.accountStatementsEndDateCalendar)
        accountStatementEndDateEditText = view.findViewById(R.id.accountStatementEndDateEditText)
        accountStatementStartDateEditText = view.findViewById(R.id.accountStatementStartDateEditText)
        accountStatementsStartDateCalendar = view.findViewById(R.id.accountStatementsStartDateCalendar)
        accountStatementProgressImage = view.findViewById(R.id.accountStatementProgressImage)
        accountStatementRecyclerView = view.findViewById(R.id.accountStatementRecyclerView)

        pickEndDate()
        pickStartDate()

        return view
    }


    private fun pickEndDate() {
        accountStatementsEndDateCalendar.setOnClickListener {
            val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                endDateCalendar.set(Calendar.YEAR, year)
                endDateCalendar.set(Calendar.MONTH, monthOfYear)
                endDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                accountStatementEndDateEditText.setText(simpleDateFormat().format(endDateCalendar.time).replace("-", "."))
                if(accountStatementStartDateEditText.toString().isNotEmpty()) {
                    getCardStatements()
                    //animateProgressImage()
                    shimmerCardContainer.startShimmerAnimation()
                    shimmerCardContainer.visibility = View.VISIBLE
                }
            }

            DatePickerDialog(
                requireContext(), R.style.DateDialogTheme, date, endDateCalendar
                    .get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH),
                endDateCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    private fun pickStartDate() {
        accountStatementsStartDateCalendar.setOnClickListener {
            val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                startDateCalendar.set(Calendar.YEAR, year)
                startDateCalendar.set(Calendar.MONTH, monthOfYear)
                startDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                accountStatementStartDateEditText.setText(
                    simpleDateFormat().format(
                        startDateCalendar.time
                    ).replace("-", ".")
                )
                if(accountStatementEndDateEditText.text.toString().isNotEmpty()) {
                    getCardStatements()
                    //animateProgressImage()
                    shimmerCardContainer.startShimmerAnimation()
                    shimmerCardContainer.visibility = View.VISIBLE
                }
            }

            DatePickerDialog(
                requireContext(), R.style.DateDialogTheme, date, startDateCalendar
                    .get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH),
                startDateCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }


    private fun getCardStatements() {

        val statementType = arguments?.getInt("type")
        viewModel.inputs.getCardStatement(
            requireActivity().intent.getLongExtra("cardId", 0L),
            requireActivity().intent.getStringExtra("cardNumber") ,
            simpleDateFormat().format(startDateCalendar.time).replace("-", "."),
            simpleDateFormat().format(endDateCalendar.time).replace("-", "."),
            statementType!!
        )

        setOutputListeners()
    }

    private fun animateProgressImage() {
        accountStatementProgressImage.visibility = View.VISIBLE
        accountStatementProgressImage.visibility = View.VISIBLE
        val rotate = RotateAnimation(
            0f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 2000
        rotate.interpolator = LinearInterpolator()
        rotate.repeatCount = Animation.INFINITE
        accountStatementProgressImage.startAnimation(rotate)
    }

    private fun setOutputListeners() {
        viewModel.outputs.showProgress().subscribe {
            if(it) {
                accountStatementRecyclerView.visibility = View.GONE
                failMessage.visibility = View.GONE
                //animateProgressImage()
                shimmerCardContainer.startShimmerAnimation()
            } else {
                //accountStatementProgressImage.clearAnimation()
                //accountStatementProgressImage.visibility = View.GONE
                shimmerCardContainer.stopShimmerAnimation()
                shimmerCardContainer.visibility = View.GONE
            }
        }.addTo(subscriptions)

        viewModel.outputs.cardStatement().subscribe {list ->
            shimmerCardContainer.stopShimmerAnimation()
            shimmerCardContainer.visibility = View.GONE

            accountStatementRecyclerView.visibility = View.VISIBLE
            failMessage.visibility = View.GONE

            val llm = LinearLayoutManager(requireContext())
            llm.orientation = LinearLayoutManager.VERTICAL
            accountStatementRecyclerView.layoutManager = llm

            val adapter =
                MlbHomeCardStatementRecyclerViewAdapter(list) {
                    val intent =
                        Intent(requireActivity(), CardStatementInternalActivity::class.java)
                    intent.putExtra("date", list[it].trDate)
                    intent.putExtra("cardNumber", list[it].cardNumber)
                    intent.putExtra("debetAmount", list[it].dtAmount)
                    intent.putExtra("creditAmount", list[it].crAmount)
                    intent.putExtra("purpose", list[it].purpose)
                    intent.putExtra("currency", list[it].currency)
                    intent.putExtra("fee", list[it].fee)
                    startActivity(intent)
                }
            accountStatementRecyclerView.adapter = adapter

            /*swipeController = SwipeController(object : SwipeControllerActions {
                override fun onRightClicked(position: Int) {
                    //open new page
                }
            })
            val itemTouchhelper = ItemTouchHelper(swipeController)
            itemTouchhelper.attachToRecyclerView(accountStatementRecyclerView)

            accountStatementRecyclerView.addItemDecoration(object : ItemDecoration() {
                override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
                    swipeController.onDraw(c)

                }

            })*/
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            shimmerCardContainer.stopShimmerAnimation()
            shimmerCardContainer.visibility = View.GONE

            accountStatementRecyclerView.visibility = View.GONE
            failMessage.text = getString(ErrorMessageMapper().getErrorMessage(it))
            failMessage.visibility = View.VISIBLE
        }.addTo(subscriptions)
    }

    private fun simpleDateFormat(): SimpleDateFormat {
        val myFormat = "dd-MM-yyyy"
        return SimpleDateFormat(myFormat, Locale.US)
    }

    override fun onResume() {
        super.onResume()
        shimmerCardContainer.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmerCardContainer.stopShimmerAnimation()
    }


}
