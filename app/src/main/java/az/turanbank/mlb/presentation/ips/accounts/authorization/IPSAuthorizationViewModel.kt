package az.turanbank.mlb.presentation.ips.accounts.authorization

import android.content.Context
import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.AccountResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.GetIpsAuthMethodResponseModel
import az.turanbank.mlb.data.remote.model.ips.response.RespAuthMethod
import az.turanbank.mlb.domain.user.usecase.ips.authorization.IPSAuthorizationUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.GetIpsAuthMethodUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.RegisterAccountsIpsSystemUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface IPSAuthorizationViewModelInputs: BaseViewModelInputs{

    fun getIpsAuthMethods()
    fun registerAccounts(accountId: ArrayList<Long>)
}

interface IPSAuthorizationViewModelOutputs: BaseViewModelOutputs{
    fun onGetIpsAuthMethods(): PublishSubject<ArrayList<RespAuthMethod>>
    fun accountsRegistered(): CompletableSubject
    fun onAuthEmpty(): PublishSubject<Boolean>
}

class IPSAuthorizationViewModel @Inject constructor(
    private val getIpsAuthMethodUseCase: GetIpsAuthMethodUseCase,
    private val registerAccountsIpsSystemUseCase: RegisterAccountsIpsSystemUseCase,
    private val sharedPrefs: SharedPreferences): BaseViewModel(), IPSAuthorizationViewModelInputs, IPSAuthorizationViewModelOutputs{

    override val inputs: IPSAuthorizationViewModelInputs = this
    override val outputs: IPSAuthorizationViewModelOutputs = this
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    private val accountsRegistered = CompletableSubject.create()
    private val unCreatedAccounts = PublishSubject.create<ArrayList<AccountResponseModel>>()

    private val authMethodList = PublishSubject.create<ArrayList<RespAuthMethod>>()

    private val onAuthEmpty = PublishSubject.create<Boolean>()

    override fun getIpsAuthMethods() {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        if (!isCustomerJuridical){
            getIpsAuthMethodUseCase
                .execute(custId, null, token, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if(it.status.statusCode == 1){
                        val authMethods = arrayListOf<RespAuthMethod>()
                        authMethods.addAll(it.authMethodList)
                        if(authMethods.size == 0){
                            onAuthEmpty.onNext(true)
                        } else {
                            onAuthEmpty.onNext(false)
                            authMethodList.onNext(authMethods)
                        }
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    it.printStackTrace()
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun accountsRegistered() = accountsRegistered
    override fun onAuthEmpty() = onAuthEmpty

    override fun registerAccounts(accountId: ArrayList<Long>) {
        registerAccountsIpsSystemUseCase
            .execute(accountId, custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    accountsRegistered.onComplete()
                } else if(it.status.statusCode == 178) {
                    var unCreated = arrayListOf<AccountResponseModel>()
                    it.accountList.forEach {items->
                        if (it.status.statusCode != 1) {
                            unCreated.add(items)
                        }
                    }
                    unCreatedAccounts.onNext(unCreated)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    private val accounts = arrayListOf<SelectAuthorizationAccountModel>()

    override fun onGetIpsAuthMethods() = authMethodList

}