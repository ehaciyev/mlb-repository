package az.turanbank.mlb.presentation.ips.paidToMe.paidtomechoice

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Base64
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_single_paid_to_me.*
import java.io.*
import javax.inject.Inject

class SinglePaidToMeActivity : BaseActivity() {

    lateinit var dialog: MlbProgressDialog

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: SinglePaidToMeViewModel


    private val paymentItem by lazy { intent.getLongExtra("paymentItem", 0L) }
    private val dtCustName by lazy { intent.getStringExtra("dtCustName") }
    private val dtIban by lazy { intent.getStringExtra("dtIban") }
    private val crIban by lazy { intent.getStringExtra("crIban") }
    private val amount by lazy { intent.getDoubleExtra("amount", 0.0) }
    private val currency by lazy { intent.getStringExtra("currency") }
    private val createdDate by lazy { intent.getStringExtra("createdDate") }
    private val operStateName by lazy { intent.getStringExtra("operStateName") }
    private val purpose by lazy { intent.getStringExtra("purpose") }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_paid_to_me)

        dialog = MlbProgressDialog(this)

        viewModel = ViewModelProvider(this, factory)[SinglePaidToMeViewModel::class.java]

        detailOpClose.setOnClickListener {
            finish()
        }

        detailOpShare.setOnClickListener { _ ->
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            //shareIntent.putExtra(Intent.EXTRA_TEXT, it.toString())
            //startActivity(Intent.createChooser(shareIntent, "Göndər..."))
        }

        detailTitle.text = getString(R.string.pay_only)
        nameSurname.text = dtCustName
        debitNumber.text = dtIban
        creditNumber.text = crIban
        amount_money.text = "$amount"
        exchange.text = currency
        detailTitle.text = operStateName
        createDate.text = createdDate
        purposeText.text = purpose

        setOutputListener()
        setInputListener()

    }

    private fun setOutputListener() {
        viewModel.outputs.paymentDoc().subscribe {
            val fileName = "$paymentItem.pdf"
            val file = getFileDestination(fileName)
            //  val destPath = getExternalFilesDir(null)?.absolutePath
            //  val file = File(destPath, fileName)

            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                inputStream = ByteArrayInputStream(byteArray)

                file?.let {f ->
                    outputStream = FileOutputStream(f)
                }

                while (true) {
                    val read = inputStream.read(fileReader)

                    if (read == -1) {
                        break
                    }
                    outputStream?.write(fileReader, 0, read)
                }

                Toast.makeText(this, fileName + getString(R.string.successfully_downloaded), Toast.LENGTH_LONG)
                    .show()

                openDownloadedPdfFile(file)

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)

        viewModel.outputs.paymentADoc().subscribe {
            val fileName = "$paymentItem.adoc"

            //  val filePath = File(Environment.getExternalStorageDirectory(), null)
            val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName)
            //   val destPath = getExternalFilesDir(null)?.absolutePath
            //   val file = File(destPath, fileName)

            val byteArray = Base64.decode(it.bytes, Base64.DEFAULT)

            var iStream: InputStream? = null
            var oStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                iStream = ByteArrayInputStream(byteArray)
                oStream = FileOutputStream(file)

                while (true) {
                    val read = iStream.read(fileReader)

                    if (read == -1) {
                        break
                    }
                    oStream.write(fileReader, 0, read)
                }

                Toast.makeText(this, fileName + getString(R.string.successfully_downloaded), Toast.LENGTH_LONG)
                    .show()

                openDirectoryIntent(file)

                //openAdocFolder(Environment.getExternalStorageDirectory())

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                iStream?.close()
                oStream?.close()
            }
        }.addTo(subscriptions)

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)
    }

    private fun setInputListener() {
        /*operationActionDownloadAdoc.setOnClickListener {
            if (isStoragePermissionGranted(2)) {
                viewModel.inputs.getAsanDocByOperId(paymentItem)
            }
        }

        operationActionDownloadPdf.setOnClickListener {
            if (isStoragePermissionGranted(1)) {
                viewModel.inputs.getPaymentDocByOperId(paymentItem)
            }
        }*/
    }

    private fun isStoragePermissionGranted(requestCode: Int): Boolean {
        return if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED
            )
                true
            else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    requestCode
                )
                false
            }
        } else {
            true
        }
    }


    fun openDirectoryIntent(file: File) {
        val selectedUri = FileProvider.getUriForFile(this, "$packageName.fp", file)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(selectedUri, "*/*")
        startActivity(intent)
    }

    private fun openDownloadedPdfFile(file: File?) {
        try {
            val apkURI = FileProvider.getUriForFile(
                this,
                applicationContext
                    .packageName + ".provider", file!!
            )

            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(apkURI, "application/pdf")
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val requestedPermissionsCount = permissions.size
        var grantedPermissionsCount = 0

        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                grantedPermissionsCount++
            }
        }

        if (requestedPermissionsCount == grantedPermissionsCount) {
            if (requestCode == 1) {
                viewModel.inputs.getPaymentDocByOperId(paymentItem)
            } else if (requestCode == 2) {
                viewModel.inputs.getAsanDocByOperId(paymentItem)
            }
        }
    }

    private fun getFileDestination(fileName: String): File? {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            return File(Environment.getExternalStorageDirectory(), fileName)
        }
        return getExternalFilesDir(null)
    }
}
