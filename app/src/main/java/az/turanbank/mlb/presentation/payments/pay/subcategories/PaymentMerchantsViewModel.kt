package az.turanbank.mlb.presentation.payments.pay.subcategories

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.pg.response.payments.merchant.MerchantResponseModel
import az.turanbank.mlb.domain.user.usecase.pg.merchant.GetMerchantListIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.merchant.GetMerchantListJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface PaymentMerchantsViewModelInputs: BaseViewModelInputs{
    fun getMerchants(categoryId: Long)
}
interface PaymentMerchantsViewModelOutputs: BaseViewModelOutputs{
    fun onMerchants(): PublishSubject<ArrayList<MerchantResponseModel>>
}
class PaymentMerchantsViewModel @Inject constructor(
    private val getMerchantListIndividualUseCase: GetMerchantListIndividualUseCase,
    private val getMerchantListJuridicalUseCase: GetMerchantListJuridicalUseCase,
    sharedPrefs: SharedPreferences
): BaseViewModel(),
        PaymentMerchantsViewModelInputs,
        PaymentMerchantsViewModelOutputs{
    override val inputs: PaymentMerchantsViewModelInputs = this

    override val outputs: PaymentMerchantsViewModelOutputs = this


    private val token = sharedPrefs.getString("token", "")
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val lang = sharedPrefs.getString("lang", "az")
    var enumLangType = EnumLangType.AZ

    private val merchants = PublishSubject.create<ArrayList<MerchantResponseModel>>()

    override fun getMerchants(categoryId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        if (isCustomerJuridical){
            getMerchantListJuridicalUseCase
                .execute(custId, compId, token, categoryId, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        merchants.onNext(it.merchantList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        } else {
            getMerchantListIndividualUseCase
                .execute(custId, token, categoryId, enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.status.statusCode == 1) {
                        merchants.onNext(it.merchantList)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }

    override fun onMerchants() = merchants
}