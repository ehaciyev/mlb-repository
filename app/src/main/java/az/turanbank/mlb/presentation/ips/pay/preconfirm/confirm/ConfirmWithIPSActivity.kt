package az.turanbank.mlb.presentation.ips.pay.preconfirm.confirm

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.util.decimalFormat
import az.turanbank.mlb.util.firstPartOfMoney
import az.turanbank.mlb.util.secondPartOfMoney
import az.turanbank.mlb.util.splitString
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_confirm_ips.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import java.text.DecimalFormat
import javax.inject.Inject

class ConfirmWithIPSActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ConfirmWithIPSViewModel

    lateinit var submitButton: LinearLayout

    lateinit var bigViewAmount: TextView

    lateinit var amount_ : TextView

    lateinit var fromAccountTextView: TextView
    lateinit var toAccountTextView: TextView

    lateinit var dialog: MlbProgressDialog

    lateinit var fromAccountOrAliasName: TextView

    lateinit var toAccountOrAliasName: TextView

    lateinit var receiverFullName: TextView

    lateinit var ipsPurposeText: TextView

    lateinit var voenName: TextView

    private val senderAccountOrAlias: String by lazy { intent.getStringExtra("senderAccountOrAlias") }
    private val receiverAccountOrAlias: String by lazy { intent.getStringExtra("receiverAccountOrAlias") }
    private val ipsPurpose: String by lazy { intent.getStringExtra("ipsPurpose") }
    private val amount: Double by lazy { intent.getDoubleExtra("amount",0.0) }
    private val senderIBAN: String by lazy { intent.getStringExtra("senderIBAN") }
    private val businessProcessId: Long by lazy { intent.getLongExtra("businessProcessId",0L) }
    private val receiverId: Long by lazy { intent.getLongExtra("receiverId",0L) }
    private val fromAccount: Boolean by lazy { intent.getBooleanExtra("fromAccount",true) }
    private val toAccount: Boolean by lazy { intent.getBooleanExtra("toAccount",true) }
    private val senderAliasTypeId: Int by lazy { intent.getIntExtra("senderAliasTypeId",0) }
    private val receiverCustomerName: String by lazy { intent.getStringExtra("receiverCustomerName") }
    private val receiverCustomerSurname: String by lazy { intent.getStringExtra("receiverCustomerSurname") }
    private val voenCode: String by lazy { intent.getStringExtra("voenCode") }
    private val ipsMessage: String by lazy { intent.getStringExtra("ipsMessage") }
    private val senderFullName: String by lazy { intent.getStringExtra("senderFullName") }
    private val budgetLvl: String by lazy { intent.getStringExtra("budgetLvl") }
    private val budgetCode: String by lazy { intent.getStringExtra("budgetCode") }
    private val receiverIban: String by lazy { intent.getStringExtra("receiverIban") }

    private val ipsPaymentType: Int by lazy { intent.getIntExtra("ipsPaymentType",1) }

    private var bLvl = ""
    private var bCode = ""


    var receiverFullNameString = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_ips)

        toolbar_title.text = getString(R.string.payment_operation_confirmation)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        viewModel = ViewModelProvider(this, factory)[ConfirmWithIPSViewModel::class.java]

        initViews()
        setDataToView()

        setOutputListener()
        setInputListener()

    }

    private fun setDataToView() {

        if(!budgetLvl.isNullOrEmpty()){
            bLvl = budgetLvl
        }

        if(!budgetCode.isNullOrEmpty()){
            bCode = budgetCode
        }

        amount.toString().replace(',','.')
        amount_.text = splitString(decimalFormat(amount, DecimalFormat("0.00"),',') + "AZN",',')

        if(fromAccount && toAccount){
            fromAccountOrAliasName.text = getString(R.string.from_account_text)
            toAccountOrAliasName.text = getString(R.string.to_account_text)
        } else if(!fromAccount && !toAccount){
            fromAccountOrAliasName.text = getString(R.string.from_alias_text)
            toAccountOrAliasName.text = getString(R.string.to_alias_text)
        } else if(fromAccount && !toAccount){
            fromAccountOrAliasName.text = getString(R.string.from_account_text)
            toAccountOrAliasName.text = getString(R.string.to_alias_text)
        } else if(!fromAccount && toAccount){
            fromAccountOrAliasName.text = getString(R.string.from_alias_text)
            toAccountOrAliasName.text = getString(R.string.to_account_text)
        }

        senderFullNameText.text = senderFullName

        bigAmountInteger.text =  firstPartOfMoney(amount)
        bigAmountReminder.text = secondPartOfMoney(amount)

        fromAccountTextView.text = senderAccountOrAlias
        toAccountTextView.text = receiverAccountOrAlias



        if(!receiverCustomerSurname.isNullOrEmpty() && !receiverCustomerSurname.isNullOrEmpty()){
            receiverFullName.text = "$receiverCustomerName $receiverCustomerSurname"
            receiverFullNameString = "$receiverCustomerName $receiverCustomerSurname"
        } else{
            receiverFullName.text = "$receiverCustomerName"
            receiverFullNameString = "$receiverCustomerName"
        }

        voenName.text = voenCode

        ipsPurposeText.text = ipsPurpose
    }

    private fun setInputListener() {
        submitButton.setOnClickListener{
            if(fromAccount && toAccount){ // account to account
                viewModel.inputs.createAccounttoAccountOperation(
                    receiverId,ipsPaymentType, 0, "",
                    amount, senderFullName, receiverFullNameString ,
                    senderAccountOrAlias, receiverAccountOrAlias ,
                    ipsMessage,"test", ipsPurpose, bLvl, bCode)

            } else if(fromAccount && !toAccount) { // account to alias
                viewModel.inputs.createAccounttoAccountOperation(
                    receiverId,ipsPaymentType, 0, "",
                    amount, senderFullName, receiverFullNameString ,
                    senderIBAN, receiverIban ,
                    ipsMessage,"test", ipsPurpose, bLvl, bCode)

            } else if(!fromAccount && toAccount){ // alias to account
                viewModel.inputs.createAccounttoAccountOperation(
                    receiverId,ipsPaymentType, 0, "",
                    amount, senderFullName, receiverFullNameString ,
                    senderIBAN, receiverAccountOrAlias ,
                    ipsMessage,"test", ipsPurpose, bLvl, bCode)}
            else if(!fromAccount && !toAccount){ //alis to alias
                viewModel.inputs.createAccounttoAccountOperation(
                    receiverId,ipsPaymentType, 0, "",
                    amount, senderFullName, receiverFullNameString ,
                    senderIBAN, receiverIban ,
                    ipsMessage,"test", ipsPurpose, bLvl, bCode)
            }
        }
    }

    private fun setOutputListener() {

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onCreateOperationSucess().subscribe {
            setResult(Activity.RESULT_OK, Intent().
            putExtra("description", getString(R.string.order_created)).
            putExtra("finishOnly", false).
            putExtra("goToPage", "myPayment"))
            finish()
        }.addTo(subscriptions)

        viewModel.onError().subscribe {
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun initViews(){
        submitButton = findViewById(R.id.submitButton)
        dialog = MlbProgressDialog(this)
        //bigViewAmount = findViewById(R.id.bigViewAmount)
        ipsPurposeText = findViewById(R.id.ipsPurposeText)
        amount_ = findViewById(R.id.amount_)
        fromAccountOrAliasName = findViewById(R.id.fromAccountOrAliasName)
        toAccountOrAliasName = findViewById(R.id.toAccountOrAliasName)
        fromAccountTextView = findViewById(R.id.fromAccountTextView)
        toAccountTextView = findViewById(R.id.toAccountTextView)
        voenName = findViewById(R.id.voenName)
        receiverFullName = findViewById(R.id.receiverFullName)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 7){
            if (resultCode == Activity.RESULT_OK){
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }
}
