package az.turanbank.mlb.presentation.ips.accounts.register

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.ips.response.AccountResponseModel
import az.turanbank.mlb.data.remote.model.response.DeleteIPSAccount
import az.turanbank.mlb.domain.user.usecase.ips.credentials.GetUnregisteredIpsAccountsUseCase
import az.turanbank.mlb.domain.user.usecase.ips.register.RegisterAccountsIpsSystemUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.presentation.data.SelectUnregisteredAccountModel
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface RegisterAccountIpsSystemViewModelInputs: BaseViewModelInputs{
    fun getAccounts()
    fun registerAccounts(accountId: ArrayList<Long>)
}
interface RegisterAccountIpsSystemViewModelOutputs: BaseViewModelOutputs{
    fun onAccountListSuccess(): PublishSubject<ArrayList<SelectUnregisteredAccountModel>>
    fun accountsRegistered(): CompletableSubject
    fun unCreatedAccounts(): PublishSubject<ArrayList<AccountResponseModel>>
}
class RegisterAccountIpsSystemViewModel @Inject constructor(
    private val getUnregisteredIpsAccountsUseCase: GetUnregisteredIpsAccountsUseCase,
    private val registerAccountsIpsSystemUseCase: RegisterAccountsIpsSystemUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
        RegisterAccountIpsSystemViewModelInputs,
        RegisterAccountIpsSystemViewModelOutputs{
    override val inputs: RegisterAccountIpsSystemViewModelInputs = this

    override val outputs: RegisterAccountIpsSystemViewModelOutputs = this
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)

    private val accountList = PublishSubject.create<ArrayList<SelectUnregisteredAccountModel>>()
    private val unCreatedAccounts = PublishSubject.create<ArrayList<AccountResponseModel>>()

    private val accounts = arrayListOf<SelectUnregisteredAccountModel>()
    private val accountsRegistered = CompletableSubject.create()


    override fun getAccounts() {
        if (!isCustomerJuridical){
            getUnregisteredIpsAccountsUseCase
                .execute(custId, token, EnumLangType.AZ)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({accountsResponse ->

                    if (accountsResponse.status.statusCode == 1) {
                        val filteredList = arrayListOf<AccountResponseModel>()
                        accountsResponse.accountList.forEach {
                            if (it.currName == "AZN")
                                filteredList.add(it)
                        }
                        filteredList.forEach { accounts.add(SelectUnregisteredAccountModel(it, false)) }
                        accountList.onNext(accounts)
                    } else {
                        error.onNext(accountsResponse.status.statusCode)
                    }
                }, {
                    error.onNext(1878)
                }).addTo(subscriptions)
        }
    }
    override fun registerAccounts(accountId: ArrayList<Long>) {
        registerAccountsIpsSystemUseCase
            .execute(accountId, custId, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    accountsRegistered.onComplete()
                } else if(it.status.statusCode == 178) {
                    var unCreated = arrayListOf<AccountResponseModel>()
                    it.accountList.forEach {items->
                        if (it.status.statusCode != 1) {
                            unCreated.add(items)
                        }
                    }
                    unCreatedAccounts.onNext(unCreated)
                }else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun accountsRegistered() = accountsRegistered
    override fun unCreatedAccounts() = unCreatedAccounts

    override fun onAccountListSuccess() = accountList
}