package az.turanbank.mlb.presentation.ips.pay.debitorchoice.account

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.IPSCreditorAccountSelectActivity
import az.turanbank.mlb.util.animateProgressImage
import az.turanbank.mlb.util.revertProgressImageAnimation
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class IPSAccountChoiceFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: IPSAccountChoiceViewModel

    lateinit var dialog: MlbProgressDialog

    private lateinit var accountContainer: LinearLayout
    private lateinit var progressImage: ImageView
    private lateinit var submitText: TextView

    lateinit var submitButton: LinearLayout

    lateinit var account: AppCompatTextView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_ipsaccount_choice, container, false)

        viewModel =
            ViewModelProvider(this, factory)[IPSAccountChoiceViewModel::class.java]
        initView(view)
        setOutputListeners()
        setInputListeners()
        return view
    }

    override fun onResume() {
        super.onResume()
    }



    private fun initView(view: View){
        dialog = MlbProgressDialog(requireActivity())
        accountContainer = view.findViewById(R.id.accountContainer)
        submitButton = view.findViewById(R.id.submitButton)
        progressImage = view.findViewById(R.id.progressImage)
        submitText = view.findViewById(R.id.submitText)
        account = view.findViewById(R.id.account)
    }

    private fun setOutputListeners() {

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.accountsSuccess().subscribe { accountList ->
            accountContainer.setOnClickListener {
                val builder: android.app.AlertDialog.Builder =
                    android.app.AlertDialog.Builder(requireContext())
                builder.setTitle(getString(R.string.select_account))

                val list = arrayOfNulls<Long>(accountList.size)
                val nameList = arrayOfNulls<String>(accountList.size)

                for (i in 0 until accountList.size) {
                    list[i] = accountList[i].id
                    nameList[i] = accountList[i].iban
                }

                builder.setItems(nameList) { _, which ->
                    account.text = nameList[which]
                    viewModel.setSelectedAccount(nameList[which])
                    viewModel.setSelectedAccountId(list[which])
                }

                val dialog = builder.create()
                dialog.show()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onSenderSuccess().subscribe{
            val intent = Intent(requireActivity(), IPSCreditorAccountSelectActivity::class.java)
            intent.putExtra("senderAccountOrAlias", viewModel.getSelectedAccount())
            intent.putExtra("senderIBAN", viewModel.getSelectedAccount())
            intent.putExtra("senderFullName",it.name + " " + it.surname)
            intent.putExtra("fromAccount",true)
            startActivityForResult(intent,7)
            showProgressBar(false)
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getAccounts()
        submitButton.setOnClickListener {
            showProgressBar(true)
            if(account.text.trim().isEmpty()){
                AlertDialogMapper(requireActivity(), 124018).showAlertDialog()
                showProgressBar(false)
            } else {
                viewModel.inputs.getSenderInfo(account.text.trim().toString())
            }
        }
    }

    private fun showProgressBar(show: Boolean) {
        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        accountContainer.isClickable = !show
        account.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                requireActivity().setResult(Activity.RESULT_OK)
                requireActivity().finish()
            }
        }
    }
}
