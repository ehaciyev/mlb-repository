package az.turanbank.mlb.presentation.ips.accounts.setdefault.account

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.ips.AliasAccountCompoundModel
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_delete_ipsalias.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class ChooseAccountForDefaultActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: ChooseAccountForDefaultViewModel

    lateinit var adapter: ChooseAccountForDefaultAdapter

    private var selectedAccount = ""
    private var selectedAlias = ""
    private val linkedAccountList by lazy { intent.getSerializableExtra("linkedAccountList") as ArrayList<AliasAccountCompoundModel> }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_ips_account)

        viewModel =
            ViewModelProvider(this, factory)[ChooseAccountForDefaultViewModel::class.java]

        message.text = getString(R.string.choose_default_account_for_alias)
        toolbar_title.text = getString(R.string.choose_default_account)
        toolbar_back_button.setOnClickListener { onBackPressed() }
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm

        adapter =
            ChooseAccountForDefaultAdapter(
                linkedAccountList
            ) {
                selectedAccount = linkedAccountList[it].iban
                selectedAlias = linkedAccountList[it].alias
                viewModel.inputs.setDefault(linkedAccountList[it].id)
                //animateProgress()
            }

        progress.visibility = View.GONE
        recyclerView.adapter = adapter

        setOutputListeners()
    }

    private fun setOutputListeners() {
        viewModel.outputs.onDefaultCompleted().subscribe{
            /*progress.clearAnimation()
            progress.visibility = View.GONE*/
            finish()
            Toast.makeText(this, getString(R.string.account_succesfully_linked, selectedAccount, selectedAlias), Toast.LENGTH_LONG).show()
        }.addTo(subscriptions)
        viewModel.outputs.onError().subscribe{
            /*progress.clearAnimation()
            progress.visibility = View.GONE*/
            recyclerView.visibility = View.VISIBLE
            AlertDialogMapper(this, it).showAlertDialog()
        }.addTo(subscriptions)
    }

}
