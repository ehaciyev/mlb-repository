package az.turanbank.mlb.presentation.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class RegisterWithMobileActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: RegisterWithMobileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual_register_mobile)

        viewModel = ViewModelProvider(this, factory)[RegisterWithMobileViewModel::class.java]
        spanUserAgreement()
        toolbar_back_button.setOnClickListener { onBackPressed() }
        toolbar_title.text = resources.getString(R.string.register_with_mobile)
    }

    private fun spanUserAgreement() {
        val spanString = SpannableString(resources.getString(R.string.you_agree_terms_with_continue))

        val clickOnSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                startActivity(Intent(this@RegisterWithMobileActivity, UserAgreementActivity::class.java))
            }

            @SuppressLint("NewApi")
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.underlineColor = ContextCompat.getColor(baseContext, R.color.colorPrimaryDark)
            }
        }

        spanString.setSpan(clickOnSpan, 45, 78, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

//        user_agreement.text = spanString
//        user_agreement.movementMethod = LinkMovementMethod.getInstance()
//        user_agreement.highlightColor = Color.TRANSPARENT
    }
}
