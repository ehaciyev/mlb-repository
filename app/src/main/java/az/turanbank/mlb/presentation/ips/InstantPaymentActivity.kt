package az.turanbank.mlb.presentation.ips

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.ips.accounts.IPSAccountsActivity
import az.turanbank.mlb.presentation.ips.aliases.IPSAliasesActivity
import az.turanbank.mlb.presentation.ips.outgoing.MyIncomePaymentsActivity
import az.turanbank.mlb.presentation.ips.paidToMe.PaidToMeActivity
import az.turanbank.mlb.presentation.ips.pay.IPSPayActivity
import az.turanbank.mlb.presentation.ips.pay.debitorchoice.IPSTransferActivity
import az.turanbank.mlb.presentation.ips.payrequest.requestor.IPSPayRequestTabbedActivity
import kotlinx.android.synthetic.main.activity_instant_payment.*
import kotlinx.android.synthetic.main.mlb_toolbar_layout.*
import javax.inject.Inject

class InstantPaymentActivity : BaseActivity() {

    private val isCancel: Boolean by lazy { intent.getBooleanExtra("isCancel", true) }

    @Inject
    lateinit var factory: ViewModelProviderFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instant_payment)

        toolbar_title.text = getString(R.string.instant_payments)
        toolbar_back_button.setOnClickListener { onBackPressed() }

        ips_pay.setOnClickListener {
            startActivityForResult(Intent(this, IPSPayActivity::class.java),7)
            //startActivityForResult(Intent(this, IPSTransferActivity::class.java),7)
        }
        ips_request.setOnClickListener {
            startActivityForResult(Intent(this, IPSPayRequestTabbedActivity::class.java),7)
        }
        ips_aliases.setOnClickListener {
            startActivityForResult(Intent(this, IPSAliasesActivity::class.java),7)
        }
        ips_my_payments.setOnClickListener {
            startActivityForResult(Intent(this, MyIncomePaymentsActivity::class.java),7)
        }

        ips_paid_to_me.setOnClickListener{
            startActivityForResult(Intent(this, PaidToMeActivity::class.java),7)
        }

        ips_my_accounts.setOnClickListener{
            startActivityForResult(Intent(this, IPSAccountsActivity::class.java),7)
        }

        /*linkAliasToAccountContainer.setOnClickListener {
            //do something when you click
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                intent.putExtra("isCancel",isCancel)
                setResult(Activity.RESULT_OK,intent)
                finish()
            }
        }
    }
}
