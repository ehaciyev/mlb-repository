package az.turanbank.mlb.presentation.resources.account

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.response.DomesticOperationResponseModel
import az.turanbank.mlb.domain.user.usecase.resources.account.CreateInLandOperationIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.resources.account.CreateInLandOperationJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface AccountOperationInLandVerifyViewModelInputs : BaseViewModelInputs {
    fun createInLandOperation(
        dtAccountId: Long,
        crIban: String,
        crCustTaxid: String,
        crCustName: String,
        crBankCode: String,
        crBankName: String,
        crBankTaxid: String,
        crBankCorrAcc: String,
        budgetCode: String,
        budgetLvl: String,
        amount: Double,
        purpose: String,
        note: String,
        operationType: Int
    )
}

interface AccountOperationInLandVerifyViewModelOutputs : BaseViewModelOutputs {
    fun createInLandOperationSuccess(): PublishSubject<DomesticOperationResponseModel>
}
class AccountOperationInLandVerifyViewModel @Inject constructor(
    private val createInLandOperationJuridicalUseCase: CreateInLandOperationJuridicalUseCase,
    private val createInLandOperationIndividualUseCase: CreateInLandOperationIndividualUseCase,
    private val sharedPrefs: SharedPreferences
): BaseViewModel(), AccountOperationInLandVerifyViewModelInputs, AccountOperationInLandVerifyViewModelOutputs {

    override fun createInLandOperationSuccess() = domesticOperationSuccess

    override val inputs: AccountOperationInLandVerifyViewModelInputs = this
    override val outputs: AccountOperationInLandVerifyViewModelOutputs = this
    private val domesticOperationSuccess: PublishSubject<DomesticOperationResponseModel> = PublishSubject.create()

    val token = sharedPrefs.getString("token", "")
    val custId = sharedPrefs.getLong("custId", 0L)
    val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun createInLandOperation(
        dtAccountId: Long,
        crIban: String,
        crCustTaxid: String,
        crCustName: String,
        crBankCode: String,
        crBankName: String,
        crBankTaxid: String,
        crBankCorrAcc: String,
        budgetCode: String,
        budgetLvl: String,
        amount: Double,
        purpose: String,
        note: String,
        operationType: Int
    ) {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        if(isCustomerJuridical) {
            createInLandOperationJuridicalUseCase.execute(
                custId,
                compId,
                token,
                dtAccountId,
                crIban,
                crCustTaxid,
                crCustName,
                crBankCode,
                crBankName,
                crBankTaxid,
                crBankCorrAcc,
                budgetCode,
                budgetLvl,
                amount,
                purpose,
                note,
                operationType,
                enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if(it.status.statusCode == 1) {
                        domesticOperationSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                } ,{
                    it.printStackTrace()
                    error.onNext(1878)
                })
                .addTo(subscriptions)
        } else {
            createInLandOperationIndividualUseCase.execute(
                custId,
                token,
                dtAccountId,
                crIban,
                crCustTaxid,
                crCustName,
                crBankCode,
                crBankName,
                crBankTaxid,
                crBankCorrAcc,
                budgetCode,
                budgetLvl,
                amount,
                purpose,
                note,
                operationType,
                enumLangType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if(it.status.statusCode == 1) {
                        domesticOperationSuccess.onNext(it)
                    } else {
                        error.onNext(it.status.statusCode)
                    }
                } ,{
                    it.printStackTrace()
                    error.onNext(1878)
                })
                .addTo(subscriptions)
        }
    }
}