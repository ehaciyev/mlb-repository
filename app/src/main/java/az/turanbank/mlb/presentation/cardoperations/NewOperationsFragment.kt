package az.turanbank.mlb.presentation.cardoperations


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import az.turanbank.mlb.R
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.cardoperations.cardtocard.CardToCardActivity
import az.turanbank.mlb.presentation.cardoperations.othercard.CardToOtherCardActivity
import az.turanbank.mlb.presentation.cardoperations.cashbycode.CashByCodeActivity
import com.google.android.material.tabs.TabLayout
import javax.inject.Inject

class NewOperationsFragment : BaseFragment() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var betweenMyCards: LinearLayout
    private lateinit var cashByCode: LinearLayout
    private lateinit var toAnotherBank: LinearLayout
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_new_operations, container, false)
        betweenMyCards = view.findViewById(R.id.between_my_cards)
        cashByCode = view.findViewById(R.id.cashByCode)
        toAnotherBank = view.findViewById(R.id.toAnotherBank)

        setInputListeners()
        return view

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 13 || requestCode == 14 || requestCode == 15){
            if (resultCode == Activity.RESULT_OK){
                val tabs = requireActivity().findViewById<TabLayout>(R.id.tabs)
                tabs.selectTab(tabs.getTabAt(1))
            }
        }
    }

    private fun setInputListeners() {
        betweenMyCards.setOnClickListener {
            val intent = Intent(requireActivity(), CardToCardActivity::class.java)

            Log.i("CardID",  requireActivity().intent.getLongExtra("cardId", 0L).toString())
            intent.putExtra("cardId", requireActivity().intent.getLongExtra("cardId", 0L))
            startActivityForResult(intent, 13)
        }

        cashByCode.setOnClickListener {
            val intent = Intent(requireActivity(), CashByCodeActivity::class.java)

            intent.putExtra("cardId", requireActivity().intent.getLongExtra("cardId", 0L))
            startActivityForResult(intent, 15)
        }

        toAnotherBank.setOnClickListener {
            val intent = Intent(requireActivity(), CardToOtherCardActivity::class.java)
            intent.putExtra("cardId", requireActivity().intent.getLongExtra("cardId", 0L))
            startActivityForResult(intent, 14)
        }
    }
}
