package az.turanbank.mlb.presentation.ips.pay.debitorchoice.alias


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.AlertDialogMapper
import az.turanbank.mlb.data.remote.model.ips.request.EnumAliasType
import az.turanbank.mlb.di.ViewModelProviderFactory
import az.turanbank.mlb.presentation.view.MlbProgressDialog
import az.turanbank.mlb.presentation.base.BaseFragment
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.IPSCreditorAccountSelectActivity
import az.turanbank.mlb.presentation.ips.pay.creditorchoice.moreinfo.AccountAndCustomerInfoActivity
import az.turanbank.mlb.util.animateProgressImage
import az.turanbank.mlb.util.revertProgressImageAnimation
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class IPSAliasChoiceFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    lateinit var viewModel: IPSAliasChoiceViewModel
    lateinit var aliasContainer: LinearLayout
    private lateinit var progressImage: ImageView
    private lateinit var submitText: TextView

    lateinit var submitButton: LinearLayout

    lateinit var dialog: MlbProgressDialog

    lateinit var alias: AppCompatTextView

    private var description: String = ""

    private var ipsType: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_ipsalias_choice, container, false)

        viewModel =
            ViewModelProvider(this, factory)[IPSAliasChoiceViewModel::class.java]

        aliasContainer = view.findViewById(R.id.aliasContainer)
        submitButton = view.findViewById(R.id.submitButton)
        progressImage = view.findViewById(R.id.progressImage)
        submitText = view.findViewById(R.id.submitText)
        alias = view.findViewById(R.id.alias)

        setOutputListeners()
        setInputListeners()

        return view
    }

    private fun setOutputListeners() {

        viewModel.outputs.showProgress().subscribe {
            if (it) {
                dialog.showDialog()
            } else {
                dialog.hideDialog()
            }
        }.addTo(subscriptions)


        viewModel.outputs.aliasesSuccess().subscribe { aliasesList ->
            aliasContainer.setOnClickListener {
                val builder: android.app.AlertDialog.Builder =
                    android.app.AlertDialog.Builder(requireContext())
                builder.setTitle(getString(R.string.select_alias))

                val list = arrayOfNulls<Long>(aliasesList.size)
                val nameList = arrayOfNulls<String>(aliasesList.size)
                val typeList = arrayOfNulls<String>(aliasesList.size)

                for (i in 0 until aliasesList.size) {
                    list[i] = aliasesList[i].id
                    nameList[i] = aliasesList[i].ipsValue
                    typeList[i] = aliasesList[i].ipsType
                }
                builder.setItems(nameList) { _, which ->
                    alias.text = nameList[which]
                    viewModel.setSelectedAliasType(which)
                    viewModel.setSelectedAliasId(list[which])
                    ipsType = typeList[which]!!
                }
                val dialog = builder.create()
                dialog.show()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onAccountAndCustomerInfoByAlias().subscribe {
            description = getString(R.string.the_account_linked_the_alias)
            showProgressBar(false)
            val intent = Intent(requireActivity(), IPSCreditorAccountSelectActivity::class.java)
            intent.putExtra("senderAccountOrAlias", alias.text.toString())
            intent.putExtra("senderIBAN" , it.id.iban)
            intent.putExtra("senderFullName" , it.name!! + " " + it.surname!!)
            intent.putExtra("fromAccount",false)
            startActivityForResult(intent,7)

        }.addTo(subscriptions)

        viewModel.outputs.onAccountInfoByAliasNotFound().subscribe{
            if(it ==33311){
                showProgressBar(false)
                AlertDialogMapper(requireActivity(), it).showAlertDialog()
            }
        }.addTo(subscriptions)

        viewModel.outputs.onError().subscribe{
            showProgressBar(false)
            AlertDialogMapper(requireActivity(),it).showAlertDialog()
        }.addTo(subscriptions)
    }

    private fun setInputListeners() {
        viewModel.inputs.getAliases()
        submitButton.setOnClickListener {
            showProgressBar(true)
            if(alias.text.toString().trim().isEmpty()){
                showProgressBar(false)
            }

            var typeID = EnumAliasType.MOBILE
            when(ipsType){
                "MOBILE" -> typeID = EnumAliasType.MOBILE
                "EMAIL" -> typeID = EnumAliasType.EMAIL
                "TIN" -> typeID = EnumAliasType.TIN
                "PIN" -> typeID = EnumAliasType.PIN
            }

            viewModel.inputs.getAccountAndCustomerInfoByAlias(typeID, alias.text.toString())
        }
    }

    private fun showProgressBar(show: Boolean) {
        submitButton.isClickable = !show
        submitButton.isEnabled = !show
        submitButton.isFocusable = !show
        aliasContainer.isClickable = !show
        alias.isClickable = !show

        if (show) {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.light_gray))
            submitText.text = getString(R.string.pending_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.gray))
            progressImage.visibility = View.VISIBLE
            animateProgressImage(progressImage)
        } else {
            submitButton.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
            submitText.text = getString(R.string.proceed_all_caps)
            submitText.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            progressImage.visibility = View.GONE
            revertProgressImageAnimation(progressImage)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 7){
            if(resultCode == Activity.RESULT_OK){
                requireActivity().setResult(Activity.RESULT_OK)
                requireActivity().finish()
            }
        }
    }
}
