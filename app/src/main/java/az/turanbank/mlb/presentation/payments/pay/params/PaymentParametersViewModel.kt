package az.turanbank.mlb.presentation.payments.pay.params

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.pg.request.IdentificationCodeRequestModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.CheckMerchantResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.param.MerchantParamListResponseModel
import az.turanbank.mlb.domain.user.usecase.pg.check.CheckMerchantIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.param.GetMerchantParamListIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.param.GetMerchantParamListJuridicalUseCase
import az.turanbank.mlb.domain.user.usecase.pg.template.RemovePaymentTemplateIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.template.RemovePaymentTemplateJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.CompletableSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface PaymentParametersViewModelInputs : BaseViewModelInputs {
    fun getMerchantParams(merchantId: Long)
    fun check(
        merchantId: Long,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        categoryId: Long,
        providerId: Long
    )
    fun removeTemplate(tempId: Long)
}

interface PaymentParametersViewModelOutputs : BaseViewModelOutputs {
    fun onInputsReceived(): PublishSubject<ArrayList<MerchantParamListResponseModel>>
    fun onCheckCompleted(): PublishSubject<CheckMerchantResponseModel>
    fun onTemplateRemoved(): CompletableSubject
}

class PaymentParametersViewModel @Inject constructor(
    private val getMerchantParamListIndividualUseCase: GetMerchantParamListIndividualUseCase,
    private val getMerchantParamListJuridicalUseCase: GetMerchantParamListJuridicalUseCase,
    private val checkMerchantIndividualUseCase: CheckMerchantIndividualUseCase,
    private val removePaymentTemplateIndividualUseCase: RemovePaymentTemplateIndividualUseCase,
    private val removePaymentTemplateJuridicalUseCase: RemovePaymentTemplateJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(),
    PaymentParametersViewModelInputs,
    PaymentParametersViewModelOutputs {

    override val inputs: PaymentParametersViewModelInputs = this

    override val outputs: PaymentParametersViewModelOutputs = this

    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)
    private val lang = sharedPrefs.getString("lang", "az")
    var enumLangType = EnumLangType.AZ

    private val fields = PublishSubject.create<ArrayList<MerchantParamListResponseModel>>()
    private val check = PublishSubject.create<CheckMerchantResponseModel>()
    override fun getMerchantParams(merchantId: Long) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        val respond = if (isCustomerJuridical) {
            getMerchantParamListJuridicalUseCase
                .execute(custId, compId, token, merchantId, enumLangType)
        } else {
            getMerchantParamListIndividualUseCase
                .execute(custId, token, merchantId, enumLangType)
        }
        respond
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    fields.onNext(it.merchantParamList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }


    private val templateDeleted = CompletableSubject.create()

    override fun removeTemplate(tempId: Long) {
        val respond = if(isCustomerJuridical){
            removePaymentTemplateJuridicalUseCase
                .execute(custId, compId, token, tempId)
        } else {
            removePaymentTemplateIndividualUseCase
                .execute(custId, token, tempId)
        }
        respond
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    templateDeleted.onComplete()
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun check(
        merchantId: Long,
        identificationType: String,
        identificationCode: ArrayList<IdentificationCodeRequestModel>,
        categoryId: Long,
        providerId: Long
    ) {
        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "en" -> enumLangType = EnumLangType.EN
            "ru" -> enumLangType = EnumLangType.RU
        }
        checkMerchantIndividualUseCase
            .execute(
                custId,
                token,
                identificationType,
                identificationCode,
                merchantId,
                categoryId,
                enumLangType,
                providerId
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1) {
                    check.onNext(it)
                } else {
                    error.onNext(it.status.statusCode)
                }
            }, {
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onCheckCompleted() = check
    override fun onInputsReceived() = fields
    override fun onTemplateRemoved(): CompletableSubject = templateDeleted
}