package az.turanbank.mlb.presentation.payments.template

import android.content.SharedPreferences
import az.turanbank.mlb.data.remote.model.pg.response.payments.template.PaymentTemplateResponseModel
import az.turanbank.mlb.domain.user.usecase.pg.template.PaymentTemplateListIndividualUseCase
import az.turanbank.mlb.domain.user.usecase.pg.template.PaymentTemplateListJuridicalUseCase
import az.turanbank.mlb.presentation.base.BaseViewModel
import az.turanbank.mlb.presentation.base.BaseViewModelInputs
import az.turanbank.mlb.presentation.base.BaseViewModelOutputs
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import rx.subjects.PublishSubject
import javax.inject.Inject

interface PaymentTemplatesViewModelInputs : BaseViewModelInputs {
    fun getTemplates()
}

interface PaymentTemplatesViewModelOutputs : BaseViewModelOutputs {
    fun onTemplateSuccess(): PublishSubject<ArrayList<PaymentTemplateResponseModel>>
}

class PaymentTemplatesViewModel @Inject constructor(
    private val paymentTemplateListIndividualUseCase: PaymentTemplateListIndividualUseCase,
    private val paymentTemplateListJuridicalUseCase: PaymentTemplateListJuridicalUseCase,
    sharedPrefs: SharedPreferences
) : BaseViewModel(), PaymentTemplatesViewModelInputs, PaymentTemplatesViewModelOutputs {
    override val inputs: PaymentTemplatesViewModelInputs = this

    override val outputs: PaymentTemplatesViewModelOutputs = this


    private val token = sharedPrefs.getString("token", "")
    private val custId = sharedPrefs.getLong("custId", 0L)
    private val compId = sharedPrefs.getLong("compId", 0L)
    private val isCustomerJuridical = sharedPrefs.getBoolean("isCustomerJuridical", false)

    private val templateList = PublishSubject.create<ArrayList<PaymentTemplateResponseModel>>()

    val lang = sharedPrefs.getString("lang","az")
    var enumLangType = EnumLangType.AZ

    override fun getTemplates() {

        when(lang){
            "az" -> enumLangType = EnumLangType.AZ
            "ru" -> enumLangType = EnumLangType.RU
            "en" -> enumLangType = EnumLangType.EN
        }
        val respond = if (isCustomerJuridical){
            paymentTemplateListJuridicalUseCase
                .execute(custId, compId, token,enumLangType)
        } else {
            paymentTemplateListIndividualUseCase
                .execute(custId, token,enumLangType)
        }

        respond.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.status.statusCode == 1){
                    templateList.onNext(it.paymentTempList)
                } else {
                    error.onNext(it.status.statusCode)
                }
            },{
                error.onNext(1878)
            }).addTo(subscriptions)
    }

    override fun onTemplateSuccess() = templateList
}