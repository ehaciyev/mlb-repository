package az.turanbank.mlb.util

enum class EnumLangType {
    AZ, EN, RU
}