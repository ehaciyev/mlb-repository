package az.turanbank.mlb.util

import android.content.Context
import android.graphics.Typeface
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.util.Log.d
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.BitmapDescriptor
import java.io.IOException
import java.lang.Double.parseDouble
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.InetSocketAddress
import java.net.Socket
import java.net.URL
import java.util.logging.Logger


fun String.addPrefix(): String = "994$this"

fun splitString(text: String, seperator: Char): SpannableString {
    val textTrim = text.trim()
    val pos = textTrim.indexOf(seperator)
    val spannableString = SpannableString(textTrim)
    spannableString.setSpan(RelativeSizeSpan(1.2f), 0, pos, 0) // set size
    spannableString.setSpan(StyleSpan(Typeface.BOLD), 0, pos, 0)
    return spannableString
}

fun Double.formatDecimal() = decimalFormatter().format(this).toString()

fun decimalFormat(amount: Double, decimalFormat: DecimalFormat, decimalSeparator: Char): String {
    val symbols = DecimalFormatSymbols()
    decimalFormat.roundingMode = RoundingMode.HALF_EVEN
    symbols.decimalSeparator = decimalSeparator
    decimalFormat.decimalFormatSymbols = symbols
    return decimalFormat.format(amount)
}

fun feeAmountCalc(feeCalculationMethod: String, amount: Double,feePercent: Double,feeMinAmount: Double, feeMaxAmount: Double): Double{
    var feeAmount = 0.0
    if(feeCalculationMethod == "WITHOUT_FEE"){
        feeAmount = 0.0
    } else {
        feeAmount = (amount * feePercent) /100
        if(feeAmount <= feeMinAmount){
            feeAmount = feeMinAmount
        } else if(feeAmount >= feeMaxAmount){
            feeAmount = feeMaxAmount
        }
    }
    return decimalFormat(feeAmount,DecimalFormat("0.00"),'.').toDouble()
}

private fun decimalFormatter(): DecimalFormat {
    val decimalFormat = DecimalFormat()
    val symbols = DecimalFormatSymbols()
    decimalFormat.roundingMode = RoundingMode.HALF_EVEN
    symbols.decimalSeparator = '.'
    decimalFormat.decimalFormatSymbols = symbols
    return decimalFormat
}

fun animateProgressImage(progressImage: ImageView) {
    val rotate = RotateAnimation(
        0f,
        360f,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    rotate.duration = 2000
    rotate.interpolator = LinearInterpolator()
    rotate.repeatCount = Animation.INFINITE
    progressImage.startAnimation(rotate)
}

fun animateProgressImage(container: RecyclerView, progressImage: ImageView) {
    container.visibility = View.GONE
    val rotate = RotateAnimation(
        0f,
        360f,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    rotate.duration = 2000
    rotate.interpolator = LinearInterpolator()
    rotate.repeatCount = Animation.INFINITE
    progressImage.startAnimation(rotate)
}

fun revertProgressImageAnimation(progressImage: ImageView) {
    progressImage.clearAnimation()
}

fun firstPartOfMoney(amount: Double): String {
    val symbols = DecimalFormatSymbols()
    val decimalFormat = DecimalFormat("0.00")
    decimalFormat.roundingMode = RoundingMode.HALF_EVEN
    symbols.decimalSeparator = '.'
    decimalFormat.decimalFormatSymbols = symbols
    return decimalFormat.format(amount).toString().split(".")[0] + ","
}


fun secondPartOfMoney(amount: Double): String {
    val symbols = DecimalFormatSymbols()
    val decimalFormat = DecimalFormat("0.00")
    decimalFormat.roundingMode = RoundingMode.HALF_EVEN
    symbols.decimalSeparator = '.'
    decimalFormat.decimalFormatSymbols = symbols
    return decimalFormat.format(amount).toString().split(".")[1] + " "
}

fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
    val vectorDrawable: Drawable = ContextCompat.getDrawable(context, vectorResId)!!
    vectorDrawable.setBounds(
        0,
        0,
        vectorDrawable.intrinsicWidth / 2,
        vectorDrawable.intrinsicHeight / 2
    )
    val bitmap: Bitmap = Bitmap.createBitmap(
        vectorDrawable.intrinsicWidth,
        vectorDrawable.intrinsicHeight,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    vectorDrawable.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}

fun hasNetworkAvailable(context: Context): Boolean {
    val service = Context.CONNECTIVITY_SERVICE
    val manager = context.getSystemService(service) as ConnectivityManager
    val network = manager.activeNetworkInfo
    return network.isConnectedOrConnecting
}

fun isInternetAvailable(): Boolean{
    return try {
        val timeout = 1000
        val sock = Socket()
        val sockAddr = InetSocketAddress("www.google.com", 80)
        sock.connect(sockAddr, timeout)
        sock.close()
        true
    } catch (io: Exception){
        false
    }
}

