package az.turanbank.mlb

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import az.turanbank.mlb.presentation.base.BaseActivity
import java.util.*
import javax.inject.Inject

object RuntimeLocaleChanger{

   /* @Inject
    lateinit var prefs: SharedPreferences*/

    fun wrapContext(context: Context?): Context? {

        var prefs:SharedPreferences?=null
        prefs = context?.getSharedPreferences("turanbank.mlb", Context.MODE_PRIVATE)
        val savedLocale = Locale(prefs?.getString("lang", Resources.getSystem().configuration.locale.language)) // else return the original untouched context

        // as part of creating a new context that contains the new locale we also need to override the default locale.
        Locale.setDefault(savedLocale)

        // create new configuration with the saved locale
        val newConfig = Configuration()
        newConfig.setLocale(savedLocale)

        return context?.createConfigurationContext(newConfig)
    }
}

