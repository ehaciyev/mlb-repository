package az.turanbank.mlb.picasso

import android.content.Context
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient
import java.lang.RuntimeException
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.inject.Inject
import javax.net.ssl.*
import javax.security.cert.CertificateException


class PicassoTrustAll @Inject constructor(private val context: Context) {

    private fun provideOkHttpClientPicasso(
    ) : OkHttpClient{

        var trustAllCerts: Array<TrustManager> = emptyArray()

        try {

            trustAllCerts = arrayOf<TrustManager>(
                object : X509TrustManager {
                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(chain: Array<X509Certificate?>?, authType: String?) {}

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(chain: Array<X509Certificate?>?, authType: String?) {}

                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return arrayOf()
                    }
                }
            )

            val sslContext = SSLContext.getInstance("SSL")

            sslContext.init(null, trustAllCerts, SecureRandom())

            val sSLSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sSLSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier(HostnameVerifier { hostname, session -> true })

            return builder.build()

        } catch (e: Exception) {
            e.printStackTrace()
            throw RuntimeException(e)
        }

    }
    private var mInstance: Picasso = Picasso.Builder(context)
        .downloader(OkHttp3Downloader(provideOkHttpClientPicasso()))
        .build()

    fun getInstance(context: Context): Picasso {
        if (mInstance == null) {
            PicassoTrustAll(context)
        }
        return mInstance
    }
}