package az.turanbank.mlb.picasso

import android.util.Base64
import okhttp3.Interceptor
import okhttp3.Response

class PicassoInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        val auth = Base64.encodeToString(
            ("username" + ":" + "password").toByteArray(),
            Base64.NO_WRAP
        )
        response.request().newBuilder()
            .addHeader("Authorization", "Basic: $auth")
            .build()

        return response
    }
}
