package az.turanbank.mlb.data.remote.model.response

data class OrderPeriod (
    val id: Long,
    val name: String
)