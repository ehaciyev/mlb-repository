package az.turanbank.mlb.data.remote.model.pg.response.payments.category

data class CategoryResponseModel(
    val categoryId: Long,
    val categoryName: String,
    val displayName: String
)