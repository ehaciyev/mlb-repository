package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.ips.ConditionalDetailsModel
import az.turanbank.mlb.data.remote.model.ips.ConditionalIbanModel
import az.turanbank.mlb.data.remote.model.ips.ConditionalServicerModel
import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

class GetAccountAndCustomerInfoByAliasResponseModel (
    val id: ConditionalIbanModel,
    val type: String,
    val currency: String,
    val servicer: ConditionalServicerModel,
    val name: String,
    val surname: String,
    val nickName: String,
    val taxNumber: String,
    val details: ConditionalDetailsModel,
    val status: ServerStatusModel
)
