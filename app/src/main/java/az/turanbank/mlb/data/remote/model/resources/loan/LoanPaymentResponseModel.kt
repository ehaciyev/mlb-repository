package az.turanbank.mlb.data.remote.model.resources.loan

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel
import java.io.Serializable

data class LoanPaymentResponseModel(
    val dtIban: String,
    val contractNumber: String,
    val fullName: String,
    val trDate: String,
    val description: String,
    val merchantName: String,
    val amount: Double,
    val currency: String,
    val status: ServerStatusModel
): Serializable