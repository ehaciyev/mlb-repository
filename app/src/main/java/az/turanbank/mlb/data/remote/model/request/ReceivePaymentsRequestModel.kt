package az.turanbank.mlb.data.remote.model.request

data class ReceivePaymentsRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?
)