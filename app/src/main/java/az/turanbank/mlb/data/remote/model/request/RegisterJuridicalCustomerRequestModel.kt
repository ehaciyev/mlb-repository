package az.turanbank.mlb.data.remote.model.request

data class RegisterJuridicalCustomerRequestModel(
    val pin: String,
    val taxNo: String,
    val username: String,
    val password: String,
    val repeatPassword: String
)