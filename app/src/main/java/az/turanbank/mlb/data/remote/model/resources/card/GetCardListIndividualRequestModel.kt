package az.turanbank.mlb.data.remote.model.resources.card

import az.turanbank.mlb.util.EnumLangType

data class GetCardListIndividualRequestModel(
    val custId: Long,
    val token: String?,
    val lang: EnumLangType
)