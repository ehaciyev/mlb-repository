package az.turanbank.mlb.data.remote.model.ips.response

data class GetMLBLinkAccountsResponseModel(
    val accountList: ArrayList<GetIPSAccounts>,
    val status:  RespStatus
)