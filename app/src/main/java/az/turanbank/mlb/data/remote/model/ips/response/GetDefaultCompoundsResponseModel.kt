package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.ips.AliasAccountCompoundModel
import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetDefaultCompoundsResponseModel(
    val compounds: ArrayList<AliasAccountCompoundModel>,
    val status: ServerStatusModel
)