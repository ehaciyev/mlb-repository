package az.turanbank.mlb.data.remote.model.ips.response

data class DeleteAliaseResponseModel (
    val id: Long,
    val ipsType: String?,
    val ipsValue: String?,
    val startDate: String?,
    val expirationDate: String?,
    val status: String?,
    val ipsAliasId: Long?,
    val respStatus: RespStatus

)
