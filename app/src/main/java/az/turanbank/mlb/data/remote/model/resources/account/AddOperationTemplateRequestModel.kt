package az.turanbank.mlb.data.remote.model.resources.account

data class AddOperationTemplateRequestModel(
    val custId: Long,
    val compId: Long?,
    val tempName: String,
    val token: String?,
    val operId: Long
)