package az.turanbank.mlb.data.remote.model.request

data class SelectCustCompanyRequestModel(
    val pin: String,
    val taxNo: String,
    val mobile: String,
    val userId: String,
    val device: String,
    val location: String
)