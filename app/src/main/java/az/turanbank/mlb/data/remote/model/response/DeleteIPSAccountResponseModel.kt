package az.turanbank.mlb.data.remote.model.response

import java.io.Serializable

data class DeleteIPSAccountResponseModel(
    val accountList: ArrayList<DeleteIPSAccount>,
    val status: ServerStatusModel
): Serializable
