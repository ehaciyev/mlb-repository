package az.turanbank.mlb.data.remote.model.pg.response.payments.check

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel
import java.io.Serializable

data class CheckMerchantResponseModel(
    val transactionId: String?,
    val transactionNumber: String,
    val invoice: ArrayList<InvoiceResponseModel>,
    val avans: ArrayList<AvansResponseModel>,
    val status: ServerStatusModel
): Serializable