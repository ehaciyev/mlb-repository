package az.turanbank.mlb.data.remote.model.response

data class GetAllOperationListResponseModel(
    val operationList: ArrayList<OperationModel>,
    val status: ServerStatusModel
)