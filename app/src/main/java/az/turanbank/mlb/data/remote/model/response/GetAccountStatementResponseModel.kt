package az.turanbank.mlb.data.remote.model.response

import az.turanbank.mlb.data.remote.model.resources.account.AccountStatement

data class GetAccountStatementResponseModel (
        val accountStatementList: ArrayList<AccountStatement>,
        val status: ServerStatusModel
)