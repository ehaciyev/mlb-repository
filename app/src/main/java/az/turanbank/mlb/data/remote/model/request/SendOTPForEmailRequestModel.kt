package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class SendOTPForEmailRequestModel(
    val custId: Long,
    val compId: Long?,
    val email: String,
    val token: String?,
    val lang: EnumLangType
)