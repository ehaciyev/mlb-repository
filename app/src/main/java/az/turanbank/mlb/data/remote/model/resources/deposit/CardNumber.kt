package az.turanbank.mlb.data.remote.model.resources.deposit

data class CardNumber(
    val cardNumber: String
)