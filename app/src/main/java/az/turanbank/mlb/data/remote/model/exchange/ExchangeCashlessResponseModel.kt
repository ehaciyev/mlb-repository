package az.turanbank.mlb.data.remote.model.exchange

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class ExchangeCashlessResponseModel(
    val exchangeCashlessList: ArrayList<CashlessResponseModel>,
    val status: ServerStatusModel
)