package az.turanbank.mlb.data.remote.model.cardoperations

import az.turanbank.mlb.util.EnumLangType

data class CardToOtherCardRequestModel(
    val custId: Long,
    val requestorCardId: Long,
    val destinationCardNumber: String,
    val currency: String,
    val amount: Double?,
    val token: String?,
    val lang: EnumLangType
)