package az.turanbank.mlb.data.local

import android.app.Activity
import android.app.AlertDialog
import az.turanbank.mlb.R
import az.turanbank.mlb.data.local.ErrorMessageMapper.Companion.errorMap

open class AlertDialogMapper(private val activity: Activity, private val errorResources: Int) {
    open fun showAlertDialog() {
        var errorResources = errorResources
        if (errorMap[this.errorResources] === null) {
            errorResources = R.string.default_error_message
        }
        AlertDialog.Builder(activity)
            .setTitle(R.string.error_occured)
            .setMessage(activity.getString(ErrorMessageMapper().getErrorMessage(errorResources)))
            .setPositiveButton(R.string.ok) { _, _ -> }
            .setCancelable(true)
            .show()
    }
    open fun showSuccessAlertDialog() {
        var errorResources = errorResources
        if (errorMap[this.errorResources] === null) {
            errorResources = R.string.operation_done
        }
        AlertDialog.Builder(activity)
            .setTitle(R.string.successful_operation)
            .setMessage(activity.getString(errorMap[errorResources]!!))
            .setPositiveButton(R.string.ok) { _, _ -> }
            .setCancelable(true)
            .show()
    }

    open fun showAlertDialogWithCloseOption() {
        AlertDialog.Builder(activity)
            .setTitle(R.string.error_occured)
            .setMessage(activity.getString(ErrorMessageMapper().getErrorMessage(errorResources)))
            .setPositiveButton(R.string.ok) { _, _ ->
                activity.finish()
            }
            .setCancelable(false)
            .show()
    }

    open fun setAccountLinkAliasWithShowDialog(account: String){
        var errorResources = errorResources
        if (errorMap[this.errorResources] === null) {
            errorResources = R.string.default_error_message
        }
        AlertDialog.Builder(activity)
            .setTitle(R.string.error_occured)
            .setMessage(activity.getString(ErrorMessageMapper().getErrorMessage(errorResources), account))
            .setPositiveButton(R.string.ok) { _, _ -> }
            .setCancelable(true)
            .show()
    }
}