package az.turanbank.mlb.data.remote.model.pg.request

import az.turanbank.mlb.util.EnumLangType

data class CheckMerchantRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val identificationType: String,
    val identificationCode: ArrayList<IdentificationCodeRequestModel>,
    val merchantId: Long,
    val categoryId: Long,
    val lang: EnumLangType,
    val providerId: Long
)