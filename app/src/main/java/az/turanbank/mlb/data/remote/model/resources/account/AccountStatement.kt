package az.turanbank.mlb.data.remote.model.resources.account

data class AccountStatement(
        val dtAmount: Double,
        val crAmount: Double,
        val dtAznAmt: Double,
        val crAznAmt: Double,
        val dtAccount: String,
        val crAccount: String,
        val currName: String,
        val purpose: String,
        val trDate: String
)