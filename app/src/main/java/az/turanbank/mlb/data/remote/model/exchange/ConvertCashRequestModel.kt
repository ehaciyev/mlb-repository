package az.turanbank.mlb.data.remote.model.exchange

data class ConvertCashRequestModel(
    val fromCurrency: String,
    val toCurrency: String,
    val currencyAmount: Double
)