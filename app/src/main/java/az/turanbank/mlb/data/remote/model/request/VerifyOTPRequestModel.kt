package az.turanbank.mlb.data.remote.model.request

data class VerifyOTPRequestModel(
    val custId: Long,
    val compId: Long?,
    val confirmCode: String,
    val broadcastType: Int
)
