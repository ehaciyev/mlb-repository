package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.data.remote.Constants
import az.turanbank.mlb.data.remote.model.provider.ServiceProvider
import az.turanbank.mlb.data.remote.model.request.*
import az.turanbank.mlb.data.remote.model.response.*
import io.reactivex.Single
import io.reactivex.Observable
import retrofit2.http.*

interface AuthApiService {
    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getPinFromAsan")
    fun getPinFromAsan(
        @Body request: GetPinFromAsanRequestModel
    ): Single<GetPinFromAsanResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("checkPinForAsanRegister")
    fun checkPinForAsanRegister(
        @Query("pin") pin: String
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("checkPinForAsanJuridicalRegister")
    fun checkPinForAsanJuridicalRegister(
        @Query("pin") pin: String
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getAuthInfo")
    fun getAuthInfo(
        @Body request: GetAuthInfoRequestModel
    ): Single<GetAuthInfoResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("registerWithAsan")
    fun registerWithAsan(
        @Body request: RegisterWithAsanRequestModel
    ): Single<RegisterWithASANResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("checkIndividualCustomer")
    fun checkIndividualCustomer(
        @Body checkIndividualCustomerRequestModel: CheckIndividualCustomerRequestModel
    ): Observable<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("sendOtpCode")
    fun sendOTPCode(
        @Body sendOTPRequestModel: SendOTPRegisterRequestModel
    ): Observable<SendOTPResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("verifyOtpCode")
    fun verifyOTPCode(
        @Body verifyOTPRequestModel: VerifyOTPRequestModel
    ): Observable<VerifyOTPResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("registerIndividualMobile")
    fun registerWithMobile(
        @Body registerIndividualASANRequestModel: RegisterIndividualAsanRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("registerIndividualAsan")
    fun registerIndividualAsan(
        @Body request: RegisterIndividualAsanRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loginWithUsrAndPwdIndividual")
    fun loginUsernamePasswordIndividual(
        @Body loginWithUsernameAndPasswordIndividualRequestModel: LoginWithUsernameAndPasswordIndividualRequestModel
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loginWithUsrAndPwdJuridical")
    fun loginUsernamePasswordJuridical(
        @Body loginWithUsernameAndPasswordJuridicalRequestModel: LoginWithUsernameAndPasswordIndividualRequestModel
    ): Single<MlbResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("checkJuridicalCustomer")
    fun checkJuridicalCustomer(
        @Body checkJuridicalCustomerRequestModel: CheckForgotPassJuridicalRequestModel
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("registerJuridicalCustomer")
    fun registerJuridicalCustomer(
        @Body registerJuridicalCustomerRequestModel: RegisterJuridicalCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loginWithAsan")
    fun loginWithAsan(
        @Body request: LoginWithASANRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getCompaniesCert")
    fun getCompaniesCert(
        @Body request: GetCompaniesCertRequestModel
    ): Single<GetCompaniesCertResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("selectCustCompany")
    fun selectCustCompany(
        @Body request: SelectCustCompanyRequestModel
    ): Single<SelectCustCompanyResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getCustCompany")
    fun getCustCompany(
        @Body request: GetCustCompanyRequestModel
    ): Single<GetCustCompanyResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("registerJuridicalAsan")
    fun registerJuridicalAsan(
        @Body request: RegisterJuridicalAsanRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("checkPinForAsanLogin")
    fun checkPinForAsanLogin(
        @Query("pin") pin: String
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getToken")
    fun getToken(
        @Body getTokenRequestModel: GetTokenRequestModel
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("checkPinForAsanJuridicalLogin")
    fun checkPinForAsanJuridicalLogin(
        @Query("pin") pin: String
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("checkForgotPassJuridical")
    fun checkForgotPassJuridical(
        @Body checkJuridicalCustomerRequestModel: CheckForgotPassJuridicalRequestModel
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("checkForgotPassIndividual")
    fun checkForgotPassIndividual(
        @Body checkIndividualCustomerRequestModel: CheckIndividualCustomerRequestModel
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("changePasswordIndividual")
    fun changePasswordIndividual(
        @Body changePasswordRequestModel: ChangePasswordRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("changePasswordJuridical")
    fun changePasswordJuridical(
        @Body changePasswordRequestModel: ChangePasswordRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("checkUnblockJuridical")
    fun checkUnblockJuridical(
        @Body checkForgotPassJuridicalRequestModel: CheckForgotPassJuridicalRequestModel
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loginIndividualForAsan")
    fun loginIndividualForAsan(
        @Body loginIndividualForAsanRequestModel: LoginIndividualForAsanRequestModel
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("checkUnblockIndividual")
    fun checkUnblockIndividual(
        @Body checkIndividualCustomerRequestModel: CheckIndividualCustomerRequestModel
    ): Single<CustomerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("unblockIndividualCustomer")
    fun unblockIndividualCustomer(
        @Body unblockIndividualCustomerCustomerRequestMode: UnblockIndividualCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("unblockJuridicalCustomer")
    fun unblockJuridicalCustomer(
        @Body unblockIndividualCustomerRequestMode: UnblockJuridicalCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("blockIndividualCustomer")
    fun blockIndividualCustomer(
        @Body unblockIndividualCustomerCustomerRequestMode: UnblockIndividualCustomerRequestModel
    ): Single<ServerResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("blockJuridicalCustomer")
    fun blockJuridicalCustomer(
        @Body unblockIndividualCustomerRequestMode: UnblockJuridicalCustomerRequestModel
    ): Single<ServerResponseModel>
}

interface AuthApiServiceProvider :
    ServiceProvider<AuthApiService>