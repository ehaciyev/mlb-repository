package az.turanbank.mlb.data.remote.model.response

data class GetProfileImageResponseModel(
    val id: Long,
    val fileName: String?,
    val bytes: String,
    val status: ServerStatusModel
)