package az.turanbank.mlb.data.remote.model.resources.card

data class SaveCashByCodeTempRequestModel(
    val custId: Long,
    val tempName: String,
    val requestorCardNumber: String,
    val destinationPhoneNumber: String,
    val currency: String,
    val amount: Double,
    val token: String?
)