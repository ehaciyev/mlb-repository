package az.turanbank.mlb.data.remote.model.request

data class LoginWithUsernameAndPasswordIndividualRequestModel(
    val username: String,
    val password: String
)