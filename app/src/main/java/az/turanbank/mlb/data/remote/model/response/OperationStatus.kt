package az.turanbank.mlb.data.remote.model.response

data class OperationStatus(
    val operId: Long,
    val statusCode: Int,
    val statusMessage: String
)