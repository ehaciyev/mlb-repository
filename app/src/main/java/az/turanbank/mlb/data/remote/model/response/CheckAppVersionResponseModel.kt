package az.turanbank.mlb.data.remote.model.response

data class CheckAppVersionResponseModel(
    val status: ServerStatusModel
)