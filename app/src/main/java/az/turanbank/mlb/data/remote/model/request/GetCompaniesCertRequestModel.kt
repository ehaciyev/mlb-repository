package az.turanbank.mlb.data.remote.model.request

data class GetCompaniesCertRequestModel(
    val pin: String,
    val mobile: String,
    val userId: String,
    val custCompStatus: Int = 2
)
