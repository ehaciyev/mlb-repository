package az.turanbank.mlb.data.remote.model.request

data class GetAccountRequisitesForIndividualCustomerRequestModel (
    val custId: Long,
    val accountId: Long,
    val token: String?
)