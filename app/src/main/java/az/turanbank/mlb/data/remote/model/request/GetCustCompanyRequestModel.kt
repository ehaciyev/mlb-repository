package az.turanbank.mlb.data.remote.model.request

data class GetCustCompanyRequestModel(
    val pin: String,
    val taxNo: String,
    val mobile: String,
    val userId: String,
    val custCompStatus: Int = 2
)