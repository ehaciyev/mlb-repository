package az.turanbank.mlb.data.local

import az.turanbank.mlb.R

open class PlasticCardMapper {
    open fun resolveCard(cardType: String): Int{
        when (cardType) {
            "PP MC Standard-USD",
            "PP MC Standard-EUR",
            "PP MC Standard Salary-AZN",
            "PP MC Standard Salary-USD",
            "PP MC Standard Salary-EUR",
            "PP MC St Corporative-AZN",
            "PP MC St Corporative-USD",
            "PP MC St Corporative-EUR",
            "MC Standard AG-EUR",
            "MC Standard AG-USD",
            "MC Standard AG-AZN" -> {
                return R.drawable.card
            }
            "PP MC GOLD-AZN",
            "PP MC GOLD-USD",
            "PP MC GOLD-EUR",
            "PP MC GOLD Salary-EUR",
            "PP MC GOLD Salary-AZN",
            "PP MC GOLD Salary-USD",
            "MCHIP GOLD NEW-AZN",
            "MCHIP GOLD NEW-USD",
            "MCHIP GOLD NEW-EUR",
            "MC GOLD  Salary-EUR",
            "MC GOLD  Salary-AZN",
            "MC GOLD  Salary-USD" -> {
                return R.drawable.group_7
            }
            "MC Debit Salary-AZN",
            "MC Debit Salary-USD",
            "MC Debit Salary-EUR",
            "MC Debit TAX-AZN",
            "MC Debit-AZN",
            "MC Debit-USD",
            "MC Debit-EUR" -> {
                return R.drawable.group_7_copy_3
            }
            "MC Black Edition-AZN",
            "MC Black Edition-USD",
            "MC Black Edition-EUR" -> {
                return R.drawable.group_7_copy_2
            }
        }
        return R.drawable.card
    }
}