package az.turanbank.mlb.data.remote.model.pg.response.payments.check

import java.io.Serializable

data class PaymentReceiverResponseModel(
    val code: Long,
    val name: String
): Serializable