package az.turanbank.mlb.data.remote.model.request

data class PasswordChangeIndividualRequestModel(
    val custId: Long,
    val token: String?,
    val oldPassword: String?,
    val password: String?,
    val repeatPassword: String?
)