package az.turanbank.mlb.data.remote.model.ips

data class ConditionalDetailsModel(
    val authMethod: String
)