package az.turanbank.mlb.data.remote.model.resources.loan

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class LoanPayedAmountsResponseModel(
    val loanPayments: ArrayList<LoanPayedAmountModel>,
    val status: ServerStatusModel
)