package az.turanbank.mlb.data.remote.model.pg.request

import az.turanbank.mlb.util.EnumLangType

data class CreatePaymentTemplateRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val tempName: String,
    val merchantId: Long,
    val identificationType: String,
    val identificationCode: ArrayList<IdentificationCodeRequestModel>,
    val amount: Double,
    val currency: String,
    val providerId: Long,
    val categoryId: Long,
    val lang: EnumLangType
)