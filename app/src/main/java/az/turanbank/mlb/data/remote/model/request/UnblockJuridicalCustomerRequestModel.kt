package az.turanbank.mlb.data.remote.model.request

data class UnblockJuridicalCustomerRequestModel(
    val custId: Long,
    val compId: Long
)