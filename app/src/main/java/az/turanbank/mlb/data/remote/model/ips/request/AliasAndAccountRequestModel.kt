package az.turanbank.mlb.data.remote.model.ips.request

data class AliasAndAccountRequestModel(
    val aliasId: Long,
    val accountId: Long,
    val isDefault: Boolean
)