package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetSecondSignInfoRequestModel(
    val operationIds: ArrayList<Long?>,
    val custId: Long,
    val compId: Long,
    val token: String?,
    val phoneNumber: String?,
    val userId: String?,
    val lang: EnumLangType,
    val certCode: String?
)