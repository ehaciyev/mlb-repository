package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetJuridicalCardListRequestModel (
        val custId: Long,
        val compId: Long?,
        val token: String?,
        val lang: EnumLangType
)