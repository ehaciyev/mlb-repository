package az.turanbank.mlb.data.remote.model.response

data class GetCustInfoByIbanResponseModel(
    val custName: String,
    val taxNumber: String?,
    val status: ServerStatusModel
)