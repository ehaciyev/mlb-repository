package az.turanbank.mlb.data.remote.model.cardoperations

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class CardTemplateResponseModel(
    val tempId: Long,
    val tempName: String,
    val requestorCardNumber: String,
    val destinationCardNumber: String,
    val amount: Double,
    val currency: String,
    val cardOperationType: Int,
    val cardOperationTypeValue: String,
    val createdDate: String,
    val status: ServerStatusModel
)