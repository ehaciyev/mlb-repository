package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.ips.ConditionalDetailsModel
import az.turanbank.mlb.data.remote.model.ips.ConditionalIbanModel
import az.turanbank.mlb.data.remote.model.ips.ConditionalServicerModel
import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetAccountAndCustomerInfoResponseModel (
    val type: String,
    val currency: String,
    val name: String,
    val surname: String,
    val nickName: String,
    val taxNumber: String,
    val status: ServerStatusModel,
    val id: ConditionalIbanModel,
    val servicer: ConditionalServicerModel,
    val details: ConditionalDetailsModel
)
