package az.turanbank.mlb.data.remote.model.resources.account.exchange

import az.turanbank.mlb.util.EnumLangType

data class CreateExchangeOperationRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val dtAccountId: Long,
    val dtAmount: Double,
    val crAmount: Double,
    val crIban: String,
    val dtBranchId: Long,
    val crBranchId: Long,
    val exchangeRate: Double,
    val exchangeOperationType: Int,
    val lang: EnumLangType,
    val sourceId: Int = 2
)