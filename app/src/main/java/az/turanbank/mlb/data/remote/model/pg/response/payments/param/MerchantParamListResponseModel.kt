package az.turanbank.mlb.data.remote.model.pg.response.payments.param

data class MerchantParamListResponseModel(
    val paramId: Long,
    val paramName: String,
    val paramDisplayName: String,
    val paramType: String,
    val format: String,
    val maxLen: Int,
    val merchantDescName: String,
    val merchantDescId: Long,
    val merchantDescValue: String,
    val providerId: Long,
    val merchantDisplayName: String,
    val paramValue: String,
    val paramDesc: String
)