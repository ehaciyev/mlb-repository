package az.turanbank.mlb.data.remote.model.response

data class BudgetCode (
    val budgetId: String,
    val budgetValue: String
)