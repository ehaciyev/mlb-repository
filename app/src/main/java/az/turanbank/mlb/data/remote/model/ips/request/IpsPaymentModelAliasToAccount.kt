package az.turanbank.mlb.data.remote.model.ips.request

data class IpsPaymentModelAliasToAccount (
    val paymentAmount: Long,
    val receiverCustomerName: String,
    val receiverCustomerAccount: String,
    val additionalInfoAboutPayment: String,
    val additionalInfoFromCustomer: String,
    val remittanceInfo: String,
    val receiverTaxid: String,
    val budgetLvl: String,
    val budgetCode: String
)