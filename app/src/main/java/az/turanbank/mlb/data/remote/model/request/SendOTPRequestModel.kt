package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class SendOTPRequestModel(
    val custId: Long,
    val compId: Long?,
    val mobile: String,
    val token: String?,
    val lang: EnumLangType
)