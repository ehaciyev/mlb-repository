package az.turanbank.mlb.data.remote.model.request

data class GetAccountListForIndividualCustomerRequestModel(
    val custId: Long,
    val token: String
)
