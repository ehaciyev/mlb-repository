package az.turanbank.mlb.data.remote.model.pg.response.payments.check

import java.io.Serializable

data class InvoiceResponseModel(
    val invoiceNumber: String,
    val invoiceCode: String,
    val invoiceDate: String, //pattern = "dd.MM.yyyy HH:mm"
    val fullName: String,
    val amount: Double,
    val paymentReceiver: Long,
    val paymentReceiverDescription: String,
    val paymentReceiverCodeName: ArrayList<PaymentReceiverResponseModel>,
    val partialPayment: Int,
    val minAllowed: Double,
    val maxAllowed: Double,
    val serviceName: String,
    val payerCode: String,
    val transactionNumber: String,
    val feeCalculationMethod: String,
    val feePercent: Double,
    val feeMinAmount: Double,
    val feeMaxAmount: Double,
    val serviceCode: Long,
    val respChildInvoiceList: ArrayList<ChildInvoiceResponseModel>
): Serializable