package az.turanbank.mlb.data.remote.model.response

data class GetCompaniesCertResponseModel(
    val company: List<CompanyModel>,
    val status: ServerStatusModel
)