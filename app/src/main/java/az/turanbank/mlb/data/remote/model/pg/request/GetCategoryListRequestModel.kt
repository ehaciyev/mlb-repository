package az.turanbank.mlb.data.remote.model.pg.request

import az.turanbank.mlb.util.EnumLangType

data class GetCategoryListRequestModel(
    val token: String?,
    val compId: Long?,
    val custId: Long,
    val lang: EnumLangType
)