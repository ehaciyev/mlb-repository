package az.turanbank.mlb.data.remote.model.ips.request

import az.turanbank.mlb.util.EnumLangType

data class CreateAccountToAccountIpsOperationRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val lang: EnumLangType,
    val receiverBankId: Long,
    val ipsPaymentTypeId:Int,
    val branchId: Int,
    val branchName: String,
    val operNameId: Long = 5L,
    val operTypeId: Long = 7L,
    val sourceId: Int = 2,
    val ipsPayment: IpsPaymentModelAccountToAccount
)