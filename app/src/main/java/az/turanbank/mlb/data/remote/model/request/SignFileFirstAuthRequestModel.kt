package az.turanbank.mlb.data.remote.model.request

data class SignFileFirstAuthRequestModel(
    val custId: Long,
    val token: String?,
    val compId: Long,
    val transactionId: Long,
    val userId: String?,
    val phoneNumber: String?,
    val certCode: String?,
    val batchId: Long,
    val successOperId: ArrayList<Long>,
    val failOperId: ArrayList<Long>
)