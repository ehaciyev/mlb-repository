package az.turanbank.mlb.data.remote.model.resources.loan

data class LoanListModel(
        val id: Long,
        val loanType: String,
        val loanAmount: Double,
        val currency: String,
        val branch: String
)