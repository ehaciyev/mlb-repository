package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetOrderKindListRequestModel(
    val custId: Long,
    val token: String?,
    val orderType: Int?,
    val kindType: Long?,
    val lang: EnumLangType
)