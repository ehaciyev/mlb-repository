package az.turanbank.mlb.data.remote.model.ips.response

import java.io.Serializable

data class RespStatus (
    val statusCode: Int,
    val statusMessage: String
): Serializable
