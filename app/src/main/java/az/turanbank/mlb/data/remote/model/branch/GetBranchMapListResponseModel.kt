package az.turanbank.mlb.data.remote.model.branch

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetBranchMapListResponseModel(
    val respBranchMapList: ArrayList<BranchMapResponseModel>,
    val status: ServerStatusModel
)