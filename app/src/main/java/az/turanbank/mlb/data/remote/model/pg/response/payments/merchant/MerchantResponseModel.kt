package az.turanbank.mlb.data.remote.model.pg.response.payments.merchant

data class MerchantResponseModel(
    val merchantId: Long,
    val merchantName: String,
    val displayName: String,
    val img: String,
    val providerMerchantId: Long
)