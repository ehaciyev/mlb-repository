package az.turanbank.mlb.data.remote.model.response

data class UpdateProfileImageResponseModel(
    val id: Long,
    val fileName: String?,
    val bytes: String,
    val status: ServerStatusModel
)