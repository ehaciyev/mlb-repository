package az.turanbank.mlb.data.remote.model.request

data class GetCardStatementForIndividualCustomerRequestModel (
        val custId: Long,
        val token: String?,
        val cardNumber: String?,
        val startDate: String,
        val endDate: String
)