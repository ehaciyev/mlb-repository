package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetSignInfoRequestModel(
    val operationIds: ArrayList<Long?>,
    val custId: Long,
    val token: String?,
    val phoneNumber: String?,
    val userId: String?,
    val lang: EnumLangType,
    val certCode: String?
)