package az.turanbank.mlb.data.remote.model.provider

interface ServiceProvider<SERVICE_TYPE> :
    DependencyProvider<SERVICE_TYPE>