package az.turanbank.mlb.data.remote.model.ips.request

data class CreateAliasRequestModel(
    val custId: Long,
    val token: String?,
    val aliases: ArrayList<AliasRequestModel>
)