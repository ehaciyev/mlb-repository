package az.turanbank.mlb.data.remote

import az.turanbank.mlb.BuildConfig

class Constants {
    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
        const val CHARSET = "Charset: UTF-8"
        const val CONTENT_TYPE_JSON = "content-type: application/json"
        const val ACCEPT = "Accept: application/json"
        //const val BASIC_AUTH = "authorization: Basic YWRtaW46RWdtWU1rU0VSQmU3NGpEZg=="
        const val BASIC_AUTH = BuildConfig.BASIC_AUTH
        const val BASE_URL = BuildConfig.MLB_URL
        const val PEM_FILE = BuildConfig.PEM_FILE_NAME
    }
}