package az.turanbank.mlb.data.remote.model.response

data class CampaignResponseModel(
    val respCampaignList: ArrayList<CampaignModel>,
    val status: ServerStatusModel
)