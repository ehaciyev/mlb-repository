package az.turanbank.mlb.data.remote.model.response

data class DeleteIPSAccount (
    val accountId: Long,
    val accountName: String?,
    val iban: String,
    val openDate: String?,
    val currName: String?,
    val branchId: String?,
    val branchName: String?,
    val currentBalance: String?,
    val status: ServerStatusModel
)
