package az.turanbank.mlb.data.remote.model.response

data class AccountModel(
    val accountId: Long,
    val accountName: String,
    val iban: String,
    val openDate: String,
    val currName: String,
    val branchName: String,
    val currentBalance: Double,
    val statusServerStatusModel: ServerStatusModel
)