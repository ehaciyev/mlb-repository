package az.turanbank.mlb.data.remote.model.pg.response.payments.list

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetPaymentListResponseModel(
    val paymentList: ArrayList<PaymentResponseModel>,
    val status: ServerStatusModel
)