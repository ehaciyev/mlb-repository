package az.turanbank.mlb.data.remote.model.resources.loan

data class LoanPayedAmountModel(
    val payDate: String,
    val payAmount: Double,
    val payRest: Double
)