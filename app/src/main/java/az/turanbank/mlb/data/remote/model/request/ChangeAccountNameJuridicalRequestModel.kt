package az.turanbank.mlb.data.remote.model.request

data class ChangeAccountNameJuridicalRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val accountId: Long,
    val accountName: String
)