package az.turanbank.mlb.data.remote.model.ips.request

import az.turanbank.mlb.util.EnumLangType

data class GetIpsAuthMethodRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val lang: EnumLangType
)