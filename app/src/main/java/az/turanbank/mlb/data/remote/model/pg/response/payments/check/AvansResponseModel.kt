package az.turanbank.mlb.data.remote.model.pg.response.payments.check

import java.io.Serializable

data class AvansResponseModel(
    val amount: Double,
    val partialPayment: Int,
    val minAllowed: Double,
    val maxAllowed: Double,
    val serviceCode: Long,
    val paymentReceiverCodeName: ArrayList<PaymentReceiverResponseModel>,
    val fullName: String,
    val paymentReceiver: Long?,
    val paymentReceiverDescription: String?,
    val serviceName: String,
    val transactionNumber: String,
    val feeCalculationMethod: String,
    val feePercent: Double,
    val feeMinAmount: Double,
    val feeMaxAmount: Double
) : Serializable