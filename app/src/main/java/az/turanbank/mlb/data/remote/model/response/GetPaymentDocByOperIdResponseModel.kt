package az.turanbank.mlb.data.remote.model.response

data class GetPaymentDocByOperIdResponseModel(
    val status: ServerStatusModel,
    val bytes: String
)