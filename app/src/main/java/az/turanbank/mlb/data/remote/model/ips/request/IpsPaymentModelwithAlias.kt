package az.turanbank.mlb.data.remote.model.ips.request

data class IpsPaymentModelWithAlias (
    val paymentAmount: Long,
    val receiverCustomerName: String,
    val additionalInfoAboutPayment: String,
    val additionalInfoFromCustomer: String,
    val remittanceInfo: String
)