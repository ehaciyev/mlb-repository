package az.turanbank.mlb.data.remote.model.resources.loan

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class LoanPaymentPlanResponseModel(
    val loanPaymentPlan: ArrayList<LoanPaymentPlanModel>,
    val status: ServerStatusModel
)