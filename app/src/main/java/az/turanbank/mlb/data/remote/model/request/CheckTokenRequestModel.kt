package az.turanbank.mlb.data.remote.model.request

data class CheckTokenRequestModel(
    val token: String?,
    val custId: Long,
    val compId: Long?
)