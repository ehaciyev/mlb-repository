package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetAccountByIdRequestModel2(
    val compId: Long?,
    val custId: Long,
    val token: String?,
    val accountId: Long,
    val lang: EnumLangType
)