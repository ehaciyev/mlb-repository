package az.turanbank.mlb.data.remote.model.response

data class GetCustCompanyResponseModel(
    val customer: CustomerResponseModel,
    val company: CompanyModel,
    val status: ServerStatusModel
)