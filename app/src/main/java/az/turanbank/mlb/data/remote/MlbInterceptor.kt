package az.turanbank.mlb.data.remote

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import az.turanbank.mlb.R
import az.turanbank.mlb.data.remote.model.response.ServerResponseModel
import az.turanbank.mlb.presentation.NoConnectionException
import az.turanbank.mlb.presentation.NoInternetException
import az.turanbank.mlb.presentation.activity.ErrorPageViewActivity
import az.turanbank.mlb.presentation.activity.WarningPageViewActivity
import az.turanbank.mlb.presentation.base.BaseActivity
import az.turanbank.mlb.presentation.base.BaseApplication
import az.turanbank.mlb.util.hasNetworkAvailable
import az.turanbank.mlb.util.isInternetAvailable
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import javax.inject.Inject

class MlbInterceptor @Inject constructor(
    val context: Context,
    val activity: BaseActivity,
    val sharedPrefs: SharedPreferences
): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        /*if(!hasNetworkAvailable((context as BaseApplication).baseContext)){
            val intent = Intent(context, ErrorPageViewActivity::class.java)
            intent.putExtra("description", context.getString(R.string.error_in_system))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.putExtra("tokenExpired", true)
            context.startActivity(intent)
            activity.finish()
            throw NoConnectionException()
        } else if(!isInternetAvailable()){
                    }*/

        val request = chain.request()
        val response = chain.proceed(request)

        val fullResponseBody = response.body()?.string()

        if(response.code() == 200) {
            val serverResponseModel =
                Gson().fromJson<ServerResponseModel>(fullResponseBody, ServerResponseModel::class.java)

            if(serverResponseModel.status.statusCode == 200) {
                // sharedPrefs.edit().putBoolean("userLoggedIn", false).apply()
                //    Toast.makeText(context, "Sessiyanız başa çatmışdır. Zəhmət olmasa, yenidən daxil olun.", Toast.LENGTH_LONG).show()
                val intent = Intent(context, WarningPageViewActivity::class.java)
                intent.putExtra("description", context.getString(R.string.session_timeout))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                intent.putExtra("tokenExpired", true)
                context.startActivity(intent)
                activity.finish()

            } else if(serverResponseModel.status.statusCode == 100) {
                //   sharedPrefs.edit().putBoolean("userLoggedIn", false).apply()
                val intent = Intent(context, ErrorPageViewActivity::class.java)
                intent.putExtra("description", context.getString(R.string.error_in_system))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                intent.putExtra("tokenExpired", true)
                context.startActivity(intent)
                activity.finish()
            }
        }
        return response.newBuilder().body(ResponseBody.create(response.body()?.contentType(), fullResponseBody!!)).build()
    }
}