package az.turanbank.mlb.data.remote.model.resources.deposit

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class DepositByIdResponseModel(
    val id: Long,
    val holder: String,
    val custId: Long,
    val branchName: String,
    val amount: Double,
    val yearPercent: Int,
    val currency: String,
    val paymentFrequency: String,
    val openDate: String,
    val endDate: String,
    val iban: String,
    val name: String,
    val status: ServerStatusModel
)