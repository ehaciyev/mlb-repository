package az.turanbank.mlb.data.remote.model.response

import java.io.Serializable

data class DomesticOperationResponseModel (
    val operationId: Long,
    val operationNo: String,
    val dtIban: String,
    val operationName: String,
    val operationStatus: String,
    val amount: Double,
    val currency: String,
    val crName: String,
    val crIban: String,
    val crTaxNo: String,
    val purpose: String,
    val status: ServerStatusModel
): Serializable {
    override fun toString(): String {
        return "CreateInLandOperationResponseModel(operationNo='$operationNo', dtIban='$dtIban', operationName='$operationName', operationStatus='$operationStatus', amount=$amount, currency='$currency', crName='$crName', crIban='$crIban', crTaxNo='$crTaxNo', purpose='$purpose', status=$status)"
    }
}