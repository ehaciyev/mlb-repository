package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class CreateInLandOperationIndividualRequestModel(
    val custId: Long,
    val token: String?,
    val dtAccountId: Long,
    val crIban: String,
    val crCustTaxid: String,
    val crCustName: String,
    val crBankCode: String,
    val crBankName: String,
    val crBankTaxid: String,
    val crBankCorrAcc: String,
    val budgetCode: String,
    val budgetLvl: String,
    val amount: Double,
    val purpose: String,
    val note: String,
    val operationType: Int,
    val lang: EnumLangType,
    val sourceId: Int = 2
)