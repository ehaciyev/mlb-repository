package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetMLBAccountsResponseModel(
    val accounts: ArrayList<IPSMLBAccountResponseModel>,
    val status: ServerStatusModel
)