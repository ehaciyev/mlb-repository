package az.turanbank.mlb.data.remote.model.resources.deposit

import az.turanbank.mlb.util.EnumLangType

data class GetDepositIndividualRequestModel(
    val custId: Long,
    val depositId: Long,
    val token: String?,
    val lang: EnumLangType
)