package az.turanbank.mlb.data.remote.model.pg.request

import az.turanbank.mlb.util.EnumLangType

data class GetMerchantListRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val categoryId: Long,
    val lang: EnumLangType
)
