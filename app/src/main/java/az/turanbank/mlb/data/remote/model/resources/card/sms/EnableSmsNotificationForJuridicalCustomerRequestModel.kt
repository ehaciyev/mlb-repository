package az.turanbank.mlb.data.remote.model.resources.card.sms

data class EnableSmsNotificationForJuridicalCustomerRequestModel(
    val mobile: String,
    val repeatMobile: String,
    val custId: Long,
    val compId: Long,
    val token: String?,
    val cardId: Long
)