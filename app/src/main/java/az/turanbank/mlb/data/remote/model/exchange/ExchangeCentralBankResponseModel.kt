package az.turanbank.mlb.data.remote.model.exchange

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class ExchangeCentralBankResponseModel(
    val exchangeCentralBankList: ArrayList<ExchangeCentralBankListResponseModel>,
    val status: ServerStatusModel
)