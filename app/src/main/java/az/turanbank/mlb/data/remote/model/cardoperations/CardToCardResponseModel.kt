package az.turanbank.mlb.data.remote.model.cardoperations

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class CardToCardResponseModel (
    val receiptNo: String,
    val operationDate: String,
    val requestorCardNumber: String,
    val destCardNumber: String,
    val amount: Double,
    val feeAmount: Double,
    val currency: String,
    val operationName: String,
    val note: String,
    val status: ServerStatusModel
)