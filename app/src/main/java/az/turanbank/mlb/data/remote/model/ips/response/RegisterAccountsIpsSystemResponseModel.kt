package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class RegisterAccountsIpsSystemResponseModel(
    val accountList: ArrayList<AccountResponseModel>,
    val status: ServerStatusModel
)