package az.turanbank.mlb.data.remote.model.response

data class GetBankInfoResponseModel(
    val bankInfoList: ArrayList<BankInfoModel>,
    val status: ServerStatusModel
)