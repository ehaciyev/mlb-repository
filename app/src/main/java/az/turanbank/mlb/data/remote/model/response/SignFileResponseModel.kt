package az.turanbank.mlb.data.remote.model.response

data class SignFileResponseModel(
    val successStatus: ArrayList<OperationStatus>,
    val failStatus: ArrayList<OperationStatus>,
    val status: ServerStatusModel
)