package az.turanbank.mlb.data.remote.model.response

data class GetOrderPeriodListResponseModel (
    val status: ServerStatusModel,
    val respOrderPeriodList: ArrayList<OrderPeriod>
)