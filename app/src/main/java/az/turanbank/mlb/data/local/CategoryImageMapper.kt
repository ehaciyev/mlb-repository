package az.turanbank.mlb.data.local

import az.turanbank.mlb.R

class CategoryImageMapper {
    companion object{
        val categoryMap = hashMapOf<String, Int>()
    }
    fun getCategoryImage(categoryName: String): Int{
        return if (categoryMap[categoryName]===null){
            0
        } else {
            categoryMap[categoryName]!!
        }
    }
    fun saveCategoryMapping() {
        categoryMap["communal"] = R.drawable.ic_communal_payments
        categoryMap["UTILITY"] = R.drawable.ic_communal_payments

        //test
        categoryMap["court"] = R.drawable.buro
        categoryMap["SMART_UTILITY"] = R.drawable.ic_w
        categoryMap["POPULAR_PAYMENTS"] = R.drawable.ic_w
        categoryMap["MUNICIPALITY"] = R.drawable.ic_w
        categoryMap["OTHER"] = R.drawable.file_alt_blue
        categoryMap["EDUCATION"] = R.drawable.ic_education_payment
        categoryMap["PENALTY"] = R.drawable.ic_penalty_payments
        categoryMap["MORTGAGE"] = R.drawable.ic_kredit
        categoryMap["INSURANCE"] = R.drawable.ic_insurance_payment

        categoryMap["dues"] = R.drawable.ic_dues_payments
        categoryMap["PENALTY"] = R.drawable.ic_penalty_payments
        categoryMap["penalty"] = R.drawable.ic_penalty_payments
        categoryMap["DONATION"] = R.drawable.ic_iane
        categoryMap["Mobile"] = R.drawable.ic_m
        categoryMap["TELECOM"] = R.drawable.ic_communication
        categoryMap["credit"] = R.drawable.ic_kredit
        categoryMap["MORTGAGE"] = R.drawable.ic_kredit
        categoryMap["insurance"] = R.drawable.ic_insurance_payment
        categoryMap["education"] = R.drawable.ic_education_payment
        categoryMap["TV"] = R.drawable.ic_tv
        categoryMap["INTERNET"] = R.drawable.ic_w
        categoryMap["other"] = R.drawable.file_alt_blue
        categoryMap["BUDGET"] = R.drawable.ic_budget


        categoryMap["fond"] = R.drawable.ic_iane
        categoryMap["internet"] = R.drawable.ic_w
    }
}