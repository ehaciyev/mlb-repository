package az.turanbank.mlb.data.remote.model.provider

import retrofit2.Retrofit

interface RetrofitProvider :
    DependencyProvider<Retrofit>
