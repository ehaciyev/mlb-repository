package az.turanbank.mlb.data.remote.model.pg.response.payments.template

data class PaymentTemplateResponseModel(
    val id: Long,
    val tempName: String,
    val merchantId: Long,
    val description: String,
    val amount: Double,
    val currency: String,
    val providerId: Long,
    val createdDate: String,
    val categoryId: Long
)