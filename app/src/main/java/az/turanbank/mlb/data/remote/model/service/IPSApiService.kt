package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.data.remote.Constants
import az.turanbank.mlb.data.remote.model.ips.request.*
import az.turanbank.mlb.data.remote.model.ips.request.VerifyOTPRequestModel
import az.turanbank.mlb.data.remote.model.ips.response.*
import az.turanbank.mlb.data.remote.model.provider.ServiceProvider
import az.turanbank.mlb.data.remote.model.request.*
import az.turanbank.mlb.data.remote.model.resources.card.GetCardListIndividualRequestModel
import az.turanbank.mlb.data.remote.model.response.*
import az.turanbank.mlb.data.remote.model.request.GetAccountByIdRequestModel
import az.turanbank.mlb.domain.user.data.GetBudgetCode
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface IPSApiService {
    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("individualRegistration")
    fun registerIndividualCustomer(
        @Body getIndividualCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getAccountByIdForIndividualCustomer")
    fun getAccountByIdForIndividualCustomer(@Body getAccountByIdRequestModel: GetAccountByIdRequestModel2)
            : Observable<GetAccountIdResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getIpsAuthMethods")
    fun getIpsAuthMethods(@Body getIpsAuthMethodRequestModel: GetIpsAuthMethodRequestModel)
            : Observable<GetIpsAuthMethodResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("checkCustomerRegistration")
    fun checkCustomerRegistration(
        @Body getJuridicalCardListRequestModel: GetJuridicalCardListRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("checkAccountRegistration")
    fun checkAccountRegistration(
        @Body getJuridicalCardListRequestModel: GetJuridicalCardListRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getIpsAccounts")
    fun getIpsAccounts(
        @Body getJuridicalCardListRequestModel: GetJuridicalCardListRequestModel
    ): Observable<GetIPSAccountsResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getIpsAliases")
    fun getIpsAliases(
        @Body getJuridicalCardListRequestModel: GetJuridicalCardListRequestModel
    ): Observable<GetIPSAliasesResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("createAccount")
    fun registerIpsAccounts(
        @Body registerAccountIpsSystemRequestModel: RegisterAccountIpsSystemRequestModel
    ): Observable<RegisterAccountsIpsSystemResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getAllBanks")
    fun getAllBanks(
        @Body getJuridicalCardListRequestModel: GetJuridicalCardListRequestModel
    ): Observable<GetAllBanksResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getAliases")
    fun getMLBAliases(
        @Body getJuridicalCardListRequestModel: GetJuridicalCardListRequestModel
    ): Observable<GetMLBAliasesResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getAccounts")
    fun getMLBAccounts(
        @Body getJuridicalCardListRequestModel: GetJuridicalCardListRequestModel
    ): Observable<GetMLBAccountsResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deleteIpsAlias")
    fun deleteIpsAlias(
        @Body deleteIPSAliasRequestModel: DeleteIPSAliasRequestModel
    ): Observable<DeleteAliaseServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("linkAliasToAccount")
    fun linkAliasToAccount(
        @Body linkAliasToAccountRequestModel: LinkAliasToAccountRequestModel
    ): Observable<LinkAliasToAccountResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("createAlias")
    fun createAlias(
        @Body createAliasRequestModel: CreateAliasRequestModel
    ): Observable<GetMLBAliasesResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getDefaultCompounds")
    fun getDefaultCompounds(
        @Body getIndividualCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<GetDefaultCompoundsResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("sendOtpCodeForEmail")
    fun     sendOtpCodeForEmail(
        @Body sendOTPRequestModel: SendOTPForEmailRequestModel
    ): Observable<SendOTPResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("sendOtpCode")
    fun sendOtpCode(
        @Body sendOTPRequestModel: SendOTPRequestModel
    ): Observable<SendOTPResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("verifyOtpCode")
    fun verifyOtpCode(
        @Body verifyRequestModel: VerifyOTPRequestModel
    ): Observable<SendOTPResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("verifyOtpCodeForEmail")
    fun verifyOtpCodeForEmail(
        @Body verifyRequestModel: VerifyOTPRequestModel
    ): Observable<SendOTPResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getIpsAccountByAlias")
    fun getIpsAccountByAlias(
        @Body getAccountByAliasRequestModel: GetAccountByAliasRequestModel
    ): Observable<GetAccountByAliasResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getUnregAccounts")
    fun getUnregAccounts(
        @Body getIndividualCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<RegisterAccountsIpsSystemResponseModel>

    @POST("getAuthAccounts")
    fun getAuthAccounts(
        @Body getIndividualCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<RegisterAccountsIpsSystemResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deleteIpsAccount")
    fun deleteIpsAccount(
        @Body getAccountByIdRequestModel: GetAccountByIdRequestModel
    ): Observable<DeleteIPSAccountResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("setDefault")
    fun setDefault(
        @Body setDefaultAccountRequestModel: SetDefaultAccountRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getCompounds")
    fun getCompounds(
        @Body getIndividualCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<GetDefaultCompoundsResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getBudgetLvlList")
    fun getBudgetLvlList(@Body getBudgetCode: GetBudgetCode): Observable<GetBankInfos>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getBudgetCodeList")
    fun getBudgetCodeList(@Body getBudgetCode: GetBudgetCode): Observable<GetBankInfos>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("createIpsOperation")
    fun createIpsOperation(@Body requestModel: CreateAccountToAliasIpsOperationRequestModel): Observable<CreateIpsOperationResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("createIpsOperation")
    fun createIpsOperation(@Body requestModel: CreateAccountToAccountIpsOperationRequestModel): Observable<CreateIpsOperationResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("createIpsOperation")
    fun createIpsOperation(@Body requestModel: CreateAliasToAliasIpsOperationRequestModel): Observable<CreateIpsOperationResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("createIpsOperation")
    fun createIpsOperation(@Body requestModel: CreateAliasToAccountIpsOperationRequestModel): Observable<CreateIpsOperationResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getBudgetAccountByIban")
    fun getBudgetAccountByIbanIPS(
        @Body getBudgetAccountByIbanRequestModel: GetBudgetAccountByIbanRequestModel
    ): Single<GetBudgetAccountByIbanResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getAccountAndCustomerInfo")
    fun getAccountAndCustomerInfo(@Body requestModel: GetAccountAndCustomerInfoRequestModel)
    : Observable<GetAccountAndCustomerInfoResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getNotLinkedAccountListByAliasId")
    fun getNotLinkedAccountListByAliasId(@Body requestModel: GetNotLinkedAccountListByAliasIdRequestModel)
            : Observable<GetMLBAccountsResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getAccountsByAliasId")
    fun getAccountsByAliasId(@Body requestModel: GetNotLinkedAccountListByAliasIdRequestModel)
            : Observable<GetMLBLinkAccountsResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("getAccountAndCustomerInfoByAlias")
    fun getAccountAndCustomerInfoByAlias(@Body requestModel: GetAccountAndCustomerInfoByAliasRequestModel)
            : Observable<GetAccountAndCustomerInfoByAliasResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operationAdvancedSearch")
    fun operationAdvancedSearch(@Body operationAdvancedSearchRequestModel: OperationAdvancedSearchRequestModel)
            : Observable<GetAllOperationListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("receivePaymentListIndividual")
    fun receivePaymentListIndividual(@Body receivePaymentListIndividualRequestModel: ReceivePaymentsRequestModel)
            : Observable<ReceivePaymentListIndividualResponseModel>

}

interface IPSApiServiceProvider :
    ServiceProvider<IPSApiService>
