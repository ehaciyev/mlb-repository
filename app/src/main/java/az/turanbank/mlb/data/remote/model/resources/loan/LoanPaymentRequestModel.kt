package az.turanbank.mlb.data.remote.model.resources.loan

data class LoanPaymentRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val payType: Int,
    val dtAccountId: Long,
    val loanId: Long,
    val amount: Double,
    val sourceId: Int = 2
)