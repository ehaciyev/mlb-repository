package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.data.remote.Constants
import az.turanbank.mlb.data.remote.model.exchange.*
import az.turanbank.mlb.data.remote.model.provider.ServiceProvider
import io.reactivex.Observable
import retrofit2.http.*

interface ExchangeApiService {
    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("cash/{exchangeDate}")
    fun exchangeCash(
        @Path(value = "exchangeDate", encoded = true) exchangeDate: String?
    ): Observable<ExchangeCashResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("cashless/{exchangeDate}")
    fun exchangeCashless(
        @Path(value = "exchangeDate", encoded = true) exchangeDate: String?
    ): Observable<ExchangeCashlessResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("centralBank/{exchangeDate}")
    fun centralBank(
        @Path(value = "exchangeDate", encoded = true) exchangeDate: String?
    ): Observable<ExchangeCentralBankResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("currenciesCash")
    fun currenciesCash(
    ): Observable<ConverterCurrencyListModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("currenciesCashless")
    fun currenciesCashless(
    ): Observable<ConverterCurrencyListModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("converterCash")
    fun converterCash(
        @Body convertCashRequestModel: ConvertCashRequestModel
    ): Observable<ConvertCashResponseModel>

}
interface ExchangeApiServiceProvider :
    ServiceProvider<ExchangeApiService>