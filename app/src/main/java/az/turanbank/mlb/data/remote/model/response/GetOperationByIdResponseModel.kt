package az.turanbank.mlb.data.remote.model.response

data class GetOperationByIdResponseModel (
    val operId: Long,
    val custId: Long,
    val compId: Long,
    val dtIban: String,
    val crIban: String,
    val crCustName: String,
    val crCustAddress: String,
    val crCustTaxid: String,
    val crCustPhone: String,
    val crBankCode: String,
    val crBankName: String,
    val crBankTaxId: String,
    val crBankCorrAcc: String,
    val crBankSwift: String,
    val crCorrBankName: String,
    val crCorrBankSwift: String,
    val budgetCode: String,
    val budgetLvl: String,
    val budgetCodeValue: String,
    val budgetLvlValue: String,
    val amt: Double,
    val dtAmt: Double,
    val crAmt: Double,
    val feeRate: Double,
    val feeAmt: Double,
    val exchRate: Double,
    val custExchRate: Double,
    val amtCcy: String,
    val dtCcy: String,
    val crCcy: String,
    val operPurpose: String,
    val note: String,
    val crBankCountry: String,
    val crBankCity: String,
    val crCorrBankCountry: String,
    val crBankCorrCity: String,
    val crCorrBankBranch: String,
    val crCorrBankAccount: String,
    val createdDate: String,
    val operName: String,
    val operNameId: Long,
    val operState: String,
    val operStateId: Long,
    val operationType: Long?,
    val operTypeId: Int?,
    val status: ServerStatusModel
) {
    override fun toString(): String {

        val fullAmount = "$amt AZN"

        return "Əməliyyatın tipi: " + operName + "\n" +
                "Əməliyyatın Nömrəsi: " + operId.toString() + "\n" +
                "Alanın adı: " + crCustName + "\n" +
                "Alanın hesabı: " + crIban + "\n" +
                "Vəsait tutulan hesab: " + dtIban + "\n" +
                "Məbləğ: " + fullAmount + "\n" +
                "Yaranma tarixi: " + createdDate + "\n" +
                "Təyinat: " + operPurpose + "\n"

    }
}