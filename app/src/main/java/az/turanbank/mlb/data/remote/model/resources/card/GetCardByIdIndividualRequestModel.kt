package az.turanbank.mlb.data.remote.model.resources.card

import az.turanbank.mlb.util.EnumLangType

data class GetCardByIdIndividualRequestModel(
    val custId: Long,
    val cardId: Long,
    val token: String?,
    val lang: EnumLangType
)
