package az.turanbank.mlb.data.remote.model.pg.response.payments.param

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetMerchantParamListResponseModel(
    val merchantParamList: ArrayList<MerchantParamListResponseModel>,
    val status: ServerStatusModel
)