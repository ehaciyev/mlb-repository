package az.turanbank.mlb.data.remote.model.cardoperations

import az.turanbank.mlb.util.EnumLangType

data class CardToCardRequestModel(
    val custId: Long,
    val requestorCardId: Long,
    val destinationCardId: Long,
    val currency: String,
    val amount: Double?,
    val token: String?,
    val lang: EnumLangType
)
