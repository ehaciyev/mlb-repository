package az.turanbank.mlb.data.remote.model.response

data class RegisterWithASANResponseModel(
    val custId: Long,
    val status: ServerStatusModel
)
