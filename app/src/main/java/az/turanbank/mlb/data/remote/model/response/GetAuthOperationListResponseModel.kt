package az.turanbank.mlb.data.remote.model.response

data class GetAuthOperationListResponseModel(
    val operationList: ArrayList<AuthOperation>,
    val status: ServerStatusModel
)