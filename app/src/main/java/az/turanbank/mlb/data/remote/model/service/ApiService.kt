package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.data.remote.model.provider.ServiceProvider

interface ApiService

interface ApiServiceProvider :
    ServiceProvider<ApiService>