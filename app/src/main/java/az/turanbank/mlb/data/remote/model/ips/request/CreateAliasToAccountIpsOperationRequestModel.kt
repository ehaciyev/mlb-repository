package az.turanbank.mlb.data.remote.model.ips.request

data class CreateAliasToAccountIpsOperationRequestModel (
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val receiverBankId: Long,
    val senderAliasId: Long,
    val businessProcessId: Long,
    val operNameId: Long = 5L,
    val operTypeId: Long = 10L,
    val ipsPayment: IpsPaymentModelAliasToAccount
)
