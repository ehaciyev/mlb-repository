package az.turanbank.mlb.data.remote.model.request

data class UpdateProfileImageRequestModel(
    val custId: Long,
    val token: String?,
    val compId: Long?,
    val bytesStr: String
)