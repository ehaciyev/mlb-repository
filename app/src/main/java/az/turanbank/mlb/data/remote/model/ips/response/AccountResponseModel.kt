package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class AccountResponseModel(
    val accountId: Long,
    val accountName: String,
    val iban: String,
    val openDate: String,
    val currName: String,
    val branchId: Long,
    val branchName: String,
    val currentBalance: Double,
    val status: ServerStatusModel
) {
    override fun toString(): String {
        return "AccountResponseModel(accountId=$accountId, accountName='$accountName', iban='$iban', openDate='$openDate', currName='$currName', branchId=$branchId, branchName='$branchName', currentBalance=$currentBalance, status=$status)"
    }
}