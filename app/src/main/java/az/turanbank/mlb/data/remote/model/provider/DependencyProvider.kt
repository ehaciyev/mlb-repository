package az.turanbank.mlb.data.remote.model.provider

interface DependencyProvider<INSTANCE_TYPE> {
    fun getInstance(): INSTANCE_TYPE
}