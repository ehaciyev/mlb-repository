package az.turanbank.mlb.data.remote.model.exchange

data class CashlessResponseModel(
    val currency: String,
    val cashlessBuy: Double,
    val cashlessSell: Double
)