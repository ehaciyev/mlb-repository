package az.turanbank.mlb.data.remote.model.response

data class BudgetAccountModel (
    val name: String,
    val taxNumber: String,
    val iban: String
)