package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetOrderKindTypeListRequestModel(
    val custId: Long,
    val token: String?,
    val orderType: Int?,
    val lang: EnumLangType
)