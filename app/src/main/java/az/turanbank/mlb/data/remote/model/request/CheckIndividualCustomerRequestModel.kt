package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class CheckIndividualCustomerRequestModel(
    val pin: String,
    val mobile: String,
    val lang: EnumLangType,
    val sourceId: Long = 2L
)