package az.turanbank.mlb.data.local

open class CurrencySignMapper {
    companion object{
        val signMap = hashMapOf<String, String>()
    }
    open fun getCurrencySign(currency: String): String{
        return if (signMap[currency]===null){
            currency
        } else {
            signMap[currency]!!
        }
    }
    open fun saveSignMap() {
        signMap["USD"] = "$"
        signMap["AZN"] = "₼"
        signMap["EUR"] = "€"
        signMap["RUB"] = "₽"
        signMap["TRY"] = "₺"
        signMap["GEL"] = "\u20BE"
        signMap["DEM"] = "DM"
        signMap["FRF"] = "Fr"
        signMap["CNY"] = "¥"
        signMap["CHF"] = "kr"
        signMap["GBP"] = "£"
    }
}