package az.turanbank.mlb.data.remote.model.request

data class UnblockIndividualCustomerRequestModel(
    val custId: Long
)