package az.turanbank.mlb.data.remote.model.resources.deposit

data class DepositListModel(
    val id: Long,
    val holder: String,
    val custId: Long,
    val branchName: String,
    val amount: Double,
    val yearPercent: Double,
    val currency: String,
    val paymentFrequency: String,
    val openDate: String,
    val endDate: String,
    val iban: String,
    val name: String
)
