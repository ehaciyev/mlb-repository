package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.ips.ConditionalDetailsModel
import az.turanbank.mlb.data.remote.model.ips.ConditionalIbanModel
import az.turanbank.mlb.data.remote.model.ips.ConditionalServicerModel
import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class IPSAccountResponseModel(
    val id: ConditionalIbanModel,
    val type: String,
    val currency: String,
    val servicer: ConditionalServicerModel,
    val openingDate: String,
    val closingDate: String,
    val details: ConditionalDetailsModel,
    val status: ServerStatusModel
)