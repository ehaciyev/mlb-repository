package az.turanbank.mlb.data.remote.model.response

data class CustomerResponseModel(
    val custId: Long,
    val custCode: Long,
    val name: String,
    val surname: String,
    val patronymic: String,
    val pin: String,
    val mobile: String,
    val userStatus: Int,
    val birthDate: String,
    val birthPlace: String,
    val country: String,
    val city: String,
    val address: String,
    val email: String,
    val gender: String,
    val work: String,
    val workPlace: String,
    val position: String,
    val workPhone: String,
    val extNo: String,
    val blockReason: String,
    val status: ServerStatusModel,
    val token: String,
    val compId: Long,
    val certCode: String
) {
    override fun toString(): String {
        return "CustomerResponseModel(custId=$custId, " +
                "custCode=$custCode, " +
                "name='$name', " +
                "surname='$surname', " +
                "patronymic='$patronymic', " +
                "pin='$pin', " +
                "mobile='$mobile', " +
                "userStatus=$userStatus, " +
                "birthDate='$birthDate', " +
                "birthPlace='$birthPlace', " +
                "country='$country', " +
                "city='$city', " +
                "address='$address', " +
                "email='$email', " +
                "gender='$gender', " +
                "work='$work', " +
                "workPlace='$workPlace', " +
                "position='$position', " +
                "workPhone='$workPhone', " +
                "extNo='$extNo', " +
                "blockReason='$blockReason', " +
                "status=$status, " +
                "token='$token', " +
                "compId='$compId')"
    }
}
