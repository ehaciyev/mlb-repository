package az.turanbank.mlb.data.remote.model.response

data class GetAccountRequisitesResponseModel(
    val branchCode: Long,
    val branchName: String,
    val branchInn: String,
    val correspondentAccount: String,
    val swift: String,
    val address: String,
    val phone: String,
    val fax: String,
    val accountName: String,
    val currName: String,
    val iban: String,
    val accountInn: String?,
    val status: ServerStatusModel
) {
    override fun toString(): String {
        var string =
            "Filialın kodu: " + branchCode.toString() + "\n" +
            "Filial adı: " + branchName + "\n" +
            "Bankın VÖEN-i: " + branchInn + "\n" +
            "Müxbir hesabı: " + correspondentAccount + "\n" +
            "SWIFT kodu: " + swift + "\n" +
            "Ünvanı: " + address + "\n" +
            "Nömrə: " + phone + "\n" +
            "Fax: " + fax + "\n" +
            "Hesabın adı: " + accountName + "\n" +
            "Valyutası: " + currName + "\n" +
            "Hesab nömrəsi: " + iban + "\n"

        if(accountInn != null) {
            string = string + "Hesabın VÖEN-i: " + accountInn + "\n"
        }
        return string
    }
}
