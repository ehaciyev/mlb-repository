package az.turanbank.mlb.data.remote.model.resources.account

import az.turanbank.mlb.util.EnumLangType

data class CreateAbroadOperationRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val dtAccountId: Long,
    val amount: Double,
    val crBankName: String,
    val crBankSwift: String,
    val crCustName: String,
    val crCustAddress: String,
    val crIban: String,
    val purpose: String,
    val crBankBranch: String,
    val crBankAddress: String,
    val crBankCountry: String,
    val crBankCity: String,
    val crCustPhone: String,
    val note: String,
    val crCorrBankName: String,
    val crCorrBankCountry: String,
    val crCorrBankCity: String,
    val crCorrBankSwift: String,
    val crCorrBankAccount: String,
    val crCorrBankBranch: String,
    val lang: EnumLangType,
    val sourceId: Int = 2
)
