package az.turanbank.mlb.data.remote.model.resources.card

data class BlockCardForJuridicalCustomerRequestModel(
    val custId: Long,
    val compId: Long,
    val token: String?,
    val cardId: Long,
    val blockReason: String
)
