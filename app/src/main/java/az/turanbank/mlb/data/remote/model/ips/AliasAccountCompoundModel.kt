package az.turanbank.mlb.data.remote.model.ips

import java.io.Serializable

data class AliasAccountCompoundModel(
    val id: Long,
    val accountId: Long,
    val aliasId: Long,
    val customerName: String,
    val customerSurname: String,
    val alias: String,
    val aliasType: String,
    val iban: String,
    val isDefault: Int,
    var isSelected: Boolean
): Serializable