package az.turanbank.mlb.data.remote.model.pg.response.payments.pay

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel
import java.io.Serializable

data class PayResponseModel(
    val specialCode: String?,
    val receiptNumber: String?,
    val billingDate: String?,
    val cardNumber: String?,
    val merchantName: String?,
    val amount: Double?,
    val description: String?,
    val status: ServerStatusModel,
    val merchantId:Long,
    val categoryId: Long,
    val providerId: Long
): Serializable