package az.turanbank.mlb.data.remote.model.pg.request

data class RemovePaymentTemplateRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val tempId: Long
)