package az.turanbank.mlb.data.remote.model.resources.card

data class UnBlockCardForJuridicalCustomerRequestModel(
    val custId: Long,
    val compId: Long,
    val token: String?,
    val cardId: Long
)

