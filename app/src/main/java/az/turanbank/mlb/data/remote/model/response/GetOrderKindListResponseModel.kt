package az.turanbank.mlb.data.remote.model.response

// THIS RESPONSE MODEL IS EXACTLY SAME WITH @GetOrderKindResponseModel. AND BOTH CLASSES SHOULD BE REFACTORED
data class GetOrderKindListResponseModel(
    val respOrderTypeList: ArrayList<OrderKind>,
    val status: ServerStatusModel
)