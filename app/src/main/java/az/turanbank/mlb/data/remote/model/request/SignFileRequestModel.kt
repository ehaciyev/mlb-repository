package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class SignFileRequestModel(
    val custId: Long,
    val token: String?,
    val operationIds: ArrayList<Long>,
    val transactionId: Long,
    val phoneNumber: String?,
    val userId: String?,
    val lang: EnumLangType,
    val successOperId: ArrayList<Long>,
    val failOperId: ArrayList<Long>,
    val certCode: String?
)