package az.turanbank.mlb.data.remote.model.response

data class GetCardStatementResponseModel(
        val cardStamentList: ArrayList<CardStatementModel>,
        val status: ServerStatusModel
)