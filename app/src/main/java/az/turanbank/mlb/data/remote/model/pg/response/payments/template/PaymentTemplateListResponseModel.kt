package az.turanbank.mlb.data.remote.model.pg.response.payments.template

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class PaymentTemplateListResponseModel(
    val paymentTempList: ArrayList<PaymentTemplateResponseModel>,
    val status: ServerStatusModel
)