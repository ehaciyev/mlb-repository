package az.turanbank.mlb.data.remote.model.pg.response.payments.list

data class PaymentResponseModel(
    val transactionNumber: String?,
    val currency: String,
    val receiptNumber: String?,
    val cardNumber: String?,
    val description: String?,
    val merchantName: String?,
    val billingDate: String?,
    val amount: Double?,
    val merchantId:Long,
    val categoryId: Long,
    val providerId: Long
)