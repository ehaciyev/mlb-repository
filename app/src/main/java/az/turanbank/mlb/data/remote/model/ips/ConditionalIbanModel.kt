package az.turanbank.mlb.data.remote.model.ips

data class ConditionalIbanModel(
    val iban: String
)