package az.turanbank.mlb.data.remote.model.resources.account

data class GetAccountStatementForJuridicalCustomerRequestModel(
        val custId: Long,
        val compId: Long,
        val token: String?,
        val accountId: Long,
        val startDate: String,
        val endDate: String
)