package az.turanbank.mlb.data.remote.model.ips.response

data class GetIPSAccounts(

    val accountId: Long,
    val accountName: String?,
    val iban: String,
    val openDate: String,
    val currName: String,
    val branchId: Long,
    val branchName: String,
    val currentBalance: String,
    val ipsAccountId: String,
    val currency: String,
    val isLinked: String,
    var isDefault: Int,
    var checked: Boolean,
    val status: RespStatus

)
