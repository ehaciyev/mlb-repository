package az.turanbank.mlb.data.remote.model.cardoperations

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class CashByCodeResponseModel(
    val cashCode: Long,
    val receiptNo: String,
    val operationDate: String,
    val dtCardNumber: String,
    val mobile: String,

    val amount: Double,
    val feeAmount: Double,
    val currency: String,
    val operationName: String,
    val note: String,
    val status: ServerStatusModel
)