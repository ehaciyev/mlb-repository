package az.turanbank.mlb.data.remote.model.ips.request

data class SetDefaultAccountRequestModel(
    val custId: Long,
    val token: String?,
    val compoundId: Long
)