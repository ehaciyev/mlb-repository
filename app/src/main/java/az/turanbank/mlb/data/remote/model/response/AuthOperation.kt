package az.turanbank.mlb.data.remote.model.response

import java.io.Serializable

data class AuthOperation(
    val operId: Long? = null,
    val dtName: String? = null,
    val dtIban: String? = null,
    val crName: String,
    val crIban: String,
    val amount: Double? = null,
    val operPurpose: String? = null,
    val currency: String? = null,
    val createdDate: String? = null,
    val operName: String? = null,
    val operState: String? = null,
    val operStateId: Int = 3,
    val operNameId: Int?,
    val operTypeId: Int?,
    val feeRate: Double?,
    val feeAmt: Double?,
    val exchRate: Double?,
    val custExchRate: Double?,
    val dtAmt: Double?,
    val crAmt: Double?,
    val dtCcy: String?,
    val crCcy: String?,
    val status: ServerStatusModel? = null,
    var isSelected: Boolean = false
): Serializable