package az.turanbank.mlb.data.remote.model.response

data class CampaignModel(
    val campaignId: Long,
    val name: String,
    val image: String,
    val note: String,
    val status: ServerStatusModel
)