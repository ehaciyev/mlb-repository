package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetSignInfoResponseModel (
    val verificationCode: String,
    val transactionId: Long,
    val successOperId: ArrayList<Long>,
    val failOperId: ArrayList<Long>,
    val status: ServerStatusModel
)