package az.turanbank.mlb.data.remote.model.ips.response

data class IPSBankResponseModel(
    val name: String,
    val code: String,
    val id: Long,
    val corrAccount: String,
    val subCorrAccount: String,
    val swiftCode: String,
    val testSwiftCode: String,
    val taxNumber: String
)