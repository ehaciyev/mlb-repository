package az.turanbank.mlb.data.remote.model.resources.card.sms

import az.turanbank.mlb.util.EnumLangType

class GetCardRequisitesForJuridicalCustomerRequestModel(
    val custId: Long,
    val compId: Long,
    val cardId: Long,
    val token: String?,
    val lang: EnumLangType
)
