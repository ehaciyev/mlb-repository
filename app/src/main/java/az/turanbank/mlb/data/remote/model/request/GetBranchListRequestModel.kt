package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetBranchListRequestModel(
    val custId: Long,
    val token: String?,
    val lang: EnumLangType
)