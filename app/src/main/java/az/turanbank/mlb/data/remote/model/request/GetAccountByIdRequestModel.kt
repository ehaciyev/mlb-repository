package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetAccountByIdRequestModel(
    val compId: Long?,
    val custId: Long,
    val token: String?,
    val accountIds: ArrayList<Long>,
    val lang: EnumLangType
)