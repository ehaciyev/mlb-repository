package az.turanbank.mlb.data.remote.model.response

import java.io.Serializable

data class BatchOperation(
    val batchId: Long,
    val batchName: String,
    val docCount: Int,
    val createdDate: String,
    val batchType: String,
    val status: ServerStatusModel,
    var isSelected: Boolean
): Serializable