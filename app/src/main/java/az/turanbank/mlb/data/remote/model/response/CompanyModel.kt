package az.turanbank.mlb.data.remote.model.response

data class CompanyModel(
    val id: Long,
    val name: String,
    val taxNo: String,
    val signCount: Int,
    val signLevel: Int,
    val certCode: String
)
