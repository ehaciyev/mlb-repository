package az.turanbank.mlb.data.remote.model.resources.loan

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetLoansListResponseModel(
        val loans: ArrayList<LoanListModel>,
        val status: ServerStatusModel
)