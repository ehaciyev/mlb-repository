package az.turanbank.mlb.data.remote.model.pg.request

import java.io.Serializable

data class IdentificationCodeRequestModel(
    val paramName: String?,
    val paramValue: String?
): Serializable