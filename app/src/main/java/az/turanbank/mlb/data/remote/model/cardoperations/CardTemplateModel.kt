package az.turanbank.mlb.data.remote.model.cardoperations

data class CardTemplateModel(
    val tempId: Long,
    val tempName: String,
    val requestorCardNumber: String,
    val destinationCardNumber: String,
    val amount: Double,
    val currency: String,
    val cardOperationType: Int,
    val cardOperationTypeValue: String,
    val createdDate: String
)