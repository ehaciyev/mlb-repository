package az.turanbank.mlb.data.remote.model.request

data class RegisterJuridicalAsanRequestModel(
    val pin: String,
    val taxNo: String,
    val custId: Long,
    val compId: Long,
    val username: String,
    val password: String,
    val repeatPassword: String
)
