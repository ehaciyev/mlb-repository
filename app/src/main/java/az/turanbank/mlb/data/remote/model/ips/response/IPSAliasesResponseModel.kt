package az.turanbank.mlb.data.remote.model.ips.response

data class IPSAliasesResponseModel(
    val recordId: Long,
    val type: String,
    val value: String,
    val startDate: String,
    val expirationDate: String,
    val status: String,
    val accounts: ArrayList<IPSAccountResponseModel>
)