package az.turanbank.mlb.data.remote.model.response

data class GetAccountIdResponseModel(
    val accountId: Long,
    val accountName: String,
    val iban: String,
    val openDate: String,
    val currName: String,
    val branchId: Long,
    val branchName: String,
    val currentBalance: Double,
    val status: ServerStatusModel
)