package az.turanbank.mlb.data.remote.model.resources.card

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class BlockReasonResponseModel(
    val blockReasonList: Array<String>,
    val status: ServerStatusModel
)