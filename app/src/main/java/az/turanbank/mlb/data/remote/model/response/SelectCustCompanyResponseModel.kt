package az.turanbank.mlb.data.remote.model.response

data class SelectCustCompanyResponseModel (
    val status: ServerStatusModel,
    val customer: CustomerResponseModel,
    val company: CompanyModel
)