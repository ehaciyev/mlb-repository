package az.turanbank.mlb.data.remote.model.request

data class GetOperationByIdRequestModel(
    val operId: Long,
    val custId: Long,
    val token: String?
)