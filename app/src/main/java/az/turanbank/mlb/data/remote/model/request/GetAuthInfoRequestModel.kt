package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetAuthInfoRequestModel(
    val phoneNumber: String,
    val userId: String,
    val lang: EnumLangType
)