package az.turanbank.mlb.data.remote.model.resources.account

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetAccountListResponseModel(
        val accountList: ArrayList<AccountListModel>,
        val status: ServerStatusModel

)