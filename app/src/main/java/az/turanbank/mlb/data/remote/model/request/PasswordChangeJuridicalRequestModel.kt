package az.turanbank.mlb.data.remote.model.request

data class PasswordChangeJuridicalRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val oldPassword: String?,
    val password: String?,
    val repeatPassword: String?
)