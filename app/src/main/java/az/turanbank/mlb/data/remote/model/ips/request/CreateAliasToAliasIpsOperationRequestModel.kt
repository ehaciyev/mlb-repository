package az.turanbank.mlb.data.remote.model.ips.request

data class CreateAliasToAliasIpsOperationRequestModel (
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val receiverBankId: Long,
    val senderAliasId: Long,
    val receiverAliasTypeId: Long,
    val receiverAlias: String?,
    val operNameId: Long = 5L,
    val operTypeId: Long = 9L,
    val ipsPayment: IpsPaymentModelWithAlias
)
