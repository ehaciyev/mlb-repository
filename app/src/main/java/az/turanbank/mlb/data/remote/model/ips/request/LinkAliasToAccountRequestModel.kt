package az.turanbank.mlb.data.remote.model.ips.request

data class LinkAliasToAccountRequestModel(
    val custId: Long,
    val token: String?,
    val aliasIdAndAccountIdList: ArrayList<AliasAndAccountRequestModel>
)