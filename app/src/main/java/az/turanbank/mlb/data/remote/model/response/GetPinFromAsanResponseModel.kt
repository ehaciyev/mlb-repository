package az.turanbank.mlb.data.remote.model.response

data class GetPinFromAsanResponseModel(
    val pin: String,
    val status: ServerStatusModel
)
