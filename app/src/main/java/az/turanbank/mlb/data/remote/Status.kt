package az.turanbank.mlb.data.remote

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}