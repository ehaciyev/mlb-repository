package az.turanbank.mlb.data.remote.model.resources.card

data class UnBlockCardForIndividualCustomerRequestModel (
    val custId: Long,
    val token: String?,
    val cardId: Long
)
