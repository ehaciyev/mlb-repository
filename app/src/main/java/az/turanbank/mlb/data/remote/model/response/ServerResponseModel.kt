package az.turanbank.mlb.data.remote.model.response

data class ServerResponseModel(
    val status: ServerStatusModel
)