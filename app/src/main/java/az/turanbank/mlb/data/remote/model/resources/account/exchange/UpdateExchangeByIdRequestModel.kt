package az.turanbank.mlb.data.remote.model.resources.account.exchange

data class UpdateExchangeByIdRequestModel (
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val dtAccountId: Long,
    val dtAmount: Double,
    val crAmount: Double,
    val crIban: String,
    val dtBranchId: Long,
    val crBranchId: Long,
    val exchangeRate: Double,
    val exchangeOperationType: Int,
    val operId: Long,
    val operNameId: Long
    )
