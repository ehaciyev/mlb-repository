package az.turanbank.mlb.data.remote.model.response

data class GetBudgetAccountByIbanResponseModel (
    val budgetAccount: BudgetAccountModel,
    val status: ServerStatusModel
)