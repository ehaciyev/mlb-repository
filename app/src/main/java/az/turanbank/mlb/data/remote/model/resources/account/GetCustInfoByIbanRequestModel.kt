package az.turanbank.mlb.data.remote.model.resources.account

data class GetCustInfoByIbanRequestModel (
    val custId: Long,
    val token: String?,
    val iban: String
)