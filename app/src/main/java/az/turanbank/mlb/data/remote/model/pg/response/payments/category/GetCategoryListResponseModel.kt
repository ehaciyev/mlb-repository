package az.turanbank.mlb.data.remote.model.pg.response.payments.category

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetCategoryListResponseModel(
    val categoryList: ArrayList<CategoryResponseModel>,
    val status: ServerStatusModel
)