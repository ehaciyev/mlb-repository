package az.turanbank.mlb.data.local

import az.turanbank.mlb.R

open class ExchangeFlagMapper {
    open fun resolveFlag(flagCode: String): Int{
        when (flagCode){
            "AZN" -> return R.drawable.currency_aze
            "RUB" -> return R.drawable.currency_rub
            "EUR" -> return R.drawable.currency_euro
            "USD" -> return R.drawable.currency_usd
            "TRY" -> return R.drawable.currency_try
            "AED" -> return R.drawable.currency_aed
            "GBP" -> return R.drawable.currency_gbp
            "FRF" -> return R.drawable.currency_french
            "DEM" -> return R.drawable.currency_german
            "CHF" -> return R.drawable.currency_sweden
            "GEL" -> return R.drawable.currency_georgia
            "YEN" -> return R.drawable.currency_japan
            "CNY" -> return R.drawable.currency_china
            "KWD" -> return R.drawable.currency_kuwait
            "SAR" -> return R.drawable.currency_saud_arabia
            "UAH" -> return R.drawable.currency_ukraina
        }
        return 0
    }
}