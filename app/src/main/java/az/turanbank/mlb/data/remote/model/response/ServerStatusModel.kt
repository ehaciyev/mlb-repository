package az.turanbank.mlb.data.remote.model.response

import java.io.Serializable

data class ServerStatusModel(
    val statusCode: Int,
    val statusMessage: String
): Serializable
