package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.util.EnumLangType

data class GetNotLinkedAccountListByAliasIdRequestModel (
    val custId: Long,
    val compId: Long?,
    val aliasId: Long,
    val token: String?,
    val lang: EnumLangType
)
