package az.turanbank.mlb.data.remote.model.ips.request

data class IpsPaymentModel (
    val paymentAmount: Long,
    val receiverCustomerAccount: String,
    val additionalInfoAboutPayment: String,
    val additionalInfoFromCustomer: String,
    val remittanceInfo: String,
    val receiverTaxId: String
)