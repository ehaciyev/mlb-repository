package az.turanbank.mlb.data.remote.model.response

data class GetFirstAuthOperationListResponseModel (
    val status: ServerStatusModel,
    val batchOperationList: ArrayList<BatchOperation>
)