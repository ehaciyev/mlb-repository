package az.turanbank.mlb.data.remote.model.request

data class GetBudgetAccountByIbanRequestModel (
    val custId: Long,
    val token: String?,
    val iban: String
)