package az.turanbank.mlb.data.remote.model.resources.deposit

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class EarlyPaysResponseModel(
    val fromPeriod: Int,
    val toPeriod: Int,
    val percent: Double,
    val status: ServerStatusModel
)