package az.turanbank.mlb.data.remote.model.request

data class GetBankListForJuridicalRequestModel(
    val ccy: String,
    val custId: Long,
    val compId: Long,
    val token: String?
)