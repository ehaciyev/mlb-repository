package az.turanbank.mlb.data.remote.model.response

data class MlbResponseModel(
    val customer: CustomerResponseModel,
    val company: CompanyModel,
    val status: ServerStatusModel
)