package az.turanbank.mlb.data.remote.model.cardoperations

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetCardOperationTempListResponseModel(
    val cardTempList: ArrayList<CardTemplateModel>,
    val status: ServerStatusModel
)