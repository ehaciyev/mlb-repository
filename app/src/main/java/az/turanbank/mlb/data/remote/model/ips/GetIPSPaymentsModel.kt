package az.turanbank.mlb.data.remote.model.ips

data class GetIPSPaymentsModel(
    val id: Long
)