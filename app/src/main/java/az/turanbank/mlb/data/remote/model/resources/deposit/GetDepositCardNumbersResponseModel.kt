package az.turanbank.mlb.data.remote.model.resources.deposit

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetDepositCardNumbersResponseModel(
    val cards: ArrayList<CardNumber>,
    val status: ServerStatusModel
)