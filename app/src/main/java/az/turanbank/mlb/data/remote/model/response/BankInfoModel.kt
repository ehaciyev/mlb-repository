package az.turanbank.mlb.data.remote.model.response

data class BankInfoModel(
    val bankCode: String,
    val bankTaxid: String,
    val bankCorrAcc: String,
    val bankSwift: String,
    val bankName: String,
    val currency: String
) {
     val bankNameFull: String
         get() ="$bankCode - $bankName"
}