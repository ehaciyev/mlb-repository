package az.turanbank.mlb.data.remote.model.response

data class CardStatementModel (
        val cardNumber: String,
        val dtAmount: Double,
        val crAmount: Double,
        val currency: String,
        val purpose: String,
        val fee: Double,
        val trDate: String
)