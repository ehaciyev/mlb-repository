package az.turanbank.mlb.data.remote.model.pg.response.payments.merchant

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetMerchantListResponseModel(
    val merchantList: ArrayList<MerchantResponseModel>,
    val status: ServerStatusModel
)