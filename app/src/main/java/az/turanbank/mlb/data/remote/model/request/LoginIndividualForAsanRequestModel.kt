package az.turanbank.mlb.data.remote.model.request

data class LoginIndividualForAsanRequestModel (
    val userId: String?,
    val phoneNumber: String?,
    val transactionId: Long,
    val certificate: String,
    val challenge: String,
    val custId: Long,
    val device: String,
    val location: String
)