package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetAllBanksResponseModel(
    val bankInfos: ArrayList<IPSBankResponseModel>,
    val status: ServerStatusModel
)