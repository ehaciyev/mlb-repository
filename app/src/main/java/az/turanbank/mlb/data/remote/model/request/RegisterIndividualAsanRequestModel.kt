package az.turanbank.mlb.data.remote.model.request

data class RegisterIndividualAsanRequestModel(
    val username: String,
    val password: String,
    val repeatPassword: String,
    val custId: Long
)