package az.turanbank.mlb.data.remote.model.resources.card

data class BlockCardForIndividualCustomerRequestModel(
    val custId: Long,
    val token: String?,
    val cardId: Long,
    val blockReason: String
)
