package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class CreateOrderRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val branchId: Long,
    val orderType: Int?,
    val kindId: Long?,
    val periodId: Int,
    val amount: Double,
    val ccy: String,
    val lang: EnumLangType
)