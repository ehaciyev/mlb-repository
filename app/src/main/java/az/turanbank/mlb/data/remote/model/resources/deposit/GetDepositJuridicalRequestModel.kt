package az.turanbank.mlb.data.remote.model.resources.deposit

import az.turanbank.mlb.util.EnumLangType

data class GetDepositJuridicalRequestModel(
    val custId: Long,
    val compId: Long,
    val depositId: Long,
    val token: String?,
    val lang:EnumLangType
)