package az.turanbank.mlb.data.remote.model.ips.request

data class CreateAccountToAliasIpsOperationRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val receiverBankId: Long,
    val senderAccountId: Long,
    val receiverAliasTypeId: Long,
    val receiverAlias: String?,
    val operNameId: Long = 5L,
    val operTypeId: Long = 8L,
    val ipsPayment: IpsPaymentModelWithAlias
)