package az.turanbank.mlb.data.remote.model.request

data class GetBankListForIndividualRequestModel(
    val ccy: String,
    val custId: Long,
    val token: String?
)