package az.turanbank.mlb.data.remote.model.ips.response

data class GetIpsAuthMethodResponseModel(
    val authMethodList: ArrayList<RespAuthMethod>,
    val status: RespStatus
)