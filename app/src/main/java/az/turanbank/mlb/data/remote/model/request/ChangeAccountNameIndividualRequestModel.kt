package az.turanbank.mlb.data.remote.model.request

data class ChangeAccountNameIndividualRequestModel(
    val custId: Long,
    val token: String?,
    val accountId: Long,
    val accountName: String
)