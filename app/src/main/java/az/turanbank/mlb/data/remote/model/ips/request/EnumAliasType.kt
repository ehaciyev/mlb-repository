package az.turanbank.mlb.data.remote.model.ips.request

enum class EnumAliasType {
    PIN,
    TIN,
    EMAIL,
    MOBILE,
    MERCHANTID,
    OBJECTCODE
}
