package az.turanbank.mlb.data.remote.model.exchange

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class CurrencyResponseModel(
    val currency: String,
    val cashBuy: Double,
    val cashSell: Double,
    val status: ServerStatusModel?
) {
    override fun toString(): String {
        return "CurrencyResponseModel(currency='$currency', cashBuy=$cashBuy, cashSell=$cashSell, status=$status)"
    }
}