package az.turanbank.mlb.data.remote.model.ips.request

data class AliasRequestModel(
    val type: String,
    val value: String,
    val startDate: String,
    val expirationDate: String?,
    val status: Int
)