package az.turanbank.mlb.data.remote.model.resources.card

data class SaveCardOperationTempRequestModel(
    val custId: Long,
    val tempName: String,
    val requestorCardNumber: String,
    val destinationCardNumber: String,
    val currency: String,
    val cardOperationType: Int,
    val amount: Double,
    val token: String?
)