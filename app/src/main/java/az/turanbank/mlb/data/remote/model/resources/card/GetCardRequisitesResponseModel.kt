package az.turanbank.mlb.data.remote.model.resources.card

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetCardRequisitesResponseModel(
    val branchCode: Long,
    val branchName: String,
    val branchInn: String,
    val correspondentAccount: String,
    val swift: String,
    val address: String,
    val phone: String,
    val fax: String,
    val accountName: String,
    val cardNumber: String,
    val currName: String,
    val iban: String,
    val fullName: String,
    val accountInn: String?,
    val status: ServerStatusModel
) {
    override fun toString(): String {
        var string =
            "Filialın kodu: " + branchCode.toString() + "\n" +
                    "Filial adı: " + branchName + "\n" +
                    "Bankın VÖEN-i: " + branchInn + "\n" +
                    "Bankın adı: " + branchName + "\n" +
                    "Kartın nömrəsi: " + cardNumber + "\n" +
                    "Müxbir hesabı: " + correspondentAccount + "\n" +
                    "SWIFT kodu: " + swift + "\n" +
                    "Ünvanı: " + address + "\n" +
                    "Nömrə: " + phone + "\n" +
                    "Müştərinin adı: " + fullName + "\n" +
                    "Valyutası: " + currName + "\n" +
                    "Hesab nömrəsi: " + iban + "\n"

        if(accountInn != null) {
            string = string + "Müştərinin VÖEN-i: " + accountInn + "\n"
        }
        return string
    }
}