package az.turanbank.mlb.data.remote.model.resources.account

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetAccountTemplateListResponseModel(
    val respOperationTempList: ArrayList<AccountTemplateResponseModel>,
    val status: ServerStatusModel
)