package az.turanbank.mlb.data.remote.model.ips.response

data class RespAuthMethod(
    val type: String,
    val name: String,
    var checked: Boolean
)