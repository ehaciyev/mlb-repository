package az.turanbank.mlb.data.remote.model.resources.account

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel
import java.io.Serializable

data class AccountTemplateResponseModel(
    val id: Long,
    val tempName: String,
    val custId: Long,
    val compId: Long,
    val operationTypeId: Int,
    val operNameId: Int,
    val dtIban: String,
    val dtCustName: String,
    val dtAccountBranchId: Long,
    val dtAccountBranch: String,
    val dtAccount: String,
    val dtCcy: String,
    val crIban: String,
    val crAccountBranch: String,
    val crAccount: String,
    val crCcy: String,
    val crCustTaxid: String,
    val crCustAddress: String,
    val crCustPhone: String,
    val crBankAddress: String,
    val crBankCountry: String,
    val crBankCity: String,
    val crCustName: String,
    val crBankCode: String,
    val crBankName: String,
    val crBankTaxid: String,
    val crBankCorrAcc: String,
    val crBankSwift: String,
    val crCorrBankName: String,
    val crCorrBankSwift: String,
    val crCorrBankCountry: String,
    val crCorrBankCity: String,
    val crCorrBankBranch: String,
    val budgetCode: String,
    val budgetLvl: String,
    val dtAmt: Double,
    val crAmt: Double,
    val amt: Double,
    val amtCcy: String,
    val chargeAccount: String,
    val chargeAmt: Double,
    val exchRate: Double,
    val custExchRate: Double,
    val operPurpose: String,
    val note: String,
    val operName: String,
    val operDescription: String,
    val operStateName: String,
    val operStateDescription: String,
    val operTypeName: String,
    val createdDate: String,
    val status: ServerStatusModel
): Serializable {
    override fun toString(): String {
        return "AccountTemplateResponseModel(id=$id, tempName='$tempName', custId=$custId, compId=$compId, operationTypeId=$operationTypeId, operNameId=$operNameId, dtIban='$dtIban', dtCustName='$dtCustName', dtAccountBranchId=$dtAccountBranchId, dtAccountBranch='$dtAccountBranch', dtAccount='$dtAccount', dtCcy='$dtCcy', crIban='$crIban', crAccountBranch='$crAccountBranch', crAccount='$crAccount', crCcy='$crCcy', crCustTaxid='$crCustTaxid', crBankAddress='$crBankAddress', crCustName='$crCustName', crBankCode='$crBankCode', crBankName='$crBankName', crBankTaxid='$crBankTaxid', crBankCorrAcc='$crBankCorrAcc', crBankSwift='$crBankSwift', crCorrBankName='$crCorrBankName', crCorrBankSwift='$crCorrBankSwift', budgetCode='$budgetCode', budgetLevel='$budgetLvl', dtAmt=$dtAmt, crAmt=$crAmt, amt=$amt, amtCcy='$amtCcy', chargeAccount='$chargeAccount', chargeAmt=$chargeAmt, exchRate=$exchRate, custExchRate=$custExchRate, operPurpose='$operPurpose', note='$note', operName='$operName', operDescription='$operDescription', operStateName='$operStateName', operStateDescription='$operStateDescription', operTypeName='$operTypeName', createdDate='$createdDate', status=$status)"
    }
}