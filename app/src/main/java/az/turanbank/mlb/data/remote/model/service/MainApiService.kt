package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.data.remote.Constants
import az.turanbank.mlb.data.remote.model.branch.GetBranchMapListResponseModel
import az.turanbank.mlb.data.remote.model.cardoperations.*
import az.turanbank.mlb.data.remote.model.pg.request.*
import az.turanbank.mlb.data.remote.model.pg.response.payments.category.GetCategoryListResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.check.CheckMerchantResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.list.GetPaymentListResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.merchant.GetMerchantListResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.param.GetMerchantParamListResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.pay.PayResponseModel
import az.turanbank.mlb.data.remote.model.pg.response.payments.template.PaymentTemplateListResponseModel
import az.turanbank.mlb.data.remote.model.provider.ServiceProvider
import az.turanbank.mlb.data.remote.model.request.*
import az.turanbank.mlb.data.remote.model.resources.account.*
import az.turanbank.mlb.data.remote.model.resources.account.exchange.CreateExchangeOperationRequestModel
import az.turanbank.mlb.data.remote.model.resources.account.exchange.CreateExchangeOperationResponseModel
import az.turanbank.mlb.data.remote.model.resources.account.exchange.UpdateExchangeByIdRequestModel
import az.turanbank.mlb.data.remote.model.resources.card.*
import az.turanbank.mlb.data.remote.model.resources.card.sms.*
import az.turanbank.mlb.data.remote.model.resources.deposit.*
import az.turanbank.mlb.data.remote.model.resources.loan.*
import az.turanbank.mlb.data.remote.model.response.*
import az.turanbank.mlb.util.EnumLangType
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface MainApiService {
    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("register/checkToken")
    fun checkToken(
        @Body checkCustomerRequestModel: CheckTokenRequestModel
    ): Single<CheckTokenResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardListForIndividualCustomer")
    fun getCardListForIndividualCustomer(
        @Body getCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<GetCardListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardListForJuridicalCustomer")
    fun getCardListForJuridicalCustomer(
        @Body getJuridicalCardListIndividualRequestModel: GetJuridicalCardListRequestModel
    ): Observable<GetCardListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getAccountListForIndividualCustomer")
    fun getAccountListForIndividualCustomer(
        @Body getCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<GetAccountListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("util/checkAppVersion/{versionName}")
    fun checkAppVersion(
        @Path(value = "versionName") versionName: String
    ): Single<CheckAppVersionResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @GET("util/checkInternet")
    fun checkInternet(): Single<CheckInternetServerStatusModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getNoCardAccountListForIndividualCustomer")
    fun getNoCardAccountListForIndividualCustomer(
        @Body getCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<GetAccountListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getNoCardAccountListForJuridicalCustomer")
    fun getNoCardAccountListForJuridicalCustomer(
        @Body getCardListJuridicalRequestModel: GetJuridicalCardListRequestModel
    ): Observable<GetAccountListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loan/loanIndividual")
    fun loansForIndividualCustomer(
        @Body getCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<GetLoansListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/individualDepositList")
    fun individualDepositList(
        @Body getCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Observable<DepositListResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getAccountListForJuridicalCustomer")
    fun getAccountListForJuridicalCustomer(
        @Body getCardListJuridicalRequestModel: GetJuridicalCardListRequestModel
    ): Observable<GetAccountListResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loan/loanJuridical")
    fun loansForJuridicalCustomer(
        @Body getCardListJuridicalRequestModel: GetJuridicalCardListRequestModel
    ): Observable<GetLoansListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/juridicalDepositList")
    fun juridicalDepositList(
        @Body getCardListJuridicalRequestModel: GetJuridicalCardListRequestModel
    ): Observable<DepositListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardStatementForJuridicalCustomer")
    fun getCardStatementForJuridicalCustomer(
        @Body getCardStatementForJuridicalCustomerRequestModel: GetCardStatementForJuridicalCustomerRequestModel
    ): Observable<GetCardStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardStatementForIndividualCustomer")
    fun getCardStatementForIndividualCustomer(
        @Body getCardStatementForIndividualCustomerRequestModel: GetCardStatementForIndividualCustomerRequestModel
    ): Observable<GetCardStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getDebitStatementForIndividualCustomer")
    fun getCardDebitStatementForIndividualCustomer(
        @Body getCardStatementForJuridicalCustomerRequestModel: GetCardStatementForIndividualCustomerRequestModel
    ): Observable<GetCardStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getDebitStatementForJuridicalCustomer")
    fun getCardDebitStatementForJuridicalCustomer(
        @Body getCardStatementForIndividualCustomerRequestModel: GetCardStatementForJuridicalCustomerRequestModel
    ): Observable<GetCardStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCreditStatementForIndividualCustomer")
    fun getCardCreditStatementForIndividualCustomer(
        @Body getCardStatementForJuridicalCustomerRequestModel: GetCardStatementForIndividualCustomerRequestModel
    ): Observable<GetCardStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCreditStatementForJuridicalCustomer")
    fun getCardCreditStatementForJuridicalCustomer(
        @Body getCardStatementForIndividualCustomerRequestModel: GetCardStatementForJuridicalCustomerRequestModel
    ): Observable<GetCardStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardBlockReasons")
    fun getCardBlockReasons(
        @Body getCardListIndividualRequestModel: GetCardListIndividualRequestModel
    ): Single<BlockReasonResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getAccountStatementForJuridicalCustomer")
    fun getAccountStatementForJuridicalCustomer(
        @Body getCardListIndividualRequestModel: GetAccountStatementForJuridicalCustomerRequestModel
    ): Single<GetAccountStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getDebitStatementForIndividualCustomer")
    fun getDebitStatementForIndividualCustomer(
        @Body getCardListIndividualRequestModel: GetAccountStatementForIndividualCustomerRequestModel
    ): Single<GetAccountStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getDebitStatementForJuridicalCustomer")
    fun getDebitStatementForJuridicalCustomer(
        @Body getCardListIndividualRequestModel: GetAccountStatementForJuridicalCustomerRequestModel
    ): Single<GetAccountStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getCreditStatementForJuridicalCustomer")
    fun getCreditStatementForJuridicalCustomer(
        @Body getCardListIndividualRequestModel: GetAccountStatementForJuridicalCustomerRequestModel
    ): Single<GetAccountStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getCreditStatementForIndividualCustomer")
    fun getCreditStatementForIndividualCustomer(
        @Body getCardListIndividualRequestModel: GetAccountStatementForIndividualCustomerRequestModel
    ): Single<GetAccountStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getAccountStatementForIndividualCustomer")
    fun getAccountStatementForIndividualCustomer(
        @Body getCardListIndividualRequestModel: GetAccountStatementForIndividualCustomerRequestModel
    ): Single<GetAccountStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/blockCardForIndividualCustomer")
    fun blockCardForIndividualCustomer(
        @Body blockCardForIndividualCustomerRequestModel: BlockCardForIndividualCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/blockCardForJuridicalCustomer")
    fun blockCardForJuridicalCustomer(
        @Body blockCardForJuridicalCustomerRequestModel: BlockCardForJuridicalCustomerRequestModel
    ): Single<ServerResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/unblockCardForIndividualCustomer")
    fun unBlockCardForIndividualCustomer(
        @Body unBlockCardForIndividualCustomerRequestModel: UnBlockCardForIndividualCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/unblockCardForJuridicalCustomer")
    fun unBlockCardForJuridicalCustomer(
        @Body unBlockCardForJuridicalCustomerRequestModel: UnBlockCardForJuridicalCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/disableSmsNotificationForIndividualCustomer")
    fun disableSmsNotificationForIndividualCustomer(
        @Body enableSmsNotificationForIndividualCustomerRequestModel: EnableSmsNotificationForIndividualCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/addOrUpdateSmsNotifNumberForIndividualCustomer")
    fun enableSmsNotificationForIndividualCustomer(
        @Body enableSmsNotificationForIndividualCustomerRequestModel: EnableSmsNotificationForIndividualCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/disableSmsNotificationForJuridicalCustomer")
    fun disableSmsNotificationForJuridicalCustomer(
        @Body enableSmsNotificationForJuridicalCustomerRequestModel: EnableSmsNotificationForJuridicalCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/addOrUpdateSmsNotifNumberForJuridicalCustomer")
    fun enableSmsNotificationForJuridicalCustomer(
        @Body enableSmsNotificationForJuridicalCustomerRequestModel: EnableSmsNotificationForJuridicalCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardRequisiteForIndividualCustomer")
    fun getCardRequisiteForIndividualCustomer(
        @Body getCardRequisitesForIndividualCustomerRequestModel: GetCardRequisitesForIndividualCustomerRequestModel
    ): Single<GetCardRequisitesResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardRequisiteForJuridicalCustomer")
    fun getCardRequisiteForJuridicalCustomer(
        @Body getCardRequisitesForJuridicalCustomerRequestModel: GetCardRequisitesForJuridicalCustomerRequestModel
    ): Single<GetCardRequisitesResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getAccountRequisitesForJuridicalCustomer")
    fun getAccountRequisitesForJuridicalCustomer(
        @Body getAccountRequisitesForJuridicalCustomerRequestModel: GetAccountRequisitesForJuridicalCustomerRequestModel
    ): Single<GetAccountRequisitesResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/getAccountRequisitesForIndividualCustomer")
    fun getAccountRequisitesForIndividualCustomer(
        @Body getAccountRequisitesForIndividualCustomerRequestModel: GetAccountRequisitesForIndividualCustomerRequestModel
    ): Single<GetAccountRequisitesResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardByIdForJuridical")
    fun getCardByIdForJuridical(
        @Body getCardByIdJuridicalRequestModel: GetCardByIdJuridicalRequestModel
    ): Observable<CardListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardByIdForIndividual")
    fun getCardByIdForIndividual(
        @Body getCardByIdIndividualRequestModel: GetCardByIdIndividualRequestModel
    ): Observable<CardListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/cardToCard")
    fun cardToCard(
        @Body cardToCardRequestModel: CardToCardRequestModel
    ): Single<CardToCardResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/cashByCode")
    fun cashByCode(
        @Body cashByCodeRequestModel: CashByCodeRequestModel
    ): Single<CashByCodeResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/cardToOtherCard")
    fun cardToOtherCard(
        @Body cardToOtherCardRequestModel: CardToOtherCardRequestModel
    ): Single<CardToCardResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardOperationTempList")
    fun getCardOperationTempList(
        @Body getTempRequestModel: GetCardOperationTempListRequestModel
    ): Single<GetCardOperationTempListResponseModel>


    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getCardOperationTempById")
    fun getCardOperationTempById(
        @Body getTempByIdRequestModel: GetCardOperationTempListRequestModel
    ): Single<CardTemplateResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/deleteCardOperationTemp")
    fun deleteCardOperationTemp(
        @Body getTempByIdRequestModel: GetCardOperationTempListRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/saveCashByCodeTemp")
    fun saveCashByCodeTemp(
        @Body saveCashByCodeTempRequestModel: SaveCashByCodeTempRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/saveCardOperationTemp")
    fun saveCardOperationTemp(
        @Body saveCardOperationTempRequestModel: SaveCardOperationTempRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getSmsNotificationNumberForIndividualCustomer")
    fun getSmsNotificationNumberForIndividualCustomer(
        @Body model: GetCardByIdIndividualRequestModel
    ): Single<GetSmsNotificationNumberResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/getSmsNotificationNumberForJuridicalCustomer")
    fun getSmsNotificationNumberForJuridicalCustomer(
        @Body model: GetCardByIdJuridicalRequestModel
    ): Single<GetSmsNotificationNumberResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/addOrUpdateSmsNotifNumberForIndividualCustomer")
    fun addOrUpdateSmsNotifNumberForIndividualCustomer(
        @Body enableSmsNotificationForIndividualCustomerRequestModel: EnableSmsNotificationForIndividualCustomerRequestModel
    ): Single<GetSmsNotificationNumberResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("card/addOrUpdateSmsNotifNumberForJuridicalCustomer")
    fun addOrUpdateSmsNotifNumberForJuridicalCustomer(
        @Body enableSmsNotificationForJuridicalCustomerRequestModel: EnableSmsNotificationForJuridicalCustomerRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/createInternalOperationIndividual")
    fun createInternalOperationIndividual(
        @Body createInternalOperationIndividualRequestModel: CreateInternalOperationIndividualRequestModel
    ): Single<DomesticOperationResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/createInternalOperationJuridical")
    fun createInternalOperationJuridical(
        @Body createInternalOperationJuridical: CreateInternalOperationJuridicalRequestModel
    ): Single<DomesticOperationResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getCustInfoByIban")
    fun getCustInfoByIban(
        @Body createInternalOperationIndividualRequestModel: GetCustInfoByIbanRequestModel
    ): Single<GetCustInfoByIbanResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getAllOperationListIndividual")
    fun getAllOperationListIndividual(
        @Body getAllOperationListIndividualRequestModel: GetAllOperationListIndividualRequestModel
    ): Single<GetAllOperationListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getAllOperationListJuridical")
    fun getAllOperationListJuridical(
        @Body getAllOperationListJuridicalRequestModel: GetAllOperationListJuridicalRequestModel
    ): Single<GetAllOperationListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getBankList")
    fun getBankListForIndividual(
        @Body getBankListForIndividualRequestModel: GetBankListForIndividualRequestModel
    ): Single<GetBankInfoResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getBankList")
    fun getBankListForJuridical(
        @Body getBankListForJuridicalRequestModel: GetBankListForJuridicalRequestModel
    ): Single<GetBankInfoResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getBudgetClassificationCode")
    fun getBudgetClassificationCode(
        @Body getBudgetCodeRequestModel: GetBudgetCodeRequestModel
    ): Single<GetBudgetCodeResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getBudgetLevelCode")
    fun getBudgetLevelCode(
        @Body getBudgetCodeRequestModel: GetBudgetCodeRequestModel
    ): Single<GetBudgetCodeResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getBudgetAccountList")
    fun getBudgetAccountList(
        @Body getBudgetAccountListRequestModel: GetBudgetAccountListRequestModel
    ): Single<GetBudgetAccountListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getBudgetAccountByIban")
    fun getBudgetAccountByIban(
        @Body getBudgetAccountByIbanRequestModel: GetBudgetAccountByIbanRequestModel
    ): Single<GetBudgetAccountByIbanResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/createInlandOperationIndividual")
    fun createInLandOperationIndividual(
        @Body createInLandOperationIndividualRequestModel: CreateInLandOperationIndividualRequestModel
    ): Single<DomesticOperationResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/createInlandOperationJuridical")
    fun createInLandOperationJuridical(
        @Body createInLandOperationJuridicalRequestModel: CreateInLandOperationJuridicalRequestModel
    ): Single<DomesticOperationResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("util/getCampaignList")
    fun getCampaignList(@Body getCampaignListRequestModel: GetCampaignListRequestModel): Single<CampaignResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("util/getCampaignById")
    fun getCampaignById(@Body getCampaignByIdRequestModel: GetCampaignByIdRequestModel): Single<CampaignModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getAuthOperationListIndividual")
    fun getAuthOperationListIndividual(
        @Body getAuthOperationListIndividualRequestModel: GetAuthOperationListIndividualRequestModel
    ): Single<GetAuthOperationListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loan/loanStatementIndividual")
    fun loanStatementIndividual(
        @Body loanPaymentPlanIndividualRequestModel: LoanPaymentPlanIndividualRequestModel
    ): Single<LoanStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loan/loanStatementJuridical")
    fun loanStatementJuridical(
        @Body loanPaymentPlanJuridicalRequestModel: LoanPaymentPlanJuridicalRequestModel
    ): Single<LoanStatementResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loan/loanPaymentPlanIndividual")
    fun loanPaymentPlanIndividual(
        @Body loanPaymentPlanIndividualRequestModel: LoanPaymentPlanIndividualRequestModel
    ): Single<LoanPaymentPlanResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loan/loanPaymentPlanJuridical")
    fun loanPaymentPlanJuridical(
        @Body loanPaymentPlanJuridicalRequestModel: LoanPaymentPlanJuridicalRequestModel
    ): Single<LoanPaymentPlanResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loan/loanPaymentIndividual")
    fun loanPaymentIndividual(
        @Body loanPaymentPlanIndividualRequestModel: LoanPaymentPlanIndividualRequestModel
    ): Single<LoanPayedAmountsResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("loan/loanPaymentJuridical")
    fun loanPaymentJuridical(
        @Body loanPaymentPlanJuridicalRequestModel: LoanPaymentPlanJuridicalRequestModel
    ): Single<LoanPayedAmountsResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/individualDepositById")
    fun individualDepositById(
        @Body depositIndividualRequestModel: GetDepositIndividualRequestModel
    ): Single<DepositByIdResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/juridicalDepositById")
    fun juridicalDepositById(
        @Body depositJuridicalRequestModel: GetDepositJuridicalRequestModel
    ): Single<DepositByIdResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/individualCardsById")
    fun individualDepositCardsById(
        @Body depositIndividualRequestModel: GetDepositIndividualRequestModel
    ): Single<GetDepositCardNumbersResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/juridicalCardsById")
    fun juridicalDepositCardsById(
        @Body depositJuridicalRequestModel: GetDepositJuridicalRequestModel
    ): Single<GetDepositCardNumbersResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/individualEarlyPayPercentsById")
    fun individualEarlyPayPercentsById(
        @Body depositIndividualRequestModel: GetDepositIndividualRequestModel
    ): Single<GetEarlyPaysByDepositResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/juridicalEarlyPayPercentsById")
    fun juridicalEarlyPayPercentsById(
        @Body depositJuridicalRequestModel: GetDepositJuridicalRequestModel
    ): Single<GetEarlyPaysByDepositResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/individualPayedAmountsById")
    fun individualPayedAmountsById(
        @Body depositIndividualRequestModel: GetDepositIndividualRequestModel
    ): Single<GetPayedAmountsByDepositIdResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("deposit/juridicalPayedAmountsById")
    fun juridicalPayedAmountsById(
        @Body depositJuridicalRequestModel: GetDepositJuridicalRequestModel
    ): Single<GetPayedAmountsByDepositIdResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getSignInfo")
    fun getSignInfo(
        @Body getSignInfoRequestModel: GetSignInfoRequestModel
    ): Single<GetSignInfoResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getSecondSignInfo")
    fun getSecondSignInfo(
        @Body getSecondSignInfoRequestModel: GetSecondSignInfoRequestModel
    ): Single<GetSignInfoResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/signFile")
    fun signFile(
        @Body signFileRequestModel: SignFileRequestModel
    ): Single<SignFileResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/signFileSecondAuth")
    fun signFileSecondAuth(
        @Body signFileSecondAuthRequestModel: SignFileSecondAuthRequestModel
    ): Single<SignFileResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("register/logout")
    fun logout(
        @Body logoutRequestModel: LogoutRequestModel
    ): Single<LogoutResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getSecondAuthOperationList")
    fun getSecondAuthOperationList(
        @Body getSecondAuthOperationListRequestModel: GetSecondAuthOperationListRequestModel
    ): Single<GetAuthOperationListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getFirstAuthOperationList")
    fun getFirstAuthOperationList(
        @Body getFirstAuthOperationListRequestModel: GetFirstAuthOperationListRequestModel
    ): Single<GetFirstAuthOperationListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getFirstSignInfo")
    fun getFirstSignInfo(
        @Body getFirstSignInfoRequestModel: GetFirstSignInfoRequestModel
    ): Single<GetSignInfoResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/signFileFirstAuth")
    fun signFileFirstAuth(
        @Body signFileFirstAuthRequestModel: SignFileFirstAuthRequestModel
    ): Single<SignFileResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getOperationTempListIndividual")
    fun getOperationTempListIndividual(
        @Body request: GetCardListIndividualRequestModel
    ): Single<GetAccountTemplateListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getOperationTempListJuridical")
    fun getOperationTempListJuridical(
        @Body request: GetJuridicalCardListRequestModel
    ): Single<GetAccountTemplateListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/deleteOperation")
    fun deleteOperation(
        @Body deleteOperation: DeleteOperationRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/addOperationTempJuridical")
    fun addOperationTempJuridical(
        @Body addOperationTemplateRequestModel: AddOperationTemplateRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/addOperationTempIndividual")
    fun addOperationTempIndividual(
        @Body addOperationTemplateRequestModel: AddOperationTemplateRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getPaymentDocByOperId")
    fun getPaymentDocByOperId(
        @Body docByOperIdRequestModel: GetPaymentDocByOperIdRequestModel
    ): Single<GetPaymentDocByOperIdResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getAsanDocByOperId")
    fun getAsanDocByOperId(
        @Body docByOperIdRequestModel: GetPaymentDocByOperIdRequestModel
    ): Single<GetPaymentDocByOperIdResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/deleteOperationTemp")
    fun deleteOperationTemp(
        @Body deleteOperationTemplateRequestModel: DeleteOperationTemplateRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getOperationAllInfoById")
    fun getOperationById(
        @Body getOperationByIdRequestModel: GetOperationByIdRequestModel
    ): Single<GetOperationByIdResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/createAbroadOperationIndividual")
    fun createAbroadOperationIndividual(
        @Body createAbroadOperationRequestModel: CreateAbroadOperationRequestModel
    ): Single<DomesticOperationResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/createAbroadOperationJuridical")
    fun createAbroadOperationJuridical(
        @Body createAbroadOperationRequestModel: CreateAbroadOperationRequestModel
    ): Single<DomesticOperationResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/updateOperationById")
    fun updateOperationById(
        @Body updateOperationByIdRequestModel: UpdateOperationByIdRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/updateOperationById")
    fun updateAbroadById(
        @Body abroadOperationRequestModel: UpdateAbroadOperationRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/updateOperationById")
    fun updateExchangeById(
        @Body updateExchangeByIdRequestModel: UpdateExchangeByIdRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("order/getBranchList")
    fun getBranchList(
        @Body getBranchListRequestModel: GetBranchListRequestModel
    ): Single<GetBranchListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("order/getOrderKindList")
    fun getOrderKindList(
        @Body getOrderKindListRequestModel: GetOrderKindListRequestModel
    ): Single<GetOrderKindListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/createExchangeOperationJuridical")
    fun createExchangeOperationJuridical(
        @Body createExchangeOperationRequestModel: CreateExchangeOperationRequestModel
    ): Single<CreateExchangeOperationResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/createExchangeOperationIndividual")
    fun createExchangeOperationIndividual(
        @Body createExchangeOperationRequestModel: CreateExchangeOperationRequestModel
    ): Single<CreateExchangeOperationResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("order/createOrderJuridical")
    fun createOrderJuridical(
        @Body createOrderRequestModel: CreateOrderRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("order/createOrderIndividual")
    fun createOrderIndividual(
        @Body createOrderRequestModel: CreateOrderRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("order/getOrderKindTypeList")
    fun getOrderKindTypeList(
        @Body kindTypeListRequestModel: GetOrderKindTypeListRequestModel
    ): Single<GetOrderKindTypeResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("order/getOrderPeriodList")
    fun getOrderPeriodList(
        @Body periodRequestModel: GetOrderPeriodRequestModel
    ): Single<GetOrderPeriodListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("util/getBranchMapList/{lang}")
    fun getBranchMapList( @Path(value = "lang") lang: EnumLangType ): Single<GetBranchMapListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/operationAdvancedSearch")
    fun operationAdvancedSearch(
        @Body advancedSearchRequestModel: OperationAdvancedSearchRequestModel
    ): Single<GetAllOperationListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("operation/getOperationStateList")
    fun getOperationStateList(
        @Body getOperationStateListRequestModel: GetOperationStateListRequestModel
    ): Single<GetOperationStateListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("setting/updateProfileImage")
    fun updateProfileImage(
        @Body updateProfileImageRequestModel: UpdateProfileImageRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("setting/getProfileImage")
    fun getProfileImage(
        @Body getProfileImageRequestModel: GetProfileImageRequestModel
    ): Single<GetProfileImageResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("setting/changePasswordIndividual")
    fun changePasswordIndividual(
        @Body passwordChangeIndividualRequestModel: PasswordChangeIndividualRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("setting/changePasswordJuridical")
    fun changePasswordJuridical(
        @Body passwordChangeJuridicalRequestModel: PasswordChangeJuridicalRequestModel
    ): Single<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pay/payIndividual")
    fun loanPayIndividual(
        @Body loanPaymentRequestModel: LoanPaymentRequestModel
    ): Single<LoanPaymentResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pay/payJuridical")
    fun loanPayJuridical(
        @Body loanPaymentRequestModel: LoanPaymentRequestModel
    ): Single<LoanPaymentResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/getCategoryListIndividual")
    fun getCategoryListIndividual(
        @Body getCategoryListRequestModel: GetCategoryListRequestModel
    ): Observable<GetCategoryListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/getCategoryListJuridical")
    fun getCategoryListJuridical(
        @Body getCategoryListRequestModel: GetCategoryListRequestModel
    ): Observable<GetCategoryListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/getMerchantListIndividual")
    fun getMerchantListIndividual(
        @Body getMerchantListRequestModel: GetMerchantListRequestModel
    ): Observable<GetMerchantListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/getMerchantListJuridical")
    fun getMerchantListJuridical(
        @Body getMerchantListRequestModel: GetMerchantListRequestModel
    ): Observable<GetMerchantListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/getMerchantParamListIndividual")
    fun getMerchantParamListIndividual(
        @Body getMerchantParamListRequestModel: GetMerchantParamListRequestModel
    ): Observable<GetMerchantParamListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/getMerchantParamListJuridical")
    fun getMerchantParamListJuridical(
        @Body getMerchantParamListRequestModel: GetMerchantParamListRequestModel
    ): Observable<GetMerchantParamListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/checkMerchantIndividual")
    fun checkMerchantIndividual(
        @Body checkMerchantRequestModel: CheckMerchantRequestModel
    ): Observable<CheckMerchantResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/checkMerchantJuridical")
    fun checkMerchantJuridical(
        @Body checkMerchantRequestModel: CheckMerchantRequestModel
    ): Observable<CheckMerchantResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/payIndividual")
    fun payIndividual(
        @Body payRequestModel: PayRequestModel
    ): Observable<PayResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/payJuridical")
    fun payJuridical(
        @Body payRequestModel: PayRequestModel
    ): Observable<PayResponseModel>
    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/paymentListIndividual")
    fun paymentListIndividual(
        @Body getPaymentListRequestModel: GetPaymentListRequestModel
    ): Observable<GetPaymentListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/paymentListJuridical")
    fun paymentListJuridical(
        @Body getPaymentListRequestModel: GetPaymentListRequestModel
    ): Observable<GetPaymentListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/paymentTempListIndividual")
    fun paymentTempListIndividual(
        @Body getPaymentListRequestModel: GetPaymentListRequestModel
    ): Observable<PaymentTemplateListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/paymentTempListJuridical")
    fun paymentTempListJuridical(
        @Body getPaymentListRequestModel: GetPaymentListRequestModel
    ): Observable<PaymentTemplateListResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/createPaymentTempIndividual")
    fun createPaymentTempIndividual(
        @Body createPaymentTemplateRequestModel: CreatePaymentTemplateRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/createPaymentTempJuridical")
    fun createPaymentTempJuridical(
        @Body createPaymentTemplateRequestModel: CreatePaymentTemplateRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/changeAccountNameIndividual")
    fun changeAccountNameIndividual(
        @Body changeAccountNameIndividual: ChangeAccountNameIndividualRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("account/changeAccountNameJuridical")
    fun changeAccountNameJuridical(
        @Body changeAccountNameJuridicalRequestModel: ChangeAccountNameJuridicalRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/removePaymentTempIndividual")
    fun removePaymentTempIndividual(
        @Body removePaymentTemplateRequestModel: RemovePaymentTemplateRequestModel
    ): Observable<ServerResponseModel>

    @Headers(
        Constants.CONTENT_TYPE_JSON,
        Constants.CHARSET,
        Constants.ACCEPT,
        Constants.BASIC_AUTH
    )
    @POST("pg/removePaymentTempJuridical")
    fun removePaymentTempJuridical(
        @Body removePaymentTemplateRequestModel: RemovePaymentTemplateRequestModel
    ): Observable<ServerResponseModel>
}

interface MainApiServiceProvider : ServiceProvider<MainApiService>