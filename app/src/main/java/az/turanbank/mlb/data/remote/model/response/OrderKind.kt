package az.turanbank.mlb.data.remote.model.response

data class OrderKind (
    val id: Long,
    val name: String
)