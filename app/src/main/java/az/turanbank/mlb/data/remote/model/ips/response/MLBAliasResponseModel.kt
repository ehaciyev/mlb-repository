package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel
import java.io.Serializable

data class  MLBAliasResponseModel(
    val id: Long,
    val ipsType: String,
    val ipsValue: String,
    val startDate: String,
    val expirationDate: String,
    val status: Int,
    val ipsAliasId: Long,
    val respStatus: ServerStatusModel,
    var isSelected: Boolean
) : Serializable{
    override fun toString(): String {
        return "MLBAliasResponseModel(id=$id, ipsType='$ipsType', ipsValue='$ipsValue', startDate='$startDate', expirationDate='$expirationDate', status=$status, ipsAliasId=$ipsAliasId, respStatus=$respStatus)"
    }
}