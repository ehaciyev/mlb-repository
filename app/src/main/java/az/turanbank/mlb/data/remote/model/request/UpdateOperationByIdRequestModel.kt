package az.turanbank.mlb.data.remote.model.request

data class UpdateOperationByIdRequestModel (
    val custId: Long,
    val compId: Long? = null,
    val token: String?,
    val dtAccountId: Long? = null,
    val crIban: String? = null,
    val crCustTaxid: String? = null,
    val crCustName: String? = null,
    val crBankCode: String? = null,
    val crBankName: String? = null,
    val crBankTaxid: String? = null,
    val crBankCorrAcc: String? = null,
    val budgetCode: String? = null,
    val budgetLvl: String? = null,
    val amount: Double,
    val purpose: String? = null,
    val note: String? = null,
    val operationType: Int? = null,
    val operId: Long,
    val operNameId: Long
)