package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetAccountByAliasResponseModel(
    val respIpsAccountList: ArrayList<IPSAccountResponseModel>,
    val status: ServerStatusModel
)