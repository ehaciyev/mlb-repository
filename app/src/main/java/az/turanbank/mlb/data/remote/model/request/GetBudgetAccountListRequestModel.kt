package az.turanbank.mlb.data.remote.model.request

data class GetBudgetAccountListRequestModel (
    val custId: Long,
    val token: String?
)