package az.turanbank.mlb.data.remote.model.exchange

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class ExchangeCashResponseModel(
    val exchangeCashList: ArrayList<CurrencyResponseModel>,
    val status: ServerStatusModel

) {
    override fun toString(): String {
        return "ExchangeCashResponseModel(exchangeCashList=$exchangeCashList, status=$status)"
    }
}