package az.turanbank.mlb.data.remote.model.pg.request

data class ReqPaymentDataList (
    val invoiceNumber: String?,
    val paymentReceiver: Long?,
    val paymentReceiverDescription: String,
    val subscriberName: String,
    val serviceName: String,
    val budgetCode: String,
    val amount: Double,
    val identificationCode: ArrayList<IdentificationCodeRequestModel>,
    val identificationType: String,
    val transactionNumber: String,
    val feeCalculationMethod: String,
    val feeAmount: Double,
    val parentTrNumber: String?
)
