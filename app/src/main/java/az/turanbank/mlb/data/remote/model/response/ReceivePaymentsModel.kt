package az.turanbank.mlb.data.remote.model.response

data class ReceivePaymentsModel(
    val dtCustName: String,
    val operId: Long,
    val operStateId: Int,
    val operStateName: String,
    val bankName: String,
    val dtIban: String,
    val crIban: String,
    val amount: Double,
    val currency: String,
    val createdDate: String,
    val purpose: String
)