package az.turanbank.mlb.data.remote.model.ips.request

data class RegisterAccountIpsSystemRequestModel(
    val accountId: ArrayList<Long>,
    val custId: Long,
    val token: String?
)