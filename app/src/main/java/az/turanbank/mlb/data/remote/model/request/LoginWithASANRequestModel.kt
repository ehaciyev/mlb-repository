package az.turanbank.mlb.data.remote.model.request

data class LoginWithASANRequestModel(
    val transactionId: Long,
    val certificate: String,
    val challenge: String
)
