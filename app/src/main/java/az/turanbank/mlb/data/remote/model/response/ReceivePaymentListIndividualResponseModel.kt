package az.turanbank.mlb.data.remote.model.response

data class ReceivePaymentListIndividualResponseModel(
    val receivePayments: ArrayList<ReceivePaymentsModel>,
    val status: ServerStatusModel
)