package az.turanbank.mlb.data.remote.model.pg.request

data class PayRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val sourceId: Int = 2,
    val transactionId: String?,
    val categoryId: Long,
    val merchantId: Long,
    val providerId: Long,
    val cardId: Long,
    val reqPaymentDataList: ArrayList<ReqPaymentDataList>
)