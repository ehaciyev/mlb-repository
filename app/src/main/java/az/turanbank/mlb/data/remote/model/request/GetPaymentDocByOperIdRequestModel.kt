package az.turanbank.mlb.data.remote.model.request

data class GetPaymentDocByOperIdRequestModel(
    val custId: Long,
    val token: String?,
    val operId: Long
)