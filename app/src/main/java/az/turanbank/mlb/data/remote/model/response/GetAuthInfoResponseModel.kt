package az.turanbank.mlb.data.remote.model.response

data class GetAuthInfoResponseModel(
    val phoneNumber: String,
    val userId: String,
    val lang: String,
    val transactionId: Long,
    val verificationCode: String,
    val certificate: String,
    val personalCode: String,
    val challenge: String,
    val fullName: String,
    val status: ServerStatusModel
)