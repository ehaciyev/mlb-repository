package az.turanbank.mlb.data.remote.model.ips.request

data class GetAccountAndCustomerInfoRequestModel(
    val iban: String?,
    val servicerBIC: String? = null,
    val servicerMemberId: String? = null,
    val swiftCode: String?,
    val testSwiftCode: String?,
    val custId: Long,
    val token: String?
)
