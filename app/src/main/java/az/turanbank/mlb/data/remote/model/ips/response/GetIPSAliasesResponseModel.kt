package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetIPSAliasesResponseModel(
    val respIpsAliases: ArrayList<IPSAliasesResponseModel>,
    val status: ServerStatusModel
)