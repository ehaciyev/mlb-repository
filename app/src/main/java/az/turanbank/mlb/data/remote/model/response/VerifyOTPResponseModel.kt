package az.turanbank.mlb.data.remote.model.response

data class VerifyOTPResponseModel(
    val status: ServerStatusModel
) {
    override fun toString(): String {
        return "VerifyOTPResponseModel(status=$status)"
    }
}
