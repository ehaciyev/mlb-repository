package az.turanbank.mlb.data.remote.model.request

data class GetBudgetCodeRequestModel (
    val custId: Long,
    val token: String?
)