package az.turanbank.mlb.data.remote.model.cardoperations

import az.turanbank.mlb.util.EnumLangType

data class GetCardOperationTempListRequestModel(
    val tempId: Long?,
    val custId: Long,
    val token: String?,
    val lang: EnumLangType
)