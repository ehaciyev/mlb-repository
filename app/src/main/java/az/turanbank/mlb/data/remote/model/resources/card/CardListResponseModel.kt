package az.turanbank.mlb.data.remote.model.resources.card

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class CardListResponseModel(
    val cardId: Long,
    val holder: String,
    val cardNumber: String,
    val correspondentAccount: String,
    val balance: Double,
    val currency: String,
    val cardType: String,
    val creditLimit: Long,
    val openDate: String,
    val expiryDate: String,
    val branchName: String,
    val cardStatus: String,
    val cardStatusType: Int,
    val status: ServerStatusModel
) {
    override fun toString(): String {
        return "CardListModel(cardId=$cardId, holder='$holder', cardNumber='$cardNumber', correspondentAccount='$correspondentAccount', balance=$balance, currency='$currency', cardType='$cardType', creditLimit=$creditLimit, openDate='$openDate', expiryDate='$expiryDate', branchName='$branchName', cardStatus='$cardStatus', cardStatusType=$cardStatusType)"
    }
}