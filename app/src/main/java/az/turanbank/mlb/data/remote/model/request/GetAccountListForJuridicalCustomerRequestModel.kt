package az.turanbank.mlb.data.remote.model.request

data class GetAccountListForJuridicalCustomerRequestModel(
    val custId: Long,
    val companyId: Long,
    val token: String
)