package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetFirstSignInfoRequestModel(
        val custId: Long,
        val compId: Long,
        val token: String?,
        val userId: String?,
        val phoneNumber: String?,
        val certCode: String?,
        val lang: EnumLangType,
        val batchId: Long
)