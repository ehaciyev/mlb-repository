package az.turanbank.mlb.data.remote.model.resources.deposit

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetEarlyPaysByDepositResponseModel(
    val respDepositEarlyPays: ArrayList<EarlyPaysResponseModel>,
    val status: ServerStatusModel
)