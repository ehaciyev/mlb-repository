package az.turanbank.mlb.data.remote.model.response

data class GetBudgetCodeResponseModel (
    val budgetCodeList: ArrayList<BudgetCode>,
    val status: ServerStatusModel
)