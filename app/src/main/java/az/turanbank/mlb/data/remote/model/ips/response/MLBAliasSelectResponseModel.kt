package az.turanbank.mlb.data.remote.model.ips.response

import java.io.Serializable

data class MLBAliasSelectResponseModel (
    val id: Long,
    val ipsValue: String
): Serializable
