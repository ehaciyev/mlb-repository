package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class GetAllOperationListJuridicalRequestModel(
    val custId: Long,
    val compId: Long,
    val token: String?,
    val lang: EnumLangType
)