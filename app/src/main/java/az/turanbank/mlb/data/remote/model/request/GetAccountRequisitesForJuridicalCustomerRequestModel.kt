package az.turanbank.mlb.data.remote.model.request

data class GetAccountRequisitesForJuridicalCustomerRequestModel(
    val custId: Long,
    val compId: Long,
    val accountId: Long,
    val token: String?
)