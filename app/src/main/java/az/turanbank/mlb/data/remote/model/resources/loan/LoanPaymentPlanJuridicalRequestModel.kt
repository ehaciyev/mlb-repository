package az.turanbank.mlb.data.remote.model.resources.loan

data class LoanPaymentPlanJuridicalRequestModel(
    val custId: Long,
    val compId: Long,
    val loanId: Long,
    val token: String?
)