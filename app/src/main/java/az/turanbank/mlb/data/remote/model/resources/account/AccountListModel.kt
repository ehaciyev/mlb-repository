package az.turanbank.mlb.data.remote.model.resources.account

data class AccountListModel(
        val accountId: Long,
        val accountName: String,
        val iban: String,
        val openDate: String,
        val currName: String,
        val branchId: Long,
        val branchName: String,
        val currentBalance: Double
) {
        override fun toString(): String {
                return "AccountListModel(accountId=$accountId, accountName='$accountName', iban='$iban', openDate='$openDate', currName='$currName', branchId=$branchId, branchName='$branchName', currentBalance=$currentBalance)"
        }
}