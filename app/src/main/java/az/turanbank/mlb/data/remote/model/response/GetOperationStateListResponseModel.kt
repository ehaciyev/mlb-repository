package az.turanbank.mlb.data.remote.model.response


data class GetOperationStateListResponseModel(
    val status: ServerStatusModel,
    val respStateList: ArrayList<OperationStatusModel>
)