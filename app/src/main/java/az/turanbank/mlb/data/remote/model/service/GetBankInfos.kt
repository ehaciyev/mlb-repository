package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetBankInfos(
    val bankInfos: ArrayList<ConditionalBankList>,
    val status: ServerStatusModel
)
