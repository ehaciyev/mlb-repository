package az.turanbank.mlb.data.remote.model.ips.request

import az.turanbank.mlb.util.EnumLangType

data class DeleteIPSAliasRequestModel(
    val compId: Long?,
    val custId: Long,
    val token: String?,
    val aliasIds: ArrayList<Long>,
    val lang: EnumLangType
)