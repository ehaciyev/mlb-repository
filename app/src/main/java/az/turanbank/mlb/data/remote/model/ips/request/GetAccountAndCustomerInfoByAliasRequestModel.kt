package az.turanbank.mlb.data.remote.model.ips.request

data class GetAccountAndCustomerInfoByAliasRequestModel (
    val custId: Long,
    val token: String?,
    val aliasType: EnumAliasType,
    val value: String
)
