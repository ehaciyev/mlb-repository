package az.turanbank.mlb.data.remote.model.exchange

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class ConverterCurrencyListModel(
    val currencies: ArrayList<String>,
    val status: ServerStatusModel
)