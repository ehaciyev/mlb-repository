package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.util.EnumLangType

data class GetCampaignListRequestModel (
    val lang: EnumLangType
)