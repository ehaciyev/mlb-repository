package az.turanbank.mlb.data.remote.model.resources.loan

data class LoanPaymentPlanIndividualRequestModel(
    val custId: Long,
    val loanId: Long,
    val token: String?
)