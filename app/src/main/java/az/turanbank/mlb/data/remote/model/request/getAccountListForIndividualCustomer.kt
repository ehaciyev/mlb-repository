package az.turanbank.mlb.data.remote.model.request

data class getAccountListForIndividualCustomer(
    val custId: String,
    val token: String
)