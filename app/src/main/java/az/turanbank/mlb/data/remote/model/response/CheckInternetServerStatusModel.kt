package az.turanbank.mlb.data.remote.model.response

import java.io.Serializable

data class CheckInternetServerStatusModel(
    val status: ServerStatusModel
): Serializable
