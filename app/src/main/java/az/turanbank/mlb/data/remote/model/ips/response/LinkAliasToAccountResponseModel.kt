package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class LinkAliasToAccountResponseModel(
    val linkings: ArrayList<AliasAndAccountResponseModel>,
    val status: ServerStatusModel
)