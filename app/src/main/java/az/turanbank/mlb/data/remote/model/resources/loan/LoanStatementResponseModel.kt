package az.turanbank.mlb.data.remote.model.resources.loan

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class LoanStatementResponseModel(
    val id: Long,
    val fullName: String,
    val iban: String,
    val loanType: String,
    val loanAmount: Double,
    val currency: String,
    val annualInterestRate: Double,
    val contractNumber: String,
    val branch: String,
    val rest: Double,
    val interestRate: Double,
    val openDate: String,
    val closeDate: String,
    val payForMonth: Double,
    val loanTypeId: Int,
    val status: ServerStatusModel
)