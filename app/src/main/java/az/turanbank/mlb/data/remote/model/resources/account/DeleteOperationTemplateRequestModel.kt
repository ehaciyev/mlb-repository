package az.turanbank.mlb.data.remote.model.resources.account

data class DeleteOperationTemplateRequestModel(
    val custId: Long,
    val token: String?,
    val tempId: Long
)