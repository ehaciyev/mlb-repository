package az.turanbank.mlb.data.remote.model.request

data class CheckAppVersionRequestModel(
    val versionName: String
)