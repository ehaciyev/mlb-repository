package az.turanbank.mlb.data.remote.model.exchange

data class ExchangeCentralBankListResponseModel(
    val currency: String,
    val centralBank: Double
) {
    override fun toString(): String {
        return "ExchangeCentralBankListResponseModel(currency='$currency', centralBank=$centralBank)"
    }
}