package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class OperationAdvancedSearchRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val dtIban: String?,
    val minAmt: Double?,
    val maxAmt: Double?,
    val startDate: String?,
    val endDate: String?,
    val operStateId: Int?,
    val operNameId: Int?,
    val lang: EnumLangType
)