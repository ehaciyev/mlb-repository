package az.turanbank.mlb.data.remote.model.request

class GetCardStatementForJuridicalCustomerRequestModel (
        val custId: Long,
        val compId: Long,
        val token: String?,
        val cardNumber: String?,
        val startDate: String,
        val endDate: String
)