package az.turanbank.mlb.data.remote.model.resources.deposit

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class PayedAmountResponseModel(
    val payedAmount: Double,
    val paymentDate: String,
    val status: ServerStatusModel
)