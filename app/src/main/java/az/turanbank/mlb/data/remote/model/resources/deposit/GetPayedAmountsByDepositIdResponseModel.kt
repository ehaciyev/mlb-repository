package az.turanbank.mlb.data.remote.model.resources.deposit

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetPayedAmountsByDepositIdResponseModel(
    val respDepositPayedAmounts: ArrayList<PayedAmountResponseModel>,
    val status: ServerStatusModel
)