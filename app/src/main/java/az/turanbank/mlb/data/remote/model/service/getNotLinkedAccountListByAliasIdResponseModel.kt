package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class getNotLinkedAccountListByAliasIdResponseModel (
    val accounts: ArrayList<String>,
    val status: ServerStatusModel
)