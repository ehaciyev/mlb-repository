package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class IPSMLBAccountResponseModel(
    val id: Long,
    val ipsCustomerId: Long,
    val custId: Long,
    val ipsAccountId: Long,
    val iban: String,
    val other: String,
    val type: String,
    val currency: String,
    val bic: String,
    val memberId: String,
    val openingDate: String,
    val closingDate: String,
    val authMethod: String,
    var isSelected: Boolean,
    val status: ServerStatusModel

)