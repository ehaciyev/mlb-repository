package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class LogoutResponseModel (
    val status: ServerStatusModel
)