package az.turanbank.mlb.data.remote.model.resources.card.sms

import az.turanbank.mlb.util.EnumLangType

data class GetCardRequisitesForIndividualCustomerRequestModel(
    val custId: Long,
    val cardId: Long,
    val token: String?,
    val lang: EnumLangType
)
