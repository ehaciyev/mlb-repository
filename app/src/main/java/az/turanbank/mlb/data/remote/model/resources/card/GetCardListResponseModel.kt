package az.turanbank.mlb.data.remote.model.resources.card

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetCardListResponseModel(
        val cardList: ArrayList<CardListModel>,
        val status: ServerStatusModel
) {
    override fun toString(): String {
        return "GetCardListResponseModel(cardList=$cardList, status=$status)"
    }
}