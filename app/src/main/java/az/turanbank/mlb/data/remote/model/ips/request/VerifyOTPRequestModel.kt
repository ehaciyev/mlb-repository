package az.turanbank.mlb.data.remote.model.ips.request

import az.turanbank.mlb.util.EnumLangType

data class VerifyOTPRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val confirmCode: String
)