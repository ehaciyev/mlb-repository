package az.turanbank.mlb.data.remote.model.branch

import java.io.Serializable

data class BranchMapResponseModel(
    val branchId: Long,
    val branchName: String,
    val address: String,
    val longitude: Double,
    val latitude: Double
): Serializable