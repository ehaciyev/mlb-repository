package az.turanbank.mlb.data.remote.model.response

data class OperationStatusModel(
    val stateId: Long,
    val stateValue: String
)