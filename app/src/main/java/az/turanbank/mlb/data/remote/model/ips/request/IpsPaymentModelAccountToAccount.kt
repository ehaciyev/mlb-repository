package az.turanbank.mlb.data.remote.model.ips.request

import az.turanbank.mlb.util.EnumLangType

data class IpsPaymentModelAccountToAccount (
    val paymentAmount: Double,
    val senderCustomerName: String,
    val receiverCustomerName: String,
    val senderCustomerIban: String,
    val receiverCustomerIban: String,
    val additionalInfoAboutPayment: String,
    val additionalInfoFromCustomer: String,
    val remittanceInfo: String,
    val budgetLvl: String,
    val budgetCode: String
)