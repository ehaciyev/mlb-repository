package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class DeleteAliaseServerResponseModel(
    val aliases: ArrayList<MLBAliasResponseModel>,
    val status:  ServerStatusModel
)