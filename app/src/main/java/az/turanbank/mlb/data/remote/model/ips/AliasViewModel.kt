package az.turanbank.mlb.data.remote.model.ips

import az.turanbank.mlb.data.remote.model.ips.response.MLBAliasResponseModel

data class AliasViewModel(
    var checked: Boolean,
    val alias: MLBAliasResponseModel
) {
    override fun toString(): String {
        return "AliasViewModel(checked=$checked, alias=$alias)"
    }
}