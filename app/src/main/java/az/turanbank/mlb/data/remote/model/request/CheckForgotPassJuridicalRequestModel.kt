package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class CheckForgotPassJuridicalRequestModel(
    val pin: String,
    val taxNo: String,
    val mobile: String,
    val lang: EnumLangType
)