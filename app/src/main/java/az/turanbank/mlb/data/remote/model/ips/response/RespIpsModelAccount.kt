package az.turanbank.mlb.data.remote.model.ips.response

data class RespIpsModelAccount(
    val id: Long,
    val ipsCustomerId: Long,
    val custId: Long,
    val ipsAccountId: Long,
    val iban: String,
    val other: String,
    val type: String,
    val currency: String,
    val bic: String,
    val memberId: String,
    val openingDate: String,
    val closingDate: String,
    val authMethod: String,
    val status: RespStatus
)
