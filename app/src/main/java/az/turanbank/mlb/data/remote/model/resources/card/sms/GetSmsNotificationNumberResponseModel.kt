package az.turanbank.mlb.data.remote.model.resources.card.sms

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class GetSmsNotificationNumberResponseModel (
    val mobile: String,
    val status: ServerStatusModel
)