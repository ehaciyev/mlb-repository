package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.ips.GetIPSPaymentsModel

data class GetIPSPaymentsResponseModel(
    val payments: ArrayList<GetIPSPaymentsModel>
)