package az.turanbank.mlb.data.remote.model.ips.request

data class GetAccountByAliasRequestModel(
    val custId: Long,
    val token: String?,
    val type: String,
    val value: String
)