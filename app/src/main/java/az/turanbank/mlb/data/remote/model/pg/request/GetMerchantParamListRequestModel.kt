package az.turanbank.mlb.data.remote.model.pg.request

import az.turanbank.mlb.util.EnumLangType

data class GetMerchantParamListRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val merchantId: Long,
    val lang: EnumLangType
)