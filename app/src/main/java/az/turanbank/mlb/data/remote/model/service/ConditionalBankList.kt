package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class ConditionalBankList(
    val id: Long,
    val name: String,
    val code: Long,
    val corrAccount: String?,
    val subCorrAccount: String?,
    val swiftCode: String?,
    val testSwiftCode: String?,
    val taxNumber: String?,
    val budgetId: String,
    val budgetValue: String
)
