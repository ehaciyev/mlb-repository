package az.turanbank.mlb.data.remote.model.ips.response

data class GetAliasResponseModel(
    val accounts: ArrayList<RespIpsModelAccount>,
    val status:  RespStatus
)