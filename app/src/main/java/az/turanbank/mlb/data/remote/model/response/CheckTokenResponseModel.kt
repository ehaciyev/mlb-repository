package az.turanbank.mlb.data.remote.model.response

data class CheckTokenResponseModel(
    val customer: CustomerResponseModel,
    val company: CompanyModel,
    val status: ServerStatusModel
)