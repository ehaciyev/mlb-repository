package az.turanbank.mlb.data.remote.model.request

data class GetPinFromAsanRequestModel(
    val phoneNumber: String,
    val userId: String
)