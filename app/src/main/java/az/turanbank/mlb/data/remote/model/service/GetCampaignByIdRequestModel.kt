package az.turanbank.mlb.data.remote.model.service

import az.turanbank.mlb.util.EnumLangType

data class GetCampaignByIdRequestModel (
    val lang: EnumLangType,
    val id: Long
)
