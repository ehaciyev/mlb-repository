package az.turanbank.mlb.data.remote.model.pg.request

import az.turanbank.mlb.util.EnumLangType

data class GetPaymentListRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?,
    val lang: EnumLangType
)