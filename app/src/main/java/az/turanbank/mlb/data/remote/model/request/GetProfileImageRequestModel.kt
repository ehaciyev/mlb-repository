package az.turanbank.mlb.data.remote.model.request

data class GetProfileImageRequestModel(
    val custId: Long,
    val compId: Long?,
    val token: String?
)