package az.turanbank.mlb.data.remote.model.resources.account.exchange

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel
import java.io.Serializable

data class CreateExchangeOperationResponseModel(
    val operationId: Long,
    val operationNo: String,
    val operationName: String,
    val operationStatus: String,
    val dtIban: String,
    val dtAmount: Double,
    val dtCurrency: String,
    val crIban: String,
    val crAmount: Double,
    val crCurrency: String,
    val crName: String,
    val crTaxNo: String,
    val purpose: String,
    val status: ServerStatusModel
): Serializable