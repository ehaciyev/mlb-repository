package az.turanbank.mlb.data.remote.model.request

data class GetTokenRequestModel(
    val custId: Long,
    val device: String,
    val location: String
)