package az.turanbank.mlb.data.remote.model.resources.account

data class GetAccountStatementForIndividualCustomerRequestModel (
        val custId: Long,
        val token: String?,
        val accountId: Long,
        val startDate: String,
        val endDate: String
)