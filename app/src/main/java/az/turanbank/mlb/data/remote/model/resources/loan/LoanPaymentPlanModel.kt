package az.turanbank.mlb.data.remote.model.resources.loan

data class LoanPaymentPlanModel(
    val payDate: String,
    val rest: Double,
    val principal: Double,
    val interest: Double,
    val totalPay: Double
)