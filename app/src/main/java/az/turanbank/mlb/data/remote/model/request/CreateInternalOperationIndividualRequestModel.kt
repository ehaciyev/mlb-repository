package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class CreateInternalOperationIndividualRequestModel(
    val custId: Long,
    val token: String?,
    val dtAccountId: Long,
    val crIban: String,
    val amount: Double,
    val purpose: String,
    val taxNo: String?,
    val lang: EnumLangType,
    val sourceId: Int = 2
)