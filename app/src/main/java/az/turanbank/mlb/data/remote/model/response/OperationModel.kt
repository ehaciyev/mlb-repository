package az.turanbank.mlb.data.remote.model.response

data class OperationModel(
    val operId: Long,
    val dtName: String,
    val dtIban: String,
    val crName: String,
    val crIban: String,
    val amount: Double,
    val operPurpose: String,
    val currency: String,
    val createdDate: String,
    val operName: String,
    val operState: String,
    val operTypeId: Int,
    val crAmt: Double,
    val crCcy: String,
    val dtAmt: Double,
    val dtCcy: String,
    val operStateId: Int
)