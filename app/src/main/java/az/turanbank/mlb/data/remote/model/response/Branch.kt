package az.turanbank.mlb.data.remote.model.response

data class Branch(
    val branchId: Long,
    val branchName: String,
    val address: String,
    val bankCode: String,
    val phone: String,
    val email: String
)