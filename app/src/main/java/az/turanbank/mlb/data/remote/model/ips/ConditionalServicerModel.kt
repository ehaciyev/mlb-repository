package az.turanbank.mlb.data.remote.model.ips

data class ConditionalServicerModel(
    val bic: String
)