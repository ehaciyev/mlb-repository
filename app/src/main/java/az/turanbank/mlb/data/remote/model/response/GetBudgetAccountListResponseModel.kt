package az.turanbank.mlb.data.remote.model.response

data class GetBudgetAccountListResponseModel (
    val budgetAccounts: ArrayList<BudgetAccountModel>,
    val status: ServerStatusModel
)
