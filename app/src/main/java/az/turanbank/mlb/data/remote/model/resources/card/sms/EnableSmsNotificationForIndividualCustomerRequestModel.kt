package az.turanbank.mlb.data.remote.model.resources.card.sms

data class EnableSmsNotificationForIndividualCustomerRequestModel(
    val mobile: String,
    val repeatMobile: String,
    val custId: Long,
    val token: String?,
    val cardId: Long
)