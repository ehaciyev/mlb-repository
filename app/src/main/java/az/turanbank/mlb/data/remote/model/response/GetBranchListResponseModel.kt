package az.turanbank.mlb.data.remote.model.response

data class GetBranchListResponseModel(
    val respBranchList: ArrayList<Branch>,
    val status: ServerStatusModel
)