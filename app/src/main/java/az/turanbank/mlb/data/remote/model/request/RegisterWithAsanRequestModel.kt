package az.turanbank.mlb.data.remote.model.request

data class RegisterWithAsanRequestModel(
    val transactionId: Long,
    val certificate: String,
    val challenge: String,
    val pin: String,
    val custCode: Long,
    val sourceId: Long = 2L
)