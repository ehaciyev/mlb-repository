package az.turanbank.mlb.data.remote.model.request

data class ChangePasswordRequestModel(
    val custId: Long,
    val compId: Long?,
    val password: String,
    val repeatPassword: String
)