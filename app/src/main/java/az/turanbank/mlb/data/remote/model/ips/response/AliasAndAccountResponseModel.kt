package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class AliasAndAccountResponseModel(
    val aliasId: Long,
    val accountId: Long,
    val isDefault: Boolean,
    val aliasValue: String,
    val accountIban: String,
    val status: ServerStatusModel
)