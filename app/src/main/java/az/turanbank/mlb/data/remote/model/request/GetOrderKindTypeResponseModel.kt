package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.data.remote.model.response.OrderKind
import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

// THIS RESPONSE MODEL IS EXACTLY SAME WITH @GetOrderKindResponseModel.
// AND BOTH CLASSES SHOULD BE REFACTORED
data class GetOrderKindTypeResponseModel (
    val respOrderTypeList: ArrayList<OrderKind>,
    val status: ServerStatusModel
)