package az.turanbank.mlb.data.remote.model.resources.deposit

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class DepositListResponseModel(
        val deposits: ArrayList<DepositListModel>,
        val status: ServerStatusModel
)