package az.turanbank.mlb.data.remote.model.request

import az.turanbank.mlb.util.EnumLangType

data class SendOTPRegisterRequestModel(
    val custId: Long,
    val compId: Long?,
    val mobile: String,
    val lang: EnumLangType
)