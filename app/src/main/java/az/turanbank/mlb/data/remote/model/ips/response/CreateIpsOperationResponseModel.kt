package az.turanbank.mlb.data.remote.model.ips.response

import az.turanbank.mlb.data.remote.model.response.ServerStatusModel

data class CreateIpsOperationResponseModel(
    val operationId: Long,
    val operationNo: String,
    val dtIban: String,
    val operationName: String,
    val operationStatus: String,
    val amount: Double,
    val currency: String,
    val crName: String,
    val crIban: String,
    val crTaxNo: String,
    val purpose: String,
    val status: ServerStatusModel
)